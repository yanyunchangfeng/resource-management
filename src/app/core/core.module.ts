import { NgModule } from '@angular/core';
// import {HttpModule} from '@angular/http';
import {HttpClientModule} from "@angular/common/http";
import {AuthGuard} from '../services/auth-guard';
import {StorageService} from '../services/storage.service';
import {LoginService} from '../login/login.service';
import {MessageService} from "primeng/components/common/messageservice";
import {ConfirmationService} from 'primeng/primeng';
import {EventBusService} from '../services/event-bus.service';

@NgModule({
  exports: [HttpClientModule],
  providers: [AuthGuard, StorageService, LoginService, ConfirmationService, EventBusService,MessageService],
})
export class CoreModule { }
