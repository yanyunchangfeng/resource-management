import { NgModule } from '@angular/core';
import {LoginRoutingModule} from './login-routing.module';
import {ShareModule} from '../shared/share.module';
import {LoginComponent} from './login.component';
@NgModule({
  declarations: [LoginComponent],
  imports: [
    ShareModule,
    LoginRoutingModule,
  ]
})
export class LoginModule { }
