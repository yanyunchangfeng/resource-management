import {Injectable} from '@angular/core';
// import {Http, Response} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {StorageService} from '../services/storage.service';
import {environment} from '../../environments/environment';
import { of } from 'rxjs/observable/of';

@Injectable()
export class LoginService {
   redirectUrl: string;
  constructor(private http: HttpClient, private storageService: StorageService) {
  }

  login(loginModel): Observable<any> {

    this.storageService.setUserName('userName', loginModel.userName);
    return of(null);
  }

  isLoginIn(): boolean {
    const userName = this.storageService.getUserName('userName');
    if (userName) {
      return true;
    }else {
      return false;
    }
  }
}
