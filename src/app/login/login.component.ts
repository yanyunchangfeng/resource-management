import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoginService} from './login.service';
import {ConfirmationService} from "primeng/primeng";
import {BasePage} from "../base.page";
import {MessageService} from "primeng/components/common/messageservice";
@Component({
  selector:  'app-login',
  templateUrl:  './login.component.html',
  styleUrls:  ['./login.component.css']
})
export class LoginComponent extends BasePage implements OnInit {
  loginModel = {
    userName: 'admin',
    userPwd: '123456'
  };
  loginForm: FormGroup;
  constructor(
    private router: Router,
    private loginService: LoginService,
    public confirmationService:ConfirmationService,
    public messageService:MessageService,
    fb:FormBuilder
  ){
    super(confirmationService,messageService);
    this.loginForm = fb.group({
      // userName: ['', [Validators.required, Validators.minLength(4)]],
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
   }
  ngOnInit() {
      // console.log(this.loginForm)

      // this.loginService.restIpconfig().subscribe(data=>{
      // })
    // this.login()
  }
  //登录
  login() {
    this.loginService.login(this.loginModel).subscribe(() => {
      if (this.loginService.isLoginIn()) {
        let redirectUrl = this.loginService.redirectUrl ? this.loginService.redirectUrl : '/';
        this.router.navigate([redirectUrl]);
      }
    },(err:Error)=>{
      this.toastError(err);
    });
  }
}






