import {NgModule} from '@angular/core';
import {ShareModule} from '../shared/share.module';
import {ConnectionManagementComponent} from './connection-management/connection-management.component';
import {ConnectionInfoMangementComponent} from './connection-info-mangement/connection-info-mangement.component';
import {ConnectionViewComponent} from './connection-view/connection-view.component';
import {ConnectionRoutingModule} from './connection-routing.module';

@NgModule({
  declarations: [
    ConnectionManagementComponent,
    ConnectionInfoMangementComponent,
    ConnectionViewComponent
  ],
  imports: [
    ShareModule,
    ConnectionRoutingModule,
  ]
})
export class ConnectionModule {
}
