import { NgModule } from '@angular/core';
import { RouterModule , Routes} from '@angular/router';

import {ConnectionInfoMangementComponent} from './connection-info-mangement/connection-info-mangement.component';
import {ConnectionManagementComponent} from './connection-management/connection-management.component';
import {ConnectionViewComponent} from './connection-view/connection-view.component';

const route: Routes = [
  {path: 'connectionInfo', component: ConnectionInfoMangementComponent},
  {path: 'connection', component: ConnectionManagementComponent},
  {path: 'connectionView', component: ConnectionViewComponent},
];

@NgModule({
  imports: [RouterModule.forChild(route),
  ],
  exports: [RouterModule]
})
export class ConnectionRoutingModule {

}
