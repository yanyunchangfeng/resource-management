import { Injectable } from '@angular/core';
import { Observable} from "rxjs/Observable";
import 'rxjs/add/observable/of';

@Injectable()
export class EventService {

  constructor() { }
  getEvents(): Observable<any> {
    let data: any = [{
      title: 'All Day Event',
      start: '2017-11-13 07:00:00'
    },
    {
      title: 'Long Event',
      start: '2017-11-14 07:00:00',
      end: '2017-11-16 07:00:00',
      color: '#4c5d9e'
    },
    {
      id: 999,
      title: 'Repeating Event',
      start: '2017-11-17 07:00:00'
    },
    {
      id: 999,
      title: 'Repeating Event',
      start: '2017-11-18 07:00:00'
    },
    {
      title: 'Conference',
      start: '2017-11-19 07:00:00',
      end: '2017-11-21 07:00:00'
    },
    {
      title: 'Meeting',
      start: '2017-11-22 07:00:00',
      end: '2017-11-24 07:00:00'
    },
    {
      title: 'Lunch',
      start: '2017-11-25 07:00:00'
    },
    {
      title: 'Meeting',
      start: '2017-11-26 07:00:00'
    },
    {
      title: 'Happy Hour',
      start: '2017-11-27 07:00:00'
    },
    {
      title: 'Dinner',
      start: '2017-11-28 07:00:00'
    },
    {
      title: 'Birthday Party',
      start: '2017-11-29 07:00:00'
    },
    {
      title: 'Click for Google',
      url: 'http://google.com/',
      start: '2017-12-13 07:00:00'
    }];
    return Observable.of(data);
  }
};
