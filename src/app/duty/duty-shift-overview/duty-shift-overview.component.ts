import { Component, OnInit } from '@angular/core';
import { DutyService} from '../duty.service';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-duty-shift-overview',
  templateUrl: './duty-shift-overview.component.html',
  styleUrls: ['./duty-shift-overview.component.scss']
})
export class DutyShiftOverviewComponent implements OnInit {
    statusOptions = [];
    scheduleName: any;
    originOperater: any;
    currentOperater: any;
    msgs: Message[];
    deletemsgs: Message[];
    total: string;
    page_size: string;
    page_total: string;

    constructor(private dutyService: DutyService) { }

    ngOnInit() {
        this.dutyService.getALLshiftsLog().subscribe(res => {
            this.statusOptions = res.items;
            this.total = res.page.total;
            this.page_size = res.page.page_size;
            this.page_total = res.page.page_total;
        });
    }
    searchSchedule() {
        this.dutyService.getALLshiftsLog(this.scheduleName, this.originOperater, this.currentOperater).subscribe(res => {
            this.statusOptions = res.items;
            this.total = res.page.total;
            this.page_size = res.page.page_size;
            this.page_total = res.page.page_total;
        });
    }

    paginate(event) {
        // console.log(event);
        this.dutyService.getALLshiftsLog('', '', '', (event.page + 1).toString(), event.rows.toString()).subscribe( res => {
            this.statusOptions = res.items;
            this.total = res.page.total;
            this.page_size = res.page.page_size;
            this.page_total = res.page.page_total;
        });
    }

    clear() {
        this.scheduleName = '';
        this.originOperater =  '';
        this.currentOperater = '';
    }
}
