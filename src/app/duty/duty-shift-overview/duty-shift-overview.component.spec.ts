import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DutyShiftOverviewComponent } from './duty-shift-overview.component';

describe('DutyShiftOverviewComponent', () => {
  let component: DutyShiftOverviewComponent;
  let fixture: ComponentFixture<DutyShiftOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DutyShiftOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DutyShiftOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
