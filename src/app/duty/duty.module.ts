import {NgModule} from '@angular/core';
import {CurrentDutyComponent} from './current-duty/current-duty.component';
import {ShareModule} from '../shared/share.module';
import {DutyRoutingModule} from './duty-routing.module';
import {DutyService} from './duty.service';
import { OverviewDutyComponent } from './overview-duty/overview-duty.component';
import { ScheduleManageComponent } from './schedule-manage/schedule-manage.component';
import { ScheduleAddComponent } from './schedule-add/schedule-add.component';
import { ScheduleEditComponent } from './schedule-edit/schedule-edit.component';
import { ScheduleViewComponent } from './schedule-view/schedule-view.component';
import { ScheduleApprovalComponent } from './schedule-approval/schedule-approval.component';
import { DutyShiftOverviewComponent } from './duty-shift-overview/duty-shift-overview.component';
import { SettingDutyComponent } from './setting-duty/setting-duty.component';
import {PublicService} from '../services/public.service';
import {PublicModule} from '../public/public.module';
import { CreateHandoverComponent } from './create-handover/create-handover.component';
import { HandoverOverviewComponent } from './handover-overview/handover-overview.component';
import { MinHandoverOverviewComponent } from './min-handover-overview/min-handover-overview.component';
import { ViewHandoverComponent } from './view-handover/view-handover.component';
import { SearchFormComponent } from './public/search-form/search-form.component';
import { HandoverTableComponent } from './public/handover-table/handover-table.component';
import { BtnEditComponent } from './public/btn-edit/btn-edit.component';
import { BtnDeleteComponent } from './public/btn-delete/btn-delete.component';
import { BtnAcceptComponent } from './public/btn-accept/btn-accept.component';
import { BtnRejectComponent } from './public/btn-reject/btn-reject.component';
import { BtnRehandoverComponent } from './public/btn-rehandover/btn-rehandover.component';



@NgModule({
  declarations: [
      CurrentDutyComponent,
      OverviewDutyComponent,
      ScheduleManageComponent,
      ScheduleAddComponent,
      ScheduleEditComponent,
      ScheduleViewComponent,
      ScheduleApprovalComponent,
      DutyShiftOverviewComponent,
      SettingDutyComponent,
      CreateHandoverComponent,
      HandoverOverviewComponent,
      MinHandoverOverviewComponent,
      ViewHandoverComponent,
      SearchFormComponent,
      HandoverTableComponent,
      BtnEditComponent,
      BtnDeleteComponent,
      BtnAcceptComponent,
      BtnRejectComponent,
      BtnRehandoverComponent,
  ],
  imports: [
      ShareModule,
      DutyRoutingModule,
      PublicModule
  ],
  providers: [
      DutyService,
      PublicService
  ]
})

export class DutyModule {

}
