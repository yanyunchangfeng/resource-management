import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHandoverComponent } from './create-handover.component';

describe('CreateHandoverComponent', () => {
  let component: CreateHandoverComponent;
  let fixture: ComponentFixture<CreateHandoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHandoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHandoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
