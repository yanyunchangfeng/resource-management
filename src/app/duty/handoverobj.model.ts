export class HandoverobjModel {
    sid: string;
    creator: string;
    acceptor: string;
    status: string;
    creator_pid: string;
    creator_org: string;
    creator_org_oid: string;
    create_time: string;
    shiftexchange_type: string;
    acceptor_pid: string;
    accept_time: string;
    content: string;
    remarks: string;
    shift_name: string;
    shift_time: string;
    shift_id: string;
    accept_remarks: string;

    constructor(obj?) {
        this.sid = obj && obj['sid'] || '';
        this.creator = obj && obj['creator'] || '';
        this.acceptor = obj && obj['acceptor'] || '';
        this.status = obj && obj['status'] || '';
        this.creator_pid = obj && obj['creator_pid'] || '';
        this.creator_org = obj && obj['creator_org'] || '';
        this.creator_org_oid = obj && obj['creator_org_oid'] || '';
        this.create_time = obj && obj['create_time'] || '';
        this.shiftexchange_type = obj && obj['shiftexchange_type'] || '';
        this.acceptor_pid = obj && obj['acceptor_pid'] || '';
        this.accept_time = obj && obj['accept_time'] || '';
        this.content = obj && obj['content'] || '';
        this.remarks = obj && obj['remarks'] || '';
        this.shift_name = obj && obj['shift_name'] || '';
        this.shift_time = obj && obj['shift_time'] || '';
        this.shift_id = obj && obj['shift_id'] || '';
        this.accept_remarks = obj && obj['accept_remarks'] || '';
    }
}
