import {Component, OnInit} from '@angular/core';
import { TreeNode} from 'primeng/primeng';
import {DutyService} from '../duty.service';

@Component({
  selector: 'app-current-duty',
  templateUrl: './current-duty.component.html',
  styleUrls: ['./current-duty.component.scss']
})
export class CurrentDutyComponent implements OnInit {

  constructor(private  dutyService: DutyService) { }

  data1: TreeNode[];
  selectedNode: TreeNode;
  display  = false;
  currentTime: string;

  ngOnInit() {
      this.dutyService.getCurrentDuty().subscribe(res => {
          if (!res) {
              this.display = true;
          }else {
              this.data1 = res;
              this.display = false;
          }
      });
      this.initCurrentDuty();
      this.getcurrentTime();
  }
  initCurrentDuty() {
      window.setInterval(function () {
          this.dutyService.getCurrentDuty().subscribe(res => {
              if (!res) {
                  this.display = true;
              }else {
                  this.data1 = res;
                  this.display = false;
              }
          });
      }, 900000);

  }
  getcurrentTime() {
     window.setInterval(() => {
        this.currentTime = this.getTime();
     }, 1000);

  }
  getTime() {
      let today = new Date();
      let y, month, d, h, m, s;
      y = today.getFullYear();
      month = this.pad(today.getMonth() + 1);
      d = this.pad(today.getDate());
      h = this.pad(today.getHours());
      m = this.pad(today.getMinutes());
      s = this.pad(today.getSeconds());
      return `${y}-${month}-${d}  ${h}:${m}:${s}`;
  }
  pad(n) {
      if ( n < 10 ) {
          return `0${n}`;
      }
      return n;
  }

  onNodeSelect(event) {
    console.log(222);
  }

}
