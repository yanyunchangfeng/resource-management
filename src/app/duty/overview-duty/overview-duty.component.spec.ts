import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewDutyComponent } from './overview-duty.component';

describe('OverviewDutyComponent', () => {
  let component: OverviewDutyComponent;
  let fixture: ComponentFixture<OverviewDutyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverviewDutyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewDutyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
