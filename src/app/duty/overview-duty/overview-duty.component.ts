import { Component, OnInit } from '@angular/core';
import {DutyService} from '../duty.service';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-overview-duty',
  templateUrl: './overview-duty.component.html',
  styleUrls: ['./overview-duty.component.scss']
})
export class OverviewDutyComponent implements OnInit {
    width;
    windowSize;
    shifts;                 // 班级记录
    events: any[];          // 值班记录
    headerConfig: any;      // 日历头设置
    optionsConfig: object;  // 日历默认设置参数配置
    display;                // 控制弹框是否显示
    beginTime;              // 开始时间
    endTime;                // 结束时间
    timeObj;                // 时间对象
    statusOptions;
    selectedStatus;
    eventObj;
    msgPop: Message[] = [];                // 验证消息弹框提示
    sDutys: Object = {};

    constructor(private dutyService: DutyService) { }

    ngOnInit() {
        this.windowSize = window.innerWidth;
        if(this.windowSize<1024){
          this.width = this.windowSize*0.9;
        }else{
          this.width = this.windowSize*0.8;
        }
        this.display = false;
        this.headerConfig = {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        };
        this.optionsConfig = {
        buttonText: {
          year: '年',
          today: '今天',
          month: '月',
          week: '周',
          day: '日',
        },
        editable: true,
        eventLimit: true,
        height: 800,
        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        dayNames: ['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
        dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
        dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
        titleFormat: 'YYYY年M月'
      };
        this.getAllshiftSetting();
        this.timeObj = this.getCurrentNextTime();
        this.getAlldutys();
    }
    getCurrentNextTime(){
    const monthArra = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    const currentTime = new Date();
    let curMontDays: any = new Date(currentTime.getFullYear(), monthArra[currentTime.getMonth()], 0).getDate();
    curMontDays = curMontDays.toString().length >= 2 ? curMontDays : '0' + curMontDays;
    let currentMonth = (monthArra[currentTime.getMonth()]).toString().length >= 2 ? monthArra[currentTime.getMonth()] : '0' +  monthArra[currentTime.getMonth()];
    this.beginTime = currentTime.getFullYear()  + '-' + currentMonth + '-' + '01';
    this.endTime = currentTime.getFullYear()  + '-' + currentMonth + '-' + curMontDays;
    return { begin: this.beginTime, end: this.endTime};
  }
    getAllshiftSetting() {
        this.dutyService.getAllshiftSetting().subscribe(res => {
             this.shifts = res;
        });
    }
    getAlldutys() {
        this.dutyService.getAllDuty(this.timeObj.begin, this.timeObj.end).subscribe(res => {
             this.events = res;
        });
    }
    onEventClick(event) {
        this.display = true;
        this.dutyService.getPremissionPerson(event['calEvent']['did'], event['calEvent']['per']).subscribe(res => {
            this.statusOptions = res;
        });
        this.eventObj = event['calEvent'];
        this.sDutys = {
            snumber: event['calEvent']['did'],
            sdate: event['calEvent']['begin_time_1'],
            sshifts: event['calEvent']['sid'],
            scurrentPerson: event['calEvent']['per_name']
        };
    }
    onClickButton(event) {
        this.beginTime = event.view.start.format('YYYY-MM-DD');
        this.endTime = event.view.end.format('YYYY-MM-DD');
        // console.log(event.view.start.format('YYYY-MM-DD'));
        // console.log(event.view.end.format('YYYY-MM-DD'));
        this.dutyService.getAllDuty(this.beginTime, this.endTime).subscribe(res => {
            this.events = res;
        });
    }
    changeShift() {
        console.dir(this.eventObj);

        this.dutyService.addShift(this.eventObj['did'],
            this.eventObj['sid'],
            this.eventObj['begin_time_1'],
            this.eventObj['per'],
            this.eventObj['per_name'],
            this.selectedStatus['pid'],
            this.selectedStatus['name']).subscribe(res => {
                if (res){
                    this.msgPop = [];
                    this.msgPop.push({severity: 'success', summary: '消息提示', detail: '调班成功'});
                    this.getAlldutys();
                }else {
                    this.msgPop = [];
                    this.msgPop.push({severity: 'error', summary: '提示消息', detail: '调班失败'});
                }
        });
        this.display = false;
    }
}
