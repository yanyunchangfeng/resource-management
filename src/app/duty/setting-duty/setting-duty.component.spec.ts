import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingDutyComponent } from './setting-duty.component';

describe('SettingDutyComponent', () => {
  let component: SettingDutyComponent;
  let fixture: ComponentFixture<SettingDutyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingDutyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingDutyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
