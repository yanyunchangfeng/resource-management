import { Component, OnInit } from '@angular/core';
import {Message, TreeNode} from 'primeng/primeng';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DutyService} from '../duty.service';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-schedule-view',
  templateUrl: './schedule-view.component.html',
  styleUrls: ['./schedule-view.component.scss']
})
export class ScheduleViewComponent implements OnInit {


    beginTime: Date;                       // 开始时间
    endTime: Date;                         // 结束时间
    shiftSelectedMultipleOption: any[];    // 排班班次已选数据
    relativeSelectedMultipleOption: any[]; // 关联计划已选数据
    approveSelectedMultipleOption: any[];  // 审批人已选数据
    defaultId: string;                     // 新增排班编号
    defaultstatus: string;                     // 新增排班编号
    defaultCreator: string;                // 新增排班创建人
    defaultCreateTime: string;             // 新增排班创建时间
    defaultName: string;                   // 计划名称
    newSchdule: FormGroup;
    formBuilder: FormBuilder = new FormBuilder();
    msgs: Message[] = [];                  // 表单验证提示
    apporvalSuggestion: string;            // 审批意见
    progressBar: boolean;                  // 进度条
    currentSheduleId;                      // 当前计划ID
    selectedDepartment = '';               // 表单显示的已选中的部门组织
    msgPop: Message[] = [];                // 验证消息弹框提示
    template_file_path = '';

    constructor(private dutyService: DutyService, private activitedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.createForm();
        this.currentSheduleId = this.activitedRoute.snapshot.paramMap.get('id');
        this.dutyService.getScheduleById(this.currentSheduleId).subscribe(res => {
            console.log(res);
            this.defaultId = res.did;
            this.defaultCreator = res.creator;
            this.defaultName = res.name;
            this.defaultCreateTime = res.create_time;
            this.selectedDepartment = res.duty_org;
            this.beginTime = res['begin_time'];
            this.endTime = res.end_time;
            this.approveSelectedMultipleOption = res['approver'];
            this.shiftSelectedMultipleOption = res['shifts_name'];
            this.relativeSelectedMultipleOption = res['related_duty'];
            this.defaultstatus = res['status'];
            this.apporvalSuggestion = res.approve_remarks;
            this.template_file_path = res['template_file_path'];

        });
        this.progressBar = true;

    }

    get isName() {
        return !this.newSchdule.controls['schduleName'].untouched && (!this.newSchdule.controls['schduleName'].valid);
    }

    createForm() {
        this.newSchdule = this.formBuilder.group({
            schduleName: ['', Validators.required],
            scheduleApprovor: [this.approveSelectedMultipleOption, Validators.required],
            scheduleId: [this.defaultId, Validators.required],
            scheduleCreator: [this.defaultCreator, Validators.required],
            scheduleCreateTime: [this.defaultCreateTime, Validators.required],
            shift:  '',
            start: this.beginTime
        });
    }

    download() {
        this.progressBar = false;
        if (!this.template_file_path) {
            console.log(this.template_file_path);
            this.progressBar = true;
            this.msgPop = [];
            this.msgPop.push({severity: 'error', summary: '提示消息', detail: '下载排班计划失败：排班计划未提交' });
        }else {
            this.progressBar = true;
            this.msgPop = [];
            window.open(`${environment.url.management}/${this.template_file_path}`, '_blank');
            this.msgPop.push({severity: 'success', summary: '消息提示', detail: '模板生成成功！请在本地文件夹中查看'});
        }
    }

    hiddenProgBar() {
        this.progressBar = !this.progressBar;
    }

}
