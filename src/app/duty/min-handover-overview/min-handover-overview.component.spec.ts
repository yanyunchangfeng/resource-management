import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinHandoverOverviewComponent } from './min-handover-overview.component';

describe('MinHandoverOverviewComponent', () => {
  let component: MinHandoverOverviewComponent;
  let fixture: ComponentFixture<MinHandoverOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinHandoverOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinHandoverOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
