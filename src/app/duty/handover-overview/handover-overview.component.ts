import {Component, Input, OnInit} from '@angular/core';
import {HandoverobjModel} from '../handoverobj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {DutyService} from "../duty.service";
import {SearchObjModel} from "../searchObj.model";
import {EventBusService} from "../../services/event-bus.service";
import {PUblicMethod} from '../../services/PUblicMethod';
import {BasePage} from '../../base.page';
import {ConfirmationService} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-handover-overview',
  templateUrl: './handover-overview.component.html',
  styleUrls: ['./handover-overview.component.scss']
})
export class HandoverOverviewComponent extends BasePage implements OnInit {
    allStatus: any;
    searchObj: SearchObjModel;
    constructor(private router: Router,
                private activedRouter: ActivatedRoute,
                private eventBusService: EventBusService,
                private dutyService: DutyService,
                public c: ConfirmationService,
                public m: MessageService) {
        super(c, m);
    }

    ngOnInit() {
        this.searchObj = new SearchObjModel();
        this.getStatus();
    }
    getStatus() {
        this.dutyService.getHandoverStatus().subscribe(res => {
            if(res['errcode'] === '00000') {
                this.allStatus = PUblicMethod.formateDropDown(res['datas']);
                this.searchObj.status = this.allStatus[0]['value'];
            }else {
                this.alert(`获取所有状态失败 + ${res['errmsg']}`, 'error');
            }

        });
    }
    search() {
       this.eventBusService.handover.next(this.searchObj);
    }
    jumper() {
        this.router.navigate(['../createhandover'], {relativeTo: this.activedRouter});
    }
}
