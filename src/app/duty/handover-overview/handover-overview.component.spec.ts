import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandoverOverviewComponent } from './handover-overview.component';

describe('HandoverOverviewComponent', () => {
  let component: HandoverOverviewComponent;
  let fixture: ComponentFixture<HandoverOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandoverOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandoverOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
