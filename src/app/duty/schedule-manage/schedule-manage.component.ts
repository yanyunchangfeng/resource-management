import { Component, OnInit } from '@angular/core';
import { DutyService} from '../duty.service';
import {Message} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-schedule-manage',
  templateUrl: './schedule-manage.component.html',
  styleUrls: ['./schedule-manage.component.scss']
})
export class ScheduleManageComponent implements OnInit {

  scheduleDatas = [];
  statusOptions;
  selectedStatus;
  msgs: Message[] = [];
  deletemsgs: Message[] = [];
  scheduleName: string;
  total: string;
  page_size: string;
  page_total: string;
  dialogDisplay: boolean;
  did = [];


  constructor(private dutyService: DutyService) { }

  ngOnInit() {
      this.dutyService.getAllStatus().subscribe(res => {
          this.statusOptions = res;
      });
      this.dutyService.getAllSchduleDatas().subscribe( res => {
         this.scheduleDatas = res.items;
         this.total = res.page.total;
         this.page_size = res.page.page_size;
         this.page_total = res.page.page_total;
      });
      this.dialogDisplay = false;
  }
    editSchdule(schedule: any) {
      if (true) {
          this.showWarn('编辑');
      }
    }
    deleteSchdule(schedule: any) {
        this.did = [];
        this.dialogDisplay = true;
        this.did.push(schedule.did);
    }
    sureDelete(schedule: any){
        // if(true) {
        //     this.showWarn('删除');
        // }
        this.dutyService.deleteSchedule(this.did).subscribe(res => {
            if (res === '00000') {
                this.showSuccess();
                this.dutyService.getAllSchduleDatas().subscribe(res => {
                    this.scheduleDatas = res.items;
                    this.total = res.page.total;
                    this.page_size = res.page.page_size;
                    this.page_total = res.page.page_total;
                });
            }else {
                this.showError(res);
            }
        });
        this.dialogDisplay = false;
    }

    overviewSchdule(schedule: any) {
        this.showWarn('查看');
    }

    approvalSchdule(schedule: any) {
        this.showWarn('审批');
    }

    showWarn(operator: string) {
        this.msgs = [];
        this.msgs.push({severity: 'warn', summary: '注意', detail: '您没有' + operator + '权限！'});
    }

    showSuccess() {
        this.deletemsgs = [];
        this.deletemsgs.push({severity: 'success', summary: '消息提示', detail: '删除成功'});
    }

    showError(res) {
        this.deletemsgs = [];
        this.deletemsgs.push({severity: 'error', summary: '消息提示', detail: '删除失败' + res});
    }

    searchSchedule() {
        (!this.selectedStatus) && ( this.selectedStatus = {});
        this.dutyService.getAllSchduleDatas(this.scheduleName, this.selectedStatus['name']).subscribe(res => {
            this.scheduleDatas = res.items;
            this.total = res.page.total;
            this.page_size = res.page.page_size;
            this.page_total = res.page.page_total;
        });
    }

    paginate(event) {
        this.dutyService.getAllSchduleDatas('', '', (event.page + 1).toString(), event.rows.toString()).subscribe( res => {
            this.scheduleDatas = res.items;
            this.total = res.page.total;
            this.page_size = res.page.page_size;
            this.page_total = res.page.page_total;
        });
    }
}
