import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HandoverobjModel} from '../handoverobj.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-view-handover',
  templateUrl: './view-handover.component.html',
  styleUrls: ['./view-handover.component.scss']
})
export class ViewHandoverComponent implements OnInit {
    title: string = '查看';
    formObj: HandoverobjModel;
    constructor(private router: Router,
                private activedRouter: ActivatedRoute) {
    }

    ngOnInit() {
      this.formObj = new HandoverobjModel();
      this.getRouterParam();
    }
    getRouterParam() {
      this.activedRouter.queryParams.subscribe(param => {
        if (param['datas']) {
            this.formObj = new HandoverobjModel(JSON.parse(param['datas']));
            console.dir(JSON.parse(param['datas']));
        }
      });
    }
    goBack() {
        this.router.navigate(['../handoveroverview'], {relativeTo: this.activedRouter})
    }

}
