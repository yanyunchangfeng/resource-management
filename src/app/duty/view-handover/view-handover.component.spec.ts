import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewHandoverComponent } from './view-handover.component';

describe('ViewHandoverComponent', () => {
  let component: ViewHandoverComponent;
  let fixture: ComponentFixture<ViewHandoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewHandoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewHandoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
