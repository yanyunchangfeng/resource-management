import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HandoverobjModel} from '../../handoverobj.model';
import {EventBusService} from '../../../services/event-bus.service';
import {DutyService} from '../../duty.service';
import {ConfirmationService} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';
import {BasePage} from '../../../base.page';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PUblicMethod} from '../../../services/PUblicMethod';

@Component({
  selector: 'app-btn-reject',
  templateUrl: './btn-reject.component.html',
  styleUrls: ['./btn-reject.component.scss']
})
export class BtnRejectComponent extends BasePage implements OnInit {
  @Input() public data: any;
  display: boolean = false;
  myForm: FormGroup;
  fb: FormBuilder = new FormBuilder();
  handoverObj: HandoverobjModel;
  constructor(private router: Router,
              private dutyService: DutyService,
              private eventBusService: EventBusService,
              public c: ConfirmationService,
              public m: MessageService) {
    super(c, m);
  }

  ngOnInit() {
    this.handoverObj = new HandoverobjModel();
    this.initForm();
  }
  initForm() {
      this.myForm = this.fb.group({
          reason: [null, Validators.required]
      });
  }
  isFieldValid(field: string) {
      return PUblicMethod.isFieldValid(this.myForm, 'reason');
  }
  jumper() {
    this.handoverObj = new HandoverobjModel(this.data);
    this.handoverObj.status = '拒绝';
    this.display = true;
  }
  onHide() {
      this.display = false;
      this.initForm();
  }
  reject() {
      if (this.myForm.valid) {
          this.requestReject();
      }else {
          PUblicMethod.validateAllFormFields(this.myForm);
      }
  }
  requestReject() {
    this.dutyService.acceptOrRejectHandover(this.handoverObj).subscribe(res => {
      if (res === '00000') {
        this.alert('拒绝成功');
        this.eventBusService.handover.next(true);
      }else {
        this.alert(`拒绝失败 + ${res}`, 'error');
      }
    });
  }
}
