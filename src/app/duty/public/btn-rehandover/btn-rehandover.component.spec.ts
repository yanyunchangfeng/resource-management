import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnRehandoverComponent } from './btn-rehandover.component';

describe('BtnRehandoverComponent', () => {
  let component: BtnRehandoverComponent;
  let fixture: ComponentFixture<BtnRehandoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnRehandoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnRehandoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
