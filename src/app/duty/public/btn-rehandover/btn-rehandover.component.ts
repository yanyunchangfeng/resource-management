import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-btn-rehandover',
  templateUrl: './btn-rehandover.component.html',
  styleUrls: ['./btn-rehandover.component.scss']
})
export class BtnRehandoverComponent implements OnInit {
  @Input() public data: any;
  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }
  jumper() {
    this.router.navigate(['../createhandover'], {queryParams: {datas: JSON.stringify(this.data)} , relativeTo: this.activatedRoute});
  }
}
