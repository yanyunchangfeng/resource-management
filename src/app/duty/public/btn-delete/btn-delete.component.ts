import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HandoverobjModel} from '../../handoverobj.model';
import {DutyService} from '../../duty.service';
import {EventBusService} from '../../../services/event-bus.service';
import {ConfirmationService} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';
import {BasePage} from '../../../base.page';

@Component({
  selector: 'app-btn-delete',
  templateUrl: './btn-delete.component.html',
  styleUrls: ['./btn-delete.component.scss']
})
export class BtnDeleteComponent extends BasePage implements OnInit {
  @Input() public data: any;
  handoverObj: HandoverobjModel;
  constructor(private router: Router,
              private dutyService: DutyService,
              private eventBusService: EventBusService,
              public c: ConfirmationService,
              public m: MessageService) {
    super(c, m);
  }

  ngOnInit() {
    this.handoverObj = new HandoverobjModel();
  }
  jumper() {
    this.handoverObj = new HandoverobjModel(this.data);
    this.confirmationService.confirm({
      message: '确认删除吗？',
      accept: () => {
        this.requestDelete();
      },
      reject: () => {

      }
    });
  }
  requestDelete() {
    this.dutyService.deletHandover([this.handoverObj['sid']]).subscribe(res => {
      if (res === '00000') {
        this.alert('删除成功');
        this.eventBusService.handover.next(true);
      }else {
        this.alert(`删除失败 + ${res}`, 'error');
      }
    });
  }
}
