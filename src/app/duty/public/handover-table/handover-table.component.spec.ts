import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandoverTableComponent } from './handover-table.component';

describe('HandoverTableComponent', () => {
  let component: HandoverTableComponent;
  let fixture: ComponentFixture<HandoverTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandoverTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandoverTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
