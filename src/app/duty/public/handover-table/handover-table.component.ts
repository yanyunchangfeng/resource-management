import {Component, Input, OnInit} from '@angular/core';
import {DutyService} from '../../duty.service';
import {SearchObjModel} from '../../searchObj.model';
import {EventBusService} from '../../../services/event-bus.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-handover-table',
  templateUrl: './handover-table.component.html',
  styleUrls: ['./handover-table.component.scss']
})
export class HandoverTableComponent implements OnInit {
   @Input() wichDatas: string;
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数
    searchObj: SearchObjModel;

    constructor(private dutyService: DutyService,
                private eventBusService: EventBusService,
                private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.searchObj = new SearchObjModel();
        this.eventBusService.handover.subscribe(data => {
         (typeof data === 'boolean') && (this.getOverviewDatas());
         if (typeof data === 'object') {
           this.searchObj = data;
           this.getOverviewDatas();
         }
        });
        (!this.wichDatas) && (this.getOverviewDatas());
        (this.wichDatas) && (this.getMineDatas());
        // this.getOverviewDatas();
        // this.getMineDatas();
    }
    getOverviewDatas() {
      this.dutyService.getHandoverOverView(this.searchObj).subscribe(res => {
        if (res) {
          this.resetPage(res);
        }else {
          this.scheduleDatas = [];
        }
      });
    }
    getMineDatas() {
        if (this.wichDatas) {
            this.searchObj.status = '待接班';
            this.dutyService.getMineHandover(this.searchObj).subscribe(res => {
                if (res) {
                    this.resetPage(res);
                }else {
                    this.scheduleDatas = [];
                }
            });
        }
    }
    resetPage(res) {
      if ( 'items' in res) {
        this.scheduleDatas = res.items;
        this.total = res['page']['total'];
        this.page_size = res['page']['page_size'];
        this.page_total = res['page']['page_total'];
      }
    }
    paginate(event) {
      this.searchObj.page_size = event.rows.toString();
      this.searchObj.page_number = (event.page + 1).toString();
      this.getOverviewDatas();
    }
    onOperate(data) {
      this.router.navigate(['../viewhandover'], {queryParams: {datas: JSON.stringify(data)}, relativeTo: this.activatedRoute});
    }
}
