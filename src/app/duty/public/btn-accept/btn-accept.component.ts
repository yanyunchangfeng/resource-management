import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DutyService} from '../../duty.service';
import {HandoverobjModel} from '../../handoverobj.model';
import {BasePage} from '../../../base.page';
import {ConfirmationService} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-btn-accept',
  templateUrl: './btn-accept.component.html',
  styleUrls: ['./btn-accept.component.scss']
})
export class BtnAcceptComponent extends BasePage implements OnInit {
    @Input() public data: any;
    handoverObj: HandoverobjModel;
    constructor(private router: Router,
                private dutyService: DutyService,
                private eventBusService: EventBusService,
                public c: ConfirmationService,
                public m: MessageService) {
      super(c, m);
    }

    ngOnInit() {
      this.handoverObj = new HandoverobjModel();
    }
    jumper() {
        this.handoverObj = new HandoverobjModel(this.data);
        this.handoverObj.status = '接受';
        this.confirmationService.confirm({
          message: '确认接受吗？',
          accept: () => {
            this.requestAccept();
          },
          reject: () => {

          }
        });
    }
    requestAccept() {
      this.dutyService.acceptOrRejectHandover(this.handoverObj).subscribe(res => {
        if (res === '00000') {
          this.alert('接受成功');
          this.eventBusService.handover.next(true);
        }else {
          this.alert(`接受失败 + ${res}`, 'error');
        }
      });
    }
}
