import { NgModule } from '@angular/core';
import { RouterModule , Routes} from '@angular/router';

import {CurrentDutyComponent} from './current-duty/current-duty.component';
import {DutyShiftOverviewComponent} from './duty-shift-overview/duty-shift-overview.component';
import {OverviewDutyComponent} from './overview-duty/overview-duty.component';
import {ScheduleAddComponent} from './schedule-add/schedule-add.component';
import {ScheduleApprovalComponent} from './schedule-approval/schedule-approval.component';
import {ScheduleEditComponent} from './schedule-edit/schedule-edit.component';
import {ScheduleManageComponent} from './schedule-manage/schedule-manage.component';
import {ScheduleViewComponent} from './schedule-view/schedule-view.component';
import {SettingDutyComponent} from './setting-duty/setting-duty.component';
import {HandoverOverviewComponent} from './handover-overview/handover-overview.component';
import {MinHandoverOverviewComponent} from './min-handover-overview/min-handover-overview.component';
import {CreateHandoverComponent} from './create-handover/create-handover.component';
import {ViewHandoverComponent} from './view-handover/view-handover.component';

const route: Routes = [
    {path: '', component: CurrentDutyComponent},
    {path: 'current', component: CurrentDutyComponent},
    {path: 'add', component: ScheduleAddComponent},
    {path: 'setting', component: SettingDutyComponent},
    {path: 'dutyshift', component: DutyShiftOverviewComponent},
    {path: 'overview', component: OverviewDutyComponent},
    {path: 'approval/:id', component: ScheduleApprovalComponent},
    {path: 'edit/:id', component: ScheduleEditComponent},
    {path: 'manage', component: ScheduleManageComponent},
    {path: 'view/:id', component: ScheduleViewComponent},
    {path: 'handoveroverview', component: HandoverOverviewComponent},
    {path: 'minhandoveroverview', component: MinHandoverOverviewComponent},
    {path: 'createhandover', component: CreateHandoverComponent},
    {path: 'viewhandover', component: ViewHandoverComponent}
];
@NgModule({
    imports: [RouterModule.forChild(route)],
    exports: [RouterModule],
})

export class DutyRoutingModule {

}
