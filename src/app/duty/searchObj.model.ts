export class SearchObjModel {
  sid: string;
  creator: string;
  acceptor: string;
  status: string;
  page_size: string;
  page_number: string;

  constructor(obj?) {
    this.sid = obj && obj['sid'] || '';
    this.creator = obj && obj['creator'] || '';
    this.acceptor = obj && obj['acceptor'] || '';
    this.status = obj && obj['status'] || '';
    this.page_size = obj && obj['page_size'] || '10';
    this.page_number = obj && obj['page_number'] || '1';
  }
}
