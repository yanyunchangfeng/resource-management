export class ShiftObjModel {
    sid: string;
    name: string;
    shift_type: string;
    begin_time_1: string;
    end_time_1: string;
    begin_time_2: string;
    end_time_2: string;
    overall_time: string;
    color: string;
    status: string;

    constructor(obj?) {
        this.sid = obj && obj['sid'] || '';
        this.name = obj && obj['name'] || '';
        this.shift_type = obj && obj['shift_type'] || '';
        this.begin_time_1 = obj && obj['begin_time_1'] || '';
        this.end_time_1 = obj && obj['end_time_1'] || '';
        this.begin_time_2 = obj && obj['begin_time_2'] || '';
        this.end_time_2 = obj && obj['end_time_2'] || '';
        this.overall_time = obj && obj['overall_time'] || '';
        this.color = obj && obj['color'] || '';
        this.status = obj && obj['status'] || '';
    }

}