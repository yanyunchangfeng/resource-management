import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TreeNode} from 'primeng/primeng';
import {PublicService} from '../../services/public.service';
import {CabinetModel} from '../../capacity/cabinet.model';
import {ReportService} from "../report.service";

@Component({
  selector: 'app-report-location',
  templateUrl: './report-location.component.html',
  styleUrls: ['./report-location.component.scss']
})
export class ReportLocationComponent implements OnInit {
  width;
  windowSize;
  queryModel;
  @ViewChild('expandingTree')
  selected: TreeNode[];
  display: boolean;
  orgs: TreeNode[];                      // 组织树的数据
  treeDatas = [];
  cabinetObj: CabinetModel;
  // 表格数据
  @Output() closeLocation = new EventEmitter();
  @Output() addTree = new EventEmitter();
  @Input() treeState;
  @Input() pnAnddataSource;
  submitData;
  eventData: any;
  constructor(private publicService: PublicService,
              private reportService :ReportService,
              ) { }
  ngOnInit() {
    if(window.innerWidth<1440){
      this.width = window.innerWidth * 0.3;
    }else{
      this.width = window.innerWidth * 0.3;
    }
    this.cabinetObj = new CabinetModel();
    this.display = true;
    // 查询组织树
    this.queryModel = {
      'father_did': '',
      'sp_type_min': ''
    };
    if(this.treeState === 'update'){
      this.pnAnddataSource = this.reportService.deepClone(this.pnAnddataSource);
      this.submitData = {
        'cid':this.pnAnddataSource['cid'],
        "devices":this.pnAnddataSource['data']
      }
    }
    this.initTreeDatas();
  }
  handleRowSelect(event) {
    this.eventData = event.data;

  }
  closeLocationMask(bool) {
    this.closeLocation.emit(bool);
  }
  initTreeDatas() {
    this.publicService.getCapBasalDatas('', this.cabinetObj.sp_type).subscribe(res => {
      this.treeDatas = res;
    });
  }
  formSubmit(){
      let arr = [];
      for (let key of this.selected){
        let obj = {};
        obj['label'] = key['label'];
        obj['did'] = key['did'];
        arr.push(obj);
      }

      this.addTree.emit(arr);
  }

    onNodeSelect(event) {
      console.log(event);
    }
}
