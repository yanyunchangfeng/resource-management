import { NgModule } from '@angular/core';
import { RouterModule , Routes} from '@angular/router';
import {ReportStartComponent} from './report-start/report-start.component';
import {ReportViewComponent} from './report-view/report-view.component';
import {HistoryReportComponent} from './history-report/history-report.component';
import {ViewDetailComponent} from './view-detail/view-detail.component';

const route: Routes = [
  {path: '', component: ReportViewComponent},
  {path: 'review', component: ReportStartComponent},
  {path: 'history', component: HistoryReportComponent},
  {path: 'ViewDetail', component: ViewDetailComponent},
];
@NgModule({
  imports: [
    RouterModule.forChild(route),

  ],
  exports: [RouterModule]
})
export class ReportRoutingModule {

}
