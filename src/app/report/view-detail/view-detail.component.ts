import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {environment} from '../../../environments/environment';
@Component({
  selector: 'app-view-detail',
  templateUrl: './view-detail.component.html',
  styleUrls: ['./view-detail.component.scss']
})
export class ViewDetailComponent implements OnInit {
  display;
  @Output() closeViewDetail = new EventEmitter();
  @Input() reportDetail;
  @Input() state;
  imgUrl= [];
  width;
  windowSize;
  constructor() { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if (this.windowSize < 1024){
      this.width = this.windowSize * 0.4;
    }else{
      this.width = this.windowSize * 0.6;
    }
    this.display = true;
    if (!this.reportDetail.img_url){
      this.imgUrl = [];
    }else{
      this.imgUrl = this.reportDetail.img_url.split(';');
      for (let i = 0; i < this.imgUrl.length; i++){
        this.imgUrl[i] = environment.url.management + '/' + this.imgUrl[i];
      }
      // this.imgUrl = environment.url.management+'/'+this.reportDetail.img_url;
    }
  }
  closeViewDetailMask(bool){
    this.closeViewDetail.emit(bool);
  }
}
