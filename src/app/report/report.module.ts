import {NgModule} from '@angular/core';
import {ReportStartComponent} from './report-start/report-start.component';
import {ReportViewComponent} from './report-view/report-view.component';
import {ShareModule} from '../shared/share.module';
import {ReportRoutingModule} from './report-routing.module';
import { HistoryReportComponent } from './history-report/history-report.component';
import {ReportService} from './report.service';
import { ViewDetailComponent } from './view-detail/view-detail.component';
import { ReportTreatmentComponent } from './report-treatment/report-treatment.component';
import { ReportHandleViewComponent } from './report-handle-view/report-handle-view.component';
import { ReportViewEchartComponent } from './report-view-echart/report-view-echart.component';
import {SplitButtonModule} from "primeng/primeng";
import { PersonDialogComponent } from './person-dialog/person-dialog.component';
import {PublicService} from "../services/public.service";
import { ReportAddEquipmentComponent } from './report-add-equipment/report-add-equipment.component';
import { ReportLocationComponent } from './report-location/report-location.component';

@NgModule({
  declarations: [
    ReportStartComponent,
    ReportViewComponent,
    HistoryReportComponent,
    ViewDetailComponent,
    ReportTreatmentComponent,
    ReportHandleViewComponent,
    ReportViewEchartComponent,
    PersonDialogComponent,
    ReportAddEquipmentComponent,
    ReportLocationComponent
  ],
  imports: [
    ShareModule,
    ReportRoutingModule,
    SplitButtonModule,
  ],
  providers: [ReportService,PublicService]

})
export class ReportModule {

}
