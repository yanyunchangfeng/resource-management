import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ReportService} from '../report.service';
import {StorageService} from '../../services/storage.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {InspectionService} from '../../inspection/inspection.service';

@Component({
  selector: 'app-report-treatment',
  templateUrl: './report-treatment.component.html',
  styleUrls: ['./report-treatment.component.scss']
})
export class ReportTreatmentComponent implements OnInit {
  display;
  windowSize;
  width;
  @Output() closeReportTreatmentMask = new EventEmitter();
  @Output() updateDev = new EventEmitter(); //修改处理内容
  @Input() reportTreatment;
  reportTreamentStatus;
  treamentReportData;
  reportTreatmentForm: FormGroup; //报障处理表单验证声明
  fb: FormBuilder =  new FormBuilder();
  constructor(
    private reportService: ReportService,
    private storageService: StorageService,
  ) { }
  ngOnInit() {
    this.windowSize = window.innerWidth;
    if (this.windowSize < 1440){
      this.width = this.windowSize * 0.4;
    }else{
      this.width = this.windowSize * 0.6;
    }
    this.display = true;
    this.reportTreamentStatus = ['未处理', '已处理'];
    this.treamentReportData = {
      'id': this.reportTreatment.cid,
      'status': this.reportTreamentStatus[1],
      'handle_content': '',
      'handle_people': this.storageService.getUserName('userName'),
      'handle_datetime': ''
    };
    this.reportTreatmentForm = this.fb.group({
      'status': '',
      'handle_content': ['', Validators.required],
      'handle_people': [''],
      'handle_datetime': [''],
    });
  }
  updateReportTreamentContent(){

    this.treamentReportData.handle_datetime = this.reportService.getFormatTime();
    this.reportService.handleReport(this.treamentReportData).subscribe(data => {
      this.updateDev.emit(this.treamentReportData['handle_content']);
      this.closeReportTreatmentMask.emit(false);
    }, (err: Error) => {

    });
  }
  closeReportDetailMask(bool){
    this.closeReportTreatmentMask.emit(bool);
  }

}
