import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
import Color from '../../util/color.util';
import {IntervalObservable} from "rxjs/observable/IntervalObservable";
import {ReportService} from "../report.service";
@Component({
  selector: 'app-report-handle-view',
  templateUrl: './report-handle-view.component.html',
  styleUrls: ['./report-handle-view.component.css']
})
export class ReportHandleViewComponent implements OnInit,OnDestroy {
  @ViewChild('asset') asset: ElementRef;
  echart;
  report;
  intervalObser;
  reportName = [];
  reportCount = [];
  public opt: any = {
    color: Color.baseColor,
    title: {
      text:'今日报障总览',
      top:20,
      x:'center',
      textStyle:{//标题内容的样式
        color:'#333333',//京东红
        fontStyle:'normal',//主标题文字字体风格，默认normal，有italic(斜体),oblique(斜体)
        fontWeight:"bold",//可选normal(正常)，bold(加粗)，bolder(加粗)，lighter(变细)，100|200|300|400|500...
        // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
        fontSize:14//主题文字z字体大小，默认为18px
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // type: 'cross'
        type: 'shadow'
      }
    },
    xAxis: [
      {
        type: 'category',
        data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
        axisTick: {
          alignWithLabel: true
        }
      }
    ],
    yAxis: [{
      // name: '/万元',
      // min: 0,
      // max: 10000,
      // splitNumber: 10,
      // axisLabel: {
      //   formatter: function (value) {
      //     return (value / 10000);
      //   }
      // },
      type: 'value'

    }],
    series: [{
      name: '数量',
      type: 'bar',
      barCategoryGap: '50%',
      itemStyle: {
        normal: {
          color: params => {
            const color = Color.genColor(this.opt.series[0].data);
            return color[params.dataIndex];
          }
        }
      },
      data: [100, 200, 300, 400, 500, 600, 100, 200, 300, 400, 500, 600 ]
    }]
  };
  constructor(private reportService:ReportService) { }

  ngOnInit() {
    this.echart = echarts.init(this.asset.nativeElement);
    this.echart.setOption(this.opt);
    this.reportService.getReportView().subscribe(data=>{
      this.report = data.handle_count;
      if(!this.report){
        this.report = [];
      }
      for(let i = 0;i<this.report.length;i++){
        this.reportName.push(this.report[i].name);
        this.reportCount.push(this.report[i].count);
      }
      this.opt.series[0]['data'] = this.reportCount;
      this.opt.xAxis[0]['data'] = this.reportName;
      this.echart.setOption(this.opt);
      this.intervalObser = IntervalObservable.create(100).subscribe(index=>{
        this.initWidth();
      })
    })
  }
  initWidth(){
    this.echart.resize();
    this.intervalObser.unsubscribe();
  }
  ngOnDestroy(): void {
      this.echart.dispose(this.asset.nativeElement);
      this.echart = null;
  }
  @HostListener('window:resize', ['$event'])
  public onWindowResize(event): void {
    this.echart.resize()
  }
}
