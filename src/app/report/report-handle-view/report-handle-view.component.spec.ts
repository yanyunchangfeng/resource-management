import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportHandleViewComponent } from './report-handle-view.component';

describe('ReportHandleViewComponent', () => {
  let component: ReportHandleViewComponent;
  let fixture: ComponentFixture<ReportHandleViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportHandleViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportHandleViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
