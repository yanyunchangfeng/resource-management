import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAddEquipmentComponent } from './report-add-equipment.component';

describe('ReportAddEquipmentComponent', () => {
  let component: ReportAddEquipmentComponent;
  let fixture: ComponentFixture<ReportAddEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportAddEquipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAddEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
