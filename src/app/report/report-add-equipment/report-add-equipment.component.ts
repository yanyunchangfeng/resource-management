import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ConfirmationService} from "primeng/primeng";
import {ReportService} from "../report.service";

@Component({
  selector: 'app-report-add-equipment',
  templateUrl: './report-add-equipment.component.html',
  styleUrls: ['./report-add-equipment.component.scss']
})
export class ReportAddEquipmentComponent implements OnInit {
  display: boolean = false;
  @Output() closereportEquement = new EventEmitter;
  cols;
  width;
  dataSource;
  totalRecords;
  assets;
  queryModel;
  @Output() addDev = new EventEmitter();
  selectMaterial = [];
  constructor(
    private reportService: ReportService,
    private comfirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.queryModel = {
      "ano":"",
      "father":"",
      "card_position":"",
      "asset_no":"",
      "name":"",
      "brand":"",
      "modle":"",
      "function":"",
      "system":"",
      "status":"",
      "on_line_date_start":"",
      "on_line_date_end":"",
      "room":"",
      "cabinet":"",
      "location":"",
      "belong_dapart":"",
      "belong_dapart_manager":" ",
      "manage_dapart":"",
      "manager":"",
      "ip":"",
      "maintain_vender":""
    }
    this.display = true;
    if(window.innerWidth<1440){
      this.width = window.innerWidth * 0.6;
    }else{
      this.width = window.innerWidth * 0.6;
    }
    this.cols = [
      {field: 'ano', header: '设备编号'},
      {field: 'name', header: '设备名称'},
      {field: 'asset_no', header: '设备型号'},
      {field: 'asset_no', header: '设备位置'},
      {field: 'asset_no', header: '责任人'},
      {field: 'status', header: '设备状态'},

    ];
    this.queryAssets();

  }
  closeEquementMask(bool){
    this.closereportEquement.emit(bool)
  }

  queryAssets(){
    this.reportService.getAssets().subscribe(asset =>{
      this.dataSource = asset;
      if(this.dataSource){
        this.totalRecords = this.dataSource.length;
        this.assets = this.dataSource.slice(0,10);
      }else{
        this.totalRecords = 0;
        this.assets = []
      }
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status === 0|| JSON.parse(JSON.stringify(err)).status === 504){
        message = '似乎网络出现了问题，请联系管理员或稍后重试'
      }else{
        message =err
      }
      this.comfirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
  queryByKeyWords(){
    for(let key in this.queryModel){
      if(!this.queryModel[key]){
        this.queryModel[key] = '';
      }
      this.queryModel[key] = this.queryModel[key].trim()
    }
    this.reportService.queryByKeyWords(this.queryModel).subscribe(data=>{
      this.dataSource = data;
      if(this.dataSource){
        this.totalRecords = this.dataSource.length;
        this.assets = this.dataSource.slice(0,10);
      }else{
        this.totalRecords = 0;
        this.assets = []
      }
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status === 0|| JSON.parse(JSON.stringify(err)).status === 504){
        message = '似乎网络出现了问题，请联系管理员或稍后重试'
      }else{
        message =err
      }
      this.comfirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
  loadCarsLazy(event) {
    setTimeout(() => {
      if(this.dataSource) {
        this.assets = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 250);
  }
  clearSearch(){
    this.queryModel.ano ='';
    this.queryModel.name = '';
  }
  formSubmit(bool){
    this.addDev.emit(this.selectMaterial);
    // this.closeEquementMask(false);
  }
}
