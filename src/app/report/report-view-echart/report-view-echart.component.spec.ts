import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportViewEchartComponent } from './report-view-echart.component';

describe('ReportViewEchartComponent', () => {
  let component: ReportViewEchartComponent;
  let fixture: ComponentFixture<ReportViewEchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportViewEchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportViewEchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
