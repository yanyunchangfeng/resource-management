import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
import Color from '../../util/color.util'
import {IntervalObservable} from "rxjs/observable/IntervalObservable";
import {ReportService} from "../report.service";
@Component({
  selector: 'app-report-view-echart',
  templateUrl: './report-view-echart.component.html',
  styleUrls: ['./report-view-echart.component.css']
})
export class ReportViewEchartComponent implements OnInit,OnDestroy {

  @ViewChild('room') room: ElementRef;
  reportView;
  intervalObser;
  baseColors;
  reportViewName = [] ;
  reportViewCount = [];
  reportViewColors = [];
  option = {
    title: {
      text:'今日报障总览',
      top:20,
      x:'center',
      textStyle:{//标题内容的样式
        color:'#333333',//京东红
        fontStyle:'normal',//主标题文字字体风格，默认normal，有italic(斜体),oblique(斜体)
        fontWeight:"bold",//可选normal(正常)，bold(加粗)，bolder(加粗)，lighter(变细)，100|200|300|400|500...
        // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
        fontSize:14//主题文字z字体大小，默认为18px
      }
    },
    color: Color.baseColor,
    tooltip : {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
      type: 'scroll',
      orient: 'vertical',
      // right: 0,
      left:0,
      top: 20,
      bottom: 20,
      data: []
    },
    series : [
      {
        // name: '姓名',
        type: 'pie',
        radius : '55%',
        center: ['50%', '50%'],
        data: [{
          value: 3350,
          name: '深圳'
        },
        {
          value: 310,
          name: '北京'
        },
        {
          value: 234,
          name: '广州'
        },
        {
          value: 135,
          name: '上海'
        },
        {
          value: 1548,
          name: '长沙'
        }],
        // data: [123,435,567,667,334],
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
    ]
  };
  echart;
  constructor(private reportService:ReportService) { }

  ngOnInit() {
    this.baseColors = [
      '#25859e',
      '#6acece',
      '#e78816',
      '#eabc7f',
      '#12619d',
      '#ad2532',
      '#15938d',
      '#cb4a1c',
      '#b3aa9b',
      '#042d4c',
    ]
    this.echart = echarts.init(this.room.nativeElement);
    this.echart.setOption(this.option);
    this.reportService.getReportView().subscribe(data=>{
      this.reportView = data.type_count;
      if(!this.reportView){
        this.reportView = [];
      }
      let num = 0;
      for(let i = 0;i<this.reportView.length;i++){
        this.reportViewName.push(this.reportView[i].name);
        this.reportViewCount.push(this.reportView[i].count);
        if(num<this.baseColors.length){
          this.reportViewColors[i]= this.baseColors[num++];
        }else{
          num=0;
          this.reportViewColors[i] = this.baseColors[num++];
        }
      }
      this.option.series[0]['data'] = this.reportService.formatPieData(this.reportView);
      this.option.legend['data'] =  this.reportViewName;
      this.option.color = this.reportViewColors;
      this.echart.setOption(this.option);
      this.intervalObser=IntervalObservable.create(100).subscribe(index=>{
        this.initWidth()
      })
    })
  }
  initWidth(){
    this.echart.resize();
    this.intervalObser.unsubscribe();
  }
  ngOnDestroy(): void {
      this.echart.dispose(this.room.nativeElement);
      this.echart =null;
  }
  @HostListener('window:resize', ['$event'])
  public onWindowResize(event): void {
    this.echart.resize()
  }
}
