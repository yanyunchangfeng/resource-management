import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDealServiceComponent } from './view-deal-service.component';

describe('ViewDealServiceComponent', () => {
  let component: ViewDealServiceComponent;
  let fixture: ComponentFixture<ViewDealServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDealServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDealServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
