import { Component, OnInit } from '@angular/core';
import {FormObjModel} from '../formObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestForServiceService} from '../request-for-service.service';
import {Message} from "primeng/primeng";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-accept-service',
  templateUrl: './accept-service.component.html',
  styleUrls: ['./accept-service.component.scss']
})
export class AcceptServiceComponent implements OnInit {
    formObj: FormObjModel;
    message: Message[] = [];               // 交互提示弹出框

    constructor(private activatedRouter: ActivatedRoute,
                private router: Router,
                private eventBusService: EventBusService,
                private rfsService: RequestForServiceService) {
        this.formObj = new FormObjModel();
    }
    ngOnInit() {
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.rfsService.getTrick(this.formObj.sid).subscribe(res => {
           this.formObj.assign_time = res['assign_time'];
        });
        this.rfsService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
    }
    goBack() {
        // this.router.navigateByUrl('index/reqForService/rfsBase/serviceOverview');
        this.router.navigate(['../serviceOverview'], {relativeTo: this.activatedRouter});
    }
    submitTrick() {
        this.formObj.status = '待处理';
        this.rfsService.recieveOrRejectTrick(this.formObj).subscribe(res => {
            if ( res === '00000' ) {

                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '操作成功'});
                window.setTimeout(() => {
                    // this.router.navigate(['/index/reqForService/rfsBase/dealService', {id: this.formObj.sid, status: this.formObj.status}]);
                    this.goBack();
                }, 1100);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `操作失败${res}`});
            }
        });
    }
    reject() {
        this.formObj.status = '待分配';
        this.rfsService.recieveOrRejectTrick(this.formObj).subscribe(res => {
            if ( res === '00000' ) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '操作成功'});
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `操作失败${res}`});
            }
        });
    }

}
