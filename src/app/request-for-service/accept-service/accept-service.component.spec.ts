import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptServiceComponent } from './accept-service.component';

describe('AcceptServiceComponent', () => {
  let component: AcceptServiceComponent;
  let fixture: ComponentFixture<AcceptServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
