import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewNewServiceComponent } from './view-new-service.component';

describe('ViewNewServiceComponent', () => {
  let component: ViewNewServiceComponent;
  let fixture: ComponentFixture<ViewNewServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewNewServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewNewServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
