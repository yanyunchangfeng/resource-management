import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAssignServiceComponent } from './edit-assign-service.component';

describe('EditAssignServiceComponent', () => {
  let component: EditAssignServiceComponent;
  let fixture: ComponentFixture<EditAssignServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAssignServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAssignServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
