import {Component, OnInit} from '@angular/core';
import {EventBusService} from '../../services/event-bus.service';
import {RequestForServiceService} from '../request-for-service.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-service-overview',
  templateUrl: './service-overview.component.html',
  styleUrls: ['./service-overview.component.scss']
})
export class ServiceOverviewComponent implements OnInit {

    eventDatas: Object;                    // 子组件返回的数据
    constructor( private rfsService: RequestForServiceService,
                 private activatedRouter: ActivatedRoute,
                 private eventBusService: EventBusService) {
    }
    ngOnInit() {
        // console.log(7878878);
        this.rfsService.getFlowChart().subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
    }
    searchEmitter(event) {
        this.eventDatas = event;
    }
}
