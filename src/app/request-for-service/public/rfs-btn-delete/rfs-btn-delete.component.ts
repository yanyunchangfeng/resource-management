import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfirmationService} from 'primeng/primeng';
import {EventBusService} from '../../../services/event-bus.service';
import {RequestForServiceService} from '../../request-for-service.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-rfs-btn-delete',
  templateUrl: './rfs-btn-delete.component.html',
  styleUrls: ['./rfs-btn-delete.component.scss']
})
export class RfsBtnDeleteComponent implements OnInit {
    @Input() public data: any;
    @Output() public dataEmitter: EventEmitter<any> = new EventEmitter();
    sidArray: string[] = [];

    constructor(private router: Router,
                private activatedRouter: ActivatedRoute,
                private confirmationService: ConfirmationService,
                private rfsService: RequestForServiceService,
                private eventBusService: EventBusService){ }
    ngOnInit() {
    }
    delete(data) {
        this.sidArray.length = 0;
        this.sidArray[this.sidArray.length] = data['sid'];
        this.rfsService.getFlowChart(data['status']).subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: () => {
                this.sure();
                this.getFlowChart();
            },
            reject: () => {
                this.getFlowChart();
            }
        });
    }
    goBack() {
        this.router.navigate(['../serviceOverview'], {relativeTo: this.activatedRouter});
    }
    sure() {
        this.rfsService.deleteTrick(this.sidArray).subscribe(res => {
            this.dataEmitter.emit(res);
        });
    }
    getFlowChart() {
        this.rfsService.getFlowChart().subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
    }
}
