import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnDeleteComponent } from './rfs-btn-delete.component';

describe('RfsBtnDeleteComponent', () => {
  let component: RfsBtnDeleteComponent;
  let fixture: ComponentFixture<RfsBtnDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
