import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rfs-btn-close',
  templateUrl: './rfs-btn-close.component.html',
  styleUrls: ['./rfs-btn-close.component.scss']
})
export class RfsBtnCloseComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router) { }

    ngOnInit() {
    }
    jumper() {
        this.router.navigate([ '/index/reqForService/rfsBase/colseService', { id: this.data.sid, status: this.data.status} ]);

    }
}
