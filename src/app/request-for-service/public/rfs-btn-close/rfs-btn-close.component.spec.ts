import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnCloseComponent } from './rfs-btn-close.component';

describe('RfsBtnCloseComponent', () => {
  let component: RfsBtnCloseComponent;
  let fixture: ComponentFixture<RfsBtnCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
