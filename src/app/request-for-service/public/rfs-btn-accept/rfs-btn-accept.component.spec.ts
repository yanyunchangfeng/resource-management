import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnAcceptComponent } from './rfs-btn-accept.component';

describe('RfsBtnAcceptComponent', () => {
  let component: RfsBtnAcceptComponent;
  let fixture: ComponentFixture<RfsBtnAcceptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnAcceptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnAcceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
