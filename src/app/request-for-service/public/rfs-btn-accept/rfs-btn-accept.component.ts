import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rfs-btn-accept',
  templateUrl: './rfs-btn-accept.component.html',
  styleUrls: ['./rfs-btn-accept.component.scss']
})
export class RfsBtnAcceptComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router) { }

    ngOnInit() {
    }
    jumper() {
            this.router.navigate([ '/index/reqForService/rfsBase/acceptService', { id: this.data.sid, status: this.data.status} ]);

    }
}
