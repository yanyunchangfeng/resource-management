import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnViewComponent } from './rfs-btn-view.component';

describe('RfsBtnViewComponent', () => {
  let component: RfsBtnViewComponent;
  let fixture: ComponentFixture<RfsBtnViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
