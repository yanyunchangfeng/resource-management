import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-rfs-btn-view',
  templateUrl: './rfs-btn-view.component.html',
  styleUrls: ['./rfs-btn-view.component.scss']
})
export class RfsBtnViewComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router) { }

    ngOnInit() {
    }

    jumper() {
        switch (this.data.status) {
            case '新建':
            case '待分配':
            case '待接受':
                this.router.navigate([ '/index/reqForService/rfsBase/viewNewService', { id: this.data.sid, status: this.data.status} ]);
                break;
            case '待处理':
                this.router.navigate([ '/index/reqForService/rfsBase/viewDealService', { id: this.data.sid, status: this.data.status} ]);
                break;
            case '处理中':
                this.router.navigate([ '/index/reqForService/rfsBase/ViewSolveService', { id: this.data.sid, status: this.data.status} ]);
                break;
            case '已解决':
                this.router.navigate([ '/index/reqForService/rfsBase/viewCloseService', { id: this.data.sid, status: this.data.status} ]);
                break;
            case '已关闭':
                this.router.navigate([ '/index/reqForService/rfsBase/viewCloseService', { id: this.data.sid, status: this.data.status} ]);
                break;
        }
    }

}
