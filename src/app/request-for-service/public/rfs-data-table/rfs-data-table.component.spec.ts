import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsDataTableComponent } from './rfs-data-table.component';

describe('RfsDataTableComponent', () => {
  let component: RfsDataTableComponent;
  let fixture: ComponentFixture<RfsDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
