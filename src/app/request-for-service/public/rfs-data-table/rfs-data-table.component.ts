import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {RequestForServiceService} from '../../request-for-service.service';
import {SearchObjModel} from "../../SearchObj.model";
import {Message} from "primeng/primeng";
import {Router} from "@angular/router";

@Component({
  selector: 'app-rfs-data-table',
  templateUrl: './rfs-data-table.component.html',
  styleUrls: ['./rfs-data-table.component.scss']
})
export class RfsDataTableComponent implements OnInit, OnChanges {

    @Input() searchType;
    @Input() eventDatas: Object;
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数
    status: string;                        // 状态字段
    searchobj: SearchObjModel;             // 搜索对象
    message: Message[];

    constructor(private rfsService: RequestForServiceService,
                private router: Router) {}
    ngOnInit() {
        this.searchobj = new SearchObjModel();
        this.searchobj.status = this.searchType ? this.searchType :  '';
        this.getList(this.searchobj);
    }
    ngOnChanges(changes: SimpleChanges): void {
        if (changes['eventDatas']) {
            let res =  changes['eventDatas']['currentValue'];
            if ( res ) {
                if (res) {
                    this.scheduleDatas = res.items;
                    if (res['page']) {
                        this.total = res['page']['total'];
                        this.page_size = res['page']['page_size'];
                        this.page_total = res['page']['page_total'];
                        // console.error(this.page_total);
                    }
                }else {
                    this.scheduleDatas = [];
                }
            }
        }
    }
    getList(so: Object) {
        this.rfsService.getRfsList(so).subscribe(res => {
            if (res) {
               this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    }
    resetPage(res) {
        if ( 'items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    }
    paginate(event) {
        this.searchobj.page_size = event.rows.toString();
        this.searchobj.page_number = (event.page + 1).toString();

        this.getList(this.searchobj);
    }
    dataEmitter(event) {
        // this.getList(this.searchobj);
        this.rfsService.getRfsList(this.searchobj).subscribe(res => {
            if (res) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '删除成功'});
                if (res.length === 0){
                   this.scheduleDatas = [];
                }else {
                    this.resetPage(res);
                }
            }else {
                this.scheduleDatas = [];
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `删除失败${res}`});
            }
        });
    }
    onOperate(data) {
        switch (data.status) {
            case '新建' :
                this.router.navigate(['/index/reqForService/rfsBase/addOrEditService', { id: data.sid, status: data.status}]);
                break;
            case '待分配' :
                this.router.navigate(['/index/reqForService/rfsBase/assignService', { id: data.sid, status: data.status}]);
                break;
            case '已分配' :
                this.router.navigate(['/index/reqForService/rfsBase/acceptService', { id: data.sid, status: data.status}]);
                break;
            case '待接受' :
                this.router.navigate(['/index/reqForService/rfsBase/acceptService', { id: data.sid, status: data.status}]);
                break;
            case '待处理' :
                this.router.navigate(['/index/reqForService/rfsBase/dealService', { id: data.sid, status: data.status}]);
                break;
            case '处理中' :
                this.router.navigate(['/index/reqForService/rfsBase/solveService', { id: data.sid, status: data.status}]);
                break;
            case '已解决' :
                this.router.navigate(['/index/reqForService/rfsBase/colseService', { id: data.sid, status: data.status}]);
                break;
            case '已关闭' :
                this.router.navigate(['/index/reqForService/rfsBase/ViewSolveService', { id: data.sid, status: data.status}]);
                break;
        }
    }
}
