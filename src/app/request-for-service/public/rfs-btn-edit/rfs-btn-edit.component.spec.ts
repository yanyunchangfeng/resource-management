import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnEditComponent } from './rfs-btn-edit.component';

describe('RfsBtnEditComponent', () => {
  let component: RfsBtnEditComponent;
  let fixture: ComponentFixture<RfsBtnEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
