import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-rfs-btn-edit',
  templateUrl: './rfs-btn-edit.component.html',
  styleUrls: ['./rfs-btn-edit.component.scss']
})
export class RfsBtnEditComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router) { }

    ngOnInit() {
    }

    jumper() {
        switch (this.data.status) {
            case '新建':
                this.router.navigate([ '/index/reqForService/rfsBase/addOrEditService', { id: this.data.sid, status: this.data.status} ]);
                break;
            case '待分配':
                this.router.navigate([ '/index/reqForService/rfsBase/editAssignService', { id: this.data.sid, status: this.data.status} ]);
                break;
            case '待接受':
                this.router.navigate([ '/index/reqForService/rfsBase/editAcceptService', { id: this.data.sid, status: this.data.status} ]);
                break;
        }
    }
}
