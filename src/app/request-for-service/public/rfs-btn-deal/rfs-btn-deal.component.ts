import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rfs-btn-deal',
  templateUrl: './rfs-btn-deal.component.html',
  styleUrls: ['./rfs-btn-deal.component.scss']
})
export class RfsBtnDealComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router) { }

    ngOnInit() {
    }
    jumper() {
        this.router.navigate([ '/index/reqForService/rfsBase/solveService', { id: this.data.sid, status: this.data.status} ]);

    }

}
