import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnDealComponent } from './rfs-btn-deal.component';

describe('RfsBtnDealComponent', () => {
  let component: RfsBtnDealComponent;
  let fixture: ComponentFixture<RfsBtnDealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnDealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnDealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
