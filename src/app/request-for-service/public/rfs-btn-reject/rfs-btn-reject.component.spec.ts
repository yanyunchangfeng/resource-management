import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnRejectComponent } from './rfs-btn-reject.component';

describe('RfsBtnRejectComponent', () => {
  let component: RfsBtnRejectComponent;
  let fixture: ComponentFixture<RfsBtnRejectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnRejectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnRejectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
