import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rfs-btn-reject',
  templateUrl: './rfs-btn-reject.component.html',
  styleUrls: ['./rfs-btn-reject.component.scss']
})
export class RfsBtnRejectComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router) { }

    ngOnInit() {
    }
    jumper() {
        this.router.navigate([ '/index/reqForService/rfsBase/acceptService', { id: this.data.sid, status: this.data.status} ]);

    }

}
