import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnProcessComponent } from './rfs-btn-process.component';

describe('RfsBtnProcessComponent', () => {
  let component: RfsBtnProcessComponent;
  let fixture: ComponentFixture<RfsBtnProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
