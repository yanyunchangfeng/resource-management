import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rfs-btn-process',
  templateUrl: './rfs-btn-process.component.html',
  styleUrls: ['./rfs-btn-process.component.scss']
})
export class RfsBtnProcessComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router) { }

    ngOnInit() {
    }
    jumper() {
        this.router.navigate([ '/index/reqForService/rfsBase/dealService', { id: this.data.sid, status: this.data.status} ]);
        // this.router.navigate([ '/index/reqForService/rfsBase/dealService'], { queryParams: {id: this.data.sid, status: this.data.status }} );
    }
}
