import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsUploadViewComponent } from './rfs-upload-view.component';

describe('RfsUploadViewComponent', () => {
  let component: RfsUploadViewComponent;
  let fixture: ComponentFixture<RfsUploadViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsUploadViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsUploadViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
