import {Component, Input, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-rfs-upload-view',
  templateUrl: './rfs-upload-view.component.html',
  styleUrls: ['./rfs-upload-view.component.scss']
})
export class RfsUploadViewComponent implements OnInit {
    @Input() file;
    constructor() { }

    ngOnInit() {
    }
    download() {
        window.open(`${environment.url.management}/${this.file.path}`, '_blank');
    }
}
