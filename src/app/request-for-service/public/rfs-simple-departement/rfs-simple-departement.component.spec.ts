import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsSimpleDepartementComponent } from './rfs-simple-departement.component';

describe('RfsSimpleDepartementComponent', () => {
  let component: RfsSimpleDepartementComponent;
  let fixture: ComponentFixture<RfsSimpleDepartementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsSimpleDepartementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsSimpleDepartementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
