import { Component, OnInit } from '@angular/core';
import {FormObjModel} from '../../formObj.model';
import {ActivatedRoute} from '@angular/router';
import {RequestForServiceService} from '../../request-for-service.service';

@Component({
  selector: 'app-rfs-simple-departement',
  templateUrl: './rfs-simple-departement.component.html',
  styleUrls: ['./rfs-simple-departement.component.scss']
})
export class RfsSimpleDepartementComponent implements OnInit {
    sid: string;
    formObj: FormObjModel;
    constructor(private activatedRouter: ActivatedRoute,
                private rfsService: RequestForServiceService) {
        this.formObj = new FormObjModel();
    }

    ngOnInit() {
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.rfsService.getTrick(this.sid).subscribe(res => {
          this.formObj.acceptor_org = res['acceptor_org'];
          this.formObj.acceptor = res['acceptor'];
        });

    }

}
