import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBtnAssignComponent } from './rfs-btn-assign.component';

describe('RfsBtnAssignComponent', () => {
  let component: RfsBtnAssignComponent;
  let fixture: ComponentFixture<RfsBtnAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBtnAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBtnAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
