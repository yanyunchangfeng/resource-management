import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rfs-btn-assign',
  templateUrl: './rfs-btn-assign.component.html',
  styleUrls: ['./rfs-btn-assign.component.scss']
})
export class RfsBtnAssignComponent implements OnInit {

    @Input() public data: any;
    constructor(private router: Router) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate([ '/index/reqForService/rfsBase/assignService', { id: this.data.sid, status: this.data.status} ]);

    }
}
