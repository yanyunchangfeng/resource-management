import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsSimpleSolveComponent } from './rfs-simple-solve.component';

describe('RfsSimpleSolveComponent', () => {
  let component: RfsSimpleSolveComponent;
  let fixture: ComponentFixture<RfsSimpleSolveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsSimpleSolveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsSimpleSolveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
