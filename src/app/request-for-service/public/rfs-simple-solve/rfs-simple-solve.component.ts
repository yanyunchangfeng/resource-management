import { Component, OnInit } from '@angular/core';
import {FormObjModel} from '../../formObj.model';
import {ActivatedRoute} from '@angular/router';
import {RequestForServiceService} from '../../request-for-service.service';

@Component({
  selector: 'app-rfs-simple-solve',
  templateUrl: './rfs-simple-solve.component.html',
  styleUrls: ['./rfs-simple-solve.component.scss']
})
export class RfsSimpleSolveComponent implements OnInit {
    sid: string;
    formObj: FormObjModel;
    constructor(private activatedRouter: ActivatedRoute,
                private rfsService: RequestForServiceService) {
        this.formObj = new FormObjModel();
    }

    ngOnInit() {
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.rfsService.getTrick(this.sid).subscribe(res => {
           this.formObj.process_time = res['process_time'];
           this.formObj.finish_time = res['finish_time'];
           this.formObj.solve_org = res['solve_org'];
           this.formObj.solve_per = res['solve_per'];
           this.formObj.reason = res['reason'];
           this.formObj.means = res['means'];
        });

    }
}
