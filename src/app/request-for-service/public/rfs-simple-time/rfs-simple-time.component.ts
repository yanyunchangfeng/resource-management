import { Component, OnInit } from '@angular/core';
import {FormObjModel} from "../../formObj.model";
import {ActivatedRoute} from "@angular/router";
import {RequestForServiceService} from "../../request-for-service.service";

@Component({
  selector: 'app-rfs-simple-time',
  templateUrl: './rfs-simple-time.component.html',
  styleUrls: ['./rfs-simple-time.component.scss']
})
export class RfsSimpleTimeComponent implements OnInit {

    sid: string;
    formObj: FormObjModel;
    constructor(private activatedRouter: ActivatedRoute,
                private rfsService: RequestForServiceService) {
        this.formObj = new FormObjModel();
    }

    ngOnInit() {
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.rfsService.getTrick(this.sid).subscribe(res => {
            this.formObj.assign_time = res['assign_time'];
            this.formObj.accept_time = res['accept_time'];
        });

    }

}
