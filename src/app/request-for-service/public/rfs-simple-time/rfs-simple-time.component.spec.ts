import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsSimpleTimeComponent } from './rfs-simple-time.component';

describe('RfsSimpleTimeComponent', () => {
  let component: RfsSimpleTimeComponent;
  let fixture: ComponentFixture<RfsSimpleTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsSimpleTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsSimpleTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
