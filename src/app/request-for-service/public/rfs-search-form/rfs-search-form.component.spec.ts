import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsSearchFormComponent } from './rfs-search-form.component';

describe('RfsSearchFormComponent', () => {
  let component: RfsSearchFormComponent;
  let fixture: ComponentFixture<RfsSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsSearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
