import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PUblicMethod} from '../../../services/PUblicMethod';
import {RequestForServiceService} from '../../request-for-service.service';
import {SearchObjModel} from '../../SearchObj.model';

@Component({
  selector: 'app-rfs-search-form',
  templateUrl: './rfs-search-form.component.html',
  styleUrls: ['./rfs-search-form.component.scss']
})
export class RfsSearchFormComponent implements OnInit {

    @Input() searchType: string;
    @Output() searchInfoEmitter: EventEmitter<Object>  = new EventEmitter();
    zh: any;                               // 汉化
    searchObj: SearchObjModel;             // 请求查询对象
    allStatus: Object[];

    constructor(private rfsService: RequestForServiceService) { }

    ngOnInit() {
        this.searchObj = new SearchObjModel();
        this.zh = new PUblicMethod().initZh();
        this.getAllStatus();
    }
    getAllStatus() {
        this.rfsService.getAllRfsStatus().subscribe(res => {
            this.allStatus = PUblicMethod.formateDropDown(res);
            this.searchObj.status = this.allStatus[0]['value'];
        });
    }
    searchSchedule() {
        (this.searchObj.begin_time) && (this.searchObj.begin_time = PUblicMethod.formateEnrtyTime(this.searchObj.begin_time));
        (this.searchObj.end_time) && (this.searchObj.end_time = PUblicMethod.formateEnrtyTime(this.searchObj.end_time));
        this.rfsService.getRfsList(this.searchObj).subscribe(res => {
            if (res) {
                this.searchInfoEmitter.emit(res);
            }
        });
    }
    clearSearch() {
        this.searchObj.end_time = '';
        this.searchObj.sid = '';
        // this.searchObj.status = '';
        this.searchObj.begin_time = '';
        this.searchObj.submitter = '';
    }

}
