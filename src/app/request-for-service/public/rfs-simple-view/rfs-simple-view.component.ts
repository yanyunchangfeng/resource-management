import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RequestForServiceService} from '../../request-for-service.service';
import {FormObjModel} from '../../formObj.model';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-rfs-simple-view',
  templateUrl: './rfs-simple-view.component.html',
  styleUrls: ['./rfs-simple-view.component.scss']
})
export class RfsSimpleViewComponent implements OnInit {
    sid: string;
    formObj: FormObjModel;
    constructor(private activatedRouter: ActivatedRoute,
                private eventBusService: EventBusService,
                private rfsService: RequestForServiceService) {
        this.formObj = new FormObjModel();
    }

    ngOnInit() {
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.rfsService.getTrick(this.sid).subscribe(res => {
            this.formObj.sid = res['sid'];
            this.formObj.fromx = res['fromx'];
            this.formObj.status = res['status'];
            this.formObj.creator = res['creator'];
            this.formObj.create_time = res['create_time'];
            this.formObj.type = res['type'];
            this.formObj.submitter = res['submitter'];
            this.formObj.submitter_org = res['submitter_org'];
            this.formObj.submitter_phone = res['submitter_phone'];
            this.formObj.occurrence_time = res['occurrence_time'];
            this.formObj.deadline = res['deadline'];
            this.formObj.bt_system = res['bt_system'];
            this.formObj.influence = res['influence'];
            this.formObj.urgency = res['urgency'];
            this.formObj.priority = res['priority'];
            this.formObj.title = res['title'];
            this.formObj.content = res['content'];
            this.formObj.attachments = res['attachments'];
            this.formObj.service = res['service'];
            this.formObj.devices = res['devices'];
            this.getFlowChart();
        });


    }
    getFlowChart() {
        this.rfsService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
    }

}
