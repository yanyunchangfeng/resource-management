import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsSimpleViewComponent } from './rfs-simple-view.component';

describe('RfsSimpleViewComponent', () => {
  let component: RfsSimpleViewComponent;
  let fixture: ComponentFixture<RfsSimpleViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsSimpleViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsSimpleViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
