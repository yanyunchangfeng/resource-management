import { Component, OnInit } from '@angular/core';
import {FormObjModel} from '../formObj.model';
import {Message} from "primeng/primeng";
import {RequestForServiceService} from "../request-for-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-close-service',
  templateUrl: './close-service.component.html',
  styleUrls: ['./close-service.component.scss']
})
export class CloseServiceComponent implements OnInit {
    options: Object = [
        {name: '成功解决', code: 'NY'},
        {name: '临时解决', code: 'RM'},
        {name: '不成功', code: 'LDN'},
        {name: '撤销', code: 'IST'}
    ];
    formObj: FormObjModel;
    message: Message[] = [];               // 交互提示弹出框

    constructor(private rfsService: RequestForServiceService,
                private activatedRouter: ActivatedRoute,
                private eventBusService: EventBusService,
                private router: Router) {
    }

    ngOnInit() {
        this.formObj = new FormObjModel();
        this.formObj.close_code = this.options[0].name;
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.rfsService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
    }
    goBack() {
        this.router.navigate(['../serviceOverview'], {relativeTo: this.activatedRouter});
    }
    submitTrick() {
        console.log(this.formObj);
        if (this.formObj.close_code['name']) {
            this.formObj.close_code = this.formObj.close_code['name'];
        }
        this.rfsService.closeTrick( this.formObj ).subscribe(res => {
            if ( res === '00000' ) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '关闭成功'});
                window.setTimeout(() => {
                    this.router.navigateByUrl('/index/reqForService/rfsBase/serviceOverview');
                }, 1100);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `关闭失败${res}`});
            }
        });
    }
}
