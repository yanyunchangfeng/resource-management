import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolveServiceComponent } from './solve-service.component';

describe('SolveServiceComponent', () => {
  let component: SolveServiceComponent;
  let fixture: ComponentFixture<SolveServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolveServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolveServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
