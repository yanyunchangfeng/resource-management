import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/primeng';
import {RequestForServiceService} from '../request-for-service.service';
import {FormObjModel} from '../formObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-deal-service',
  templateUrl: './deal-service.component.html',
  styleUrls: ['./deal-service.component.scss']
})
export class DealServiceComponent implements OnInit {

    message: Message[] = [];               // 交互提示弹出框
    formObj: FormObjModel;
    constructor(private rfsService: RequestForServiceService,
                private activatedRouter: ActivatedRoute,
                private eventBusService: EventBusService,
                private router: Router) { }

    ngOnInit() {
        this.formObj = new FormObjModel();
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');

        this.rfsService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
        // this.activatedRouter.queryParams.subscribe(parm => {
        //     console.log(parm);
        // });
        this.rfsService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
    }
    goBack() {
        this.router.navigate(['../serviceOverview'], {relativeTo: this.activatedRouter});    }
    submitTrick() {
        this.rfsService.dealTrick( this.formObj ).subscribe(res => {
            if ( res === '00000' ) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '操作成功'});
                window.setTimeout(() => {
                    this.router.navigate(['/index/reqForService/rfsBase/solveService', {id: this.formObj.sid, status: this.formObj.status}]);
                }, 1100);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `操作失败${res}`});
            }
        });
    }
}
