import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealServiceComponent } from './deal-service.component';

describe('DealServiceComponent', () => {
  let component: DealServiceComponent;
  let fixture: ComponentFixture<DealServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
