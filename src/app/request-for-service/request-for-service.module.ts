import { NgModule } from '@angular/core';
import {ShareModule} from '../shared/share.module';
import {RequestForServiceService} from './request-for-service.service';
import {PublicService} from '../services/public.service';
import { RfsBasalDataComponent } from './rfs-basal-data/rfs-basal-data.component';
import {RequestForServiceRoutingModule} from './request-for-service-routing.module';
import { RfsBaseComponent } from './rfs-base/rfs-base.component';
import { AddOrEditServiceComponent } from './add-or-edit-service/add-or-edit-service.component';
import { ViewNewServiceComponent } from './view-new-service/view-new-service.component';
import { AssignServiceComponent } from './assign-service/assign-service.component';
import { EditAssignServiceComponent } from './edit-assign-service/edit-assign-service.component';
import { AcceptServiceComponent } from './accept-service/accept-service.component';
import { EditAcceptServiceComponent } from './edit-accept-service/edit-accept-service.component';
import { DealServiceComponent } from './deal-service/deal-service.component';
import { ViewDealServiceComponent } from './view-deal-service/view-deal-service.component';
import { SolveServiceComponent } from './solve-service/solve-service.component';
import { ViewSolveServiceComponent } from './view-solve-service/view-solve-service.component';
import { CloseServiceComponent } from './close-service/close-service.component';
import { ViewCloseServiceComponent } from './view-close-service/view-close-service.component';
import { UnassignServiceOverviewComponent } from './unassign-service-overview/unassign-service-overview.component';
import { UnacceptServiceOverviewComponent } from './unaccept-service-overview/unaccept-service-overview.component';
import { UndealServiceOverviewComponent } from './undeal-service-overview/undeal-service-overview.component';
import { UnsolveServiceOverviewComponent } from './unsolve-service-overview/unsolve-service-overview.component';
import { UncloseServiceOverviewComponent } from './unclose-service-overview/unclose-service-overview.component';
import { ServiceOverviewComponent } from './service-overview/service-overview.component';
import { RfsSimpleViewComponent } from './public/rfs-simple-view/rfs-simple-view.component';
import { RfsDataTableComponent } from './public/rfs-data-table/rfs-data-table.component';
import { RfsSearchFormComponent } from './public/rfs-search-form/rfs-search-form.component';
import { RfsSimpleTimeComponent } from './public/rfs-simple-time/rfs-simple-time.component';
import { RfsSimpleDepartementComponent } from './public/rfs-simple-departement/rfs-simple-departement.component';
import { RfsSimpleSolveComponent } from './public/rfs-simple-solve/rfs-simple-solve.component';
import { RfsBtnEditComponent } from './public/rfs-btn-edit/rfs-btn-edit.component';
import { RfsBtnDeleteComponent } from './public/rfs-btn-delete/rfs-btn-delete.component';
import { RfsBtnViewComponent } from './public/rfs-btn-view/rfs-btn-view.component';
import { RfsBtnAssignComponent } from './public/rfs-btn-assign/rfs-btn-assign.component';
import { RfsBtnAcceptComponent } from './public/rfs-btn-accept/rfs-btn-accept.component';
import { RfsBtnRejectComponent } from './public/rfs-btn-reject/rfs-btn-reject.component';
import { RfsBtnProcessComponent } from './public/rfs-btn-process/rfs-btn-process.component';
import { RfsBtnDealComponent } from './public/rfs-btn-deal/rfs-btn-deal.component';
import { RfsBtnCloseComponent } from './public/rfs-btn-close/rfs-btn-close.component';
import { RfsUploadViewComponent } from './public/rfs-upload-view/rfs-upload-view.component';
import {PublicModule} from '../public/public.module';

@NgModule({
    declarations: [
        RfsBasalDataComponent,
        RfsBaseComponent,
        AddOrEditServiceComponent,
        ViewNewServiceComponent,
        AssignServiceComponent,
        EditAssignServiceComponent,
        AcceptServiceComponent,
        EditAcceptServiceComponent,
        DealServiceComponent,
        ViewDealServiceComponent,
        SolveServiceComponent,
        ViewSolveServiceComponent,
        CloseServiceComponent,
        ViewCloseServiceComponent,
        UnassignServiceOverviewComponent,
        UnacceptServiceOverviewComponent,
        UndealServiceOverviewComponent,
        UnsolveServiceOverviewComponent,
        UncloseServiceOverviewComponent,
        ServiceOverviewComponent,
        RfsSimpleViewComponent,
        RfsDataTableComponent,
        RfsSearchFormComponent,
        RfsSimpleTimeComponent,
        RfsSimpleDepartementComponent,
        RfsSimpleSolveComponent,
        RfsBtnEditComponent,
        RfsBtnDeleteComponent,
        RfsBtnViewComponent,
        RfsBtnAssignComponent,
        RfsBtnAcceptComponent,
        RfsBtnRejectComponent,
        RfsBtnProcessComponent,
        RfsBtnDealComponent,
        RfsBtnCloseComponent,
        RfsUploadViewComponent
    ],
    imports: [
        ShareModule,
        RequestForServiceRoutingModule,
        PublicModule
    ],
    providers: [
        RequestForServiceService,
        PublicService
    ]
})
export class RequestForServiceModule { }
