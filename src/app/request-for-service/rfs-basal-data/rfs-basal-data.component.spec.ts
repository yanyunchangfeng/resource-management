import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBasalDataComponent } from './rfs-basal-data.component';

describe('RfsBasalDataComponent', () => {
  let component: RfsBasalDataComponent;
  let fixture: ComponentFixture<RfsBasalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBasalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBasalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
