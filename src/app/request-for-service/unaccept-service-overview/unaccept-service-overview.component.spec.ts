import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnacceptServiceOverviewComponent } from './unaccept-service-overview.component';

describe('UnacceptServiceOverviewComponent', () => {
  let component: UnacceptServiceOverviewComponent;
  let fixture: ComponentFixture<UnacceptServiceOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnacceptServiceOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnacceptServiceOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
