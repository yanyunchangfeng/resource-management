import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UndealServiceOverviewComponent } from './undeal-service-overview.component';

describe('UndealServiceOverviewComponent', () => {
  let component: UndealServiceOverviewComponent;
  let fixture: ComponentFixture<UndealServiceOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UndealServiceOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UndealServiceOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
