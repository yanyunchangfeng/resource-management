import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UncloseServiceOverviewComponent } from './unclose-service-overview.component';

describe('UncloseServiceOverviewComponent', () => {
  let component: UncloseServiceOverviewComponent;
  let fixture: ComponentFixture<UncloseServiceOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UncloseServiceOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UncloseServiceOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
