import { Component, OnInit } from '@angular/core';
import {EventBusService} from "../../services/event-bus.service";
import {RequestForServiceService} from "../request-for-service.service";

@Component({
  selector: 'app-unclose-service-overview',
  templateUrl: './unclose-service-overview.component.html',
  styleUrls: ['./unclose-service-overview.component.scss']
})
export class UncloseServiceOverviewComponent implements OnInit {
    searchType = '已解决';
    eventDatas: Object;                    // 子组件返回的数据
    constructor(private eventBusService: EventBusService,
                private rfsService: RequestForServiceService) { }

    ngOnInit() {
        this.rfsService.getFlowChart().subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
    }
    searchEmitter(event) {
        this.eventDatas = event;
    }
}
