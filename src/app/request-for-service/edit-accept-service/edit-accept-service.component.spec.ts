import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAcceptServiceComponent } from './edit-accept-service.component';

describe('EditAcceptServiceComponent', () => {
  let component: EditAcceptServiceComponent;
  let fixture: ComponentFixture<EditAcceptServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAcceptServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAcceptServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
