import { Component, OnInit } from '@angular/core';
import {EventBusService} from "../../services/event-bus.service";
import {RequestForServiceService} from "../request-for-service.service";

@Component({
  selector: 'app-unsolve-service-overview',
  templateUrl: './unsolve-service-overview.component.html',
  styleUrls: ['./unsolve-service-overview.component.scss']
})
export class UnsolveServiceOverviewComponent implements OnInit {
    searchType = '处理中';
    eventDatas: Object;                    // 子组件返回的数据
    constructor(private eventBusService: EventBusService,
                private rfsService: RequestForServiceService) { }

    ngOnInit() {
        this.rfsService.getFlowChart().subscribe(res => {
            this.eventBusService.rfs.next(res);
        });
    }
    searchEmitter(event) {
        this.eventDatas = event;
    }

}
