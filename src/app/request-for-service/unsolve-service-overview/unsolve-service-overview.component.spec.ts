import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnsolveServiceOverviewComponent } from './unsolve-service-overview.component';

describe('UnsolveServiceOverviewComponent', () => {
  let component: UnsolveServiceOverviewComponent;
  let fixture: ComponentFixture<UnsolveServiceOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnsolveServiceOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsolveServiceOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
