import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrEditServiceComponent } from './add-or-edit-service.component';

describe('AddOrEditServiceComponent', () => {
  let component: AddOrEditServiceComponent;
  let fixture: ComponentFixture<AddOrEditServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrEditServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrEditServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
