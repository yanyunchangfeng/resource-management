import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../services/storage.service';

@Injectable()
export class RequestForServiceService {
    ip = environment.url.management;

    constructor(private http: HttpClient,
                private storageService: StorageService) { }

    //    新增服务请求基础数据
    addRfsBasalDatas(name, status, father, dep) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!father) && (father = '');
        (!dep) && (dep = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'servicerequest_tree_add',
                'data': {
                    'name': name,
                    'remark': '',
                    'del': '',
                    'dep': dep,
                    'father': father,
                    'status': status,
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //    编辑/冻结/启用故障管理基础数据
    editRfsBasalDatas(name, status, did, dep, father) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!did) && (did = '');
        (!dep) && (dep = '');
        (!father) && (father = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'servicerequest_tree_mod',
                'data': {
                    'did': did,
                    'name': name,
                    'remark': '',
                    'del': '',
                    'dep': dep,
                    'father': father,
                    'status': status,
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //    删除服务请求基础数据
    deleteRfsDatas(ids) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'servicerequest_tree_del',
                'ids': ids
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //    流程图接口获取
    getFlowChart(status?) {
        status = status ? status : '';
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_workflow_get',
                'data': {
                    'status': status
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

    //  获取、查询故障管理总览列表接口
    getRfsList(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_get',
                'data': {
                    'condition': {
                        'sid': obj['sid'],
                        'submitter': obj['submitter'],
                        'begin_time': obj['begin_time'],
                        'end_time': obj['end_time'],
                        'status': obj['status']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }

            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    //  获取服务请求所有状态获取接口
    getAllRfsStatus() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_statuslist_get'
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

    // 获取服务请求来源、服务类型、所属系统、影响度、紧急度接口
    getParamOptions(){
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'servicerequest_config_get',
                'ids': [
                    '来源',
                    '服务类型',
                    '所属系统',
                    '影响度',
                    '紧急度'
                ]
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    }

    //  服务请求优先级获取接口
    getPrority(influency, uergency) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'servicerequest_priority_get',
                'data': {
                    'influence': influency,
                    'urgency': uergency
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    }

    //  获取最终期限
    getDeadline(influency: string, urgency: string, prority: string, time_source ) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'servicerequest_deadline_get',
                'data': {
                    'time_source': time_source,
                    'influence': influency ,
                    'urgency': urgency,
                    'priority': prority
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    }

    //  请求单保存
    saveTicket(ticket: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_save',
                'data': {
                    'sid': ticket['sid'],
                    'name': ticket['name'],
                    'fromx': ticket['fromx'],
                    'type': ticket['type'],
                    'status': ticket['status'],
                    'submitter': ticket['submitter'],
                    'submitter_pid': ticket['submitter_pid'],
                    'submitter_org': ticket['submitter_org'],
                    'submitter_org_oid': ticket['submitter_org_oid'],
                    'submitter_phone': ticket['submitter_phone'],
                    'occurrence_time': ticket['occurrence_time'],
                    'deadline': ticket['deadline'],
                    'influence': ticket['influence'],
                    'urgency': ticket['urgency'],
                    'priority': ticket['priority'],
                    'level': ticket['level'],
                    'bt_system': ticket['bt_system'],
                    'addr': ticket['addr'],
                    'service': ticket['service'],
                    'devices': ticket['devices'],
                    'title': ticket['title'],
                    'content': ticket['content'],
                    'attachments': ticket['attachments'],
                    'acceptor': ticket['acceptor'],
                    'acceptor_pid': ticket['acceptor_pid'],
                    'acceptor_org': ticket['acceptor_org'],
                    'acceptor_org_oid': ticket['acceptor_org_oid']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单提交、提交并分配
    submitTicket(ticket: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_add',
                'data': {
                    'sid': ticket['sid'],
                    'name': ticket['name'],
                    'fromx': ticket['fromx'],
                    'type': ticket['type'],
                    'status': ticket['status'],
                    'submitter': ticket['submitter'],
                    'submitter_pid': ticket['submitter_pid'],
                    'submitter_org': ticket['submitter_org'],
                    'submitter_org_oid': ticket['submitter_org_oid'],
                    'submitter_phone': ticket['submitter_phone'],
                    'occurrence_time': ticket['occurrence_time'],
                    'deadline': ticket['deadline'],
                    'influence': ticket['influence'],
                    'urgency': ticket['urgency'],
                    'priority': ticket['priority'],
                    'bt_system': ticket['bt_system'],
                    'service': ticket['service'],
                    'devices': ticket['devices'],
                    'title': ticket['title'],
                    'content': ticket['content'],
                    'attachments': ticket['attachments'],
                    'acceptor': ticket['acceptor'],
                    'acceptor_pid': ticket['acceptor_pid'],
                    'acceptor_org': ticket['acceptor_org'] ,
                    'acceptor_org_oid': ticket['acceptor_org_oid']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单信息获取
    getTrick(trickSid: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_get_byid',
                'id': trickSid
            }
        ).map((res: Response) => {
            return res['data'];
        });
    }

    // 请求单分配
    assignTrick(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_assign',
                'data': {
                    'sid':  obj['sid'],
                    'acceptor':  obj['acceptor'],
                    'acceptor_pid':  obj['acceptor_pid'],
                    'acceptor_org':  obj['acceptor_org'],
                    'acceptor_org_oid': obj['acceptor_org_oid'],
                    'status': obj['status']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 请求单接受、拒绝
    recieveOrRejectTrick(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_accept',
                'data': {
                    'sid': obj['sid'],
                    'status': obj['status']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 请求单处理
    dealTrick(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_process',
                'data': {
                    'sid': obj['sid'],
                    'solve_per': obj['solve_per'],
                    'solve_per_pid': obj['solve_per_pid'],
                    'solve_org': obj['solve_org'],
                    'solve_org_oid': obj['solve_org_oid'],
                    'reason': obj['reason'],
                    'means': obj['means'],
                    'finish_time': obj['finish_time']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 请求单保存、解决
    saveOrSolveTrick(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_finish',
                'data': {
                    'sid': obj['sid'],
                    'status': obj['status'],
                    'solve_per': obj['solve_per'],
                    'solve_per_pid': obj['solve_per_pid'],
                    'solve_org': obj['solve_org'],
                    'solve_org_oid': obj['solve_org_oid'],
                    'reason': obj['reason'],
                    'means': obj['means'],
                    'finish_time': obj['finish_time']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 请求单关闭
    closeTrick(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_close',
                'data': {
                    'sid': obj['sid'],
                    'close_code': obj['close_code']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 请求单刪除
    deleteTrick(ids: Array<string>) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'servicerequest_del',
                'ids': ids
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
}
