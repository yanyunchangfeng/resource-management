export class SearchObjModel {
    sid: string =  '';
    submitter: string =  '';
    begin_time: string =  '';
    end_time: string =  '';
    status: any =  '';
    page_size: string =  '10';
    page_number: string =  '1';
    constructor() {
    }
}