import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RfsBasalDataComponent} from './rfs-basal-data/rfs-basal-data.component';
import {RfsBaseComponent} from './rfs-base/rfs-base.component';
import {ServiceOverviewComponent} from './service-overview/service-overview.component';
import {AddOrEditServiceComponent} from './add-or-edit-service/add-or-edit-service.component';
import {UnassignServiceOverviewComponent} from './unassign-service-overview/unassign-service-overview.component';
import {UnacceptServiceOverviewComponent} from './unaccept-service-overview/unaccept-service-overview.component';
import {UndealServiceOverviewComponent} from './undeal-service-overview/undeal-service-overview.component';
import {UnsolveServiceOverviewComponent} from './unsolve-service-overview/unsolve-service-overview.component';
import {UncloseServiceOverviewComponent} from './unclose-service-overview/unclose-service-overview.component';
import {ViewNewServiceComponent} from './view-new-service/view-new-service.component';
import {ViewDealServiceComponent} from './view-deal-service/view-deal-service.component';
import {ViewSolveServiceComponent} from './view-solve-service/view-solve-service.component';
import {ViewCloseServiceComponent} from './view-close-service/view-close-service.component';
import {AcceptServiceComponent} from './accept-service/accept-service.component';
import {AssignServiceComponent} from './assign-service/assign-service.component';
import {CloseServiceComponent} from './close-service/close-service.component';
import {DealServiceComponent} from './deal-service/deal-service.component';
import {EditAssignServiceComponent} from './edit-assign-service/edit-assign-service.component';
import {EditAcceptServiceComponent} from './edit-accept-service/edit-accept-service.component';
import {SolveServiceComponent} from './solve-service/solve-service.component';

const route: Routes = [
    {path: '', component: RfsBaseComponent},
    {path: 'rfsBasalData', component: RfsBasalDataComponent},
    {path: 'rfsBase', component: RfsBaseComponent, children: [
        // {path: '', component: ServiceOverviewComponent},
        {path: 'addOrEditService', component: AddOrEditServiceComponent},
        {path: 'serviceOverview', component: ServiceOverviewComponent},
        {path: 'unAssignOverview', component: UnassignServiceOverviewComponent},
        {path: 'unAcceptOverview', component: UnacceptServiceOverviewComponent},
        {path: 'unDealOverview', component: UndealServiceOverviewComponent},
        {path: 'unSolveOverview', component: UnsolveServiceOverviewComponent},
        {path: 'uncloseOverview', component: UncloseServiceOverviewComponent},
        {path: 'viewNewService', component: ViewNewServiceComponent},
        {path: 'viewDealService', component: ViewDealServiceComponent},
        {path: 'ViewSolveService', component: ViewSolveServiceComponent},
        {path: 'viewCloseService', component: ViewCloseServiceComponent},
        {path: 'acceptService', component: AcceptServiceComponent},
        {path: 'editAcceptService', component: EditAcceptServiceComponent},
        {path: 'assignService', component: AssignServiceComponent},
        {path: 'editAssignService', component: EditAssignServiceComponent},
        {path: 'colseService', component: CloseServiceComponent},
        {path: 'solveService', component: SolveServiceComponent},
        {path: 'dealService', component: DealServiceComponent},
        {path: 'assignService', component: AssignServiceComponent},
    ]}
];
@NgModule({
  imports: [RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class RequestForServiceRoutingModule { }
