import { Component, OnInit } from '@angular/core';
import {FormObjModel} from '../formObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../services/event-bus.service';
import {RequestForServiceService} from '../request-for-service.service';

@Component({
  selector: 'app-view-close-service',
  templateUrl: './view-close-service.component.html',
  styleUrls: ['./view-close-service.component.scss']
})
export class ViewCloseServiceComponent implements OnInit {
    formObj: FormObjModel;
    constructor(private activatedRouter: ActivatedRoute,
                private rfsService: RequestForServiceService,
                private router: Router) {
      this.formObj = new FormObjModel();
    }

    ngOnInit() {
          this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
          this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
          this.rfsService.getTrick(this.formObj.sid).subscribe(res => {
              this.formObj.close_code = res['close_code'];
              this.formObj.close_time = res['close_time'];
          });
    }
    goBack() {
        this.router.navigate(['../serviceOverview'], {relativeTo: this.activatedRouter});
    }
}
