import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCloseServiceComponent } from './view-close-service.component';

describe('ViewCloseServiceComponent', () => {
  let component: ViewCloseServiceComponent;
  let fixture: ComponentFixture<ViewCloseServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCloseServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCloseServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
