import { Component, OnInit } from '@angular/core';
import {FormObjModel} from '../formObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../services/event-bus.service';
import {RequestForServiceService} from '../request-for-service.service';

@Component({
  selector: 'app-view-solve-service',
  templateUrl: './view-solve-service.component.html',
  styleUrls: ['./view-solve-service.component.scss']
})
export class ViewSolveServiceComponent implements OnInit {
    formObj: FormObjModel;
    constructor(private activatedRouter: ActivatedRoute,
                private router: Router) {
        this.formObj = new FormObjModel();
    }
    ngOnInit() {
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
    }
    goBack() {
        this.router.navigate(['../serviceOverview'], {relativeTo: this.activatedRouter});
    }
}
