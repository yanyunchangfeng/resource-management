import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSolveServiceComponent } from './view-solve-service.component';

describe('ViewSolveServiceComponent', () => {
  let component: ViewSolveServiceComponent;
  let fixture: ComponentFixture<ViewSolveServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSolveServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSolveServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
