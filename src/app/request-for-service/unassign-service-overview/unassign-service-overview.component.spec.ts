import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnassignServiceOverviewComponent } from './unassign-service-overview.component';

describe('UnassignServiceOverviewComponent', () => {
  let component: UnassignServiceOverviewComponent;
  let fixture: ComponentFixture<UnassignServiceOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnassignServiceOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnassignServiceOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
