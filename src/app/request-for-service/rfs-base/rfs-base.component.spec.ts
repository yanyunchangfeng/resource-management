import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfsBaseComponent } from './rfs-base.component';

describe('RfsBaseComponent', () => {
  let component: RfsBaseComponent;
  let fixture: ComponentFixture<RfsBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfsBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfsBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
