import { Component, OnInit } from '@angular/core';
import {RequestForServiceService} from '../request-for-service.service';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-rfs-base',
  templateUrl: './rfs-base.component.html',
  styleUrls: ['./rfs-base.component.scss']
})
export class RfsBaseComponent implements OnInit {
    flowChartDatas = [];                    // 流程图节点数据

    constructor( private rfsService: RequestForServiceService,
                 private eventBusService: EventBusService) {

    }

    ngOnInit() {
        this.rfsService.getFlowChart().subscribe(res => {
            this.flowChartDatas = res;
        });
        this.eventBusService.rfs.subscribe((res) => {
            // console.log(res);
            this.flowChartDatas = res;
        });
    }

}
