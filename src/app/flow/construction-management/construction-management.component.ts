import {Component, OnInit} from '@angular/core';
// import * as jsp from 'jsplumb/dist/js/jsplumb.js';
@Component({
  selector: 'app-construction-management',
  templateUrl: './construction-management.component.html',
  styleUrls: ['./construction-management.component.css']
})
export class ConstructionManagementComponent implements OnInit {
  flowPicture;
  constructor() {
    this.flowPicture=[
      {name:'开始',editing:false,editor:false},
      {name:'设备上线申请',editing:false,editor:false},
      {name:'录入安装信息',editing:false,editor:false},
      {name:'审核',editing:false,editor:false}
    ]
  }

  ngOnInit() {
  }
  change(i){
    this.flowPicture[i].editing = true
    this.flowPicture[i].editor = true
  }
  recover(i){
    this.flowPicture[i].editing = false
    this.flowPicture[i].editor = false
  }
  deleteNode(i){
    this.flowPicture.splice(i,1)
  }
  addNode(i){
    if((i+1)===this.flowPicture.length) {
      this.flowPicture.push({name:'',editing:false,editor:false})
    }else{
      this.flowPicture.splice(i-1,0,{name:'',editing:false,editor:false})
    }
  }
}

