import {NgModule} from '@angular/core';
import {ShareModule} from "../shared/share.module"
import {FlowRoutingModule} from './flow-routing.module';
import { DeviceOnlineComponent } from './device-online/device-online.component';
import { DeviceOfflineComponent } from './device-offline/device-offline.component';
import { DeviceChangedManagementComponent } from './device-changed-management/device-changed-management.component';
import { MetrialAccessComponent } from './metrial-access/metrial-access.component';
import { ConstructionManagementComponent } from './construction-management/construction-management.component';

@NgModule({
  imports: [ShareModule,FlowRoutingModule],
  declarations: [
    DeviceOnlineComponent,
    DeviceOfflineComponent,
    DeviceChangedManagementComponent,
    MetrialAccessComponent,
    ConstructionManagementComponent]
})
export class FlowModule {

}
