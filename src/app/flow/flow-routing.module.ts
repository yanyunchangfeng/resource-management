import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ConstructionManagementComponent} from './construction-management/construction-management.component';
import {DeviceChangedManagementComponent} from './device-changed-management/device-changed-management.component';
import {DeviceOfflineComponent} from './device-offline/device-offline.component';
import {DeviceOnlineComponent} from './device-online/device-online.component';
import {MetrialAccessComponent} from './metrial-access/metrial-access.component';

const route: Routes = [
  {path: 'construction', component: ConstructionManagementComponent},
  {path: 'device', component: DeviceChangedManagementComponent},
  {path: 'offline', component: DeviceOfflineComponent},
  {path: 'online', component: DeviceOnlineComponent},
  {path: 'metrial', component: MetrialAccessComponent},
];
@NgModule({
  imports: [RouterModule.forChild(route),
  ],
  exports: [RouterModule]
})
export class FlowRoutingModule{

}
