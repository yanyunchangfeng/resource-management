import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {BasicOverviewTableComponent} from '../public/basic-overview-table/basic-overview-table.component';

@Component({
  selector: 'app-unapproval-overview',
  templateUrl: './unapproval-overview.component.html',
  styleUrls: ['./unapproval-overview.component.scss']
})
export class UnapprovalOverviewComponent implements OnInit {
    @ViewChild('basicOverViewTable') basicOverViewTable: BasicOverviewTableComponent;
    constructor() { }

    ngOnInit() {
    }

    getSearchObjEmitter = seachObj => {
        this.basicOverViewTable.searchObj = seachObj;
        this.basicOverViewTable.getDatas();
    }

}
