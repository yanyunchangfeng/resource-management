import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUnapprovalNoteComponent } from './view-unapproval-note.component';

describe('ViewUnapprovalNoteComponent', () => {
  let component: ViewUnapprovalNoteComponent;
  let fixture: ComponentFixture<ViewUnapprovalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUnapprovalNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUnapprovalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
