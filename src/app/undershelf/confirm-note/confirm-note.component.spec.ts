import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmNoteComponent } from './confirm-note.component';

describe('ConfirmNoteComponent', () => {
  let component: ConfirmNoteComponent;
  let fixture: ComponentFixture<ConfirmNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
