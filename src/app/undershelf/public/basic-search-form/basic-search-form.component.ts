import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SearchObjModel} from '../../searchObj.model';
import {UnderShelfService} from '../../under-shelf.service';
import {EventBusService} from '../../../services/event-bus.service';


@Component({
  selector: 'app-basic-search-form',
  templateUrl: './basic-search-form.component.html',
  styleUrls: ['./basic-search-form.component.scss']
})
export class BasicSearchFormComponent implements OnInit {
    @Output() searchObjEmitter = new EventEmitter();
    allStatus: Array<object>;
    searchObj: SearchObjModel;

    constructor(private underShelfService: UnderShelfService,
               ) {
    }

    ngOnInit() {
        this.searchObj = new SearchObjModel();
        this.getAllAccessStatus();
    }

    clearSearch = () => {
        this.searchObj.sid = '';
        this.searchObj.applicant = '';
        this.searchObj.status = '';
    };

    searchSchedule = () => {
        this.searchObjEmitter.emit(this.searchObj);
    };

    getAllAccessStatus = () => {
        this.underShelfService.getAllUndershelfStatus().subscribe(res => {
            this.allStatus = res;
            this.searchObj.status = res[0]['value'];
        });
    };


}
