import { Component, OnInit } from '@angular/core';
import {UndershelfObjModel} from '../../undershelfObj.model';

@Component({
  selector: 'app-basic-time-form',
  templateUrl: './basic-time-form.component.html',
  styleUrls: ['./basic-time-form.component.scss']
})
export class BasicTimeFormComponent implements OnInit {
    formObj: UndershelfObjModel;
    constructor() { }

    ngOnInit() {
        this.formObj = new UndershelfObjModel();
    }

}
