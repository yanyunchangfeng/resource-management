import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicTimeFormComponent } from './basic-time-form.component';

describe('BasicTimeFormComponent', () => {
  let component: BasicTimeFormComponent;
  let fixture: ComponentFixture<BasicTimeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicTimeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicTimeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
