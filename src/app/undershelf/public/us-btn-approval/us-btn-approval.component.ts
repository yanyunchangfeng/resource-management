import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-us-btn-approval',
  templateUrl: './us-btn-approval.component.html',
  styleUrls: ['./us-btn-approval.component.scss']
})
export class UsBtnApprovalComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate(['../approvalnote'], {queryParams: {sid: this.data.sid, status: this.data.status} , relativeTo: this.activatedRoute});
    }

}
