import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsBtnApprovalComponent } from './us-btn-approval.component';

describe('UsBtnApprovalComponent', () => {
  let component: UsBtnApprovalComponent;
  let fixture: ComponentFixture<UsBtnApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsBtnApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsBtnApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
