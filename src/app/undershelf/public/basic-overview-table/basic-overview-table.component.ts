import {Component, Input, OnInit} from '@angular/core';
import {SearchObjModel} from '../../searchObj.model';
import {UnderShelfService} from '../../under-shelf.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-basic-overview-table',
  templateUrl: './basic-overview-table.component.html',
  styleUrls: ['./basic-overview-table.component.scss']
})
export class BasicOverviewTableComponent implements OnInit {

    @Input() searchType;
    searchObj: SearchObjModel;
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数

    constructor(private underShelfService: UnderShelfService,
                private activatedRoute: ActivatedRoute,
                private router: Router) { }

    ngOnInit() {
        this.searchObj = new SearchObjModel();
        this.getQueryParam();
        this.getDatas();
    }

    getQueryParam = () => {
        this.activatedRoute.queryParams.subscribe(res => {
            (res['status']) && (this.searchObj.status = res['status']);
            (res['searchType']) && (this.searchType = res['searchType']);
        });
    };

    getDatas = () => {
        if (this.searchType === 'all') {
            // this.eventBusService.accessMineOverview.next('all');
            this.getAccessOverview(this.searchObj);
        }
        if (this.searchType === 'mine') {
            // this.eventBusService.accessMineOverview.next('mine');
            this.getMineAccessOverview(this.searchObj);
        }
    };

    getAccessOverview = (obj?) => {
        this.underShelfService.getAcessOverview(obj).subscribe(res => {
            if (res) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    };

    getMineAccessOverview = (obj?) => {
        this.underShelfService.getMineAcessOverview(obj).subscribe(res => {
            if (res) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    };

    resetPage = res => {
        if ( 'items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    };

    paginate = event => {
        this.searchObj.page_size = event.rows.toString();
        this.searchObj.page_number = (event.page + 1).toString();
        this.getDatas();
    };

    getInformChanged = event => {
        if (event === '00000') {
            this.getDatas();
        }
    }

    onOperate = data => {
        switch (data.status){
            case 'new':
            case 'approve':
                this.router.navigate(['../viewnewnote'], {queryParams: {sid: data.sid, status: data.status}, relativeTo: this.activatedRoute});
                break;
            case 'reject':
            case 'close_approve':
            case 'execute':
            case 'close_reject':
                this.router.navigate(['../viewunexecutivenote'], {queryParams: {sid: data.sid, status: data.status}, relativeTo: this.activatedRoute});
                break;
            case 'executing':
            case 'done':
                this.router.navigate(['../viewUnconfirmnote'], {queryParams:  {sid: data.sid, status: data.status}, relativeTo: this.activatedRoute});
        }
    };

}
