import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicOverviewTableComponent } from './basic-overview-table.component';

describe('BasicOverviewTableComponent', () => {
  let component: BasicOverviewTableComponent;
  let fixture: ComponentFixture<BasicOverviewTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicOverviewTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicOverviewTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
