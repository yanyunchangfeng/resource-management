import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SearchObjModel} from "../../searchObj.model";
import {EquipObjModel} from "../../equipObj.model";

@Component({
  selector: 'app-select-equipment',
  templateUrl: './select-equipment.component.html',
  styleUrls: ['./select-equipment.component.scss']
})
export class SelectEquipmentComponent implements OnInit {
    @Input() display: boolean;
    @Output() displayEmitter: EventEmitter<any> = new EventEmitter();
    allStatus: Array<object>;
    searchObj: SearchObjModel;
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数
    selectEquip ;
  totalRecords;
    width: string;
    equipObj: EquipObjModel;
    constructor() {
        this.allStatus = [
            {
                'label': '全部',
                'code': '全部'
            },
            {
                'label': '新建',
                'code': '新建'
            },
            {
                'label': '待审批',
                'code': '待审批'
            },
            {
                'label': '待下架',
                'code': '待下架'
            },
            {
                'label': '下架中',
                'code': '下架中'
            },
            {
                'label': '待确认',
                'code': '待确认'
            },
            {
                'label': '已完成',
                'code': '已完成'
            }
        ];
    }

    ngOnInit() {
        this.searchObj = new SearchObjModel();
        this.equipObj = new EquipObjModel();
        this.initWidth();

    }
    initWidth() {
        let windowInnerWidth = window.innerWidth;
        if(windowInnerWidth < 1024) {
            this.width = (windowInnerWidth * 0.7).toString();
        }else {
            this.width = (windowInnerWidth * 0.7).toString();
        }
    }
    clearSearch() {
        this.searchObj.sid = '';
        this.searchObj.applicant = '';
        this.searchObj.status = '';
    }
    cancel(){
       this.onHide();
    }
    sure() {
       this.onHide();
    }
    onHide() {
        this.display = false;
        this.displayEmitter.emit(this.display);
    }
    searchSchedule() {

    }


}
