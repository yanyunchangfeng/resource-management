import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-us-btn-edit',
  templateUrl: './us-btn-edit.component.html',
  styleUrls: ['./us-btn-edit.component.scss']
})
export class UsBtnEditComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate(['../createNote'], {queryParams: {sid: this.data.sid, status: this.data.status} , relativeTo: this.activatedRoute});
    }

}
