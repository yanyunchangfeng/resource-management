import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsBtnEditComponent } from './us-btn-edit.component';

describe('UsBtnEditComponent', () => {
  let component: UsBtnEditComponent;
  let fixture: ComponentFixture<UsBtnEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsBtnEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsBtnEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
