import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicSelectedTableComponent } from './basic-selected-table.component';

describe('BasicSelectedTableComponent', () => {
  let component: BasicSelectedTableComponent;
  let fixture: ComponentFixture<BasicSelectedTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicSelectedTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicSelectedTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
