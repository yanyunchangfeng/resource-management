import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EquipmentService} from '../../../equipment/equipment.service';
import {ConfirmationService} from 'primeng/components/common/confirmationservice';
import {MessageService} from 'primeng/components/common/messageservice';
import {BasePage} from '../../../base.page';

@Component({
  selector: 'app-basic-selected-table',
  templateUrl: './basic-selected-table.component.html',
  styleUrls: ['./basic-selected-table.component.scss']
})
export class BasicSelectedTableComponent extends BasePage implements OnInit {
    @Input() searchType;
    tableDatas: any[] = [];
    cols: any[] = [];
    selectedCars3: any = [];
    showOperator: boolean = false;

    showAddOrUpdateMask: boolean = false;
    tempAsset;
    state = 'update';
    dataSource;
    totalRecords: number;
    assets;

    showViewDetailMask:boolean = false;


    constructor(private activedRouter: ActivatedRoute,
                public messageService: MessageService,
                private equipmentService: EquipmentService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        // this.formObj = new FormobjModel();
        this.initCols();
        this.getQueryParams();
    }

    initCols = () => {
        this.cols = [
            { field: '设备编号', header: '设备编号' },
            { field: '设备名称', header: '设备名称' },
            { field: '设备分类', header: '设备分类' },
            { field: '型号', header: '型号' },
            { field: '在架/部署位置', header: '位置' },
            { field: '责任人', header: '责任人' },
            { field: '状态', header: '状态' },
            { field: '下架去向', header: '下架去向' }
        ];
    }

    tableDataEmitter = dataArray => {
        // this.unaccessTable.tableDatas = dataArray;
        // this.accessSelectEquip.searchObj.filter_asset_nos = this.filterUnaccessArrayOnlyId(this.unaccessTable.tableDatas); // 设备过滤
        // this.getSelectedAnoArray(dataArray);
    };

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            if (params['status'] === 'execute') {
                this.cols.push(  { field: '操作', header: '操作' });
                this.showOperator = true;
            }
        });
    };

    edit = data => {
        this.tempAsset = data;
        this.showAddOrUpdateMask = !this.showAddOrUpdateMask;
    }

    closeMask(bool){
        this.showAddOrUpdateMask = bool;
    }

    updateAsset(bool){
        this.queryAssets();
        this.showAddOrUpdateMask = bool;
    }

    queryAssets(){
        this.equipmentService.getAssets().subscribe(asset => {
            this.dataSource = asset;
            if (this.dataSource){
                this.totalRecords = this.dataSource.length;
                this.assets = this.dataSource.slice(0, 10);
            }else{
                this.totalRecords = 0;
                this.assets = [];
            }
        }, (err: Error) => {
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504){
                this.alert('似乎网络出现了问题，请联系管理员或稍后重试', 'error');
            }else{
                this.alert(err, 'error');
            }
        })
    }

    getAfterEditedEmitter = data => {
        this.findIndexOfVariation(this.tableDatas, data);
    };

    findIndexOfVariation = (array, data) => {
        const  index = array.findIndex(v => {
            return  v['ano'] = data['ano'];
        });
        if (index > -1) {
            for (let key in array[index]) {
                array[index][key] = data[key];
            }
            this.tableDatas.splice(index, 1, array[index]);
        }

    }

    closeViewDetail(bool){
        this.showViewDetailMask = bool;
    }

    viewOption(currentAsset){
        this.tempAsset= currentAsset;
        this.showViewDetailMask = !this.showViewDetailMask
    }
}
