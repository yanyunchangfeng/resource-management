import {Component, OnInit, ViewChild} from '@angular/core';
import {FormObjModel} from '../../formObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {UndershelfObjModel} from '../../undershelfObj.model';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-basic-reason-form',
  templateUrl: './basic-reason-form.component.html',
  styleUrls: ['./basic-reason-form.component.scss']
})
export class BasicReasonFormComponent implements OnInit {

    formObj: UndershelfObjModel;
    constructor(private router: Router,
                private eventBusService: EventBusService,
                private activedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.formObj = new UndershelfObjModel();
        this.getQueryParams();

    }

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['status']) && (this.eventBusService.underShelfFlowCharts.next(params['status']));
        });
    };


}
