import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicReasonFormComponent } from './basic-reason-form.component';

describe('BasicReasonFormComponent', () => {
  let component: BasicReasonFormComponent;
  let fixture: ComponentFixture<BasicReasonFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicReasonFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicReasonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
