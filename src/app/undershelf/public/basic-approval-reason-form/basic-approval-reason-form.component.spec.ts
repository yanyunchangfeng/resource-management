import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicApprovalReasonFormComponent } from './basic-approval-reason-form.component';

describe('BasicApprovalReasonFormComponent', () => {
  let component: BasicApprovalReasonFormComponent;
  let fixture: ComponentFixture<BasicApprovalReasonFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicApprovalReasonFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicApprovalReasonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
