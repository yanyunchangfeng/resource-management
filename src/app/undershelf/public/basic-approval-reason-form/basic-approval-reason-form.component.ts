import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormObjModel} from "../../formObj.model";
import {UndershelfObjModel} from '../../undershelfObj.model';

@Component({
  selector: 'app-basic-approval-reason-form',
  templateUrl: './basic-approval-reason-form.component.html',
  styleUrls: ['./basic-approval-reason-form.component.scss']
})
export class BasicApprovalReasonFormComponent implements OnInit {

    formObj: UndershelfObjModel;
    constructor(private router: Router,
                private activedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.formObj = new UndershelfObjModel();
    }

}
