import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-us-btn-under-shelf',
  templateUrl: './us-btn-under-shelf.component.html',
  styleUrls: ['./us-btn-under-shelf.component.scss']
})
export class UsBtnUnderShelfComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate(['../executivenote'], {queryParams: {sid: this.data.sid, status: this.data.status} , relativeTo: this.activatedRoute});
    }

}
