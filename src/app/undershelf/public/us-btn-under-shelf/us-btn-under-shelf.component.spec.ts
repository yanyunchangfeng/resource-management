import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsBtnUnderShelfComponent } from './us-btn-under-shelf.component';

describe('UsBtnUnderShelfComponent', () => {
  let component: UsBtnUnderShelfComponent;
  let fixture: ComponentFixture<UsBtnUnderShelfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsBtnUnderShelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsBtnUnderShelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
