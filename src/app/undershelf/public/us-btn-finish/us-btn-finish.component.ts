import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-us-btn-finish',
  templateUrl: './us-btn-finish.component.html',
  styleUrls: ['./us-btn-finish.component.scss']
})
export class UsBtnFinishComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate(['../confirmnote'], {queryParams: {sid: this.data.sid, status: this.data.status} , relativeTo: this.activatedRoute});
    }

}
