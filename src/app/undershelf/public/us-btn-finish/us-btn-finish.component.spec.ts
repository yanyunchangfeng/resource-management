import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsBtnFinishComponent } from './us-btn-finish.component';

describe('UsBtnFinishComponent', () => {
  let component: UsBtnFinishComponent;
  let fixture: ComponentFixture<UsBtnFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsBtnFinishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsBtnFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
