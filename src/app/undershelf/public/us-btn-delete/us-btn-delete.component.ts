import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {EventBusService} from '../../../services/event-bus.service';
import {UnderShelfService} from '../../under-shelf.service';
import {BasePage} from '../../../base.page';

@Component({
  selector: 'app-us-btn-delete',
  templateUrl: './us-btn-delete.component.html',
  styleUrls: ['./us-btn-delete.component.scss']
})
export class UsBtnDeleteComponent extends BasePage implements OnInit {
    @Input() public data: any;
    @Output() informTableChangedEmitter: EventEmitter<any> = new EventEmitter();
    sidArray: string[] = [];
    constructor(public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private underShelfService: UnderShelfService,
                private eventBusService: EventBusService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {

    }

    delete = data => {
        this.sidArray.length = 0;
        this.sidArray[this.sidArray.length] = data['sid'];
        this.eventBusService.underShelfFlowCharts.next(status);
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: () => {
                this.requestDelet();
                this.eventBusService.underShelfFlowCharts.next('');
            },
            reject: () => {
                this.eventBusService.underShelfFlowCharts.next('');
            }
        });
    };

    requestDelet = () => {
        this.underShelfService.requestDelete(this.sidArray).subscribe(res => {
            if (res === '00000') {
                this.alert('删除成功');
                this.informTableChangedEmitter.emit(res);
                // this.eventBusService.accessSearchOverview.next(this.searchObj);
            }else {
                this.alert('删除失败');
            }

        })
    };

}
