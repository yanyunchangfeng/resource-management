import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsBtnDeleteComponent } from './us-btn-delete.component';

describe('UsBtnDeleteComponent', () => {
  let component: UsBtnDeleteComponent;
  let fixture: ComponentFixture<UsBtnDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsBtnDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsBtnDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
