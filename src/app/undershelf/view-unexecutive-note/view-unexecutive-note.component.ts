import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BasicReasonFormComponent} from '../public/basic-reason-form/basic-reason-form.component';
import {BasicSelectedTableComponent} from '../public/basic-selected-table/basic-selected-table.component';
import {UndershelfObjModel} from '../undershelfObj.model';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {UnderShelfService} from '../under-shelf.service';
import {EventBusService} from '../../services/event-bus.service';
import {BasePage} from '../../base.page';
import {BasicApprovalReasonFormComponent} from '../public/basic-approval-reason-form/basic-approval-reason-form.component';

@Component({
  selector: 'app-view-unexecutive-note',
  templateUrl: './view-unexecutive-note.component.html',
  styleUrls: ['./view-unexecutive-note.component.scss']
})
export class ViewUnexecutiveNoteComponent extends BasePage  implements OnInit {
    @ViewChild('basicReasonForm') basicReasonForm: BasicReasonFormComponent;
    @ViewChild('basicSelectedTable') basicSelectedTable: BasicSelectedTableComponent;
    @ViewChild('basicApprovalReasonForm') basicApprovalReasonForm: BasicApprovalReasonFormComponent;
    formObj: UndershelfObjModel;
    constructor(private router: Router,
                public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private underShelfService: UnderShelfService,
                private eventBusService: EventBusService,
                private activedRouter: ActivatedRoute) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new UndershelfObjModel();
        this.getQueryParams();
    }

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.formObj.sid = params['sid']);
            this.requestMesageBySid(params['sid']);
        });
    };

    requestMesageBySid = id => {
        this.underShelfService.getMesageBySid(id).subscribe(res => {
            if (res['errcode'] === '00000') {
                this.formObj = new UndershelfObjModel(res['data']);
                this.basicReasonForm.formObj = new UndershelfObjModel(res['data']);
                this.basicApprovalReasonForm.formObj = new UndershelfObjModel(res['data']);
                (res['data']['devices']) && (this.basicSelectedTable.tableDatas = res['data']['devices']);
            }else {
                this.alert(`获取信息失败 + ${res['errmsg']}`, 'error');
            }
        });
    };

    goBack = () => {
        this.eventBusService.underShelfFlowCharts.next('');
        this.router.navigate(['../undershelfOverview'], {relativeTo: this.activedRouter});
    }
}
