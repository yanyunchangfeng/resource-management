import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUnexecutiveNoteComponent } from './view-unexecutive-note.component';

describe('ViewUnexecutiveNoteComponent', () => {
  let component: ViewUnexecutiveNoteComponent;
  let fixture: ComponentFixture<ViewUnexecutiveNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUnexecutiveNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUnexecutiveNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
