import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormObjModel} from '../formObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {PUblicMethod} from '../../services/PUblicMethod';
import {BasicReasonFormComponent} from '../public/basic-reason-form/basic-reason-form.component';
import {UndershelfObjModel} from '../undershelfObj.model';
import {UnderShelfService} from '../under-shelf.service';
import {BasicSelectedTableComponent} from '../public/basic-selected-table/basic-selected-table.component';
import {BasePage} from '../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-approval-note',
  templateUrl: './approval-note.component.html',
  styleUrls: ['./approval-note.component.scss']
})
export class ApprovalNoteComponent   extends BasePage implements OnInit {
    @ViewChild('basicReasonForm') basicReasonForm: BasicReasonFormComponent;
    @ViewChild('basicSelectedTable') basicSelectedTable: BasicSelectedTableComponent;
    controlDiaglog: boolean;
    myForm: FormGroup;
    formObj: UndershelfObjModel;
    approval: Array<object>;

    displayPersonel: boolean = false;              // 人员组织组件是否显示

    constructor(private fb: FormBuilder,
                private router: Router,
                public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private underShelfService: UnderShelfService,
                private eventBusService: EventBusService,
                private activedRouter: ActivatedRoute) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new UndershelfObjModel();
        this.initMyForm();
        this.getQueryParams();
    }

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.formObj.sid = params['sid']);
            this.requestMesageBySid(params['sid']);
            // (params['status']) && (this.eventBusService.variationFlowCharts.next(params['status']));
        });
    };

    requestMesageBySid = id => {
        this.underShelfService.getMesageBySid(id).subscribe(res => {
            if (res['errcode'] === '00000') {
                this.formObj = new UndershelfObjModel(res['data']);
                this.basicReasonForm.formObj = new UndershelfObjModel(res['data']);
                // this.basicSelectedTable.tableDatas = res['data']['devices'];
                (res['data']['devices']) && (this.basicSelectedTable.tableDatas = res['data']['devices']);
                // (res['data']['approver_pid']) && this.filterSelectedApporve(this.allApprovals, res['data']['approver_pid']);
                // (res['data']['devices'] && res['data']['devices']['length'] > 0) && this.getSelectedAnoArray(res['data']['devices']);
            }else {
                this.alert(`获取出入单信息失败 + ${res['errmsg']}`, 'error');
            }
        });
    };

    initMyForm() {
        this.myForm = this.fb.group({
            executor_org: [null, Validators.required],
            executor: [null, Validators.required],
            approve_remarks: [null, Validators.required]
        });
    }
    goBack = (): void => {
        this.eventBusService.underShelfFlowCharts.next('');
        this.router.navigate(['../undershelfOverview'], {relativeTo: this.activedRouter});
    };

    cancel(){
        this.onHide();
    }
    sure() {
        this.onHide();
    }
    onHide(): void {
        this.controlDiaglog = false;
    }
    showTreeDialog(): void {
        this.controlDiaglog = true;
    }
    clearTreeDialog(): void {

    }
    overrule() {

    }
    reject() {

    }
    pass() {

    }

    showPersonDialog = () => {this.displayPersonel = true; };

    clearPersonDialog = () => {
        this.formObj.executor = '';
        this.formObj.executor_pid = '';
        this.formObj.executor_org_oid = '';
        this.formObj.executor_org = '';
    };

    dataEmitter = event => {
        this.formObj.executor = event.name;
        this.formObj.executor_pid = event.pid;
        this.formObj.executor_org = event.organization;
        this.formObj.executor_org_oid = event.oid;
    };

    displayEmitter = event => { this.displayPersonel = event; };

    isFieldValid = (name) => {
        return PUblicMethod.isFieldValid(this.myForm, name);
    };

    operate = result => {
        this.formObj.status = result;
        if (!this.formObj.executor || !this.formObj.executor_org) {
            PUblicMethod.validateAllFormFields(this.myForm);
        } else {
            this.requestApprove();
        }
    };

    requestApprove = () =>  {
        this.underShelfService.requestApprove(this.formObj).subscribe(res => {
            if (res === '00000') {
                this.alert('操作成功');
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            } else {
                this.alert(`操作失败${res}`, 'error');
            }
        });
    }

}
