import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalNoteComponent } from './approval-note.component';

describe('ApprovalNoteComponent', () => {
  let component: ApprovalNoteComponent;
  let fixture: ComponentFixture<ApprovalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
