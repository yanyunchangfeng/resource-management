import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnconfirmOverviewComponent } from './unconfirm-overview.component';

describe('UnconfirmOverviewComponent', () => {
  let component: UnconfirmOverviewComponent;
  let fixture: ComponentFixture<UnconfirmOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnconfirmOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnconfirmOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
