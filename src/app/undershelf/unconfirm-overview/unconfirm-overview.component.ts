import {Component, OnInit, ViewChild} from '@angular/core';
import {BasicOverviewTableComponent} from '../public/basic-overview-table/basic-overview-table.component';

@Component({
  selector: 'app-unconfirm-overview',
  templateUrl: './unconfirm-overview.component.html',
  styleUrls: ['./unconfirm-overview.component.scss']
})
export class UnconfirmOverviewComponent implements OnInit {
    @ViewChild('basicOverViewTable') basicOverViewTable: BasicOverviewTableComponent;
    constructor() { }

    ngOnInit() {
    }

    getSearchObjEmitter = seachObj => {
        this.basicOverViewTable.searchObj = seachObj;
        this.basicOverViewTable.getDatas();
    }

}
