export class UndershelfObjModel {
    sid: string;
    status: string;
    status_name: string;
    plantime_begin: string;
    plantime_end: string;
    unshelve_type: string;
    unshelve_reason: string;
    acttime_begin: string;
    acttime_end: string;

    creator: string;
    creator_pid: string;
    creator_org: string;
    creator_org_oid: string;
    create_time: string;

    applicant: string;
    applicant_pid: string;
    applicant_org: string;
    applicant_org_oid: string;

    approve: string;
    approve_pid: string;
    approve_org: string;
    approve_org_pid: string;
    approve_time: string;
    approve_remarks: string;

    executor: string;
    executor_pid: string;
    executor_org: string;
    executor_org_oid: string;
    executor_time: string;

    owner: string;
    owner_pid: string;

    begin_time: string;
    end_time: string;
    devices: Array<any>;
    operations: Array<any>;


    constructor(obj?: any) {
        this.sid = obj && obj['sid'] || '';
        this.status = obj && obj['status'] || '';
        this.status_name = obj && obj['status_name'] || '';
        this.plantime_begin = obj && obj['plantime_begin'] || '';
        this.plantime_end = obj && obj['plantime_end'] || '';
        this.unshelve_type = obj && obj['unshelve_type'] || '';
        this.unshelve_reason = obj && obj['unshelve_reason'] || '';
        this.acttime_begin = obj && obj['acttime_begin'] || '';
        this.acttime_end = obj && obj['acttime_end'] || '';

        this.creator = obj && obj['creator'] || '';
        this.creator_pid = obj && obj['creator_pid'] || '';
        this.creator_org = obj && obj['creator_org'] || '';
        this.creator_org_oid = obj && obj['creator_org_oid'] || '';
        this.create_time = obj && obj['create_time'] || '';

        this.applicant = obj && obj['applicant'] || '';
        this.applicant_pid = obj && obj['applicant_pid'] || '';
        this.applicant_org = obj && obj['applicant_org'] || '';
        this.applicant_org_oid = obj && obj['applicant_org_oid'] || '';

        this.approve = obj && obj['approve'] || '';
        this.approve_pid = obj && obj['approve_pid'] || '';
        this.approve_org = obj && obj['approve_org'] || '';
        this.approve_org_pid = obj && obj['approve_org_pid'] || '';
        this.approve_time = obj && obj['approve_time'] || '';
        this.approve_remarks = obj && obj['approve_remarks'] || '';

        this.executor = obj && obj['executor'] || '';
        this.executor_pid = obj && obj['executor_pid'] || '';
        this.executor_org = obj && obj['executor_org'] || '';
        this.executor_org_oid = obj && obj['executor_org_oid'] || '';
        this.executor_time = obj && obj['executor_time'] || '';

        this.owner = obj && obj['owner'] || '';
        this.owner_pid = obj && obj['owner_pid'] || '';

        this.begin_time = obj && obj['begin_time'] || '';
        this.end_time = obj && obj['end_time'] || '';
        this.devices = obj && obj['devices'] || [];
        this.operations = obj && obj['operations'] || [];
    }


}
