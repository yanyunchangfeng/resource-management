export class EquipObjModel {
    id: string;
    name: string;
    position: string;
    constructor(obj?: any) {
        this.id = obj && obj.id || '';
        this.name = obj && obj.name || '';
        this.position = obj && obj.position || '';
    }
}