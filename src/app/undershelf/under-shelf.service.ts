import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../services/storage.service';

@Injectable()
export class UnderShelfService {
    ip = environment.url.management;
    constructor(private http: HttpClient,
                private storageService: StorageService) { }

    //    流程图接口获取
    getFlowChart(status?) {
        status = status ? status : '';
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_workflow_get',
                'data': {
                    'status': status
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

    //  请求单保存
    requestSave(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_save',
                'data': {
                    applicant: obj['applicant'],
                    applicant_pid: obj['applicant_pid'],
                    applicant_org: obj['applicant_org'],
                    applicant_org_oid: obj['applicant_org_oid'],
                    approve: obj['approve'],
                    approve_pid: obj['approve_pid'],
                    plantime_begin: obj['plantime_begin'],
                    plantime_end: obj['plantime_end'],
                    unshelve_type: obj['unshelve_type'],
                    unshelve_reason: obj['unshelve_reason'],
                    devices: obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单编辑后保存
    requestSaveAfterEdited(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_mod_save',
                'data': {
                    sid: obj['sid'],
                    applicant: obj['applicant'],
                    applicant_pid: obj['applicant_pid'],
                    applicant_org: obj['applicant_org'],
                    applicant_org_oid: obj['applicant_org_oid'],
                    approve: obj['approve'],
                    approve_pid: obj['approve_pid'],
                    plantime_begin: obj['plantime_begin'],
                    plantime_end: obj['plantime_end'],
                    unshelve_type: obj['unshelve_type'],
                    unshelve_reason: obj['unshelve_reason'],
                    devices: obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单提交
    requestSubmite(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_add',
                'data': {
                    applicant: obj['applicant'],
                    applicant_pid: obj['applicant_pid'],
                    applicant_org: obj['applicant_org'],
                    applicant_org_oid: obj['applicant_org_oid'],
                    approve: obj['approve'],
                    approve_pid: obj['approve_pid'],
                    plantime_begin: obj['plantime_begin'],
                    plantime_end: obj['plantime_end'],
                    unshelve_type: obj['unshelve_type'],
                    unshelve_reason: obj['unshelve_reason'],
                    devices: obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单修改提交
    requestSubmitAfterEdit(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_mod_submit',
                'data': {
                    sid: obj['sid'],
                    applicant: obj['applicant'],
                    applicant_pid: obj['applicant_pid'],
                    applicant_org: obj['applicant_org'],
                    applicant_org_oid: obj['applicant_org_oid'],
                    approve: obj['approve'],
                    approve_pid: obj['approve_pid'],
                    plantime_begin: obj['plantime_begin'],
                    plantime_end: obj['plantime_end'],
                    unshelve_type: obj['unshelve_type'],
                    unshelve_reason: obj['unshelve_reason'],
                    devices: obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  获取、查询出入管理总览
    getAcessOverview(obj?: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_get',
                'data': {
                    'condition': {
                        sid: obj['sid'] ,
                        applicant: obj['applicant'] ,
                        begin_time: obj['begin_time'],
                        end_time: obj['end_time'] ,
                        status: obj['status']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }

            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

 //  获取带我处理出入管理
    getMineAcessOverview(obj?: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_get_byowner',
                'data': {
                    'condition': {
                        sid: obj['sid'] ,
                        applicant: obj['applicant'] ,
                        begin_time: obj['begin_time'],
                        end_time: obj['end_time'] ,
                        status: obj['status']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }


            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    // 请求单刪除
    requestDelete(ids: Array<string>) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_del',
                'ids': ids
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单信息获取
    getMesageBySid(trickSid: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_get_byid',
                'id': trickSid
            }
        ).map((res: Response) => {
            return res;
        });
    }

    // 请求单审批
    requestApprove(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_approve',
                'data': {
                    sid: obj['sid'],
                    approve_remarks: obj['approve_remarks'],
                    status: obj['status'],
                    executor: obj['executor'],
                    executor_pid: obj['executor_pid'],
                    executor_org: obj['executor_org'],
                    executor_org_oid: obj['executor_org_oid']
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  执行中的申请单进行【设备下架】| 【保存】
    requestUndershelfOrSave(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_process',
                'data': {
                    sid: obj['sid'],
                    status: obj['status'],
                    devices: obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 完成变动
    requestFinish(sid: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_finish',
                'data': {
                    sid: sid
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  所有状态获取接口
    getAllUndershelfStatus() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'assetunshelve_statuslist_get'
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

}
