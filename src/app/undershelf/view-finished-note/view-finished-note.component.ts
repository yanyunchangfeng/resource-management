import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-view-finished-note',
  templateUrl: './view-finished-note.component.html',
  styleUrls: ['./view-finished-note.component.scss']
})
export class ViewFinishedNoteComponent implements OnInit {
    constructor(private router: Router,
                private activedRouter: ActivatedRoute) { }

    ngOnInit() {

    }
    goBack() {
        this.router.navigate(['../undershelfOverview'], {relativeTo: this.activedRouter});
    }

}
