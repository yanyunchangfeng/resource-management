import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFinishedNoteComponent } from './view-finished-note.component';

describe('ViewFinishedNoteComponent', () => {
  let component: ViewFinishedNoteComponent;
  let fixture: ComponentFixture<ViewFinishedNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFinishedNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFinishedNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
