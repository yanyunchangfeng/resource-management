import {Component, OnInit, ViewChild} from '@angular/core';
import {BasicOverviewTableComponent} from '../public/basic-overview-table/basic-overview-table.component';


@Component({
  selector: 'app-finished-overview',
  templateUrl: './finished-overview.component.html',
  styleUrls: ['./finished-overview.component.scss']
})
export class FinishedOverviewComponent implements OnInit {
    @ViewChild('basicOverViewTable') basicOverViewTable: BasicOverviewTableComponent;
    constructor() { }

    ngOnInit() {
    }

    getSearchObjEmitter = seachObj => {
        this.basicOverViewTable.searchObj = seachObj;
        this.basicOverViewTable.getDatas();
    }
}
