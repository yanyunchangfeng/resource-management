import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnexecutiveOverviewComponent } from './unexecutive-overview.component';

describe('UnexecutiveOverviewComponent', () => {
  let component: UnexecutiveOverviewComponent;
  let fixture: ComponentFixture<UnexecutiveOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnexecutiveOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnexecutiveOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
