import {Component, OnInit, ViewChild} from '@angular/core';
import {BasicOverviewTableComponent} from '../public/basic-overview-table/basic-overview-table.component';

@Component({
  selector: 'app-unexecutive-overview',
  templateUrl: './unexecutive-overview.component.html',
  styleUrls: ['./unexecutive-overview.component.scss']
})
export class UnexecutiveOverviewComponent implements OnInit {
    @ViewChild('basicOverViewTable') basicOverViewTable: BasicOverviewTableComponent;
    constructor() { }

    ngOnInit() {
    }

    getSearchObjEmitter = seachObj => {
        this.basicOverViewTable.searchObj = seachObj;
        this.basicOverViewTable.getDatas();
    }

}
