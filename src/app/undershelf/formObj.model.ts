export class FormObjModel {
    sid: string;
    status: string;
    applicant: string;
    applicant_org: string;
    type: string;
    approver: string;
    plan_under_time: string;
    plan_finished_time: string;
    under_reason: string;
    executive: string;
    executive_org: string;
    executive_start_time: string;
    executive_end_time: string;

    constructor(obj?: any) {
        this.sid = obj && obj.sid || '';
        this.status = obj && obj.status || '';
        this.applicant = obj && obj.applicant || '';
        this.applicant_org = obj && obj.applicant_org || '';
        this.type = obj && obj.type || '';
        this.approver = obj && obj.approver || '';
        this.plan_under_time = obj && obj.plan_under_time || '';
        this.plan_finished_time = obj && obj.plan_finished_time || '';
        this.under_reason = obj && obj.under_reason || '';
        this.executive = obj && obj.executive || '';
        this.executive_org = obj && obj.executive_org || '';
        this.executive_start_time = obj && obj.executive_start_time || '';
        this.executive_end_time = obj && obj.executive_end_time || '';
    }
}