import {Component, OnInit, ViewChild} from '@angular/core';
import {EventBusService} from '../../services/event-bus.service';
import {UnderShelfService} from '../under-shelf.service';
import {BasicOverviewTableComponent} from '../public/basic-overview-table/basic-overview-table.component';

@Component({
  selector: 'app-under-shelf-mine-overview',
  templateUrl: './under-shelf-mine-overview.component.html',
  styleUrls: ['./under-shelf-mine-overview.component.scss']
})
export class UnderShelfMineOverviewComponent implements OnInit {
    @ViewChild('basicOverViewTable') basicOverViewTable: BasicOverviewTableComponent;
    searchType: string = 'mine';
    constructor( private eventBusService: EventBusService) { }

    ngOnInit() {
        this.eventBusService.underShelfOverview.next(this.searchType);
    }
    getSearchObjEmitter = seachObj => {
        this.basicOverViewTable.searchObj = seachObj;
        this.basicOverViewTable.getDatas();
    }

}
