import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnderShelfMineOverviewComponent } from './under-shelf-mine-overview.component';

describe('UnderShelfMineOverviewComponent', () => {
  let component: UnderShelfMineOverviewComponent;
  let fixture: ComponentFixture<UnderShelfMineOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnderShelfMineOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnderShelfMineOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
