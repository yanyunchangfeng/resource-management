import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UsBasicComponent} from './us-basic/us-basic.component';
import {CreateNoteComponent} from './create-note/create-note.component';
import {UnapprovalOverviewComponent} from './unapproval-overview/unapproval-overview.component';
import {UnexecutiveOverviewComponent} from './unexecutive-overview/unexecutive-overview.component';
import {UnconfirmOverviewComponent} from './unconfirm-overview/unconfirm-overview.component';
import {FinishedOverviewComponent} from './finished-overview/finished-overview.component';
import {UnderShelfOverviewComponent} from './under-shelf-overview/under-shelf-overview.component';
import {ViewNewNoteComponent} from './view-new-note/view-new-note.component';
import {ApprovalNoteComponent} from './approval-note/approval-note.component';
import {ExecutiveNoteComponent} from './executive-note/executive-note.component';
import {ViewUnexecutiveNoteComponent} from './view-unexecutive-note/view-unexecutive-note.component';
import {ConfirmNoteComponent} from './confirm-note/confirm-note.component';
import {ViewUnconfirmNoteComponent} from './view-unconfirm-note/view-unconfirm-note.component';
import {ViewFinishedNoteComponent} from './view-finished-note/view-finished-note.component';
import {UnderShelfMineOverviewComponent} from './under-shelf-mine-overview/under-shelf-mine-overview.component';

const routes: Routes = [
    {path: '', component: UsBasicComponent},
    {path: 'usBasic', component: UsBasicComponent, children: [
        {path: 'undershelfOverview', component: UnderShelfOverviewComponent},
        {path: 'undershelfmineOverview', component: UnderShelfMineOverviewComponent},
        {path: 'unApprovalOverview', component: UnapprovalOverviewComponent},
        {path: 'unExecutiveOverview', component: UnexecutiveOverviewComponent},
        {path: 'unConfirmOverview', component: UnconfirmOverviewComponent},
        {path: 'finishedOverview', component: FinishedOverviewComponent},
        {path: 'createNote', component: CreateNoteComponent},
        {path: 'approvalnote', component: ApprovalNoteComponent},
        {path: 'executivenote', component: ExecutiveNoteComponent},
        {path: 'confirmnote', component: ConfirmNoteComponent},
        {path: 'viewnewnote', component: ViewNewNoteComponent},
        {path: 'viewunexecutivenote', component: ViewUnexecutiveNoteComponent},
        {path: 'viewUnconfirmnote', component: ViewUnconfirmNoteComponent},
        {path: 'viewfinishednote', component: ViewFinishedNoteComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnderShelfRoutingModule { }
