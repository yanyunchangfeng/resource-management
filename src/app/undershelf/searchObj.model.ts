export class SearchObjModel {
    sid: string;
    applicant: string;
    status: string;
    page_size: string;
    page_number: string;

    constructor(obj?: any) {
        this.status = obj && obj.status || '';
        this.sid = obj && obj.sid || '';
        this.applicant = obj && obj.applicant || '';
        this.page_size = obj && obj.page_size || '10';
        this.page_number = obj && obj.page_number || '1';
    }
}
