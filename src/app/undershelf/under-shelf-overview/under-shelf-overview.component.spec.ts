import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnderShelfOverviewComponent } from './under-shelf-overview.component';

describe('UnderShelfOverviewComponent', () => {
  let component: UnderShelfOverviewComponent;
  let fixture: ComponentFixture<UnderShelfOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnderShelfOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnderShelfOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
