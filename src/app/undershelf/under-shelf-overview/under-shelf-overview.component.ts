import {Component, OnInit, ViewChild} from '@angular/core';
import {EventBusService} from '../../services/event-bus.service';
import {UnderShelfService} from '../under-shelf.service';
import {BasicOverviewTableComponent} from '../public/basic-overview-table/basic-overview-table.component';

@Component({
  selector: 'app-under-shelf-overview',
  templateUrl: './under-shelf-overview.component.html',
  styleUrls: ['./under-shelf-overview.component.scss']
})
export class UnderShelfOverviewComponent implements OnInit {
    @ViewChild('basicOverViewTable') basicOverViewTable: BasicOverviewTableComponent;
    searchType: string = 'all';
    constructor( private eventBusService: EventBusService) { }

    ngOnInit() {
        this.eventBusService.underShelfOverview.next(this.searchType);
    }

    getSearchObjEmitter = seachObj => {
        this.basicOverViewTable.searchObj = seachObj;
        this.basicOverViewTable.getDatas();
    }
}
