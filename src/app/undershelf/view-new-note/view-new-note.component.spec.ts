import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewNewNoteComponent } from './view-new-note.component';

describe('ViewNewNoteComponent', () => {
  let component: ViewNewNoteComponent;
  let fixture: ComponentFixture<ViewNewNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewNewNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewNewNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
