import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UndershelfObjModel} from '../undershelfObj.model';
import {BasicReasonFormComponent} from '../public/basic-reason-form/basic-reason-form.component';
import {UnderShelfService} from '../under-shelf.service';
import {BasicSelectedTableComponent} from '../public/basic-selected-table/basic-selected-table.component';
import {BasePage} from '../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-view-new-note',
  templateUrl: './view-new-note.component.html',
  styleUrls: ['./view-new-note.component.scss']
})
export class ViewNewNoteComponent extends BasePage  implements OnInit {
    @ViewChild('basicReasonForm') basicReasonForm: BasicReasonFormComponent;
    @ViewChild('basicSelectedTable') basicSelectedTable: BasicSelectedTableComponent;
    formObj: UndershelfObjModel;
    constructor(private router: Router,
                public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private underShelfService: UnderShelfService,
                private eventBusService: EventBusService,
                private activedRouter: ActivatedRoute) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new UndershelfObjModel();
        this.getQueryParams();
    }

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.formObj.sid = params['sid']);
            this.requestMesageBySid(params['sid']);
        });
    };

    requestMesageBySid = id => {
        this.underShelfService.getMesageBySid(id).subscribe(res => {
            if (res['errcode'] === '00000') {
                this.formObj = new UndershelfObjModel(res['data']);
                this.basicReasonForm.formObj = new UndershelfObjModel(res['data']);
                (res['data']['devices']) && (this.basicSelectedTable.tableDatas = res['data']['devices']);
            }else {
                this.alert(`获取信息失败 + ${res['errmsg']}`, 'error');
            }
        });
    };

    goBack = () => {
        this.eventBusService.underShelfFlowCharts.next('');
        this.router.navigate(['../undershelfOverview'], {relativeTo: this.activedRouter});
    }
}
