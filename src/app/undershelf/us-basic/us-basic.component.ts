import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UnderShelfService} from "../under-shelf.service";
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-us-basic',
  templateUrl: './us-basic.component.html',
  styleUrls: ['./us-basic.component.scss']
})
export class UsBasicComponent implements OnInit {
    flowChartDatas: Array<object> = [];
    searchType: string = '';

    constructor(private router: Router,
                private activedRouter: ActivatedRoute,
                private underShelfService: UnderShelfService,
                private eventBusService: EventBusService,
                ) { }

    ngOnInit() {
        this.getFlowChart();

        this.eventBusService.underShelfOverview.subscribe(searchType => {
            this.searchType = searchType;
        });

        this.eventBusService.underShelfFlowCharts.subscribe(status => {
          this.getFlowChart(status);
        });
    }

    getFlowChart = (status?: string) => {
        this.underShelfService.getFlowChart(status).subscribe(res => {
            this.flowChartDatas = res;
        });
    };

    jumper(num) {
        switch (num.toString()){
            case '1' :
                this.router.navigate(['./createNote'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '2':
                this.router.navigate(['./unApprovalOverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '3':
                this.router.navigate(['./unExecutiveOverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '5':
                this.router.navigate(['./unConfirmOverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '7':
                this.router.navigate(['./finishedOverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
        }

    }

}
