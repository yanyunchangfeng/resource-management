import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsBasicComponent } from './us-basic.component';

describe('UsBasicComponent', () => {
  let component: UsBasicComponent;
  let fixture: ComponentFixture<UsBasicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsBasicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
