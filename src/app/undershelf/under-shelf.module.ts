import { NgModule } from '@angular/core';

import { UnderShelfRoutingModule } from './under-shelf-routing.module';
import {ShareModule} from '../shared/share.module';
import {PublicService} from '../services/public.service';
import {UnderShelfService} from './under-shelf.service';
import { CreateNoteComponent } from './create-note/create-note.component';
import { ViewNewNoteComponent } from './view-new-note/view-new-note.component';
import { ApprovalNoteComponent } from './approval-note/approval-note.component';
import { ViewUnapprovalNoteComponent } from './view-unapproval-note/view-unapproval-note.component';
import { ExecutiveNoteComponent } from './executive-note/executive-note.component';
import { ViewUnexecutiveNoteComponent } from './view-unexecutive-note/view-unexecutive-note.component';
import { ConfirmNoteComponent } from './confirm-note/confirm-note.component';
import { ViewUnconfirmNoteComponent } from './view-unconfirm-note/view-unconfirm-note.component';
import { ViewFinishedNoteComponent } from './view-finished-note/view-finished-note.component';
import { BasicReasonFormComponent } from './public/basic-reason-form/basic-reason-form.component';
import { BasicApprovalReasonFormComponent } from './public/basic-approval-reason-form/basic-approval-reason-form.component';
import { BasicTimeFormComponent } from './public/basic-time-form/basic-time-form.component';
import { BasicOverviewTableComponent } from './public/basic-overview-table/basic-overview-table.component';
import { BasicSelectedTableComponent } from './public/basic-selected-table/basic-selected-table.component';
import { BasicSearchFormComponent } from './public/basic-search-form/basic-search-form.component';
import { UsBtnEditComponent } from './public/us-btn-edit/us-btn-edit.component';
import { UsBtnDeleteComponent } from './public/us-btn-delete/us-btn-delete.component';
import { UsBtnApprovalComponent } from './public/us-btn-approval/us-btn-approval.component';
import { UsBtnUnderShelfComponent } from './public/us-btn-under-shelf/us-btn-under-shelf.component';
import { UsBasicComponent } from './us-basic/us-basic.component';
import { UsFlowChartComponent } from './us-flow-chart/us-flow-chart.component';
import { UnderShelfOverviewComponent } from './under-shelf-overview/under-shelf-overview.component';
import { UnapprovalOverviewComponent } from './unapproval-overview/unapproval-overview.component';
import { UnexecutiveOverviewComponent } from './unexecutive-overview/unexecutive-overview.component';
import { UnconfirmOverviewComponent } from './unconfirm-overview/unconfirm-overview.component';
import { FinishedOverviewComponent } from './finished-overview/finished-overview.component';
import { SelectEquipmentComponent } from './public/select-equipment/select-equipment.component';
import { UnderShelfMineOverviewComponent } from './under-shelf-mine-overview/under-shelf-mine-overview.component';
import {PublicModule} from '../public/public.module';
import { UsBtnFinishComponent } from './public/us-btn-finish/us-btn-finish.component';


@NgModule({
    imports: [
        ShareModule,
        PublicModule,
        UnderShelfRoutingModule
    ],
    declarations: [
        CreateNoteComponent,
        ViewNewNoteComponent,
        ApprovalNoteComponent,
        ViewUnapprovalNoteComponent,
        ExecutiveNoteComponent,
        ViewUnexecutiveNoteComponent,
        ConfirmNoteComponent,
        ViewUnconfirmNoteComponent,
        ViewFinishedNoteComponent,
        BasicReasonFormComponent,
        BasicApprovalReasonFormComponent,
        BasicTimeFormComponent,
        BasicOverviewTableComponent,
        BasicSelectedTableComponent,
        BasicSearchFormComponent,
        UsBtnEditComponent,
        UsBtnDeleteComponent,
        UsBtnApprovalComponent,
        UsBtnUnderShelfComponent,
        UsBasicComponent,
        UsFlowChartComponent,
        UnderShelfOverviewComponent,
        UnapprovalOverviewComponent,
        UnexecutiveOverviewComponent,
        UnconfirmOverviewComponent,
        FinishedOverviewComponent,
        SelectEquipmentComponent,
        UnderShelfMineOverviewComponent,
        UsBtnFinishComponent
    ],
    providers: [
        PublicService,
        UnderShelfService
    ]
})
export class UnderShelfModule { }
