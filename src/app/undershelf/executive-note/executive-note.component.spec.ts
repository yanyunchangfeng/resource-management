import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutiveNoteComponent } from './executive-note.component';

describe('ExecutiveNoteComponent', () => {
  let component: ExecutiveNoteComponent;
  let fixture: ComponentFixture<ExecutiveNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutiveNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutiveNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
