import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BasicReasonFormComponent} from '../public/basic-reason-form/basic-reason-form.component';
import {BasicSelectedTableComponent} from '../public/basic-selected-table/basic-selected-table.component';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {UnderShelfService} from '../under-shelf.service';
import {BasePage} from '../../base.page';
import {UndershelfObjModel} from '../undershelfObj.model';
import {BasicApprovalReasonFormComponent} from '../public/basic-approval-reason-form/basic-approval-reason-form.component';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-executive-note',
  templateUrl: './executive-note.component.html',
  styleUrls: ['./executive-note.component.scss']
})
export class ExecutiveNoteComponent  extends BasePage  implements OnInit {

    @ViewChild('basicReasonForm') basicReasonForm: BasicReasonFormComponent;
    @ViewChild('basicSelectedTable') basicSelectedTable: BasicSelectedTableComponent;
    @ViewChild('basicApproveReasonForm') basicApproveReasonForm: BasicApprovalReasonFormComponent;
    formObj: UndershelfObjModel;

    constructor(private router: Router,
                public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private underShelfService: UnderShelfService,
                private eventBusService: EventBusService,
                private activedRouter: ActivatedRoute) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new UndershelfObjModel();
        this.getQueryParams();
    }

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.formObj.sid = params['sid']);
            this.requestMesageBySid(params['sid']);
        });
    };

    requestMesageBySid = id => {
        this.underShelfService.getMesageBySid(id).subscribe(res => {
            if (res['errcode'] === '00000') {
                this.formObj = new UndershelfObjModel(res['data']);
                this.basicReasonForm.formObj = new UndershelfObjModel(res['data']);
                this.basicApproveReasonForm.formObj = new UndershelfObjModel(res['data']);
                (res['data']['devices']) && (this.basicSelectedTable.tableDatas = res['data']['devices']);
            }else {
                this.alert(`获取出入单信息失败 + ${res['errmsg']}`, 'error');
            }
        });
    };

    goBack = () => {
        this.eventBusService.underShelfFlowCharts.next('');
        this.router.navigate(['../undershelfOverview'], {relativeTo: this.activedRouter});
    };

    volumeOffShelf = status => {
        this.formObj.status = status;
        this.formObj.devices = this.basicSelectedTable.tableDatas;
        this.requestUndershelfOrSave();
    };

    requestUndershelfOrSave = () => {
        this.underShelfService.requestUndershelfOrSave(this.formObj).subscribe(res => {
            if ( res === '00000' ) {
                this.alert('操作成功');
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            }else {
                this.alert(`操作失败 + ${res}`, 'error');
            }
        });
    };
}
