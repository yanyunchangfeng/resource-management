import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsFlowChartComponent } from './us-flow-chart.component';

describe('UsFlowChartComponent', () => {
  let component: UsFlowChartComponent;
  let fixture: ComponentFixture<UsFlowChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsFlowChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsFlowChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
