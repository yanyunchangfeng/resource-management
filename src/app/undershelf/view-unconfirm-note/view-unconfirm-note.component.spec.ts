import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUnconfirmNoteComponent } from './view-unconfirm-note.component';

describe('ViewUnconfirmNoteComponent', () => {
  let component: ViewUnconfirmNoteComponent;
  let fixture: ComponentFixture<ViewUnconfirmNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUnconfirmNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUnconfirmNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
