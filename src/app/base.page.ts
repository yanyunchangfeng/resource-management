import {ConfirmationService} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';

export class BasePage{
  constructor(
    public confirmationService: ConfirmationService,
    public messageService: MessageService
  ){

  }
  confirm(msg, cb?){
    this.confirmationService.confirm({
      message: msg,
      rejectVisible: true,
      accept: () => {
        if (cb && typeof cb === 'function'){
          cb();
        }
      },
      reject: () => {

      }
    });
  }
  alert(msg, type= 'success'){
    this.messageService.clear();
    this.messageService.add({severity: type, summary: '提示消息', detail: msg});
  }
  handleError(err){
    if (err['ok'] === false){
      this.alert('似乎网络出现了问题,请联系管理员或稍后重试');
      return;
    }
    this.alert(err['message']);
  }
  toastError(err){
    if(err['ok'] === false){
      this.toast('似乎网络出现了问题,请联系管理员或稍后重试');
      return
    }
    this.toast(err['message']);
  }
  toast(msg){
    this.confirmationService.confirm({
      message: msg,
      rejectVisible:false,
      accept: () => {

      },
    })
  }
}
