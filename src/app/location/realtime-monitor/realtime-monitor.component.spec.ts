import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtimeMonitorComponent } from './realtime-monitor.component';

describe('RealtimeMonitorComponent', () => {
  let component: RealtimeMonitorComponent;
  let fixture: ComponentFixture<RealtimeMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtimeMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtimeMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
