import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SelectItem} from "primeng/primeng";

@Component({
  selector: 'app-visitor-edit',
  templateUrl: './visitor-edit.component.html',
  styleUrls: ['./visitor-edit.component.css']
})
export class VisitorEditComponent implements OnInit {
  display;
  title;
  visitor: FormGroup;
  @Output() closeVisitorEdit = new EventEmitter();
  @Input() currentVisitor;
  @Input() state;
  ch;
  maxDate;
  sexs: SelectItem[];

  constructor(
    private fb: FormBuilder,
    ) {
    this.visitor = fb.group({
      name: [''],
      sex: [''],
      nation: [''],
      borthday: [''],
      homeAddress: [''],
      idNumber: [''],
      signOrg: [''],
      remark: [''],
    });
    this.sexs = [
      {label:'', value:null},
      {label:'男', value:'男'},
      {label:'女', value:'女'},
    ];
  }

  ngOnInit() {
    // console.log(this.currentVisitor)
    this.display = true;
    if(this.state == "add"){
      this.title = "添加访客";
    }else {
      this.title = "修改访客";
    }
    this.ch = {
      firstDayOfWeek: 0,
      dayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
      dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
      dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
      monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
      monthNamesShort: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    };
    this.maxDate = new Date();
  }

  closeVisitorEditMask(bool){
    this.closeVisitorEdit.emit(bool);
  }

}
