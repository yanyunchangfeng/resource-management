import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasestationGroupComponent } from './basestation-group.component';

describe('BasestationGroupComponent', () => {
  let component: BasestationGroupComponent;
  let fixture: ComponentFixture<BasestationGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasestationGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasestationGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
