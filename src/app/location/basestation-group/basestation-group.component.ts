import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-basestation-group',
  templateUrl: './basestation-group.component.html',
  styleUrls: ['./basestation-group.component.css']
})
export class BasestationGroupComponent implements OnInit {
  display;
  title;
  @Output() closeBaseGroup = new EventEmitter();
  baseGroups;
  baseStations;
  selectStation;

  constructor() { }

  ngOnInit() {
    this.display = true;
    this.title = "基站分组";
    this.baseGroups = [
      {one:'001',two:'1',three:'192.168.1.1',four:'1',five:'192.168.1.1'},
      {one:'002',two:'2',three:'192.168.1.2',four:'2',five:'192.168.1.2'},
    ];
    this.baseStations = [
      {one:'1',two:'192.168.1.1',three:'100',four:'100'},
      {one:'2',two:'192.168.1.1',three:'100',four:'100'},
    ];
  }

  addGroup(){
    alert(1111)
    console.log(this.selectStation)
  }

  //关闭基站分组弹框
  closeBaseGroupMask(bool){
    this.closeBaseGroup.emit(bool);
  }

}
