import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SelectItem} from "primeng/primeng";

@Component({
  selector: 'app-bind-tag',
  templateUrl: './bind-tag.component.html',
  styleUrls: ['./bind-tag.component.css']
})
export class BindTagComponent implements OnInit {
  display;
  title;
  person: FormGroup;
  @Output() clostBindTag = new EventEmitter();
  @Output() addBase = new EventEmitter();
  ids: SelectItem[];

  constructor(
    private fb: FormBuilder,
  ) {
    this.person = fb.group({
        remark1: ['', Validators.maxLength(100)],
      }
    )
    this.ids = [
      {label:'', value:null},
      {label:'1', value:'1'},
      {label:'2', value:'2'},
    ];
  }

  ngOnInit() {
    this.display = true;
    this.title = "绑定定位标签";
  }

  closeBindTagMask(bool){
    this.clostBindTag.emit(bool);
  }

}
