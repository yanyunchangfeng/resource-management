import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindTagComponent } from './bind-tag.component';

describe('BindTagComponent', () => {
  let component: BindTagComponent;
  let fixture: ComponentFixture<BindTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
