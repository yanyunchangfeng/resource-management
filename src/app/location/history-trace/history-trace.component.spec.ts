import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryTraceComponent } from './history-trace.component';

describe('HistoryTraceComponent', () => {
  let component: HistoryTraceComponent;
  let fixture: ComponentFixture<HistoryTraceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryTraceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryTraceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
