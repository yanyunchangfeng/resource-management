import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {LocationService} from "../location.service";

@Component({
  selector: 'app-history-trace',
  templateUrl: './history-trace.component.html',
  styleUrls: ['./history-trace.component.css']
})
export class HistoryTraceComponent implements OnInit {
  private myCanvas: any;
  private newCanvas: any;
  private routeCanvas: any;
  context;
  newContext;
  routeContext;
  count = 0;
  @ViewChild("myCanvas") myCanvasElem: ElementRef;
  @ViewChild("newCanvas") newCanvasElem: ElementRef;
  @ViewChild("routeCanvas") routeCanvasElem: ElementRef;
  @Input() currentVis;
  @Output() goBack = new EventEmitter();
  historyTraceData;

  constructor(
    private locationService: LocationService,
  ) { }

  ngOnInit() {
    this.getHistoryTraceData(this.currentVis);
    //底图
    this.newCanvas = this.newCanvasElem.nativeElement
    this.newContext = this.newCanvas.getContext("2d");
    let backgroundImg = new Image();
    backgroundImg.src = "./data/background.png";
    let that = this;
    backgroundImg.onload = function () {
      // that.newContext.drawImage(backgroundImg,0,0,725,347,0,0,that.newCanvas.width,that.newCanvas.height)
      that.newContext.drawImage(backgroundImg,0,0,that.newCanvas.width,that.newCanvas.height)
    }

    //历史轨迹
    this.routeCanvas = this.routeCanvasElem.nativeElement
    this.routeContext = this.routeCanvas.getContext("2d");

    //移动的轨迹
    this.myCanvas = this.myCanvasElem.nativeElement
    this.context = this.myCanvas.getContext("2d");
    /*let intervalObj = setInterval(function () {
      that.clearMap();
      that.move2D(that.context);//画标签
    },1000)*/
  }

  clearMap(){
    this.context.clearRect(0, 0, this.myCanvas.width, this.myCanvas.height);
  }

  move2D(context,x,y){
    /*let x = Math.random() * 100;
    let y = Math.random() * 100;*/
    this.drawPosition(context,"",x, y,"rgba(255,0,0,1)")
    /*$.get("http://localhost:8080/real_time_coor",
      function(data,status){
        if(status == "success"){
          //var res1=eval("("+data+")");
          var res1=data;
          for (var i = 0; i < res1.length; i++) {
            var res = res1[i];
            x = res.x;
            y = res.y;
            x_filter = res.x_filter;
            y_filter = res.y_filter;
            if(res.lab_id == lab_id_){
              pos_bs_ip1 =  res.bss[0].ip;
              pos_bs_ip2 =  res.bss[1].ip;
              pos_bs_dis1 =  res.bss[0].dis_f;
              pos_bs_dis2 =  res.bss[1].dis_f;
            }
            // pos_bs_id1 =  res.bss[0].id
            // pos_bs_id2 =  res.bss[1].id
            // pos_bs_dis1 =  res.bss[0].lab_dist
            // pos_bs_dis2 =  res.bss[1].lab_dist

            //distance_p3 = res.r3
            drawPosition(context,"",x, y,"rgba(204,0,51,0.1)")
            drawPosition(context,"id"+res.lab_id,x_filter, y_filter,"rgba(204,0,51,1)")
          }

        }
      });*/
  }

  //在地图上画点
  drawPosition(context,name,x,y,color){
    //基站坐标比例缩放
    let zoom = 1;
    let x_ = x*zoom;
    let y_ = y*zoom;

    context.beginPath();
    context.arc(x_, y_, 5, 0, Math.PI * 2, true);
    //不关闭路径路径会一直保留下去，当然也可以利用这个特点做出意想不到的效果
    context.fillStyle = color;
    context.fill();
    context.closePath();

    context.fillText(name+"("+(x)+","+(y)+")", x_+20,y_+20)
  }

  //历史轨迹的数据
  getHistoryTraceData(visitor){
    //请求数据
    let tag = {
      "id": "",
      "cid": "",
      "lable_id": "",
      "visitor_cid": visitor.cid,
      "space_cid": "",
      "x": "",
      "y": "",
      "status": "",
      "datetime_start": "",
      "datetime_end": ""
    };
    this.locationService.getTagHistoryInfo(tag).subscribe(data => {
      console.log(data);
      this.historyTraceData = data;
      //画历史轨迹
      this.routeContext.moveTo(0,0);
      for (let i = 0,len = this.historyTraceData.length;i < len;i++){
        this.routeContext.lineTo(this.historyTraceData[i].x / 7,this.historyTraceData[i].y / 7);
      }
      this.routeContext.stroke();
      //按历史轨迹移动
      let that = this;
      let intervalObj = setInterval(function () {
        that.clearMap();
        that.move2D(that.context,that.historyTraceData[that.count].x / 60,that.historyTraceData[that.count].y / 60);//画标签
        that.count++;
      },30)
    })
  }

  //返回
  goBackPage(){
    this.goBack.emit();
  }

}
