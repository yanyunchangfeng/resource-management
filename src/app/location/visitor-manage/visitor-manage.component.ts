import {Component, OnInit} from '@angular/core';
import {LocationService} from "../location.service";

@Component({
  selector: 'app-visitor-manage',
  templateUrl: './visitor-manage.component.html',
  styleUrls: ['./visitor-manage.component.css']
})
export class VisitorManageComponent implements OnInit {
  queryModel;
  tabs = [];
  tabIndex = 0;
  // flags: SelectItem[];
  flags: Array<object>;
  todayVisitors;
  visitors;
  visitorEditMask;
  bindTagMask;
  selectVisitor;
  visitorState;
  visitorDetailMask;
  historyTraceMask = false;
  selectVis: any = {};
  ch;
  name;
  sex;
  maxDate;
  yearRange;//日期选择器，年份可选范围
  currentVisitor = {};
  historyTotal;


  constructor(
    private locationService: LocationService,
  ) {
    this.flags = [
      {label:'', value:null},
      {label:'是', value:'是'},
      {label:'否', value:'否'},
    ];
  }

  ngOnInit() {
    this.queryModel={
      "condition":{
        "name": "",
        "id_number": "",
        // "end": "",//是否结束
        "alarm": "",
        "start_time_start":"",
        "start_time_end":"",
        "end_time_start":"",
        "end_time_end":"",
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.yearRange = (new Date().getFullYear() - 10) + ":" + (new Date().getFullYear() + 10);
    console.log(this.yearRange)
    this.visitorEditMask = false;
    this.visitorDetailMask = false;
    this.bindTagMask = false;
    this.maxDate = new Date();
    this.tabs = [
      {label:'今日访客'},
      {label:'历史访客'},
    ];
    this.ch = {
      firstDayOfWeek: 0,
      dayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
      dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
      dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
      monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
      monthNamesShort: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    };
    //加载表格数据-今日访客
    let todayParam = {};
    this.getVisitorsInfo(todayParam);
    //加载表格数据-历史访客
    /*let historyParam = {};
    this.getVisitorsInfo(historyParam);*/
  }

  //查询访客-历史访客
  queryVisitor(){
    console.log(this.queryModel);
    this.getVisitorsInfo(this.queryModel);
  }

  //显示编辑访客信息弹框
  showVisitorEdit(datas){
    if(!datas){//新增
      this.visitorState = "add";
    }else {//修改
      this.selectVisitor = datas;
      this.visitorState = "update";
    }
    this.visitorEditMask = true;
  }

  //关闭编辑访客信息弹框
  closeVisitorEdit(bool){
    this.visitorEditMask = bool;
  }

  //显示访客详情弹框
  showVisitorDetail(data,type){
    if (type == "today"){
      console.log(data)
      this.name = data.name;
      this.sex = data.sex;
      this.currentVisitor = data;
      console.log(this.currentVisitor)
    }else {
      console.log(data)
      this.changeTab(2);
      this.currentVisitor = data;
      console.log(this.currentVisitor)
      // this.visitorDetailMask = true;
    }
  }

  //查看历史轨迹
  showHistoryTrace(data){
    console.log(data)
    for(let key in data){
      this.selectVis[key] = data[key];
    }
    console.log(this.selectVis)
    this.historyTraceMask = true;
  }

  goBack(){
    this.historyTraceMask = false;
  }

  //关闭访客详情弹框
  clostVisitorDetail(bool){
    this.visitorEditMask = bool;
  }

  //显示绑定定位标签弹框
  bindTag(){
    this.bindTagMask = true;
  }

  //切换选项卡
  changeTab(index){
    this.currentVisitor = {};
    this.tabIndex = index;
    this.getVisitorsInfo("");
  }

  //关闭绑定定位标签弹框
  clostBindTag(bool){
    this.bindTagMask = bool;
  }

  //获取访客数据
  getVisitorsInfo(data){
    this.locationService.getVisitorsInfo(data).subscribe(data => {
      // console.log(data)
      this.todayVisitors = data.items;
      this.visitors = data.items;
      this.historyTotal = this.visitors.length;
    })
  }

}
