import { Component, OnInit } from '@angular/core';
import {SelectItem} from "primeng/primeng";

@Component({
  selector: 'app-history-alarm',
  templateUrl: './history-alarm.component.html',
  styleUrls: ['./history-alarm.component.css']
})
export class HistoryAlarmComponent implements OnInit {
  queryModel;
  flags: SelectItem[];
  visitors;
  visitorEditMask;
  bindTagMask;
  selectVisitor;
  visitorState;

  constructor() {
    this.flags = [
      {label:'', value:null},
      {label:'是', value:'yes'},
      {label:'否', value:'no'},
    ]; }

  ngOnInit() {
    this.queryModel={
      "condition":{
        "sid": "",
        "status": "",
        "begin_time": "",
        "end_time": "",
        "creator": ""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.visitorEditMask = false;
    this.bindTagMask = false;
    this.visitors = [
      {one:'张三',two:'越界报警',three:'2',four:'2018-01-02 10:10',five:'张三进入权限外区域：B机房',six:'未处理',seven:'',eight:''},
      {one:'李四',two:'越界报警',three:'2',four:'2018-01-02 10:10',five:'李四进入权限外区域：B机房',six:'未处理',seven:'',eight:''},
    ];
  }

  //显示编辑访客信息弹框
  showVisitorEdit(datas){
    if(!datas){//新增
      this.visitorState = "add";
    }else {//修改
      this.visitorState = "update";
    }
    this.visitorEditMask = true;
  }

  //关闭编辑访客信息弹框
  clostVisitorEdit(bool){
    this.visitorEditMask = bool;
  }

  //显示绑定定位标签弹框
  bindTag(){
    this.bindTagMask = true;
  }

  //关闭绑定定位标签弹框
  clostBindTag(bool){
    this.bindTagMask = bool;
  }

}
