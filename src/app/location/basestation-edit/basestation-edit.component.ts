import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-basestation-edit',
  templateUrl: './basestation-edit.component.html',
  styleUrls: ['./basestation-edit.component.css']
})
export class BasestationEditComponent implements OnInit {
  display;
  title;
  person: FormGroup;
  @Output() closeBaseEdit = new EventEmitter();
  @Output() addBase = new EventEmitter();
  @Output() updateBase = new EventEmitter();
  @Input() currentBase;
  @Input() state;

  constructor(
    private fb: FormBuilder,
  ) {
    this.person = fb.group({
        remark1: ['', Validators.maxLength(100)],
      }
    )
  }

  ngOnInit() {
    this.display = true;
    if(this.state == "add"){
      this.title = "添加基站";
    }else {
      this.title = "修改基站";
    }
  }

  closeBaseEditMask(bool){
    this.closeBaseEdit.emit(bool);
  }

}
