import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasestationEditComponent } from './basestation-edit.component';

describe('BasestationEditComponent', () => {
  let component: BasestationEditComponent;
  let fixture: ComponentFixture<BasestationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasestationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasestationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
