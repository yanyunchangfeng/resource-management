import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {StorageService} from "../services/storage.service";

@Injectable()
export class LocationService {
  ip = environment.url.management;

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
  ) { }

  //获取访客信息
  getVisitorsInfo(data){
    let token = this.storageService.getToken("token");
    return this.http.post(
      `${environment.url.management}/position`,
      {
        "access_token": token,
        "type": "position_visitor_get",
        "data": data
      }
    ).map((res: Response) => {
      if(res["errcode"] !== "00000"){
        return [];
      }
      return res["data"];
    })
  }

  //获取标签定位实时数据树数据
  getTagTree(tag){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/position`, {
      "access_token": token,
      "type": "position_visitor_realtime_data_tree_get",
      "data": tag
     /* "data": {
        "id": "",
        "cid": "",
        "name": "",
        "dep": "",
        "nid": "",
        "father_nid": ""
      }*/
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      //构造primeNG tree的数据
      let datas = res['datas'];
      /*for(let i = 0;i < datas.length;i++){
        datas[i].label = datas[i].name;
        datas[i].leaf = false;
      }*/
      return datas;
    })
  }

  //修改树节点的状态：展开还是折叠
  updateNodeStatus(node){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/position`, {
      "access_token": token,
      "type": "position_space_mod",
      "data": node
      /*"data": {
        "cid": "cid",
        "name": "",
        "dep": "",
        "nid": "",
        "father_nid": "",
        "node_type": "",
        "remark": "",
        "del": "",
        "open": "是否打开（固定值：是、否）"
      }*/

    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return [];
    })
  }

  //获取定位标签实时位置数据
  getTagLocationInfo(cids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/position`, {
      "access_token": token,
      "type": "position_visitor_realtime_data_get",
      "cids": cids
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //获取定位标签历史位置数据
  getTagHistoryInfo(tag){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/position`, {
      "access_token": token,
      "type": "position_history_records_get",
      "data": tag
      /*"data": {
        "id": "自增长id",
        "cid": "cid",
        "lable_id": "标签id",
        "visitor_cid": "所属来访人员cid",
        "space_cid": "所属地图cid",
        "x": "x坐标",
        "y": "y坐标",
        "status": "状态",
        "datetime_start": "开始时间",
        "datetime_end": "结束时间"
      }*/

    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }

}
