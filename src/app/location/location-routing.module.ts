import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LocationConfigComponent} from "./location-config/location-config.component";
import {VisitorManageComponent} from "./visitor-manage/visitor-manage.component";
import {RealtimeMonitorComponent} from "./realtime-monitor/realtime-monitor.component";
import {HistoryAlarmComponent} from "./history-alarm/history-alarm.component";

const route: Routes = [
  {path: 'config', component: LocationConfigComponent},
  {path: 'visitor', component: VisitorManageComponent},
  {path: 'monitor', component: RealtimeMonitorComponent},
  {path: 'alarm', component: HistoryAlarmComponent},
];

@NgModule({
  imports: [RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class LocationRoutingModule { }
