import { Component, OnInit } from '@angular/core';
import {MenuItem, SelectItem, TreeNode} from "primeng/primeng";

@Component({
  selector: 'app-location-config',
  templateUrl: './location-config.component.html',
  styleUrls: ['./location-config.component.css']
})
export class LocationConfigComponent implements OnInit {
  queryModel;
  tabs = [];
  tabIndex = 0;
  cities: SelectItem[];
  baseEditMask: boolean;//是否显示编辑基站弹框
  baseGroupMask: boolean;//是否显示基站分组弹框
  roomstationsEditMask: boolean;//是否显示添加基站分组到机房弹框
  selectBase;
  baseState;
  rooms;//区域管理，左侧树数据
  items: MenuItem[];//机房树，操作列表,新增，修改，删除
  selected: TreeNode;                     // 被选中的节点
  baseGroups;//区域管理，右侧表格数据
  tags;
  strategies;

  constructor() {
    this.cities = [
      {label:'', value:null},
      {label:'通信正常', value:'NY'},
      {label:'断线', value:'RM'},
    ];
  }

  ngOnInit() {
    this.queryModel={
      "condition":{
        "sid": "",
        "status": "",
        "begin_time": "",
        "end_time": "",
        "creator": ""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.tabs = [
      {label:'基站管理'},
      {label:'区域管理'},
      {label:'标签管理'},
      {label:'告警配置'}
    ];
    this.baseEditMask = false;
    this.baseGroupMask = false;
    this.rooms = [
      {
        label:'区域',
        children:[
          {label:'A机房区域'},
          {label:'A机房区域'},
          {label:'A机房区域'},
          {label:'A机房区域'},
        ]
      }
    ];
    //右击机房节点的弹框列表
    this.items = [
      {label: '新增', icon: 'fa-plus', command: (event) => this.showRoomEdit(this.selected, true)},
      {label: '修改', icon: 'fa-edit', command: (event) => this.showRoomEdit(this.selected, false)},
      {label: '删除', icon: 'fa-close', command: (event) => this.showRoomDelete(this.selected)},
    ];
    this.baseGroups = [
      {one:'001',two:'1',three:'192.168.1.1',four:'1',five:'192.168.1.1'},
      {one:'002',two:'2',three:'192.168.1.2',four:'2',five:'192.168.1.2'},
    ];
    this.tags = [
      {one:'1',two:'备注',three:'通信正常',four:'A机房区域',five:'100',six:'100',seven:'001'},
      {one:'2',two:'备注',three:'通信正常',four:'B机房区域',five:'100',six:'100',seven:'001'},
    ];
    this.strategies = [
      {one:'越界报警',two:'是',three:'短信'},
      {one:'设备故障报警',two:'是  ',three:'短信'},
    ];
  }

  //弹出新增或者修改节点弹框
  showRoomEdit(node, flag) {
    /*this.systemService.judgeOrgEditPower().subscribe(res =>{//新增或修改组织节点前，判断当前用户是否有权限
      // console.log(res['errcode']);
      if(res['errcode'] == '00000'){//有权限
        this.showOrgEditMask = true;
        this.selectedNode = node;
        if(flag){//新增
          this.state = 'add';
        }else{//修改
          this.state = 'update';
        }
      }else {//没权限
        this.showError(res);
      }
    });*/
  }

  //显示删除组织节点弹框
  showRoomDelete(node){
    /*if(node.oid == 1){
      this.showWarn('不可删除根组织！');
    }else {
      this.systemService.judgeOrgEditPower().subscribe(res =>{//判断当前用户是否有删除权限
        console.log(res['errcode']);
        if(res['errcode'] == '00000'){//有权限
          this.systemService.getPersonList(node.oid).subscribe(data => {
            if(data.length == 0){
              this.deleteOrgTip = "请确认是否删除组织？";
            }else {
              this.deleteOrgTip = "存在关联人员，删除后组织人员将归并于上一级组织，请确认是否删除？";
            }
            this.showOrgDeleteMask = true;
            this.deleteOid = node.oid;
          }, (err: Error) => {
            console.log(err)
          })
        }else {//没权限
          this.showError(res);
        }
      });
    }*/
  }

  //显示编辑基站弹框
  showBaseEdit(datas){
    if(!datas){//新增
      this.baseState = "add";
    }else {//修改
      this.baseState = "update";
    }
    this.baseEditMask = true;
  }

  //关闭编辑基站弹框
  closeBaseEdit(bool){
    this.baseEditMask = bool;
  }

  //显示基站分组弹框
  showBaseGroup(){
    this.baseGroupMask = true;
  }

  //关闭基站分组弹框
  closeBaseGroup(bool){
    this.baseGroupMask = bool;
  }

  //显示"添加基站分组到机房"弹框
  showRoomstationsEdit(){
    this.roomstationsEditMask = true;
  }

  //关闭"添加基站分组到机房"弹框
  closeRoomstationsEdit(bool){
    this.roomstationsEditMask = bool;
  }

  //切换选项卡
  changeTab(index){
    this.tabIndex = index;
  }
}
