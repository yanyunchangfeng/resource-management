import { NgModule } from '@angular/core';

import { LocationRoutingModule } from './location-routing.module';
import { LocationConfigComponent } from './location-config/location-config.component';
import { VisitorManageComponent } from './visitor-manage/visitor-manage.component';
import { RealtimeMonitorComponent } from './realtime-monitor/realtime-monitor.component';
import { HistoryAlarmComponent } from './history-alarm/history-alarm.component';
import {ShareModule} from "../shared/share.module";
import { BasestationEditComponent } from './basestation-edit/basestation-edit.component';
import { BasestationGroupComponent } from './basestation-group/basestation-group.component';
import { RoomstationsEditComponent } from './roomstations-edit/roomstations-edit.component';
import { VisitorEditComponent } from './visitor-edit/visitor-edit.component';
import { BindTagComponent } from './bind-tag/bind-tag.component';
import { VisitorDetailComponent } from './visitor-detail/visitor-detail.component';
import { TagEditComponent } from './tag-edit/tag-edit.component';
import {LocationService} from "./location.service";
import { HistoryTraceComponent } from './history-trace/history-trace.component';

@NgModule({
  imports: [
    ShareModule,
    LocationRoutingModule
  ],
  providers: [
    LocationService,
  ],
  declarations: [LocationConfigComponent, VisitorManageComponent, RealtimeMonitorComponent, HistoryAlarmComponent, BasestationEditComponent, BasestationGroupComponent, RoomstationsEditComponent, VisitorEditComponent, BindTagComponent, VisitorDetailComponent, TagEditComponent, HistoryTraceComponent]
})
export class LocationModule { }
