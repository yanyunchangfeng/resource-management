import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-roomstations-edit',
  templateUrl: './roomstations-edit.component.html',
  styleUrls: ['./roomstations-edit.component.css']
})
export class RoomstationsEditComponent implements OnInit {
  display;
  title;
  @Output() closeRoomstationsEdit = new EventEmitter();
  baseGroups;

  constructor() { }

  ngOnInit() {
    this.display = true;
    this.title = "添加基站定位分组";
    this.baseGroups = [
      {one:'001',two:'1',three:'192.168.1.1',four:'1',five:'192.168.1.1'},
      {one:'002',two:'2',three:'192.168.1.2',four:'2',five:'192.168.1.2'},
    ];
  }

  closeRoomstationsEditMask(bool){
    this.closeRoomstationsEdit.emit(bool);
  }

}
