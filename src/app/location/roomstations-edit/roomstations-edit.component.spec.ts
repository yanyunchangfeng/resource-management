import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomstationsEditComponent } from './roomstations-edit.component';

describe('RoomstationsEditComponent', () => {
  let component: RoomstationsEditComponent;
  let fixture: ComponentFixture<RoomstationsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomstationsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomstationsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
