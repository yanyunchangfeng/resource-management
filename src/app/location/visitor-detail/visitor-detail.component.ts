import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-visitor-detail',
  templateUrl: './visitor-detail.component.html',
  styleUrls: ['./visitor-detail.component.css']
})
export class VisitorDetailComponent implements OnInit {
  display;
  title;
  visitor: FormGroup;
  @Output() clostVisitorDetail = new EventEmitter();
  @Input() currentVisitor;

  constructor(
    private fb: FormBuilder,
  ) {
    this.visitor = fb.group({
      remark1: ['', Validators.maxLength(100)],
    })
  }

  ngOnInit() {
    this.display = true;
    this.title = "访客详情";
  }

  closeVisitorDetailMask(bool){
    this.clostVisitorDetail.emit(bool);
  }

}
