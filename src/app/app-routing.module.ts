import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './services/auth-guard';
import {HomeModule} from './home/home.module';
import {SelectivePreloadingStrategy} from './selective-preloading-strategy';

const route: Routes = [

  {path: '', canActivate: [AuthGuard], redirectTo:  'index/equipment', pathMatch: 'full'},
  {
      path: 'login',
      loadChildren: 'app/login/login.module#LoginModule'
  }, // 惰性加载
];
@NgModule({
  imports: [HomeModule, RouterModule.forRoot(route, {preloadingStrategy: SelectivePreloadingStrategy, enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
