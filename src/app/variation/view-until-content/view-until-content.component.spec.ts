import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUntilContentComponent } from './view-until-content.component';

describe('ViewUntilContentComponent', () => {
  let component: ViewUntilContentComponent;
  let fixture: ComponentFixture<ViewUntilContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUntilContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUntilContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
