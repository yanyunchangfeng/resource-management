import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RectificationVariationComponent } from './rectification-variation.component';

describe('RectificationVariationComponent', () => {
  let component: RectificationVariationComponent;
  let fixture: ComponentFixture<RectificationVariationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RectificationVariationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RectificationVariationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
