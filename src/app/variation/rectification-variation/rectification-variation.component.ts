import {Component, OnInit, ViewChild} from '@angular/core';
import {BasePage} from '../../base.page';
import {SimpleFormContentComponent} from '../public/simple-form-content/simple-form-content.component';
import {UnvariationTableComponent} from '../public/unvariation-table/unvariation-table.component';
import {SimpleFormResultComponent} from '../public/simple-form-result/simple-form-result.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VariationObjModel} from '../variationObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {VariationService} from '../variation.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {PUblicMethod} from '../../services/PUblicMethod';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-rectification-variation',
  templateUrl: './rectification-variation.component.html',
  styleUrls: ['./rectification-variation.component.scss']
})
export class RectificationVariationComponent extends BasePage implements OnInit {
    @ViewChild('simpleFormContent') simpleFormContent: SimpleFormContentComponent;
    @ViewChild('unvariationTable') unvariationTable: UnvariationTableComponent;
    @ViewChild('simpleFormResult') simpleFormResult: SimpleFormResultComponent;
    formTitle = '整改变动';
    myForm: FormGroup;
    formObj: VariationObjModel;




    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private variationService: VariationService,
                public messageService: MessageService,
                private eventBusService: EventBusService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new VariationObjModel();
        this.initMyForm();
        this.getQueryParams();
    }

    initMyForm = () =>  {
        this.myForm = this.fb.group({
            approve_remarks: [null, Validators.required]
        });
    };

    goBack = () => {
        this.eventBusService.variationFlowCharts.next('');
        this.router.navigate(['../overview'], {relativeTo: this.activedRouter});
    };

    isFieldValid = name => {
        return PUblicMethod.isFieldValid(this.myForm, name);
    };

    operate = result => {
        // this.formObj.status = result;
        // this.requestReview();
    };

    requestReview = () =>  {
        this.variationService.requestReview(this.formObj).subscribe(res => {
            if (res === '00000') {
                this.alert('审核操作成功');
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            } else {
                this.alert(`审核操作失败${res}`, 'error');
            }
        });
    }

    unaccessTableDatasEimitter = device => {
        // this.unaccessEquipTabel.tableDatas = device;
    }

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.formObj.sid = params['sid']);
            this.requestMesageBySid(params['sid']);
            (params['status']) && (this.eventBusService.variationFlowCharts.next(params['status']));
        });
    };

    requestMesageBySid = id => {
        this.variationService.getMesageBySid(id).subscribe(res => {
            if (res['errcode'] === '00000') {
                this.formObj = new VariationObjModel(res['data']);
                this.simpleFormContent.formObj = new VariationObjModel(res['data']);
                this.simpleFormResult.formObj = new VariationObjModel(res['data']);
                this.unvariationTable.tableDatas = res['data']['devices'];
            }else {
                this.alert(`获取出入单信息失败 + ${res['errmsg']}`, 'error');
            }
        });
    };

    save = () => {
        this.formObj.devices = this.unvariationTable.tableDatas;
        this.requestSaveAfterEdited();
    };

    requestSaveAfterEdited = () => {
        this.variationService.requestSaveAfterEdited(this.formObj).subscribe(res => {
            if ( res === '00000' ) {
                this.alert('修改保存成功')
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            }else {
                this.alert(`修改保存失败 + ${res}`, 'error');
            }
        });
    };

    finish = () => {
        this.formObj.status = 'yes';
        this.formObj.devices = this.unvariationTable.tableDatas;
        this.requestFinish();
    };

    requestFinish = () => {
        this.variationService.requestFinish(this.formObj).subscribe(res => {
            if ( res === '00000' ) {
                this.alert('执行完成成功')
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            }else {
                this.alert(`执行完成失败 + ${res}`, 'error');
            }
        });
    };


}
