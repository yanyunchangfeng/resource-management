import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnapproveVariationComponent } from './unapprove-variation.component';

describe('UnapproveVariationComponent', () => {
  let component: UnapproveVariationComponent;
  let fixture: ComponentFixture<UnapproveVariationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnapproveVariationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnapproveVariationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
