import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleFormResultComponent } from './simple-form-result.component';

describe('SimpleFormResultComponent', () => {
  let component: SimpleFormResultComponent;
  let fixture: ComponentFixture<SimpleFormResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleFormResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleFormResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
