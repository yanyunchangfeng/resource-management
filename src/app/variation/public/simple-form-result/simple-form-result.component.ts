import { Component, OnInit } from '@angular/core';
import {VariationObjModel} from '../../variationObj.model';

@Component({
  selector: 'app-simple-form-result',
  templateUrl: './simple-form-result.component.html',
  styleUrls: ['./simple-form-result.component.scss']
})
export class SimpleFormResultComponent implements OnInit {
    formObj: VariationObjModel;
    constructor() { }

    ngOnInit() {
        this.formObj = new VariationObjModel();
    }

}
