import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfirmationService} from 'primeng/api';
import {VariationService} from '../../variation.service';
import {BasePage} from '../../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-variation-btn-delete',
  templateUrl: './variation-btn-delete.component.html',
  styleUrls: ['./variation-btn-delete.component.scss']
})
export class VariationBtnDeleteComponent extends BasePage implements OnInit {
    @Input() public data: any;
    @Output() informTableChangedEmitter: EventEmitter<any> = new EventEmitter();
    sidArray: string[] = [];
    constructor(public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private variationService: VariationService,
                private eventBusService: EventBusService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {

    }

    delete = data => {
        this.sidArray.length = 0;
        this.sidArray[this.sidArray.length] = data['sid'];
        this.eventBusService.variationFlowCharts.next(status);
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: () => {
                this.requestDelet();
                this.eventBusService.variationFlowCharts.next('');
            },
            reject: () => {
                this.eventBusService.variationFlowCharts.next('');
            }
        });
    };

    requestDelet = () => {
        this.variationService.requestDelete(this.sidArray).subscribe(res => {
            if (res === '00000') {
                this.alert('删除成功');
                this.informTableChangedEmitter.emit(res);
                // this.eventBusService.accessSearchOverview.next(this.searchObj);
            }else {
                this.alert('删除失败');
            }

        })
    };

    getQueryParam = status => {

    }

}
