import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBtnDeleteComponent } from './variation-btn-delete.component';

describe('VariationBtnDeleteComponent', () => {
  let component: VariationBtnDeleteComponent;
  let fixture: ComponentFixture<VariationBtnDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBtnDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBtnDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
