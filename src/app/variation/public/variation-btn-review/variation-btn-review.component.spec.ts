import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBtnReviewComponent } from './variation-btn-review.component';

describe('VariationBtnReviewComponent', () => {
  let component: VariationBtnReviewComponent;
  let fixture: ComponentFixture<VariationBtnReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBtnReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBtnReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
