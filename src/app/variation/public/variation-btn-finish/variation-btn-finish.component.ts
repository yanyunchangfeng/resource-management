import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-variation-btn-finish',
  templateUrl: './variation-btn-finish.component.html',
  styleUrls: ['./variation-btn-finish.component.scss']
})
export class VariationBtnFinishComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate(['../finish'], {queryParams: {sid: this.data.sid, status: this.data.status_code} , relativeTo: this.activatedRoute});
    }

}
