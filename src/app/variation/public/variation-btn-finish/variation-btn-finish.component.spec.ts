import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBtnFinishComponent } from './variation-btn-finish.component';

describe('VariationBtnFinishComponent', () => {
  let component: VariationBtnFinishComponent;
  let fixture: ComponentFixture<VariationBtnFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBtnFinishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBtnFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
