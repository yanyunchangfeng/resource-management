import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationSearchFormComponent } from './variation-search-form.component';

describe('VariationSearchFormComponent', () => {
  let component: VariationSearchFormComponent;
  let fixture: ComponentFixture<VariationSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationSearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
