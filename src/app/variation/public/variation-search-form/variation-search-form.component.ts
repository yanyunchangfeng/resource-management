import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {VariationService} from '../../variation.service';
import {PUblicMethod} from '../../../services/PUblicMethod';
import {VariationSearchObjModel} from '../../variationSearchObj.model';

@Component({
  selector: 'app-variation-search-form',
  templateUrl: './variation-search-form.component.html',
  styleUrls: ['./variation-search-form.component.scss']
})
export class VariationSearchFormComponent implements OnInit {
    @Output() searchObjEmitter = new EventEmitter();
    allStatus: Array<object> = [];
    searchObj: VariationSearchObjModel;
    zh: any;

    constructor(private variationService: VariationService) {
    }

    ngOnInit() {
        this.zh = new PUblicMethod().initZh();
        this.searchObj = new VariationSearchObjModel();
        this.getAllAccessStatus();
    }

    getAllAccessStatus = () => {
        this.variationService.getAllVariationStatus().subscribe(res => {
            this.allStatus = res;
            this.searchObj.status = res[0]['value'];
        });
    };

    clearSearch() {
        this.searchObj.sid = '';
        this.searchObj.applicant = '';
        this.searchObj.status = '';
        this.searchObj.begin_time = '';
    };

    searchSchedule() {
        (this.searchObj.begin_time) && (this.searchObj.begin_time = PUblicMethod.formateEnrtyTime(this.searchObj.begin_time));
        this.searchObjEmitter.emit(this.searchObj);
    }

}
