import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BasePage} from '../../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {VariationService} from '../../variation.service';

@Component({
  selector: 'app-variation-btn-close',
  templateUrl: './variation-btn-close.component.html',
  styleUrls: ['./variation-btn-close.component.scss']
})
export class VariationBtnCloseComponent extends BasePage implements OnInit {
    @Input() public data: any;
    @Output() informTableChangedEmitter: EventEmitter<any> = new EventEmitter();
    constructor(private router: Router,
                public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private variationService: VariationService,
                private activatedRoute: ActivatedRoute) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
    }

    close = data => {
        this.confirmationService.confirm({
            message: '确认执行吗？',
            accept: () => {
                this.requestClose(data['sid']);
                // this.eventBusService.accessFlowCharts.next('');
            },
            reject: () => {
                // this.eventBusService.accessFlowCharts.next('');
            }
        });
    }

    requestClose = sid => {
        this.variationService.requestClose(sid).subscribe(res => {
            if (res === '00000') {
                this.alert('关闭成功');
                this.informTableChangedEmitter.emit(res);
            }else {
                this.alert('关闭失败');
            }
        })
    }
}
