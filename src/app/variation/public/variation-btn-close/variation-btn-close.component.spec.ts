import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBtnCloseComponent } from './variation-btn-close.component';

describe('VariationBtnCloseComponent', () => {
  let component: VariationBtnCloseComponent;
  let fixture: ComponentFixture<VariationBtnCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBtnCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBtnCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
