import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleFormContentComponent } from './simple-form-content.component';

describe('SimpleFormContentComponent', () => {
  let component: SimpleFormContentComponent;
  let fixture: ComponentFixture<SimpleFormContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleFormContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleFormContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
