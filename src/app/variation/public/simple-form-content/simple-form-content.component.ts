import { Component, OnInit } from '@angular/core';
import {VariationObjModel} from '../../variationObj.model';
import {EventBusService} from '../../../services/event-bus.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-simple-form-content',
  templateUrl: './simple-form-content.component.html',
  styleUrls: ['./simple-form-content.component.scss']
})
export class SimpleFormContentComponent implements OnInit {
    formObj: VariationObjModel;
    constructor(private eventBusService: EventBusService,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.formObj = new VariationObjModel();
        this.getQueryParam();
    }

    getQueryParam = () => {
        this.activatedRoute.queryParams.subscribe(param => {
            (param['status']) && (this.eventBusService.variationFlowCharts.next(param['status']));
        })
    }


}
