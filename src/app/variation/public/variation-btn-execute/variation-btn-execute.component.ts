import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {VariationService} from '../../variation.service';
import {BasePage} from '../../../base.page';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-variation-btn-execute',
  templateUrl: './variation-btn-execute.component.html',
  styleUrls: ['./variation-btn-execute.component.scss']
})
export class VariationBtnExecuteComponent extends BasePage implements OnInit {
    @Input() public data: any;
    @Output() informTableChangedEmitter: EventEmitter<any> = new EventEmitter();
    constructor(private router: Router,
                public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private variationService: VariationService,
                private eventBusService: EventBusService,
                private activatedRoute: ActivatedRoute) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
    }

    execute = data => {
        this.eventBusService.variationFlowCharts.next(status);
        this.confirmationService.confirm({
            message: '确认执行吗？',
            accept: () => {
                this.requestExecute(data['sid']);
                this.eventBusService.variationFlowCharts.next('');
            },
            reject: () => {
                this.eventBusService.variationFlowCharts.next('');
            }
        });

    };

    requestExecute = sid => {
        this.variationService.requestExecute(sid).subscribe(res => {
            if (res === '00000') {
                this.alert('执行成功');
                this.informTableChangedEmitter.emit(res);
            }else {
                this.alert('执行失败');
            }
        })
    }

}
