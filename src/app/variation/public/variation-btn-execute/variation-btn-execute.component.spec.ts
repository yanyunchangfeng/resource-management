import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBtnExecuteComponent } from './variation-btn-execute.component';

describe('VariationBtnExecuteComponent', () => {
  let component: VariationBtnExecuteComponent;
  let fixture: ComponentFixture<VariationBtnExecuteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBtnExecuteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBtnExecuteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
