import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-variation-btn-approve',
  templateUrl: './variation-btn-approve.component.html',
  styleUrls: ['./variation-btn-approve.component.scss']
})
export class VariationBtnApproveComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate(['../approve'], {queryParams: {sid: this.data.sid, status: this.data.status_code} , relativeTo: this.activatedRoute});
    }

}
