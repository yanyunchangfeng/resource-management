import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBtnApproveComponent } from './variation-btn-approve.component';

describe('VariationBtnApproveComponent', () => {
  let component: VariationBtnApproveComponent;
  let fixture: ComponentFixture<VariationBtnApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBtnApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBtnApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
