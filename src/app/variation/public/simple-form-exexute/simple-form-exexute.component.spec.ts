import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleFormExexuteComponent } from './simple-form-exexute.component';

describe('SimpleFormExexuteComponent', () => {
  let component: SimpleFormExexuteComponent;
  let fixture: ComponentFixture<SimpleFormExexuteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleFormExexuteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleFormExexuteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
