import { Component, OnInit } from '@angular/core';
import {VariationObjModel} from '../../variationObj.model';

@Component({
  selector: 'app-simple-form-exexute',
  templateUrl: './simple-form-exexute.component.html',
  styleUrls: ['./simple-form-exexute.component.scss']
})
export class SimpleFormExexuteComponent implements OnInit {
    formObj: VariationObjModel;
    constructor() { }

    ngOnInit() {
        this.formObj = new VariationObjModel();
    }

}
