import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBtnRectificationComponent } from './variation-btn-rectification.component';

describe('VariationBtnRectificationComponent', () => {
  let component: VariationBtnRectificationComponent;
  let fixture: ComponentFixture<VariationBtnRectificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBtnRectificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBtnRectificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
