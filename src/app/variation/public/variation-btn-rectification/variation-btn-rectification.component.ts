import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-variation-btn-rectification',
  templateUrl: './variation-btn-rectification.component.html',
  styleUrls: ['./variation-btn-rectification.component.scss']
})
export class VariationBtnRectificationComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate(['../rectifivation'], {queryParams: {sid: this.data.sid, status: this.data.status_code} , relativeTo: this.activatedRoute});
    }
}
