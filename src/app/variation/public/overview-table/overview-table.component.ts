import {Component, Input, OnInit} from '@angular/core';
import {VariationService} from '../../variation.service';
import {ActivatedRoute, Router} from '@angular/router';
import {VariationSearchObjModel} from '../../variationSearchObj.model';

@Component({
  selector: 'app-overview-table',
  templateUrl: './overview-table.component.html',
  styleUrls: ['./overview-table.component.scss']
})
export class OverviewTableComponent implements OnInit {
    @Input() searchType;
    searchObj: VariationSearchObjModel;
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数

    constructor(private variationService: VariationService,
                private activatedRoute: ActivatedRoute,
                private router: Router) { }

    ngOnInit() {
        this.searchObj = new VariationSearchObjModel();
        this.getQueryParam();
        this.getDatas();
    }
    getQueryParam = () => {
        this.activatedRoute.queryParams.subscribe(res => {
            // (res['status']) && (this.searchObj.status = res['status']);
            (res['searchType']) && (this.searchType = res['searchType']);
        });
    };

    getDatas = () => {
        if (this.searchType === 'all') {
            // this.eventBusService.accessMineOverview.next('all');
            this.getAccessOverview(this.searchObj);
        }
        if (this.searchType === 'mine') {
            // this.eventBusService.accessMineOverview.next('mine');
            this.getMineAccessOverview(this.searchObj);
        }
    };

    getAccessOverview = (obj?) => {
        this.variationService.getAcessOverview(obj).subscribe(res => {
            if (res) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    };

    getMineAccessOverview = (obj?) => {
        this.variationService.getMineAcessOverview(obj).subscribe(res => {
            if (res) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    };

    resetPage = res => {
        if ( 'items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    };

    paginate = event => {
        this.searchObj.page_size = event.rows.toString();
        this.searchObj.page_number = (event.page + 1).toString();
        this.getDatas();
    };

    onOperate = data => {
        switch (data.status_code){
            case 'new':
            case 'approve':
            case 'approve_no':
                this.router.navigate(['../viewuntilcontent'], {queryParams: {sid: data.sid, status: data.status_code}, relativeTo: this.activatedRoute});
                break;
            case 'execute':
            case 'executing':
            case 'reject':
                this.router.navigate(['../viewuntilexexute'], {queryParams: {sid: data.sid, status: data.status_code}, relativeTo: this.activatedRoute});
                break;
            case 'auditor':
            case 'auditor_yes':
            case 'readjust':
                this.router.navigate(['../viewuntilresult'], {queryParams:  {sid: data.sid, status: data.status_code}, relativeTo: this.activatedRoute});
        }
    };

    getInformChanged = event => {
        if (event === '00000') {
            this.getDatas();
        }
    }

}
