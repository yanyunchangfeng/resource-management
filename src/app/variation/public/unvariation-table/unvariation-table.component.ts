import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {VariationService} from '../../variation.service';
import {EquipmentService} from '../../../equipment/equipment.service';
import {BasePage} from '../../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';

@Component({
  selector: 'app-unvariation-table',
  templateUrl: './unvariation-table.component.html',
  styleUrls: ['./unvariation-table.component.scss']
})
export class UnvariationTableComponent extends BasePage  implements OnInit {
    @Input() searchType;
    tableDatas: any[] = [];
    cols: any[] = [];
    selectedCars3: any = [];
    showOperator: boolean = false;
    showAddOrUpdateMask: boolean = false;
    tempAsset;
    state = 'update';
    dataSource;
    totalRecords;
    assets;


    constructor(private activedRouter: ActivatedRoute,
                private equipmentService: EquipmentService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        // this.formObj = new FormobjModel();
        this.initCols();
        this.getQueryParams();
    }

    initCols = () => {
        this.cols = [
            { field: '设备编号', header: '设备编号' },
            { field: '设备名称', header: '设备名称' },
            { field: '设备分类', header: '设备分类' },
            { field: '型号', header: '型号' },
            { field: '位置', header: '位置' },
            { field: '状态', header: '状态' }
        ];
    }

    tableDataEmitter = dataArray => {
        // this.tableDatas = dataArray;
        // console.log(dataArray)
        // this.accessSelectEquip.searchObj.filter_asset_nos = this.filterUnaccessArrayOnlyId(this.unaccessTable.tableDatas); // 设备过滤
        // this.getSelectedAnoArray(dataArray);
    };

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            if (params['status'] === 'readjust' || params['status'] === 'executing') {
                this.cols.push(  { field: '操作', header: '操作' });
               this.showOperator = true;
            }
        });
    };

    edit = data => {
        this.tempAsset = data;
        console.log(this.tempAsset)
        this.showAddOrUpdateMask = !this.showAddOrUpdateMask;
    }

    closeMask(bool){
        this.showAddOrUpdateMask = bool;
    }

    updateAsset(bool){
        this.queryAssets();
        this.showAddOrUpdateMask = bool;
    }

    queryAssets(){
        this.equipmentService.getAssets().subscribe(asset => {
            this.dataSource = asset;
            if (this.dataSource){
                this.totalRecords = this.dataSource.length;
                this.dataSource = this.dataSource.slice(0, 10);
            }else{
                this.totalRecords = 0;
                this.dataSource = [];
            }
        }, (err: Error) => {
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504){
                this.alert('似乎网络出现了问题，请联系管理员或稍后重试', 'error');
            }else{
                this.alert(err, 'error');
            }
        })
    }


    getAfterEditedEmitter = data => {
        this.findIndexOfVariation(this.tableDatas, data);
        // this.totalRecords = data.length;
        // this.tableDatas = data.slice(0, 10);
    };

    findIndexOfVariation = (array, data) => {
        const  index = array.findIndex(v => {
           return  v['ano'] = data['ano'];
        });
        if (index > -1) {
           for (let key in array[index]) {
               array[index][key] = data[key];
           }
           this.tableDatas.splice(index, 1, array[index]);
        }
    }
  loadCarsLazy(event) {
    setTimeout(() => {
      if(this.dataSource) {
        this.tableDatas = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 0);
  }
}
