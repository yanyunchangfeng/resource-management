import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnvariationTableComponent } from './unvariation-table.component';

describe('UnvariationTableComponent', () => {
  let component: UnvariationTableComponent;
  let fixture: ComponentFixture<UnvariationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnvariationTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnvariationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
