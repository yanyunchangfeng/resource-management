import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-variation-btn-edit',
  templateUrl: './variation-btn-edit.component.html',
  styleUrls: ['./variation-btn-edit.component.scss']
})
export class VariationBtnEditComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        this.router.navigate(['../createvariation'], {queryParams: {sid: this.data.sid, status: this.data.status_code} , relativeTo: this.activatedRoute});
    }

}
