import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBtnEditComponent } from './variation-btn-edit.component';

describe('VariationBtnEditComponent', () => {
  let component: VariationBtnEditComponent;
  let fixture: ComponentFixture<VariationBtnEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBtnEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBtnEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
