import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadjustVariationOverviewComponent } from './readjust-variation-overview.component';

describe('ReadjustVariationOverviewComponent', () => {
  let component: ReadjustVariationOverviewComponent;
  let fixture: ComponentFixture<ReadjustVariationOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadjustVariationOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadjustVariationOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
