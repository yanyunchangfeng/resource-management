import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewVariationComponent } from './review-variation.component';

describe('ReviewVariationComponent', () => {
  let component: ReviewVariationComponent;
  let fixture: ComponentFixture<ReviewVariationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewVariationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewVariationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
