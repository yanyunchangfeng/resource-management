import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUntilResultComponent } from './view-until-result.component';

describe('ViewUntilResultComponent', () => {
  let component: ViewUntilResultComponent;
  let fixture: ComponentFixture<ViewUntilResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUntilResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUntilResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
