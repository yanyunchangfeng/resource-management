export class VariationObjModel {
    sid: string;
    name: string;
    status: string;
    status_code: string;
    create_time: string;
    update_time: string;
    adjust_type: string;
    content: string;
    plan_time_begin: string;
    plan_time_end: string;
    room_name: string;
    room_did: string;

    creator: string;
    creator_pid: string;
    creator_org: string;
    creator_org_oid: string;

    applicant: string;
    applicant_pid: string;
    applicant_org: string;
    applicant_org_oid: string;

    approve: string;
    approve_pid: string;
    approve_org: string;
    approve_org_oid: string;
    approve_remarks: string;
    approve_time: string;

    executor: string;
    executor_pid: string;
    executor_org: string;
    executor_org_oid: string;
    executor_time: string;
    executor_result: string;

    auditor: string;
    auditor_pid: string;
    auditor_org: string;
    auditor_org_oid: string;
    auditor_remarks: string;
    auditor_time: string;

    owner: string;
    owner_pid: string;
    devices: Array<any>;
    operations: Array<any>;

    constructor(obj?) {
        this.sid = obj && obj['sid'] || '';
        this.name = obj && obj['name'] || '';
        this.status = obj && obj['status'] || '';
        this.status_code = obj && obj['status_code'] || '';
        this.create_time = obj && obj['create_time'] || '';
        this.update_time = obj && obj['update_time'] || '';
        this.adjust_type = obj && obj['adjust_type'] || '';
        this.content = obj && obj['content'] || '';
        this.plan_time_begin = obj && obj['plan_time_begin'] || '';
        this.plan_time_end = obj && obj['plan_time_end'] || '';
        this.room_name = obj && obj['room_name'] || '';
        this.room_did = obj && obj['room_did'] || '';

        this.creator = obj && obj['creator'] || '';
        this.creator_pid = obj && obj['creator_pid'] || '';
        this.creator_org = obj && obj['creator_org'] || '';
        this.creator_org_oid = obj && obj['creator_org_oid'] || '';

        this.applicant = obj && obj['applicant'] || '';
        this.applicant_pid = obj && obj['applicant_pid'] || '';
        this.applicant_org = obj && obj['applicant_org'] || '';
        this.applicant_org_oid = obj && obj['applicant_org_oid'] || '';

        this.approve = obj && obj['approve'] || '';
        this.approve_pid = obj && obj['approve_pid'] || '';
        this.approve_org = obj && obj['approve_org'] || '';
        this.approve_org_oid = obj && obj['approve_org_oid'] || '';
        this.approve_remarks = obj && obj['approve_remarks'] || '';
        this.approve_time = obj && obj['approve_time'] || '';

        this.executor = obj && obj['executor'] || '';
        this.executor_pid = obj && obj['executor_pid'] || '';
        this.executor_org = obj && obj['executor_org'] || '';
        this.executor_org_oid = obj && obj['executor_org_oid'] || '';
        this.executor_time = obj && obj['executor_time'] || '';
        this.executor_result = obj && obj['executor_result'] || '';

        this.auditor = obj && obj['auditor'] || '';
        this.auditor_pid = obj && obj['auditor_pid'] || '';
        this.auditor_org = obj && obj['auditor_org'] || '';
        this.auditor_org_oid = obj && obj['auditor_org_oid'] || '';
        this.auditor_remarks = obj && obj['auditor_remarks'] || '';
        this.auditor_time = obj && obj['auditor_time'] || '';

        this.owner = obj && obj['owner'] || '';
        this.owner_pid = obj && obj['owner_pid'] || '';
        this.devices = obj && obj['devices'] || [];
        this.operations = obj && obj['operations'] || [];

    }
}