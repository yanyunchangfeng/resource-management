import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnexecuteVariationOverviewComponent } from './unexecute-variation-overview.component';

describe('UnexecuteVariationOverviewComponent', () => {
  let component: UnexecuteVariationOverviewComponent;
  let fixture: ComponentFixture<UnexecuteVariationOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnexecuteVariationOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnexecuteVariationOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
