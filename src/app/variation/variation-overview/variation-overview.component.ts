import {Component, OnInit, ViewChild} from '@angular/core';
import {VariationSearchFormComponent} from '../public/variation-search-form/variation-search-form.component';
import {OverviewTableComponent} from '../public/overview-table/overview-table.component';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-variation-overview',
  templateUrl: './variation-overview.component.html',
  styleUrls: ['./variation-overview.component.scss']
})
export class VariationOverviewComponent implements OnInit {
    @ViewChild('variationSearchForm') variationSearchForm: VariationSearchFormComponent;
    @ViewChild('overviewTable') overviewTable: OverviewTableComponent;
    searchType: string = 'all';
    constructor(private eventBusService: EventBusService) { }

    ngOnInit() {
        this.eventBusService.variationMineOverview.next('');
    }

    getSearchObjEmitter = data => {
        this.overviewTable.getAccessOverview(data);
    };

}
