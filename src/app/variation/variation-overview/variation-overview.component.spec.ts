import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationOverviewComponent } from './variation-overview.component';

describe('VariationOverviewComponent', () => {
  let component: VariationOverviewComponent;
  let fixture: ComponentFixture<VariationOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
