import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishedVariationComponent } from './finished-variation.component';

describe('FinishedVariationComponent', () => {
  let component: FinishedVariationComponent;
  let fixture: ComponentFixture<FinishedVariationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishedVariationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedVariationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
