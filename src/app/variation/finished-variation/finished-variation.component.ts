import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from 'primeng/components/common/messageservice';
import {VariationService} from '../variation.service';
import {ConfirmationService} from 'primeng/api';
import {BasePage} from '../../base.page';
import {PUblicMethod} from '../../services/PUblicMethod';
import {SimpleFormContentComponent} from '../public/simple-form-content/simple-form-content.component';
import {UnvariationTableComponent} from '../public/unvariation-table/unvariation-table.component';
import {VariationObjModel} from '../variationObj.model';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-finished-variation',
  templateUrl: './finished-variation.component.html',
  styleUrls: ['./finished-variation.component.scss']
})
export class FinishedVariationComponent extends BasePage implements OnInit {
    @ViewChild('simpleFormContent') simpleFormContent: SimpleFormContentComponent;
    @ViewChild('unvariationTable') unvariationTable: UnvariationTableComponent;
    formTitle = '完成变动';
    myForm: FormGroup;
    formObj: VariationObjModel;

    constructor(private fb: FormBuilder,
                private router: Router,
                private eventBusService: EventBusService,
                private activedRouter: ActivatedRoute,
                private variationService: VariationService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new VariationObjModel();
        this.initMyForm();
        this.getQueryParams();
    }

    initMyForm = () =>  {
        this.myForm = this.fb.group({
            executor_result: [null, Validators.required]
        });
    };

    goBack = () => {
        this.eventBusService.variationFlowCharts.next('');
        this.router.navigate(['../overview'], {relativeTo: this.activedRouter});
    };

    isFieldValid = name => {
        return PUblicMethod.isFieldValid(this.myForm, name);
    };

    operate = result => {
        // this.formObj.status = result;
        // if (!this.formObj.approver_remarks) {
        //     PUblicMethod.validateAllFormFields(this.myForm);
        // } else {
        //     this.requestApprove();
        // }
    };

    requestApprove = () =>  {
        // this.accessService.requestApprove(this.formObj).subscribe(res => {
        //     if (res === '00000') {
        //         this.alert('操作成功');
        //         window.setTimeout(() => {
        //             this.goBack();
        //         }, 1100);
        //     } else {
        //         this.alert(`操作失败${res}`, 'error');
        //     }
        // });
    }

    unaccessTableDatasEimitter = device => {
        // this.unaccessEquipTabel.tableDatas = device;
    }

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.formObj.sid = params['sid']);
            this.requestMesageBySid(params['sid']);
            (params['status']) && (this.eventBusService.variationFlowCharts.next(params['status']));
        });
    };

    requestMesageBySid = id => {
        this.variationService.getMesageBySid(id).subscribe(res => {
            if (res['errcode'] === '00000') {
                this.formObj = new VariationObjModel(res['data']);
                this.simpleFormContent.formObj = new VariationObjModel(res['data']);
                this.unvariationTable.tableDatas = res['data']['devices'];
            }else {
                this.alert(`获取出入单信息失败 + ${res['errmsg']}`, 'error');
            }
        });
    };

    requestSaveAfterEdited = () => {
        this.variationService.requestSaveAfterEdited(this.formObj).subscribe(res => {
            if ( res === '00000' ) {
                this.alert('修改保存成功')
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            }else {
                this.alert(`修改保存失败 + ${res}`, 'error');
            }
        });
    };

    finish = () => {
        this.formObj.status = 'yes';
        if (this.myForm.valid) {
            this.requestFinish();
        }else {
            PUblicMethod.validateAllFormFields(this.myForm);
        }
    };

    requestFinish = () => {
        this.variationService.requestFinish(this.formObj).subscribe(res => {
            if ( res === '00000' ) {
                this.alert('执行完成成功')
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            }else {
                this.alert(`执行完成失败 + ${res}`, 'error');
            }
        });
    }

}
