import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditVariationComponent } from './create-edit-variation.component';

describe('CreateEditVariationComponent', () => {
  let component: CreateEditVariationComponent;
  let fixture: ComponentFixture<CreateEditVariationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditVariationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditVariationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
