import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../services/storage.service';

@Injectable()
export class VariationService {
    ip = environment.url.management;
    constructor(private http: HttpClient,
                private storageService: StorageService) { }

    //    流程图接口获取
    getFlowChart(status?) {
        status = status ? status : '';
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_workflow_get',
                'data': {
                    'status': status
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

    //  获取、查询出入管理总览
    getAcessOverview(obj?: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_get',
                'data': {
                    'condition': {
                        sid: obj['sid'] ,
                        creator: obj['creator'] ,
                        applicant: obj['applicant'] ,
                        begin_time: obj['begin_time'],
                        end_time: obj['end_time'] ,
                        status: obj['status']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }

            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    //  获取带我处理出入管理
    getMineAcessOverview(obj?: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_get_byowner',
                'data': {
                    'condition': {
                        sid: obj['sid'] ,
                        creator: obj['creator'] ,
                        applicant: obj['applicant'] ,
                        begin_time: obj['begin_time'],
                        end_time: obj['end_time'] ,
                        status: obj['status']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }


            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    //  所有状态获取接口
    getAllVariationStatus() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_statuslist_get'
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

    //  请求单保存
    requestSave(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_save',
                'data': {
                    adjust_type: obj['adjust_type'],
                    content: obj['content'],
                    plan_time_begin: obj['plan_time_begin'],
                    plan_time_end: obj['plan_time_end'],
                    room_name: obj['room_name'],
                    room_did: obj['room_did'],
                    applicant: obj['applicant'],
                    applicant_pid: obj['applicant_pid'],
                    applicant_org: obj['applicant_org'],
                    applicant_org_oid: obj['applicant_org_oid'],
                    approve: obj['approve'],
                    approve_pid: obj['approve_pid'],
                    approve_org: obj['approve_org'],
                    approve_org_oid: obj['approve_org_oid'],
                    devices: obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单编辑后保存
    requestSaveAfterEdited(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_mod_save',
                'data': {
                    sid: obj['sid'],
                    adjust_type: obj['adjust_type'],
                    content: obj['content'],
                    plan_time_begin: obj['plan_time_begin'],
                    plan_time_end: obj['plan_time_end'],
                    room_name: obj['room_name'],
                    room_did: obj['room_did'],
                    applicant: obj['applicant'],
                    applicant_pid: obj['applicant_pid'],
                    applicant_org: obj['applicant_org'],
                    applicant_org_oid: obj['applicant_org_oid'],
                    approve: obj['approve'],
                    approve_pid: obj['approve_pid'],
                    approve_org: obj['approve_org'],
                    approve_org_oid: obj['approve_org_oid'],
                    devices: obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }


    //  请求单提交
    requestSubmite(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_add',
                'data': {
                    adjust_type: obj['adjust_type'],
                    content: obj['content'],
                    plan_time_begin: obj['plan_time_begin'],
                    plan_time_end: obj['plan_time_end'],
                    room_name: obj['room_name'],
                    room_did: obj['room_did'],
                    applicant: obj['applicant'],
                    applicant_pid: obj['applicant_pid'],
                    applicant_org: obj['applicant_org'],
                    applicant_org_oid: obj['applicant_org_oid'],
                    approve: obj['approve'],
                    approve_pid: obj['approve_pid'],
                    approve_org: obj['approve_org'],
                    approve_org_oid: obj['approve_org_oid'],
                    'devices': obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单修改提交
    requestSubmitAfterEdit(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_mod_submit',
                'data': {
                    sid: obj['sid'],
                    adjust_type: obj['adjust_type'],
                    content: obj['content'],
                    plan_time_begin: obj['plan_time_begin'],
                    plan_time_end: obj['plan_time_end'],
                    room_name: obj['room_name'],
                    room_did: obj['room_did'],
                    applicant: obj['applicant'],
                    applicant_pid: obj['applicant_pid'],
                    applicant_org: obj['applicant_org'],
                    applicant_org_oid: obj['applicant_org_oid'],
                    approve: obj['approve'],
                    approve_pid: obj['approve_pid'],
                    approve_org: obj['approve_org'],
                    approve_org_oid: obj['approve_org_oid'],
                    devices: obj['devices']
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单信息获取
    getMesageBySid(trickSid: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_get_byid',
                'id': trickSid
            }
        ).map((res: Response) => {
            return res;
        });
    }

    // 请求单刪除
    requestDelete(ids: Array<string>) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_del',
                'ids': ids
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 执行变动
    requestExecute(sid: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_execute',
                'data': { 'sid': sid }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 关闭驳回
    requestClose(sid: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_auditor',
                'data': { 'sid': sid }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 【执行中】||【整改】状态下的完成变动
    requestFinish(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_executing',
                'data': {
                    sid: obj['sid'],
                    status: obj['status'],
                    executor_result: obj['executor_result'],
                    devices: obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 变动单审核
    requestReview(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_auditor',
                'data': {
                    sid: obj['sid'],
                    auditor_remarks: obj['auditor_remarks'],
                    status: obj['status']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 请求单审批
    requestApprove(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'adjustment_approve',
                'data': {
                    sid: obj['sid'],
                    approve_remarks: obj['approve_remarks'],
                    status: obj['status'],
                    executor: obj['executor'],
                    executor_pid: obj['executor_pid'],
                    executor_org: obj['executor_org'],
                    executor_org_oid: obj['executor_org_oid'],
                    devices: obj['devices']
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
}
