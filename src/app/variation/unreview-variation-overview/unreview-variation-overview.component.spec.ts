import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnreviewVariationOverviewComponent } from './unreview-variation-overview.component';

describe('UnreviewVariationOverviewComponent', () => {
  let component: UnreviewVariationOverviewComponent;
  let fixture: ComponentFixture<UnreviewVariationOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnreviewVariationOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnreviewVariationOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
