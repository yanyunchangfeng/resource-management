import {Component, OnInit, ViewChild} from '@angular/core';
import {VariationSearchFormComponent} from '../public/variation-search-form/variation-search-form.component';
import {OverviewTableComponent} from '../public/overview-table/overview-table.component';
import {VariationSearchObjModel} from '../variationSearchObj.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-unreview-variation-overview',
  templateUrl: './unreview-variation-overview.component.html',
  styleUrls: ['./unreview-variation-overview.component.scss']
})
export class UnreviewVariationOverviewComponent implements OnInit {
    @ViewChild('variationSearchForm') variationSearchForm: VariationSearchFormComponent;
    @ViewChild('overviewTable') overviewTable: OverviewTableComponent;
    searchObj: VariationSearchObjModel;
    searchType: string = '';

    constructor(private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.searchObj = new VariationSearchObjModel();
        this.getQueryParams();
    }

    getQueryParams = () => {
        this.activatedRoute.queryParams.subscribe(param => {
            this.searchObj.status = param['status'];
            param['searchType'] ? this.searchType  = param['searchType'] : '';
            (this.searchType ) && (this.overviewTable.getMineAccessOverview(this.searchObj));
            (!this.searchType ) && (this.overviewTable.getAccessOverview(this.searchObj));
        })
    };

    getSearchObjEmitter = data => {
        (this.searchType ) && (this.overviewTable.getMineAccessOverview(this.searchObj));
        (!this.searchType ) && (this.overviewTable.getAccessOverview(this.searchObj));
    };

}
