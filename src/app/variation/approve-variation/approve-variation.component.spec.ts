import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveVariationComponent } from './approve-variation.component';

describe('ApproveVariationComponent', () => {
  let component: ApproveVariationComponent;
  let fixture: ComponentFixture<ApproveVariationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveVariationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveVariationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
