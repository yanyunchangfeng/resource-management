import {Component, OnInit, ViewChild} from '@angular/core';
import {SimpleFormContentComponent} from '../public/simple-form-content/simple-form-content.component';
import {UnvariationTableComponent} from '../public/unvariation-table/unvariation-table.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VariationObjModel} from '../variationObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {VariationService} from '../variation.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {BasePage} from '../../base.page';
import {SimpleFormExexuteComponent} from '../public/simple-form-exexute/simple-form-exexute.component';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-view-until-exexute',
  templateUrl: './view-until-exexute.component.html',
  styleUrls: ['./view-until-exexute.component.scss']
})
export class ViewUntilExexuteComponent  extends  BasePage implements OnInit {
    @ViewChild('simpleFormContent') simpleFormContent: SimpleFormContentComponent;
    @ViewChild('simpleFormExecute') simpleFormExecute: SimpleFormExexuteComponent;
    @ViewChild('unvariationTable') unvariationTable: UnvariationTableComponent;
    formTitle = '查看申请单';
    myForm: FormGroup;
    formObj: VariationObjModel;
    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private variationService: VariationService,
                public messageService: MessageService,
                private eventBusService: EventBusService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new VariationObjModel();
        this.initMyForm();
        this.getQueryParams();
    }

    initMyForm = () =>  {
        this.myForm = this.fb.group({
            approve_remarks: [null, Validators.required],
            executor_org: [null, Validators.required]
        });
    };

    goBack = () => {
        this.eventBusService.variationFlowCharts.next('');
        this.router.navigate(['../overview'], {relativeTo: this.activedRouter});
    };

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.formObj.sid = params['sid']);
            this.requestMesageBySid(params['sid']);
        });
    };

    requestMesageBySid = id => {
        this.variationService.getMesageBySid(id).subscribe(res => {
            if (res['errcode'] === '00000') {
                this.formObj = new VariationObjModel(res['data']);
                this.simpleFormContent.formObj = new VariationObjModel(res['data']);
                this.simpleFormExecute.formObj = new VariationObjModel(res['data']);
                this.unvariationTable.tableDatas = res['data']['devices'];
            }else {
                this.alert(`获取出入单信息失败 + ${res['errmsg']}`, 'error');
            }
        });
    };

}
