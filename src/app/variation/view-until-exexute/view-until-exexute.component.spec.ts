import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUntilExexuteComponent } from './view-until-exexute.component';

describe('ViewUntilExexuteComponent', () => {
  let component: ViewUntilExexuteComponent;
  let fixture: ComponentFixture<ViewUntilExexuteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUntilExexuteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUntilExexuteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
