import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishedVariationOverviewComponent } from './finished-variation-overview.component';

describe('FinishedVariationOverviewComponent', () => {
  let component: FinishedVariationOverviewComponent;
  let fixture: ComponentFixture<FinishedVariationOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishedVariationOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedVariationOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
