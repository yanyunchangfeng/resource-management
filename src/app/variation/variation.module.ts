import { NgModule } from '@angular/core';

import { VariationRoutingModule } from './variation-routing.module';
import {ShareModule} from '../shared/share.module';
import {PublicModule} from '../public/public.module';
import {VariationService} from './variation.service';
import {PublicService} from '../services/public.service';
import { VariationBasicComponent } from './variation-basic/variation-basic.component';
import { VariationSearchFormComponent } from './public/variation-search-form/variation-search-form.component';
import { OverviewTableComponent } from './public/overview-table/overview-table.component';
import { UnvariationTableComponent } from './public/unvariation-table/unvariation-table.component';
import { SimpleFormContentComponent } from './public/simple-form-content/simple-form-content.component';
import { SimpleFormResultComponent } from './public/simple-form-result/simple-form-result.component';
import { VariationBtnEditComponent } from './public/variation-btn-edit/variation-btn-edit.component';
import { VariationBtnDeleteComponent } from './public/variation-btn-delete/variation-btn-delete.component';
import { VariationBtnApproveComponent } from './public/variation-btn-approve/variation-btn-approve.component';
import { VariationBtnExecuteComponent } from './public/variation-btn-execute/variation-btn-execute.component';
import { VariationBtnFinishComponent } from './public/variation-btn-finish/variation-btn-finish.component';
import { VariationBtnReviewComponent } from './public/variation-btn-review/variation-btn-review.component';
import { VariationBtnCloseComponent } from './public/variation-btn-close/variation-btn-close.component';
import { VariationBtnRectificationComponent } from './public/variation-btn-rectification/variation-btn-rectification.component';
import { VariationOverviewComponent } from './variation-overview/variation-overview.component';
import { VariationMineOverviewComponent } from './variation-mine-overview/variation-mine-overview.component';
import { CreateEditVariationComponent } from './create-edit-variation/create-edit-variation.component';
import { FinishedVariationComponent } from './finished-variation/finished-variation.component';
import { ReviewVariationComponent } from './review-variation/review-variation.component';
import { UnapproveVariationComponent } from './unapprove-variation/unapprove-variation.component';
import { UnexecuteVariationOverviewComponent } from './unexecute-variation-overview/unexecute-variation-overview.component';
import { UnreviewVariationOverviewComponent } from './unreview-variation-overview/unreview-variation-overview.component';
import { FinishedVariationOverviewComponent } from './finished-variation-overview/finished-variation-overview.component';
import { ApproveVariationComponent } from './approve-variation/approve-variation.component';
import { ReadjustVariationOverviewComponent } from './readjust-variation-overview/readjust-variation-overview.component';
import { RectificationVariationComponent } from './rectification-variation/rectification-variation.component';
import { ViewUntilContentComponent } from './view-until-content/view-until-content.component';
import { ViewUntilExexuteComponent } from './view-until-exexute/view-until-exexute.component';
import { ViewUntilResultComponent } from './view-until-result/view-until-result.component';
import { SimpleFormExexuteComponent } from './public/simple-form-exexute/simple-form-exexute.component';

@NgModule({
    imports: [
      ShareModule,
      VariationRoutingModule,
      PublicModule
    ],
    declarations: [VariationBasicComponent, VariationSearchFormComponent, OverviewTableComponent, UnvariationTableComponent, SimpleFormContentComponent, SimpleFormResultComponent, VariationBtnEditComponent, VariationBtnDeleteComponent, VariationBtnApproveComponent, VariationBtnExecuteComponent, VariationBtnFinishComponent, VariationBtnReviewComponent, VariationBtnCloseComponent, VariationBtnRectificationComponent, VariationOverviewComponent, VariationMineOverviewComponent, CreateEditVariationComponent, FinishedVariationComponent, ReviewVariationComponent, UnapproveVariationComponent, UnexecuteVariationOverviewComponent, UnreviewVariationOverviewComponent, FinishedVariationOverviewComponent, ApproveVariationComponent, ReadjustVariationOverviewComponent, RectificationVariationComponent, ViewUntilContentComponent, ViewUntilExexuteComponent, ViewUntilResultComponent, SimpleFormExexuteComponent],
    providers: [
        VariationService,
        PublicService
    ]
})
export class VariationModule { }
