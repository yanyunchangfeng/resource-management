import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VariationBasicComponent} from './variation-basic/variation-basic.component';
import {VariationOverviewComponent} from './variation-overview/variation-overview.component';
import {VariationMineOverviewComponent} from './variation-mine-overview/variation-mine-overview.component';
import {CreateEditVariationComponent} from './create-edit-variation/create-edit-variation.component';
import {UnapproveVariationComponent} from './unapprove-variation/unapprove-variation.component';
import {UnreviewVariationOverviewComponent} from './unreview-variation-overview/unreview-variation-overview.component';
import {FinishedVariationOverviewComponent} from './finished-variation-overview/finished-variation-overview.component';
import {UnexecuteVariationOverviewComponent} from './unexecute-variation-overview/unexecute-variation-overview.component';
import {ApproveVariationComponent} from './approve-variation/approve-variation.component';
import {FinishedVariationComponent} from './finished-variation/finished-variation.component';
import {ReviewVariationComponent} from './review-variation/review-variation.component';
import {ReadjustVariationOverviewComponent} from './readjust-variation-overview/readjust-variation-overview.component';
import {RectificationVariationComponent} from './rectification-variation/rectification-variation.component';
import {ViewUntilContentComponent} from './view-until-content/view-until-content.component';
import {ViewUntilExexuteComponent} from './view-until-exexute/view-until-exexute.component';
import {ViewUntilResultComponent} from './view-until-result/view-until-result.component';

const routes: Routes = [
    {path: 'basic', component: VariationBasicComponent, children: [
        {path: 'overview', component: VariationOverviewComponent},
        {path: 'mineoverview', component: VariationMineOverviewComponent},
        {path: 'createvariation', component: CreateEditVariationComponent},
        {path: 'unapproveoverview', component: UnapproveVariationComponent},
        {path: 'unexecuteoverview', component: UnexecuteVariationOverviewComponent},
        {path: 'unreviewoverview', component: UnreviewVariationOverviewComponent},
        {path: 'finishedoverview', component: FinishedVariationOverviewComponent},
        {path: 'readjustoverview', component: ReadjustVariationOverviewComponent},
        {path: 'approve', component: ApproveVariationComponent},
        {path: 'finish', component: FinishedVariationComponent},
        {path: 'review', component: ReviewVariationComponent},
        {path: 'rectifivation', component: RectificationVariationComponent},
        {path: 'viewuntilcontent', component: ViewUntilContentComponent},
        {path: 'viewuntilexexute', component: ViewUntilExexuteComponent},
        {path: 'viewuntilresult', component: ViewUntilResultComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VariationRoutingModule { }
