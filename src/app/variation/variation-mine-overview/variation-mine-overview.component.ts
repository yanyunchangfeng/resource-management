import {Component, OnInit, ViewChild} from '@angular/core';
import {VariationSearchFormComponent} from '../public/variation-search-form/variation-search-form.component';
import {OverviewTableComponent} from '../public/overview-table/overview-table.component';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-variation-mine-overview',
  templateUrl: './variation-mine-overview.component.html',
  styleUrls: ['./variation-mine-overview.component.scss']
})
export class VariationMineOverviewComponent implements OnInit {
    @ViewChild('variationSearchForm') variationSearchForm: VariationSearchFormComponent;
    @ViewChild('overviewTable') overviewTable: OverviewTableComponent;
    searchType: string = 'mine';
    constructor(private eventBusService: EventBusService) { }

    ngOnInit() {
        this.eventBusService.variationMineOverview.next('mine');
    }

    getSearchObjEmitter = data => {
        this.overviewTable.getMineAccessOverview(data);
    };
}
