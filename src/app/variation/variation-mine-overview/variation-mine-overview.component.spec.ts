import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationMineOverviewComponent } from './variation-mine-overview.component';

describe('VariationMineOverviewComponent', () => {
  let component: VariationMineOverviewComponent;
  let fixture: ComponentFixture<VariationMineOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationMineOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationMineOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
