import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariationBasicComponent } from './variation-basic.component';

describe('VariationBasicComponent', () => {
  let component: VariationBasicComponent;
  let fixture: ComponentFixture<VariationBasicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariationBasicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariationBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
