import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../services/event-bus.service';
import {VariationService} from '../variation.service';

@Component({
  selector: 'app-variation-basic',
  templateUrl: './variation-basic.component.html',
  styleUrls: ['./variation-basic.component.scss']
})
export class VariationBasicComponent implements OnInit {
    flowChartDatas: Array<object> =  [];
    searchType: string = '';
    timer;

    constructor(private router: Router,
                private activedRouter: ActivatedRoute,
                private eventBusService: EventBusService,
                private variationService: VariationService) {
    }

    ngOnInit() {
        this.getFlowChart();
      this.ngAfterViewInit();
      this.eventBusService.variationMineOverview.subscribe(data => {
            this.searchType = data;
            console.log(this.searchType)
        });
        this.eventBusService.variationFlowCharts.subscribe(status => {
            this.getFlowChart(status);
        })
    }
    ngAfterViewInit() {
      this.timer = setInterval(() => {
       this.getFlowChart();
      }, 1000*60);

    }
    jumper = (num): void => {
        switch (num.toString()){
            case '1' :
                this.router.navigate(['./createvariation'], {queryParams: {status: 'STATUS_NEW', searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '2':
                this.router.navigate(['./unapproveoverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '4':
                this.router.navigate(['./unexecuteoverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '5':
            this.router.navigate(['./unreviewoverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '6':
                this.router.navigate(['./readjustoverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '8':
                this.router.navigate(['./finishedoverview'], {queryParams: {status: this.flowChartDatas[num]['status'], searchType: this.searchType}, relativeTo: this.activedRouter});
        }

    }

    getFlowChart = (status?: string) => {
        this.variationService.getFlowChart(status).subscribe(res => {
            this.flowChartDatas = res;
        });
    }
}
