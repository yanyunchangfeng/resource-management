import {NgModule} from '@angular/core';
import {ShareModule} from '../shared/share.module';
import {MonitorRoutingModule} from './monitor-routing.module';
import {MonitorEqumentComponent} from './monitor-equment/monitor-equment.component';
import {MonitorInfoComponent} from './monitor-info/monitor-info.component';

@NgModule({
  declarations: [
    MonitorEqumentComponent,
    MonitorInfoComponent
  ],
  imports: [
    ShareModule,
    MonitorRoutingModule,
  ]
})
export class MonitorModule {
}
