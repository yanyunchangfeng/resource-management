import { NgModule } from '@angular/core';
import { RouterModule , Routes} from '@angular/router';

import {MonitorEqumentComponent} from './monitor-equment/monitor-equment.component';
import {MonitorInfoComponent} from './monitor-info/monitor-info.component';

const route: Routes = [
  {path: 'monitor', component: MonitorEqumentComponent},
  {path: 'monitorInfo', component: MonitorInfoComponent},
];

@NgModule({
  imports: [RouterModule.forChild(route),
  ],
  exports: [RouterModule]
})
export class MonitorRoutingModule {

}
