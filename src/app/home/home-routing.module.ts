import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {AuthGuard} from '../services/auth-guard';
import {Code404Component} from './code404/code404.component';
const route: Routes = [
  {
    path: 'index', canActivate: [AuthGuard], component: HomeComponent,
    canActivateChild: [AuthGuard],
    children: [
      {path: 'equipment', loadChildren: 'app/equipment/equipment.module#EquipmentModule', data: { preload: true }},
      // {path: 'volume', loadChildren: 'app/volume/volume.module#VolumeModule'},
      // {path: 'monitor', loadChildren: 'app/monitor/monitor.module#MonitorModule'},
      // {path: 'connection', loadChildren: 'app/connection/connection.module#ConnectionModule'},
      {path: 'report', loadChildren: 'app/report/report.module#ReportModule'},
      {path: 'inspection', loadChildren: 'app/inspection/inspection.module#InspectionModule'},
      {path: 'duty', loadChildren: 'app/duty/duty.module#DutyModule'},
      // {path: 'flow', loadChildren: 'app/flow/flow.module#FlowModule'},
      // {path: 'position', loadChildren: 'app/position/position.module#PositionModule'},
      {path: 'maintenance', loadChildren: 'app/maintenance/maintenance.module#MaintenanceModule'},
      // {path: 'inventorymaintenance', loadChildren: 'app/inventorymaintenance/invenntorymaintenance.module#InventorymaintenanceModule'},
      // {path: 'spacemaintenance', loadChildren: 'app/inventorymaintenance/invenntorymaintenance.module#InventorymaintenanceModule'},
      {path: 'system', loadChildren: 'app/system/system.module#SystemModule'},
      {path: 'inventory', loadChildren: 'app/inventory/inventory.module#InventoryModule'},
      {path: 'reqForService', loadChildren: 'app/request-for-service/request-for-service.module#RequestForServiceModule'},
      // {path: 'process', loadChildren: 'app/process/process.module#ProcessModule'},
      // {path: 'shelf', loadChildren: 'app/shelf/shelf.module#ShelfModule'},
      {path: 'capacity', loadChildren: 'app/capacity/capacity.module#CapacityModule'},
      // {path: 'undershelf', loadChildren: 'app/undershelf/under-shelf.module#UnderShelfModule'},
      {path: 'knowledge', loadChildren: 'app/knowledge/knowledge.module#KnowledgeModule'},
      // {path: 'overview', loadChildren: 'app/overview/overview.module#OverviewModule'},
      {path: 'reports', loadChildren: 'app/reports/reports.module#ReportsModule'},
      // {path: 'location', loadChildren: 'app/location/location.module#LocationModule'},
      // {path: 'access', loadChildren: 'app/access/access.module#AccessModule'},
      // {path: 'variation', loadChildren: 'app/variation/variation.module#VariationModule'},
      // {path: 'test', loadChildren: 'app/testsvg/svg.module#SvgModule'},
      {path: '**', component: Code404Component}
      ]
  },
];
@NgModule({
  imports: [
    RouterModule.forChild(route),
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule {

}
