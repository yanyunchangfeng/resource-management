import {Component, OnInit} from '@angular/core';
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  showheader = true;
  private toggleBtnStatus:boolean = false;
  private menuActiveStatus:boolean = false;
  constructor(private eventBusService:EventBusService) {}

  ngOnInit() {
    if(window.sessionStorage.getItem('route')){
      this.showheader = false;
    }

  }
  public onTogglerClick(event):void{
    this.toggleBtnStatus = !this.toggleBtnStatus;
    this.menuActiveStatus = !this.menuActiveStatus
    this.eventBusService.menuActice.subscribe(value=>{
      this.menuActiveStatus = value
    })
    this.eventBusService.topToggleBtn.next(this.toggleBtnStatus);
    this.eventBusService.menuActice.next(this.menuActiveStatus);
  }
}
