import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import 'rxjs/add/operator/map';
import {StorageService} from '../services/storage.service';
import {environment} from "../../environments/environment";

@Injectable()
export class MainMenuService {

  constructor(private http: HttpClient, private storageService: StorageService) { }
  getMainMenu() {
    environment.url.management = this.storageService.getEnvironmentUrl('url');
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/jur`, {
      "access_token":token,
      "type": "menu",
      "id": "1"
    }).map((res: Response) => {
      // console.log(res);
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
         // let body = res.json();

      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
    })
  }
}
