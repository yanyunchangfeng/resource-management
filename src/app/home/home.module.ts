import {NgModule} from '@angular/core';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {MainMenuService} from './main-menu.service';
import {Code404Component } from './code404/code404.component';
import {ShareModule} from '../shared/share.module';

@NgModule({
  imports: [
    ShareModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    MainMenuComponent,
    Code404Component
    ],
  providers: [MainMenuService]
})
export class HomeModule {

}
