import {Directive, ElementRef, OnChanges, Input, OnInit, OnDestroy} from '@angular/core';
import * as echarts from 'echarts';

@Directive({
    selector: 'echart '
})
export class  EChartOptionDirective1 implements OnInit , OnChanges ,OnDestroy{
    @Input('chartType') chartType: any;
    echart;
    constructor(private el: ElementRef) {}

    public ngOnInit(): void {
      this.echart = echarts.init(this.el.nativeElement);
      this.echart.resize();
      this.echart.setOption(this.chartType);
      this.echart.resize();
    }
    ngOnChanges() {
      this.echart = echarts.init(this.el.nativeElement);
      this.echart.resize();
      this.echart.setOption(this.chartType);
      // if(this.echart){
      //   window.addEventListener('resize',()=>{
      //     this.echart.resize()
      //   })
      // }
    }
    ngOnDestroy(): void {
      // if(this.echart){
      //   this.echart.dispose(this.el.nativeElement);
      //   window.removeEventListener('resize',()=>{
      //      this.echart.resize()
      //   });
      // }
    }
}
