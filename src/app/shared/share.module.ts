import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SafeHtmlPipe} from './safe-html.pipe';
import {EChartOptionDirective1} from './echart-option.directive';
import {
  AutoCompleteModule,
  CalendarModule,
  CodeHighlighterModule,
  ColorPickerModule,
  ConfirmDialogModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  TreeModule,
  InputTextModule,
  MessageModule,
  MessagesModule,
  OrganizationChartModule,
  PanelModule,
  ProgressBarModule,
  RadioButtonModule,
  ScheduleModule,
  SharedModule,
  TabViewModule,
  ButtonModule,
  FileUploadModule,
  GrowlModule,
  InputTextareaModule,
  CheckboxModule,
  PaginatorModule,
  ContextMenuModule,
  DataListModule
} from 'primeng/primeng';
import {TableModule} from 'primeng/table';


@NgModule({
  declarations: [
      EChartOptionDirective1,
      SafeHtmlPipe
  ],
  exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AutoCompleteModule,
        CalendarModule,
        DataTableModule,
        ButtonModule,
        InputTextModule,
        DialogModule,
        CodeHighlighterModule,
        DropdownModule,
        FileUploadModule,
        GrowlModule,
        InputTextareaModule,
        ConfirmDialogModule,
        PanelModule,
        SharedModule,
        TabViewModule,
        EChartOptionDirective1,
        TreeModule,
        OrganizationChartModule,
        ProgressBarModule,
        MessagesModule,
        MessageModule,
        SafeHtmlPipe,
        RadioButtonModule,
        ColorPickerModule,
        ScheduleModule,
        PaginatorModule,
        ContextMenuModule,
        CheckboxModule,
        DataListModule,
        TableModule
  ]
})
export class ShareModule {

}
