export class KnowlsearchobjModel {
    classify: string;
    label: string;
    search: string;
    page_size: string;
    page_number: string;
    kid: string;
    workorder_type: string;
    workorder_type_name: string;
    did: string;
    status: string;
    filter_sids: Array<string>;

    constructor(obj?) {
        this.classify = obj && obj['classify'] || '';
        this.kid = obj && obj['kid'] || '';
        this.label = obj && obj['label'] || '';
        this.search = obj && obj['search'] || '';
        this.page_size = obj && obj['page_size'] || '10';
        this.page_number = obj && obj['page_number'] || '1';
        this.workorder_type = obj && obj['workorder_type'] || '';
        this.workorder_type_name = obj && obj['workorder_type_name'] || '';
        this.did = obj && obj['did'] || '';
        this.status = obj && obj['status'] || '';
        this.filter_sids = obj && obj['filter_sids'] || [];
    }
}