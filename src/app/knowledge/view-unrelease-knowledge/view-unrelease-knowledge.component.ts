import {Component, OnInit, ViewChild} from '@angular/core';
import {EditorComponent} from '../../public/editor/editor.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BasicinfoModel} from '../basicinfo.model';
import {Message} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {KnowledgeService} from '../knowledge.service';
import {FormobjModel} from '../formobj.model';

@Component({
  selector: 'app-view-unrelease-knowledge',
  templateUrl: './view-unrelease-knowledge.component.html',
  styleUrls: ['./view-unrelease-knowledge.component.scss']
})
export class ViewUnreleaseKnowledgeComponent implements OnInit {
    @ViewChild(EditorComponent) editor: EditorComponent;
    formTitle = '查看';
    myForm: FormGroup;
    formObj: FormobjModel;
    approval: Array<object>;
    ip: string;
    message: Message[] = [];
    switchValue: string = '0';

    constructor(private fb: FormBuilder,
                private router: Router,
                private knowledgeService: KnowledgeService,
                private activedRouter: ActivatedRoute) {

    }

    ngOnInit() {
        this.formObj = new FormobjModel();
        this.initMyForm();
        this.ip = environment.url.management;
        this.getQueryParam();
    }
    initMyForm() {
        this.myForm = this.fb.group({
            plan_under_time: '',
            plan_finished_time: ''
        });
    }
    goBack() {
        this.router.navigate(['../klgmanageoverview'], {relativeTo: this.activedRouter});
    }
    switch(which) {
        this.switchValue = which;
    }
    publishTopic() {
        let topicContent = this.editor.clickHandle();

        if (!topicContent){
            alert('请输入内容！');
            return;
        }
        alert(topicContent);
    }

    getQueryParam = () => {
        this.activedRouter.queryParams.subscribe(data => {
            if (data['kid']){
                this.knowledgeService.getKlgMessage(data['kid']).subscribe(res => {
                    if (res) {
                        this.formObj = new FormobjModel(res);
                    }
                });
            }
        });
    }
}
