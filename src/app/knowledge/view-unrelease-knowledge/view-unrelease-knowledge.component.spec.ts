import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUnreleaseKnowledgeComponent } from './view-unrelease-knowledge.component';

describe('ViewUnreleaseKnowledgeComponent', () => {
  let component: ViewUnreleaseKnowledgeComponent;
  let fixture: ComponentFixture<ViewUnreleaseKnowledgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUnreleaseKnowledgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUnreleaseKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
