import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-klg-mine',
  templateUrl: './klg-mine.component.html',
  styleUrls: ['./klg-mine.component.scss']
})
export class KlgMineComponent implements OnInit {
    searchType: string = 'mine';
    key: string = '';
    constructor(private router: Router,
                private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
    }

    refresh = () => {

    };

    searchSchedule = () => {
        this.router.navigate(['../klgmine'], {queryParams: {search: this.key}, relativeTo: this.activatedRouter});
    };
}
