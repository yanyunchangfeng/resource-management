import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgMineComponent } from './klg-mine.component';

describe('KlgMineComponent', () => {
  let component: KlgMineComponent;
  let fixture: ComponentFixture<KlgMineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgMineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgMineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
