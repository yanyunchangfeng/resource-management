export class BasicinfoModel {
    sid: string;
    creator: string;
    knowledge_type: string;
    create_time: string;
    subcategory: string;
    subdirectory: string;
    approver: string;
    manager: string;
    tags: string;
    title: string;
    details: string;
    attachments: string;
    reason: string
    constructor(obj?) {
        this.sid = obj && obj.sid || '';
        this.creator = obj && obj.creator || '';
        this.knowledge_type = obj && obj.knowledge_type || '';
        this.create_time = obj && obj.create_time || '';
        this.subcategory = obj && obj.subcategory || '';
        this.subdirectory = obj && obj.subdirectory || '';
        this.approver = obj && obj.approver || '';
        this.manager = obj && obj.manager || '';
        this.tags = obj && obj.tags || '';
        this.title = obj && obj.title || '';
        this.details = obj && obj.details || '';
        this.attachments = obj && obj.attachments || '';
    }
}