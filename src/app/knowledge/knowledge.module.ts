import { NgModule } from '@angular/core';

import { KnowledgeRoutingModule } from './knowledge-routing.module';
import {ShareModule} from '../shared/share.module';
import {PublicService} from '../services/public.service';
import {KnowledgeService} from './knowledge.service';
import { KnowledgeBasalDataComponent } from './knowledge-basal-data/knowledge-basal-data.component';
import { KlgManageChartComponent } from './klg-manage-chart/klg-manage-chart.component';
import { BtnEditComponent } from './public/btn-edit/btn-edit.component';
import { BtnDeleteComponent } from './public/btn-delete/btn-delete.component';
import { BtnReleaseComponent } from './public/btn-release/btn-release.component';
import { BtnReviewComponent } from './public/btn-review/btn-review.component';
import { BtnViewComponent } from './public/btn-view/btn-view.component';
import { KlgReuseTableComponent } from './public/klg-reuse-table/klg-reuse-table.component';
import { KlgViewAttachmentComponent } from './public/klg-view-attachment/klg-view-attachment.component';
import { KlgViewApprovalComponent } from './public/klg-view-approval/klg-view-approval.component';
import { KlgCurriculumTableComponent } from './public/klg-curriculum-table/klg-curriculum-table.component';
import { KlgAssociateOrderTableComponent } from './public/klg-associate-order-table/klg-associate-order-table.component';
import { AddBasicInfoComponent } from './add-basic-info/add-basic-info.component';
import { ApprovalBasicInfoComponent } from './approval-basic-info/approval-basic-info.component';
import { ReleaseBasicInfoComponent } from './release-basic-info/release-basic-info.component';
import { KlgManageOverviewComponent } from './klg-manage-overview/klg-manage-overview.component';
import { KlgMineComponent } from './klg-mine/klg-mine.component';
import { KlgBasicComponent } from './klg-basic/klg-basic.component';
import { BasicSearchFormComponent } from './public/basic-search-form/basic-search-form.component';
import { SelectAssociateOrderComponent } from './public/select-associate-order/select-associate-order.component';
import {PublicModule} from '../public/public.module';
import { ApprovalKnowledgeComponent } from './approval-knowledge/approval-knowledge.component';
import { ReleaseKnowledgeComponent } from './release-knowledge/release-knowledge.component';
import { ReviewKnowledgeComponent } from './review-knowledge/review-knowledge.component';
import { ViewUnapprovalKnowledgeComponent } from './view-unapproval-knowledge/view-unapproval-knowledge.component';
import { ViewUnreleaseKnowledgeComponent } from './view-unrelease-knowledge/view-unrelease-knowledge.component';
import { BtnApprovalComponent } from './public/btn-approval/btn-approval.component';

@NgModule({
    imports: [
        ShareModule,
        KnowledgeRoutingModule,
        PublicModule
    ],
    declarations: [KnowledgeBasalDataComponent, KlgManageChartComponent, BtnEditComponent, BtnDeleteComponent, BtnReleaseComponent, BtnReviewComponent, BtnViewComponent, KlgReuseTableComponent, KlgViewAttachmentComponent, KlgViewApprovalComponent, KlgCurriculumTableComponent, KlgAssociateOrderTableComponent, AddBasicInfoComponent, ApprovalBasicInfoComponent, ReleaseBasicInfoComponent, KlgManageOverviewComponent, KlgMineComponent, KlgBasicComponent, BasicSearchFormComponent, SelectAssociateOrderComponent, ApprovalKnowledgeComponent, ReleaseKnowledgeComponent, ReviewKnowledgeComponent, ViewUnapprovalKnowledgeComponent, ViewUnreleaseKnowledgeComponent, BtnApprovalComponent],
    providers: [
        PublicService,
        KnowledgeService
    ]
})
export class KnowledgeModule { }
