import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUnapprovalKnowledgeComponent } from './view-unapproval-knowledge.component';

describe('ViewUnapprovalKnowledgeComponent', () => {
  let component: ViewUnapprovalKnowledgeComponent;
  let fixture: ComponentFixture<ViewUnapprovalKnowledgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUnapprovalKnowledgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUnapprovalKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
