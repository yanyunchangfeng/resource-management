import {Component, OnInit, ViewChild} from '@angular/core';
import {EditorComponent} from '../../public/editor/editor.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BasicinfoModel} from '../basicinfo.model';
import {Message} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-view-unapproval-knowledge',
  templateUrl: './view-unapproval-knowledge.component.html',
  styleUrls: ['./view-unapproval-knowledge.component.scss']
})
export class ViewUnapprovalKnowledgeComponent implements OnInit {
    @ViewChild(EditorComponent) editor: EditorComponent;
    formTitle = '查看';
    myForm: FormGroup;
    formObj: BasicinfoModel;
    approval: Array<object>;
    ip: string;
    message: Message[] = [];
    switchValue: string = '0';

    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute) {

    }

    ngOnInit() {
        this.formObj = new BasicinfoModel();
        this.initMyForm();
        this.ip = environment.url.management;
    }
    initMyForm() {
        this.myForm = this.fb.group({
            plan_under_time: '',
            plan_finished_time: ''
        });
    }
    goBack() {
        this.router.navigate(['../klgmanageoverview'], {relativeTo: this.activedRouter});
    }
    switch(which) {
        this.switchValue = which;
    }
    publishTopic() {
        let topicContent = this.editor.clickHandle();

        if (!topicContent){
            alert('请输入内容！');
            return;
        }
        alert(topicContent);
    }


}
