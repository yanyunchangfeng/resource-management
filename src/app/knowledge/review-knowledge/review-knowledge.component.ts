import {Component, OnInit, ViewChild} from '@angular/core';
import {EditorComponent} from '../../public/editor/editor.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Message} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {KnowledgeService} from '../knowledge.service';
import {FormobjModel} from '../formobj.model';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/primeng';
import {BasePage} from '../../base.page';

@Component({
  selector: 'app-review-knowledge',
  templateUrl: './review-knowledge.component.html',
  styleUrls: ['./review-knowledge.component.scss']
})
export class ReviewKnowledgeComponent extends BasePage implements OnInit {
    @ViewChild(EditorComponent) editor: EditorComponent;
    formTitle = '审核知识';
    myForm: FormGroup;
    formObj: FormobjModel;
    approval: Array<object>;
    ip: string;
    message: Message[] = [];
    switchValue: string = '0';

    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private knowledgeService: KnowledgeService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new FormobjModel();
        this.initMyForm();
        this.ip = environment.url.management;
        this.getQueryParam();
    }
    initMyForm() {
        this.myForm = this.fb.group({
            plan_under_time: '',
            plan_finished_time: ''
        });
    }
    goBack() {
        this.router.navigate(['../klgmanageoverview'], {relativeTo: this.activedRouter});
    }
    switch(which) {
        this.switchValue = which;
    }
    abolished() {
        this.formObj.auditor = '0';
        this.requestReview();
    }
    submit() {
        this.formObj.auditor = '1';
        this.requestReview();
    }
    requestReview() {
        this.knowledgeService.approveOrReviewKnowledage(this.formObj).subscribe(res => {
            if (res === '00000') {
                this.alert('废止成功');
                window.setTimeout(() => {
                    this.goBack();
                });
            }else {
                this.alert(`废止失败 + &{res}`, 'error');
            }
        });
    }
    getQueryParam = () => {
        this.activedRouter.queryParams.subscribe(data => {
            if (data['kid']) {
                this.knowledgeService.approveOrReviewKnowledage(data['kid']).subscribe(res => {
                    if (res) {
                        this.formObj.kid = data['kid'];
                    }
                });
            }
        });
    }

}
