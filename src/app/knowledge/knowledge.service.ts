import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../services/storage.service';

@Injectable()
export class KnowledgeService {
    ip = environment.url.management;

    constructor(private http: HttpClient,
                private storageService: StorageService) { }


    //  获取热门标签
    getHotTags() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_label'
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

    //  获取最新知识
    getNewKnowledge() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_new'
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }


    //  获取知识库总览列表数据
    //  关键字搜索、热门标签搜索、分类搜索
    getKnowledgeOverview(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_listl',
                'data': {
                    'condition': {
                        'did': obj['did'],
                        'label': obj['label'],
                        'search': obj['search'],
                        'status': obj['status']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }
    //  获取我的知识库记录
    getMineKnowledgeOverview(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_my',
                'data': {
                    'condition': {
                        'search': obj['search']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    //  创建知识库编号
    createKid() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_kid'

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res;
        });
    }
    //  消除知识库编号
    clearKid(kid: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_kid',
                'id': kid
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
    // 获取联动分类接口
    getClassifyDatas(father_did?) {
        (!father_did) && (father_did = '1');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_gettree_byfather',
                'id': father_did
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });

    }

    //  保存知识库
    saveKnowledage(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_save',
                'data': {
                    'kid': obj['kid'],
                    'knowledge_classify': obj['knowledge_classify'],
                    'knowledge_classify_did': obj['knowledge_classify_did'],
                    'classify': obj['classify'],
                    'classify_did':  obj['classify_did'],
                    'catalog': obj['catalog'],
                    'catalog_did': obj['catalog_did'],
                    'approver_name': obj['approver_name'],
                    'approver_pid': obj['approver_pid'],
                    'manager_name':  obj['manager_name'],
                    'manager_pid':  obj['manager_pid'],
                    'label': obj['label'],
                    'title': obj['title'],
                    'content': obj['content'],
                    'attachments': obj['attachments'],
                    'devices': obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
//  提交[状态为空时候]知识库 ： 由没有状态转变为待审批
    submitKnowledage(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_add',
                'data': {
                    'kid': obj['kid'],
                    'knowledge_classify': obj['knowledge_classify'],
                    'knowledge_classify_did': obj['knowledge_classify_did'],
                    'classify': obj['classify'],
                    'classify_did':  obj['classify_did'],
                    'catalog': obj['catalog'],
                    'catalog_did': obj['catalog_did'],
                    'approver_name': obj['approver_name'],
                    'approver_pid': obj['approver_pid'],
                    'manager_name':  obj['manager_name'],
                    'manager_pid':  obj['manager_pid'],
                    'label': obj['label'],
                    'title': obj['title'],
                    'content': obj['content'],
                    'attachments': obj['attachments'],
                    'devices': obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
    //  提交[状态存在时候]知识库 ： 由新建变待审批| 由已发布变待审批
    submitEditKnowledage(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_mod_submit',
                'data': {
                    'kid': obj['kid'],
                    'knowledge_classify': obj['knowledge_classify'],
                    'classify': obj['classify'],
                    'catalog': obj['catalog'],
                    'approver_name': obj['approver_name'],
                    'approver_pid': obj['approver_pid'],
                    'manager_name':  obj['manager_name'],
                    'manager_pid':  obj['manager_pid'],
                    'label': obj['label'],
                    'title': obj['title'],
                    'content': obj['content'],
                    'attachments': obj['attachments'],
                    'devices': obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
    //  保存[状态存在时候]知识库 ： 状态保持不变
    keepSaveKnowledage(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_mod_save',
                'data': {
                    'kid': obj['kid'],
                    'knowledge_classify': obj['knowledge_classify'],
                    'knowledge_classify_did': obj['knowledge_classify_did'],
                    'classify': obj['classify'],
                    'classify_did':  obj['classify_did'],
                    'catalog': obj['catalog'],
                    'catalog_did': obj['catalog_did'],
                    'approver_name': obj['approver_name'],
                    'approver_pid': obj['approver_pid'],
                    'manager_name':  obj['manager_name'],
                    'manager_pid':  obj['manager_pid'],
                    'label': obj['label'],
                    'title': obj['title'],
                    'content': obj['content'],
                    'attachments': obj['attachments'],
                    'devices': obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 根据知识单号获取知识单信息
    getKlgMessage(kid) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_bykid',
                'id': kid
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }
    //  审批、发布、审核接口
    approveOrReviewKnowledage(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_auditor',
                'data': {
                    'kid': obj['kid'],
                    'auditor': obj['auditor'],
                    'remark': obj['remark']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
    // 查看履历
    getCurriculum(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_resume',
                'data': {
                    'condition': {
                        'kid': obj['kid']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }

            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }
    //  删除知识
    deleteKnowledage(array: Array<any>) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_del',
                'ids': array
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 获取关联工单工单类型
    getWorkType() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_workorder_type'
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }
    // 通过工单类型和工单单号获取相关数据
    getDatasByWorkTypeOrKid(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': obj['workorder_type'],
                'data': {
                    'condition': {
                        'sid': obj['kid'],
                        'filter_sids': obj['filter_sids']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }
    // 获取已经关联的工单
    getAssociatedOrders(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_workorder',
                'data': {
                    'condition':  [{
                        'label': '',
                        'value': '',
                        'sid': obj['kid'],
                        'name': '',
                        'status': '',
                        'title': ''
                    }],
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }

            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    //  获取知识分类
    getClaasifyKnowledge() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_get_classify'
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }
    // 根据知识单号获取知识单信息
    getKnowledgeFlowChart(status?: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_workflow_get',
                'data': {
                    'status': status
                }

            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }


// 基础数据↓
    //    新增知识库基础数据
    addklgBasalDatas(name, status, father, dep) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!father) && (father = '');
        (!dep) && (dep = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_tree_add',
                'data': {
                    'name': name,
                    'remark': '',
                    'del': '',
                    'dep': dep,
                    'father': father,
                    'status': status,
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //    编辑/冻结/启用知识库基础数据
    editklgBasalDatas(name, status, did, dep, father) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!did) && (did = '');
        (!dep) && (dep = '');
        (!father) && (father = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_tree_mod',
                'data': {
                    'did': did,
                    'name': name,
                    'remark': '',
                    'del': '',
                    'dep': dep,
                    'father': father,
                    'status': status,
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
    //    删除知识库基础数据
    deleteklgDatas(ids) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_tree_del',
                'ids': ids
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
}
