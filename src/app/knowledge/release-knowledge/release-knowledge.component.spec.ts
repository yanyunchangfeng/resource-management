import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleaseKnowledgeComponent } from './release-knowledge.component';

describe('ReleaseKnowledgeComponent', () => {
  let component: ReleaseKnowledgeComponent;
  let fixture: ComponentFixture<ReleaseKnowledgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReleaseKnowledgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleaseKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
