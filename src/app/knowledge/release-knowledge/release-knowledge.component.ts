import {Component, OnInit, ViewChild} from '@angular/core';
import {EditorComponent} from '../../public/editor/editor.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BasicinfoModel} from '../basicinfo.model';
import {Message} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {FormobjModel} from '../formobj.model';
import {KnowledgeService} from '../knowledge.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/primeng';
import {BasePage} from '../../base.page';

@Component({
  selector: 'app-release-knowledge',
  templateUrl: './release-knowledge.component.html',
  styleUrls: ['./release-knowledge.component.scss']
})
export class ReleaseKnowledgeComponent extends BasePage implements OnInit {
    formTitle = '知识发布';
    myForm: FormGroup;
    formObj: FormobjModel;
    approval: Array<object>;
    ip: string;
    message: Message[] = [];
    switchValue: string = '0';

    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private knowledgeService: KnowledgeService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);

    }

    ngOnInit() {
        this.formObj = new FormobjModel();
        this.initMyForm();
        this.ip = environment.url.management;
        this.getQueryParam();
    }
    initMyForm() {
        this.myForm = this.fb.group({
            plan_under_time: '',
            plan_finished_time: ''
        });
    }
    goBack() {
        this.router.navigate(['../klgmanageoverview'], {relativeTo: this.activedRouter});
    }
    switch(which) {
        this.switchValue = which;
    }
    release() {
        this.formObj.auditor = '1';
        this.requestRelease();
    }

    abolished = () => {
        this.formObj.auditor = '0';
        this.requestRelease();
    };

    requestRelease() {
        this.knowledgeService.approveOrReviewKnowledage(this.formObj).subscribe(res => {
            if (res === '00000') {
                this.alert('操作成功');
                window.setTimeout(() => {
                    this.goBack();
                });
            }else {
                this.alert(`操作失败 + &{res}`, 'error');
            }
        });
    }
    getQueryParam = () => {
        this.activedRouter.queryParams.subscribe(data => {
            if (data['kid']) {
                this.formObj.kid = data['kid'];
                this.getOrderDetails(data);
            }
        });
    }

    getOrderDetails = data => {
        this.knowledgeService.getKlgMessage(data['kid']).subscribe(res => {
            if (res) {
                this.formObj = new FormobjModel(res);
            }
        });
    }

}
