import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {KnowledgeBasalDataComponent} from './knowledge-basal-data/knowledge-basal-data.component';
import {KlgManageChartComponent} from './klg-manage-chart/klg-manage-chart.component';
import {KlgManageOverviewComponent} from './klg-manage-overview/klg-manage-overview.component';
import {AddBasicInfoComponent} from './add-basic-info/add-basic-info.component';
import {KlgBasicComponent} from './klg-basic/klg-basic.component';
import {ApprovalBasicInfoComponent} from './approval-basic-info/approval-basic-info.component';
import {ReleaseBasicInfoComponent} from './release-basic-info/release-basic-info.component';
import {KlgMineComponent} from './klg-mine/klg-mine.component';
import {ApprovalKnowledgeComponent} from './approval-knowledge/approval-knowledge.component';
import {ReleaseKnowledgeComponent} from './release-knowledge/release-knowledge.component';
import {ReviewKnowledgeComponent} from './review-knowledge/review-knowledge.component';
import {ViewUnapprovalKnowledgeComponent} from './view-unapproval-knowledge/view-unapproval-knowledge.component';
import {ViewUnreleaseKnowledgeComponent} from './view-unrelease-knowledge/view-unrelease-knowledge.component';

const routes: Routes = [
    {path: 'basaldata', component: KnowledgeBasalDataComponent},
    {path: 'chart', component: KlgManageChartComponent},
    {path: 'addbasicinfo', component: AddBasicInfoComponent},
    {path: 'klgmine', component: KlgMineComponent},
    {path: 'klgbasic', component: KlgBasicComponent, children: [
        {path: 'klgmanageoverview', component: KlgManageOverviewComponent},
        {path: 'addbasicinfo', component: AddBasicInfoComponent},
        {path: 'approvalbasicinfo', component: ApprovalBasicInfoComponent},
        {path: 'releasebasicinfo', component: ReleaseBasicInfoComponent},
        {path: 'approvalknowledge', component: ApprovalKnowledgeComponent},
        {path: 'releaseknowledge', component: ReleaseKnowledgeComponent},
        {path: 'reviewknowledge', component: ReviewKnowledgeComponent},
        {path: 'viewunapprovalknowledge', component: ViewUnapprovalKnowledgeComponent},
        {path: 'viewunreleaseknowledge', component: ViewUnreleaseKnowledgeComponent}
    ]},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KnowledgeRoutingModule { }
