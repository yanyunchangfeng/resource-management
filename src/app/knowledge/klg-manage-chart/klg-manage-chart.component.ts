import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {KnowledgeService} from '../knowledge.service';
import {EventBusService} from '../../services/event-bus.service';
import {KnowlsearchobjModel} from '../knowlsearchobj.model';

@Component({
  selector: 'app-klg-manage-chart',
  templateUrl: './klg-manage-chart.component.html',
  styleUrls: ['./klg-manage-chart.component.scss']
})
export class KlgManageChartComponent implements OnInit {

    hotTags: Array<object>;
    newKnowledge: any;
    classsifyKnowledge: any;
    searchObj: KnowlsearchobjModel;
    whichComponent: string = 'knowledage';

    constructor(private router: Router,
                private activatedRouter: ActivatedRoute,
                private knowledgeService: KnowledgeService,
                private eventBusService: EventBusService){}

    ngOnInit(){
        this.searchObj = new KnowlsearchobjModel();
        this.initHotTag();
        this.initNewKnowledge();
        this.initClassify();
    }

    refresh(){

    }
    jumperOverView = (key: string) => {
        this.router.navigate(['../klgbasic/klgmanageoverview'], {queryParams: {search: key}, relativeTo: this.activatedRouter});
    }
    jumperOverViewBy = (did: string) => {
        this.router.navigate(['../klgbasic/klgmanageoverview'], {queryParams: {did: did}, relativeTo: this.activatedRouter});
    }

    jumperOverViewByKid = (key: string) => {
        this.router.navigate(['../klgbasic/viewunreleaseknowledge'], {queryParams: {kid: key}, relativeTo: this.activatedRouter});
    }
    jumper(type: string): void{
        (type === 'list') && (this.router.navigate(['../klgbasic/klgmanageoverview'], {relativeTo: this.activatedRouter}));
        (type === 'add') && (this.router.navigate(['../klgbasic/addbasicinfo'], {relativeTo: this.activatedRouter}));
    }
    initHotTag = () => {
        this.knowledgeService.getHotTags().subscribe(res => {
           this.hotTags = res;
           // this.eventBusService.tagcloud.next(res);
        });
    }
    initNewKnowledge = () => {
        this.knowledgeService.getNewKnowledge().subscribe(res =>{
            this.newKnowledge = res;
        });
    }
    initClassify = () => {
        this.knowledgeService.getClaasifyKnowledge().subscribe(res => {
            this.classsifyKnowledge = res;
        });
    }
}
