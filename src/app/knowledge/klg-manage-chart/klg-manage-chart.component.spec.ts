import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgManageChartComponent } from './klg-manage-chart.component';

describe('KlgManageChartComponent', () => {
  let component: KlgManageChartComponent;
  let fixture: ComponentFixture<KlgManageChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgManageChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgManageChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
