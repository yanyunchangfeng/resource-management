import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalBasicInfoComponent } from './approval-basic-info.component';

describe('ApprovalBasicInfoComponent', () => {
  let component: ApprovalBasicInfoComponent;
  let fixture: ComponentFixture<ApprovalBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
