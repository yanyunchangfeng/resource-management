import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-approval-basic-info',
  templateUrl: './approval-basic-info.component.html',
  styleUrls: ['./approval-basic-info.component.scss']
})
export class ApprovalBasicInfoComponent implements OnInit {
    searchStatus: string = '待审批';
    searchType: string = 'all';
    constructor() { }

    ngOnInit() {
    }

}
