import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {FormobjModel} from '../../formobj.model';
import {KnowledgeService} from '../../knowledge.service';
import {EditorViewComponent} from '../../../public/editor-view/editor-view.component';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-klg-view-attachment',
  templateUrl: './klg-view-attachment.component.html',
  styleUrls: ['./klg-view-attachment.component.scss']
})
export class KlgViewAttachmentComponent implements OnInit {
    @ViewChild(EditorViewComponent) editor: EditorViewComponent;
    formObj: FormobjModel;
    ip: string;

    constructor(private router: Router,
                private activedRouter: ActivatedRoute,
                private knowledgeService: KnowledgeService,
                private eventBusService: EventBusService) {
    }
    ngOnInit() {
        this.formObj = new FormobjModel();
        this.ip = environment.url.management;
        this.getQueryParam();
    }
    goBack = () => {
        this.router.navigate(['../klgmanageoverview'], {relativeTo: this.activedRouter});
    }
    getQueryParam = () => {
        this.activedRouter.queryParams.subscribe(data => {
            if (data['kid']){
                this.knowledgeService.getKlgMessage(data['kid']).subscribe(res => {
                    if (res) {
                        this.formObj = new FormobjModel(res);
                        this.setContent(this.formObj.content);
                        if (this.formObj.status) {
                            this.eventBusService.klgFlowCharts.next(this.formObj.status);
                        }
                    }
                });
            }
        });
    }
    setContent = (content) => {
        this.editor.setContent(content);
    }
}
