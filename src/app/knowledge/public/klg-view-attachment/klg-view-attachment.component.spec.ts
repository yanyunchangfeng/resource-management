import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgViewAttachmentComponent } from './klg-view-attachment.component';

describe('KlgViewAttachmentComponent', () => {
  let component: KlgViewAttachmentComponent;
  let fixture: ComponentFixture<KlgViewAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgViewAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgViewAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
