import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgCurriculumTableComponent } from './klg-curriculum-table.component';

describe('KlgCurriculumTableComponent', () => {
  let component: KlgCurriculumTableComponent;
  let fixture: ComponentFixture<KlgCurriculumTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgCurriculumTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgCurriculumTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
