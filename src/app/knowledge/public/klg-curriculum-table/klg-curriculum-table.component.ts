import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {KnowledgeService} from '../../knowledge.service';
import {KnowlsearchobjModel} from '../../knowlsearchobj.model';

@Component({
  selector: 'app-klg-curriculum-table',
  templateUrl: './klg-curriculum-table.component.html',
  styleUrls: ['./klg-curriculum-table.component.scss']
})
export class KlgCurriculumTableComponent implements OnInit {
    @Input() kid;
    searchObj: KnowlsearchobjModel = new KnowlsearchobjModel();
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数

    constructor(private activedRouter: ActivatedRoute,
                private knowledgeService: KnowledgeService) { }

    ngOnInit() {
        this.getQueryParam();
    }

    getQueryParam = () => {
        this.activedRouter.queryParams.subscribe(data => {
            if (data['kid']) {
                this.searchObj.kid = data['kid'];
                this.getDatas();
            }
        });
    }
    getDatas() {
        this.knowledgeService.getCurriculum(this.searchObj).subscribe(res => {
            if (res) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    }
    resetPage(res) {
        if ( 'items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    }
    paginate(event) {
        this.searchObj.page_size = event.rows.toString();
        this.searchObj.page_number = (event.page + 1).toString();
        this.getDatas();
    }

}
