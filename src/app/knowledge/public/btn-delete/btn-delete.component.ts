import {Component, Input, OnInit} from '@angular/core';
import {ConfirmationService} from 'primeng/primeng';
import {KnowledgeService} from '../../knowledge.service';
import {BasePage} from '../../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-btn-delete',
  templateUrl: './btn-delete.component.html',
  styleUrls: ['./btn-delete.component.scss']
})
export class BtnDeleteComponent extends BasePage implements OnInit {
    @Input() public data: any;
    constructor(  public confirmationService: ConfirmationService,
                  private knowledgeService: KnowledgeService,
                  public messageService: MessageService,
                  private eventBusService: EventBusService
                ) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
    }
    delete(data) {
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: () => {
                this.requestDelet();
            },
            reject: () => {

            }
        });
    }
    requestDelet() {
        this.knowledgeService.deleteKnowledage([this.data['kid']]).subscribe(res => {
            if (res === '00000') {
                this.alert('删除成功');
                this.eventBusService.klgdelete.next(true);
            }else {
                this.alert(`删除失败 ${res}`, 'error');
            }
        });
    }
}
