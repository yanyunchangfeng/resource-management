import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgViewApprovalComponent } from './klg-view-approval.component';

describe('KlgViewApprovalComponent', () => {
  let component: KlgViewApprovalComponent;
  let fixture: ComponentFixture<KlgViewApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgViewApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgViewApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
