import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-btn-review',
  templateUrl: './btn-review.component.html',
  styleUrls: ['./btn-review.component.scss']
})
export class BtnReviewComponent implements OnInit {
    @Input() public data: any;
    @Input() public type: string;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        if(this.type === 'mine') {
            this.router.navigate(['../klgbasic/reviewknowledge'], {queryParams: {kid: this.data.kid}, relativeTo: this.activatedRoute });
        }else{
            this.router.navigate(['../reviewknowledge'], {queryParams: {kid: this.data.kid}, relativeTo: this.activatedRoute });
        }
    }
}
