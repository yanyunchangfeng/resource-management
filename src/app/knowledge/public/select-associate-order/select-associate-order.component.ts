import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {EquipObjModel} from '../../../undershelf/equipObj.model';
import {KnowledgeService} from '../../knowledge.service';
import {KnowlsearchobjModel} from '../../knowlsearchobj.model';


@Component({
  selector: 'app-select-associate-order',
  templateUrl: './select-associate-order.component.html',
  styleUrls: ['./select-associate-order.component.scss']
})
export class SelectAssociateOrderComponent implements OnInit, OnChanges {

    @Input() display: boolean;
    @Output() displayEmitter: EventEmitter<any> = new EventEmitter();
    @Output() selectedWorkOrders: EventEmitter<any> = new EventEmitter();
    searchObj: KnowlsearchobjModel;
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数
    selectEquip = [];
    width: string;
    equipObj: EquipObjModel;
    allTypes: Array<any> = [];
    constructor(private knowledgeService: KnowledgeService) {
    }

    ngOnInit() {
        this.selectEquip = [];
        this.searchObj = new KnowlsearchobjModel();
        this.equipObj = new EquipObjModel();
        this.initWidth();
        this.initTypes();
    }
    ngOnChanges(changes: SimpleChanges): void {
        if (changes['display']['currentValue']) {
            this.selectEquip = [];
            this.scheduleDatas = [];
            this.searchObj.kid = '';
        }
    }
    initWidth() {
        let windowInnerWidth = window.innerWidth;
        if (windowInnerWidth < 1024) {
            this.width = (windowInnerWidth * 0.7).toString();
        }else {
            this.width = (windowInnerWidth * 0.7).toString();
        }
    }
    initTypes = () => {
        this.knowledgeService.getWorkType().subscribe(res => {
            this.allTypes = res;
            this.searchObj.workorder_type = res[0]['value']['workorder_value'];
            this.searchObj.workorder_type_name = res[0]['value']['workorder_label'];
        });
    }
    clearSearch() {
        this.searchObj.kid = '';
        this.initTypes();
        this.scheduleDatas = [];
    }
    cancel(){
        this.onHide();
    }
    sure() {
        this.onHide();
        this.selectEquip.map(x => {
            x['workorder_label'] = this.searchObj.workorder_type_name;
            x['workorder_value'] = this.searchObj.workorder_type;
        });
        this.selectedWorkOrders.emit(this.selectEquip);
    }

    onHide() {
        this.display = false;
        this.displayEmitter.emit(this.display);
    }

    search = () => {
       this.getDatas();
    };

    getDatas = () => {
        this.knowledgeService.getDatasByWorkTypeOrKid(this.searchObj).subscribe(res => {
            if (res['items']) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    };
    resetPage(res) {
        if ( 'items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    }
    paginate(event) {
        this.searchObj.page_size = event.rows.toString();
        this.searchObj.page_number = (event.page + 1).toString();
        this.getDatas();
    }
    clickDropdown = () => {
        if (typeof this.searchObj.workorder_type === 'object') {
           this.searchObj.workorder_type_name = this.searchObj.workorder_type['label'];
           this.searchObj.workorder_type = this.searchObj.workorder_type['value'];
        }
    }
}
