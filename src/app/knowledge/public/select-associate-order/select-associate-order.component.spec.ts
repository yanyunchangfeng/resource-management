import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectAssociateOrderComponent } from './select-associate-order.component';

describe('SelectAssociateOrderComponent', () => {
  let component: SelectAssociateOrderComponent;
  let fixture: ComponentFixture<SelectAssociateOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectAssociateOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectAssociateOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
