import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-basic-search-form',
  templateUrl: './basic-search-form.component.html',
  styleUrls: ['./basic-search-form.component.scss']
})
export class BasicSearchFormComponent implements OnInit {

    constructor(private router: Router,
                private activatedRouter: ActivatedRoute){}

    ngOnInit(){
    }
    refresh(){

    }
    jumper(type: string): void{
        (type === 'chart') && (this.router.navigate(['../../chart'], {relativeTo: this.activatedRouter}));
        (type === 'add') && (this.router.navigate(['../addbasicinfo'], {relativeTo: this.activatedRouter}));
    }
    jumperOverView = (key: string) => {
        this.router.navigate(['../../klgbasic/klgmanageoverview'], {queryParams: {search: key}, relativeTo: this.activatedRouter});
    }

}
