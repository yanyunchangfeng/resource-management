import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-btn-edit',
  templateUrl: './btn-edit.component.html',
  styleUrls: ['./btn-edit.component.scss']
})
export class BtnEditComponent implements OnInit {
    @Input() public data: any;
    @Input() public type: string;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        if(this.type === 'mine') {
            switch (this.data.status) {
                case '新建':
                    this.router.navigate(['../klgbasic/addbasicinfo'], {queryParams: {kid: this.data.kid}, relativeTo: this.activatedRoute });
                    break;
                case '已发布':
                    this.router.navigate(['../klgbasic/addbasicinfo'], {queryParams: {kid: this.data.kid}, relativeTo: this.activatedRoute });
            }
        }else {
            switch (this.data.status) {
                case '新建':
                    this.router.navigate(['../addbasicinfo'], {queryParams: {kid: this.data.kid}, relativeTo: this.activatedRoute });
                    break;
                case '已发布':
                    this.router.navigate(['../addbasicinfo'], {queryParams: {kid: this.data.kid}, relativeTo: this.activatedRoute });
            }
        }

    }

}
