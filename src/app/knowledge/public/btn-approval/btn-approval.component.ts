import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {el} from '@angular/platform-browser/testing/src/browser_util';

@Component({
  selector: 'app-btn-approval',
  templateUrl: './btn-approval.component.html',
  styleUrls: ['./btn-approval.component.scss']
})
export class BtnApprovalComponent implements OnInit {
    @Input() public data: any;
    @Input() public type: string;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        if (this.type === 'mine') {
            this.router.navigate([ '../klgbasic/approvalknowledge' ], {queryParams: {kid: this.data['kid']}, relativeTo: this.activatedRoute});
        }else{
            this.router.navigate([ '../approvalknowledge' ], {queryParams: {kid: this.data['kid']}, relativeTo: this.activatedRoute});
        }

    }

}
