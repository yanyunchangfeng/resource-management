import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnApprovalComponent } from './btn-approval.component';

describe('BtnApprovalComponent', () => {
  let component: BtnApprovalComponent;
  let fixture: ComponentFixture<BtnApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
