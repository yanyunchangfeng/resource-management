import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgAssociateOrderTableComponent } from './klg-associate-order-table.component';

describe('KlgAssociateOrderTableComponent', () => {
  let component: KlgAssociateOrderTableComponent;
  let fixture: ComponentFixture<KlgAssociateOrderTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgAssociateOrderTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgAssociateOrderTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
