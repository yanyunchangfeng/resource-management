import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {KnowledgeService} from '../../knowledge.service';
import {ActivatedRoute} from '@angular/router';
import {KnowlsearchobjModel} from '../../knowlsearchobj.model';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-klg-associate-order-table',
  templateUrl: './klg-associate-order-table.component.html',
  styleUrls: ['./klg-associate-order-table.component.scss']
})
export class KlgAssociateOrderTableComponent implements OnInit {

    @Input() scheduleDatas = [];
    @Input() vieAssociateOrder: string = '';
    searchObj: KnowlsearchobjModel;
    selectDeleteItems: Array<any> = [];

    cols: any[] = [];

    constructor(private knowledgeService: KnowledgeService,
                private activedRouter: ActivatedRoute,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.initCols();
        this.searchObj = new KnowlsearchobjModel();
        this.getQueryParam();
        this.eventBusService.klgdeletedevice.subscribe(res => {
            if(res) {
                this.scheduleDatas = res;
            }
        });

    };

    getQueryParam = () => {
        this.activedRouter.queryParams.subscribe(data => {
            if (data['kid']) {
                this.searchObj.kid = data['kid'];
                this.getAssociatedOrders(data['kid']);
            }
        });
    };

    getAssociatedOrders = (kid) => {
        this.knowledgeService.getKlgMessage(kid).subscribe(res => {
            if (res) {
                this.scheduleDatas = res['devices'];
            }
        });
    };

    initCols = () => {
        this.cols = [
            { field: '关联单号', header: '关联单号' },
            { field: '工单类型', header: '工单类型' },
            { field: '标题', header: '标题' },
            { field: '状态', header: '状态' }
        ];
    }


}
