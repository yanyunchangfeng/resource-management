import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-btn-release',
  templateUrl: './btn-release.component.html',
  styleUrls: ['./btn-release.component.scss']
})
export class BtnReleaseComponent implements OnInit {
    @Input() public data: any;
    @Input() public type: string;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
    }

    jumper() {
        if(this.type === 'mine') {
            this.router.navigate([ '../klgbasic/releaseknowledge' ], {queryParams: {kid: this.data['kid']}, relativeTo: this.activatedRoute});
        }else {
            this.router.navigate([ '../releaseknowledge' ], {queryParams: {kid: this.data['kid']}, relativeTo: this.activatedRoute});
        }

    }
}
