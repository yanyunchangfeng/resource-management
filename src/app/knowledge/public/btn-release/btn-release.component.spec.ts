import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnReleaseComponent } from './btn-release.component';

describe('BtnReleaseComponent', () => {
  let component: BtnReleaseComponent;
  let fixture: ComponentFixture<BtnReleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnReleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
