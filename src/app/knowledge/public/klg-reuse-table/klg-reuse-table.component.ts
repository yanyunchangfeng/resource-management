import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {KnowlsearchobjModel} from '../../knowlsearchobj.model';
import {KnowledgeService} from '../../knowledge.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-klg-reuse-table',
  templateUrl: './klg-reuse-table.component.html',
  styleUrls: ['./klg-reuse-table.component.scss']
})
export class KlgReuseTableComponent implements OnInit, OnChanges {

    @Input() searchType;
    @Input() searchStatus;
    searchObj: KnowlsearchobjModel = new KnowlsearchobjModel();
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数

    constructor(private knowledgeService: KnowledgeService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.getQueryParam();
        // this.getOverviewDatas();
        this.eventBusService.klgdelete.subscribe(res => {
            (res) && (this.getOverviewDatas());
        });
    }
    ngOnChanges(changes: SimpleChanges): void {
        if (changes['searchStatus'].currentValue){
           this.searchObj.status =  changes['searchStatus'].currentValue;
            this.getOverviewDatas();
        }
    }
    getQueryParam = () => {
        this.activatedRoute.queryParams.subscribe(res => {
            (res['search']) && (this.searchObj.search = res['search']);
            (res['did']) && (this.searchObj.did = res['did']);
            this.getOverviewDatas();
        });
    }
    getOverviewDatas() {
        (this.searchType === 'all') && (this.getAllDatas());
        (this.searchType === 'mine') && (this.getMineOverViewDatas());
    }
    getMineOverViewDatas() {
        this.knowledgeService.getMineKnowledgeOverview(this.searchObj).subscribe(res => {
            if (res) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    }
    resetPage(res) {
        if ( 'items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    }
    paginate(event) {
        this.searchObj.page_size = event.rows.toString();
        this.searchObj.page_number = (event.page + 1).toString();
        this.getOverviewDatas();
    }
    getAllDatas() {
        this.knowledgeService.getKnowledgeOverview(this.searchObj).subscribe(res => {
            if (res) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    }
    onOperate(data) {
        if ((this.searchType === 'mine')) {
            switch (data.status) {
                case '新建':
                case '待审批':
                    this.router.navigate(['../klgbasic/viewunapprovalknowledge'], {queryParams: {kid: data.kid}, relativeTo: this.activatedRoute});
                    break;
                case '待发布':
                case '已发布':
                case '已废止':
                    this.router.navigate(['../klgbasic/viewunreleaseknowledge'], {queryParams: {kid: data.kid}, relativeTo: this.activatedRoute});

            }
        }else {
            switch (data.status) {
                case '新建':
                case '待审批':
                    this.router.navigate(['../viewunapprovalknowledge'], {queryParams: {kid: data.kid}, relativeTo: this.activatedRoute});
                    break;
                case '待发布':
                case '已发布':
                case '已废止':
                    this.router.navigate(['../viewunreleaseknowledge'], {queryParams: {kid: data.kid}, relativeTo: this.activatedRoute});

            }
        }

    }
}
