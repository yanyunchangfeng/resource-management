import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgReuseTableComponent } from './klg-reuse-table.component';

describe('KlgReuseTableComponent', () => {
  let component: KlgReuseTableComponent;
  let fixture: ComponentFixture<KlgReuseTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgReuseTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgReuseTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
