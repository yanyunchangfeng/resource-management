import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleaseBasicInfoComponent } from './release-basic-info.component';

describe('ReleaseBasicInfoComponent', () => {
  let component: ReleaseBasicInfoComponent;
  let fixture: ComponentFixture<ReleaseBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReleaseBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleaseBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
