import { Component, OnInit } from '@angular/core';
import {KnowlsearchobjModel} from '../knowlsearchobj.model';
import {EventBusService} from '../../services/event-bus.service';
import {KnowledgeService} from '../knowledge.service';

@Component({
  selector: 'app-klg-manage-overview',
  templateUrl: './klg-manage-overview.component.html',
  styleUrls: ['./klg-manage-overview.component.scss']
})
export class KlgManageOverviewComponent implements OnInit {
    searchType: string = 'all'; // 最全总览
    constructor( private eventBusService: EventBusService) { }

    ngOnInit() {
        this.eventBusService.klgFlowCharts.next(false);
    }

}
