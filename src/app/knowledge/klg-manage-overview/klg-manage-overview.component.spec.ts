import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgManageOverviewComponent } from './klg-manage-overview.component';

describe('KlgManageOverviewComponent', () => {
  let component: KlgManageOverviewComponent;
  let fixture: ComponentFixture<KlgManageOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgManageOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgManageOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
