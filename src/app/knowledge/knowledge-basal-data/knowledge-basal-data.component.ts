import { Component, OnInit } from '@angular/core';
import {Message, TreeNode} from 'primeng/primeng';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PublicService} from '../../services/public.service';
import {KnowledgeService} from '../knowledge.service';
import {ConfirmationService} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';
import {BasePage} from '../../base.page';

@Component({
  selector: 'app-knowledge-basal-data',
  templateUrl: './knowledge-basal-data.component.html',
  styleUrls: ['./knowledge-basal-data.component.scss']
})
export class KnowledgeBasalDataComponent extends BasePage implements OnInit {
  totalRecords;
  knowModel =[];
  malfunctionDatas: TreeNode[];      // 数据配置树
    titleName: string;                 // 右边表格标题名
    tableDatas: any;                   // 表格数据
    val1: string;                      // 新增启用
    addDisplay: boolean;               // 新增模态框显示控制
    canAdd: boolean;                   // 是都可以新增
    dataKid: string;                   // 编号
    dataName: string;                  // 新增名称字段
    msgs: Message[] = [];              // 表单验证提示
    nodeFather: string;                // 表格中选中的节点的父节点
    nodeDid: string;                   // 表格中选中的节点的did
    nodeDep: string;                   // 表格中选中的节点Dep
    addOrEdit: string;                 // 新增或者编辑按钮显示哪一个
    did: string;                       // 重新请求表格的需要的did
    dep: string;                       // 重新请求表格的需要的dep
    msgPop: Message[];                 // 操作请求后的提示
    selected: any;                     // 被选中的节点
    title: string;                     // 弹出框标题
    dialogDisplay: boolean;            // 删除框控制
    idsArray = [];                     // 数据
    isDisable: boolean;                // 显示设置
    myForm: FormGroup;

    constructor(
                private publicService: PublicService,
                private fb: FormBuilder,
                private knowledgeService: KnowledgeService,
                public confirmationService: ConfirmationService,
                public messageService: MessageService) {
        super(confirmationService, messageService );

    }

    ngOnInit() {
        this.initForm();
        this.initKlgBasalDatas();
        this.val1 = '启用';
        this.addDisplay = false;
        this.canAdd = true;
        this.dialogDisplay = false;
        this.isDisable = false;
    }
  cancelMask(bool){
    this.addDisplay = false;
    this.msgs =[];
  }
    initKlgBasalDatas = () => {
        this.publicService.getKlgBasalDatas().subscribe(res => {
            this.malfunctionDatas  = res;
            if(res) {
                this.expandAll(this.malfunctionDatas);
            }
        });
    }
    expandAll(treeDatas): void {
        treeDatas.forEach( node => {
            this.expandRecursive(node, true);
        } );
    }
    private expandRecursive(node: TreeNode, isExpand: boolean): void{
        node.expanded = isExpand;
        if (node.children){
            node.children.forEach( childNode => {
                this.expandRecursive(childNode, isExpand);
            } );
        }
    }
    initForm() {
        this.myForm = this.fb.group({
            'nodeKid': '',
            'nodeName': ['', Validators.required],
            'nodeActive': '',
            'nodeUnactive': ''
        });
    }
    get isDirty(): boolean {
        let valid = !this.myForm.controls['nodeName'].untouched && !this.myForm.controls['nodeName'].value;
        let validAgain = this.myForm.controls['nodeName'].untouched && !this.myForm.controls['nodeName'].value;
        return !( valid || validAgain);
    }
    // 组织树选中
    NodeSelect(event) {
        if ( parseInt(event.node.dep) < 3) {
            this.titleName = event.node.name;
            this.nodeFather = event.node.did;
            this.nodeDep = event.node.dep;
            this.did = event.node.did;
            this.dep = event.node.dep;
            this.canAdd = false;
            this.publicService.getChildrenNodeDpFatherID(event.node.did).subscribe(res => {
                this.tableDatas = res;
               this.totalRecords = this.tableDatas.length;
               this.knowModel = this.tableDatas.slice(0, 10);
            });
        }else {
            this.titleName = '';
            this.nodeFather = event.node.did;
            this.nodeDep = event.node.dep;
            this.did = event.node.did;
            this.dep = event.node.dep;
            this.canAdd = true;
            this.tableDatas = [];
        }
    }
  loadCarsLazy(event) {
    setTimeout(() => {
      if (this.tableDatas) {
        this.knowModel = this.tableDatas.slice(event.first, (event.first + event.rows));
      }
    }, 250);
  }
    add() {
        this.myForm.get('nodeKid').disable({onlySelf: true});
        this.myForm.get('nodeName').enable({ onlySelf: true, emitEvent: true});
        this.addDisplay = true;
        this.addOrEdit = 'add';
        this.dataName = '';
        this.dataKid = '';
        this.title = '新增';
    }
    ansureAddDialog() {
        if ( !this.dataName ) {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
        }else {
            this.knowledgeService.addklgBasalDatas(this.dataName, this.val1, this.nodeFather, this.nodeDep).subscribe(res => {
                if (res === '00000') {
                    this.msgPop = [];
                    this.msgPop.push({severity: 'success', summary: '消息提示', detail: '新增成功'});
                    this.refreshDataTable();
                    this.initKlgBasalDatas();
                }else {
                    this.msgPop = [];
                    this.msgPop.push({severity: 'error', summary: '提示消息', detail: '新增失败' + '\n' + res });
                }
            });
            this.addDisplay = false;
        }
    }

    edit(data) {
        this.myForm.get('nodeKid').disable({onlySelf: true});
        this.myForm.get('nodeName').enable({ onlySelf: true, emitEvent: true});
        this.dataKid = data.did;
        this.dataName = data.name;
        this.nodeDid = data.did;
        this.nodeDep = data.dep;
        this.addDisplay = true;
        this.addOrEdit = 'edit';
        this.val1 = data.status;
        this.title = '编辑';
    }

    ansureEditDialog() {
        if ( !this.dataName ) {
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
        }else {
            this.knowledgeService.editklgBasalDatas(this.dataName, this.val1, this.nodeDid, this.nodeDep, this.nodeFather).subscribe(res => {
                if (res === '00000') {
                    this.msgPop = [];
                    this.msgPop.push({severity: 'success', summary: '消息提示', detail: '编辑成功'});
                    this.refreshDataTable();
                    this.initKlgBasalDatas();
                }else {
                    this.msgPop = [];
                    this.msgPop.push({severity: 'error', summary: '提示消息', detail: '编辑失败' + '\n' + res });
                }
            });
            this.addDisplay = false;
        }
    }

    refreshDataTable() {
        this.publicService.getClassifyDatas(this.did).subscribe(res => {
            this.tableDatas = res;
        });
    }

    frezzeOrActive(data) {
        let status = '';
        (data.status === '启用') && ( status = '冻结');
        (data.status === '冻结') && ( status = '启用');
        this.knowledgeService.editklgBasalDatas(data.name, status, data.did, data.dep, data.father).subscribe(res => {
            if (res === '00000') {
                this.msgPop = [];
                this.msgPop.push({severity: 'success', summary: '消息提示', detail: status+'成功'});
                this.refreshDataTable();
                this.initKlgBasalDatas();
            }else {
                this.msgPop = [];
                this.msgPop.push({severity: 'error', summary: '提示消息', detail: status+ '失败' + '\n' + res });
            }
        });
    }
    delete(data) {
        this.dialogDisplay = true;
        this.idsArray = [];
        this.idsArray.push(data.did);
    }
    sureDelete() {
        this.knowledgeService.deleteklgDatas(this.idsArray).subscribe(res => {
            // console.log(res);
            if (res === '00000') {
                this.msgPop = [];
                this.msgPop.push({severity: 'success', summary: '消息提示', detail: '删除成功'});
                this.refreshDataTable();
                this.initKlgBasalDatas();
            }else {
                this.msgPop = [];
                this.msgPop.push({severity: 'error', summary: '提示消息', detail: '删除失败' + '\n' + res });
            }
            this.dialogDisplay = false;
        });
    }
    view(data) {
        // console.log(data);
        this.isDisable = true;
        this.title = '查看';
        this.addDisplay = true;
        this.dataKid = data.did;
        this.dataName = data.name;
        this.val1 = data.status;
        this.addOrEdit = '';
        this.myForm.get('nodeName').disable({onlySelf: true, emitEvent: true});
        this.myForm.get('nodeKid').disable({onlySelf: true, emitEvent: true});
    }

}
