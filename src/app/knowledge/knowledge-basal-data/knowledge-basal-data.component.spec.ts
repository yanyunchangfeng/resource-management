import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnowledgeBasalDataComponent } from './knowledge-basal-data.component';

describe('KnowledgeBasalDataComponent', () => {
  let component: KnowledgeBasalDataComponent;
  let fixture: ComponentFixture<KnowledgeBasalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnowledgeBasalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnowledgeBasalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
