import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KlgBasicComponent } from './klg-basic.component';

describe('KlgBasicComponent', () => {
  let component: KlgBasicComponent;
  let fixture: ComponentFixture<KlgBasicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KlgBasicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlgBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
