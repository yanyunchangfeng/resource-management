import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {KnowledgeService} from '../knowledge.service';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-klg-basic',
  templateUrl: './klg-basic.component.html',
  styleUrls: ['./klg-basic.component.scss']
})
export class KlgBasicComponent implements OnInit {

    flowChartDatas: Array<object> =  [];


    constructor(private router: Router,
                private activedRouter: ActivatedRoute,
                private knowledgeService: KnowledgeService,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.requestFlowChart();
        this.eventBusService.klgFlowCharts.subscribe(status => {
            if (status) {
                this.requestFlowChart(status);
            }else {
                this.requestFlowChart();
            }
        });
    }

    requestFlowChart = (status?) => {
        this.knowledgeService.getKnowledgeFlowChart(status).subscribe(res => {
            this.flowChartDatas = res;
        });
    }
    jumper(num) {
        switch (num.toString()){
            case '1' :
                this.router.navigate(['./addbasicinfo'], {relativeTo: this.activedRouter});
                break;
            case '2':
                this.router.navigate(['./approvalbasicinfo'], {relativeTo: this.activedRouter});
                break;
            case '3':
                this.router.navigate(['./releasebasicinfo'], {relativeTo: this.activedRouter});
        }

    }

}
