export class FormobjModel {
    kid: string;
    knowledge_classify: string;
    knowledge_classify_did: string;
    classify: string;
    classify_did: string;
    catalog: string;
    catalog_did: string;
    approver_name: string;
    approver_pid: string;
    manager_name:  string;
    manager_pid: string;
    label: string;
    title: string;
    content: string;
    attachments: Array<any>;
    devices: Array<any>;
    create_name: string;
    create_time: string;
    status: string;
    auditor: string;
    remark: string;
    approver_remark;

    constructor(obj?) {
        this.kid = obj && obj['kid'] || '';
        this.knowledge_classify = obj && obj['knowledge_classify'] || '';
        this.classify = obj && obj['classify'] || '';
        this.catalog = obj && obj['catalog'] || '';
        this.knowledge_classify_did = obj && obj['knowledge_classify_did'] || '';
        this.classify_did = obj && obj['classify_did'] || '';
        this.catalog_did = obj && obj['catalog_did'] || '';
        this.approver_name = obj && obj['approver_name'] || '';
        this.approver_pid = obj && obj['approver_pid'] || '';
        this.manager_name = obj && obj['manager_name'] || '';
        this.manager_pid = obj && obj['manager_pid'] || '';
        this.label = obj && obj['label'] || '';
        this.title = obj && obj['title'] || '';
        this.content = obj && obj['content'] || '';
        this.attachments = obj && obj['attachments'] || [];
        this.devices = obj && obj['devices'] || [];
        this.create_name = obj && obj['create_name'] || '';
        this.create_time = obj && obj['create_time'] || '';
        this.status = obj && obj['status'] || '';
        this.auditor = obj && obj['auditor'] || '';
        this.remark = obj && obj['remark'] || '';
        this.approver_remark = obj && obj['approver_remark'] || '';
    }

}
