import {Component, OnInit, ViewChild} from '@angular/core';
import {EditorComponent} from '../../public/editor/editor.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {FormobjModel} from '../formobj.model';
import {PUblicMethod} from '../../services/PUblicMethod';
import {KnowledgeService} from '../knowledge.service';
import {BasePage} from '../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/primeng';


@Component({
  selector: 'app-approval-knowledge',
  templateUrl: './approval-knowledge.component.html',
  styleUrls: ['./approval-knowledge.component.scss']
})
export class ApprovalKnowledgeComponent extends  BasePage implements OnInit {
    @ViewChild(EditorComponent) editor: EditorComponent;
    formTitle = '技术审批';
    myForm: FormGroup;
    formObj: FormobjModel;
    approval: Array<object>;
    ip: string;
    message: Message[] = [];
    switchValue: string = '0';
    vieAssociateOrder: string = 'view';

    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private knowledgeService: KnowledgeService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new FormobjModel();
        this.initMyForm();
        this.ip = environment.url.management;
        this.getQueryParam();
    }
    initMyForm() {
        this.myForm = this.fb.group({
            remark: [null, Validators.required]
        });
    }
    goBack() {
        this.router.navigate(['../klgmanageoverview'], {relativeTo: this.activedRouter});
    }
    switch(which) {
        this.switchValue = which;
    }
    isFiledValid(name: string) {
        return PUblicMethod.isFieldValid(this.myForm, name);
    }
    submit(code: string) {
        this.formObj.auditor = code;
        if (this.myForm.valid){
            this.knowledgeService.approveOrReviewKnowledage(this.formObj).subscribe(res => {
                if (res === '00000') {
                    this.alert('操作成功');
                    window.setTimeout(() => {
                        this.goBack();
                    }, 1100);
                }else {
                    this.alert(`操作失败 + &{res}`, 'error');
                }
            });
        }else {
            PUblicMethod.validateAllFormFields(this.myForm);
        }
    }
    getQueryParam = () => {
        this.activedRouter.queryParams.subscribe(data => {
            if (data['kid']) {
                this.formObj.kid = data['kid'];
            }
        });
    }

}
