import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalKnowledgeComponent } from './approval-knowledge.component';

describe('ApprovalKnowledgeComponent', () => {
  let component: ApprovalKnowledgeComponent;
  let fixture: ComponentFixture<ApprovalKnowledgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalKnowledgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
