import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ShelfService} from "../shelf.service";
import {StorageService} from "../../services/storage.service";
import {ConfirmationService} from "primeng/primeng";
import {FormBuilder, Validators} from "@angular/forms";
import {onlytimeValidator, timeValidator} from "../../validator/validators";
import {BasePage} from "../../base.page";
import {MessageService} from "primeng/components/common/messageservice";
import {PUblicMethod} from "../../services/PUblicMethod";


@Component({
  selector: 'app-shelf-application',
  templateUrl: './shelf-application.component.html',
  styleUrls: ['./shelf-application.component.scss']
})
export class ShelfApplicationComponent extends BasePage  implements OnInit {
  shelfForm;
  fb: FormBuilder = new FormBuilder();
  beginTime;
  endTime;
  zh;
  cols;
  submitData;
  initStartTime: Date;
  showAddEquement : boolean = false;
  showAddMask: boolean = false;
  displayLocation: boolean = false;
  showViewDetailMask:boolean = false;
  dataSource = [];
  inspections = [];
  personalData = [];
  displayPersonel: boolean = false;
  brand: string;
  totalRecords;
  selectConstructions = [];
  statusNames;
  categories;
  radioButton = [];
  type;//判断是申请人还是审批人
  viewState;
  tableDatas;
  dropDownData;
  assetshelve_type;
  location = [];
  treeModel={
    oid:""
  }
  nowDate;
  assets;
  state = 'add';
  tempAsset;
  @Input() ano=[]; //过滤单号
  labels = [];
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private shelfService:ShelfService,
    private storageService: StorageService,
    public confirmationService: ConfirmationService,
    public   messageService:MessageService
  ) {
    super(confirmationService,messageService)
  }

  ngOnInit() {
    this.nowDate = new Date();
    this.radioButton = [
      {label:'一级供电',value:'一级供电'},
      {label:'二级供电',value:'二级供电'},
      {label:'三级供电',value:'三级供电'}
    ];
    this.shelfForm = this.fb.group({
      'sid': [''],//上架单号
      'status':[''],//状态
      'creator': ['',Validators.required], //申请人
      'creator_org':[''], //申请人组织
      'creator_pid':[''], //申请人组织
      'assetshelve_type':[''],//上架类型
      'org_approver':[''], //审批人名称
      "usage_time": [''], //预计使用周期
      "assetshelve_location":[''], //计划上架区域
      "preoccupied_sid":[''],//关联预占单号
      "power_level":[''], //供应电极
      "assetshelve_reason":[''], //上架原因
      timeGroup:this.fb.group({
        startTime: ['', Validators.required],
        endTime: ['', Validators.required],
      },{validator:onlytimeValidator}),
    });
    this.beginTime = new Date();
    this.endTime = new Date();
    this.initStartTime = new Date();
    this.zh = new PUblicMethod().initZh();
    this.statusNames = [
      {label:'新建',value:'新建'},
    ];
    this.assetshelve_type = [
      {label:'自有上架',value:'自有上架'},
      {label:'托管上架',value:'托管上架'},
      {label:'故障替换',value:'故障替换'},
    ];
    this.cols = [
      {field: 'name', header: '设备名称'},
      {field: 'system', header: '设备分类'},
      {field: 'modle', header: '设备型号'},
      {field: 'power_jack_position1', header: 'PDU口编号'},
      {field: 'size', header: '高度(U)'},
    ];
    this.submitData = {
      "status": this.statusNames[0]['value'],
      'applicant': "", //申请人
      'creator_pid': "", //申请人账号
      'applicant_pid': "", //申请人账号
      'applicant_org':"", //申请人组织
      'applicant_org_oid':"", //申请人组织账号
      'assetshelve_type':this.assetshelve_type[0]['value'],//上架类型
      'approve':"", //审批人名称
      'approve_pid':"", //审批人账号
      'org_approver_pid':"", //审批人账号
      'assetshelve_plantime_begin':"",//计划上架时间
      "assetshelve_plantime_end": "",//计划完成时间
      "usage_time": "", //预计使用周期
      "assetshelve_location":this.location, //计划上架区域
      "preoccupied_sid":"",//关联预占单号
      "power_level":"", //供应电极
      "assetshelve_reason":"", //上架原因
      "devices":[], //上架设备
    };
  }
  updateOption(currentAsset,state,index){
    this.state =state;
    this.tempAsset= currentAsset;
    console.log(this.tempAsset)
    this.storageService.setCurrentShelf('shelfindex',index);
    this.showAddMask = true;
  }
  viewOption(currentAsset){
    this.tempAsset= currentAsset;
    this.showViewDetailMask = !this.showViewDetailMask
  }
  closeViewDetail(bool){
    this.showViewDetailMask = bool;
  }
  updateShelf(storage){
    let index = this.storageService.getCurrentShelf('shelfindex');
    if(this.viewState ==='update'){
      this.dataSource[index] = storage;
      this.inspections =storage;
      this.inspections = this.dataSource.slice(0,10);
      this.showAddMask = false;
    }
    this.dataSource[index] = storage;
    this.inspections = this.dataSource.slice(0,10);
    for(let  i = 0 ;i<this.inspections.length;i++){
      let temp = this.inspections[i];
      temp['timeGroup'] = temp['room']+'>'+temp['cabinet']+'>'+temp['location'];
    }
    console.log(this.inspections)
    this.showAddMask = false;
  }
  //添加
  showAdd(){
    this.state ='add'
    this.showAddMask = !this.showAddMask;
  }
  removeMask(bool){
    this.showAddMask = bool;
  }
  showLocationMask(type){
    this.type = type;
    this.displayLocation = !this.displayLocation;
  }
  closeAddLocationMask(bool){
    this.displayLocation = bool;
  }
  addDevice(asset){
    this.dataSource = [];
    this.dataSource.push(asset);
    this.totalRecords = this.dataSource.length;
    this.inspections = this.dataSource.slice(0,10);
    for(let  i = 0 ;i<this.inspections.length;i++){
      let temp = this.inspections[i];
      temp['timeGroup'] = temp['room']+'>'+temp['cabinet']+'>'+temp['location'];
    }
    this.alert('添加成功')
  }
  addConstructionOrg(org){
    this.location = org;
    this.submitData.assetshelve_location = [];
    for(let i = 0;i<org.length;i++){
      this.submitData.assetshelve_location.push(org[i]['label'])
    }
    this.submitData.assetshelve_location =  this.submitData.assetshelve_location.join('>');
    this.displayLocation = false;
  }
  goBack() {
    // let sid =  this.detailData['sid'];
    // this.detailData['status_code'] = "";
    // this.eventBusService.updateshelf.next(this.detailData['status_code']);
    this.router.navigate(['../shelfoverview'],{queryParams:{sid:'',state:'',title:'上架总览'},relativeTo:this.route})
  }

  displayEmitter(event) {
    this.displayPersonel = event;
  }
  dataEmitter(event) {
    console.log(event);
    this.displayPersonel = false;
      this.submitData.applicant  = event.name;
      this.submitData.applicant_pid  = event.pid;
      this.submitData.applicant_org  = event.organization;
      this.submitData.applicant_org_oid  = event.oid;
      this.treeModel.oid =  this.submitData.applicant_org_oid;
      this.queryJurList('');
  }

  //查询人员表格数据
  queryJurList(oid){
    this.shelfService.getPersonList(this.treeModel.oid).subscribe(data => {
      this.tableDatas = data;
      this.dropDownData = this.shelfService.DropdownData(this.tableDatas);
      this.submitData.approve = this.dropDownData[0]['value']['name'];
      this.submitData.approve_pid = this.dropDownData[0]['value']['pid'];
    })
  }
  showPersonMask(type){
    this.type = type;
    this.displayPersonel = !this.displayPersonel;
  }
  clearTreeDialog () {
      this.submitData.creator = '';
      this.submitData.creator_org = '';
  }
  showAddEquementMask(){
    this.showAddEquement = !this.showAddEquement;
  }
  closeAddEquementMask(bool){
    this.showAddEquement = bool;
  }
  addDev(metail){
    this.showAddEquement = false;
    for(let i = 0;i<metail.length;i++){
          this.dataSource.push(metail[i]);
        }
    this.totalRecords = this.dataSource.length;
    this.inspections = this.dataSource.slice(0,10);
    // console.log(this.inspections)
        let anos = [];
        for (let i in this.inspections){
          anos.push(this.inspections[i].ano)
        }
        this.ano = anos;
    for(let  i = 0 ;i<this.inspections.length;i++){
      let temp = this.inspections[i];
      temp['timeGroup'] = temp['room']+'>'+temp['cabinet']+'>'+temp['location'];
    }
  }
  loadCarsLazy(event) {
    setTimeout(() => {
      if(this.dataSource) {
        this.inspections = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 0);
  }

  filterApprove = data => {
    if(typeof data === 'object') {
      this.submitData.approve = data['name'];
      this.submitData.approve_pid =  data['pid'];
    }
  }
  // 提交
  addShelfSubmint(submitData,dataSource){
    this.filterApprove(this.submitData.approve);
    submitData.devices = dataSource;
    submitData.assetshelve_location = this.location;
    this.shelfService.addShelf(submitData).subscribe(()=>{
      this.router.navigate(['../shelfoverview'],{queryParams:{title:'上架总览'},relativeTo:this.route})
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  };
  // 保存
  saveShelfSubmint(submitData,dataSource){
    this.filterApprove(this.submitData.approve);
    submitData.devices = dataSource;
    submitData.assetshelve_location = this.location;
    this.shelfService.addSave(submitData).subscribe(()=>{
      this.router.navigate(['../shelfoverview'],{queryParams:{title:'上架总览'},relativeTo:this.route})
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  };
  formSave(){
    this.saveShelfSubmint(this.submitData,this.dataSource)
  }
  formSubmit(){
    this.addShelfSubmint(this.submitData,this.dataSource);
  }
  deleteStorage(){
    for(let i = 0;i<this.selectConstructions.length;i++){
      for(let j = 0;j<this.dataSource.length;j++){
        if(this.selectConstructions[i]['ano']===this.dataSource[j]['ano']){
          this.dataSource.splice(j,1);
          this.inspections = this.dataSource.slice(0,10);
          this.selectConstructions = [];
        }
      }
    }
  }
}
