import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfApplicationComponent } from './shelf-application.component';

describe('ShelfApplicationComponent', () => {
  let component: ShelfApplicationComponent;
  let fixture: ComponentFixture<ShelfApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
