import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplyShelfComponent } from './comply-shelf.component';

describe('ComplyShelfComponent', () => {
  let component: ComplyShelfComponent;
  let fixture: ComponentFixture<ComplyShelfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplyShelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplyShelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
