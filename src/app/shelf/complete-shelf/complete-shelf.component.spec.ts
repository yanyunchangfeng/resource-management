import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteShelfComponent } from './complete-shelf.component';

describe('CompleteShelfComponent', () => {
  let component: CompleteShelfComponent;
  let fixture: ComponentFixture<CompleteShelfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteShelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteShelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
