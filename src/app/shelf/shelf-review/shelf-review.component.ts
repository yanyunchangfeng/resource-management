import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmationService} from "primeng/primeng";
import {ShelfService} from "../shelf.service";
import {StorageService} from "../../services/storage.service";
import {timeValidator} from "../../validator/validators";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-shelf-review',
  templateUrl: './shelf-review.component.html',
  styleUrls: ['./shelf-review.component.scss']
})
export class ShelfReviewComponent implements OnInit {
  shelfForm;
  fb: FormBuilder = new FormBuilder();
  beginTime;
  endTime;
  zh;
  cols;
  submitData;
  initStartTime: Date;
  dataSource = [];
  inspections = [];
  personalData = [];
  displayPersonel: boolean = false;
  brand: string;
  totalRecords;
  selectConstructions = [];
  statusNames;
  categories;
  radioButton = [];
  type;//判断是申请人还是审批人
  viewState;
  tabs;
  asscitedCols;
  OperationLogCols;
  tabIndex = 0;
  detailData = [];
  sid;
  devices;
  optButtons;
  tempAsset;
  showViewDetailMask:boolean = false;
  state;
  showAddMask: boolean = false;
  shelflocation=[];
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private shelfService:ShelfService,
    private storageService: StorageService,
    private confirmationService: ConfirmationService,
    private eventBusService:EventBusService
  ) { }

  ngOnInit() {
    this.tabs = [
      {label:'基本信息'},
      {label:'审批信息'},
      {label:'操作日志'}
    ];

    //审批页面关联设备
    this.asscitedCols = [
      {field: 'approve_time', header: '审批时间'},
      {field: 'approve', header: '审批人'},
      {field: 'approve_status', header: '审批状态'},
      {field: 'approve_opinion', header: '审批意见'},
    ];
    //审批页面操作日志
    this.OperationLogCols = [
      {field: 'operate_time', header: '操作时间'},
      {field: 'operator', header: '操作人'},
      {field: 'operate_content', header: '操作内容'},
      {field: 'operate_description', header: '操作说明'},
    ];
    this.shelfForm = this.fb.group({
      'sid': [''],//上架单号
      'status':[''],//状态
      'creator': ['',Validators.required], //申请人
      'creator_org':[''], //申请人组织
      'assetshelve_type':[''],//上架类型
      'org_approver':[''], //审批人名称
      'assetshelve_plantime_begin':[''],//计划上架时间
      "assetshelve_plantime_end": [''],//计划完成时间
      "usage_time": [''], //预计使用周期
      "assetshelve_location":[''], //计划上架区域
      "preoccupied_sid":[''],//关联预占单号
      "power_level":[''], //供应电极
      "assetshelve_reason":[''], //上架原因
      "next_approver_org":[''], //执行人组织
      "next_approver":[''], //上架执行人
      "assetshelve_acttime_start":[''], //上架开始时间
      "assetshelve_acttime_end":[''], //上架完成时间
      "review_remarks":['',Validators.required], //复核说明
    });
    this.beginTime = new Date();
    this.endTime = new Date();
    this.initStartTime = new Date();

    this.cols = [
      {field: 'name', header: '设备名称'},
      {field: 'system', header: '设备分类'},
      {field: 'modle', header: '设备型号'},
      {field: 'power_jack_position1', header: 'PDU口编号'},
      {field: 'size', header: '高度(U)'},
    ];
    this.submitData = {
      "sid": "",
      "auditor_remarks": "",
      "status": "",
    };
    this.optButtons=[
      {label:'复核不通过',status:'no'},
      {label:'复核通过',status:'yes'},
    ]
      this.queryDetail();
  }
  //添加
  showAdd(type){
    this.showAddMask = !this.showAddMask;
  }
  removeMask(bool){
    this.showAddMask = bool;
  }
  updateOption(currentAsset,state,index){
    this.state =state;
    this.tempAsset= currentAsset;
    console.log(this.tempAsset)
    this.storageService.setCurrentShelf('shelfindex',index);
    this.showAddMask = true;
  }
  updateShelf(storage){
    let index = this.storageService.getCurrentShelf('shelfindex');
    if(this.viewState ==='update'){
      this.dataSource[index] = storage;
      this.devices =storage;
      this.devices = this.dataSource.slice(0,10);
      this.showAddMask = false;
    }
    this.dataSource[index] = storage;
    this.devices = this.dataSource.slice(0,10);
    for(let  i = 0 ;i<this.devices.length;i++){
      let temp = this.devices[i];
      temp['timeGroup'] = temp['room']+'>'+temp['cabinet']+'>'+temp['location'];
      console.log( temp['timeGroup'] )

    }
    this.showAddMask = false;
  }
  viewOption(currentAsset){
    this.tempAsset= currentAsset;
    this.showViewDetailMask = !this.showViewDetailMask
  }
  closeViewDetail(bool){
    this.showViewDetailMask = bool;
  }
  queryDetail(){
    this.route.queryParams.subscribe(parms=>{
      if(parms['sid']){
        this.sid =parms['sid'];
      }
      this.submitData.sid = this.sid;
      this.shelfService.addDetail(this.sid).subscribe(data=>{
        this.detailData = data;
        this.shelflocation = this.detailData['assetshelve_location'];
        let locatios = [];
        for (let i in this.shelflocation){
          locatios.push(this.shelflocation[i].label);
        }
        this.detailData['assetshelve_location'] = locatios;
        this.submitData.devices = this.detailData['devices'];
        this.dataSource = this.detailData['devices']
        for(let  i = 0 ;i<this.dataSource.length;i++){
          let temp = this.dataSource[i];
          temp['timeGroup'] = temp['room']+'>'+temp['cabinet']+'>'+temp['location'];
          console.log(temp['timeGroup'] )
        }
        if(!this.dataSource){
          this.dataSource = [];
          this.totalRecords = 0;
        }
        this.totalRecords = this.dataSource.length;
        this.devices = this.dataSource.slice(0,10);
      })
    });
  }
  goBack() {
    let sid =  this.detailData['sid'];
    this.detailData['status_code'] = "";
    this.eventBusService.updateshelf.next(this.detailData['status_code']);
    this.router.navigate(['../shelfoverview'],{queryParams:{sid:sid,state:'',title:'上架总览'},relativeTo:this.route})
  }
  complyOption(status){
    this.submitData.status = status;
    this.detailData['status_code'] = '';
    this.eventBusService.updateshelf.next(this.detailData['status_code']);
    this.shelfService.shelfReview(this.submitData).subscribe(() =>{
      this.router.navigate(['../shelfoverview'],{relativeTo:this.route});
    })
  }

  loadCarsLazy(event) {
    setTimeout(() => {
      if(this.dataSource) {
        this.inspections = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 0);
  }




  changeTab(index){
    this.tabIndex = index;
  }

}
