import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfReviewComponent } from './shelf-review.component';

describe('ShelfReviewComponent', () => {
  let component: ShelfReviewComponent;
  let fixture: ComponentFixture<ShelfReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
