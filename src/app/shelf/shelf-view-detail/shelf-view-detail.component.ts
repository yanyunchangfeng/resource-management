import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from "primeng/components/common/messageservice";
import {ConfirmationService} from "primeng/api";
import {StorageService} from "../../services/storage.service";
import {ShelfService} from "../shelf.service";
import {BasePage} from "../../base.page";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-shelf-view-detail',
  templateUrl: './shelf-view-detail.component.html',
  styleUrls: ['./shelf-view-detail.component.scss']
})
export class ShelfViewDetailComponent extends BasePage implements OnInit {
  radioButton;
  queryModel;
  detailData =[];
  totalRecords;
  dataSource;
  devices;
  sid;
  cols;
  tabIndex = 0;
  tabs;
  asscitedCols;
  OperationLogCols;
  shelflocation =[];
  tempAsset;
  showViewDetailMask:boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private shelfService:ShelfService,
    private storageService: StorageService,
    public confirmationService: ConfirmationService,
    public   messageService:MessageService,
    private eventBusService:EventBusService
  ) {
    super(confirmationService,messageService)
  }

  ngOnInit() {
    this.tabs = [
      {label:'基本信息'},
      {label:'审批信息'},
      {label:'操作日志'}
    ];
    //审批页面关联设备
    this.asscitedCols = [

      {field: 'approve_time', header: '审批时间'},
      {field: 'approve', header: '审批人'},
      {field: 'approve_status', header: '审批状态'},
      {field: 'approve_opinion', header: '审批意见'},
    ];
    //审批页面操作日志
    this.OperationLogCols = [

      {field: 'operate_time', header: '操作时间'},
      {field: 'operator', header: '操作人'},
      {field: 'operate_content', header: '操作内容'},
      {field: 'operate_description', header: '操作说明'},
    ];
    this.radioButton = [
      {label:'一级供电',value:true},
      {label:'二级供电',value:false},
      {label:'三级供电',value:false}
    ];
    this.queryModel ={
        id:""
    }
    this.cols = [

      {field: 'name', header: '设备名称'},
      {field: 'system', header: '设备分类'},
      {field: 'modle', header: '设备型号'},
      {field: 'power_jack_position1', header: 'PDU口编号'},
      {field: 'size', header: '高度(U)'},
    ];
    this.queryDetail();
  }
  viewOption(currentAsset){
    this.tempAsset= currentAsset;
    this.showViewDetailMask = !this.showViewDetailMask
  }
  closeViewDetail(bool){
    this.showViewDetailMask = bool;
  }
  queryDetail(){
    this.route.queryParams.subscribe(parms=>{
      if(parms['sid']){
        this.sid =parms['sid'];
      }
      this.queryModel.id = this.sid;
      this.shelfService.addDetail(this.sid).subscribe(data=>{
        this.detailData = data;
        this.shelflocation = this.detailData['assetshelve_location'];
        let locatios = [];
        for (let i in this.shelflocation){
          locatios.push(this.shelflocation[i].label);
        }
        this.detailData['assetshelve_location'] = locatios;
        this.dataSource = this.detailData['devices'];
        for(let  i = 0 ;i<this.dataSource.length;i++){
          let temp = this.dataSource[i];
          temp['timeGroup'] = temp['room']+'>'+temp['cabinet']+'>'+temp['location'];
        }
        if(!this.dataSource){
          this.dataSource = [];
          this.totalRecords = 0;
        }
        this.totalRecords = this.dataSource.length;
        this.devices = this.dataSource.slice(0,10);
      })
    });
  }
  goBack() {
    let sid =  this.detailData['sid'];
    this.detailData['status_code'] = "";
    this.eventBusService.updateshelf.next(this.detailData['status_code']);
    this.router.navigate(['../shelfoverview'],{queryParams:{sid:sid,state:'',title:'上架总览'},relativeTo:this.route})
  }
  loadCarsLazy(event) {
    setTimeout(() => {
      if(this.dataSource) {
        this.devices = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 0);
  }
  changeTab(index){
    this.tabIndex = index;
  }
}
