import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfViewDetailComponent } from './shelf-view-detail.component';

describe('ShelfViewDetailComponent', () => {
  let component: ShelfViewDetailComponent;
  let fixture: ComponentFixture<ShelfViewDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfViewDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
