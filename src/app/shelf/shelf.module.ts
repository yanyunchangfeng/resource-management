import { NgModule } from '@angular/core';
import {ShareModule} from '../shared/share.module';
import {ShelfRoutingModule} from './shelf-routing.module';
import { ShelfFlowCharComponent } from './shelf-flow-char/shelf-flow-char.component';
import { ShelfOverviewComponent } from './shelf-overview/shelf-overview.component';
import { ShelfApplicationComponent } from './shelf-application/shelf-application.component';
import { ShelfApprovedComponent } from './shelf-approved/shelf-approved.component';
import { ComplyShelfComponent } from './comply-shelf/comply-shelf.component';
import { ShelfReviewComponent } from './shelf-review/shelf-review.component';
import {ShelfService} from './shelf.service';
import { CompleteShelfComponent } from './complete-shelf/complete-shelf.component';
import { PersonalDialogComponent } from './personal-dialog/personal-dialog.component';
import {PublicService} from '../services/public.service';
import { ShelfViewDetailComponent } from './shelf-view-detail/shelf-view-detail.component';
import { ShelfAddEquipmentComponent } from './shelf-add-equipment/shelf-add-equipment.component';
import { ShelfAddOrUpdateDevicesComponent } from './shelf-add-or-update-devices/shelf-add-or-update-devices.component';
import {EquipmentService} from '../equipment/equipment.service';
import { AddShelfTreeComponent } from './add-shelf-tree/add-shelf-tree.component';
import { ShelfLocationComponent } from './shelf-location/shelf-location.component';
import { ShelfWaitForHandleComponent } from './shelf-wait-for-handle/shelf-wait-for-handle.component';
import { ExecutorComponent } from './executor/executor.component';
import { UpdateApplicatonComponent } from './update-applicaton/update-applicaton.component';
import { UpdateAssetLocationComponent } from './update-asset-location/update-asset-location.component';
import {AssetDetailComponent} from './asset-detail/asset-detail.component';

@NgModule({
  imports: [
    ShareModule,
    ShelfRoutingModule
  ],
  declarations: [
    ShelfFlowCharComponent,
    ShelfOverviewComponent,
    ShelfApplicationComponent,
    ShelfApprovedComponent,
    ComplyShelfComponent,
    ShelfReviewComponent,
    CompleteShelfComponent,
    PersonalDialogComponent,
    ShelfViewDetailComponent,
    ShelfAddEquipmentComponent,
    ShelfAddOrUpdateDevicesComponent,
    AddShelfTreeComponent,
    ShelfLocationComponent,
    ShelfWaitForHandleComponent,
    ExecutorComponent,
    UpdateApplicatonComponent,
    UpdateAssetLocationComponent,
    AssetDetailComponent,
  ],
providers: [ ShelfService, PublicService, EquipmentService]
})
export class ShelfModule { }
