import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EventBusService} from "../../services/event-bus.service";
import {ShelfService} from "../shelf.service";

@Component({
  selector: 'app-shelf-flow-char',
  templateUrl: './shelf-flow-char.component.html',
  styleUrls: ['./shelf-flow-char.component.scss']
})
export class ShelfFlowCharComponent implements OnInit {
  flowCharts;
  title = '上架总览';
  queryModel;
  childrenflowCharts;
  statusData;
  constructor(
    public router:Router,
    public route:ActivatedRoute,
    public eventBus:EventBusService,
    private shelfService:ShelfService
  ) { }

  ngOnInit() {
    this.queryModel={
      "status": "",
    };
    this.flowCharts = [
      {label:'开始',route:'',class:'cursor',status:"",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'0',rx:'10',width:100,height:40,title:'开始',text:{x:'34',y:'24',label:'开始'},g:[{x1:'50',y1:'40',x2:'50',y2:'70',},{x1:'50',y1:'70',x2:'45',y2:'64'},{x1:'50',y1:'70',x2:'55',y2:'64'}]},
      {label:'上架申请',route:'shelfapplication',stroke:"black",fill:'#ffffff',active:false,x:'0',y:'70',width:100,height:40,title:'上架申请',text:{x:'20',y:'94',label:'上架申请'},g:[{x1:'50',y1:'140',x2:'50',y2:'110'},{x1:'50',y1:'140',x2:'45',y2:'134'},{x1:'50',y1:'140',x2:'55',y2:'134'}]},
      {label:'审批',route:'shelfoverview',status:"approve",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'140',width:100,height:40,title:'上架审批',text:{x:'34',y:'164',label:'上架审批'},g:[{x1:'50',y1:'210',x2:'50',y2:'180'},{x1:'50',y1:'210',x2:'45',y2:'204'},{x1:'50',y1:'210',x2:'55',y2:'204'}]},
      {label:'执行上架', route:'shelfoverview',status:"SHELVE_AWAIT",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'210',width:100,height:40,title:'执行上架',text:{x:'20',y:'234',label:'执行上架'},g:[{x1:'50',y1:'280',x2:'50',y2:'250'},{x1:'50',y1:'280',x2:'45',y2:'274'},{x1:'50',y1:'280',x2:'55',y2:'274'}]},
      {label:'上架复核',route:'shelfoverview',status:"auditor",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'280',width:100,height:40,title:'信息复核',text:{x:'20',y:'304',label:'信息复核'},g:[{x1:'50',y1:'350',x2:'50',y2:'320'},{x1:'50',y1:'350',x2:'45',y2:'344'},{x1:'50',y1:'350',x2:'55',y2:'344'}]},
      {label:'上架调整',route:'shelfoverview',status:"adjust",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'350',width:100,height:40,title:'完成上架',text:{x:'20',y:'374',label:'完成上架'},g:[{x1:'50',y1:'420',x2:'50',y2:'390'},{x1:'50',y1:'420',x2:'45',y2:'414'},{x1:'50',y1:'420',x2:'55',y2:'414'}]},
      {label:'信息更新',class:'stroke-dasharray',route:'',status:"",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'420',width:100,height:40,title:'信息更新',text:{x:'20',y:'444',label:'信息更新'},g:[{x1:'50',y1:'490',x2:'50',y2:'460'},{x1:'50',y1:'490',x2:'45',y2:'484'},{x1:'50',y1:'490',x2:'55',y2:'484'}]},
      {label:'结束',route:'',stroke:"black",status:"SHELVE_CLOSE",fill:'#ffffff',active:true,x:'0',y:'490',rx:'10',width:100,height:40,title:'结束',text:{x:'34',y:'514',label:'结束'}},
    ];
    this.childrenflowCharts = [
      {label:'',route:'',stroke:"black",fill:'black',active:true,x:'0',y:'0',title:'开始',text:{x:'30',y:'24',label:'1'}},
      {label:'',route:'',stroke:"black",fill:'#ffffff',active:false,x:'65',y:'70',width:35,height:12,title:'上架申请',text:{x:'80',y:'82',label:'2'}},
      {label:'3',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'140',width:35,height:12,title:'审批',text:{x:'80',y:'152',label:'1'}},
      {label:'1', route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'210',width:35,height:12,title:'执行上架',text:{x:'80',y:'222',label:''}},
      {label:'5',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'280',width:35,height:12,title:'信息复核',text:{x:'80',y:'292',label:'2'}},
      {label:'6',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'350',width:35,height:12,title:'完成上架',text:{x:'80',y:'362',label:'3'}},
      {label:'',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'420',title:'信息更新',text:{x:'80',y:'432',label:'4'}},
      {label:'', route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'490',title:'结束',text:{x:'80',y:'502',label:''}},
    ]

    this.eventBus.updateshelf.subscribe(data=>{
      this.queryModel.status = data;
      this.queryStatusData();
    })
    this.route.queryParams.subscribe(params=>{
      if(params['title']){
        this.title = params['title'];
      }
    });
    this.queryStatusData();
  }
  queryStatusData(){
    this.shelfService.queryflowChar(this.queryModel).subscribe(data=>{
      if(!this.statusData){
        this.statusData = [];
      }
      this.statusData = data;
      for(let i = 0;i<this.statusData.length;i++){
        let temp = this.statusData[i];
        this.flowCharts[i]['fill'] = temp['fill'] ;
      }
      for(let i = 0;i<this.statusData.length;i++){
        let temp = this.statusData[i];
        this.childrenflowCharts[i]['label'] = temp['record'];
      }
    });
  }
  openPage(flow,index){
    if(index+1 === this.flowCharts.length)return
    for(let i = 0;i<this.flowCharts.length;i++){
      this.flowCharts[i].active = false;
    }
    this.flowCharts[index].active = true;
    this.queryStatusData();
    if(flow.status){
      this.eventBus.updateshelf.next(flow.status)
    }
    if(flow.status){
      this.eventBus.updateshelf.next(this.queryModel)
    }
    if(flow.route){
      this.router.navigate([flow.route],{relativeTo:this.route});
      this.title = flow.title;
    }
  }

}
