import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfFlowCharComponent } from './shelf-flow-char.component';

describe('ShelfFlowCharComponent', () => {
  let component: ShelfFlowCharComponent;
  let fixture: ComponentFixture<ShelfFlowCharComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfFlowCharComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfFlowCharComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
