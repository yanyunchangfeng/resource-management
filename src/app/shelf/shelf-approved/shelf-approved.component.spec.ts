import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfApprovedComponent } from './shelf-approved.component';

describe('ShelfApprovedComponent', () => {
  let component: ShelfApprovedComponent;
  let fixture: ComponentFixture<ShelfApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
