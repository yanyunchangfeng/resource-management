import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ShelfService} from "../shelf.service";
import {StorageService} from "../../services/storage.service";
import {ConfirmationService} from "primeng/primeng";
import {timeValidator} from "../../validator/validators";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-shelf-approved',
  templateUrl: './shelf-approved.component.html',
  styleUrls: ['./shelf-approved.component.scss']
})
export class ShelfApprovedComponent implements OnInit {
  shelfForm;
  fb: FormBuilder = new FormBuilder();
  cols;
  approvedModel;
  showAddShelf : boolean = false;
  dataSource = [];
  personalData = [];
  showexecutor: boolean = false;
  brand: string;
  totalRecords;
  selectConstructions = [];
  statusNames;
  categories;
  radioButton = [];
  type;//判断是申请人还是审批人
  viewState;
  tabs;
  asscitedCols;
  OperationLogCols;
  tableDatas;
  executortableDatas;
  tabIndex = 0;
  sid;
  detailData=[];
  devices;
  treeModel;
  dropDownData;
  optButtons;
  executorDownData;
  shelflocation;
  tempAsset;
  state;
  showAddMask:boolean =false;
  showViewDetailMask: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private shelfService:ShelfService,
    private storageService: StorageService,
    private confirmationService: ConfirmationService,
    private eventBusService:EventBusService
  ) { }

  ngOnInit() {
    this.tabs = [
      {label:'基本信息'},
      {label:'审批信息'},
      {label:'操作日志'}
    ];

    //审批页面关联设备
    this.asscitedCols = [
      {field: 'approve_time', header: '审批时间'},
      {field: 'approve', header: '审批人'},
      {field: 'approve_status', header: '审批状态'},
      {field: 'approve_opinion', header: '审批意见'},
    ];
    //审批页面操作日志
    this.OperationLogCols = [
      {field: 'operate_time', header: '操作时间'},
      {field: 'operator', header: '操作人'},
      {field: 'operate_content', header: '操作内容'},
      {field: 'operate_description', header: '操作说明'},
    ];

    this.shelfForm = this.fb.group({
      'sid': [''],//上架单号
      'status':[''],//状态
      'creator': ['',Validators.required], //申请人
      'creator_org':[''], //申请人组织
      'assetshelve_type':[''],//上架类型
      'org_approver':[''], //审批人名称
      'assetshelve_plantime_begin':[''],//计划上架时间
      "assetshelve_plantime_end": [''],//计划完成时间
      "usage_time": [''], //预计使用周期
      "assetshelve_location":[''], //计划上架区域
      "preoccupied_sid":[''],//关联预占单号
      "power_level":[''], //供应电极
      "assetshelve_reason":[''], //上架原因
      "approve_remarks":['',Validators.required], //审批意见
      "next_approver_org":[''], //下一审批角色
      "next_approver":[''], //下一神审批人
      "executor_org":['',Validators.required], //执行人组织
      "executor":[''], //下一神审批人
    });
    this.statusNames = [
      {label:'新建',value:'新建'},
    ]

    this.cols = [
      {field: 'name', header: '设备名称'},
      {field: 'system', header: '设备分类'},
      {field: 'modle', header: '设备型号',editable:true},
      {field: 'power_jack_position1', header: 'PDU口编号'},
      {field: 'size', header: '高度(U)'},
    ];
    this.approvedModel = {
      "sid": "",
      "approve_remarks": "",
      "status": "",
      "next_approve": "",
      "next_approve_pid": "",
      "next_approve_org": "",
      "next_approve_org_oid": "",
      "executor": "",
      "executor_pid": "",
      "executor_org": "",
      "executor_org_oid": "",
      "devices": [],

    };
       this.treeModel={
         oid:""
       }
    this.optButtons=[
      {label:'驳回',status:'reject'},
      {label:'拒绝',status:'no'},
      {label:'通过',status:'yes'},
    ]
    this.queryDetail();
  }
  viewOption(currentAsset){
    this.tempAsset= currentAsset;
    this.showViewDetailMask = !this.showViewDetailMask
  }
  closeViewDetail(bool){
    this.showViewDetailMask = bool;
  }
  queryDetail(){
    this.route.queryParams.subscribe(parms=>{
      if(parms['sid']){
        this.sid =parms['sid'];
      }
      this.approvedModel.sid = this.sid;
      this.shelfService.addDetail(this.sid).subscribe(data=>{
        this.detailData = data;
        this.approvedModel.devices = this.detailData['devices']
        this.shelflocation = this.detailData['assetshelve_location'];
        let locatios = [];
        for (let i in this.shelflocation){
          locatios.push(this.shelflocation[i].label)
        }
        this.detailData['assetshelve_location'] = locatios
        this.dataSource = this.detailData['devices']
        for(let  i = 0 ;i<this.dataSource.length;i++){
          let temp = this.dataSource[i];
          temp['timeGroup'] = temp['room']+'>'+temp['cabinet']+'>'+temp['location'];
          console.log( temp['timeGroup'] )
        }
        if(!this.dataSource){
          this.dataSource = [];
          this.totalRecords = 0;
        }
        this.totalRecords = this.dataSource.length;
        this.devices = this.dataSource.slice(0,10);
      })
    });
  }
  updateOption(currentAsset,state,index){
    this.state =state;
    this.tempAsset= currentAsset;
    console.log(this.tempAsset)
    this.storageService.setCurrentShelf('shelfindex',index);
    this.showAddMask = true;
  }

  updateShelf(storage){
    let index = this.storageService.getCurrentShelf('shelfindex');
    if(this.viewState ==='update'){
      this.dataSource[index] = storage;
      this.devices =storage;
      this.devices = this.dataSource.slice(0,10);
      this.showAddMask = false;
    }
    this.dataSource[index] = storage;
    this.devices = this.dataSource.slice(0,10);
    for(let  i = 0 ;i<this.devices.length;i++){
      let temp = this.devices[i];
      temp['timeGroup'] = temp['room']+'>'+temp['cabinet']+'>'+temp['location'];
      console.log( temp['timeGroup'] )

      // this.detailData['devices']=temp['timeGroup']
    }
    this.showAddMask = false;
  }
  //添加
  showAdd(type){
    this.showAddMask = !this.showAddMask;
  }
  removeMask(bool){
    this.showAddMask = bool;
  }
  goBack() {
    let sid =  this.detailData['sid'];
    this.detailData['status_code'] = "";
    this.eventBusService.updateshelf.next(this.detailData['status_code']);
    this.router.navigate(['../shelfoverview'],{queryParams:{sid:sid,state:'',title:'上架总览'},relativeTo:this.route})
  }
  //添加组织人员
  addPlanOrg(org){
    this.approvedModel.next_approve_org_oid=[] ;
    this.approvedModel.next_approve_org = [];
    this.treeModel.oid =  this.approvedModel.next_approve_org_oid;
    for(let i = 0;i<org.length;i++){
      this.approvedModel.next_approve_org_oid.push(org[i]['oid'])
      this.approvedModel.next_approve_org.push(org[i]['label'])
    }
    this.approvedModel.next_approve_org =  this.approvedModel.next_approve_org.join(',');
    this.showAddShelf = false;
    this.queryJurList('');
  }
//查询人员表格数据
  queryJurList(oid){
    this.shelfService.getJurList(this.treeModel.oid).subscribe(data => {
      this.tableDatas = data;

      this.dropDownData = this.shelfService.DropdownData(this.tableDatas);
      console.log(this.dropDownData)
      this.approvedModel.next_approve = this.dropDownData[0]['value']['name'];
      this.approvedModel.next_approve_pid = this.dropDownData[0]['value']['pid'];

    })
  }
  //执行人组织人员
  addExecutorOrg(org){
    this.approvedModel.executor_org_oid=[] ;
    this.approvedModel.executor_org = [];
    this.treeModel.oid =  this.approvedModel.executor_org_oid;
    for(let i = 0;i<org.length;i++){
      this.approvedModel.executor_org_oid.push(org[i]['oid'])
      this.approvedModel.executor_org.push(org[i]['label'])
    }
    this.approvedModel.executor_org =  this.approvedModel.executor_org.join(',');
    this.showexecutor = false;
    this.queryExecutorList('');
  }
//查询执行人表格数据
  queryExecutorList(oid){
    this.shelfService.getJurList(this.treeModel.oid).subscribe(data => {
      this.executortableDatas = data;
      this.executorDownData = this.shelfService.executorDropdownData(this.executortableDatas);
      console.log(this.executorDownData)
      this.approvedModel.executor = this.executorDownData[0]['value']['name'];
      this.approvedModel.executor_pid = this.executorDownData[0]['value']['pid'];
      console.log( this.approvedModel.executor )
      console.log(this.approvedModel.executor_pid)


    })
  }
  showShelfMask(){
    this.showAddShelf = !this.showAddShelf;
  }
  closeShelfMask(bool){
    this.showAddShelf = bool;
  }
  showExecutorMask(){
    this.showexecutor = !this.showexecutor;
  }
  closeExecutorMask(bool){
    this.showexecutor = bool;
  }
  clearTreeDialog(){
    this.approvedModel.next_approve_org ='';
    this.dropDownData ='';
  }
  clearExecutorDialog(){
    this.approvedModel.executor_org ='';
    this.executorDownData = '';
  }
  filterApprove = data => {
    if(typeof data === 'object') {
      this.approvedModel.next_approve = data['name'];
      this.approvedModel.next_approve_pid =  data['pid'];
      this.approvedModel.executor = data['name'];
      this.approvedModel.executor_pid =  data['pid'];
    }
  }
  filterExecutor = data => {
    if(typeof data === 'object') {
      this.approvedModel.executor = data['name'];
      this.approvedModel.executor_pid =  data['pid'];
    }
  }
  //审批
  approvedOption(status){
    this.approvedModel.status = status;
    this.detailData['status_code'] = '';
    this.eventBusService.updateshelf.next(this.detailData['status_code']);
    this.filterApprove(this.approvedModel.next_approve);
    this.filterExecutor(this.approvedModel.executor);
    this.shelfService.shelfSpproved(this.approvedModel).subscribe(() =>{
      this.router.navigate(['../shelfoverview'],{relativeTo:this.route});
    })
  }
  changeTab(index){
    this.tabIndex = index;
  }
}
