import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ShelfService} from "../shelf.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-shelf-add-equipment',
  templateUrl: './shelf-add-equipment.component.html',
  styleUrls: ['./shelf-add-equipment.component.css']
})
export class ShelfAddEquipmentComponent implements OnInit {
  display: boolean = false;
  @Output() closeEquement = new EventEmitter;
  cols;
  width;
  dataSource;
  totalRecords;
  assets;
  queryModel;
  @Output() addDev = new EventEmitter();
  selectMaterial = [];
  @Input() equpano=[];
  constructor(
    private shelfService: ShelfService,
    private comfirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.queryModel ={
      "condition": {
        "ano": "",
        "name": "",
        "status": "",
        "on_line_date_start": "",
        "on_line_date_end": "",
        "room": "",
        "filter_anos": this.equpano,
        "filter_rooms": []
      },
      "page": {
        "page_size": "10",
        "page_number": "1"
      }
    }
    this.display = true;
    if(window.innerWidth<1024){
      this.width = window.innerWidth * 0.7;
    }else{
      this.width = window.innerWidth * 0.6;
    }
    this.cols = [
      {field: 'ano', header: '设备编号'},
      {field: 'name', header: '设备名称'},
      {field: 'modle', header: '设备型号'},
      {field: 'location', header: '设备位置'},
      {field: 'manager', header: '责任人'},
      {field: 'status', header: '设备状态'},

    ];
    this.queryAssets();

  }
  closeEquementMask(bool){
    this.closeEquement.emit(bool)
  }
  clearSearch() {
    this.queryModel.condition.name = '';
    this.queryModel.condition.ano = '';
    this.queryModel.condition.status = '';

  }
  // //搜索功能
  suggestInsepectiones() {
    this.queryModel.page.page_number = '1'
    this.queryModel.page.page_size = '10';
    this.queryAssets()
  }

  //分页查询
  paginate(event) {
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryAssets();
  }

  queryAssets(){
    this.shelfService.getFillerAssets(this.queryModel).subscribe(data =>{
      if (!this.dataSource) {
        this.dataSource = [];
        this.selectMaterial = [];
      }
      this.dataSource = data.items;
      console.log(this.dataSource)
      this.totalRecords = data.page.total;
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status === 0|| JSON.parse(JSON.stringify(err)).status === 504){
        message = '似乎网络出现了问题，请联系管理员或稍后重试'
      }else{
        message =err
      }
      this.comfirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
  formSubmit(bool){
    this.addDev.emit(this.selectMaterial);
    // this.closeEquementMask(false);
  }

}
