import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfAddEquipmentComponent } from './shelf-add-equipment.component';

describe('ShelfAddEquipmentComponent', () => {
  let component: ShelfAddEquipmentComponent;
  let fixture: ComponentFixture<ShelfAddEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfAddEquipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfAddEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
