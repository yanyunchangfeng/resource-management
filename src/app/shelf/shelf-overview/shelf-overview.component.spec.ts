import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfOverviewComponent } from './shelf-overview.component';

describe('ShelfOverviewComponent', () => {
  let component: ShelfOverviewComponent;
  let fixture: ComponentFixture<ShelfOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
