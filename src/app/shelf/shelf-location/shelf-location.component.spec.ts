import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfLocationComponent } from './shelf-location.component';

describe('ShelfLocationComponent', () => {
  let component: ShelfLocationComponent;
  let fixture: ComponentFixture<ShelfLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
