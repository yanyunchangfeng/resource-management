import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {CabinetModel} from "../../capacity/cabinet.model";
import {PublicService} from "../../services/public.service";
import { TreeNode} from "primeng/primeng";


@Component({
  selector: 'app-shelf-location',
  templateUrl: './shelf-location.component.html',
  styleUrls: ['./shelf-location.component.scss']
})
export class ShelfLocationComponent implements OnInit {
  width;
  windowSize;
  queryModel;
  @ViewChild('expandingTree')
  @Output() closeAddMask = new EventEmitter();
  @Output() addTree = new EventEmitter();
  selected: TreeNode[];
  display: boolean;
  orgs: TreeNode[];                      // 组织树的数据
  treeDatas = [];
  cabinetObj: CabinetModel;
  // 表格数据
  @Output() displayEmitter = new EventEmitter();
  @Output() closeLocation = new EventEmitter;
  eventData: any;

  constructor(private publicService: PublicService) { }

  ngOnInit() {
    this.cabinetObj = new CabinetModel();
    this.display = true;
    // 查询组织树
    this.queryModel = {
      'father_did': "",
      'sp_type_min': ""
    }
    this.initTreeDatas();
  }
  handleRowSelect(event) {
    this.eventData = event.data;
    console.log(this.eventData)

  }
  closeLocationMask() {
    this.closeLocation.emit(false);
  }
  initTreeDatas() {
    this.publicService.getCapBasalDatas('', this.cabinetObj.sp_type).subscribe(res => {
      this.treeDatas = res;
      console.log(this.treeDatas)
    });
  }
  formSubmit(){
    let arr = []
    for(let key of this.selected){
      let obj={};
      obj['label']=key['label'];
      obj['did']=key['did'];
      arr.push(obj)
    }
    console.log(arr)
    this.addTree.emit(arr);
  }

}
