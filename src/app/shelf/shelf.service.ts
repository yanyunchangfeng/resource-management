import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {StorageService} from "../services/storage.service";
import {environment} from "../../environments/environment";

@Injectable()
export class ShelfService {

  constructor(private http:HttpClient,
              private storageService:StorageService) { }
  //上架管理总览接口
  shelfOverview(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_get",
      "data": {
      "condition": {
        "sid": shelf.condition.sid.trim(),
        "status": shelf.condition.status.trim(),
        "applicant": shelf.condition.applicant.trim(),
     },
      "page": {
        "page_size":shelf.page.page_size.trim(),
        "page_number": shelf.page.page_number.trim()
        }
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
    }
  //上架待我处理总览接口
  shelfWaitOverview(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_get_byowner",
      "data": {
      "condition": {
        "status": shelf.condition.status,
     },
      "page": {
        "page_size":shelf.page.page_size.trim(),
        "page_number": shelf.page.page_number.trim()
        }
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
    }
  //上架申请新增接口
  addShelf(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_add",
      "data": {
        "status":shelf.status,  // "申请人名称",
        "applicant":shelf.applicant,  // "申请人名称",
        "applicant_pid":shelf.applicant_pid,// "申请人账号",
        "applicant_org":shelf.applicant_org,// "申请人组织名称",
        "applicant_org_oid":shelf.applicant_org_oid,// "申请人组织账号",
        "approve": shelf.approve,//"审批人名称",
        "approve_pid":shelf.approve_pid,// "审批人账号",
        "approve_org":shelf.approve_org,// "审批人组织名称",
        "approve_org_pid": shelf.approve_org_pid,//"审批人组织账号",
        "executor":shelf.executor,// "执行人名称",
        "executor_pid": shelf.executor_pid,//"执行人账号",
        "executor_org": shelf.executor_org,//"执行人组织名称",
        "executor_org_oid": shelf.executor_org_oid,//"执行人组织编号",
        "assetshelve_plantime_begin":shelf.assetshelve_plantime_begin,// "计划上架时间",
        "assetshelve_plantime_end":shelf.assetshelve_plantime_end,// "计划完成时间",
        "assetshelve_acttime_begin": shelf.assetshelve_acttime_begin,//"上架开始时间",
        "assetshelve_acttime_end":shelf.assetshelve_acttime_end,// "上架完成时间",
        "assetshelve_type":shelf.assetshelve_type,// "上架类型",
        "assetshelve_reason": shelf.assetshelve_reason,//"上架原因",
        "preoccupied_sid":shelf.preoccupied_sid,// "关联预占单号",
        "power_level": shelf.power_level,//"供电等级",
        "usage_time": shelf.usage_time,//"预计使用周期",
        "assetshelve_location": shelf.assetshelve_location,//"设备位置",
        "devices": shelf.devices,//"设备",
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
    }
  //上架申请保存接口
  addSave(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_save",
      "data": {
        "status":shelf.status,  // "申请人名称",
        "applicant":shelf.applicant,  // "申请人名称",
        "applicant_pid":shelf.applicant_pid,// "申请人账号",
        "applicant_org":shelf.applicant_org,// "申请人组织名称",
        "applicant_org_oid":shelf.applicant_org_oid,// "申请人组织账号",
        "approve": shelf.approve,//"审批人名称",
        "approve_pid":shelf.approve_pid,// "审批人账号",
        "approve_org":shelf.approve_org,// "审批人组织名称",
        "approve_org_pid": shelf.approve_org_pid,//"审批人组织账号",
        "executor":shelf.executor,// "执行人名称",
        "executor_pid": shelf.executor_pid,//"执行人账号",
        "executor_org": shelf.executor_org,//"执行人组织名称",
        "executor_org_oid": shelf.executor_org_oid,//"执行人组织编号",
        "assetshelve_plantime_begin":shelf.assetshelve_plantime_begin,// "计划上架时间",
        "assetshelve_plantime_end":shelf.assetshelve_plantime_end,// "计划完成时间",
        "assetshelve_acttime_begin": shelf.assetshelve_acttime_begin,//"上架开始时间",
        "assetshelve_acttime_end":shelf.assetshelve_acttime_end,// "上架完成时间",
        "assetshelve_type":shelf.assetshelve_type,// "上架类型",
        "assetshelve_reason": shelf.assetshelve_reason,//"上架原因",
        "preoccupied_sid":shelf.preoccupied_sid,// "关联预占单号",
        "power_level": shelf.power_level,//"供电等级",
        "usage_time": shelf.usage_time,//"预计使用周期",
        "assetshelve_location": shelf.assetshelve_location,//"设备位置",
        "devices": shelf.devices,//"设备",
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
    }
  //上架申请修改保存接口
  updateSave(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_mod_save",
      "data": {
        "sid":shelf.sid,  // "单号",
        "status":shelf.status,  // "申请人名称",
        "applicant":shelf.applicant,  // "申请人名称",
        "applicant_pid":shelf.applicant_pid,// "申请人账号",
        "applicant_org":shelf.applicant_org,// "申请人组织名称",
        "applicant_org_oid":shelf.applicant_org_oid,// "申请人组织账号",
        "approve": shelf.approve,//"审批人名称",
        "approve_pid":shelf.approve_pid,// "审批人账号",
        "approve_org":shelf.approve_org,// "审批人组织名称",
        "approve_org_pid": shelf.approve_org_pid,//"审批人组织账号",
        "executor":shelf.executor,// "执行人名称",
        "executor_pid": shelf.executor_pid,//"执行人账号",
        "executor_org": shelf.executor_org,//"执行人组织名称",
        "executor_org_oid": shelf.executor_org_oid,//"执行人组织编号",
        "assetshelve_plantime_begin":shelf.assetshelve_plantime_begin,// "计划上架时间",
        "assetshelve_plantime_end":shelf.assetshelve_plantime_end,// "计划完成时间",
        "assetshelve_acttime_begin": shelf.assetshelve_acttime_begin,//"上架开始时间",
        "assetshelve_acttime_end":shelf.assetshelve_acttime_end,// "上架完成时间",
        "assetshelve_type":shelf.assetshelve_type,// "上架类型",
        "assetshelve_reason": shelf.assetshelve_reason,//"上架原因",
        "preoccupied_sid":shelf.preoccupied_sid,// "关联预占单号",
        "power_level": shelf.power_level,//"供电等级",
        "usage_time": shelf.usage_time,//"预计使用周期",
        "assetshelve_location": shelf.assetshelve_location,//"设备位置",
        "devices": shelf.devices,//"设备",
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
    }
  //上架申请修改提交接口
  updateSubmit(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_mod_submit",
      "data": {
        "sid":shelf.sid,  // "单号",
        "status":shelf.status,  // "申请人名称",
        "applicant":shelf.applicant,  // "申请人名称",
        "applicant_pid":shelf.applicant_pid,// "申请人账号",
        "applicant_org":shelf.applicant_org,// "申请人组织名称",
        "applicant_org_oid":shelf.applicant_org_oid,// "申请人组织账号",
        "approve": shelf.approve,//"审批人名称",
        "approve_pid":shelf.approve_pid,// "审批人账号",
        "approve_org":shelf.approve_org,// "审批人组织名称",
        "approve_org_pid": shelf.approve_org_pid,//"审批人组织账号",
        "executor":shelf.executor,// "执行人名称",
        "executor_pid": shelf.executor_pid,//"执行人账号",
        "executor_org": shelf.executor_org,//"执行人组织名称",
        "executor_org_oid": shelf.executor_org_oid,//"执行人组织编号",
        "assetshelve_plantime_begin":shelf.assetshelve_plantime_begin,// "计划上架时间",
        "assetshelve_plantime_end":shelf.assetshelve_plantime_end,// "计划完成时间",
        "assetshelve_acttime_begin": shelf.assetshelve_acttime_begin,//"上架开始时间",
        "assetshelve_acttime_end":shelf.assetshelve_acttime_end,// "上架完成时间",
        "assetshelve_type":shelf.assetshelve_type,// "上架类型",
        "assetshelve_reason": shelf.assetshelve_reason,//"上架原因",
        "preoccupied_sid":shelf.preoccupied_sid,// "关联预占单号",
        "power_level": shelf.power_level,//"供电等级",
        "usage_time": shelf.usage_time,//"预计使用周期",
        "assetshelve_location": shelf.assetshelve_location,//"设备位置",
        "devices": shelf.devices,//"设备",
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
    }
  //删除接口
  delete(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_del",
        "ids":shelf,
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
    }
  //上架查看获取接口
  addDetail(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_get_byid",
        "id":shelf,
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
    }
  //  流程图获取接口
  queryflowChar(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_workflow_get",
      "data": {
        "status": shelf.status.trim(),
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
    }
  //  所有状态获取接口
  queryStatus(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_statuslist_get",
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
    }
  //  上架审批接口
  shelfSpproved(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_approve",
      "data": {
        "sid":shelf.sid,
        "approve_remarks":shelf.approve_remarks,
        "status":shelf.status,
        "next_approve":shelf.next_approve,
        "next_approve_pid":shelf.next_approve_pid,
        "next_approve_org": shelf.next_approve_org,
        "next_approve_org_oid":shelf.next_approve_org_oid+"",
        "executor":shelf.executor,
        "executor_pid":shelf.executor_pid,
        "executor_org":shelf.executor_org+"",
        "executor_org_oid":shelf.executor_org_oid+"",
        "devices":shelf.devices,
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //  上架执行接口
  shelfComply(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_process",
      "data": {
        "sid":shelf.sid,
        "status":shelf.status,
        "assetshelve_acttime_begin":shelf.assetshelve_acttime_start,
        "assetshelve_acttime_end":shelf.assetshelve_acttime_end,
        "devices":shelf.devices,
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //  完成上架接口
  shelfComplate(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_finish",
      "data": {
        "sid":shelf.sid,
        "devices":shelf.devices,
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //  上架复核接口
  shelfReview(shelf){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "assetshelve_review",
      "data": {
        "sid":shelf.sid,
        "auditor_remarks":shelf.auditor_remarks,
        "status":shelf.status,
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //获取当前登录人接口
  queryPersonal(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`,{
      "access_token":token,
      "type":"get_personnel_user",
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //获取人员数据
  getPersonList(oid){
    let token = this.storageService.getToken('token');
    let oids = [];
    if(oid){
      oids.push(oid)
    }
    return this.http.post(`${environment.url.management}/personnel`, {
      "access_token": token,
      "type": "get_personnel_byoid",
      "id": [],
      "oid": oids
    }).map((res: Response) => {
      if(res['errcode'] == '00001'){//没有人员
        return [];
      }else if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }

//  总览状态下拉框转换
  formatDropdownData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = temp['code'];
      newarr.push(obj);
    }
    return newarr
  }
//  人员表格下拉框转换
  DropdownData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = {
        name: temp['name'],
        pid:temp['pid']
      };
      newarr.push(obj);
    }
    return newarr
  }
//  人员表格下拉框转换
  executorDropdownData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = {
        name: temp['name'],
        pid:temp['pid']
      };
      newarr.push(obj);
    }
    return newarr
  }



  //查询资产
  getAssets() {
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/asset`, {
      "access_token":token,
      "type": "asset_get"
    }).map((res:Response) => {
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
//  可过滤资产
  getFillerAssets(shelf) {
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token":token,
      "type": "workflow_asset_get",
      "data":{
        "condition": {
          "ano": shelf.condition.ano,
          "name": shelf.condition.name,
          "status": shelf.condition.status,
          "on_line_date_start":shelf.condition.on_line_date_start,
          "on_line_date_end": shelf.condition.on_line_date_end,
          "room":shelf.condition.room,
          "filter_anos":shelf.condition.filter_anos,
          "filter_rooms": shelf.condition.filter_rooms,
        },
        "page": {
          "page_size": shelf.page.page_size,
          "page_number":shelf.page.page_number
        }
      }

    }).map((res:Response) => {
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
//时间格式化
  getFormatTime(date?) {
    if (!date){
      date= new Date();
    }else{
      date = new Date(date)
    }
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let hour = date.getHours();
    let minutes = date.getMinutes();
    let second = date.getSeconds();
    day = day <= 9 ? "0" + day : day;
    month = month <= 9 ? "0" + month : month;
    hour = hour <= 9 ? "0" + hour : hour;
    minutes = minutes <= 9 ? "0" + minutes : minutes;
    second = second <= 9 ? "0" + second : second;
    return  year+"-"+month+"-" +day+" " +hour+":"+minutes+":"+second

  }
  //  获取组织树
  getDepartmentDatas(oid?, dep?) {
    (!oid) && (oid = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/org`,
      {
        "access_token": token,
        "type": "get_org_tree",
        "id": oid,
        "dep": dep
      }
    ).map((res: Response) => {

      if( res['errcode'] !== '00000') {
        return [];
      }else {
        res['datas'].forEach(function (e) {
          e.label = e.name;
          delete e.name;
          e.leaf = false;
          e.data = e.name;
        });
      }
      return res['datas'];
    })
  }
  //获取角色数据
  getJurList(oid){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      "access_token": token,
      "type": "get_personnel",
      "oid": [oid+""]
    }).map((res: Response) => {
      if(res['errcode'] == '00001'){//没有角色
        return [];
      }else if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  deepClone(obj){
    var o,i,j,k;
    if(typeof(obj)!="object" || obj===null)return obj;
    if(obj instanceof(Array))
    {
      o=[];
      i=0;j=obj.length;
      for(;i<j;i++)
      {
        if(typeof(obj[i])=="object" && obj[i]!=null)
        {
          o[i]=this.deepClone(obj[i]);
        }
        else
        {
          o[i]=obj[i];
        }
      }
    }
    else
    {
      o={};
      for(i in obj)
      {
        if(typeof(obj[i])=="object" && obj[i]!=null)
        {
          o[i]=this.deepClone(obj[i]);
        }
        else
        {
          o[i]=obj[i];
        }
      }
    }
    return o;
  }
}
