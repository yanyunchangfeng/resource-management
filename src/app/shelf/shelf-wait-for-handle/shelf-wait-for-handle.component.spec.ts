import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfWaitForHandleComponent } from './shelf-wait-for-handle.component';

describe('ShelfWaitForHandleComponent', () => {
  let component: ShelfWaitForHandleComponent;
  let fixture: ComponentFixture<ShelfWaitForHandleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfWaitForHandleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfWaitForHandleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
