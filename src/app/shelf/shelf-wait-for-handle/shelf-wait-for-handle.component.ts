import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ShelfService} from "../shelf.service";
import {StorageService} from "../../services/storage.service";
import {ConfirmationService} from "primeng/api";
import {MessageService} from "primeng/components/common/messageservice";
import {EventBusService} from "../../services/event-bus.service";
import {BasePage} from "../../base.page";

@Component({
  selector: 'app-shelf-wait-for-handle',
  templateUrl: './shelf-wait-for-handle.component.html',
  styleUrls: ['./shelf-wait-for-handle.component.scss']
})
export class ShelfWaitForHandleComponent extends BasePage implements OnInit {
  cols;                     //定义表格表头
  queryModel;
  shelfviewData;
  totalRecords;
  selectMaterial = [];
  chooseCids = [];
  statusNames = [];
  statusCategory = [];
  filesTree4;
  categories;
  zh;
  StatusData;

  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private shelfService:ShelfService,
    private storageService: StorageService,
    public confirmationService: ConfirmationService,
    public   messageService:MessageService,
    private eventBusService:EventBusService
  ) {
    super(confirmationService,messageService)
  }


  ngOnInit() {
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.statusNames = [
      {label: '', value: ''},
      {label: '在库', value: '在库'},
      {label: '已完成', value: '已完成'},
    ]
    this.statusCategory = [
      {label: '', value: ''},
      {label: '整机', value: '整机'},
      {label: '部件', value: '部件'},
      {label: '耗材', value: '耗材'},
      {label: '其他', value: '其他'},
    ]
    this.cols = [
      {field: 'applicant', header: '申请人'},
      {field: 'create_time', header: '创建时间'},
      {field: 'assetshelve_type', header: '上架类型'},
      {field: 'assetshelve_plantime_begin', header: '计划上架时间'},
      {field: 'assetshelve_plantime_end', header: '预计完成时间'},
      {field: 'owner', header: '当前处理人'},
      {field: 'status', header: '状态'},
    ];
    this.queryModel = {
      "condition": {
        "status": ""
      },
      "page": {
        "page_size": "10",
        "page_number": "1"
      }
    };
    this.queryShelfData();
    this.queryStatusData();
  }
  clearSearch() {
    this.queryModel.condition.name = '';
    this.queryModel.condition.sid = '';
    this.queryModel.condition.catagory = '';
  }

  // //搜索功能
  suggestInsepectiones() {
    this.queryModel.page.page_number = '1'
    this.queryModel.page.page_size = '10';
    this.queryShelfData()
  }

  //分页查询
  paginate(event) {
    this.chooseCids = []
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryShelfData();
  }

  queryShelfData() {
    this.shelfService.shelfWaitOverview(this.queryModel).subscribe(data => {
      if (!this.shelfviewData) {
        this.shelfviewData = [];
        this.selectMaterial = []
      }
      this.shelfviewData = data.items;
      console.log(this.shelfviewData)
      this.totalRecords = data.page.total;
    });
  }
  queryStatusData(){
    this.shelfService.queryStatus().subscribe(data=>{
      if(!data){
        this.StatusData = [];
      }else{
        this.StatusData = data;
        console.log(this.StatusData)
        this.StatusData = this.shelfService.formatDropdownData(this.StatusData)
      }
    });
  }
//删除
  viewDelete(current){
    //请求成功后删除本地数据
    this.chooseCids = [];
    this.chooseCids.push(current['sid']);
    this.confirmationService.confirm({
      message:'确认删除吗？',
      accept: () => {
        this.shelfService.delete(this.chooseCids).subscribe(()=>{
          this.queryShelfData();
          this.alert("删除成功")
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })

      },
      reject:() =>{
      }
    });
  }
//编辑
  viewUpdate(current){
    let sid = current['sid'];
    this.eventBusService.updateshelf.next(current.status_code);
    this.router.navigate(['../update'],{queryParams:{sid:sid,state:'update',title:'修改'},relativeTo:this.route})
  }
//审批
  viewApprove(current){
    let sid = current['sid'];
    this.eventBusService.updateshelf.next(current.status_code);
    this.router.navigate(['../shelfapproved'],{queryParams:{sid:sid,state:'viewDetail',title:'审批'},relativeTo:this.route})
  }
//待上架
  viewComplyshelf(current){
    let sid = current['sid'];
    this.eventBusService.updateshelf.next(current.status_code);
    this.router.navigate(['../complyshelf'],{queryParams:{sid:sid,state:'viewDetail',title:'执行上架'},relativeTo:this.route})
  }
//上架完成
  viewComplateshelf(current){
    let sid = current['sid'];
    this.eventBusService.updateshelf.next(current.status_code);
    this.router.navigate(['../completeshelf'],{queryParams:{sid:sid,state:'viewDetail',title:'上架完成'},relativeTo:this.route})
  }
//上架复核
  viewReviewshelf(current){
    let sid = current['sid'];
    this.eventBusService.updateshelf.next(current.status_code);
    this.router.navigate(['../shelfreview'],{queryParams:{sid:sid,state:'viewDetail',title:'上架复核'},relativeTo:this.route})
  }
//上架整改
  viewRectificationshelf(current){
    let sid = current['sid'];
    this.eventBusService.updateshelf.next(current.status_code);
    this.router.navigate(['../shelfreview'],{queryParams:{sid:sid,state:'viewDetail',title:'上架整改'},relativeTo:this.route})
  }
//查看
  viewShelf(current){
    let sid = current['sid'];
    this.eventBusService.updateshelf.next(current.status_code);
    this.router.navigate(['../shelfDetail'],{queryParams:{sid:sid,state:'viewDetail',title:'查看'},relativeTo:this.route})
  }

}
