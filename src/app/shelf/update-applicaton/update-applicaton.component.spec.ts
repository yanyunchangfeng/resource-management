import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateApplicatonComponent } from './update-applicaton.component';

describe('UpdateApplicatonComponent', () => {
  let component: UpdateApplicatonComponent;
  let fixture: ComponentFixture<UpdateApplicatonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateApplicatonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateApplicatonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
