import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfAddOrUpdateDevicesComponent } from './shelf-add-or-update-devices.component';

describe('ShelfAddOrUpdateDevicesComponent', () => {
  let component: ShelfAddOrUpdateDevicesComponent;
  let fixture: ComponentFixture<ShelfAddOrUpdateDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfAddOrUpdateDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfAddOrUpdateDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
