import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Tree} from "primeng/tree";
import {TreeNode} from "primeng/api";
import {ShelfService} from "../shelf.service";

@Component({
  selector: 'app-executor',
  templateUrl: './executor.component.html',
  styleUrls: ['./executor.component.css']
})
export class ExecutorComponent implements OnInit {

  width;
  windowSize;
  display;
  queryModel;
  @ViewChild('expandingTree')
  expandingTree: Tree;
  filesTree4: TreeNode[];
  @Output() closeAddMask = new EventEmitter();
  @Output() addTree = new EventEmitter();
  selected: TreeNode[];
  constructor( private shelfService: ShelfService ) { }
  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    this.queryModel = {
      'id':'',
      'dep':''
    }
    this.shelfService.getDepartmentDatas().subscribe( data =>{
      if(!data){
        data = [];
      }
      this.filesTree4 = data;

    })
  }

  //关闭遮罩层
  closeInspectionMask(bool){
    this.closeAddMask.emit(bool)
  }
  // 组织树懒加载
  nodeExpand(event) {
    if (event.node) {
      this.shelfService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(res => {
        event.node.children  = res;
      });
    }
  }
  formSubmit(){
    let arr = []
    for(let key of this.selected){
      let obj={};
      obj['label']=key['label'];
      obj['oid']=key['oid'];
      arr.push(obj)
    }
    console.log(arr)
    this.addTree.emit(arr);
  }


}
