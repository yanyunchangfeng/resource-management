import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAssetLocationComponent } from './update-asset-location.component';

describe('UpdateAssetLocationComponent', () => {
  let component: UpdateAssetLocationComponent;
  let fixture: ComponentFixture<UpdateAssetLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAssetLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAssetLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
