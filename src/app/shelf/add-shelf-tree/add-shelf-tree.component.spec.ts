import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddShelfTreeComponent } from './add-shelf-tree.component';

describe('AddShelfTreeComponent', () => {
  let component: AddShelfTreeComponent;
  let fixture: ComponentFixture<AddShelfTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddShelfTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddShelfTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
