import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ShelfFlowCharComponent} from "./shelf-flow-char/shelf-flow-char.component";
import {ShelfOverviewComponent} from "./shelf-overview/shelf-overview.component";
import {ShelfApplicationComponent} from "./shelf-application/shelf-application.component";
import {ShelfApprovedComponent} from "./shelf-approved/shelf-approved.component";
import {ComplyShelfComponent} from "./comply-shelf/comply-shelf.component";
import {ShelfReviewComponent} from "./shelf-review/shelf-review.component";
import {CompleteShelfComponent} from "./complete-shelf/complete-shelf.component";
import {ShelfWaitForHandleComponent} from "./shelf-wait-for-handle/shelf-wait-for-handle.component";
import {ShelfViewDetailComponent} from "./shelf-view-detail/shelf-view-detail.component";
import {UpdateApplicatonComponent} from "./update-applicaton/update-applicaton.component";

const routes: Routes = [
  {path:'flowchar',component:ShelfFlowCharComponent,children:[
    {path:'shelfoverview', component: ShelfOverviewComponent},
    {path:'shelfapplication', component: ShelfApplicationComponent},
    {path:'shelfapproved', component: ShelfApprovedComponent},
    {path:'complyshelf', component: ComplyShelfComponent},
    {path:'shelfreview', component: ShelfReviewComponent},
    {path:'completeshelf', component: CompleteShelfComponent},
    {path:'shelfwait', component: ShelfWaitForHandleComponent},
    {path:'shelfDetail', component: ShelfViewDetailComponent},
    {path:'update', component: UpdateApplicatonComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShelfRoutingModule { }
