import { NgModule } from '@angular/core';

import { CapacityRoutingModule } from './capacity-routing.module';
import {ShareModule} from '../shared/share.module';
import {CapacityService} from './capacity.service';
import {PublicService} from '../services/public.service';
import {CapacityBasalDatasComponent} from './capacity-basal-datas/capacity-basal-datas.component';
import { CapSpaceNodeComponent } from './cap-space-node/cap-space-node.component';
import { CapDataCenterComponent } from './cap-data-center/cap-data-center.component';
import { CapBuildingComponent } from './cap-building/cap-building.component';
import { CapFloorComponent } from './cap-floor/cap-floor.component';
import { CapRoomComponent } from './cap-room/cap-room.component';
import { CapModuleComponent } from './cap-module/cap-module.component';
import { CapCabinetComponent } from './cap-cabinet/cap-cabinet.component';
import {PersonelDialogComponent} from '../public/personel-dialog/personel-dialog.component';
import {PublicModule} from '../public/public.module';
import { CapRootComponent } from './cap-root/cap-root.component';
import { CapViewSpaceNodeComponent } from './cap-view-space-node/cap-view-space-node.component';
import { CapViewDataCenterComponent } from './cap-view-data-center/cap-view-data-center.component';
import { CapViewBuildingComponent } from './cap-view-building/cap-view-building.component';
import { CapViewFloorComponent } from './cap-view-floor/cap-view-floor.component';
import { CapViewRoomComponent } from './cap-view-room/cap-view-room.component';
import { CapViewModuleComponent } from './cap-view-module/cap-view-module.component';
import { CapViewCabinetComponent } from './cap-view-cabinet/cap-view-cabinet.component';
import { CapViewRootComponent } from './cap-view-root/cap-view-root.component';

@NgModule({
    imports: [
        ShareModule,
        CapacityRoutingModule,
        PublicModule
    ],
    declarations: [
        CapacityBasalDatasComponent,
        CapSpaceNodeComponent,
        CapDataCenterComponent,
        CapBuildingComponent,
        CapFloorComponent,
        CapRoomComponent,
        CapModuleComponent,
        CapCabinetComponent,
        CapRootComponent,
        CapViewSpaceNodeComponent,
        CapViewDataCenterComponent,
        CapViewBuildingComponent,
        CapViewFloorComponent,
        CapViewRoomComponent,
        CapViewModuleComponent,
        CapViewCabinetComponent,
        CapViewRootComponent,

    ],
    providers: [
        CapacityService,
        PublicService
    ]
})
export class CapacityModule { }
