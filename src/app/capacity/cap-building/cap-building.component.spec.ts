import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapBuildingComponent } from './cap-building.component';

describe('CapBuildingComponent', () => {
  let component: CapBuildingComponent;
  let fixture: ComponentFixture<CapBuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapBuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapBuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
