import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapViewFloorComponent } from './cap-view-floor.component';

describe('CapViewFloorComponent', () => {
  let component: CapViewFloorComponent;
  let fixture: ComponentFixture<CapViewFloorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapViewFloorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapViewFloorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
