import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PublicService} from '../../services/public.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-cap-view-space-node',
  templateUrl: './cap-view-space-node.component.html',
  styleUrls: ['./cap-view-space-node.component.scss']
})
export class CapViewSpaceNodeComponent implements OnInit {
    title: string = '查看';
    spaceNodeObj: Object = {
        'father': '',
        'father_did': '',
        'name': '',
        'did': '',
        'sp_type': '',
        'status': '',
        'remark': '',
        'custom1': '',
        'custom2': ''
    };
    myForm: FormGroup;
    paramDid: string;
    constructor(private fb: FormBuilder,
                private activedRouter: ActivatedRoute,
                private publicService: PublicService) { }

    ngOnInit() {
        this.initForm();
        this.getRouterParam();
        this.getNodeMessage();
    }
    initForm() {
        this.myForm = this.fb.group({
            name: '',
            remark: ''
        });
    }
    getRouterParam() {
        this.activedRouter.queryParams.subscribe(param => {
            this.paramDid = param['did'];
            this.spaceNodeObj['father'] = param['father'];
            this.spaceNodeObj['father_did'] = param['father_did'];
        });
    }
    getNodeMessage() {
        this.activedRouter.queryParams.subscribe(param => {
            if (param['did']) {
                this.publicService.getSingleNode(this.paramDid).subscribe(res => {
                    this.spaceNodeObj['name'] = res && res['name'];
                    this.spaceNodeObj['remark'] = res && res['remark'];
                });
            }
        });

    }
}
