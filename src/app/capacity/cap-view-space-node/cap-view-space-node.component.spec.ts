import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapViewSpaceNodeComponent } from './cap-view-space-node.component';

describe('CapViewSpaceNodeComponent', () => {
  let component: CapViewSpaceNodeComponent;
  let fixture: ComponentFixture<CapViewSpaceNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapViewSpaceNodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapViewSpaceNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
