import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../services/storage.service';

@Injectable()
export class CapacityService {
    ip = environment.url.management;

    constructor(private http: HttpClient,
                private storageService: StorageService) { }

    //    新增服务请求基础数据
    addCapBasalDatas(name, status, father) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!father) && (father = '');
        // (!dep) && (dep = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/cap`,
            {
                'access_token': token,
                'type': 'space_tree_add',
                'data': {
                    'father': father,
                    'name': name,
                    'status': status,
                    'custom1': '',
                    'custom2': ''
                }

            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //    编辑/冻结/启用故障管理基础数据
    editCapBasalDatas(name, status, sid, father) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!sid) && (sid = '');
        // (!dep) && (dep = '');
        (!father) && (father = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/cap`,
            {
                'access_token': token,
                'type': 'space_tree_mod',
                'data': {
                    'father': father,
                    'sid': sid,
                    'name': name,
                    'status': status,
                    'custom1': '',
                    'custom2': ''
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //    删除服务请求基础数据
    deleteCapDatas(ids) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/cap`,
            {
                'access_token': token,
                'type': 'space_tree_del',
                'ids': ids
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
}
