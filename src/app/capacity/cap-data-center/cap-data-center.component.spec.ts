import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapDataCenterComponent } from './cap-data-center.component';

describe('CapDataCenterComponent', () => {
  let component: CapDataCenterComponent;
  let fixture: ComponentFixture<CapDataCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapDataCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapDataCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
