import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PublicService} from '../../services/public.service';
import {BasePage} from '../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/primeng';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-cap-data-center',
  templateUrl: './cap-data-center.component.html',
  styleUrls: ['./cap-data-center.component.scss']
})
export class CapDataCenterComponent extends BasePage implements OnInit {
    title: string = '新增数据中心';
    spaceNodeObj: Object = {
        'father': '',
        'father_did': '',
        'name': '',
        'did': '',
        'sp_type': '',
        'status': '',
        'remark': '',
        'custom1': '',
        'custom2': ''
    };
    spaceBelong: Array<object> = [];
    myForm: FormGroup;
    paramDid: string;
    hasCapArea: boolean = true;

    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private publicService: PublicService,
                private eventBusService: EventBusService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.initForm();
        this.initDropdownDatas();
        this.getParams();
    }
    initForm() {
        this.myForm = this.fb.group({
            father: '',
            remark: ''
        });
    }
    initDropdownDatas() {
        this.publicService.getNodeByType('A').subscribe(res => {
           this.spaceBelong = res;
           if (res.length > 0) {
               this.spaceNodeObj['father'] = res[0]['label'];
               this.spaceNodeObj['father_did'] = res[0]['father_did'];
           }
           this.getFather();
        });
    }
    goBack() {
        this.router.navigate(['../../capacityBasalDatas'], {relativeTo: this.activedRouter});
    }
    getParams() {
        this.activedRouter.queryParams.subscribe(param => {
            this.paramDid = param['did'];
            if (param['did']) {
                this.title = '编辑数据中心';
                this.publicService.getSingleNode(param['did']).subscribe(res => {
                    this.spaceNodeObj['name'] = res && res['name'];
                    this.spaceNodeObj['remark'] = res && res['remark'];
                    this.spaceNodeObj['father'] = res && res['father'];
                    this.spaceNodeObj['father_did'] = res && res['father_did'];
                    this.spaceNodeObj['did'] = res && res['did'];
                    this.spaceNodeObj['sp_type'] = res && res['sp_type'];
                    this.spaceNodeObj['status'] = res && res['status'];
                });
            }
            if (param['father_did']) {
               this.spaceNodeObj['father']  = param['father'];
               this.spaceNodeObj['father_did'] = param['father_did'];
            }
        });
    }
    onChange(event) {
        this.spaceNodeObj['father_did'] = event['value']['did'];
        this.spaceNodeObj['father'] = event['value']['label'];
    }
    save() {
        if (this.spaceNodeObj['father']['did']) {
            this.spaceNodeObj['father_did'] = this.spaceNodeObj['father']['did'];
            this.spaceNodeObj['father'] = this.spaceNodeObj['father']['label'];
        }
        if (this.spaceNodeObj['name'] && this.spaceNodeObj['father']) {
            if (this.paramDid) {
                this.publicService.editSpaceNode(this.spaceNodeObj).subscribe(res => {
                    if (res === '00000') {
                        this.alert('编辑成功');
                        this.eventBusService.space.next(true);
                        this.router.navigate(['../../capacityBasalDatas'], {queryParams: {did: this.spaceNodeObj['father_did']}, relativeTo: this.activedRouter});
                    } else {
                        this.alert(`编辑失败: ${res}`, 'error');
                    }
                });
            }else {
                // this.getFather();
                this.spaceNodeObj['sp_type'] = 'B';
                this.publicService.addSpaceNode(this.spaceNodeObj).subscribe(res => {
                    if (res === '00000') {
                        this.alert('新增成功');
                        this.eventBusService.space.next(true);
                        this.router.navigate(['../../capacityBasalDatas'], {queryParams: {did: this.spaceNodeObj['father_did']}, relativeTo: this.activedRouter});
                    }else {
                        this.alert(`新增失败: ${res}`, 'error');
                    }
                });
            }
        }else {
            this.setValidators();
            this.alert('请填写完整信息', 'warn');
        }

    }
    getFather() {
        this.activedRouter.queryParams.subscribe(param => {
            if (param['father']) {
                this.spaceNodeObj['father'] = param['father'];
                this.spaceNodeObj['father_did'] = param['father_did'];
                this.spaceNodeObj['sp_type'] = param['sp_type'];
            }
        });
    }
    get remark() {
        return this.myForm.controls['remark'].untouched && this.myForm.controls['remark'].hasError('required') || (this.myForm.controls['remark'].touched && this.myForm.controls['remark'].hasError('required'));
    }
    setValidators() {
        this.myForm.controls['remark'].setValidators([Validators.required]);
        this.myForm.controls['remark'].updateValueAndValidity();
    }
}
