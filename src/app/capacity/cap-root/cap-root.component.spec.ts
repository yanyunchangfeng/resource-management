import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapRootComponent } from './cap-root.component';

describe('CapRootComponent', () => {
  let component: CapRootComponent;
  let fixture: ComponentFixture<CapRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
