import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {PublicService} from '../../services/public.service';

@Component({
  selector: 'app-cap-view-building',
  templateUrl: './cap-view-building.component.html',
  styleUrls: ['./cap-view-building.component.scss']
})
export class CapViewBuildingComponent implements OnInit {
    title: string = '查看';
    spaceNodeObj: Object = {
        'father': '',
        'father_did': '',
        'name': '',
        'did': '',
        'sp_type': '',
        'status': '',
        'remark': '',
        'custom1': '',
        'custom2': ''
    };
    myForm: FormGroup;
    constructor(private fb: FormBuilder,
                private activedRouter: ActivatedRoute,
                private publicService: PublicService) { }

    ngOnInit() {
        this.initForm();
        this.initDatas();
    }
    initForm() {
        this.myForm = this.fb.group({
            name: '',
            father: ''
        });
    }
    initDatas() {
        this.activedRouter.queryParams.subscribe(param => {
            if (param['did']) {
                this.publicService.getSingleNode(param['did']).subscribe(res => {
                    this.spaceNodeObj['name'] = res && res['name'];
                    this.spaceNodeObj['father'] = res && res['father'];
                });
            }
        });
    }
}
