import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapViewBuildingComponent } from './cap-view-building.component';

describe('CapViewBuildingComponent', () => {
  let component: CapViewBuildingComponent;
  let fixture: ComponentFixture<CapViewBuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapViewBuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapViewBuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
