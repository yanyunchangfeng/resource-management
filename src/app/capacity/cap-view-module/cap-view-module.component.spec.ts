import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapViewModuleComponent } from './cap-view-module.component';

describe('CapViewModuleComponent', () => {
  let component: CapViewModuleComponent;
  let fixture: ComponentFixture<CapViewModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapViewModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapViewModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
