import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {BasePage} from '../../base.page';
import {PublicService} from '../../services/public.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService, TreeNode} from 'primeng/primeng';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-cap-floor',
  templateUrl: './cap-floor.component.html',
  styleUrls: ['./cap-floor.component.scss']
})
export class CapFloorComponent extends BasePage implements OnInit {
    title: string = '新增楼层';
    spaceNodeObj: Object = {
        'father': '',
        'father_did': '',
        'name': '',
        'did': '',
        'sp_type': '',
        'status': '',
        'remark': '',
        'custom1': '',
        'custom2': ''
    };
    myForm: FormGroup;
    controlDiaglog: boolean;
    treeDatas: TreeNode[];
    selected: TreeNode[];
    paramDid: string;

    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private publicService: PublicService,
                private eventBusService: EventBusService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.initForm();
        this.controlDiaglog = false;
        this.initDatas();
        this.getRouterParam();
        this.getFather();
    }
    initForm() {
        this.myForm = this.fb.group({
            father: '',
            name: ''
        });
    }
    goBack() {
        this.router.navigate(['../../capacityBasalDatas'], {relativeTo: this.activedRouter});
    }
    showTreeDialog() {
        this.controlDiaglog = true;
        this.initTreeDatas();
    }
    clearTreeDialog() {
        this.spaceNodeObj['father'] = '';
        this.spaceNodeObj['father_did'] = '';
    }
    onHide() {
        this.controlDiaglog = false;
    }
    sure() {
        this.onHide();
        this.spaceNodeObj['father'] = this.selected['label'];
        this.spaceNodeObj['father_did'] = this.selected['did'];
    }
    cancel() {
        this.onHide();
    }
    initDatas() {
        this.activedRouter.queryParams.subscribe(param => {
            if (param['did']) {
                this.title = '编辑楼层信息';
                this.publicService.getSingleNode(param['did']).subscribe(res => {
                    this.spaceNodeObj['name'] = res && res['name'];
                    this.spaceNodeObj['remark'] = res && res['remark'];
                    this.spaceNodeObj['father'] = res && res['father'];
                    this.spaceNodeObj['father_did'] = res && res['father_did'];
                    this.spaceNodeObj['did'] = res && res['did'];
                    this.spaceNodeObj['sp_type'] = res && res['sp_type'];
                    this.spaceNodeObj['status'] = res && res['status'];
                });
            }
        });
    }
    initTreeDatas() {
        this.publicService.getCapBasalDatas( '', this.spaceNodeObj['sp_type']).subscribe(res => {
            this.treeDatas = res;
        });
    }
    save() {
        if (this.spaceNodeObj['name'] && this.spaceNodeObj['father']) {
            if (this.paramDid) {
                this.publicService.editSpaceNode(this.spaceNodeObj).subscribe(res => {
                    if (res === '00000') {
                        this.alert('编辑成功');
                        this.eventBusService.space.next(true);
                        this.router.navigate(['../../capacityBasalDatas'], {relativeTo: this.activedRouter});
                    } else {
                        this.alert(`编辑失败: ${res}`, 'error');
                    }
                });
            }else {

                this.publicService.addSpaceNode(this.spaceNodeObj).subscribe(res => {
                    if (res === '00000') {
                        this.alert('新增成功');
                        this.eventBusService.space.next(true);
                        this.router.navigate(['../../capacityBasalDatas'], {queryParams: {did: this.spaceNodeObj['father_did']}, relativeTo: this.activedRouter});
                    }else {
                        this.alert(`新增失败: ${res}`, 'error');
                    }
                });
            }
        }else {
            this.setValidators();
            this.alert('请填写完整信息', 'warn');
        }

    }
    get  fatherDirty() {
        return this.myForm.controls['father'].untouched && this.myForm.controls['father'].hasError('required');
    }
    get nameDirty() {
        return this.myForm.controls['name'].untouched && this.myForm.controls['name'].hasError('required') || (this.myForm.controls['name'].touched && this.myForm.controls['name'].hasError('required'));
    }
    setValidators() {
        this.myForm.controls['name'].setValidators([Validators.required]);
        this.myForm.controls['father'].setValidators([Validators.required]);
        this.myForm.controls['name'].updateValueAndValidity();
        this.myForm.controls['father'].updateValueAndValidity();
    }
    getFather() {
        this.activedRouter.queryParams.subscribe(param => {
            this.spaceNodeObj['father'] = param['father'];
            this.spaceNodeObj['father_did'] = param['father_did'];
            this.spaceNodeObj['sp_type'] = param['sp_type'];
        });
    }
    getRouterParam(){
        this.activedRouter.queryParams.subscribe(param => {
            this.paramDid = param['did'];
        });
    }
}
