export class RoomobjModel {
    father: string;
    father_did: string;
    name: string;
    sp_type: string;
    room_type: string;
    room_type_name: string;
    status: string;
    planning_power: string;
    planning_output_power: string;
    ups_power: string;
    planning_refrigerating_capacity: string;
    planning_rack: string;
    planning_network_port: string;
    planning_network_bandwidth: string;
    availability_level: string;
    area: string;
    room_manager: string;
    room_manager_pid: string;
    room_manager_phone: string;
    threshold_power: string;
    threshold_refrigeration: string;
    threshold_rack: string;
    threshold_network_port: string;
    addtional_power: string;
    addtional_refrigeration: string;
    addtional_rack: string;
    addtional_network_port: string;
    did: string;



    constructor(obj?: any) {
        this.father = obj && obj.father || '';
        this.father_did = obj && obj.father_did || '';
        this.name = obj && obj.name || '';
        this.sp_type = obj && obj.sp_type || '';
        this.room_type = obj && obj.room_type || '';
        this.room_type_name = obj && obj.room_type_name || '';
        this.ups_power = obj && obj.ups_power || '';
        this.planning_power = obj && obj.planning_power || '';
        this.planning_output_power = obj && obj.planning_output_power || '';
        this.planning_output_power = obj && obj.planning_output_power || '';
        this.planning_refrigerating_capacity = obj && obj.planning_refrigerating_capacity || '';
        this.area = obj && obj.area || '';
        this.room_manager = obj && obj.room_manager || '';
        this.planning_rack = obj && obj.planning_rack || '';
        this.planning_network_port = obj && obj.planning_network_port || '';
        this.planning_network_bandwidth = obj && obj.planning_network_bandwidth || '';
        this.availability_level = obj && obj.availability_level || '';
        this.room_manager_pid = obj && obj.room_manager_pid || '';
        this.room_manager_phone = obj && obj.room_manager_phone || '';
        this.threshold_power = obj && obj.threshold_power || '80';
        this.threshold_refrigeration = obj && obj.threshold_refrigeration || '80';
        this.threshold_rack = obj && obj.threshold_rack || '80';
        this.threshold_network_port = obj && obj.threshold_network_port || '80';
        this.addtional_power = obj && obj.addtional_power || '';
        this.addtional_refrigeration = obj && obj.addtional_refrigeration || '';
        this.addtional_rack = obj && obj.addtional_rack || '';
        this.addtional_network_port = obj && obj.addtional_network_port || '';
        this.did = obj && obj.did || '';
     }
}
