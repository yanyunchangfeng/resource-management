import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PublicService} from '../../services/public.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/primeng';
import {BasePage} from '../../base.page';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-cap-space-node',
  templateUrl: './cap-space-node.component.html',
  styleUrls: ['./cap-space-node.component.scss']
})
export class CapSpaceNodeComponent extends BasePage implements OnInit {
    title: string = '新增区域';
    spaceNodeObj: Object = {
        'father': '',
        'father_did': '',
        'name': '',
        'did': '',
        'sp_type': '',
        'status': '',
        'remark': '',
        'custom1': '',
        'custom2': ''
    };
    myForm: FormGroup;
    paramDid: string;
    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private publicService: PublicService,
                private eventBusService: EventBusService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.initForm();
        this.getRouterParam();
        this.getNodeMessage();
        // this.spaceNodeObj['']
    }
    initForm() {
        this.myForm = this.fb.group({
            name: '',
            remark: ''
        });
    }
    goBack() {
        this.router.navigate(['../../capacityBasalDatas'], {relativeTo: this.activedRouter});
    }
    getRouterParam() {
        this.activedRouter.queryParams.subscribe(param => {
            this.paramDid = param['did'];
            this.spaceNodeObj['father'] = param['father'];
            this.spaceNodeObj['father_did'] = param['father_did'];
        });
    }
    getNodeMessage() {
        this.activedRouter.queryParams.subscribe(param => {
            if (param['did']) {
                this.title = '编辑区域';
                this.publicService.getSingleNode(this.paramDid).subscribe(res => {
                    this.spaceNodeObj['name'] = res && res['name'];
                    this.spaceNodeObj['remark'] = res && res['remark'];
                    this.spaceNodeObj['father'] = res && res['father'];
                    this.spaceNodeObj['father_did'] = res && res['father_did'];
                    this.spaceNodeObj['did'] = res && res['did'];
                    this.spaceNodeObj['sp_type'] = res && res['sp_type'];
                    this.spaceNodeObj['status'] = res && res['status'];
                });
            }
        });

    }
    save() {
        if (this.spaceNodeObj['name']) {
            if (this.paramDid) {
                this.publicService.editSpaceNode(this.spaceNodeObj).subscribe(res => {
                    if (res === '00000') {
                        this.alert('编辑成功');
                        this.eventBusService.space.next(true);
                        this.router.navigate(['../../capacityBasalDatas'], {queryParams: {did: this.spaceNodeObj['father_did']}, relativeTo: this.activedRouter});
                    }else {
                        this.alert(`编辑失败: ${res}`, 'error');
                    }
                });
            }else {
                this.getFather();
                this.publicService.addSpaceNode(this.spaceNodeObj).subscribe(res => {
                    if (res === '00000') {
                        this.alert('新增成功');
                        this.eventBusService.space.next(true);
                        this.router.navigate(['../../capacityBasalDatas'], {queryParams: {did: this.spaceNodeObj['father_did']}, relativeTo: this.activedRouter});
                    }else {
                        this.alert(`新增失败: ${res}`, 'error');
                    }
                });
            }
        }else {
            this.setValidators();
            this.alert('请填写完整信息', 'warn');
        }

    }

    getFather() {
         this.activedRouter.queryParams.subscribe(param => {
             this.spaceNodeObj['father'] = param['father'];
             this.spaceNodeObj['father_did'] = param['father_did'];
             this.spaceNodeObj['sp_type'] = param['sp_type'];
        });
    }
    get name() {
        return this.myForm.controls['name'].untouched && this.myForm.controls['name'].hasError('required') || (this.myForm.controls['name'].touched && this.myForm.controls['name'].hasError('required'));
    }
    get remark() {
        return this.myForm.controls['remark'].untouched && this.myForm.controls['remark'].hasError('required');
    }
    setValidators() {
        this.myForm.controls['name'].setValidators([Validators.required]);
        // this.myForm.controls['remark'].setValidators([Validators.required]);
        this.myForm.controls['name'].updateValueAndValidity();
        // this.myForm.controls['remark'].updateValueAndValidity();
    }
}
