import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapSpaceNodeComponent } from './cap-space-node.component';

describe('CapSpaceNodeComponent', () => {
  let component: CapSpaceNodeComponent;
  let fixture: ComponentFixture<CapSpaceNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapSpaceNodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapSpaceNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
