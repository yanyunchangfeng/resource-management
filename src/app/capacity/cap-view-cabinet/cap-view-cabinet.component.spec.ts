import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapViewCabinetComponent } from './cap-view-cabinet.component';

describe('CapViewCabinetComponent', () => {
  let component: CapViewCabinetComponent;
  let fixture: ComponentFixture<CapViewCabinetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapViewCabinetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapViewCabinetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
