import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CabinetModel} from '../cabinet.model';
import {ActivatedRoute} from '@angular/router';
import {PublicService} from '../../services/public.service';

@Component({
  selector: 'app-cap-view-cabinet',
  templateUrl: './cap-view-cabinet.component.html',
  styleUrls: ['./cap-view-cabinet.component.scss']
})
export class CapViewCabinetComponent implements OnInit {
    title: string = '查看';
    myForm: FormGroup;
    cabinetObj: CabinetModel;

    constructor(private fb: FormBuilder,
                private activedRouter: ActivatedRoute,
                private publicService: PublicService) {
    }

    ngOnInit() {
        this.cabinetObj = new CabinetModel();
        this.initForm();
        this.initDatas();
    }

    initForm(): void {
        this.myForm = this.fb.group({
            father: '',
            name: '',
            planning_power: '',
            planning_rack: '',
            planning_pdu: '',
            planning_network_port: ''
        });
    }

    initDatas() {
        this.activedRouter.queryParams.subscribe(param => {
            if (param['did']) {
                this.publicService.getSingleNode(param['did']).subscribe(res => {
                    this.cabinetObj = new CabinetModel(res);
                });
            }
        });
    };
}
