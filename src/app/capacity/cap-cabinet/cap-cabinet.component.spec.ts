import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapCabinetComponent } from './cap-cabinet.component';

describe('CapCabinetComponent', () => {
  let component: CapCabinetComponent;
  let fixture: ComponentFixture<CapCabinetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapCabinetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapCabinetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
