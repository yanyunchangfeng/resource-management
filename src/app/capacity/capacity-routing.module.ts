import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CapacityBasalDatasComponent} from './capacity-basal-datas/capacity-basal-datas.component';
import {CapSpaceNodeComponent} from './cap-space-node/cap-space-node.component';
import {CapDataCenterComponent} from './cap-data-center/cap-data-center.component';
import {CapBuildingComponent} from './cap-building/cap-building.component';
import {CapFloorComponent} from './cap-floor/cap-floor.component';
import {CapRoomComponent} from './cap-room/cap-room.component';
import {CapModuleComponent} from './cap-module/cap-module.component';
import {CapCabinetComponent} from './cap-cabinet/cap-cabinet.component';
import {CapRootComponent} from './cap-root/cap-root.component';
import {CapViewRootComponent} from './cap-view-root/cap-view-root.component';
import {CapViewSpaceNodeComponent} from './cap-view-space-node/cap-view-space-node.component';
import {CapViewDataCenterComponent} from './cap-view-data-center/cap-view-data-center.component';
import {CapViewBuildingComponent} from './cap-view-building/cap-view-building.component';
import {CapViewFloorComponent} from './cap-view-floor/cap-view-floor.component';
import {CapViewRoomComponent} from './cap-view-room/cap-view-room.component';
import {CapViewModuleComponent} from './cap-view-module/cap-view-module.component';
import {CapViewCabinetComponent} from './cap-view-cabinet/cap-view-cabinet.component';

const routes: Routes = [
    {path: 'capacityBasalDatas', component: CapacityBasalDatasComponent, children: [
        {path: 'capspacenode', component: CapSpaceNodeComponent},
        {path: 'capdatacenter', component: CapDataCenterComponent},
        {path: 'capbuilding', component: CapBuildingComponent},
        {path: 'capfloor', component: CapFloorComponent},
        {path: 'caproom', component: CapRoomComponent},
        {path: 'capmodule', component: CapModuleComponent},
        {path: 'capcabinet', component: CapCabinetComponent},
        {path: 'caproot', component: CapRootComponent},
        {path: 'capviewroot', component: CapViewRootComponent},
        {path: 'capviewspacenode', component: CapViewSpaceNodeComponent},
        {path: 'capviewdatacenter', component: CapViewDataCenterComponent},
        {path: 'capviewbuilding', component: CapViewBuildingComponent},
        {path: 'capviewfloor', component: CapViewFloorComponent},
        {path: 'capviewroom', component: CapViewRoomComponent},
        {path: 'capviewmodule', component: CapViewModuleComponent},
        {path: 'capviewcabinet', component: CapViewCabinetComponent},

    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CapacityRoutingModule { }
