import { Component, OnInit } from '@angular/core';
import {ConfirmationService, MenuItem, Message, TreeNode} from 'primeng/primeng';
import {PublicService} from '../../services/public.service';
import {CapacityService} from '../capacity.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../services/event-bus.service';
import {BasePage} from '../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-capacity-basal-datas',
  templateUrl: './capacity-basal-datas.component.html',
  styleUrls: ['./capacity-basal-datas.component.scss']
})
export class CapacityBasalDatasComponent extends BasePage implements OnInit {
    treeDatas: TreeNode[];             // 数据配置树
    msgPop: Message[];                 // 操作请求后的提示
    menuItems: MenuItem[];
    displayTypes: boolean;
    selected: TreeNode[];
    selectedType: any;
    types: Array<object>;
    selectedFather: any;

    constructor(private capacityService: CapacityService,
                private publicService: PublicService,
                public confirmationService: ConfirmationService,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private eventBusService: EventBusService,
                public messageService: MessageService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.initTreeDatas();
        this.displayTypes = false;
        this.initMenuItems();
        this.eventBusService.space.subscribe(data => {
            // if (data) {
            //     this.publicService.getCapBasalDatas('ROOT').subscribe(res => {
            //         this.treeDatas = res;
            //     });
            // }
            this.initTreeDatas();
        });
    }
    initTreeDatas(): void {
        this.publicService.getCapBasalDatas('ROOT').subscribe(res => {
            this.treeDatas = res;
            if (res) {
                this.expandAll(res);
            }
        });
    }
    expandAll(treeDatas): void {
        treeDatas.forEach( node => {
            this.expandRecursive(node, true);
        } );
    }
    private expandRecursive(node: TreeNode, isExpand: boolean): void{
        node.expanded = isExpand;
        if (node.children){
            node.children.forEach( childNode => {
                this.expandRecursive(childNode, isExpand);
            } );
        }
    }
    showDialog(node) {
        this.displayTypes = true;
        this.initTypes(node['sp_type']);
        this.selectedFather = node;
    }
    initMenuItems(): void {
        this.menuItems = [
            {label: '新增', icon: 'fa-plus', command: e => {
                    if (this.selected['sp_type'] === 'G') {
                       this.alert('不允许新增', 'error');
                    }else {
                        this.showDialog(this.selected);
                    }
                }
            },
            {label: '编辑', icon: 'fa-edit', command: e => {
               this.editJumper(this.selected);
            }
            },
            {label: '删除', icon: 'fa-close', command: e => {
                this.confirmationService.confirm({
                    message: '确认删除吗？',
                    accept: () => {
                        this.deleteNode(this.selected);
                        this.eventBusService.space.next(true);
                    },
                    reject: () => {

                    }
                });
            }}
        ];
    }
    deleteNode(node) {
        let deleteArray = [node['did']];
        if (node['sp_type']) {
            this.publicService.deleteSpaceNode(deleteArray).subscribe(res => {
                if (res === '00000') {
                    this.alert('删除成功');
                }else {
                    this.alert('删除失败', 'error');
                }
            });
        }else {
            this.alert('根节点禁止删除', 'error');
        }
    }
    initTypes(type): void {
        this.publicService.getSpaceTypes(type).subscribe(res => {
            this.types = res;
            this.selectedType = res[0]['value'];
        });
    }
    onNodeSelect(event) {
        if (event.node.did === 'ROOT') {
            this.router.navigate(['./capviewroot'], {relativeTo: this.activedRouter});
        }else {
            switch (event.node.sp_type) {
                case 'A' :
                    this.router.navigate(['./capviewspacenode'], {queryParams: {did: event.node.did}, relativeTo: this.activedRouter});
                    break;
                case 'B' :
                    this.router.navigate(['./capviewdatacenter'], {queryParams: {did: event.node.did}, relativeTo: this.activedRouter});
                    break;
                case 'C':
                    this.router.navigate(['./capviewbuilding'], {queryParams: {did: event.node.did}, relativeTo: this.activedRouter});
                    break;
                case 'D' :
                    this.router.navigate(['./capviewfloor'], {queryParams: {did: event.node.did}, relativeTo: this.activedRouter});
                    break;
                case 'E' :
                    this.router.navigate(['./capviewroom'], {queryParams: {did: event.node.did}, relativeTo: this.activedRouter});
                    break;
                case 'F':
                    this.router.navigate(['./capviewmodule'], {queryParams: {did: event.node.did}, relativeTo: this.activedRouter});
                    break;
                case 'G' :
                    this.router.navigate(['./capviewcabinet'], {queryParams: {did: event.node.did}, relativeTo: this.activedRouter});
            }
        }

    }
    editJumper(type: any) {
        if (type['did'] && (type['did'] === 'ROOT')) {
            this.router.navigate(['./caproot'], {relativeTo: this.activedRouter});
        }else {
            switch (type['sp_type']) {
                case 'A' :
                    this.router.navigate(['./capspacenode'], {queryParams: {did: type['did']}, relativeTo: this.activedRouter});
                    break;
                case 'B' :
                    this.router.navigate(['./capdatacenter'], {queryParams: {did: type['did']}, relativeTo: this.activedRouter});
                    break;
                case 'C':
                    this.router.navigate(['./capbuilding'], {queryParams: {did: type['did']}, relativeTo: this.activedRouter});
                    break;
                case 'D' :
                    this.router.navigate(['./capfloor'], {queryParams: {did: type['did']}, relativeTo: this.activedRouter});
                    break;
                case 'E' :
                    this.router.navigate(['./caproom'], {queryParams: {did: type['did']}, relativeTo: this.activedRouter});
                    break;
                case 'F':
                    this.router.navigate(['./capmodule'], {queryParams: {did: type['did']}, relativeTo: this.activedRouter});
                    break;
                case 'G' :
                    this.router.navigate(['./capcabinet'], {queryParams: {did: type['did']}, relativeTo: this.activedRouter});
            }
        }
    }
    onHide() {
        this.displayTypes = false;
    }
    sure() {
        this.onHide();
        switch (this.selectedType) {
            case 'A' :
                this.router.navigate(['./capspacenode'], {queryParams: {father: this.selectedFather.label, father_did: this.selectedFather.did, sp_type: this.selectedType}, relativeTo: this.activedRouter});
                break;
            case 'B' :
                this.router.navigate(['./capdatacenter'], {queryParams: {father: this.selectedFather.label, father_did: this.selectedFather.did, sp_type: this.selectedType}, relativeTo: this.activedRouter});
                break;
            case 'C':
                this.router.navigate(['./capbuilding'], {queryParams: {father: this.selectedFather.label, father_did: this.selectedFather.did, sp_type: this.selectedType}, relativeTo: this.activedRouter});
                break;
            case 'D' :
                this.router.navigate(['./capfloor'], {queryParams: {father: this.selectedFather.label, father_did: this.selectedFather.did, sp_type: this.selectedType}, relativeTo: this.activedRouter});
                break;
            case 'E' :
                this.router.navigate(['./caproom'], {queryParams: {father: this.selectedFather.label, father_did: this.selectedFather.did, sp_type: this.selectedType}, relativeTo: this.activedRouter});
                break;
            case 'F':
                this.router.navigate(['./capmodule'], {queryParams: {father: this.selectedFather.label, father_did: this.selectedFather.did, sp_type: this.selectedType}, relativeTo: this.activedRouter});
                break;
            case 'G' :
                this.router.navigate(['./capcabinet'], {queryParams: {father: this.selectedFather.label, father_did: this.selectedFather.did, sp_type: this.selectedType}, relativeTo: this.activedRouter});
        }
    }
    cancel() {
        this.onHide();
    }

}
