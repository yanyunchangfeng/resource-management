import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityBasalDatasComponent } from './capacity-basal-datas.component';

describe('CapacityBasalDatasComponent', () => {
  let component: CapacityBasalDatasComponent;
  let fixture: ComponentFixture<CapacityBasalDatasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityBasalDatasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityBasalDatasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
