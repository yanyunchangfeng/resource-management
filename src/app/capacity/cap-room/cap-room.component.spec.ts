import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapRoomComponent } from './cap-room.component';

describe('CapRoomComponent', () => {
  let component: CapRoomComponent;
  let fixture: ComponentFixture<CapRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
