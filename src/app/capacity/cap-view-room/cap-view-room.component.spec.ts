import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapViewRoomComponent } from './cap-view-room.component';

describe('CapViewRoomComponent', () => {
  let component: CapViewRoomComponent;
  let fixture: ComponentFixture<CapViewRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapViewRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapViewRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
