import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {RoomobjModel} from '../roomobj.model';
import {ActivatedRoute} from '@angular/router';
import {PublicService} from '../../services/public.service';

@Component({
  selector: 'app-cap-view-room',
  templateUrl: './cap-view-room.component.html',
  styleUrls: ['./cap-view-room.component.scss']
})
export class CapViewRoomComponent implements OnInit {
    title: string = '查看';
    myForm: FormGroup;
    roomObj: RoomobjModel;
    displayDefault: boolean;
    displayPowerOrFacility: boolean;
    constructor(private fb: FormBuilder,
                private activedRouter: ActivatedRoute,
                private publicService: PublicService) { }

    ngOnInit() {
        this.roomObj = new RoomobjModel();
        this.initForm();
        this.initDisplay();
        this.initDatas();
    }
    initForm(): void {
        this.myForm = this.fb.group({
            father: '',
            name: '',
            planning_power: '',
            planning_refrigerating_capacity: '',
            ups_power: '',
            planning_rack: '',
            planning_network_port: '',
            planning_network_bandwidth: '',
            area: '',
            room_manager: '',
            room_manager_phone: '',
            room_type: ''
        });
    }
    initDatas() {
        this.activedRouter.queryParams.subscribe(param => {
            if (param['did']) {
                this.publicService.getSingleNode(param['did']).subscribe(res => {
                    this.roomObj = new RoomobjModel(res);
                    this.roomObj.availability_level = res['availability_level'];
                    this.roomObj.room_type = res['room_type'];
                    if (res['room_type'] === '电力间' || res['room_type'] === '设施间'){
                        this.displayDefault = false;
                        this.displayPowerOrFacility = true;
                    }else {
                        this.initDisplay();
                    }
                });
            }
        });
    }
    initDisplay(): void {
        this.displayDefault = true;
        this.displayPowerOrFacility = false;
    }
}
