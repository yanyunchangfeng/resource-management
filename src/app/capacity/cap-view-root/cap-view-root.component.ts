import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PublicService} from '../../services/public.service';

@Component({
  selector: 'app-cap-view-root',
  templateUrl: './cap-view-root.component.html',
  styleUrls: ['./cap-view-root.component.scss']
})
export class CapViewRootComponent implements OnInit {
    title: string = '查看';
    spaceNodeObj: Object = {
        'name': '',
        'did': ''
    };
    myForm: FormGroup;
    constructor(private publicService: PublicService,
                private fb: FormBuilder) { }

    ngOnInit() {
        this.initForm();
        this.initData();
    }
    initData() {
        this.publicService.getSingleNode('ROOT').subscribe(res => {
            this.spaceNodeObj['name'] = res && res['name'];
            this.spaceNodeObj['did'] = res && res['did'];
        });
    }
    initForm() {
        this.myForm = this.fb.group({
            name: ''
        });
    }
}
