import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapViewRootComponent } from './cap-view-root.component';

describe('CapViewRootComponent', () => {
  let component: CapViewRootComponent;
  let fixture: ComponentFixture<CapViewRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapViewRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapViewRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
