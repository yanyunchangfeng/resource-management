export  class CabinetModel {
    father: string;
    father_did: string;
    name: string;
    sp_type: string;
    status: string;
    planning_power: string;
    planning_pdu: string ;
    planning_rack: string ;
    planning_network_port: string ;
    threshold_power: string ;
    threshold_pdu: string ;
    threshold_rack: string ;
    threshold_network_port: string ;
    addtional_power: string ;
    addtional_pdu: string ;
    addtional_rack: string ;
    addtional_network_port: string ;
    did: string;

    constructor(obj?) {
        this.father = obj && obj['father'] || '';
        this.father_did = obj && obj['father_did'] || '';
        this.name = obj && obj['name'] || '';
        this.sp_type = obj && obj['sp_type'] || '';
        this.status = obj && obj['status'] || '';
        this.planning_power = obj && obj['planning_power'] || '';
        this.planning_pdu = obj && obj['planning_pdu'] || '';
        this.planning_rack = obj && obj['planning_rack'] || '';
        this.planning_network_port = obj && obj['planning_network_port'] || '';
        this.threshold_power = obj && obj['threshold_power'] || '80';
        this.threshold_pdu = obj && obj['threshold_pdu'] || '80';
        this.threshold_rack = obj && obj['threshold_rack'] || '80';
        this.threshold_network_port = obj && obj['threshold_network_port'] || '80';
        this.addtional_power = obj && obj['addtional_power'] || '';
        this.addtional_pdu = obj && obj['addtional_pdu'] || '';
        this.addtional_rack = obj && obj['addtional_rack'] || '';
        this.addtional_network_port = obj && obj['addtional_network_port'] || '';
        this.did = obj && obj['did'] || '';

    }
}
