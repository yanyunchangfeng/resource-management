import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapViewDataCenterComponent } from './cap-view-data-center.component';

describe('CapViewDataCenterComponent', () => {
  let component: CapViewDataCenterComponent;
  let fixture: ComponentFixture<CapViewDataCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapViewDataCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapViewDataCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
