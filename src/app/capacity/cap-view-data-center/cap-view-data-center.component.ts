import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {PublicService} from '../../services/public.service';

@Component({
  selector: 'app-cap-view-data-center',
  templateUrl: './cap-view-data-center.component.html',
  styleUrls: ['./cap-view-data-center.component.scss']
})
export class CapViewDataCenterComponent implements OnInit {
    title: string = '查看';
    spaceNodeObj: Object = {
        'father': '',
        'father_did': '',
        'name': '',
        'did': '',
        'sp_type': '',
        'status': '',
        'remark': '',
        'custom1': '',
        'custom2': ''
    };
    myForm: FormGroup;
    paramDid: string;
    constructor(private fb: FormBuilder,
                private publicService: PublicService,
                private activedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.initForm();
        this.getParams();
    }
    initForm() {
        this.myForm = this.fb.group({
            father: '',
            remark: ''
        });
    }
    getParams() {
        this.activedRouter.queryParams.subscribe(param => {
            this.paramDid = param['did'];
            if (param['did']) {
                this.publicService.getSingleNode(param['did']).subscribe(res => {
                    this.spaceNodeObj['name'] = res && res['name'];
                    this.spaceNodeObj['father'] = res && res['father'];
                });
            }
        });
    }
}
