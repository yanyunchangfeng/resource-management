import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapModuleComponent } from './cap-module.component';

describe('CapModuleComponent', () => {
  let component: CapModuleComponent;
  let fixture: ComponentFixture<CapModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
