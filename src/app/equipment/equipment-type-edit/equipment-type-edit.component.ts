import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EquipmentService} from "../equipment.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-equipment-type-edit',
  templateUrl: './equipment-type-edit.component.html',
  styleUrls: ['./equipment-type-edit.component.css']
})
export class EquipmentTypeEditComponent implements OnInit {
  display;
  title;
  @Output() closeOrgEdit =new EventEmitter();
  @Output() addOrg = new EventEmitter(); //新增组织节点
  @Output() updateOrg = new EventEmitter(); //修改组织节点
  org: FormGroup;
  @Input() currentOrg;
  @Input() state;
  submitAddOrg = {//新增组织节点参数
    "father": '',
    "dep": '',
    // "nid": '20',
    "name": '',
  };
  submitUpdateOrg = [//修改组织节点参数
    {
      "oid": '',
      "name": '',
      "remark": '',
      "dep": '',
      "father": ''
    }
  ];
  constructor(
    private equipmentService: EquipmentService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService
  ) {
    this.org = fb.group({
        /*orgid: [''],
        orgName: ['', Validators.required],
        orgRemark: [''],*/
        "nid": [''],
        "fatherName": [''],
        "name": [''],
        "dep": [''],
      }
    )
  }

  ngOnInit() {
    console.log(this.currentOrg)
    this.display = true;
    this.submitUpdateOrg[0].oid = this.currentOrg.oid;
    this.submitUpdateOrg[0].father = this.currentOrg.father;
    this.submitUpdateOrg[0].dep = this.currentOrg.dep;
    if(this.state ==='add') {
      this.getCurrentNodeData();
      this.title = '新增设备类型';
      this.submitAddOrg.father = this.currentOrg.nid;
      this.submitAddOrg.dep = this.currentOrg.dep - 0 + 1 + '';
    }else {
      this.title = '修改设备类型';
    }
  }

  closeOrgEditMask(bool){
    this.closeOrgEdit.emit(bool);
  }

  //确定
  formSubmit(bool){
    if(this.state ==='add'){
      this.addOrgNode(bool);
    }else{
      this.updateOrgNode(bool);
    }
  }

  //新增组织节点前，获取节点信息
  getCurrentNodeData(){
    /*this.equipmentService.getCurrentNodeData(this.currentOrg.oid).subscribe(data=>{
      this.submitAddOrg.oid = data.oid;
      this.submitAddOrg.father = data.father;
      this.submitAddOrg.dep = data.dep;
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })*/
  }

  //新增组织节点
  addOrgNode(bool){
    this.equipmentService.addOrgNode(this.submitAddOrg).subscribe(()=>{
      this.addOrg.emit(bool);
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }

  //修改组织节点
  updateOrgNode(bool){
    this.submitUpdateOrg[0].name = this.currentOrg.name;
    this.submitUpdateOrg[0].remark = this.currentOrg.remark;
    /*this.equipmentService.updateOrgNode(this.submitUpdateOrg).subscribe(()=>{
      this.updateOrg.emit(this.currentOrg);
      this.closeOrgEdit.emit(bool);
    })*/
  }

}
