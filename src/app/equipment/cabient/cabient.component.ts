import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-cabient',
  templateUrl: './cabient.component.html',
  styleUrls: ['./cabient.component.scss']
})
export class CabientComponent implements OnInit {
  dataSource;
  info;
  constructor(
    private route:ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params=>{
      console.log(params)
      console.log(params['cabient'])
      this.dataSource = JSON.parse(params['cabient']);
      this.info = JSON.parse(params['info']);
    })
  }
  showPort(device){
    this.router.navigate([device.route],{queryParams:{device:JSON.stringify(device)},relativeTo:this.route})
  }
}
