import { NgModule } from '@angular/core';
import { EquipmentRoutingModule } from './equipment-routing.module';
import { ShareModule } from '../shared/share.module';
import { EquipmentViewComponent } from './equipment-view/equipment-view.component';
import { EquipmentInfoManagementComponent } from './equipment-info-management/equipment-info-management.component';
import { EquipmentSearchComponent } from './equipment-search/equipment-search.component';
import { ImportEquipmentComponent } from './import-equipment/import-equipment.component';
import { EquipmentService } from './equipment.service';
import { AddOrUpdateDeviceComponent } from './add-or-update-device/add-or-update-device.component';
import { ViewLedgerComponent } from './view-ledger/view-ledger.component';
import {FileUploadModule} from "ng2-file-upload";
import { ViewDetailComponent } from './view-detail/view-detail.component';
import { CabientComponent } from './cabient/cabient.component';
import { PortComponent } from './port/port.component';
import { AssetStatusComponent } from './asset-status/asset-status.component';
import { EngineRoomComponent } from './engine-room/engine-room.component';
import { LabelMangementComponent } from './label-mangement/label-mangement.component';
import { LabelSettingsComponent } from './label-settings/label-settings.component';
import { PrintTemplateComponent } from './print-template/print-template.component';
import { LabelPrintComponent } from './label-print/label-print.component';
import { AddOrUpdateLabelComponent } from './add-or-update-label/add-or-update-label.component';
import { Equipment3dComponent } from './equipment3d/equipment3d.component';
import { EquipmentEditComponent } from './equipment-edit/equipment-edit.component';
import { EquipmentTypeEditComponent } from './equipment-type-edit/equipment-type-edit.component';
import {EquipmentConfigComponent} from "./equipment-config/equipment-config.component";
import {EquipmentClassComponent} from "./equipment-class/equipment-class.component";

@NgModule({
  declarations: [
    EquipmentViewComponent,
    EquipmentInfoManagementComponent,
    EquipmentSearchComponent,
    ImportEquipmentComponent,
    AddOrUpdateDeviceComponent,
    ViewLedgerComponent,
    ViewDetailComponent,
    CabientComponent,
    PortComponent,
    AssetStatusComponent,
    EngineRoomComponent,
    LabelMangementComponent,
    LabelSettingsComponent,
    PrintTemplateComponent,
    LabelPrintComponent,
    AddOrUpdateLabelComponent,
    EquipmentClassComponent,
    EquipmentConfigComponent,
    EquipmentEditComponent,
    EquipmentTypeEditComponent,
    Equipment3dComponent,
  ],
  imports: [
    ShareModule,
    FileUploadModule,
    EquipmentRoutingModule,
  ],
  providers: [EquipmentService]
})
export class EquipmentModule {
}
