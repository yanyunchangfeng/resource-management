import {Component, OnDestroy, OnInit} from '@angular/core';
import {EquipmentService} from "../equipment.service";
import {ConfirmationService} from "primeng/primeng";
@Component({
  selector: 'app-equipment-view',
  templateUrl: './equipment-view.component.html',
  styleUrls: ['./equipment-view.component.scss']
})
export class EquipmentViewComponent implements OnInit ,OnDestroy{
  status = [];
  piedeviceStatus={};
  bardeviceStatus={};
  statusName = [];
  statusCount = [];
  engineRoom = [];
  engineRoomName = [];
  engineRoomCount = [];
  pieengieRoom = {};
  barengieRoom = {};
  constructor(
    private equipmentService:EquipmentService,
    private confirmationService:ConfirmationService
  ) {}

  ngOnInit() {
     this.equipmentService.queryEquipmentView().subscribe((data)=>{
       // this.status = data.status;
       // this.engineRoom = data.room;
       if(!this.status){
         this.status = [];
       }
       if(!this.engineRoom){
         this.engineRoom = [];
       }
       for(let i = 0; i < this.status.length;i++) {
         this.statusName.push(this.status[i].name);
         this.statusCount.push(this.status[i].count);
       }
       this.bardeviceStatus = {
         title: {
           // subtext: '资产状态',
           le: 'center'
         },
         color: ['#3398DB'],
         tooltip: {
           trigger: 'axis',
           axisPointer: {            // 坐标轴指示器，坐标轴触发有效
             type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
           },
           formatter: ""
         },
         grid: {
           left: '3%',
           right: '4%',
           bottom: '3%',
           containLabel: true
         },
         xAxis: [
           {
             type: 'category',
             // data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
             data: this.statusName,
             axisTick: {
               alignWithLabel: true
             }
           }
         ],
         yAxis: [
           {
             type: 'value'
           }
         ],
         series: [
           {
             name: '数量',
             type: 'bar',
             // barWidth: '60%',
             barCategoryGap: '50%',
             // data: [10, 52, 200, 334, 390, 330, 220, 1000, 500, 444, 999, 11]
             data: this.statusCount
           }
         ]
       };
       this.piedeviceStatus = {
         theme: '',
         event: [
           {
             type: "click",
             cb: function (res) {
               console.log(res);
             }
           }
         ],
         title: {
           text: '',
           // subtext: '饼状图',
           x: 'center'
         },
         tooltip: {
           trigger: 'item',
           formatter: "{a} <br/>{b} : {c} ({d}%)"
         },
         legend: {
           orient: 'vertical',
           left: 'left',
           // data: ['深圳', '北京', '广州', '上海', '长沙']
           data: this.statusName
         },
         series: [{
           name: '访问来源',
           type: 'pie',
           startAngle: -180,
           radius: '55%',
           center: ['50%', '60%'],
           data: this.equipmentService.formatPieData(this.status),
           itemStyle: {
             emphasis: {
               shadowBlur: 10,
               shadowOffsetX: 0,
               shadowColor: 'rgba(0, 0, 0, 0.5)'
             }
           }
         }]
       };
       for(let i = 0;i<this.engineRoom.length;i++){
         this.engineRoomName.push(this.engineRoom[i].name);
         this.engineRoomCount.push(this.engineRoom[i].count);
       }
       this.barengieRoom = {
         title: {
           // subtext: '资产状态',
           x: 'center'
         },
         color: ['#3398DB'],
         tooltip: {
           trigger: 'axis',
           axisPointer: {            // 坐标轴指示器，坐标轴触发有效
             type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
           },
           formatter: ""
         },
         grid: {
           left: '3%',
           right: '4%',
           bottom: '3%',
           containLabel: true
         },
         xAxis: [
           {
             type: 'category',
             // data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
             data: this.engineRoomName,
             axisTick: {
               alignWithLabel: true
             }
           }
         ],
         yAxis: [
           {
             type: 'value'
           }
         ],
         series: [
           {
             name: '数量',
             type: 'bar',
             barWidth: '60%',
             // data: [10, 52, 200, 334, 390, 330, 220, 1000, 500, 444, 999, 11]
             data: this.engineRoomCount
           }
         ]
       }
       this.pieengieRoom = {
         theme: '',
         event: [
           {
             type: "click",
             cb: function (res) {
               console.log(res);
             }
           }
         ],
         title: {
           text: '',
           // subtext: '饼状图',
           x: 'center'
         },
         tooltip: {
           trigger: 'item',
           formatter: "{a} <br/>{b} : {c} ({d}%)"
         },
         legend: {
           orient: 'vertical',
           left: 'left',
           // data: ['深圳', '北京', '广州', '上海', '长沙']
           data: this.engineRoomName
         },
         series: [{
           name: '访问来源',
           type: 'pie',
           startAngle: -180,
           radius: '55%',
           center: ['50%', '60%'],
           data: this.equipmentService.formatPieData(this.engineRoom),
           itemStyle: {
             emphasis: {
               shadowBlur: 10,
               shadowOffsetX: 0,
               shadowColor: 'rgba(0, 0, 0, 0.5)'
             }
           }
         }]
       }
     },(err:Error)=>{
       let message ;
       if(JSON.parse(JSON.stringify(err)).status === 0|| JSON.parse(JSON.stringify(err)).status === 504){
         message = '似乎网络出现了问题，请联系管理员或稍后重试'
       }else{
         message = err
       }
       this.confirmationService.confirm({
         message: message,
         rejectVisible:false,
       })
     })
  }
  ngOnDestroy(): void {

  }
}
