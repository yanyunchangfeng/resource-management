import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrUpdateDeviceComponent } from './add-or-update-device.component';

describe('AddOrUpdateDeviceComponent', () => {
  let component: AddOrUpdateDeviceComponent;
  let fixture: ComponentFixture<AddOrUpdateDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrUpdateDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrUpdateDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
