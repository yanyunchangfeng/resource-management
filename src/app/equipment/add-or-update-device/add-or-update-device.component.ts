import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EquipmentService} from "../equipment.service";
import {ConfirmationService} from "primeng/primeng";
import {MessageService} from "primeng/components/common/messageservice";
import {BasePage} from "../../base.page";

@Component({
  selector: 'app-add-or-update-device',
  templateUrl: './add-or-update-device.component.html',
  styleUrls: ['./add-or-update-device.component.scss']
})
export class AddOrUpdateDeviceComponent extends BasePage implements OnInit {
  display:boolean = false;
  zh;
  width;
  windowSize;
  @Output() closeAddMask = new EventEmitter();
  @Output() updateDev = new EventEmitter();
  @Output() addDev = new EventEmitter();
  @Input() currentAsset;
  @Input() state;
  title = '修改设备';
  submitAddAsset;
  qureyModel = {
    field:'',
    value:''
  };
  brandoptions;
  modleoptions;
  functionoptions;
  systemoptions;
  statusoptions;
  sizeoptions;
  roomoptions;
  cabinetoptions;
  locationoptions;
  belong_dapartoptions;
  belong_dapart_manageroptions;
  manage_dapartoptions;
  manageroptions;
  power_leveloptions;
  power_typeoptions;
  plug_typeoptions;
  power_redundant_modeloptions;
  power_sourceoptions;
  power_jack_position1options;
  power_jack_position2options;
  power_jack_position3options;
  power_jack_position4options;
  operating_systemoptions;
  maintain_venderoptions;
  maintain_vender_peopleoptions;
  constructor(
    private equipmentService:EquipmentService,
    public confirmationService:ConfirmationService,
    public messageService:MessageService
  ) {
    super(confirmationService,messageService)
  }
  ngOnInit(){
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.currentAsset =this.equipmentService.deepClone(this.currentAsset);
    console.log(this.currentAsset);
    if(this.state === 'add'){
      this.title = '添加设备'
    }
    this.display = true;
    this.submitAddAsset = {
      "ano":"",
      "father":"",
      "card_position":"",
      "asset_no":"",
      "name":"",
      "en_name":"",
      "brand":"",
      "modle":"",
      "function":"",
      "system":"",
      "status":"",
      "on_line_datetime":"",
      "size":"",
      "room":"",
      "cabinet":"",
      "location":"",
      "belong_dapart":"",
      "belong_dapart_manager":" ",
      "belong_dapart_phone":"",
      "manage_dapart":"",
      "manager":" ",
      "manager_phone":"",
      "power_level":"",
      "power_type":"",
      "power_rate":"",
      "plug_type":"",
      "power_count":"",
      "power_redundant_model":"",
      "power_source":"",
      "power_jack_position1":"",
      "power_jack_position2":"",
      "power_jack_position3":"",
      "power_jack_position4":"",
      "ip":"",
      "operating_system":"",
      "up_connect":"",
      "down_connects":"",
      "maintain_vender":"",
      "maintain_vender_people":"",
      "maintain_vender_phone":"",
      "maintain_starttime":"",
      "maintain_endtime":"",
      "function_info":"",
      "remarks":""
    };
  }
  closeAddOrUpdateMask(bool){
    this.closeAddMask.emit(bool)
  }
  querySuggestData(obj,name){
    for(let key in obj){
      obj[key]+="";
      obj[key]= obj[key].trim();
    }
    this.equipmentService.searchChange(obj).subscribe(
      data=>{
        switch (name){
          case 'brand':
            this.brandoptions = data?data:[];
            break;
          case 'modle':
            this.modleoptions = data?data:[];
            break;
          case 'function':
            this.functionoptions = data?data:[];
            break;
          case 'system':
            this.systemoptions = data?data:[];
            break;
          case 'status':
            this.statusoptions = data?data:[];
            break;
          case 'size':
            this.sizeoptions = data?data:[];
            break;
          case 'room':
            this.roomoptions = data?data:[];
            break;
          case 'cabinet':
            this.cabinetoptions = data?data:[];
            break;
          case 'location':
            this.locationoptions = data?data:[];
            break;
          case 'belong_dapart':
            this.belong_dapartoptions = data?data:[];
            break;
          case 'belong_dapart_manager':
            this.belong_dapart_manageroptions = data?data:[];
            break;
          case 'manage_dapart':
            this.manage_dapartoptions = data?data:[];
            break;
          case 'manager':
            this.manageroptions = data?data:[];
            break;
          case 'power_level':
            this.power_leveloptions = data?data:[];
            break;
          case 'power_type':
            this.power_typeoptions = data?data:[];
            break;
          case 'plug_type':
            this.plug_typeoptions = data?data:[];
            break;
          case 'power_redundant_model':
            this.power_redundant_modeloptions = data?data:[];
            break;
          case 'power_source':
            this.power_sourceoptions = data?data:[];
            break;
          case 'power_jack_position1':
            this.power_jack_position1options = data?data:[];
            break;
          case 'power_jack_position2':
            this.power_jack_position2options = data?data:[];
            break;
          case 'power_jack_position3':
            this.power_jack_position3options = data?data:[];
            break;
          case 'power_jack_position4':
            this.power_jack_position4options = data?data:[];
            break;
          case 'operating_system':
            this.operating_systemoptions = data?data:[];
            break;
          case 'maintain_vender':
            this.maintain_venderoptions = data?data:[];
            break;
          case 'maintain_vender_people':
            this.maintain_vender_peopleoptions = data?data:[];
        }
      }
    )
  }
  searchSuggest(searchText,name){
    this.qureyModel={
      field:name,
      value:searchText.query
    }
    this.querySuggestData(this.qureyModel,name);
  }

  updateDevice(bool){
    for(let key in this.currentAsset){
      this.currentAsset[key] = String(this.currentAsset[key]).trim();
    }
     this.equipmentService.updateAssets(this.currentAsset).subscribe(()=>{
       this.updateDev.emit(bool);
     },(err:Error)=>{
       this.toastError(err);
     })
  }
  addDevice(bool){
    for(let key in this.submitAddAsset){
      this.submitAddAsset[key] = this.submitAddAsset[key].trim();
    }
    this.equipmentService.addAssets(this.submitAddAsset).subscribe(()=>{
       this.addDev.emit(this.submitAddAsset);
       this.closeAddMask.emit(bool)
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，请联系管理员或稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
  closeMask(bool){
    this.closeAddMask.emit(bool)
  }
  formSubmit(bool){
    if(this.state=='update'){
      this.updateDevice(bool)
    }else{
      this.addDevice(bool)
    }
  }
}
