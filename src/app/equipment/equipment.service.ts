import { Injectable } from '@angular/core';
// import {Http,Response} from "@angular/http"
import {HttpClient} from "@angular/common/http";
import {StorageService} from "../services/storage.service";
import {environment} from "../../environments/environment";
import { of } from 'rxjs/observable/of';
declare var $:any;
@Injectable()
export class EquipmentService {
  constructor(private http:HttpClient,private  storageService: StorageService) {}
  //查询资产
  getAssets() {
       return of(null)
    }
  //添加资产
  addAssets(asset){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/asset`,{
      "access_token":token,
      "type":"asset_addorupd",
      "datas": {
        "number":asset.number,
        "ano":asset.ano,
        "father":asset.father,
        "card_position":asset.card_position,
        "asset_no":asset.asset_no,
        "name":asset.name,
        "en_name":asset.en_name,
        "brand":asset.brand,
        "modle":asset.modle,
        "function":asset.function,
        "system":asset.system,
        "status":asset.status,
        "on_line_datetime":asset.on_line_datetime,
        "size":asset.size,
        "room":asset.room,
        "cabinet":asset.cabinet,
        "location":asset.location,
        "belong_dapart":asset.belong_dapart,
        "belong_dapart_manager":asset.belong_dapart_manager,
        "belong_dapart_phone":asset.belong_dapart_phone,
        "manage_dapart":asset.manage_dapart,
        "manager":asset.manager,
        "manager_phone":asset.manager_phone,
        "power_level":asset.power_level,
        "power_type":asset.power_type,
        "power_rate":asset.power_rate,
        "plug_type":asset.plug_type,
        "power_count":asset.power_count,
        "power_redundant_model":asset.power_redundant_model,
        "power_source":asset.power_source,
        "power_jack_position1":asset.power_jack_position1,
        "power_jack_position2":asset.power_jack_position2,
        "power_jack_position3":asset.power_jack_position3,
        "power_jack_position4":asset.power_jack_position4,
        "ip":asset.ip,
        "operating_system":asset.operating_system,
        "up_connect":asset.up_connect,
        "down_connects":asset.down_connects,
        "maintain_vender":asset.maintain_vender,
        "maintain_vender_people":asset.maintain_vender_people,
        "maintain_vender_phone":asset.maintain_vender_phone,
        "maintain_starttime":asset.maintain_starttime,
        "maintain_endtime":asset.maintain_endtime,
        "function_info":asset.function_info,
        "remarks":asset.remarks,
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas
    })
  }
  //修改
  updateAssets(asset) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/asset`, {
      access_token:token,
      type: "asset_addorupd",
      datas:{
        id:Number(asset.id),
        number:asset.number,
        ano:asset.ano,
        father:asset.father,
        card_position:asset.card_position,
        asset_no:asset.asset_no,
        name:asset.name,
        en_name:asset.en_name,
        brand:asset.brand,
        modle:asset.modle,
        function:asset.function,
        system:asset.system,
        status:asset.status,
        on_line_datetime:asset.on_line_datetime,
        size:asset.size,
        room:asset.room,
        cabinet:asset.cabinet,
        location:asset.location,
        belong_dapart:asset.belong_dapart,
        belong_dapart_manager:asset.belong_dapart_manager,
        belong_dapart_phone:asset.belong_dapart_phone,
        manage_dapart:asset.manage_dapart,
        manager:asset.manager,
        manager_phone:asset.manager_phone,
        power_level:asset.power_level,
        power_type:asset.power_type,
        power_rate:asset.power_rate,
        plug_type:asset.plug_type,
        power_count:asset.power_count,
        power_redundant_model:asset.power_redundant_model,
        power_source:asset.power_source,
        power_jack_position1:asset.power_jack_position1,
        power_jack_position2:asset.power_jack_position2,
        power_jack_position3:asset.power_jack_position3,
        power_jack_position4:asset.power_jack_position4,
        ip:asset.ip,
        operating_system:asset.operating_system,
        up_connect:asset.up_connect,
        down_connects:asset.down_connects,
        maintain_vender:asset.maintain_vender,
        maintain_vender_people:asset.maintain_vender_people,
        maintain_vender_phone:asset.maintain_vender_phone,
        maintain_starttime:asset.maintain_starttime,
        maintain_endtime:asset.maintain_endtime,
        function_info:asset.function_info,
        remarks:asset.remarks,
      }
    }).map((res:Response) => {
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
    })
  }
  //删除discard
  deleteAsset(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/asset`, {
      "access_token":token,
      "type": "asset_del",
      "ids":ids
    }).map((res:Response) => {
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
    })
  }
  //搜索建议
  searchChange(queryModel){
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/asset`,{
      "access_token":token,
      "type":"asset_diffield_get",
      "datas":{
        "field":queryModel.field,
        "value":queryModel.value
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
       // let body = res.json();
       // if(body.errcode!=='00000'){
       //   throw new Error(body.errmsg)
       // }
       // return body.datas;
    })
  }
  //查询资产台账
  queryViewLedger(asset){
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/asset`,{
      "access_token":token,
      "type":"asset_record_get",
      "datas":{
        "ano":asset.ano,
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
        // let body = res.json();
        // if(body.errcode!=='00000'){
        //   throw new Error(body.errmsg)
        // }
        // return body.datas
    })
  }
  queryEquipmentView(){
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/asset`,{
      "access_token":token,
      "type":"asset_statistics",
      "datas":[
        "status",
        "room"
      ]
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.msg)
      // }
      // return body.datas
    })
  }
  //查找功能
  queryByKeyWords(asset){
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/asset`,{
      "access_token":token,
      "type":"asset_get",
      "datas":{
        "ano":asset.ano,
        "father":asset.father,
        "card_position":asset.card_position,
        "asset_no":asset.asset_no,
        "name":asset.name,
        "brand":asset.brand,
        "modle":asset.modle,
        "function":asset.function,
        "system":asset.system,
        "status":asset.status,
        "on_line_date_start":asset.on_line_date_start,
        "on_line_date_end":asset.on_line_date_end,
        "room":asset.room,
        "cabinet":asset.cabinet,
        "location":asset.location,
        "belong_dapart":asset.belong_dapart,
        "belong_dapart_manager":asset.belong_dapart_manager,
        "manage_dapart":asset.manage_dapart,
        "manager":asset.manager,
        "ip":asset.ip,
        "maintain_vender":asset.maintain_vender
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas
    })
  }
  formatPieData(arr){
    let newarr= [];
    for(let i = 0;i<arr.length;i++){
      let obj = {};
      let def = arr[i];
      obj['name'] =def ['name'];
      obj['value'] = def['count'];
      newarr.push(obj)
    }
    return newarr
  }
    deepClone(obj){
      var o,i,j,k;
      if(typeof(obj)!="object" || obj===null)return obj;
      if(obj instanceof(Array))
      {
        o=[];
        i=0;j=obj.length;
        for(;i<j;i++)
        {
          if(typeof(obj[i])=="object" && obj[i]!=null)
          {
            o[i]=this.deepClone(obj[i]);
          }
          else
          {
            o[i]=obj[i];
          }
        }
      }
      else
      {
        o={};
        for(i in obj)
        {
          if(typeof(obj[i])=="object" && obj[i]!=null)
          {
            o[i]=this.deepClone(obj[i]);
          }
          else
          {
            o[i]=obj[i];
          }
        }
      }
      return o;
    }
  //机柜接口
  queryCabient(){
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/asset`,{
      "access_token":token,
      "type":"asset_get",
      "datas":{
        "father":"0",
        "asset_type":'机柜'
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas
    })
  }

  //获取资产管理树的数据
  getAssetManTreeDatas(did?, dep?) {
    (!did) && (did = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/foundationdata`,
      {
        'access_token': token,
        'type': 'asset_tree_get',
        'data': {
          'dep': dep,
          'did': did
        }
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return [];
      }else {
        res['datas'].forEach(function (e) {
          e.label = e.name;
          delete e.name;
          e.leaf = false;
          e.data = e.name;
        });
      }
      return res['datas'];


    });

  }

  //    新增配置信息
  addConfig(name, status, father, dep) {
    (!name) && (name = '');
    (!status) && (status = '');
    (!father) && (father = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/foundationdata`,
      {
        'access_token': token,
        'type': 'asset_tree_add',
        'data': {
          /*'name': name,
          'remark': '',
          'del': '',
          'dep': dep,
          'father': father,
          'status': status,*/

          "name": name,
          "dep": dep,
          "father": father,
          "remark": "",
          "status": status,
          "attribute1": "",
          "attribute2": ""

        }
      }
    ).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

  //    编辑/冻结/启用配置信息
  editConfig(name, status, did, dep, father) {
    (!name) && (name = '');
    (!status) && (status = '');
    (!did) && (did = '');
    (!dep) && (dep = '');
    (!father) && (father = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/foundationdata`,
      {
        'access_token': token,
        'type': 'asset_tree_mod',
        'data': {
          'did': did,
          'name': name,
          'remark': '',
          'del': '',
          'dep': dep,
          'father': father,
          'status': status,
        }
      }
    ).map((res: Response) => {
      if (res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

  //    删除配置信息
  deleteConfig(ids) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/foundationdata`,
      {
        'access_token': token,
        'type': 'asset_tree_del',
        'ids': ids
      }
    ).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }


  //获取设备类型树数据
  getOrgTree(oid){
    if(!oid){
      oid = '0';
    }
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/asset`, {
      "access_token": token,
      "type": "get_asset_type",
      "data": {
        "father": oid
      }
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      //构造primeNG tree的数据
      let datas = res['data'];
      for(let i = 0;i < datas.length;i++){
        datas[i].label = datas[i].name;
        datas[i].leaf = false;
      }
      return datas;
    })
  }

  //获取人员数据
  getPersonList(oid){
    let token = this.storageService.getToken('token');
    let oids = [];
    if(oid){
      oids.push(oid)
    }else {
      oid = "1";
    }
    return this.http.post(`${environment.url.management}/asset`, {
      "access_token": token,
      "type": "get_asset_model",
      "data": {
        "asset_type_id": oid
      }

    }).map((res: Response) => {
      if(res['errcode'] == '00001'){//没有人员
        return [];
      }else if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }

  //新增组织节点前，获取节点信息
  getCurrentNodeData(id){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      "access_token": token,
      "type": "add_org_id",
      "id": id
      // "id": treeNode.oid
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }

  //判断用户是否有操作组织节点的权限
  judgeOrgEditPower(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      "access_token": token,
      "type": "jur"
    }).map((res: Response) => {
      // console.log(res);
      return res;
    })
  }

  //新增组织节点
  addOrgNode(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/asset`, {
      "access_token": token,
      "type": "add_asset_type",
      "data":data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //修改组织节点
  updateOrgNode(datas){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      "access_token": token,
      "type": "mod_org",
      "datas":datas
      /*"datas": [{
        "oid": $scope.curModifyId,
        "name": $scope.orgName,
        "remark": $scope.orgRemark,
        "dep": $scope.dep,
        "father": $scope.father
      }]*/
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //删除组织节点
  deleteOrgNode(ids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/asset`, {
      "access_token": token,
      "type": "del_asset_type",
      "ids": ids
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['errcode'];
    })
  }

  //判断用户是否有操作人员的权限
  judgePerEditPower(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      "access_token": token,
      "type": "jur"
    }).map((res: Response) => {
      return res;
    })
  }

  //新增人员
  addPerson(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/asset`, {
      "access_token": token,
      "type": 'add_asset_model',
      "data": data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //修改人员
  updatePerson(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      "access_token": token,
      "type": "mod_personnel",
      "data":data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //删除人员
  deletePerson(pids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/asset`, {
      "access_token": token,
      "type": "del_asset_model",
      "ids": pids
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['errcode'];
    })
  }

}
