import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {EquipmentService} from "../equipment.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-equipment3d',
  templateUrl: './equipment3d.component.html',
  styleUrls: ['./equipment3d.component.css']
})
export class Equipment3dComponent implements OnInit {
  private fm: any;
  @ViewChild("fm") fmElem: ElementRef;
  private ip;

  constructor(
    private http: HttpClient,
    private equipmentService: EquipmentService
  ) { }

  ngOnInit() {
    const ip = environment.url.management;
  }

  tiaozhuan3D(){
    this.fm = this.fmElem.nativeElement;
    this.fm.submit();
  }

}
