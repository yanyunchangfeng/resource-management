import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Equipment3dComponent } from './equipment3d.component';

describe('Equipment3dComponent', () => {
  let component: Equipment3dComponent;
  let fixture: ComponentFixture<Equipment3dComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Equipment3dComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Equipment3dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
