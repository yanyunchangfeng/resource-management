import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ImportEquipmentComponent} from './import-equipment/import-equipment.component';
import {EquipmentSearchComponent} from './equipment-search/equipment-search.component';
import {EquipmentInfoManagementComponent} from './equipment-info-management/equipment-info-management.component';
import {EquipmentViewComponent} from './equipment-view/equipment-view.component';
import {ViewLedgerComponent} from './view-ledger/view-ledger.component';
import {CabientComponent} from './cabient/cabient.component';
import {PortComponent} from './port/port.component';
import {LabelMangementComponent} from './label-mangement/label-mangement.component';
import {LabelSettingsComponent} from './label-settings/label-settings.component';
import {PrintTemplateComponent} from './print-template/print-template.component';
import {LabelPrintComponent} from './label-print/label-print.component';
import {Equipment3dComponent} from './equipment3d/equipment3d.component';
import {EquipmentConfigComponent} from './equipment-config/equipment-config.component';
import {EquipmentClassComponent} from './equipment-class/equipment-class.component';

const route: Routes = [
  {path: '', component: EquipmentViewComponent},
  {path: 'import', component: ImportEquipmentComponent},
  {path: 'search', component: EquipmentSearchComponent},
  {path: 'info', component: EquipmentInfoManagementComponent, children: [
      {path: 'cabinet', component: CabientComponent},
      {path: 'port', component: PortComponent}
    ]},
  {path: 'view', component: ViewLedgerComponent},
  {path: 'label', component: LabelMangementComponent, children: [
    {path: 'settings', component: LabelSettingsComponent},
    {path: 'template', component: PrintTemplateComponent},
    {path: 'print', component: LabelPrintComponent},
    ]},
  {path: 'class', component: EquipmentClassComponent},
  {path: 'config', component: EquipmentConfigComponent},
  {path: '3D', component: Equipment3dComponent},
  ];
@NgModule({
  imports: [RouterModule.forChild(route),
  ],
  exports: [RouterModule]
})
export class EquipmentRoutingModule {

}
