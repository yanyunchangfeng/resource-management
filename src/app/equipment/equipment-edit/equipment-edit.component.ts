import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {StorageService} from "../../services/storage.service";
import {ConfirmationService} from "primeng/primeng";
import {EquipmentService} from "../equipment.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-equipment-edit',
  templateUrl: './equipment-edit.component.html',
  styleUrls: ['./equipment-edit.component.css']
})
export class EquipmentEditComponent implements OnInit {
  imgUrl_1;//图片一预览base64
  imgUrl_2;//图片二预览base64
  display;
  title;
  @Output() closePersonEdit =new EventEmitter();
  @Output() addPer = new EventEmitter(); //新增人员
  @Output() updatePer = new EventEmitter(); //修改人员
  @Input() currentPer;
  @Input() state;
  person: FormGroup;
  u_height;
  power;
  weight;
  maxDate: Date;//注册日期的最大值，今天
  minDate: Date;//失效日期的最小值，今天
  uploadedFiles: any[] = [];
  submitAddPerson = //新增人员参数
    {
      "asset_name": "",
      "u_height": "",
      "weight": "",
      "power": "",
      "asset_type_id": ""
    };
  submitUpdatePerson = [//修改人员参数
    {
      "pid": '',
      "oid": '',
      "name": '',
      "sex": '',
      "birthday": '',
      "registedate": '',
      "validdate": '',
      "organization": '',
      "post": '',
      "tel": '',
      "mobile": '',
      "addr": '',
      "email": '',
      "idcard": '',
      "license": '',
      "wechat": '',
      "photo1": '',
      "photo2": '',
      "remark1": '',
      "remark2": '',
      "remark3": '',
      "remark4": ''
    }
  ];

  constructor(
    private equipmentService: EquipmentService,
    private storageService: StorageService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    private sanitizer: DomSanitizer
  ) {
    this.person = fb.group({
      father_id: [''],
      asset_name: [''],
      u_height: [''],
      weight: [''],
      power: [''],
      asset_type_id: [''],
      }
    )
  }

  ngOnInit() {
    this.display = true;
    this.u_height = '1';
    this.power = '50';
    this.weight = '50';
    //日期格式转换为yyyy-mm-dd
    /*if(this.currentPer.birthday){
      this.currentPer.birthday = PUblicMethod.formateDateTime(this.currentPer.birthday);
    }
    if(this.currentPer.registedate){
      this.currentPer.registedate = PUblicMethod.formateDateTime(this.currentPer.registedate);
    }
    if(this.currentPer.validdate){
      this.currentPer.validdate = PUblicMethod.formateDateTime(this.currentPer.validdate);
    }*/
    this.minDate = new Date();
    this.maxDate = new Date();
    console.log(this.currentPer);
    /*this.submitAddPerson[0].oid = this.currentPer.oid;
    this.submitUpdatePerson[0].oid = this.currentPer.oid;
    this.submitAddPerson[0].organization = this.currentPer.label;*/
    if(this.state ==='add') {
      this.title = '添加设备型号';
      // this.person.s;
    }else {
      //图片赋初始值
      this.imgUrl_1 = this.currentPer.photo1;
      this.imgUrl_2 = this.currentPer.photo2;
      this.submitUpdatePerson[0].photo1 = this.currentPer.photo1;
      this.submitUpdatePerson[0].photo2 = this.currentPer.photo2;
      this.title = '修改设备型号';
    }
  }

  closePersonEditMask(bool){
    this.closePersonEdit.emit(bool);
  }

  //确定
  formSubmit(bool){
    if(this.state ==='add'){
      this.submitAddPerson.asset_name = this.person.value.asset_name;
      this.submitAddPerson.u_height = this.person.value.u_height;
      this.submitAddPerson.weight = this.person.value.weight;
      this.submitAddPerson.power = this.person.value.power;
      this.submitAddPerson.asset_type_id = this.person.value.asset_type_id;
      this.addPerson(bool);
    }else{
      /*this.submitUpdatePerson[0].name = this.person.value.perName;
      this.submitUpdatePerson[0].pid = this.person.value.pid;
      this.submitUpdatePerson[0].mobile = this.person.value.mobile;
      this.submitUpdatePerson[0].addr = this.person.value.addr;
      this.submitUpdatePerson[0].sex = this.person.value.sex;
      this.submitUpdatePerson[0].birthday = this.person.value.birthday;
      this.submitUpdatePerson[0].registedate = this.person.value.dateGroup.registedate;
      this.submitUpdatePerson[0].validdate = this.person.value.dateGroup.validdate;
      this.submitUpdatePerson[0].organization = this.person.value.organization;
      this.submitUpdatePerson[0].post = this.person.value.post;
      this.submitUpdatePerson[0].tel = this.person.value.tel;
      this.submitUpdatePerson[0].email = this.person.value.email;
      this.submitUpdatePerson[0].idcard = this.person.value.idcard;
      this.submitUpdatePerson[0].wechat = this.person.value.wechat;
      this.submitUpdatePerson[0].remark1 = this.person.value.remark1;*/
      this.updatePerson(bool);
    }
  }

  //新增人员
  addPerson(bool){
    this.equipmentService.addPerson(this.submitAddPerson).subscribe(()=>{
      this.addPer.emit(bool);
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }

  //修改人员
  updatePerson(bool){
    console.log(this.submitUpdatePerson[0]);
    /*this.equipmentService.updatePerson(this.submitUpdatePerson).subscribe(()=>{
      this.updatePer.emit(this.currentPer);
      this.closePersonEdit.emit(bool);
    })*/
  }

  //图片上传
  fileChange(event, num){
    let that = this;
    let file = event.target.files[0];
    let imgUrl = window.URL.createObjectURL(file);
    let sanitizerUrl = this.sanitizer.bypassSecurityTrustUrl(imgUrl);
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function(theFile) {
      if(num == 1){
        that.submitAddPerson[0].photo1 = theFile.target['result']  //base64编码,用一个变量存储
        that.submitUpdatePerson[0].photo1 = theFile.target['result']  //base64编码,用一个变量存储
      }else {
        that.submitAddPerson[0].photo2 = theFile.target['result']  //base64编码,用一个变量存储
        that.submitUpdatePerson[0].photo2 = theFile.target['result']  //base64编码,用一个变量存储
      }
    };
    if(num == 1){
      this.imgUrl_1 = sanitizerUrl;
    }else {
      this.imgUrl_2 = sanitizerUrl;
    }
  }

  //输入身份证，获取生日日期
  idcardChange(e){
    if(this.person.get('idcard').valid){
      let birthday = this.getBirthday(e.target.value);
      this.currentPer.birthday = birthday;
    }
  }

  //根据身份证获取生日日期
  getBirthday(psidno){
    var birthdayno,birthdaytemp
    if(psidno.length==18){
      birthdayno=psidno.substring(6,14)
    }else if(psidno.length==15){
      birthdaytemp=psidno.substring(6,12)
      birthdayno="19"+birthdaytemp
    }else{
      alert("错误的身份证号码，请核对！")
      return false
    }
    var birthday=birthdayno.substring(0,4)+"-"+birthdayno.substring(4,6)+"-"+birthdayno.substring(6,8)
    return birthday
  }

}
