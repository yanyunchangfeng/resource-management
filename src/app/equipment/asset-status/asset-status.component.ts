import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
import Color from '../../util/color.util';
import {EquipmentService} from "../equipment.service";
import {IntervalObservable} from "rxjs/observable/IntervalObservable";
@Component({
  selector: 'app-asset',
  templateUrl: './asset-status.component.html',
  styleUrls: ['./asset-status.component.css']
})
export class AssetStatusComponent implements OnInit,OnDestroy {
  @ViewChild('asset') asset: ElementRef;
  echart;
  status;
  intervalObser;
  statusName = [];
  statusCount = [];
  public opt: any = {
    color: ['#6acece', '#6acece', '#6acece'],
    title : {
      top:20,
      text:'资产状态柱状图展示',
      // subtext: '资产状态柱状图展示',
      x:'center',
      textStyle:{//标题内容的样式
        color:'#333333',//京东红
        fontStyle:'normal',//主标题文字字体风格，默认normal，有italic(斜体),oblique(斜体)
        fontWeight:"bold",//可选normal(正常)，bold(加粗)，bolder(加粗)，lighter(变细)，100|200|300|400|500...
        // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
        fontSize:14//主题文字z字体大小，默认为18px
      },
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // type: 'cross'
        type: 'shadow'
      }
    },
    xAxis: [
      {
        type: 'category',
        data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
        axisTick: {
          alignWithLabel: true
        }
      }
    ],
    yAxis: [{
      // name: '/万元',
      // min: 0,
      // max: 10000,
      // splitNumber: 10,
      // axisLabel: {
      //   formatter: function (value) {
      //     return (value / 10000);
      //   }
      // },
      type: 'value'

    }],
    series: [{
      name: '数量',
      type: 'bar',
      barCategoryGap: '50%',
      itemStyle: {
        normal: {
          color: params => {
            const color = Color.genColor(this.opt.series[0].data);
            return color[params.dataIndex];
          }
        }
      },
      data: [123, 110,370, 780, 780, 440,370, 780, 790, 440,268,1000]
    }]
  };
  constructor(private equipmentService:EquipmentService) { }

  ngOnInit() {
    this.echart = echarts.init(this.asset.nativeElement);
    this.echart.setOption(this.opt);
    this.equipmentService.queryEquipmentView().subscribe(data=>{
      // this.status = data.status;
      // if(!this.status){
      //   this.status = [];
      // }
      // for(let i = 0; i < this.status.length;i++) {
      //   this.statusName.push(this.status[i].name);
      //   this.statusCount.push(this.status[i].count);
      // }
      // this.opt.series[0]['data'] = this.statusCount;
      // this.opt.xAxis[0]['data'] = this.statusName;
      // this.echart.setOption(this.opt);

    })
     this.intervalObser = IntervalObservable.create(100).subscribe(index=>{
        this.initWidth();
      })

  }
  initWidth(){
    this.echart.resize();
    this.intervalObser.unsubscribe();
  }
  ngOnDestroy(): void {
      this.echart.dispose(this.asset.nativeElement);
      this.echart = null;
  }
  @HostListener('window:resize', ['$event'])
  public onWindowResize(event): void {
    this.echart.resize()
  }
}
