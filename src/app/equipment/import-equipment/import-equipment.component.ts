import {Component, OnInit} from '@angular/core';
import {FileUploader} from 'ng2-file-upload';
import {environment} from '../../../environments/environment';
import {ConfirmationService} from 'primeng/primeng';

@Component({
  selector: 'app-import-equipment',
  templateUrl: './import-equipment.component.html',
  styleUrls: ['./import-equipment.component.scss']
})
export class ImportEquipmentComponent implements OnInit {
  cols;
  display = false;
  totalRecords: number;
  assets;
  showImportMask: boolean = false;
  showAddMask: boolean = false;
  state = 'add';
  uploader: FileUploader;
  hasBaseDropZoneOver: boolean = false;
  hasAnotherDropZoneOver: boolean = false;
  dataSource;
  constructor(private confirmationService: ConfirmationService ) {
  }
  ngOnInit() {
    this.cols = [
        {field: 'ano', header: '设备编码'},
        // {field: 'father', header: '从属于'},
        {field: 'asset_no', header: '固定资产编号'},
        {field: 'name', header: '设备名称'},
        {field: 'status', header: '设备状态'},
        {field: 'room', header: '机房'},
        {field: 'cabinet', header: '机柜'},
        {field: 'location', header: 'U位'},
        {field: 'ip', header: '管理IP'}
    ];
    this.uploader = new FileUploader({url: `${environment.url.management}/asset/upload`})
    this.uploader.onCompleteItem = (item: any, response:any, status:any, headers:any) => {
      console.log(response);
      if(response.search("{")===-1) {
        this.confirmationService.confirm({
          message: '请导入excel文件',
          rejectVisible:false,
        })
        return
      }
      let body = JSON.parse(response);
      if(body.errcode==='00000'){
        this.confirmationService.confirm({
          message: '导入数据成功！',
          rejectVisible:false,
        })
      }else{
        this.confirmationService.confirm({
          message:body['errmsg'],
          rejectVisible:false
        })
        return
      }
      this.dataSource = body.datas;
      if(this.dataSource){
        this.totalRecords = this.dataSource.length;
        this.assets = this.dataSource.slice(0,10);
      }else{
        this.totalRecords = 0;
        this.assets = []
      }
    }
  }
  /**
   *批量上传功能
   **/

  public fileOverBase(e:any):void {
    console.log(e)
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e:any):void {
    console.log(e)
    this.hasAnotherDropZoneOver = e;
  }
  //批量导入
  showImport(){
    // this.showImportMask = !this.showImportMask;
    this.display = true;
  }
  //添加
  showAdd(){
    this.showAddMask = !this.showAddMask;
  }
  removeMask(bool){
    this.showAddMask = bool;
  }
  exportExcel(){
    window.location.href = `${environment.url.management}/data/file/asset/download/中心机房设备基础数据统计表.xlsx`
  }
  addDevice(asset){
    this.dataSource = [];
    this.dataSource.push(asset);
    this.totalRecords = this.dataSource.length;
    this.assets = this.dataSource.slice(0,10);
    this.confirmationService.confirm({
      message: '添加成功！',
      rejectVisible:false,
    })
  }
  loadCarsLazy(event) {
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    //imitate db connection over a network
    setTimeout(() => {
      if(this.dataSource) {
        this.assets = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 250);
  }
}

