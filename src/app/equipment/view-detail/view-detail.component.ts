import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-view-detail',
  templateUrl: './view-detail.component.html',
  styleUrls: ['./view-detail.component.scss']
})
export class ViewDetailComponent implements OnInit {
  @Input() currentAsset;
  @Output() closeDetailMask = new EventEmitter();
  display;
  windowSize;
  width;
  title = '查看详情';
  constructor() { }

  ngOnInit() {
    this.display = true;
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else if(this.windowSize>1500){
      this.width = this.windowSize*0.7;
    }else{
      this.width = this.windowSize*0.8;
    }
  }
  closeViewDetailMask(bool){
    this.closeDetailMask.emit(bool);
  }
}
