import { Component, OnInit } from '@angular/core';
import {StorageService} from "../../services/storage.service";
import {EquipmentService} from "../equipment.service";

@Component({
  selector: 'app-view-ledger',
  templateUrl: './view-ledger.component.html',
  styleUrls: ['./view-ledger.component.scss']
})
export class ViewLedgerComponent implements OnInit {
   asset;
   totalRecords;
   dataSource;
   viewLedgers;
   cols;
  constructor(
    private storageService:StorageService,
    private equipmentService:EquipmentService
  ) { }

  ngOnInit() {
    this.cols = [
      {field: 'id', header: 'id'},
      {field: 'ano', header: '资产编号'},
      {field: 'operate_type', header: '操作类型'},
      {field: 'create_datetime', header: '操作时间'},
      {field: 'operater', header: '操作人'},
      {field: 'notes', header: '操作内容'},
    ]
    this.asset = JSON.parse(this.storageService.getCurrentAsset('asset'));
    console.log(this.asset)
    this.equipmentService.queryViewLedger(this.asset).subscribe(data=>{
       this.dataSource = data;
       this.totalRecords = this.dataSource.length;
       console.log(this.dataSource);
       this.viewLedgers = this.dataSource.slice(0,10);
    },(err:Error)=>{

    })
  }
  loadCarsLazy(event) {
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    //imitate db connection over a network
    setTimeout(() => {
      if(this.dataSource) {
        this.viewLedgers = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 250);
  }

}
