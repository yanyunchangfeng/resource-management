import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrUpdateLabelComponent } from './add-or-update-label.component';

describe('AddOrUpdateLabelComponent', () => {
  let component: AddOrUpdateLabelComponent;
  let fixture: ComponentFixture<AddOrUpdateLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrUpdateLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrUpdateLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
