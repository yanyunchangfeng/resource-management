import {Component, HostListener, OnInit} from '@angular/core';
import {EquipmentService} from "../equipment.service";
import {StorageService} from "../../services/storage.service";
import {Router} from "@angular/router";
import {ConfirmationService} from "primeng/primeng";
declare var $:any;
@Component({
  selector: 'app-equipment-search',
  templateUrl: './equipment-search.component.html',
  styleUrls: ['./equipment-search.component.scss']
})
export class EquipmentSearchComponent implements OnInit {
  display = false;
  moreSercheIsShow;
  zh;
  cols;
  queryModel;
  totalRecords: number;
  assets;
  tempAsset;
  showAddOrUpdateMask:boolean = false;
  showViewDetailMask:boolean = false;
  dataSource;
  state = 'update';
  stacked:boolean = false;
  windowSize;
  constructor(
    private equipmentService:EquipmentService,
    private storageService:StorageService,
    private router:Router,
    private comfirmationService:ConfirmationService
  ){}
  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.stacked = true
    }

    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.queryModel = {
        "ano":"",
        "father":"",
        "card_position":"",
        "asset_no":"",
        "name":"",
        "brand":"",
        "modle":"",
        "function":"",
        "system":"",
        "status":"",
        "on_line_date_start":"",
        "on_line_date_end":"",
        "room":"",
        "cabinet":"",
        "location":"",
        "belong_dapart":"",
        "belong_dapart_manager":" ",
        "manage_dapart":"",
        "manager":"",
        "ip":"",
        "maintain_vender":""
      }
    this.cols = [
      {field: 'ano', header: '设备编码'},
      {field: 'father', header: '从属于'},
      {field: 'asset_no', header: '固定资产编号'},
      {field: 'name', header: '设备名称'},
      {field: 'status', header: '设备状态'},
      {field: 'room', header: '机房'},
      {field: 'cabinet', header: '机柜'},
      {field: 'location', header: 'U位'},
      {field: 'ip', header: '管理IP'},
    ]
    //查询资产
    this.queryAssets();
  }
  queryAssets(){
    this.equipmentService.getAssets().subscribe(asset =>{
      this.dataSource = asset;
      if(this.dataSource){
        this.totalRecords = this.dataSource.length;
        this.assets = this.dataSource.slice(0,10);
      }else{
        this.totalRecords = 0;
        this.assets = []
      }
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status === 0|| JSON.parse(JSON.stringify(err)).status === 504){
        message = '似乎网络出现了问题，请联系管理员或稍后重试'
      }else{
        message =err
      }
      this.comfirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
  closeMask(bool){
    this.showAddOrUpdateMask = bool;
  }
  updateOption(currentAsset){
    this.tempAsset= currentAsset;
    this.showAddOrUpdateMask = !this.showAddOrUpdateMask;
  }
  viewOption(currentAsset){
    this.tempAsset= currentAsset;
    this.showViewDetailMask = !this.showViewDetailMask
  }
  viewLedger(asset){
    this.storageService.setCurrnetAsset('asset',asset);
    this.router.navigate(['/index/equipment/view'])
  }
  queryByKeyWords(){
    for(let key in this.queryModel){
       if(!this.queryModel[key]){
         this.queryModel[key] = '';
       }
       this.queryModel[key] = this.queryModel[key].trim()
    }
     this.equipmentService.queryByKeyWords(this.queryModel).subscribe(data=>{
       this.dataSource = data;
       if(this.dataSource){
         this.totalRecords = this.dataSource.length;
         this.assets = this.dataSource.slice(0,10);
       }else{
         this.totalRecords = 0;
         this.assets = []
       }
     },(err:Error)=>{
       let message ;
       if(JSON.parse(JSON.stringify(err)).status === 0|| JSON.parse(JSON.stringify(err)).status === 504){
         message = '似乎网络出现了问题，请联系管理员或稍后重试'
       }else{
         message =err
       }
       this.comfirmationService.confirm({
         message: message,
         rejectVisible:false,
       })
     })
  }
  loadCarsLazy(event) {
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    //imitate db connection over a network
    setTimeout(() => {
      if(this.dataSource) {
        this.assets = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 250);
  }
  updateAsset(bool){
    this.queryAssets();
    this.showAddOrUpdateMask = bool;
    this.comfirmationService.confirm({
      message: '修改成功！',
      rejectVisible:false,
    })
  }
  closeViewDetail(bool){
    this.showViewDetailMask = bool;
  }
  resetKeyWords(){
    for(let key in this.queryModel){
      this.queryModel[key] = ''
    }
    console.log(this.queryModel)
  }
  toggleMoreSerche() {
    this.moreSercheIsShow = this.moreSercheIsShow ? false : true;
  }
  @HostListener('window:resize', ['$event'])
  public onWindowResize(event): void {
    // this.echart.resize()
    console.log(event)
    if(window.innerWidth<1024){
      this.stacked = true;
    }else{
      this.stacked = false;
    }
  }
}
