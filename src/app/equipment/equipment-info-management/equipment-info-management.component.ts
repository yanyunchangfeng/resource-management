import {Component, OnInit} from '@angular/core';
import {EquipmentService} from '../equipment.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-equipment-info-management',
  templateUrl: './equipment-info-management.component.html',
  styleUrls: ['./equipment-info-management.component.scss']
})
export class EquipmentInfoManagementComponent implements OnInit {
  filesTree11;
  selectedFile3;
  rootTree;
  firstLevelChildren;
  secondLevelChildren;
  thirdLevelChildren;
  FourthLevelChildren;

  constructor(private equipmentService: EquipmentService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.rootTree = [{
      label: '云联共创',
      'oid': '1',
      'deep': '1',
      'leaf': false,
    }];
    this.firstLevelChildren = [
      {
        'label': '房一',
        'data': '房一 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '1.1',
        'deep': '2',
        'leaf': false
      },
      {
        'label': '房二',
        'data': '房二 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '1.2',
        'deep': '2',
        'leaf': false
      },
    ];
    this.secondLevelChildren = [
      {
        'label': '模块一',
        'data': '模块一 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'leaf': false,
        'oid': '2.1',
        'deep': '3',
      },
      {
        'label': '模块二',
        'data': '模块二 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '2.2',
        'deep': '3',
        'leaf': false
      }
    ];
    this.thirdLevelChildren = [
      {
        'label': '列一',
        'data': '列一 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '3.1',
        'deep': '4',
        'leaf': false
      },
      {
        'label': '列二',
        'data': '列二 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '3.2',
        'deep': '4',
        'leaf': false
      }
    ];
    this.FourthLevelChildren = [
      {
        'label': '机柜一',
        'data': '机柜一 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '4.1',
        'deep': '5',
        'infomation': [{'label': '名称', 'key': 'name', value: '机柜一'}, {'label': '温度', 'key': 'tempture', 'value': '32℃'}],
        'route': 'cabinet',
        'cabinet': this.randomCabinet(),
        'leaf': false
      },
      {
        'label': '机柜二',
        'data': '机柜二 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'infomation': [{'label': '名称', 'key': 'name', value: '机柜二'}, {'label': '温度', 'key': 'tempture', 'value': '28℃'}],
        'oid': '4.2',
        'deep': '5',
        'leaf': false,
        'route': 'cabinet',
        'cabinet': this.randomCabinet(),
      },
      {
        'label': '机柜三',
        'data': '机柜三 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '4.3',
        'deep': '5',
        'infomation': [{'label': '名称', 'key': 'name', value: '机柜三'}, {'label': '温度', 'key': 'tempture', 'value': '25℃'}],
        'leaf': false,
        'route': 'cabinet',
        'cabinet': this.randomCabinet(),
      },
      {
        'label': '机柜四',
        'data': '机柜四 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '4.4',
        'deep': '5',
        'leaf': false,
        'infomation': [{'label': '名称', 'key': 'name', value: '机柜四'}, {'label': '温度', 'key': 'tempture', 'value': '39℃'}],
        'route': 'cabinet',
        'cabinet': this.randomCabinet(),
      },
      {
        'label': '机柜五',
        'data': '机柜五 Folder',
        'expandedIcon': 'fa-folder-open',
        'collapsedIcon': 'fa-folder',
        'oid': '4.5',
        'deep': '5',
        'leaf': false,
        'infomation': [{'label': '名称', 'key': 'name', value: '机柜五'}, {'label': '温度', 'key': 'tempture', 'value': '42℃'}],
        'route': 'cabinet',
        'cabinet': this.randomCabinet(),
      },
    ];
    this.filesTree11 = this.queryNode();
  }

  loadNode(event) {
    if (event.node) {
      //in a real application, make a call to a remote url to load children of the current node and add the new nodes as children
      event.node.children = this.queryNode(event.node.oid, event.node.deep);
    }
  }

  selectNode(event) {
    console.log(event);
    if (event.node.route) {
      this.router.navigate([event.node.route], {queryParams: {cabient: JSON.stringify(event.node.cabinet), info: JSON.stringify(event.node.infomation)}, relativeTo: this.route});
    } else {
      this.router.navigate(['/index/equipment/info'], {relativeTo: this.route});
    }
  }

  queryNode(oid?, deep?) {
    if (!oid && !deep) {
      return this.rootTree;
    }
    if (oid == 1 && deep == 1) {
      return this.firstLevelChildren;
    }
    if (oid == 1.1 && deep == 2) {
      return this.secondLevelChildren;
    }
    if (oid == 2.1 && deep == 3) {
      return this.thirdLevelChildren;
    }
    if (oid == 3.1 && deep == 4) {
      return this.FourthLevelChildren;
    }
  }
  randomCabinet(){
    //1.多少个设备
    let deviceCount = Math.floor(Math.random() * (20 - 10 + 1) + 10);
    let arr = [];
    for (let i = 0; i < deviceCount; i++){
        let obj = {};
        let u = this.randomU();
        let {row, col} = this.randomRC();
        obj['u'] = u ;
        obj['name'] = u;
        obj['row'] = row;
        obj['route'] = '../port';
        obj['col'] = col;
        obj['ports'] = this.randomPort(row, col);
        arr.push(obj);
    }
    return arr;
  }
  randomU(){
    return Math.floor(Math.random() * (4 + 1));
  }
  randomRC(){
    let r = Math.floor(Math.random() * (10 - 2) + 2);
    let c = Math.floor(Math.random() * (10 - 2) + 2);
    return {row: r, col: c};
  }
  randomPort(row, col){
    let arr = [];
    for (let i = 0; i < row * col; i++){
      let obj = {};
      obj['r'] = this.randomCell(row);
      obj['c'] = this.randomCell(col);
      arr.push(obj);
    }
    return arr;
  }
  randomCell(num){
    return Math.floor(Math.random() * (num));
  }
}
