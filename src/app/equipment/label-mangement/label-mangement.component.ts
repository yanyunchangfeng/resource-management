import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-label-mangement',
  templateUrl: './label-mangement.component.html',
  styleUrls: ['./label-mangement.component.scss']
})
export class LabelMangementComponent implements OnInit {
  subMenus;
  constructor() { }

  ngOnInit() {
    this.subMenus = [
      {label:'标签设置',route:''},
      {label:'打印模板',route:''},
      {label:'标签打印',route:''}
    ];
  }

}
