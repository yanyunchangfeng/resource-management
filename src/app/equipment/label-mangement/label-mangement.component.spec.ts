import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabelMangementComponent } from './label-mangement.component';

describe('LabelMangementComponent', () => {
  let component: LabelMangementComponent;
  let fixture: ComponentFixture<LabelMangementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabelMangementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelMangementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
