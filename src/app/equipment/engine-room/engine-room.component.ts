import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
import Color from '../../util/color.util'
import {EquipmentService} from "../equipment.service";
import {IntervalObservable} from "rxjs/observable/IntervalObservable";

@Component({
  selector: 'app-engine-room',
  templateUrl: './engine-room.component.html',
  styleUrls: ['./engine-room.component.css']
})
export class EngineRoomComponent implements OnInit ,OnDestroy {
  @ViewChild('room') room: ElementRef;
  echart;
  engineRoom;
  engineRoomName = [];
  engineRoomCount = [];
  engineRoomColor = [];
  baseColor;
  df;
  intervalObser;
  public opt:any =  {
    title : {
      // text: '某站点用户访问来源',
      top:5,
      subtext: '纯属虚构',
      x:'center',
      textStyle:{//标题内容的样式
        color:'#333',//京东红
        fontStyle:'normal',//主标题文字字体风格，默认normal，有italic(斜体),oblique(斜体)
        fontWeight:"bold",//可选normal(正常)，bold(加粗)，bolder(加粗)，lighter(变细)，100|200|300|400|500...
        fontFamily:"san-serif",//主题文字字体，默认微软雅黑
        fontSize:18//主题文字字体大小，默认为18px
      },
    },
    tooltip : {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
      orient: 'vertical',
      left: 'left',
      data: ['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
    },
    series : [
      {
        name: '访问来源',
        type: 'pie',
        radius : '55%',
        center: ['50%', '60%'],
        data:[
          {value:335, name:'直接访问'},
          {value:310, name:'邮件营销'},
          {value:234, name:'联盟广告'},
          {value:135, name:'视频广告'},
          {value:1548, name:'搜索引擎'}
        ],
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
    ]
  };

  option = {
    color: Color.baseColor,
    title : {
      text: '所属机房饼状图展示',
      top:20,
      // subtext: '所属机房饼状图展示',
      x:'center',
      textStyle:{//标题内容的样式
        color:'#111',//京东红
        fontStyle:'normal',//主标题文字字体风格，默认normal，有italic(斜体),oblique(斜体)
        fontWeight:"bold",//可选normal(正常)，bold(加粗)，bolder(加粗)，lighter(变细)，100|200|300|400|500...
        // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
        fontSize:14//主题文字字体大小，默认为18px
      }
    },
    tooltip : {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
      type: 'scroll',
      orient: 'vertical',
      // right: 100,
      left:0,
      top: 20,
      bottom: 20,
      data: []
    },
    series : [
      {
        // name: '访问来源',
        type: 'pie',
        radius : '55%',
        center: ['50%', '50%'],
        data: [{
          value: 3350,
          name: '深圳'
        },
        {
          value: 310,
          name: '北京'
        },
        {
          value: 234,
          name: '广州'
        },
        {
          value: 135,
          name: '上海'
        },
        {
          value: 1548,
          name: '长沙'
        }],
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
    ]
  };

  // public opt: any =  {
  //   theme: '',
  //   event: [
  //     {
  //       type: "click",
  //       cb: function (res) {
  //         console.log(res);
  //       }
  //     }
  //   ],
  //   title: {
  //     text: '',
  //     // subtext: '饼状图',
  //     x: 'center'
  //   },
  //   tooltip: {
  //     trigger: 'item',
  //     formatter: "{a} <br/>{b} : {c} ({d}%)"
  //   },
  //   legend: {
  //     orient: 'vertical',
  //     left: 'left',
  //     data: ['深圳', '北京', '广州', '上海', '长沙'],
  //     // data:['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
  //   },
  //   series: [{
  //     name: '访问来源',
  //     type: 'pie',
  //     startAngle: -180,
  //     radius: '55%',
  //     center: ['50%', '60%'],
  //     // data: [],
  //     data:[
  //       {value:335, name:'直接访问'},
  //       {value:310, name:'邮件营销'},
  //       {value:234, name:'联盟广告'},
  //       {value:135, name:'视频广告'},
  //       {value:1548, name:'搜索引擎'}
  //     ],
  //     itemStyle: {
  //       emphasis: {
  //         shadowBlur: 10,
  //         shadowOffsetX: 0,
  //         shadowColor: 'rgba(0, 0, 0, 0.5)'
  //       }
  //     }
  //   }]
  // };
  constructor(private equipmentService:EquipmentService) { }

  ngOnInit() {
    this.baseColor = [
      '#25859e',
      '#6acece',
      '#e78816',
      '#eabc7f',
      '#12619d',
      '#ad2532',
      '#15938d',
      '#b3aa9b',
      '#042d4c'
    ];
    this.echart = echarts.init(this.room.nativeElement);
    this.echart.setOption(this.option);
    this.equipmentService.queryEquipmentView().subscribe(data=>{
      this.engineRoom = data.room;
      if(!this.engineRoom){
        this.engineRoom = [];
      }
      let num= 0;
      for(let i = 0;i<this.engineRoom.length;i++){
        this.engineRoomName.push(this.engineRoom[i].name);
        this.engineRoomCount.push(this.engineRoom[i].count);
        if(num<this.baseColor.length){
          this.engineRoomColor[i]=this.baseColor[num++]
        }else{
          num=0;
          this.engineRoomColor[i]=this.baseColor[num++]
        }
      }
      console.log(this.engineRoomName);
      this.option.series[0]['data'] = this.equipmentService.formatPieData(this.engineRoom);
      this.option.legend['data'] =  this.engineRoomName;
      this.option.color = this.engineRoomColor;
      this.intervalObser = IntervalObservable.create(100).subscribe(index=>{
        this.initwidth();
      })
        this.echart.setOption(this.option);
    })
  }
  initwidth(){
    this.echart.resize();
    this.intervalObser.unsubscribe();
  }
  ngOnDestroy() {
      this.echart.dispose(this.room.nativeElement);
      this.echart =null;
  }
  @HostListener('window:resize', ['$event'])
  public onWindowResize(event): void {
    this.echart.resize()
  }
}
