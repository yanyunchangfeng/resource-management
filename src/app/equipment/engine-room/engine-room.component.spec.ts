import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineRoomComponent } from './engine-room.component';

describe('EngineRoomComponent', () => {
  let component: EngineRoomComponent;
  let fixture: ComponentFixture<EngineRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
