import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-label-settings',
  templateUrl: './label-settings.component.html',
  styleUrls: ['./label-settings.component.css']
})
export class LabelSettingsComponent implements OnInit {
  showMask:boolean = false;
  constructor() { }

  ngOnInit() {
  }
  showAddLabelMask(bool){
    this.showMask = bool;
  }
  closeMask(bool){
    this.showMask = bool;
  }
}
