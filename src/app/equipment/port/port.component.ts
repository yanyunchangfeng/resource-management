import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-port',
  templateUrl: './port.component.html',
  styleUrls: ['./port.component.scss']
})
export class PortComponent implements OnInit {
  device;
  // cells=[
  //   {r:0,c:3},
  //   {r:0,c:4},
  //   {r:0,c:5},
  //   {r:1,c:2},
  //   {r:1,c:3},
  //   {r:1,c:4},
  //   {r:1,c:4},
  // ]
  constructor(private route:ActivatedRoute) { }
  ngOnInit() {
   this.route.queryParams.subscribe(params=>{
     console.log(JSON.parse(params['device']))
     this.device = JSON.parse(params['device']);
   })
  }
}
