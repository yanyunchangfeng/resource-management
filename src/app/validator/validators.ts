import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
export function timeValidator(group: FormGroup): any{
  let startTime = group.get('startTime') as FormControl;
  let endTime = group.get('endTime') as FormControl;
  if (startTime.value && endTime.value){
    let valid  = new Date(endTime.value).getTime() > new Date(startTime.value).getTime();
    return valid ? null : {time: {descxxx: '结束时间不能早于开始时间'}};
  }
}
export function onlytimeValidator(group: FormGroup): any{
  let startTime = group.get('startTime') as FormControl;
  let endTime = group.get('endTime') as FormControl;
  let date  = new Date();
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  let currentdate = year + '-' + month + '-' + day + ' ';
  if (startTime.value && endTime.value){
    // let valid  = new Date(currentdate+endTime.value).getTime() > new Date(currentdate+startTime.value).getTime();
    // console.log(currentdate+endTime.value);
    // console.log(new Date(currentdate+endTime.value).getTime())
    // console.log(new Date(currentdate+startTime.value).getTime())
    let valid = endTime.value > startTime.value;
    return valid ? null : {time: {descxxx: '结束时间不能早于开始时间'}};
  }
}
/*
人员的姓名验证，需为中英文，最少2位
 */
export function nameValidator(control: FormControl): any{
  let name = (control.value || '') + '';
  let re = /^([a-zA-Z\u4e00-\u9fa5]{2,})$/;
  let valid = re.test(name);
  return valid ? null : {name: {descxxx: '姓名需为中英文，最少2位'}};
}
/*
人员的帐号验证，不支持中文，4到12位
 */
export function pidValidator(control: FormControl): any{
  let pid = (control.value || '') + '';
  let re = /[@#$%&a-zA-Z0-9]{4,12}/;
  let valid = re.test(pid);
  return valid ? null : {code: {descxxx: '不支持中文，4到12位'}};
}
/*
人员的密码验证，非空
 */
export function nullValidator(control: FormControl): any{
  let pwd = (control.value || '') + '';
  let valid = !(pwd == null || pwd.trim().length == 0);
  return valid ? null : {password: {descxxx: '密码不能为空字符串'}};
}
/*
人员的身份证验证
 */
export function idcardValidator(control: FormControl): any{
  let idcard  = (control.value || '') + '';
  let re = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
  let valid = re.test(idcard);
  return valid ? null : {idcard: {descxxx: '非法的身份证号'}};
}
/*
人员的手机号验证
 */
export function phoneValidator(control: FormControl): any{
  let phone  = (control.value || '') + '';
  let re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
  let valid = re.test(phone);
  return valid ? null : {phone: {descxxx: '非法的手机号'}};
}
/*
人员的邮箱验证
 */
export function emailValidator(control: FormControl): any{
  let email  = (control.value || '') + '';
  let re = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
  let valid = re.test(email);
  return valid ? null : {email: {descxxx: '非法的邮箱'}};
}
/*
人员的注册日期和失效日期验证，失效日期不能小于注册日期
 */
export function dateValidator(group: FormGroup): any{
  let registedate = group.get('registedate') as FormControl;
  let validdate = group.get('validdate') as FormControl;
  if (registedate.value && validdate.value){
    let valid  = new Date(validdate.value).getTime() > new Date(registedate.value).getTime();
    return valid ? null : {date: {descxxx: '失效日期不能小于注册日期'}};
  }
}
/*
起始时间不能等于结束时间
 */
export const TimeNotSameValidator = (controls: AbstractControl): {[key: string]: boolean} => {
    const beginTime = controls.get('plan_time_begin');
    const endTime = controls.get('plan_time_end');
    return (beginTime.value === endTime.value) ? {timematch: true} : null;
};
