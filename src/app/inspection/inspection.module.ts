import  { NgModule} from '@angular/core';
import { ShareModule} from '../shared/share.module';
import { InspectionRoutingModule} from './inspection-routing.module';
import { InspectionContentManagementComponent} from './inspection-content-management/inspection-content-management.component';
import { InspectionPlanComponent} from './inspection-plan/inspection-plan.component';
import { InspectionViewComponent} from './inspection-view/inspection-view.component';
import { InspectionObjectComponent} from './inspection-object/inspection-object.component';
import { InspectionCycleComponent } from './inspection-cycle/inspection-cycle.component';
import { InspectionTalkComponent } from './inspection-talk/inspection-talk.component';
import { AddOrUpdateInspectionComponent } from './add-or-update-inspection/add-or-update-inspection.component';
import {InspectionService} from "./inspection.service";
import { EditShiftComponent } from './edit-shift/edit-shift.component';
import { AddCycleTimeComponent } from './add-cycle-time/add-cycle-time.component';
import { FileUploadModule} from "ng2-file-upload";
import { UpdateInspectionObjectComponent } from './update-inspection-object/update-inspection-object.component';
import {HistoryInspectionComponent} from "./history-inspection/history-inspection.component";
import {InspectionOrdersManagementComponent} from "./inspection-orders-management/inspection-orders-management.component";
import { AddOrUpdateInspectionObjectContentComponent } from './add-or-update-inspection-object-content/add-or-update-inspection-object-content.component';
import { AssociatedInspectionObjectComponent } from './associated-inspection-object/associated-inspection-object.component';
import { AssociatedInspectionObjectItemsComponent } from './associated-inspection-object-items/associated-inspection-object-items.component';
import { AddInspectionPlanComponent } from './add-inspection-plan/add-inspection-plan.component';
import { AddInspectionPlanTreeComponent } from './add-inspection-plan-tree/add-inspection-plan-tree.component';
import { ViewInspectionPlanComponent } from './view-inspection-plan/view-inspection-plan.component';
import { ViewDetailComponent } from './view-detail/view-detail.component';
import { IntervalInspectionOrderComponent } from './interval-inspection-order/interval-inspection-order.component';
import { UpdatePlanStatusComponent } from './update-plan-status/update-plan-status.component';
import { InspectionSettingComponent } from './inspection-setting/inspection-setting.component';
import { InspectionviewDetailDialogComponent } from './inspectionview-detail-dialog/inspectionview-detail-dialog.component';
import { InspectionPointConfigurationComponent } from './inspection-point-configuration/inspection-point-configuration.component';
import { AddInspectionPointConfigurationComponent } from './add-inspection-point-configuration/add-inspection-point-configuration.component';
import { PointAssociatedComponent } from './point-associated/point-associated.component';
import { PointerInspectionItemComponent } from './pointer-inspection-item/pointer-inspection-item.component';
import { InspectionOverviewComponent } from './inspection-overview/inspection-overview.component';
import { FirstEchartsComponent } from './first-echarts/first-echarts.component';
import { SecondEchartsComponent } from './second-echarts/second-echarts.component';
import { SecondRightEchartsComponent } from './second-right-echarts/second-right-echarts.component';
import { ThirdEchartsComponent } from './third-echarts/third-echarts.component';
import { FourviewEchartsComponent } from './fourview-echarts/fourview-echarts.component';
import { SecondMothEchartsComponent } from './second-moth-echarts/second-moth-echarts.component';
import { SecondRightMothEchartsComponent } from './second-right-moth-echarts/second-right-moth-echarts.component';
import { FourViewMonthEchartsComponent } from './four-view-month-echarts/four-view-month-echarts.component';

@NgModule({
  declarations: [
    InspectionContentManagementComponent,
    InspectionPlanComponent,
    InspectionViewComponent,
    InspectionObjectComponent,
    InspectionCycleComponent,
    InspectionTalkComponent,
    InspectionOrdersManagementComponent,
    AddOrUpdateInspectionComponent,
    HistoryInspectionComponent,
    EditShiftComponent,
    AddCycleTimeComponent,
    UpdateInspectionObjectComponent,
    AddOrUpdateInspectionObjectContentComponent,
    AssociatedInspectionObjectComponent,
    AssociatedInspectionObjectItemsComponent,
    AddInspectionPlanComponent,
    AddInspectionPlanTreeComponent,
    ViewInspectionPlanComponent,
    ViewDetailComponent,
    IntervalInspectionOrderComponent,
    UpdatePlanStatusComponent,
    InspectionSettingComponent,
    InspectionviewDetailDialogComponent,
    InspectionPointConfigurationComponent,
    AddInspectionPointConfigurationComponent,
    PointAssociatedComponent,
    PointerInspectionItemComponent,
    InspectionOverviewComponent,
    FirstEchartsComponent,
    SecondEchartsComponent,
    SecondRightEchartsComponent,
    ThirdEchartsComponent,
    FourviewEchartsComponent,
    SecondMothEchartsComponent,
    SecondRightMothEchartsComponent,
    FourViewMonthEchartsComponent,
  ],
  imports: [
    ShareModule,
    FileUploadModule,
    InspectionRoutingModule,

  ],
  providers:[InspectionService]
})
export class InspectionModule {
}
