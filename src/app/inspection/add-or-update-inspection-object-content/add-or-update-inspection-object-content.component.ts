import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {ConfirmationService} from "primeng/primeng";
import {StorageService} from "../../services/storage.service";
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-or-update-inspection-object-content',
  templateUrl: './add-or-update-inspection-object-content.component.html',
  styleUrls: ['./add-or-update-inspection-object-content.component.scss']
})
export class AddOrUpdateInspectionObjectContentComponent implements OnInit {
  formBuilder: FormBuilder = new FormBuilder;
  addContent;
  width;
  windowSize;
  display :boolean = false;
  @Output() closeAddMask = new EventEmitter();
  @Output() addDev = new EventEmitter(); //添加巡检内容
  @Output() updateDev = new EventEmitter(); //修改巡检内容
  @Input() currentInspection;
  @Input() inspectionNames;
  @Input() state;
  title = '添加'
  submitAddInspection = {
    "name":"",
    "inspection_cycle_cid":'',
    "create_people":"",
    "create_time":""
  };//提交
  constructor(
    private inspectionService:InspectionService,
    private confirmationService:ConfirmationService,
    private storageService:StorageService
  ) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.4;
    }else{
      this.width = this.windowSize*0.6;
    }
    this.display = true;
    this.submitAddInspection.create_people = this.storageService.getUserName('userName');
    this.inspectionNames = this.inspectionService.formatDropDownData(this.inspectionNames);
    this.submitAddInspection.inspection_cycle_cid = this.inspectionNames.length ===1 ?this.inspectionNames[0]['value']:'';
    this.currentInspection = this.inspectionService.deepClone(this.currentInspection);
    if(this.state === 'update'){
      this.title = '修改'
    }else{
      this.submitAddInspection.inspection_cycle_cid = this.inspectionNames[0]['value'];
    }
    this.addContent = this.formBuilder.group({
      contentName:['',Validators.required],
      cycleName:['',Validators.required]
    })
  }
  addDevice(bool){
    this.submitAddInspection.create_time =this.inspectionService.getFormatTime(new Date())
    this.inspectionService.addInspectionContent(this.submitAddInspection).subscribe(()=>{
      this.addDev.emit(bool);
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
  updateDevice(bool){
    this.inspectionService.updateInspectionContent(this.currentInspection).subscribe(()=>{
      this.updateDev.emit(this.currentInspection);
      this.closeAddMask.emit(bool)
    })
  }
  closeInspectionMask(bool){
    this.closeAddMask.emit(bool)
  }
  formSubmit(bool){
    if(this.state ==='add'){
      this.addDevice(bool)
    }else{
      this.updateDevice(bool)
    }
  }

}
