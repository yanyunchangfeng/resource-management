import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrUpdateInspectionObjectContentComponent } from './add-or-update-inspection-object-content.component';

describe('AddOrUpdateInspectionObjectContentComponent', () => {
  let component: AddOrUpdateInspectionObjectContentComponent;
  let fixture: ComponentFixture<AddOrUpdateInspectionObjectContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrUpdateInspectionObjectContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrUpdateInspectionObjectContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
