import { Component, OnInit } from '@angular/core';
import {InspectionService} from "../inspection.service";
import {ConfirmationService} from "primeng/primeng";
import {StorageService} from "../../services/storage.service";

@Component({
  selector: 'app-inspection-point-configuration',
  templateUrl: './inspection-point-configuration.component.html',
  styleUrls: ['./inspection-point-configuration.component.scss']
})
export class InspectionPointConfigurationComponent implements OnInit {
  cols;
  queryModel;
  showInspectionPoint: boolean = false;
  showPointer: boolean = false;
  chooseCids =[];
  MaterialData;
  selectMaterial = [];
  totalRecords;
  state;
  currentInpection;
  constructor( private inspectionService: InspectionService,
               private confirmationService:ConfirmationService,
               private storageService: StorageService) { }

  ngOnInit() {
    this.cols = [
      {field: 'name', header: '巡检点名称'},
      {field: 'flag', header: '巡检点标签'},
      {field: 'region', header: '关联区域'},
      {field: 'point_order', header: '巡检顺序'},
    ];
    this.queryModel={
      "condition":{
        "name":"",
        "flag":""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.queryInspectionDate();
  }
  //添加成功
  addInsepection(bool){
    this.showInspectionPoint = bool;
    this.queryInspectionDate()
  }
  //搜索功能
  suggestInsepectiones(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryInspectionDate()
  }
  //分页查询
  paginate(event) {
    this.chooseCids =[];
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInspectionDate();
  }
  queryInspectionDate(){
    this.inspectionService.queryInspectionPoint(this.queryModel).subscribe(data=>{
      if(!data['items']){
        this.MaterialData = [];
        this.selectMaterial =[];
        this.totalRecords = 0;
      }else{
        this.MaterialData = data.items;
        console.log( this.MaterialData )
        this.totalRecords = data.page.total;
      }
    });
  }
  //修改
  updateOption(currentInspection){
    this.state = 'update'
    this.showInspectionPoint = !this.showInspectionPoint;
    this.currentInpection = currentInspection;
  }
  //同步更新页面数据
  updateInspectionContent(){
    this.queryInspectionDate()
  }
  deleteMaterial(){
    for(let i = 0;i<this.selectMaterial.length;i++){
      this.chooseCids.push(this.selectMaterial[i]['cid'])
    }
    this.confirmationService.confirm({
      message:'确认删除吗?',
      accept: () => {
        this.inspectionService.deleteInspectionPoint(this.chooseCids).subscribe(()=>{
          this.queryInspectionDate();
          this.selectMaterial = [];
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })
      },
      reject:() =>{

      }
    });
  }
  showInspectionPointMask(){
    this.state = 'add';
    this.showInspectionPoint = !this.showInspectionPoint;
  }
  closeInspectionPointMask(bool){
    this.showInspectionPoint = false;
  }
  //关联对象
  showPointerMask(current){
    this.storageService.setCurrentInspectionPointobj('InspectionPointObj',current)
    this.showPointer = !this.showPointer;
  }
  closePointerMaks(bool){
    this.showPointer = false;
  }
}
