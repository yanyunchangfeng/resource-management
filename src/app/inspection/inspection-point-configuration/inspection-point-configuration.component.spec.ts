import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionPointConfigurationComponent } from './inspection-point-configuration.component';

describe('InspectionPointConfigurationComponent', () => {
  let component: InspectionPointConfigurationComponent;
  let fixture: ComponentFixture<InspectionPointConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionPointConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionPointConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
