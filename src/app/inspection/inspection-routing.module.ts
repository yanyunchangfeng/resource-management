import { NgModule } from '@angular/core';
import { RouterModule , Routes} from '@angular/router';

import {InspectionContentManagementComponent} from './inspection-content-management/inspection-content-management.component';
import {InspectionPlanComponent} from './inspection-plan/inspection-plan.component';
import {InspectionViewComponent} from './inspection-view/inspection-view.component';
import {InspectionObjectComponent} from './inspection-object/inspection-object.component';
import {InspectionCycleComponent} from "./inspection-cycle/inspection-cycle.component";
import {InspectionTalkComponent} from "./inspection-talk/inspection-talk.component";
import {AddInspectionPlanComponent} from "./add-inspection-plan/add-inspection-plan.component";
import {InspectionSettingComponent} from "./inspection-setting/inspection-setting.component";
import {InspectionPointConfigurationComponent} from "./inspection-point-configuration/inspection-point-configuration.component";
import {InspectionOverviewComponent} from "./inspection-overview/inspection-overview.component";

const route: Routes = [
  {path: 'view', component: InspectionOverviewComponent},//巡检总览
  // {path: 'overview', component: InspectionOverviewComponent},//新巡检总览
  {path: 'talk', component: InspectionTalkComponent},//巡检报告管理,
  {path: 'inspectionSetting', component: InspectionSettingComponent,children:[
    {path: 'object', component: InspectionObjectComponent},//巡检对象库
    {path: 'cycle', component: InspectionCycleComponent},//巡检周期管理,
    {path: 'content', component: InspectionContentManagementComponent},//巡检内容管理
    {path: 'inspectionPointConfiguration', component: InspectionPointConfigurationComponent},//巡检点配置,
  ]},//巡检配置,

  {path: 'plan', component: InspectionPlanComponent},//巡检计划管理
  {path: 'addplan', component: AddInspectionPlanComponent},//巡检计划管理

];

@NgModule({
  imports: [RouterModule.forChild(route),
  ],
  exports: [RouterModule]
})
export class InspectionRoutingModule {

}
