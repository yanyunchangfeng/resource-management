import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {ConfirmationService} from "primeng/primeng";
import {StorageService} from "../../services/storage.service";
import {MessageService} from "primeng/components/common/messageservice";
import {BasePage} from "../../base.page";

@Component({
  selector: 'app-view-inspection-plan',
  templateUrl: './view-inspection-plan.component.html',
  styleUrls: ['./view-inspection-plan.component.scss']
})
export class ViewInspectionPlanComponent extends BasePage implements OnInit {
  @Output() closeViewMask = new EventEmitter();
  @Output() showTaskView =new EventEmitter();
  showViewDetailMask = false;
  showViewUpdatePlan: boolean = false;
  display;
  status;
  width;
  state: boolean = true;
  windowSize;
  chooseCids = [];
  queryModel;
  totalRecords;
  selectInsepections =[];
  inspectionsObject;
  cols;
  queryScheduleModel = {};
  cuerrntInspectionPlan;
  constructor(
    private inspectionService:InspectionService,
    public confirmationService: ConfirmationService,
    public messageService:MessageService,
  ) {
    super(confirmationService,messageService)
  }

  ngOnInit() {
    // this.state =true;
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    this.status = [
      {key:'',val:''},
      {key:'进行中',val:'进行中'},
      {key:'已终止',val:'已终止'},
      {key:'待执行',val:'待执行'},
      {key:'已完成',val:'已完成'},
    ];
    this.cols = [
      {field: 'name', header: '巡检计划名'},
      {field: 'start_time', header: '计划开始时间'},
      {field: 'end_time', header: '计划结束时间'},
      {field: 'create_people', header: '添加人'},
      {field: 'status', header: '计划状态'}
    ]
    this.queryModel = {
      'name':'',
      'status':this.status[0]['key'],
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.queryScheduleModel = {
      "inspection_plan_cid":"",
      "range_time_start":"",
      "range_time_end":"",

    }
    this.queryInsepection();
  }

  //确认终止按钮
  sureApproved(current){
    let cid = current['cid'];
    this.confirmationService.confirm({
      message: '是否终止?',
      accept: () => {
        this.inspectionService.updateInspectionStatus(cid).subscribe(data=>{
          // this.selectMaterial = [];
          this.queryInsepection();
        },(err:Error)=>{
          this.confirmationService.confirm({
            message: err['message'],
            rejectVisible:false,
          })
        })
      },
      reject:() =>{}
    })
  }
  //确认启动按钮
  sureStartApproved(current){
    let cid = current['cid'];
    this.confirmationService.confirm({
      message: '是否启动?',
      accept: () => {
        this.inspectionService.updateInspectionStatuses(cid).subscribe(data=>{
          // this.selectMaterial = [];
          this.queryInsepection();
        },(err:Error)=>{
          this.confirmationService.confirm({
            message: err['message'],
            rejectVisible:false,
          })
        })
      },
      reject:() =>{}
    })
  }
  //取消遮罩层
  closeAddPlanTreeMask(bool){
    this.showViewUpdatePlan = bool;
  }
  //关闭查询所有计划遮罩层
  closeInspectionMask(bool){
    this.closeViewMask.emit(bool)
  }
  //分页查询
  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection()
  }
  //搜索功能
  suggestInsepectiones(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryInsepection()
  }
  queryInsepection(){
    this.inspectionService.queryInspectionTask(this.queryModel).subscribe(data=>{
      this.inspectionsObject = data.items;
      console.log(this.inspectionsObject)
      this.totalRecords = data.page.total;
      if(!this.inspectionsObject){
        this.inspectionsObject = [];
        this.selectInsepections =[]
      }
    });
  }
  // deleteInsepection(current){
  //   let cid = current['cid'];
  //   //请求成功后删除本地数据
  //   this.inspectionService.deleteInspectionPlanItems(cid).subscribe(()=>{
  //     this.queryInsepection();
  //   },(err:Error)=>{
  //     let message ;
  //     if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
  //       message = '似乎网络出现了问题，请联系管理员或稍后重试'
  //     }else{
  //       message = err
  //     }
  //     this.confirmationService.confirm({
  //       message: message,
  //       rejectVisible:false,
  //     })
  //   })
  // }
  deleteInsepection(current){
      let cid = current['cid'];
    this.confirmationService.confirm({
      message:'确认删除吗?',
      accept: () => {
        this.inspectionService.deleteInspectionPlanItems(cid).subscribe(()=>{
          this.queryInsepection();
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })
      },
      reject:() =>{

      }
    });
    //请求成功后删除本地数据
  }
  showOption(current){
    this.cuerrntInspectionPlan =  current;
    this.showViewDetailMask = !this.showViewDetailMask;
  }
  showTask(inspectionPlan){
    this.queryScheduleModel['inspection_plan_cid'] = inspectionPlan['cid']
    this.queryScheduleModel['range_time_start'] = inspectionPlan['start_time']
    this.queryScheduleModel['range_time_end'] = inspectionPlan['end_time']
    // this.queryScheduleModel['status'] = inspectionPlan['status']
    this.inspectionService.queryInspectionSchedule(this.queryScheduleModel).subscribe(datas=>{
       this.showTaskView.emit(datas)
    })
  }
  updateDevice(bool){
    this.showViewUpdatePlan = bool
    this.queryInsepection()
  }
  closeViewDetailMask(bool){
    this.showViewDetailMask = bool;
  }

}
