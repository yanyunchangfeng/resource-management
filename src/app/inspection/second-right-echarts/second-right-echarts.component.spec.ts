import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondRightEchartsComponent } from './second-right-echarts.component';

describe('SecondRightEchartsComponent', () => {
  let component: SecondRightEchartsComponent;
  let fixture: ComponentFixture<SecondRightEchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondRightEchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondRightEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
