import { Component, OnInit } from '@angular/core';
import {InspectionService} from "../inspection.service";

@Component({
  selector: 'app-inspection-overview',
  templateUrl: './inspection-overview.component.html',
  styleUrls: ['./inspection-overview.component.scss']
})
export class InspectionOverviewComponent implements OnInit {
  shifts;
  tableData;
  tableTime;
  listData;
  missInspectionStatus;
  topInspectionStatus;
  clickMiss1Color1;
  clickMiss1Color2;
  clickMiss1Color3;
  clickMiss1Color4;
  constructor(private inspectionService: InspectionService) {
  }

  ngOnInit() {
    this.missInspectionStatus = '今年';
    this.topInspectionStatus = '今年';
    this.clickMiss1Color1 = '#39B9C6';
    this.clickMiss1Color2 = '#39B9C6';
    this.clickMiss1Color3 = '#39B9C6';
    this.clickMiss1Color4 = '#39B9C6';
    this.getAllshiftSetting()
    this.tableData = [
        {"line_name": "巡检路线1", "num": "早班",  "color": "#72a5dd", "status": ["#00AF4B", "#EA2F84", "", "","", "#81C046","", "", "", "","", "","", "", "", "","", "","", "", "", "","", "",]},
        {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "#6D75B6", "", "#F44306","", "#34A4DD","", "", "", "","", "","", "", "", "","", "","", "", "", "","", "",]},
        {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "", "", "#F44306","", "","", "", "", "","", "","", "", "", "","", "","", "", "", "","", "",]},
        // {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "#6D75B6", "", "","", "","", "", "", "","", "","", "", "", "","", "","", "", "", "","", "",]},
        // {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "#6D75B6", "", "#F44306","", "#34A4DD","", "", "", "","", "","", "", "", "","", "","", "", "", "","", "",]},
        // {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "", "", "#F44306","", "","", "", "", "","", "","", "", "", "","", "","", "#6D75B6", "", "","", "",]},
        // {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "#6D75B6", "", "#F44306","", "#34A4DD","", "", "", "","", "","", "", "", "","", "","", "", "", "","", "",]},
        // {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "#6D75B6", "", "","", "#34A4DD","", "", "", "","", "","", "#F44306", "", "","", "","", "", "", "","", "",]},
        // {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "", "", "#F44306","", "#34A4DD","", "", "", "","", "","", "", "", "","", "","", "", "", "","", "",]},
        // {"line_name": "巡检路线2", "num": "早班",  "color": "#72a5dd", "status": ["", "#6D75B6", "", "","", "","", "#FFA506", "", "","", "","", "", "", "","", "","", "", "", "","", "",]},
      ],
      this.getTodayData()
      this.listData = []
    this.clickMiss1()
    this.clickTop1()

  }
  getAllshiftSetting() {
    this.inspectionService.getAllshiftSetting().subscribe(data => {
      this.shifts = data;
      console.log(this.shifts)
    });
  }
  // 获取总览数据
  getTodayData() {
    this.inspectionService.getReportView().subscribe(data=>{
      this.tableTime = data.times
      this.tableData = data.values
      console.log(this.tableTime)
    })
  }
  // 漏检分布切换
  clickMiss1(){
    this.missInspectionStatus = '今年'
    this.clickMiss1Color1 = '#06777C'
    this.clickMiss1Color2 = '#39B9C6'
  }
  clickMiss2(){
    this.missInspectionStatus = '本月'
    this.clickMiss1Color2 = '#06777C'
    this.clickMiss1Color1 = '#39B9C6'
  }
  clickTop1(){
    this.topInspectionStatus = '今年'
    this.clickMiss1Color3 = '#06777C'
    this.clickMiss1Color4 = '#39B9C6'
  }
  clickTop2(){
    this.topInspectionStatus = '本月'
    this.clickMiss1Color4 = '#06777C'
    this.clickMiss1Color3 = '#39B9C6'
  }
}
