import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateInspectionObjectComponent } from './update-inspection-object.component';

describe('UpdateInspectionObjectComponent', () => {
  let component: UpdateInspectionObjectComponent;
  let fixture: ComponentFixture<UpdateInspectionObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateInspectionObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateInspectionObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
