import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfirmationService} from "primeng/components/common/confirmationservice";
import {InspectionService} from "../inspection.service";

@Component({
  selector: 'app-update-inspection-object',
  templateUrl: './update-inspection-object.component.html',
  styleUrls: ['./update-inspection-object.component.scss']
})
export class UpdateInspectionObjectComponent implements OnInit {
  @Input()  currentInspection;
  @Output() closeMask = new EventEmitter();
  @Output() updateDev = new EventEmitter(); //添加巡检周期
  display;
  width;
  windowSize;
  constructor(
    private inspectionService:InspectionService,
  ) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    this.currentInspection =this.inspectionService.deepClone(this.currentInspection);

  }
  updateDevice(bool){
    this.inspectionService.updateInspectionObject(this.currentInspection).subscribe(()=>{
      this.updateDev.emit(bool)
    })
  }
  closeInspectionMask(bool) {
    this.closeMask.emit(bool)
  }
  inspectionCycles =[
    {label:'数值', value:'数值'},
    {label:'状态', value:'状态'},

  ];
  formSubmit(bool){
      this.updateDevice(bool)
  }
}
