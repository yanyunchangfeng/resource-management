import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondMothEchartsComponent } from './second-moth-echarts.component';

describe('SecondMothEchartsComponent', () => {
  let component: SecondMothEchartsComponent;
  let fixture: ComponentFixture<SecondMothEchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondMothEchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondMothEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
