import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionviewDetailDialogComponent } from './inspectionview-detail-dialog.component';

describe('InspectionviewDetailDialogComponent', () => {
  let component: InspectionviewDetailDialogComponent;
  let fixture: ComponentFixture<InspectionviewDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionviewDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionviewDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
