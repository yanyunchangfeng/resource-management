import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrUpdateInspectionComponent } from './add-or-update-inspection.component';

describe('AddOrUpdateInspectionComponent', () => {
  let component: AddOrUpdateInspectionComponent;
  let fixture: ComponentFixture<AddOrUpdateInspectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrUpdateInspectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrUpdateInspectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
