import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePlanStatusComponent } from './update-plan-status.component';

describe('UpdatePlanStatusComponent', () => {
  let component: UpdatePlanStatusComponent;
  let fixture: ComponentFixture<UpdatePlanStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePlanStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePlanStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
