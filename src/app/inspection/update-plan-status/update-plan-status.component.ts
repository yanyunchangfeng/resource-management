import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";

@Component({
  selector: 'app-update-plan-status',
  templateUrl: './update-plan-status.component.html',
  styleUrls: ['./update-plan-status.component.scss']
})
export class UpdatePlanStatusComponent implements OnInit {
  display;
  status;
  width;
  windowSize;
  submitData={
    "cid":'',
    "status":''
  }
  @Output() closeAddMask = new EventEmitter();
  @Input() currentInspection;
  @Output() updateDev = new EventEmitter(); //修改巡检周期
  constructor(private inspectionService: InspectionService  ) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    this.status = [
      // {label:'正常',value:'正常'},
      {label:'进行中',value:'进行中'},
      {label:'已终止',value:'已终止'},
    ];
    let statusIndex = this.getStatus();
    this.submitData.status = this.status[statusIndex]['label'];
    this.submitData.cid = this.currentInspection.cid;
    this.currentInspection = this.inspectionService.deepClone(this.currentInspection);
  }
  getStatus(){
    for(let i=0;i<this.status.length;i++){
      if(this.currentInspection.status === this.status[i]['label']){
        return i
      }
    }
  }
  updateDevice(bool){
    this.inspectionService.updateInspectionStatus(this.submitData).subscribe(()=>{
      this.updateDev.emit(bool);
    })
  }
  closeInspectionMask(bool){
    this.closeAddMask.emit(bool)
  }
  formSubmit(bool){
      this.updateDevice(bool)
  }
}
