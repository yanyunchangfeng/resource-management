import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondRightMothEchartsComponent } from './second-right-moth-echarts.component';

describe('SecondRightMothEchartsComponent', () => {
  let component: SecondRightMothEchartsComponent;
  let fixture: ComponentFixture<SecondRightMothEchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondRightMothEchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondRightMothEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
