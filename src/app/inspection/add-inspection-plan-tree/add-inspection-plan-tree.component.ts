import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Tree, TreeNode} from "primeng/primeng";
import {InspectionService} from "../inspection.service";

@Component({
  selector: 'app-add-inspection-plan-tree',
  templateUrl: './add-inspection-plan-tree.component.html',
  styleUrls: ['./add-inspection-plan-tree.component.scss']
})
export class AddInspectionPlanTreeComponent implements OnInit {
  width;
  windowSize;
  display;
  queryModel;
  @ViewChild('expandingTree')
  expandingTree: Tree;
  filesTree4: TreeNode[];
  @Output() closeAddMask = new EventEmitter();
  @Output() addTree = new EventEmitter();
  selected: TreeNode[];
  constructor( private inspectionService: InspectionService ) { }
  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    this.queryModel = {
      'id':'',
      'dep':''
    }
    this.inspectionService.getDepartmentDatas().subscribe( data =>{
      if(!data){
        data = [];
      }
      this.filesTree4 = data;
    })
  }

  //关闭遮罩层
  closeInspectionMask(bool){
    this.closeAddMask.emit(bool)
  }
  // 组织树懒加载
  nodeExpand(event) {
    if (event.node) {
      this.inspectionService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(res => {
        event.node.children  = res;
      });
    }
  }
  formSubmit(){
    let arr = []
    for(let key of this.selected){
      let obj={};
      obj['label']=key['label'];
      obj['oid']=key['oid'];
      arr.push(obj)
    }
    console.log(arr)
    this.addTree.emit(arr);
  }
}
