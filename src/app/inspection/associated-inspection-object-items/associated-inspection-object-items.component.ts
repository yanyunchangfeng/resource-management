import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {ConfirmationService} from "primeng/primeng";
import {StorageService} from "../../services/storage.service";

@Component({
  selector: 'app-associated-inspection-object-items',
  templateUrl: './associated-inspection-object-items.component.html',
  styleUrls: ['./associated-inspection-object-items.component.scss']
})
export class AssociatedInspectionObjectItemsComponent implements OnInit {
  @Output() closeMask = new EventEmitter();
  @Output() addDev =new EventEmitter();
  display = false;
  queryModel;
  inspectionsObject;
  addModel;
  cols;
  width;
  windowSize;
  chooseCids =[];
  totalRecords;
  selectInsepections = [];
  constructor(
    private inspectionService: InspectionService,
    private storageService:StorageService,
    private confirmationService:ConfirmationService,
  ) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    this.queryModel={
      "condition":{
        "inspection_content_cid":this.storageService.getCurrentInspectionObjectCid('currentinspectionobjectcid'),
        "region":"",
        "object":"",
        "add_time":"",
        "content":"",
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.addModel={
      "inspection_content_cid":this.storageService.getCurrentInspectionObjectCid('currentinspectionobjectcid'),
      "inspection_object_cids":this.chooseCids
    }
    this.cols = [
      {field: 'region', header: '区域'},
      {field: 'object', header: '巡检对象'},
      {field: 'content', header: '巡检内容'},
      {field: 'object_type', header: '类型'},
      {field: 'reference_value', header: '参考值'},
    ]
    this.queryInsepection();
  }

  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection()
  }
  queryInsepection(){
    this.inspectionService.queryInspectionObject(this.queryModel).subscribe(data=>{
      this.inspectionsObject = data.items;
      this.totalRecords = data.page.total;
      if(!this.inspectionsObject){
        this.inspectionsObject = [];
        this.selectInsepections =[]
      }
    });
  }
  suggestInsepections(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryInsepection()
  }
  closeInspectionObjectItemMask(bool){
    this.closeMask.emit(bool)
  }
  addInspectionObject(bool){
    this.chooseCids = [];
    for(let i = 0;i<this.selectInsepections.length;i++){
      this.chooseCids.push(this.selectInsepections[i]['cid'])
    }
    this.addModel.inspection_object_cids = this.chooseCids;
    this.inspectionService.addInspectionContentItems(this.addModel).subscribe(data=>{
      this.addDev.emit(bool)
    },(err:Error)=>{
      alert(err)
    })
  }
}
