import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatedInspectionObjectItemsComponent } from './associated-inspection-object-items.component';

describe('AssociatedInspectionObjectItemsComponent', () => {
  let component: AssociatedInspectionObjectItemsComponent;
  let fixture: ComponentFixture<AssociatedInspectionObjectItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatedInspectionObjectItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedInspectionObjectItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
