import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {StorageService} from "../../services/storage.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-edit-shift',
  templateUrl: './edit-shift.component.html',
  styleUrls: ['./edit-shift.component.scss']
})
export class EditShiftComponent implements OnInit {
  addTime: boolean = false;  //添加巡检周期时间                       //添加巡检周期时间模态对话框
  display :boolean = false;
  @Output() closeEditMask = new EventEmitter();
  @Output() addDev = new EventEmitter();          //添加巡检周期
  @Output() updateDev = new EventEmitter();      //添加巡检周期
  @Input() currentInspection;
  @Input() shiftState;
  editShifts;
  title = '编辑班次';
  totalRecords;
  currentCycleTime
  cols;
  state;
  chooseCids = [];
  selectInsepections;
  width;
  windowSize;
  queryModel = {
    "condition":{
      "inspection_cycle_cid":""
    },
    "page":{
      "page_size":"10",
      "page_number":"1"
    }
  }
  constructor(
    private inspectionService:InspectionService,
    private storageService:StorageService,
    private confirmationService:ConfirmationService
  ) {}

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    if(this.shiftState!=='update'){
      this.title = '查看班次'
    }
    this.cols = [
      {field: 'start_time', header: '开始时间'},
      {field: 'end_time', header: '结束时间'}
    ]
    this.queryModel.condition.inspection_cycle_cid = this.storageService.getCurrentShiftCid('currentcid')
    this.queryInspectionShift();
  }
  closeInspectionEditMask(bool){
    this.closeEditMask.emit(bool)
  }
  showAddTime(){
    this.state = 'add'
    this.addTime = !this.addTime;
  }

  updateAddTime(current){
    this.state = 'update';
    this.currentCycleTime = current
    this.addTime = !this.addTime;
  }
  closeAddCycleMask(bool){
    this.queryInspectionShift();
    this.addTime = bool
  }
  addTimeCycleTime(bool){
    this.queryInspectionShift();
    this.addTime = bool;
  }
  updateCycleTime(bool){
    this.queryInspectionShift();
    this.addTime = bool;
  }
  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[];
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);

    this.queryInspectionShift();
  }
  queryInspectionShift(){
    this.inspectionService.queryInspectionShift(this.queryModel).subscribe(data=>{
      this.editShifts =data.items;
      console.log(this.editShifts)
      if(!this.editShifts){
        this.editShifts = [];
        this.selectInsepections =[];
      }
      this.totalRecords = data.page.total;
    })
  }
  deleteInspectionShift(){
    for(let i = 0;i<this.selectInsepections.length;i++){
      this.chooseCids.push(this.selectInsepections[i]['cid'])
    }
    //请求成功后删除本地数据
    this.inspectionService.deleteInspectionShift(this.chooseCids).subscribe(()=>{
      this.queryInspectionShift();
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，请联系管理员或稍后重试'
      }else{
        message = err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
}
