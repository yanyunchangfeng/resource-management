import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-view-detail',
  templateUrl: './view-detail.component.html',
  styleUrls: ['./view-detail.component.scss']
})
export class ViewDetailComponent implements OnInit {
  @Input() cuerrntInspectionPlan;
  @Output() closeMask = new EventEmitter();
  display;
  width;
  windowSize;
  constructor() { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    console.log(this.cuerrntInspectionPlan)
  }
  closeViewDetailMask(bool){
    this.closeMask.emit(bool);
  }
}
