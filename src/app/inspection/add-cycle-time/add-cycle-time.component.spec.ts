import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCycleTimeComponent } from './add-cycle-time.component';

describe('AddCycleTimeComponent', () => {
  let component: AddCycleTimeComponent;
  let fixture: ComponentFixture<AddCycleTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCycleTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCycleTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
