import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../services/storage.service";
import {InspectionService} from "../inspection.service";

@Component({
  selector: 'app-inspection-view',
  templateUrl: './inspection-view.component.html',
  styleUrls: ['./inspection-view.component.scss']
})
export class InspectionViewComponent implements OnInit {
  events: any[];
  header;
  showViewPlanMask: boolean = false;          //添加弹出框
  showViewPlanTaskDetailMask: boolean = false;
  headerConfig: any;                          // 日历头设置
  optionsConfig: object;                      // 日历默认设置参数配置
  display;
  zh: any;                                    // 汉化
  beginTime;              // 开始时间
  endTime;                // 结束时间
  queryModel;
  intervalStatus;
  inspections;
  inspentiontitle; //巡检单的title
  shifts;
  inpectionOrder;
  starttime;
  endtime;
  constructor(
    private inspectionService: InspectionService,
    private storageService: StorageService,
  ){}
  ngOnInit() {
    let {startTime,endTime} = this.inspectionService.formatCalendarTime()
    this.starttime = startTime;
    this.endTime = endTime;
    this.queryModel={
      "inspection_plan_cid":"",
      "range_time_start":"",
      "range_time_end":"",
      "status":""
    };
    this.headerConfig = {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    };
    this.optionsConfig = {
      buttonText: {
        today: '今天',
        month: '月',
        week: '周',
        day: '日',
      },
      editable: true,
      eventLimit: true,
      height: 800
    };
    this.queryInsepection();
    this.getAllshiftSetting();

  }
  getAllshiftSetting() {
    this.inspectionService.getAllshiftSetting().subscribe(data => {
      this.shifts = data;
      console.log(this.shifts)
    });
  }
  //显示间隔两小时巡检详情
  onEventClick(event) {
    // this.intervalStatus = event.calEvent.status;
    // this.inspentiontitle = event.calEvent.title
    this.storageService.setCurrentInspectionMissionCid('permissioncid',event.calEvent.cid);
    this.inpectionOrder = event.calEvent;
    this.showViewPlanTaskDetailMask = !this.showViewPlanTaskDetailMask
  }
  queryInsepection(){
      this.queryModel.range_time_start = this.starttime;
      this.queryModel.range_time_end = this.endTime;
    this.inspectionService.queryInspectionSchedule(this.queryModel).subscribe(data=>{
      if(!data){
        this.events = [];
        return
      }
      this.events = data;
    });
  }
  onClickButton(event) {
    // let yearSting = event.view.title.substr(0,1);
    // let monthSting = event.view.title.substr(3,1);
    // let currentDate = new Date(event.view.title);
    // let currentDate = new Date(yearSting,monthSting);
    let {startTime,endTime} = this.inspectionService.formatCalendarTime(event.view.calendar.currentDate.format());

    // let year = currentDate.getFullYear();
    // let month = currentDate.getMonth()+1;
    // let lastDay = new Date(year,month-1,0);
    // let lastDate = lastDay.getDate();
    // let stringMonth = month<10?'0'+month:month;

    this.queryModel.range_time_start  =startTime;
    this.queryModel.range_time_end = endTime;
    this.inspectionService.queryInspectionSchedule(this.queryModel).subscribe(res => {
      this.events = res;
      console.log(this.events)
    });
  }
  showAddInspectionPlanMask(){
    this.showViewPlanMask = !this.showViewPlanMask
  }
  //取消遮罩层
  closeAddPlanTreeMask(bool){
    this.showViewPlanMask = bool;
  }
  //日历上显示计划任务
  showTask(task){
    this.events = task;
    this.showViewPlanMask = false;
  }
  closeIntervalMask(bool){
    this.showViewPlanTaskDetailMask = bool;
  }
  saveIntervalInspectionOrder(bool){
    this.queryInsepection();
    this.showViewPlanTaskDetailMask = bool;
  }
}
