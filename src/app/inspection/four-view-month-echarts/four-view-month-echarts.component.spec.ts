import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourViewMonthEchartsComponent } from './four-view-month-echarts.component';

describe('FourViewMonthEchartsComponent', () => {
  let component: FourViewMonthEchartsComponent;
  let fixture: ComponentFixture<FourViewMonthEchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourViewMonthEchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourViewMonthEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
