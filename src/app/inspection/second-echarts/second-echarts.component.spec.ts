import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondEchartsComponent } from './second-echarts.component';

describe('SecondEchartsComponent', () => {
  let component: SecondEchartsComponent;
  let fixture: ComponentFixture<SecondEchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondEchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
