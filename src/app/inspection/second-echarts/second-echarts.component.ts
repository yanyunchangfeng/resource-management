import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IntervalObservable} from "rxjs/observable/IntervalObservable";
import {InspectionService} from "../inspection.service";
import * as echarts from 'echarts';

@Component({
  selector: 'app-second-echarts',
  templateUrl: './second-echarts.component.html',
  styleUrls: ['./second-echarts.component.css']
})
export class SecondEchartsComponent implements OnInit, OnDestroy {
  @ViewChild('second') second: ElementRef;
  reportView;
  intervalObser;
  baseColors;
  reportViewName = [] ;
  reportViewCount = [];
  reportViewColors = [];
  option = {
    title: {
      text:'',
      top:20,
      x:'center',
      textStyle:{//标题内容的样式
        color:'#333333',//京东红
        fontStyle:'normal',//主标题文字字体风格，默认normal，有italic(斜体),oblique(斜体)
        fontWeight:"bold",//可选normal(正常)，bold(加粗)，bolder(加粗)，lighter(变细)，100|200|300|400|500...
        // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
        fontSize:14//主题文字z字体大小，默认为18px
      }
    },
    color:['#6acece'],
    tooltip : {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
      type: 'scroll',
      orient: 'vertical',
      // right: 0,
      left:0,
      top: 20,
      bottom: 20,
      data: []
    },
    series : [
      {
        // name: '姓名',
        type: 'pie',
        radius: '55%',
        center: ['50%', '50%'],
        data: [],
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        },
        label: {            //饼图图形上的文本标签
          normal: {
            show: true,
            position: 'inner', //标签的位置
            textStyle: {
              fontWeight: 300,
              fontSize: 16    //文字的字体大小
            },
            formatter: '{d}%'
          }
        }
      }
    ]
  };
  echart;
  constructor(private reportService:InspectionService) { }

  ngOnInit() {
    this.baseColors = [
      '#52BFD0',
      '#E5403D',
      '#E8B348',
      '#81C046',
    ]
    this.echart = echarts.init(this.second.nativeElement);
    this.reportService.getStatistics_miss().subscribe(data=>{
      this.reportView = data.team;
      if(!this.reportView){
        this.reportView = [];
      }
      let num = 0;
      for(let i = 0;i<this.reportView.length;i++){
        this.reportViewName.push(this.reportView[i].name);
        this.reportViewCount.push(this.reportView[i].value);
        if(num<this.baseColors.length){
          this.reportViewColors[i]= this.baseColors[num++];
        }else{
          num=0;
          this.reportViewColors[i] = this.baseColors[num++];
        }
      }
      this.option.series[0]['data'] = this.reportView;
      this.option.legend['data'] =  this.reportViewName;
      this.option.color = this.reportViewColors;
      this.echart.setOption(this.option);
      this.intervalObser=IntervalObservable.create(100).subscribe(index=>{
        this.initWidth()
      })
    })
  }
  initWidth(){
    this.echart.resize();
    this.intervalObser.unsubscribe();
  }
  ngOnDestroy(): void {
    this.echart.dispose(this.second.nativeElement);
    this.echart =null;
  }
  @HostListener('window:resize', ['$event'])
  public onWindowResize(event): void {
    this.echart.resize()
  }

}
