import { Injectable } from '@angular/core';
// import {Http,Response} from "@angular/http";
import {HttpClient} from "@angular/common/http";
import {StorageService} from "../services/storage.service";
import {environment} from "../../environments/environment";
import {current} from "codelyzer/util/syntaxKind";

@Injectable()
export class InspectionService {

  constructor(private http: HttpClient,private  storageService: StorageService ) {}
 //  整体完成情况
  // 当日巡检记录一览
  getReportView(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`, {
      'access_token': token,
      'type': 'overview_statistics_today_inspection_status',
      // 'type': 'overview_statistics_done',
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];

    });
  }
 //  整体完成情况
  getStatistics(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`, {
      'access_token': token,
      'type': 'overview_statistics_done',
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];

    });
  }
  // 漏检分布情况
 // 获取年
 getStatistics_miss(){
  let token = this.storageService.getToken('token');
  return this.http.post(`${environment.url.management}/inspection`, {
    'access_token': token,
    'type': 'overview_statistics_miss',
    'data': '0',
  }).map((res: Response) => {
    if (res['errcode'] !== '00000'){
      throw new Error(res['errmsg']);
    }
    return res['data'];

  });
}
 getStatisticsMonth_miss(){
  let token = this.storageService.getToken('token');
  return this.http.post(`${environment.url.management}/inspection`, {
    'access_token': token,
    'type': 'overview_statistics_miss',
    'data': '1',
  }).map((res: Response) => {
    if (res['errcode'] !== '00000'){
      throw new Error(res['errmsg']);
    }
    return res['data'];

  });
}
// 异常设备推移
getException_equip(){
  let token = this.storageService.getToken('token');
  return this.http.post(`${environment.url.management}/inspection`, {
    'access_token': token,
    'type': 'overview_statistics_exception_equip',
  }).map((res: Response) => {
    if (res['errcode'] !== '00000'){
      throw new Error(res['errmsg']);
    }
    return res['data'];

  });
}
// 异常设备TOPS
  getExceptionEquipTop(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`, {
      'access_token': token,
      'type': 'overview_statistics_exception_equip_top5',
      'data': '0',
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];

    });
  }
  getMonthExceptionEquipTop(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`, {
      'access_token': token,
      'type': 'overview_statistics_exception_equip_top5',
      'data': '1',
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];

    });
  }
  //添加巡检周期接口
  addInspection(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_cycle_add",
      "data": {
        "name":inspection.name,
        "cycle":inspection.cycle
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //修改巡检周期接口
  updateInspection(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_cycle_mod",
      "data": {
        "cid":inspection.cid,
        "name":inspection.name,
        "cycle":inspection.cycle
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询巡检周期接口
  queryInspection(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_cycle_get",
      "data": {
        "condition":{
          "name":inspection.condition.name.trim()
        },
        "page":{
          "page_size":inspection.page.page_size.trim(),
          "page_number":inspection.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //删除巡检周期接口
  deleteInspection(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token":token,
      "type": "inspection_cycle_del",
      "cids":ids
    }).map((res:Response) => {
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  deepClone(obj){
    var o,i,j,k;
    if(typeof(obj)!="object" || obj===null)return obj;
    if(obj instanceof(Array))
    {
      o=[];
      i=0;j=obj.length;
      for(;i<j;i++)
      {
        if(typeof(obj[i])=="object" && obj[i]!=null)
        {
          o[i]=this.deepClone(obj[i]);
        }
        else
        {
          o[i]=obj[i];
        }
      }
    }
    else
    {
      o={};
      for(i in obj)
      {
        if(typeof(obj[i])=="object" && obj[i]!=null)
        {
          o[i]=this.deepClone(obj[i]);
        }
        else
        {
          o[i]=obj[i];
        }
      }
    }
    return o;
  }
  //添加巡检班次接口
  addInspectionShift(inspectionShift){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_cycle_tour_add",
      "data": {
        "start_time":inspectionShift.start_time,
        "end_time":inspectionShift.end_time,
        "inspection_cycle_cid":inspectionShift.inspection_cycle_cid
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询巡检班次接口
  queryInspectionShift(inspectionShift){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_cycle_tour_get",
      "data":{
        "condition":{
          "inspection_cycle_cid":inspectionShift.condition.inspection_cycle_cid
        },
        "page":{
          "page_size":inspectionShift.page.page_size,
          "page_number":inspectionShift.page.page_number
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //删除巡检班次接口
  deleteInspectionShift(insepection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_cycle_tour_del",
      "cids": insepection
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //修改巡检班次
  updateInspectionShift(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_cycle_tour_mod",
      "data":{
        "cid":inspection.cid,
        "start_time":inspection.start_time,
        "end_time":inspection.end_time
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询巡检对象库
  queryInspectionObject(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_object_get",
      "data": {
        "condition":{
          "region":inspection.condition.region,
          "object":inspection.condition.object,
          "content":inspection.condition.content,
          "add_time":inspection.condition.add_time,
          "inspection_content_cid":inspection.condition.inspection_content_cid
        },
        "page":{
          "page_size":inspection.page.page_size.trim(),
          "page_number":inspection.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //删除巡检对象库接口
  deleteInspectionObject(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token":token,
      "type": "inspection_object_del",
      "cids":ids
    }).map((res:Response) => {
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //修改巡检对象接口
  updateInspectionObject(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_object_mod",
      "data": {
        "cid":inspection.cid,
        "region":inspection.region,
        "object":inspection.object,
        "content":inspection.content,
        "object_type":inspection.object_type,
        "reference_value":inspection.reference_value,
        "unit":inspection.unit,
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //添加巡检内容接口
  addInspectionContent(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_content_add",
      "data": {
        "name":inspection.name,
        "inspection_cycle_cid":inspection.inspection_cycle_cid,
        "create_people":inspection.create_people,
        "create_time":inspection.create_time,
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //修改巡检内容接口
  updateInspectionContent(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_content_mod",
      "data": {
        "cid":inspection.cid,
        "name":inspection.name,
        "inspection_cycle_cid":inspection.inspection_cycle_cid,
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //删除巡检内容接口
  deleteInspectionContent(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token":token,
      "type": "inspection_content_del",
      "cids":ids
    }).map((res:Response) => {
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询巡检内容
  queryInspectionContent(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_content_get",
      "data": {
        "condition":{
          "name":inspection.condition.name.trim(),
        },
        "page":{
          "page_size":inspection.page.page_size.trim(),
          "page_number":inspection.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //查询巡检周期接口4cid
  queryInspectionContent4Cid(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_cycle_get",
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //查询巡检内容Forcid
  queryInspectionContentForCid(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_content_get",
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  formatDropDownData(arr){
    let newarr = [];
    for(let i = 0; i<arr.length; i++){
      let obj = {};
      obj['label'] = arr[i]['name']
      obj['value'] = arr[i]['cid']
      newarr.push(obj)
    }
    return newarr
  }
  getFormatTime(date?) {
    if (!date){
      date= new Date();
    }else{
      date = new Date(date)
    }
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let hour = date.getHours();
    let minutes = date.getMinutes();
    let second = date.getSeconds();
    day = day <= 9 ? "0" + day : day;
    month = month <= 9 ? "0" + month : month;
    hour = hour <= 9 ? "0" + hour : hour;
    minutes = minutes <= 9 ? "0" + minutes : minutes;
    second = second <= 9 ? "0" + second : second;
    return  year+"-"+month+"-" +day+" " +hour+":"+minutes+":"+second
  }
  //添加巡检内容项接口
  addInspectionContentItems(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_content_item_add",
      "data": {
        "inspection_content_cid":inspection.inspection_content_cid,
        "inspection_object_cids":inspection.inspection_object_cids
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode!=='00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询巡检内容项接口
  queryInspectionContentItems(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_content_item_get",
      "data": {
        "condition":{
          "inspection_content_cid":inspection.condition.inspection_content_cid.trim(),
          "region":inspection.condition.region.trim(),
          "object":inspection.condition.object.trim(),
          "content":inspection.condition.content.trim(),
        },
        "page":{
          "page_size":inspection.page.page_size.trim(),
          "page_number":inspection.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  // 删除巡检内容项接口
  deleteInspectionContentItems(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token":token,
      "type": "inspection_content_item_del",
      "cids":ids
    }).map((res:Response) => {
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //  获取组织树
  getDepartmentDatas(oid?, dep?) {
    (!oid) && (oid = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/org`,
      {
        "access_token": token,
        "type": "get_suborg",
        "id": oid,
        "dep": dep
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if( body.errcode !== '00000') {
      //   return [];
      // }else {
      //   body['datas'].forEach(function (e) {
      //     e.label = e.name;
      //     delete e.name;
      //     e.leaf = false;
      //     e.data = e.name;
      //   });
      // }
      // return body['datas'];
      if( res['errcode'] !== '00000') {
        return [];
      }else {
        res['datas'].forEach(function (e) {
          e.label = e.name;
          delete e.name;
          e.leaf = false;
          e.data = e.name;
        });
      }
      return res['datas'];
    })
  }
  //添加巡检计划接口
  addInspectionPlan(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_plan_add",
      "data":{
        "name":inspection.name,
        "start_time":inspection.start_time,
        "end_time":inspection.end_time,
        "organs_ids":inspection.organs_ids,
        "create_people":inspection.create_people,
        "inspection_content_cids":inspection.inspection_content_cids
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //查询巡检任务接口
  queryInspectionTask(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_plan_get",
      "data":{
        "condition":{
          "name":inspection.name,
          "status":inspection.status
        },
        "page":{
          "page_size":"10",
          "page_number":"1"
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  // 删除巡检计划接口
  deleteInspectionPlanItems(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token":token,
      "type": "inspection_plan_del_allrelated",
      "ids":[ids]
    }).map((res:Response) => {
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //修改计划状态接口
  updateInspectionStatus(cid){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_plan_mod",
      "data": {
        "cid":cid,
        "status":"已终止"
      }
    }).map((res:Response)=>{

      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //修改计划状态接口1
  updateInspectionStatuses(cid){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_plan_mod",
      "data": {
        "cid":cid,
        "status":"进行中"
      }
    }).map((res:Response)=>{

      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询巡检任务日历数据接口
  queryInspectionSchedule(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token": token,
      "type": "inspection_mission_get_calendar",
      "data": {
        "inspection_plan_cid": inspection.inspection_plan_cid,
        "range_time_start": inspection.range_time_start,
        "range_time_end": inspection.range_time_end,
        "status": inspection.status
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.datas;
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['datas'];
    })
  }
//查询巡检任务详情接口
  queryInspectionTaskDetail(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token": token,
      "type": "inspection_mission_item_get",
      "data":{
        "condition":{
          "inspection_mission_cid":inspection.condition.inspection_mission_cid
        },
        "page":{
          "page_size":inspection.page.page_size,
          "page_number":inspection.page.page_number
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
//查询巡检报告接口
  queryInspectionTalk(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token": token,
      "type": "inspection_report_get",
      "data":{
        "condition":{
          "name":inspection.condition.name,
          "inspection_plan_cid":inspection.condition.inspection_plan_cid,
          "create_date_start":inspection.condition.create_date_start,
          "create_date_end":inspection.condition.create_date_end
        },
        "page":{
          "page_size":inspection.page.page_size,
          "page_number":inspection.page.page_number
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //提交巡检任务项接口
  addInspectionTaskItems(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_mission_item_mod",
      "datas":inspection
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }

  //添加巡检点接口
  addInspectionPoint(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_point_add",
      "data": {
        "name":inspection.name,
        "flag":inspection.flag,
        "region":inspection.region,
        "flag_type":inspection.flag_type,
        "point_order":inspection.point_order+"",
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //修改巡检点接口
  updateInspectionPoint(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_point_mod",
      "data": {
        "cid":inspection.cid,
        "name":inspection.name,
        "flag":inspection.flag,
        "region":inspection.region,
        "point_order":String(inspection.point_order),
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询巡检点接口
  queryInspectionPoint(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_point_get",
      "data": {
        "condition":{
          "name":inspection.condition.name.trim(),
          "flag":inspection.condition.flag.trim()
        },
        "page":{
          "page_size":inspection.page.page_size.trim(),
          "page_number":inspection.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //删除巡检点接口
  deleteInspectionPoint(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token":token,
      "type": "inspection_point_del",
      "cids":ids
    }).map((res:Response) => {
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.datas
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询巡检关联区域接口
  queryInspectionRegion(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_object_diffield_get",
      "data": {
         "field":"region",
       }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //巡检计划颜色获取接口
  getAllshiftSetting(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`, {
      "access_token": token,
      "type": "inspection_mission_get_status_color",
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //转换提交巡检两小时数据
  resverInspectionIntervalSubmitData(arr){
    let newarr=[];
    for(let i = 0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['cid'] = temp['cid'];
      obj['status'] = temp['status'];
      obj['real_value'] = temp['real_value']+"";
      obj['remark'] = temp['remark'];
      obj['inspection_mission_cid'] = temp['inspection_mission_cid'];
      newarr.push(obj);
    }
    return newarr
  }
  //查询已有对象巡检点接口
  queryInspectionPointItems(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_point_object_get",
      "data": {
        "condition":{
          "region":inspection.condition.region,
        },
        "page":{
          "page_size":inspection.page.page_size.trim(),
          "page_number":inspection.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //提交巡检点接口
  submitInspectionPoint(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_point_object_add",
      "data": {
        "inspection_point_cid":inspection.inspection_point_cid,
        "items":inspection.items
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //查询关联巡检点接口
  queryAssociatedInspectionPoint(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_point_object_get",
      "data": {
        "condition":{
          "inspection_point_cid":inspection.condition.inspection_point_cid,
          "name":inspection.condition.name,
          "flag":inspection.condition.flag
        },
        "page":{
          "page_size":inspection.page.page_size,
          "page_number":inspection.page.page_number
        }
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //删除关联巡检点接口
  deleteAssociatedInspectionPoint(inspection){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inspection`,{
      "access_token":token,
      "type":"inspection_point_object_add",
      "data": {
        "items":inspection.items
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  formatCalendarTime(date?){
    let currentDate;
    if(!date){
      currentDate=new Date()
    }else{
      currentDate = new Date(date)
    }
    let year = currentDate.getFullYear();//当前年
    let month = currentDate.getMonth()+1;//当前月
    let lastDay = new Date(year,month,0);
    let lastDate = lastDay.getDate();
    let stringMonth = month<10?'0'+month:month;
    return {
      startTime:year+'-'+stringMonth+"-01 00:00:00",
      endTime:year+'-'+stringMonth+'-'+lastDate+' 23:59:59'
    }
  }
  // formatCalendarTimenext(date?){
  //   let currentDate;
  //   if(!date){
  //     currentDate=new Date()
  //   }else{
  //     currentDate = new Date(date)
  //   }
  //   let year = currentDate.getFullYear();//当前年
  //   let month = currentDate.getMonth()+1;//当前月
  //   let lastDay = new Date(year,month+1,0);
  //   let lastDate = lastDay.getDate();
  //   let stringMonth = month<10?'0'+month:month;
  //   return {
  //     startTime:year+'-'+stringMonth+"-01 00:00:00",
  //     endTime:year+'-'+stringMonth+'-'+lastDate+' 23:59:59'
  //   }
  // }

  formatDropdownData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp;
      obj['value'] = temp;
      newarr.push(obj);
    }
    return newarr
  }
  formatPieData(arr){
    let newarr = [];
    for (let i = 0; i < arr.length; i++){
      let obj = {};
      let def = arr[i];
      obj['name'] = def ['name'];
      obj['value'] = def['count'];
      newarr.push(obj);
    }
    return newarr;
  }

}
