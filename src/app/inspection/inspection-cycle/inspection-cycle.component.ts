import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {ConfirmationService} from "primeng/primeng";
import {StorageService} from "../../services/storage.service";

@Component({
  selector: 'app-inspection-cycle',
  templateUrl: './inspection-cycle.component.html',
  styleUrls: ['./inspection-cycle.component.scss']
})
export class InspectionCycleComponent implements OnInit {
  showAddMask: boolean = false;     //添加弹出框
  editMask: boolean = false;       //编辑弹出框
  cols;                           //定义表格表头
  totalRecords;                  //总的数据条数
  inspections;                  //存储巡检数据
  currentInpection;            //同步更新修改数据
  state;                      //复用遮罩层的状态
  shiftState;
  queryModel;
  chooseCids =[];
  selectInsepections =[];
  // page分页
  constructor(
    private inspectionService: InspectionService,
    private confirmationService: ConfirmationService,
    private storageService:StorageService
  ) { }

  ngOnInit() {
    this.queryModel={
      "condition":{
        "name":""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.cols = [
      {field: 'name', header: '巡检周期名'},
      {field: 'cycle', header: '巡检周期'}
    ]
    this.queryInsepection();
  }
  //打开遮罩层
  showInspectionMask(){
    this.state = 'add'
    this.showAddMask = !this.showAddMask;
  }
  //添加成功
  addInsepection(bool){
    this.showAddMask = bool;
    this.queryInsepection()
  }
  updateOption(currentInspection){
    this.state = 'update'
    this.showAddMask = !this.showAddMask;
    this.currentInpection = currentInspection;
  }
  showOption(current){
    this.shiftState = 'view';
    this.storageService.setCurrentShifCid('currentcid',current.cid)
    this.editMask = !this.editMask;
  }
  deleteInspection(){
    for(let i = 0;i<this.selectInsepections.length;i++){
      this.chooseCids.push(this.selectInsepections[i]['cid'])
    }
    this.confirmationService.confirm({
      message:'确认删除吗?',
      accept: () => {
        this.inspectionService.deleteInspection(this.chooseCids).subscribe(()=>{
          this.queryInsepection();
          this.selectInsepections = [];
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })
      },
      reject:() =>{

      }
    });
    //请求成功后删除本地数据


  }
  closeInspectionMask(bool){
    this.showAddMask = bool;
  }

  //分页查询
  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection()
  }
  queryInsepection(){
    this.inspectionService.queryInspection(this.queryModel).subscribe(data=>{
      this.inspections = data.items;
      this.totalRecords = data.page.total;
      if(!this.inspections){
        this.inspections = [];
        this.selectInsepections =[]
      }
    });
  }
  //搜索功能
  suggestInsepections(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryInsepection()
  }
  //同步更新页面数据
  updateInspection(){
    this.queryInsepection()
  }
  //编辑班次模块
  showEditMask(current){
    this.editMask = !this.editMask;
    this.shiftState = 'update';
    this.storageService.setCurrentShifCid('currentcid',current.cid)
  }

  closeInspectionEditMask(bool){
    this.editMask = bool;
  }
  addTimeCycle(bool){
    this.editMask
  }
}
