import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionCycleComponent } from './inspection-cycle.component';

describe('InspectionCycleComponent', () => {
  let component: InspectionCycleComponent;
  let fixture: ComponentFixture<InspectionCycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionCycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionCycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
