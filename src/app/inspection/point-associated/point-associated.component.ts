import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {StorageService} from "../../services/storage.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-point-associated',
  templateUrl: './point-associated.component.html',
  styleUrls: ['./point-associated.component.css']
})
export class PointAssociatedComponent implements OnInit {
  display;
  @Output() closePointer =  new EventEmitter;
  showPointerItem: boolean = false;
  windowSize;
  width;
  cols;
  chooseCids =[];
  selectMaterial = [];
  totalRecords;
  queryModel;
  MaterialData;
  region;
  inspectionsObject;
  deleteModel;
  selectInsepections = [];
  constructor( private inspectionService:InspectionService,
               private storageService: StorageService,
               private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.5;
    }else{
      this.width = this.windowSize*0.5;
    }

    this.queryModel={
      "condition":{
        "inspection_point_cid":JSON.parse(this.storageService.getCurrentInspectionPointobj('InspectionPointObj'))['cid'],
        "region":"",
        "object":""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.deleteModel={
      "items":[]
    }
    this.display = true;
    this.cols = [
      {field: 'region', header: '对象名称'},
      {field: 'object', header: '区域'}
    ]
    this.queryInsepection();
  }
  //分页查询
  paginate(event) {
    this.chooseCids =[];
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection();
  }
  queryInsepection(){
    this.inspectionService.queryAssociatedInspectionPoint(this.queryModel).subscribe(data=>{
      this.inspectionsObject = data.items;
      console.log(this.inspectionsObject)
      this.totalRecords = data.page.total;
      if(!this.inspectionsObject){
        this.inspectionsObject = [];
        this.selectInsepections =[]
      }
    });
  }
  closePointerMask(bool){
    this.closePointer.emit(bool);
  }

  showPointerItemMask(){
    this.showPointerItem = !this.showPointerItem;
  }
  closePointerItemsMask(bool){
    this.showPointerItem = false;
  }
  addInsepectionContent(bool){
    this.queryInsepection();
    this.showPointerItem = bool;
  }
  //删除关联巡检点接口
  deleteInspection(){
    for(let i = 0;i<this.selectInsepections.length;i++){
      this.chooseCids.push(this.selectInsepections[i]['cid'])
    }
    let items = [];
    for(let i = 0;i<this.selectInsepections.length;i++){
      let temp = this.selectInsepections[i];
      let obj = {};
      obj['object'] = temp['object'];
      obj['region'] = temp['region'];
      items.push(obj);
    }
    this.deleteModel.items = items;
    this.confirmationService.confirm({
      message:'确认删除吗?',
      accept: () => {
        this.inspectionService.deleteAssociatedInspectionPoint(this.deleteModel).subscribe(()=>{
          this.queryInsepection();
          this.selectInsepections = [];
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })
      },
      reject:() =>{

      }
    });
    //请求成功后删除本地数据


  }
}
