import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointAssociatedComponent } from './point-associated.component';

describe('PointAssociatedComponent', () => {
  let component: PointAssociatedComponent;
  let fixture: ComponentFixture<PointAssociatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointAssociatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointAssociatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
