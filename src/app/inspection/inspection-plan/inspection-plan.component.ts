import {Component, OnInit} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {StorageService} from "../../services/storage.service";

@Component({
  selector: 'app-inspection-plan',
  templateUrl: './inspection-plan.component.html',
  styleUrls: ['./inspection-plan.component.scss']
})
export class InspectionPlanComponent implements OnInit {
  showViewPlanMask: boolean = false;          //添加弹出框
  showViewPlanTaskDetailMask: boolean = false;
  headerConfig: any;                          // 日历头设置
  optionsConfig: object;                      // 日历默认设置参数配置
  display;
  zh: any;                                    // 汉化
  beginTime;              // 开始时间
  endTime;                // 结束时间
  queryModel;
  events: any[];
  inspections;
  inpectionOrder;//巡检单
  shifts;
  starttime;
  endtime;
  //弹出框是否显示
  constructor( private inspectionService: InspectionService,
               private storageService: StorageService,
               ) { }
  ngOnInit() {
    let {startTime,endTime} = this.inspectionService.formatCalendarTime()
    console.log(startTime);
    console.log(endTime);
    this.starttime =startTime;
    this.endTime = endTime;
    this.queryModel={
        "inspection_plan_cid":"",
        "range_time_start":this.starttime,
        "range_time_end":this.endTime,
        "status":""
    };
    this.headerConfig = {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    };
    this.optionsConfig = {
      buttonText: {
        today: '今天',
        month: '月',
        week: '周',
        day: '日',
      },
      editable: true,
      eventLimit: true,
      height: 800
    };
    this.queryInsepection();
    this.getAllshiftSetting();
  }
  getAllshiftSetting() {
    this.inspectionService.getAllshiftSetting().subscribe(data => {
      this.shifts = data;
      console.log(this.shifts)
    });
  }
  //显示间隔两小时巡检详情
  onEventClick(event) {
    this.inpectionOrder = event.calEvent;
    this.storageService.setCurrentInspectionMissionCid('permissioncid',event.calEvent.cid);

    this.showViewPlanTaskDetailMask = !this.showViewPlanTaskDetailMask
  }
  queryInsepection(){
    // this.queryModel.range_time_start=this.starttime;
    // this.queryModel.range_time_end = this.endTime;
    this.inspectionService.queryInspectionSchedule(this.queryModel).subscribe(data=>{
      if(!data){
        this.events = [];
        return
      }
      this.events = data;
    });
  }
  onClickButton(event) {
    // let yearSting = event.view.title.substr(0,1);
    // let monthSting = event.view.title.substr(3,1);
    // let currentDate = new Date(event.view.title);
    // let currentDate = new Date(yearSting,monthSting);
    let {startTime,endTime} = this.inspectionService.formatCalendarTime(event.view.calendar.currentDate.format());
    // let year = currentDate.getFullYear();
    // let month = currentDate.getMonth()+1;
    // let lastDay = new Date(year,month-1,0);
    // let lastDate = lastDay.getDate();
    // let stringMonth = month<10?'0'+month:month;
    // let {startTime,endTime} = this.inspectionService.formatCalendarTime()
    // let {startTime,endTime} = this.inspectionService.formatCalendarTime()

    this.queryModel.range_time_start = startTime;
    this.queryModel.range_time_end = endTime;
    console.log( this.queryModel.range_time_start);
    console.log(  this.queryModel.range_time_end);
    this.inspectionService.queryInspectionSchedule(this.queryModel).subscribe(res => {
      this.events = res;
      console.log(this.events)
    });
  }
  showAddInspectionPlanMask(){
    this.showViewPlanMask = !this.showViewPlanMask
  }
  //取消遮罩层
  closeAddPlanTreeMask(bool){
    this.showViewPlanMask = bool;
  }
  //日历上显示计划任务
  showTask(task){
    this.events = task;
    this.showViewPlanMask = false;
  }
  closeIntervalMask(bool){
    this.showViewPlanTaskDetailMask = bool;
  }
  saveIntervalInspectionOrder(bool){
    this.queryInsepection();
    this.showViewPlanTaskDetailMask = bool;
  }
}
