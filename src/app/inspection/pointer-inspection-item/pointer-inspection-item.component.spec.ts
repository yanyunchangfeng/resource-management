import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointerInspectionItemComponent } from './pointer-inspection-item.component';

describe('PointerInspectionItemComponent', () => {
  let component: PointerInspectionItemComponent;
  let fixture: ComponentFixture<PointerInspectionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointerInspectionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointerInspectionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
