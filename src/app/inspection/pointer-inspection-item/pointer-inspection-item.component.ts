import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {StorageService} from "../../services/storage.service";

@Component({
  selector: 'app-pointer-inspection-item',
  templateUrl: './pointer-inspection-item.component.html',
  styleUrls: ['./pointer-inspection-item.component.css']
})
export class PointerInspectionItemComponent implements OnInit {
  display;
  @Output() closePointItems = new EventEmitter;
  @Output() addDev =new EventEmitter();
  inspectionsRegion;
  queryModel;
  cols;
  chooseCids =[];
  totalRecords;
  selectInsepections =[];
  addModel;
  windowSize;
  width;
  constructor(
    private inspectionService: InspectionService,
    private storageService:StorageService
  ) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.5;
    }else{
      this.width = this.windowSize*0.5;
    }

    this.display = true;
    this.queryModel={
      "condition":{
       "region":'',
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    if(this.storageService.getCurrentInspectionPointobj('InspectionPointObj')){
      this.queryModel.condition.region = JSON.parse(this.storageService.getCurrentInspectionPointobj('InspectionPointObj'))['region']
    }
    this.addModel={
      "inspection_point_cid":JSON.parse(this.storageService.getCurrentInspectionPointobj('InspectionPointObj'))['cid'],
      "items":[
        // {
        //   "region":JSON.parse(this.storageService.getCurrentInspectionPointobj('InspectionPointObj'))['region'],
        //   "object":JSON.parse(this.storageService.getCurrentInspectionPointobj('InspectionPointObj'))['object'],
        // }
      ]
    }
    this.cols = [
      {field: 'region', header: '巡检区域'},
      {field: 'object', header: '巡检对象'}
    ];

    this.queryInsepection();
  }
  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection()
  }

  queryInsepection(){
    this.inspectionService.queryInspectionPointItems(this.queryModel).subscribe(data=>{
      console.log(data);
      if(!data['items']){
        this.inspectionsRegion = [];
        this.totalRecords = 0;
      }
      this.inspectionsRegion = data['items'];
      this.totalRecords = data['page']['total'];
    },(err:Error)=>{
      console.log(err);
    });
  }
  cloasePointerItems(bool){
    this.closePointItems.emit(bool);
  }

  addInspectionObject(bool){
    // this.chooseCids = [];
    let items = [];
    for(let i = 0;i<this.selectInsepections.length;i++){
      let temp = this.selectInsepections[i];
      let obj = {};
      obj['object'] = temp['object'];
      obj['region'] = temp['region'];
      items.push(obj);
     // object.push(this.selectInsepections[i]['object'])
    }
    // for(let i = 0;i<this.addModel['items']['length'];i++){
    //   let temp = this.addModel['items'][i];
    //   temp['object'] = object[i]['object'];
    // }
    this.addModel.items = items;
    this.inspectionService.submitInspectionPoint(this.addModel).subscribe(data=>{
      this.addDev.emit(data)
    },(err:Error)=>{
      alert(err)
    })
  }
}
