import {Component, OnInit} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {ConfirmationService} from "primeng/primeng";
import {StorageService} from "../../services/storage.service";

@Component({
  selector: 'app-inspection-content-management',
  templateUrl: './inspection-content-management.component.html',
  styleUrls: ['./inspection-content-management.component.scss']
})
export class InspectionContentManagementComponent implements OnInit {
  showAddMask: boolean = false;          //添加弹出框
  showAssciatedMask: boolean = false;   //关联巡检对话框
  state;
  inspectionNames;
  selectInsepections;
  cols;
  queryModel;
  chooseCids =[];
  inspections;
  totalRecords;
  currentInpection;
  constructor(
    private inspectionService:InspectionService,
    private storageService:StorageService,
    private confirmationService: ConfirmationService,
  ) { }

  ngOnInit() {
    this.queryModel={
      "condition":{
        "name":""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.cols = [
      {field: 'name', header: '巡检内容名'},
      {field: 'inspection_cycle_name', header: '巡检周期'},
      {field: 'create_people', header: '制作人'},
      {field: 'create_time', header: '制作时间'},
    ]
    this.queryInsepection();
     //获得巡检周期模块的所属周期cid
     this.inspectionService.queryInspectionContent4Cid().subscribe(data =>{
        this.inspectionNames = data.items;
        if(!this.inspectionNames){
          this.inspectionNames = [];
        }
     });
  }
  //打开遮罩层
  showInspectionContentMask(){
    this.state = 'add'
    this.showAddMask = !this.showAddMask;
  }
  //取消遮罩层
  closeInspectionMask(bool){
    this.queryInsepection();
    this.showAddMask = bool;
  }
  //添加成功
  addInsepection(bool){
    this.showAddMask = bool;
    this.queryInsepection()
  }
  //修改
  updateOption(currentInspection){
    this.state = 'update'
    this.showAddMask = !this.showAddMask;
    this.currentInpection = currentInspection;
    this.storageService.setCurrentInspectionObjctCid('currentinspectionobjectcid',currentInspection.inspection_cycle_cid)
  }
  //搜索功能
  suggestInsepectiones(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryInsepection()
  }
  //同步更新页面数据
  updateInspectionContent(){
    this.queryInsepection()
  }
  //打开关联巡检对象遮罩层
  showAssociatedInspectionMask(currentInspection){
    this.storageService.setCurrentInspectionObjctCid('currentinspectionobjectcid',currentInspection.cid)
    this.showAssciatedMask = !this.showAssciatedMask;
  }
  closeAssociatedInspectionMask(bool){
    this.showAssciatedMask = bool;
  }
  //分页查询
  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection()
  }
  queryInsepection(){
    this.inspectionService.queryInspectionContent(this.queryModel).subscribe(data=>{
      this.inspections = data.items;
      this.totalRecords = data.page.total;
      if(!this.inspections){
        this.inspections = [];
        this.selectInsepections =[]
      }
    });
  }

  deleteInspection(){
    for(let i = 0;i<this.selectInsepections.length;i++){
      this.chooseCids.push(this.selectInsepections[i]['cid'])
    }
    this.confirmationService.confirm({
      message:'确认删除吗?',
      accept: () => {
        this.inspectionService.deleteInspectionContent(this.chooseCids).subscribe(()=>{
          this.queryInsepection();
          this.selectInsepections = [];
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })
      },
      reject:() =>{

      }
    });
    //请求成功后删除本地数据


  }
}
