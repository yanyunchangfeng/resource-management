import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {ConfirmationService} from "primeng/primeng";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-inspection-point-configuration',
  templateUrl: './add-inspection-point-configuration.component.html',
  styleUrls: ['./add-inspection-point-configuration.component.css']
})
export class AddInspectionPointConfigurationComponent implements OnInit {
  display;
  windowSize;
  width;
  region;
  inspectionsRegion;
  statusNames = [];
  submitAddInspection;
  // @Input() region;
  @Output() addDev = new EventEmitter(); //添加巡检周期
  @Output() updateDev = new EventEmitter(); //修改巡检周期
  @Input()  currentInspection;
  @Input()  state;
  title = '新增'
  @Output() closeInspectionPoint = new EventEmitter;
  reportForm: FormGroup;//报障表单验证声明
  fb:FormBuilder =  new FormBuilder();

  constructor( private inspectionService:InspectionService,
               private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.submitAddInspection = {
      "name":"",
      "flag":"",
      "region":"",
      "flag_type":"",
      "point_order":"",
    };
    this.currentInspection = this.inspectionService.deepClone(this.currentInspection);
    if(this.state === 'update'){
      this.title = '修改'
    };
    this.statusNames = [
      {label: '二维码', value: '二维码'},
      {label: 'NFC', value: 'NFC'},
      {label: '条形码', value: '条形码'},
    ]

    if(this.state === 'update'){
      this.initSatus();
    }else{
      this.submitAddInspection.flag_type = this.statusNames[0]['label'];
    }
    this.reportForm = this.fb.group({
      'name': ['', Validators.required],
      'flag_type':['', Validators.required],
      'flag': ['', Validators.required],
      'region': ['', Validators.required],
      'point_order':[''],
    });
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.4;
    }else{
      this.width = this.windowSize*0.6;
    }
    this.display = true;
    this.queryInsepectionRegion();
  }
  //获取关联区域
  queryInsepectionRegion(){

    this.inspectionService.queryInspectionRegion().subscribe(data=>{
      if(!data){
        this.inspectionsRegion = [];
      }
      this.inspectionsRegion = data;
      this.region = this.inspectionService.formatDropdownData(this.inspectionsRegion)
      if(this.state ==='update'){
        this.initRegion();
      }
      this.submitAddInspection.region = this.region[0]['label'];
    });
  }
  addDevice(bool){
    this.submitAddInspection.point_order == null&&(this.submitAddInspection.point_order='');
    this.inspectionService.addInspectionPoint(this.submitAddInspection).subscribe(()=>{
      this.addDev.emit(bool);
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
  updateDevice(bool){
    this.inspectionService.updateInspectionPoint(this.currentInspection).subscribe(()=>{
      this.updateDev.emit(this.currentInspection);
      this.closeInspectionPoint.emit(bool)
    })
  }
  formSubmit(bool){
    if(this.state ==='add'){
      this.addDevice(bool)
    }else{
      this.updateDevice(bool)
    }
  }
  closeInspectionPointMask(bool){
    this.closeInspectionPoint.emit(bool)
  }
  initSatus(){
    for(let i =0;i<this.statusNames.length;i++){
      if(this.statusNames[i]['label'] === this.currentInspection['flag_type']){
        this.currentInspection['flag_type'] =  this.statusNames[i]['label'];
        break;
      }
    }
  }
  initRegion(){
    for(let i =0;i<this.region.length;i++){
      if(this.region[i]['label'] === this.currentInspection['region']){
        this.currentInspection['region'] =  this.region[i]['label'];
        break;
      }
    }
  }
}
