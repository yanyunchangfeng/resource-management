import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInspectionPointConfigurationComponent } from './add-inspection-point-configuration.component';

describe('AddInspectionPointConfigurationComponent', () => {
  let component: AddInspectionPointConfigurationComponent;
  let fixture: ComponentFixture<AddInspectionPointConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInspectionPointConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInspectionPointConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
