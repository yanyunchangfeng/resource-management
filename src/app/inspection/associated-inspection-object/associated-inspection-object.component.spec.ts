import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatedInspectionObjectComponent } from './associated-inspection-object.component';

describe('AssociatedInspectionObjectComponent', () => {
  let component: AssociatedInspectionObjectComponent;
  let fixture: ComponentFixture<AssociatedInspectionObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatedInspectionObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedInspectionObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
