import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {StorageService} from "../../services/storage.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-associated-inspection-object',
  templateUrl: './associated-inspection-object.component.html',
  styleUrls: ['./associated-inspection-object.component.scss']
})
export class AssociatedInspectionObjectComponent implements OnInit {
  showInspectionItemsMask: boolean =false;
  display :boolean = false;
  @Output() closeAssciatedMask = new EventEmitter();
  chooseCids=[];
  totalRecords;
  inspectionsObject;
  selectInsepections;
  cols;
  width;
  windowSize;
  queryModel;
  constructor(
    private inspectionService:InspectionService,
    private storageService:StorageService,
    private confirmationService:ConfirmationService
  ) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    this.queryModel={
      "condition":{
          "inspection_content_cid":this.storageService.getCurrentInspectionObjectCid('currentinspectionobjectcid'),
          "region":"",
          "object":"",
          "content":""
        },
      "page":{
          "page_size":"10",
          "page_number":"1"
       }
    };
    this.cols = [
      {field: 'region', header: '区域'},
      {field: 'object', header: '巡检对象'},
      {field: 'content', header: '巡检内容'},
      {field: 'object_type', header: '类型'},
      {field: 'reference_value', header: '参考值'},
    ]
    this.queryInsepection();
  }
  //分页查询
  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection()
  }
  suggestInsepections(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryInsepection()
  }
  queryInsepection(){
    this.inspectionService.queryInspectionContentItems(this.queryModel).subscribe(data=>{
      if(!this.inspectionsObject){
        this.inspectionsObject = [];
        this.selectInsepections =[]
      }
      this.inspectionsObject = data.items;
      this.totalRecords = data.page.total;

    });
  }
  showInspectionObjectItemsMask(){

    this.showInspectionItemsMask = !this.showInspectionItemsMask;
  }
  closeInspectionObjectMask(bool){
    this.closeAssciatedMask.emit(bool)
  }
  closeInspectionObjectItemsMask(bool){
    this.showInspectionItemsMask = bool
  }
  addInsepectionContent(bool){
    this.queryInsepection();
    this.showInspectionItemsMask = bool;
  }
  deleteInspection(){
    for(let i = 0;i<this.selectInsepections.length;i++){
      this.chooseCids.push(this.selectInsepections[i]['cid'])
    }
    //请求成功后删除本地数据
    this.confirmationService.confirm({
      message:'是否删除',
      accept: () => {
        this.inspectionService.deleteInspectionContentItems(this.chooseCids).subscribe(()=>{
          this.queryInsepection();

        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })
      },
      reject:() =>{}
    });
  }
}
