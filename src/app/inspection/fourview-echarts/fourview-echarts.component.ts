import { Component, OnInit } from '@angular/core';
import {InspectionService} from "../inspection.service";

@Component({
  selector: 'app-fourview-echarts',
  templateUrl: './fourview-echarts.component.html',
  styleUrls: ['./fourview-echarts.component.css']
})
export class FourviewEchartsComponent implements OnInit {
  viewColor1;
  viewColor3; // 冷水泵#2
  viewColor4; // UPS主机#2
  viewColor5; // 精密空调#1
  viewColor6
  viewCount;
  count1;
  count2;
  count3;
  count4;
  count5;
  shifts = [];
  equipname1;
  equipname2;
  equipname3;
  equipname4;
  equipname5;
  valueData = [];
  maxValue;
  constructor(private inspectionService: InspectionService) { }

  ngOnInit() {
    this.viewCount = 17
    this.count1 = 19
    this.count2 = 12
    this.count3 = 25
    this.count4 = 29
    this.count5 = 5
    this.viewColor1= [
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
    ]
    this.viewColor3= [
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
    ]
    this.viewColor4= [
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
    ]
    this.viewColor5= [
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
    ]
    this.viewColor6= [
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
      { dataColor: '#fff' },
    ]
    // this.shifts=[{"name":"设备1","value":"19"},{"name":"设备2","value":"5"},{"name":"设备3","value":"4"},
    //   {"name":"","value":""},{"name":"","value":""}
    //   ]
    this.getViewCount()

  }
// 计算显示的值
  getViewCount() {
    this.inspectionService.getExceptionEquipTop().subscribe(data => {
      this.shifts = data;
      for (let j=0; j < this.shifts.length; j++) {
        this.valueData.push(this.shifts[j].value)
      }
      this.maxValue = Math.max.apply(null, this.valueData)
      console.log(this.valueData)
      console.log(this.maxValue)
      this.equipname1 = this.shifts[0].name
      this.equipname2 = this.shifts[1].name
      this.equipname3 = this.shifts[2].name
      this.equipname4 = this.shifts[3].name
      this.equipname5 = this.shifts[4].name
      this.count1 = this.shifts[0].value
      this.count2 = this.shifts[1].value
      this.count3 = this.shifts[2].value
      this.count4 = this.shifts[3].value
      this.count5 = this.shifts[4].value
      let cou = Math.round(40*(this.shifts[0].value)/this.maxValue)
      for (var i = 0; i < cou; i++) {
        this.viewColor1[i].dataColor = '#52BFD0'
      }
      let cou1 = Math.round(40*(this.shifts[1].value)/this.maxValue)
      for (var i = 0; i < cou1; i++) {
        this.viewColor3[i].dataColor = '#52BFD0'
      }
      let cou2 = Math.round(40*(this.shifts[2].value)/this.maxValue)
      for (var i = 0; i < cou2; i++) {
        this.viewColor4[i].dataColor = '#52BFD0'
      }
      let cou3 = Math.round(40*(this.shifts[3].value)/this.maxValue)
      for (var i = 0; i < cou3; i++) {
        this.viewColor5[i].dataColor = '#52BFD0'
      }
      let cou4 = Math.round(40*(this.shifts[4].value)/this.maxValue)
      for (var i = 0; i < cou4; i++) {
        this.viewColor6[i].dataColor = '#52BFD0'
      }
    });
  }
}
