import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourviewEchartsComponent } from './fourview-echarts.component';

describe('FourviewEchartsComponent', () => {
  let component: FourviewEchartsComponent;
  let fixture: ComponentFixture<FourviewEchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourviewEchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourviewEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
