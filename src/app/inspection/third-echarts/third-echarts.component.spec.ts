import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdEchartsComponent } from './third-echarts.component';

describe('ThirdEchartsComponent', () => {
  let component: ThirdEchartsComponent;
  let fixture: ComponentFixture<ThirdEchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdEchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
