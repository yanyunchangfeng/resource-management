import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {IntervalObservable} from "rxjs/observable/IntervalObservable";
import {InspectionService} from "../inspection.service";
import * as echarts from 'echarts';

@Component({
  selector: 'app-third-echarts',
  templateUrl: './third-echarts.component.html',
  styleUrls: ['./third-echarts.component.css']
})
export class ThirdEchartsComponent implements OnInit {
  @ViewChild('third') third: ElementRef;
  echart;
  report;
  intervalObser;
  reportName = [];
  reportCount = [];
  public opt: any = {
    color: ['#52BFD0'],
    title: {
      text:'',
      top:0,
      x:'center',
      textStyle:{//标题内容的样式
        color:'#333333',//京东红
        fontStyle:'normal',//主标题文字字体风格，默认normal，有italic(斜体),oblique(斜体)
        fontWeight:"bold",//可选normal(正常)，bold(加粗)，bolder(加粗)，lighter(变细)，100|200|300|400|500...
        // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
        fontSize:14//主题文字z字体大小，默认为18px
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // type: 'cross'
        type: 'shadow'
      }
    },
    xAxis: [{
      type: 'category',
      data: []
    }],
    yAxis: [{
      type: 'value',
      splitLine:{ // 去掉Y轴网格线
        show:false
      }
    }],
    series: [{
      name: '异常设备数',
      type: 'bar',
      barCategoryGap: '50%',
      data: [0, 0, 0, 0, 0, 0],
      itemStyle: {
        normal: {
          label: {
            show: true, //开启显示
            position: 'top', //在上方显示
            textStyle: { //数值样式
              color: 'black',
              fontSize: 16
            }
          }
        }
      }
    }]
  };
  constructor( private inspectionService: InspectionService) { }

  ngOnInit() {
    this.echart = echarts.init(this.third.nativeElement);
    this.inspectionService.getException_equip().subscribe(data=>{

      this.report = data
      if(!this.report){
        this.report = [];
      }
      this.opt.series[0]['data'] = this.report.values;
      this.opt.xAxis[0]['data'] = this.report.times;
      this.echart.setOption(this.opt);
      this.intervalObser = IntervalObservable.create(100).subscribe(index=>{
        this.initWidth();
      })
    })
  }
  initWidth(){
    this.echart.resize();
    this.intervalObser.unsubscribe();
  }
  ngOnDestroy(): void {
    this.echart.dispose(this.third.nativeElement);
    this.echart = null;
  }
  @HostListener('window:resize', ['$event'])
  public onWindowResize(event): void {
    this.echart.resize()
  }


}
