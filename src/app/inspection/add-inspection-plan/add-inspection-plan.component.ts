import { Component,  OnInit} from '@angular/core';
import { MenuItem, Message } from "primeng/primeng";
import { InspectionService } from "../inspection.service";
import { FormBuilder, Validators } from "@angular/forms";
import {StorageService} from "../../services/storage.service";
import {ActivatedRoute, Router} from "@angular/router";
import {onlytimeValidator, timeValidator} from "../../validator/validators";

@Component({
  selector: 'app-add-inspection-plan',
  templateUrl: './add-inspection-plan.component.html',
  styleUrls: ['./add-inspection-plan.component.scss']
})
export class AddInspectionPlanComponent implements OnInit {
  width;
  windowSize;
  newInspectionPlan;
  showAddPlanMask: boolean = false;          //添加弹出框
  display ;
  zh: any;
  nowDate;// 汉化
  beginTime: Date;                       // 开始时间
  endTime: Date;// 结束时间
  msgs: Message[] = [];                  // 验证消息提示
  queryModel = {
    filed:'',
    value:''
  };
  items: MenuItem[];
  loading: boolean;
  departments =[];
  inspectionContents;
  submitData = {
    "name":"",
    "start_time":"",
    "end_time":"",
    "status":"",
    "organs_ids":[],
    "create_people":'',
    "orggans_name":'',
    "organs_names":[],
    "inspection_content_cids":[]
  };
  constructor(
    private inspectionService:InspectionService,
    private storageService:StorageService,
    private route:ActivatedRoute,
    private router:Router,
    fb:FormBuilder
  ) {
    this.newInspectionPlan = fb.group({
      inspectionName: ['', Validators.required],
      inspectiondepartment: ['', Validators.required],
      timeGroup:fb.group({
        startTime: ['', Validators.required],
        endTime: ['', Validators.required],
      },{validator:onlytimeValidator}),
      inspectionContent: ['', Validators.required]

    });
  }

  ngOnInit() {
    this.nowDate = new Date();
    console.log(this.newInspectionPlan)
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.9;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.display = true;
    this.loading = true;
    this.beginTime = new Date();
    this.endTime = new Date();
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.submitData.create_people = this.storageService.getUserName('userName');
    this.inspectionService.queryInspectionContentForCid().subscribe(data=>{
       console.log(data)
       this.inspectionContents = data.items
    });

  }

  showAddInspectionPlanMask(){
    this.showAddPlanMask = !this.showAddPlanMask
  }
  //取消遮罩层
  closeAddPlanTreeMask(bool){
    // this.queryInsepection();
    this.showAddPlanMask = bool;
  }
  clearTreeDialog () {
    this.submitData.orggans_name = '';
    this.submitData.organs_ids = [];
  }
  addPlanOrg(org){
    this.submitData.organs_ids = [];
    this.submitData.organs_names = [];
    for(let i = 0;i<org.length;i++){
      this.submitData.organs_ids.push(org[i]['oid'])
      this.submitData.organs_names.push(org[i]['label'])
    }
    this.submitData.orggans_name =  this.submitData.organs_names.join(',');
    this.showAddPlanMask = false;
  }
  formSubmit(){
    console.log(this.submitData)
    this.inspectionService.addInspectionPlan(this.submitData).subscribe( data=>{
      console.log(data);
      this.router.navigate(['../plan'],{relativeTo:this.route})

    },(err:Error)=>{
      alert(err)
    })
  }
  goBack(){
    window.history.back();
  }
}
