import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInspectionPlanComponent } from './add-inspection-plan.component';

describe('AddInspectionPlanComponent', () => {
  let component: AddInspectionPlanComponent;
  let fixture: ComponentFixture<AddInspectionPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInspectionPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInspectionPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
