import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InspectionService} from "../inspection.service";
import {StorageService} from "../../services/storage.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-interval-inspection-order',
  templateUrl: './interval-inspection-order.component.html',
  styleUrls: ['./interval-inspection-order.component.scss']
})
export class IntervalInspectionOrderComponent implements OnInit {
  @Output() closeMask = new EventEmitter();
  @Output() saveIntervalInspectionOrder = new EventEmitter();
  @Input() inpectionOrder;
  queryModel;
  cols;
  unDoneCols;
  display;
  chooseCids =[];
  inspections;
  totalRecords;
  width;
  canSubmit:boolean;
  windowSize;
  selectInsepections;
  insepectionvalue;
  constructor(
    private inspectionService: InspectionService,
    private storageService: StorageService,
    private confirmationService:ConfirmationService  ) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.6;
    }else{
      this.width = this.windowSize*0.8;
    }
    this.queryModel={
      "condition":{
        "inspection_mission_cid":this.storageService.getCurrentInspectionMissionCid('permissioncid'),
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.display = true;
    this.cols = [
      {field: 'region', header: '区域',editable:false},
      {field: 'object', header: '巡检设备',editable:false},
      {field: 'content', header: '巡检内容',editable:false},
      {field: 'object_type', header: '类型',editable:false},
      {field: 'reference_value', header: '参考值',editable:false},
      {field: 'status', header: '状态',editable:false},
      {field: 'complection_time', header: '完成时间',editable:false},
      {field: 'complection_personnal', header: '完成人',editable:false},
      // {field: 'real_value', header: '巡检记录值',editable:this.isEditable(this.inpectionOrder['status'])},
      // {field: 'remark', header: '备注',editable:this.isEditable(this.inpectionOrder['status'])},
    ];
    this.insepectionvalue =[
      {label: '正常', value: '正常'},
      {label: '异常', value: '异常'}
    ];

    this.queryInsepection();
  }
  isEditable(status){
    if(status ==='进行中'||status==='超时未完成'){
      this.canSubmit = true;
      return true
    }else{
      this.canSubmit =  false;
      return false;
    }
  }
//分页查询
  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection()
  }
  queryInsepection(){
    this.inspectionService.queryInspectionTaskDetail(this.queryModel).subscribe(data=>{
      if(!data.items){
        this.inspections = [];
        this.totalRecords = 0;
      }
      this.inspections = data.items;
      this.initInsepection();
      this.totalRecords = data.page.total

    });
  }

  closeIntervalMask(bool){
    this.closeMask.emit(bool);
  }
  tableEditComplete($event){
    console.log($event)
  }
  saveIntervalOrder(){
    console.log(this.inspections);
    let inpections = this.inspectionService.resverInspectionIntervalSubmitData(this.inspections);
    for(let i = 0;i<inpections.length;i++){
      inpections[i]['status'] = '已完成';
      if(!inpections[i]['real_value']||!inpections[i]['real_value']){
        this.confirmationService.confirm({
          message: '巡检记录值不能为空',
          rejectVisible:false,
        })
        return
      }
    }
    this.inspectionService.addInspectionTaskItems(inpections).subscribe(()=>{

    })
    this.saveIntervalInspectionOrder.emit(false);
  }
  initInsepection(){
    for(let i = 0 ;i<this.inspections.length;i++){
      if(this.inspections[i]['object_type']=="数值"){
        this.inspections[i]['showSelect'] = false;
      }else{
        this.inspections[i]['showSelect'] = true
      }
    }
    // for(let i = 0 ;i<this.inspections.length;i++){
    //   if(this.inspections[i]['optional_value'].search(/&/)!==-1){
    //
    //     this.inspections[i]['showSelect'] = true;
    //   }else{
    //     this.inspections[i]['showSelect'] = false
    //   }
    // }
    // for(let  i = 0 ;i<this.inspections.length;i++){
    //   if(this.inspections[i]['optional_value'].search(/&/)!==-1){
    //     let inspectionvalue = this.inspections[i]['optional_value'].split('&')
    //     this.inspections[i]['insepectionvalue'] = [];
    //     for(let j = 0 ;j<inspectionvalue.length;j++){
    //       let temp = inspectionvalue[j];
    //       this.inspections[i]['insepectionvalue'].push({label:temp,value:temp})
    //     }
    //   }
    // }
  }
}
