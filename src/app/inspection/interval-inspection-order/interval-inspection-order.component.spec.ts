import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntervalInspectionOrderComponent } from './interval-inspection-order.component';

describe('IntervalInspectionOrderComponent', () => {
  let component: IntervalInspectionOrderComponent;
  let fixture: ComponentFixture<IntervalInspectionOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntervalInspectionOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntervalInspectionOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
