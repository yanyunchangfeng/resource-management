import { Component, OnInit } from '@angular/core';
import {InspectionService} from "../inspection.service";
import {StorageService} from "../../services/storage.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-inspection-talk',
  templateUrl: './inspection-talk.component.html',
  styleUrls: ['./inspection-talk.component.scss']
})
export class InspectionTalkComponent implements OnInit {
  queryModel;
  cols;
  loading;
  beginTime;
  endTime;
  zh;
  chooseCids =[];
  inspections;
  totalRecords;
  selectInsepections;
  constructor(
      private inspectionService:InspectionService,
      private storageService:StorageService) { }
  ngOnInit() {
    this.loading = true;
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.queryModel={
      "condition":{
        "name":"",
        "inspection_plan_cid":'',
        "create_date_start":"",
        "create_date_end":""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.cols = [
      {field: 'number', header: '序号'},
      {field: 'name', header: '名称'},
      {field: 'inspection_plan_name', header: '所属计划'},
      {field: 'create_date', header: '日期'},
    ]
    this.queryInsepection();
  }
  //搜索功能
  suggestInsepectiones(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryInsepection()
  }
//分页查询
  paginate(event) {
    // event.first = Index of the first record
    // event.rows = Number of rows to display in new page
    // event.page = Index of the new page
    // event.pageCount = Total number of pages
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryInsepection()
  }
  queryInsepection(){
    this.inspectionService.queryInspectionTalk(this.queryModel).subscribe(data=>{
      this.inspections = data.items;
      console.log( this.inspections)
      this.totalRecords = data.page.total;
      if(!this.inspections){
        this.inspections = [];
        this.selectInsepections =[]
      }
    });
  }
  //下载模板
  exportExcel(current){
    // window.location.href = `${environment.url.management}/data/file/inspection/download/report/2017-12-20巡检报表.xlsx`
    window.location.href=`${environment.url.management}/${current['download_path']}`;
  }
}
