import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionTalkComponent } from './inspection-talk.component';

describe('InspectionTalkComponent', () => {
  let component: InspectionTalkComponent;
  let fixture: ComponentFixture<InspectionTalkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionTalkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionTalkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
