import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstEchartsComponent } from './first-echarts.component';

describe('FirstEchartsComponent', () => {
  let component: FirstEchartsComponent;
  let fixture: ComponentFixture<FirstEchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstEchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
