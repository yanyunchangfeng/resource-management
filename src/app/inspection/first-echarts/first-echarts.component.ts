import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
import {IntervalObservable} from "rxjs/observable/IntervalObservable";
import {InspectionService} from "../inspection.service";

@Component({
  selector: 'app-first-echarts',
  templateUrl: './first-echarts.component.html',
  styleUrls: ['./first-echarts.component.css']
})
export class FirstEchartsComponent implements OnInit, OnDestroy {
  @ViewChild('asset') asset: ElementRef;
  echart;
  report;
  intervalObser;
  reportName = [];
  reportCount = [];
  xData = [];
  public opt: any = {
    color: ['#52BFD0','#E5403D'],
    title: {
      text:'',
      top:0,
      x:'center',
      textStyle:{//标题内容的样式
        color:'#333333',//京东红
        fontStyle:'normal',//主标题文字字体风格，默认normal，有italic(斜体),oblique(斜体)
        fontWeight:"bold",//可选normal(正常)，bold(加粗)，bolder(加粗)，lighter(变细)，100|200|300|400|500...
        // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
        fontSize:14//主题文字z字体大小，默认为18px
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // type: 'cross'
        type: 'shadow'
      }
    },
    xAxis: [{
      type: 'category',
      data: [],

    }],
    yAxis: [{
      type: 'value',
      // show:false,
      splitLine:{ // 去掉Y轴网格线
        show:false
      }
    }],
    series: [{
      name: '巡检总数',
      type: 'bar',
      barCategoryGap: '50%',
      data: [0, 0, 0, 0, 0, 0],
      itemStyle: {
        normal: {
          label: {
            show: true, //开启显示
            position: 'top', //在上方显示
            textStyle: { //数值样式
              color: 'black',
              fontSize: 16
            }
          }
        }
      }
    },
      {
        name: '漏检数',
        type: 'bar',
        barCategoryGap: '50%',
        data: [220, 182, 191, 234, 290],
        itemStyle: {
          normal: {
            label: {
              show: true, //开启显示
              position: 'top', //在上方显示
              textStyle: { //数值样式
                color: 'black',
                fontSize: 16
              }
            }
          }
        }
      },]
  };
  constructor( private inspectionService: InspectionService) { }

  ngOnInit() {
    this.echart = echarts.init(this.asset.nativeElement);
    this.inspectionService.getStatistics().subscribe(data=>{
      this.report = data;
      if(!this.report){
        this.report = [];
      }
      this.xData =  this.report.times
      for(let i = 0;i<this.report.values.length;i++){
        this.reportName.push(this.report.values[i].miss);
        this.reportCount.push(this.report.values[i].total);
      }
      this.opt.series[0]['data'] = this.reportCount;
      this.opt.series[1]['data'] = this.reportName;
      this.opt.xAxis[0]['data'] = this.xData;
      this.echart.setOption(this.opt);
      this.intervalObser = IntervalObservable.create(100).subscribe(index=>{
        this.initWidth();
      })
    })
  }
  initWidth(){
    this.echart.resize();
    this.intervalObser.unsubscribe();
  }
  ngOnDestroy(): void {
    this.echart.dispose(this.asset.nativeElement);
    this.echart = null;
  }
  @HostListener('window:resize', ['$event'])
  public onWindowResize(event): void {
    this.echart.resize()
  }

}
