import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inspection-setting',
  templateUrl: './inspection-setting.component.html',
  styleUrls: ['./inspection-setting.component.css']
})
export class InspectionSettingComponent implements OnInit {
  cols;
  constructor() { }

  ngOnInit() {
    this.cols = [
      {field: 'number', header: '序号'},
      {field: 'name', header: '名称'},
      {field: 'inspection_plan_cid', header: '所属计划'},
      {field: 'create_date', header: '日期'},
    ]
  }

}
