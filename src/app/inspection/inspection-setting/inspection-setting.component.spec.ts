import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionSettingComponent } from './inspection-setting.component';

describe('InspectionSettingComponent', () => {
  let component: InspectionSettingComponent;
  let fixture: ComponentFixture<InspectionSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
