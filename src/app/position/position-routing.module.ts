import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {EnterRegistrationComponent} from './enter-registration/enter-registration.component';
import {PositionViewComponent} from './position-view/position-view.component';

const positionRoute: Routes = [
  {path: 'enter', component: EnterRegistrationComponent},
  {path: 'view', component: PositionViewComponent},
];

@NgModule({
  imports: [RouterModule.forChild(positionRoute)],
  exports: [RouterModule]
})
export class PositionRoutingModule {

}
