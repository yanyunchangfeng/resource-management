import {NgModule} from '@angular/core';

import {EnterRegistrationComponent} from './enter-registration/enter-registration.component';
import {PositionViewComponent} from './position-view/position-view.component';
import {PositionRoutingModule} from './position-routing.module';

@NgModule({
  imports: [PositionRoutingModule],
  declarations: [
    EnterRegistrationComponent,
    PositionViewComponent
    ]
})
export class PositionModule {

}
