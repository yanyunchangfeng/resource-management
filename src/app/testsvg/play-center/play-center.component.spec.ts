import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayCenterComponent } from './play-center.component';

describe('PlayCenterComponent', () => {
  let component: PlayCenterComponent;
  let fixture: ComponentFixture<PlayCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
