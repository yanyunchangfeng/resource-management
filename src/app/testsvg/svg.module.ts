import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SvgRoutingModule } from './svg-routing.module';
import { PlayCenterComponent } from './play-center/play-center.component';
import { CapacityManagementComponent } from './capacity-management/capacity-management.component';

@NgModule({
  imports: [
    CommonModule,
    SvgRoutingModule
  ],
  declarations: [PlayCenterComponent, CapacityManagementComponent]
})
export class SvgModule { }
