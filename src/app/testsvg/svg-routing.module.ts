import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PlayCenterComponent} from "./play-center/play-center.component";

const routes: Routes = [
  {path:'play',component:PlayCenterComponent}
    // {path:'shelfapplication', component: ShelfApplicationComponent},
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SvgRoutingModule { }
