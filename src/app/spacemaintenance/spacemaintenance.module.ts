import { NgModule } from '@angular/core';
import {ShareModule} from '../shared/share.module';
import { PublicService } from '../services/public.service';
import {MalfunctionBaseInventoryComponent} from "../inventorymaintenance/malfunctioninventory/malfunction-base-inventory/malfunction-base-inventory.component";
import {MalfunctionInventoryChangeComponent} from "../inventorymaintenance/malfunctioninventory/malfunction-inventory-change/malfunction-inventory-change.component";
import {MalfunctionInventoryOverviewComponent} from "../inventorymaintenance/malfunctioninventory/malfunction-inventory-overview/malfunction-inventory-overview.component";
import {MalfunctionInventoryComponent} from "../inventorymaintenance/malfunction-inventory/malfunction-inventory.component";
import { MalfunctionAddInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-add-inventory/malfunction-add-inventory.component';
import { MalfunctionDealInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-deal-inventory/malfunction-deal-inventory.component';
import { MalfunctionDistributeInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-distribute-inventory/malfunction-distribute-inventory.component';
import { MalfunctionReceiveInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-receive-inventory/malfunction-receive-inventory.component';
import { MalfunctionOffInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-off-inventory/malfunction-off-inventory.component';
import { MalfunctionSolveInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-solve-inventory/malfunction-solve-inventory.component';
import { MalfunctionEditInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-edit-inventory/malfunction-edit-inventory.component';
import { MalfunctionSolvedInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-solved-inventory/malfunction-solved-inventory.component';
import { MalfunctionViewInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-view-inventory/malfunction-view-inventory.component';
import { MalfunctionAcceptInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-accept-inventory/malfunction-accept-inventory.component';
import { MalfunctionProcessInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-process-inventory/malfunction-process-inventory.component';
import { MalfunctionAcceptEditInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-accept-edit-inventory/malfunction-accept-edit-inventory.component';
import { MalfunctionAcceptViewInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-accept-view-inventory/malfunction-accept-view-inventory.component';
import { MalfunctionUpInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-up-inventory/malfunction-up-inventory.component';
import { MalfunctionTurnInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-turn-inventory/malfunction-turn-inventory.component';
import { MalfunctionCloseInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-close-inventory/malfunction-close-inventory.component';
import {SpacemaintenanceRoutingModule} from "./spacemaintenance-routing.module";
import {SpacemaintenanceService} from "./spacemaintenance.service";
import { SpaceManagementComponent } from './space-management/space-management.component';



@NgModule({
  imports: [
    ShareModule,
    SpacemaintenanceRoutingModule
  ],
  declarations: [
    MalfunctionBaseInventoryComponent,
    MalfunctionInventoryChangeComponent,
    MalfunctionInventoryOverviewComponent,
    MalfunctionInventoryComponent,
    MalfunctionAddInventoryComponent,
    MalfunctionDealInventoryComponent,
    MalfunctionDistributeInventoryComponent,
    MalfunctionReceiveInventoryComponent,
    MalfunctionOffInventoryComponent,
    MalfunctionSolveInventoryComponent,
    MalfunctionEditInventoryComponent,
    MalfunctionSolvedInventoryComponent,
    MalfunctionViewInventoryComponent,
    MalfunctionAcceptInventoryComponent,
    MalfunctionProcessInventoryComponent,
    MalfunctionAcceptEditInventoryComponent,
    MalfunctionAcceptViewInventoryComponent,
    MalfunctionUpInventoryComponent,
    MalfunctionTurnInventoryComponent,
    MalfunctionCloseInventoryComponent,
    SpaceManagementComponent,

  ],
  providers: [
    SpacemaintenanceService,
    PublicService
  ]
})
export class InventorymaintenanceModule { }
