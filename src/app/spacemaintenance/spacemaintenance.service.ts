import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Injectable} from "@angular/core";
import {StorageService} from "../services/storage.service";
@Injectable()
export class SpacemaintenanceService{
  ip = environment.url.management;

  constructor(private http: HttpClient,
              private storageService: StorageService) { }


// 库房管理配置树增加接口
  addMalfunctionBasalDatas(name, status, father, dep) {
    (!name) && (name = '');
    (!status) && (status = '');
    (!father) && (father = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'inventory_tree_add',
        'data': {
          'name': name,
          'remark': '',
          'del': '',
          'dep': dep,
          'father': father,
          'status': status,
        }
      }
    ).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//    编辑/冻结/启用故障管理基础数据
  editMalfunctionBasalDatas(name, status, did, dep, father) {
    (!name) && (name = '');
    (!status) && (status = '');
    (!did) && (did = '');
    (!dep) && (dep = '');
    (!father) && (father = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'inventory_tree_mod',
        'data': {
          'did': did,
          'name': name,
          'remark': '',
          'del': '',
          'dep': dep,
          'father': father,
          'status': status,
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body['errcode'] !== '00000') {
      //     return body['errmsg'];
      // }
      // return body['errcode'];
      if (res['errcode'] !== '00000'){
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//    删除库房管理基础数据
  deleteMalfunctionDatas(ids) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'inventory_tree_del',
        'ids': ids
      }
    ).map((res: Response) => {

      if (res['errcode'] !== '00000'){
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

  //  获取库房管理基础数据配置树
  getServiceRequestBasalDatas(id?, dep?) {
    (!id) && (id = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'inventory_tree_get',
        'data': {
          'dep': dep,
          'did': id
        }
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return [];
      }else {
        res['datas'].forEach(function (e) {
          e.label = e.name;
          delete e.name;
          e.leaf = false;
          e.data = e.name;
        });
      }
      return res['datas'];
    });
  }

  //
  addServiceRequestBasalDatas(name, status, father, dep) {
    (!name) && (name = '');
    (!status) && (status = '');
    (!father) && (father = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'inventory_tree_add',
        'data': {
          'name': name,
          'remark': '',
          'del': '',
          'dep': dep,
          'father': father,
          'status': status,
        }
      }
    ).map((res: Response) => {

      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

  //    编辑/冻结/启用库房管理基础数据
  editRequestBasalDatas(name, status, did,  dep, father) {
    (!name) && (name = '');
    (!status) && (status = '');
    (!did) && (did = '');
    (!dep) && (dep = '');
    (!father) && (father = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'inventory_tree_mod',
        'data': {
          'did': did,
          'name': name,
          'remark': '',
          'del': '',
          'dep': dep,
          'father': father,
          'status': status,
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body['errcode'] !== '00000') {
      //     return body['errmsg'];
      // }
      // return body['errcode'];
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

  //    删除故障管理基础数据
  deleteServiceRequestDatas(ids) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/org`,
      {
        'access_token': token,
        'type': 'servicerequest_tree_del',
        'ids': ids
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body['errcode'] !== '00000') {
      //     return body['errmsg'];
      // }
      // return body['errcode'];
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//  3.故障管理所有状态获取接口
  getAllMalfunctionStatus() {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_statuslist_get'
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return [];
      }
      return res['datas'];
    });
  }
//    流程图接口获取
  getFlowChart(status?) {
    (!status) && (status = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_workflow_get',
        'data': {
          'status': status
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body.errcode !== '00000') {
      //     return [];
      // }
      // return body['datas'];
      if ( res['errcode'] !== '00000') {
        return [];
      }
      return res['datas'];
    });
  }
//  获取、查询故障管理总览列表接口
  getAllMainfunctionList(size?, number?, sid?, submitter?, time?, status?) {
    (!number) && (number = '1');
    (!size) && (size = '10');
    (!sid) && (sid = '');
    (!submitter) && (submitter = '');
    (!time) && (time = '');
    (!status) && (status = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_get',
        'data': {
          'condition': {
            'sid': sid,
            'submitter': submitter,
            'occurrence_time': time,
            'status': status
          },
          'page': {
            'page_size': size,
            'page_number': number
          }
        }
      }
    ).map((res: Response) => {
      if (res['errcode'] !== '00000') {
        return [];
      }
      return res['data'];
    });
  }

//  获取组织树
  getDepartmentDatas(oid?, dep?) {
    (!oid) && (oid = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/org`,
      {
        'access_token': token,
        'type': 'get_suborg',
        'id': oid,
        'dep': dep
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body.errcode !== '00000') {
      //   return [];
      // }else {
      //   body['datas'].forEach(function (e) {
      //     e.label = e.name;
      //     delete e.name;
      //     e.leaf = false;
      //     e.data = e.name;
      //   });
      // }
      // return body['datas'];
      if ( res['errcode'] !== '00000') {
        return [];
      }else {
        res['datas'].forEach(function (e) {
          e.label = e.name;
          delete e.name;
          e.leaf = false;
          e.data = e.name;
        });
      }
      return res['datas'];
    });
  }

//    库房管理规格、库存位置、分类接口
  getParamOptions(){
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'inventory_config_get',
        'ids': [
          "规格",
          "库存位置",
          "分类"
        ]
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return false;
      }
      return res['data'];
    });
  }
//  故障管理优先级获取接口
  getPrority(impact, uergency) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'trouble_priority_get',
        'data': {
          'influence': impact,
          'urgency': uergency
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body.errcode !== '00000') {
      //   return false;
      // }
      // return body['data'];
      if ( res['errcode'] !== '00000') {
        return false;
      }
      return res['data'];
    });
  }

//  获取最终期限
  getDeadline(impact: string, urgency: string, prority: string) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/foundationdata`,
      {
        'access_token': token,
        'type': 'trouble_deadline_get',
        'data': {
          'influence': impact,
          'urgency': urgency,
          'priority': prority
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body.errcode !== '00000') {
      //   return false;
      // }
      // return body['data'];
      if ( res['errcode'] !== '00000') {
        return false;
      }
      return res['data'];
    });
  }

//  故障单保存
  saveTicket(ticket: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_draft',
        'data': {
          'sid': ticket['sid'],
          'name': ticket['name'],
          'fromx': ticket['fromx'],
          'status': ticket['status'],
          'submitter': ticket['submitter'],
          'submitter_pid': ticket['submitter_pid'],
          'submitter_org': ticket['submitter_org'],
          'submitter_org_oid': ticket['submitter_org_oid'],
          'submitter_phone': ticket['submitter_phone'],
          'occurrence_time': ticket['occurrence_time'],
          'deadline': ticket['deadline'],
          'influence': ticket['influence'],
          'urgency': ticket['urgency'],
          'priority': ticket['priority'],
          'level': ticket['level'],
          'bt_system': ticket['bt_system'],
          'addr': ticket['addr'],
          'service': ticket['service'],
          'devices': ticket['devices'],
          'title': ticket['title'],
          'content': ticket['content'],
          'attachments': ticket['attachments'],
          'acceptor': ticket['acceptor'],
          'acceptor_pid': ticket['acceptor_pid'],
          'acceptor_org': ticket['acceptor_org'],
          'acceptor_org_oid': ticket['acceptor_org_oid']
        }
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//  故障单提交、提交并分配
  submitTicket(ticket: Object) {
    let token = this.storageService.getToken('token');
    ( !ticket['fromx'] ) && ( ticket['fromx'] = '');
    ( !ticket['level'] ) && ( ticket['level'] = '');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_add',
        'data': {
          'sid': ticket['sid'],
          'name': ticket['name'],
          'fromx': ticket['fromx'],
          'status': ticket['status'],
          'submitter': ticket['submitter'],
          'submitter_pid': ticket['submitter_pid'],
          'submitter_org': ticket['submitter_org'],
          'submitter_org_oid': ticket['submitter_org_oid'],
          'submitter_phone': ticket['submitter_phone'],
          'occurrence_time': ticket['occurrence_time'],
          'deadline': ticket['deadline'],
          'influence': ticket['influence'],
          'urgency': ticket['urgency'],
          'priority': ticket['priority'],
          'level': ticket['level'],
          'bt_system': ticket['bt_system'],
          'addr': ticket['addr'],
          'service': ticket['service'],
          'devices': ticket['devices'],
          'title': ticket['title'],
          'content': ticket['content'],
          'attachments': ticket['attachments'],
          'acceptor': ticket['acceptor'],
          'acceptor_pid': ticket['acceptor_pid'],
          'acceptor_org': ticket['acceptor_org'] ,
          'acceptor_org_oid': ticket['acceptor_org_oid']
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body.errcode !== '00000') {
      //     return body['errmsg'];
      // }
      // return body['errcode'];
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//  故障单刪除
  deleteTrick(ids: Array<string>) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_del',
        'ids': ids
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//    故障单信息获取
  getTrick(trickSid: string) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_get_bysid',
        'id': trickSid
      }
    ).map((res: Response) => {
      // let body = res.json();
      // return body;
      return res;
    });
  }

//    故障单接受、拒绝
  recieveOrRejectTrick(obj: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_accept',
        'data': {
          'sid': obj['sid'],
          'status': obj['status']
        }
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//   故障单处理
  dealTrick(obj: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_process',
        'data': {
          'sid': obj['sid'],
          'solve_per': obj['solve_per'],
          'solve_per_pid': obj['solve_per_pid'],
          'solve_org': obj['solve_org'],
          'solve_org_oid': obj['solve_org_oid'],
          'reason': obj['reason'],
          'means': obj['means'],
          'finish_time': obj['finish_time']
        }
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//    故障单解决
  solveTrick(obj: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_finish',
        'data': {
          'sid': obj['sid'],
          'solve_per': obj['solve_per'],
          'solve_per_pid': obj['solve_per_pid'],
          'solve_org': obj['solve_org'],
          'solve_org_oid': obj['solve_org_oid'],
          'reason': obj['reason'],
          'means': obj['means'],
          'finish_time': obj['finish_time']
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body['errcode'] !== '00000') {
      //     return body['errmsg'];
      // }
      // return body['errcode'];
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//    故障单关闭
  offTrick(obj: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_close',
        'data': {
          'sid': obj['sid'],
          'close_code': obj['close_code']
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body['errcode'] !== '00000') {
      //     return body['errmsg'];
      // }
      // return body['errcode'];
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//  故障单升级
  upTrick(obj: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_upgrade',
        'data': {
          'sid': obj['sid'],
          'processor': obj['processor'],
          'processor_pid': obj['processor_pid'],
          'processor_org': obj['processor_org'],
          'processor_org_oid': obj['processor_org_oid'],
          'reassign_reason': obj['reassign_reason'],
          'upgrade_reason': obj['upgrade_reason']

        }
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }
//    故障单转派
  turnTrick(obj: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_reassign',
        'data': {
          'sid': obj['sid'],
          'processor':  obj['processor'],
          'processor_pid':  obj['processor_pid'],
          'processor_org':  obj['processor_org'],
          'processor_org_oid':  obj['processor_org_oid'],
          'reassign_reason': obj['reassign_reason']

        }
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

//    故障单解决保存
  solvedSubmit(obj: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_finish',
        'data': {
          'sid': obj['sid'],
          'solve_per': obj['solve_per'],
          'solve_org': obj['solve_org'],
          'reason': obj['reason'],
          'means': obj['means'],
          'finish_time': obj['finish_time']
        }
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if ( body['errcode'] !== '00000') {
      //     return body['errmsg'];
      // }
      // return body['errcode'];
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }

  changeTrick(obj: Object) {
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${this.ip}/workflow`,
      {
        'access_token': token,
        'type': 'trouble_assign',
        'data': {
          'sid':  obj['sid'],
          'acceptor':  obj['acceptor'],
          'acceptor_pid':  obj['acceptor_pid'],
          'acceptor_org':  obj['acceptor_org'],
          'acceptor_org_oid': obj['acceptor_org_oid']
        }
      }
    ).map((res: Response) => {
      if ( res['errcode'] !== '00000') {
        return res['errmsg'];
      }
      return res['errcode'];
    });
  }
}
