import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  // {path: '', component: MalfunctionInventoryComponent},
  // {path: 'malfunctionInventoryBasalDatas', component: MalfunctionInventoryComponent},

  // {path: 'malfunctioninventory', component: MalfunctionBaseInventoryComponent, children: [
  //   {path: '', component: MalfunctionInventoryOverviewComponent},
  //   {path: 'malfunctionAddInventory', component: MalfunctionAddInventoryComponent},
  //   {path: 'malfunctioninventoryChange', component: MalfunctionInventoryChangeComponent},
  //   {path: 'malfuntionDealInventory', component: MalfunctionDealInventoryComponent},
  //   {path: 'malfunctionDistributeInventory', component: MalfunctionDistributeInventoryComponent},
  //   {path: 'malfunctionOffInventory', component: MalfunctionOffInventoryComponent},
  //   {path: 'malfunctionReceiveInventory', component: MalfunctionReceiveInventoryComponent},
  //   {path: 'malfunctionSolveInventory', component: MalfunctionSolveInventoryComponent},
  //   {path: 'malfunctionEditInventory', component: MalfunctionEditInventoryComponent},
  //   {path: 'malfunctionSolvedInventory', component: MalfunctionSolvedInventoryComponent},
  //   {path: 'malfunctionViewInventory', component: MalfunctionViewInventoryComponent},
  //   {path: 'malfunctionAcceptInventory', component: MalfunctionAcceptInventoryComponent},
  //   {path: 'malfunctionAcceptEditInventory', component: MalfunctionAcceptEditInventoryComponent},
  //   {path: 'malfunctionAcceptViewInventory', component: MalfunctionAcceptViewInventoryComponent},
  //   {path: 'malfunctionProcessInventory', component: MalfunctionProcessInventoryComponent},
  //   {path: 'malfunctionUpInventory', component: MalfunctionUpInventoryComponent},
  //   {path: 'malfunctionTurnInventory', component: MalfunctionTurnInventoryComponent},
  //   {path: 'malfunctionCloseInventory', component: MalfunctionCloseInventoryComponent}
  // ]}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpacemaintenanceRoutingModule { }
