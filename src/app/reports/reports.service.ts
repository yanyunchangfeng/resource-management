import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {StorageService} from "../services/storage.service";
import {environment} from "../../environments/environment";

@Injectable()
export class ReportsService {
  ip = environment.url.management;

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
  ) { }

  //获取报表模板
  getReportTemplets(temp){
    let token = this.storageService.getToken("token");
    console.log(token)
    return this.http.post(
      `${this.ip}/reports`,
      {
        "access_token": token,
        "type": "report_template_get",
        "query": temp
        /*"query": {
          "sids": [],
          "sheet": [],
          "name": "",
          "enable": ""
        }*/
      }
    ).map((res: Response) => {
      if(res["errcode"] !== "00000"){
        return [];
      }
      return res["data"];
    })
  }

  //新增模板
  addTemplet(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/reports`, {
      "access_token": token,
      "type": 'report_template_add',
      "data": data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //修改模板
  updateTemplet(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/reports`, {
      "access_token": token,
      "type": "report_template_mod",
      "data":data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //模板，启用与不启用
  enableTemplet(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/reports`, {
      "access_token": token,
      "type": "report_template_enable",
      "data":data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //删除模板
  deleteTemp(sids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/reports`, {
      "access_token": token,
      "type": "report_template_del",
      "sids": sids
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['errcode'];
    })
  }

  //获取报表
  getReports(data){
    let token = this.storageService.getToken("token");
    return this.http.post(
      `${this.ip}/reports`,
      {
        "access_token": token,
        "type": "report_get",
        "data": data
      }
    ).map((res: Response) => {
      if(res["errcode"] !== "00000"){
        return [];
      }
      return res["data"];
    })
  }

  //生成报表
  reportBuild(data){
    let token = this.storageService.getToken("token");
    return this.http.post(
      `${this.ip}/reports`,
      {
        "access_token": token,
        "type": "report_add",
        "data": data
      }
    ).map((res: Response) => {
      if(res["errcode"] !== "00000"){
        return [];
      }
      return res["data"];
    })
  }

  //获取发送配置
  getSendConfigs(){
    let token = this.storageService.getToken("token");
    return this.http.post(
      `${this.ip}/reports`,
      {
        "access_token": token,
        "type": "report_send_get",
      }
    ).map((res: Response) => {
      if(res["errcode"] !== "00000"){
        return [];
      }
      for (let i = 0,len = res["data"].length;i < len;i++){
        res["data"][i].time_send = res["data"][i].send_time + res["data"][i].week_send_time;
      }
      return res["data"];
    })
  }

  //获取报表模板-发送配置
  getTemplates(query){
    let token = this.storageService.getToken("token");
    return this.http.post(
      `${this.ip}/reports`,
      {
        "access_token": token,
        "type": "report_template_get",
        "query": {
          "sids": [],
          "sheet": [],
          "name": query,
        }
      }
    ).map((res: Response) => {
      if(res["errcode"] !== "00000"){
        return [];
      }
      return res["data"];
    })
  }

  //获取人员信息-发送配置
  getPersons(query){
    let token = this.storageService.getToken("token");
    return this.http.post(
      `${this.ip}/personnel`,
      {
        "access_token": token,
        "type": "get_personnel_by_name",
        "data": {
          "name": query,
        }
      }
    ).map((res: Response) => {
      if(res["errcode"] !== "00000"){
        return [];
      }
      return res["data"];
    })
  }

  //新增发送配置
  addSendCon(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/reports`, {
      "access_token": token,
      "type": 'report_send_add',
      "data": data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //修改发送配置
  updateSendCon(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/reports`, {
      "access_token": token,
      "type": "report_send_mod",
      "data":data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //发送配置，启用与不启用
  enableSendCon(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/reports`, {
      "access_token": token,
      "type": "report_send_enable",
      "data":data
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }

  //删除发送配置
  deleteSendCon(sids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/reports`, {
      "access_token": token,
      "type": "report_send_del",
      "sids": sids
    }).map((res: Response) => {
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['errcode'];
    })
  }

}
