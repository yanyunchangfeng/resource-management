import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ReportsService} from "../reports.service";
import {FormBuilder, Validators} from "@angular/forms";
import {ConfirmationService, SelectItem} from "primeng/primeng";

@Component({
  selector: 'app-report-build',
  templateUrl: './report-build.component.html',
  styleUrls: ['./report-build.component.css']
})
export class ReportBuildComponent implements OnInit {
  display;
  title;
  @Input() currentRep;
  @Output() reportBuild = new EventEmitter();
  @Output() closeReportBuild = new EventEmitter();
  report;
  types: SelectItem[];
  ch;
  dayTime;
  maxDate: Date;
  submitRepBuild = {//报表生成参数
    "name": "",
    "type": "",
    "manual_build_time": "",
    "url": ""
  };

  constructor(
    private fb: FormBuilder,
    private reportsService: ReportsService,
    private confirmationService: ConfirmationService,
  ) {
    this.report = fb.group({
      name: ["", Validators.required],
      type: ["", Validators.required],
      dayTime: [""]
    })
  }

  ngOnInit() {
    console.log(this.currentRep);
    this.display = true;
    this.title = "报表生成";
    this.maxDate = new Date( new Date().getTime()  - 24*60*60*1000);
    this.submitRepBuild.name = this.currentRep.name;
    this.submitRepBuild.type = this.currentRep.type;
    this.submitRepBuild.url = this.currentRep.url;
    this.types = [
      {label:"日报", value: "day"},
      {label:"周报", value: "week"},
      {label:"月报", value: "month"},
      {label:"年报", value: "year"},
      {label:"其他", value: "ohers"},
    ];
    this.ch = {
      firstDayOfWeek: 0,
      dayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
      dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
      dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
      monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
      monthNamesShort: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    };
  }

  //生成报表
  formSubmit(){
    this.submitRepBuild.manual_build_time = this.report.value.dayTime;
    this.reportsService.reportBuild(this.submitRepBuild).subscribe(res => {
      console.log(res)
      window.open("." + res);
      this.reportBuild.emit(this.currentRep);
      this.closeReportBuild.emit(false);
    })
  }

  //关闭
  closeRepBuildMask(bool){
    this.closeReportBuild.emit(bool);
  }

}
