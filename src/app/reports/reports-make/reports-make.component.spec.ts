import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsMakeComponent } from './reports-make.component';

describe('ReportsMakeComponent', () => {
  let component: ReportsMakeComponent;
  let fixture: ComponentFixture<ReportsMakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsMakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsMakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
