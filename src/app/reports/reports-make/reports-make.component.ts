import { Component, OnInit } from '@angular/core';
import {ReportsService} from "../reports.service";
import {Message} from "primeng/primeng";

@Component({
  selector: 'app-reports-make',
  templateUrl: './reports-make.component.html',
  styleUrls: ['./reports-make.component.css']
})
export class ReportsMakeComponent implements OnInit {
  templets;//表格数据
  showTempletEditMask;
  temState;
  selectedCol: any = {};                     // 表格中被选中的行
  showTempDeleteMask: boolean;//是否显示删除模板弹框
  deleteSid;//删除模板的sid
  deleteTempTip;//删除模板弹框文字
  deletemsgs: Message[];//删除提示

  constructor(
    private reportsService: ReportsService,
  ) {

  }

  ngOnInit() {
    //获取表格数据，模板
    this.getTableData();
  }

  //显示编辑模板弹框
  showTempletEdit(data){
    if (!data){
      this.showTempletEditMask = true;
      this.temState = "add";
    }else {
      this.showTempletEditMask = true;
      this.temState = "edit";
      this.selectedCol = data;
    }
  }

  //新增模板成功
  addTemNode(bool){
    this.showTempletEditMask = bool;
    this.getTableData();
  }

  //修改模板成功
  updateTemNode(bool){
    this.showTempletEditMask = bool;
    this.getTableData();
  }

  //关闭编辑模板弹框
  closeTempletEdit(bool){
    this.showTempletEditMask = bool;
  }

  //模板，启用与不启用
  enableTemplet(data,flag){
    let dataParam = {
      "sid": data.sid,
      "enable": flag
    };
    this.reportsService.enableTemplet(dataParam).subscribe(res => {
      this.getTableData();
    })
  }

  //删除模板弹框
  showTempDelete(datas){
    this.deleteTempTip = '确认删除该模板？';
    this.deleteSid = [];
    this.showTempDeleteMask = true;
    this.deleteSid.push(datas.sid);
  }

  //删除模板
  deleteTemp(){
    this.reportsService.deleteTemp(this.deleteSid).subscribe(res => {
      if(res == '00000'){
        this.showSuccess();
        this.showTempDeleteMask = false;
        this.getTableData();
      }
    })
  }

  //成功提示
  showSuccess() {
    this.deletemsgs = [];
    this.deletemsgs.push({severity:'success', summary:'消息提示', detail:'删除成功'});
  }

  //获取表格数据
  getTableData(){
    let dataParam = {
      "sids": [],
      "sheet": [],
      "name": "",
      "enable": ""
    };
    this.reportsService.getReportTemplets(dataParam).subscribe(res => {
      console.log(res)
      this.templets = res;
    })
  }

}
