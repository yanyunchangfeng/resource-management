import { NgModule } from '@angular/core';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsMakeComponent } from './reports-make/reports-make.component';
import { ReportsViewComponent } from './reports-view/reports-view.component';
import { ReportsSendComponent } from './reports-send/reports-send.component';
import {ShareModule} from '../shared/share.module';
import {ReportsService} from './reports.service';
import { TempletEditComponent } from './templet-edit/templet-edit.component';
import {FileUploadModule} from 'ng2-file-upload';
import { ReportBuildComponent } from './report-build/report-build.component';
import { SendConfigComponent } from './send-config/send-config.component';

@NgModule({
  imports: [
    ShareModule,
    FileUploadModule,
    ReportsRoutingModule
  ],
  declarations: [ReportsMakeComponent, ReportsViewComponent, ReportsSendComponent, TempletEditComponent, ReportBuildComponent, SendConfigComponent],
  providers: [ReportsService]
})
export class ReportsModule { }
