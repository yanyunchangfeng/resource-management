import { Component, OnInit } from '@angular/core';
import {ReportsService} from "../reports.service";
import {Message} from "primeng/primeng";

@Component({
  selector: 'app-reports-send',
  templateUrl: './reports-send.component.html',
  styleUrls: ['./reports-send.component.css']
})
export class ReportsSendComponent implements OnInit {
  configs;//表格数据
  showConfigEditMask;
  conState;
  selectedCol: any = {};                     // 表格中被选中的行
  showConDeleteMask: boolean;//是否显示删除配置弹框
  deleteSid;//删除模板的sid
  deleteConTip;//删除模板弹框文字
  deletemsgs: Message[];

  constructor(
    private reportsService: ReportsService,
  ) { }

  ngOnInit() {
    //获取表格数据，配置
    this.getTableData();
  }

  //显示编辑发送配置弹框
  showConfigEdit(data){
    if (!data){
      this.showConfigEditMask = true;
      this.conState = "add";
    }else {
      this.showConfigEditMask = true;
      this.conState = "edit";
      this.selectedCol = data;
    }
  }

  //新增发送配置成功
  addConNode(bool){
    this.showConfigEditMask = bool;
    this.getTableData();
  }

  //修改发送配置成功
  updateConNode(bool){
    this.showConfigEditMask = bool;
    this.getTableData();
  }

  //关闭编辑发送配置弹框
  closeConfigEdit(bool){
    this.showConfigEditMask = bool;
  }

  //发送配置，启用与不启用
  enableConfig(data,flag){
    let dataParam = {
      "sid": data.sid,
      "enable": flag
    };
    this.reportsService.enableSendCon(dataParam).subscribe(res => {
      this.getTableData();
    })
  }

  //删除发送配置弹框
  showConDelete(datas){
    this.deleteConTip = '确认删除该发送配置？';
    this.deleteSid = [];
    this.showConDeleteMask = true;
    this.deleteSid.push(datas.sid);
  }

  //删除发送配置
  deleteCon(){
    this.reportsService.deleteSendCon(this.deleteSid).subscribe(res => {
      if(res == '00000'){
        this.showSuccess();
        this.showConDeleteMask = false;
        this.getTableData();
      }
    })
  }

  //成功提示
  showSuccess() {
    this.deletemsgs = [];
    this.deletemsgs.push({severity:'success', summary:'消息提示', detail:'删除成功'});
  }

  //获取表格数据
  getTableData(){
    this.reportsService.getSendConfigs().subscribe(res => {
      console.log(res)
      this.configs = res;
    })
  }

}
