import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsSendComponent } from './reports-send.component';

describe('ReportsSendComponent', () => {
  let component: ReportsSendComponent;
  let fixture: ComponentFixture<ReportsSendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsSendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsSendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
