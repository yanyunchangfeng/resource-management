import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportsMakeComponent} from './reports-make/reports-make.component';
import {ReportsViewComponent} from './reports-view/reports-view.component';
import {ReportsSendComponent} from './reports-send/reports-send.component';

const routes: Routes = [
  {path: 'make', component: ReportsMakeComponent},
  {path: 'view', component: ReportsViewComponent},
  {path: 'send', component: ReportsSendComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
