import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendConfigComponent } from './send-config.component';

describe('SendConfigComponent', () => {
  let component: SendConfigComponent;
  let fixture: ComponentFixture<SendConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
