import { Component, OnInit } from '@angular/core';
import {ReportsService} from "../reports.service";

@Component({
  selector: 'app-reports-view',
  templateUrl: './reports-view.component.html',
  styleUrls: ['./reports-view.component.css']
})
export class ReportsViewComponent implements OnInit {
  reports;
  reportBuildMask;
  selectRep;

  constructor(
    private reportsService: ReportsService,
  ) { }

  ngOnInit() {
    //获取表格数据，报表
    this.getTableData();
  }

  //查看，报表生成
  showReportBuild(data){
    this.reportBuildMask = true;
    this.selectRep = data;
  }

  //关闭，报表生成
  closeReportBuild(){
    this.reportBuildMask = false;
  }

  //报表生成成功
  reportBuild(bool){
    this.reportBuildMask = bool;
    this.getTableData();
  }

  //下载
  uploadReport(data){
    console.log(data)
    window.open("." + data.url)
  }

  //获取表格数据
  getTableData(){
    this.reportsService.getReports({}).subscribe(res => {
      console.log(res)
      this.reports = res;
    })
  }

}
