import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempletEditComponent } from './templet-edit.component';

describe('TempletEditComponent', () => {
  let component: TempletEditComponent;
  let fixture: ComponentFixture<TempletEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempletEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempletEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
