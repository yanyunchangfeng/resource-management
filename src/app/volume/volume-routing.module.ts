import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {CabinetManagementComponent} from './cabinet-management/cabinet-management.component';
import {SpaceManagementComponent} from './space-management/space-management.component';
import {VolumeViewComponent} from './volume-view/volume-view.component';

const route: Routes = [
  {path: 'space', component: SpaceManagementComponent},
  {path: 'volume', component: VolumeViewComponent},
  {path: 'cabinet', component: CabinetManagementComponent},

];
@NgModule({
  imports: [RouterModule.forChild(route),
  ],
  exports: [RouterModule]
})
export class VolumeRoutingModule {

}
