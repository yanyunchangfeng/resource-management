import {NgModule} from '@angular/core';
import {ShareModule} from '../shared/share.module';
import {VolumeViewComponent} from './volume-view/volume-view.component';
import {SpaceManagementComponent} from './space-management/space-management.component';
import {CabinetManagementComponent} from './cabinet-management/cabinet-management.component';
import {VolumeRoutingModule} from './volume-routing.module';

@NgModule({
  declarations: [
    VolumeViewComponent,
    SpaceManagementComponent,
    CabinetManagementComponent
  ],
  imports: [
    ShareModule,
    VolumeRoutingModule,
  ]
})
export class VolumeModule {
}
