import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrUpdateEquipmentComponent } from './add-or-update-equipment.component';

describe('AddOrUpdateEquipmentComponent', () => {
  let component: AddOrUpdateEquipmentComponent;
  let fixture: ComponentFixture<AddOrUpdateEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrUpdateEquipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrUpdateEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
