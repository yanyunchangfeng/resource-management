export class SelectEquipObjModel {
    asset_no: string;
    name: string;
    status: string;
    on_line_date_start: string;
    on_line_date_end: string;
    room: string;
    filter_asset_nos: any[];
    page_size: string;
    page_number: string;

    ano: string;
    filter_anos: Array<string>;

    constructor(obj?) {
        this.asset_no = obj && obj['asset_no'] || '';
        this.name = obj && obj['name'] || '';
        this.status = obj && obj['status'] || '';
        this.on_line_date_start = obj && obj['on_line_date_start'] || '';
        this.on_line_date_end = obj && obj['on_line_date_end'] || '';
        this.room = obj && obj['room'] || '';
        this.filter_asset_nos = obj && obj['filter_asset_nos'] || [];
        this.page_size = obj && obj['page_size'] || '10';
        this.page_number = obj && obj['page_number'] || '1';
        this.ano = obj && obj['ano'] || '';
        this.filter_anos = obj && obj['filter_anos'] || [];
    }
}
