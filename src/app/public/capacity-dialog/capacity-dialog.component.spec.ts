import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityDialogComponent } from './capacity-dialog.component';

describe('CapacityDialogComponent', () => {
  let component: CapacityDialogComponent;
  let fixture: ComponentFixture<CapacityDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
