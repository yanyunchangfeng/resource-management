import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TreeNode} from 'primeng/primeng';
import {PublicService} from '../../services/public.service';
import {Message} from 'primeng/api';
@Component({
  selector: 'app-capacity-dialog',
  templateUrl: './capacity-dialog.component.html',
  styleUrls: ['./capacity-dialog.component.css']
})
export class CapacityDialogComponent implements OnInit {
    @Input() display: boolean;
    @Output() dispalyEmitter: EventEmitter<any> = new EventEmitter();
    @Output() selectedEmitter: EventEmitter<any> = new EventEmitter();
    filesTree4: TreeNode[];
    selected: Array<any> = [];
    msgs: Message[] = [];

    constructor(private publicService: PublicService) { }

    ngOnInit() {
        this.initTreeDatas();
    }
    initTreeDatas = (): void => {
        this.publicService.getCapBasalDatas('', 'F').subscribe(res => {
            this.filesTree4 = res;
            if (res) {
                this.expandAll(res);
            }
        });
    }
    expandAll = (treeDatas): void =>  {
        treeDatas.forEach( node => {
            this.expandRecursive(node, true);
        } );
    }
    private expandRecursive(node: TreeNode, isExpand: boolean): void{
        node.expanded = isExpand;
        if (node.children){
            node.children.forEach( childNode => {
                this.expandRecursive(childNode, isExpand);
            } );
        }
    }
    onNodeSelect = (event) => {
        this.msgs = [];
        (!(event.node.sp_type === 'E')) && (this.showError('只能选择房间！', 'warn'));
    };
    showError = (msg, severity = 'success') => {
        this.msgs.push({severity: severity, summary: '提示消息', detail: msg});
    };
    onHideCapcity = () => {
        this.dispalyEmitter.emit(false);
    };
    closeTreeDialog = () => {
        this.onHideCapcity();
    }
    selectedTreeDialog = () => {
        this.onHideCapcity();
        this.selectedEmitter.emit(this.selected);
    }
     clearSelected = () => {
        this.selected = [];
    }
}
