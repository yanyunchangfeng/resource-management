import { NgModule } from '@angular/core';
import {PersonelDialogComponent} from './personel-dialog/personel-dialog.component';
import {ShareModule} from '../shared/share.module';
import {PublicService} from '../services/public.service';
import {UploadFileViewComponent} from './upload-file-view/upload-file-view.component';
import { EditorComponent } from './editor/editor.component';
import { EditorViewComponent } from './editor-view/editor-view.component';
import { TagCloudComponent } from './tag-cloud/tag-cloud.component';
import {TagCloudModule} from 'angular-tag-cloud-module';
import { FieldErrorDisplayComponent } from './field-error-display/field-error-display.component';
import { CapacityDialogComponent } from './capacity-dialog/capacity-dialog.component';
import { SelectEquipmentsComponent } from './select-equipments/select-equipments.component';
import { AddOrUpdateEquipmentComponent } from './add-or-update-equipment/add-or-update-equipment.component';
import {EquipmentService} from '../equipment/equipment.service';
import { ViewEquipmentComponent } from './view-equipment/view-equipment.component';

@NgModule({
    imports: [
        ShareModule,
        TagCloudModule
    ],
    declarations: [
        PersonelDialogComponent,
        UploadFileViewComponent,
        EditorComponent,
        EditorViewComponent,
        TagCloudComponent,
        FieldErrorDisplayComponent,
        CapacityDialogComponent,
        SelectEquipmentsComponent,
        AddOrUpdateEquipmentComponent,
        ViewEquipmentComponent
    ],
    exports: [
        PersonelDialogComponent,
        UploadFileViewComponent,
        EditorComponent,
        EditorViewComponent,
        TagCloudComponent,
        FieldErrorDisplayComponent,
        CapacityDialogComponent,
        SelectEquipmentsComponent,
        AddOrUpdateEquipmentComponent,
        ViewEquipmentComponent
    ],
    providers: [
        PublicService,
        EquipmentService
    ]
})
export class PublicModule { }
