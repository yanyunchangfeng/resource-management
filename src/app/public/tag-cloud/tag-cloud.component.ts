import {Component, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {CloudData, CloudOptions} from 'angular-tag-cloud-module';
import {Observable} from 'rxjs/Observable';
import {EventBusService} from '../../services/event-bus.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-tag-cloud',
  templateUrl: './tag-cloud.component.html',
  styleUrls: ['./tag-cloud.component.scss']
})
export class TagCloudComponent  implements OnInit{

    @Input() textDatas: any = [];
    @Input() whichComponent: string = '';
    options: CloudOptions = {
        width : 0.85,
        height : 330,
        overflow: false,
        zoomOnHover: {
            scale: 1.3,
            transitionTime: 1.2
        }
    };

    data1: CloudData[] = this._randomData();
    dataRotate: CloudData[] = this._randomData(20, true);

    constructor(private eventBusService: EventBusService,
                private router: Router,
                private activatedRouter: ActivatedRoute) {
    }
    ngOnInit(): void {
        this.reactHeigh();
        this.windowOnresize();
        // this.eventBusService.tagcloud.subscribe(res => {
        //     let array: Array<any> = [];
        //     if(res) {
        //         res.forEach(function (e) {
        //             array.push(e['text']);
        //         });
        //         this.textDatas = array;
        //         // this.windowOnresize();
        //     }
        // });
    }
    windowOnresize() {
        window.onresize = () => {
            // console.log(window.innerWidth);
            if (this.textDatas && this.textDatas.hasOwnProperty('length')) {
                this._randomData(this.textDatas.length);
            }
            this.reactHeigh();
        };
    }
    reactHeigh() {
        (window.innerWidth === 1920) && (this.options.height = 330);
        (window.innerWidth === 1440) && (this.options.height = 319);
        if (window.innerWidth === 1366) {
            this.options.height = 230;
            this.options.width = 0.8;
        };
    }
    newDateFromObservable() {
        const changedData$: Observable<CloudData[]> = Observable.of(this._randomData());
        // changedData$.subscribe(res => this.data2 = res);
    }

    log(eventType: string, e?: CloudData) {
        // console.log(eventType, e);
        this.data1 = this._randomData();
    }
    jumper = (eventType: string, e?: CloudData) => {
        // console.log(eventType, e);
        if (this.whichComponent === 'knowledage') {
            this.router.navigate(['../klgbasic/klgmanageoverview'], {queryParams: {search: e.text}, relativeTo: this.activatedRouter});
        }

    }
    dataChanges(eventType: string, e?: CloudData) {
        // console.log(eventType, e);
    }
    private _randomData(cnt?: number, rotate?: boolean): CloudData[] {
        if (this.textDatas) {
            if (!cnt) { cnt = this.textDatas.length; }

            const cd: CloudData[] = [];
            // const textDatas: Array<string> = ['漏水绳故障', 'UPS故障', '服务请求流程', '巡检终止', '工单管理导出', '漏水绳故障'];
            // const textDatas: Array<string> = this.textDatas;


            for (let i = 0; i < cnt; i++) {
                let link: string;
                let color: string;
                let external: boolean;
                let weight = 5;
                let text = '';
                let r = 0;

                // randomly set link attribute and external
                if (Math.random() >= 0.5) {
                    link = 'http://example.org';
                    if (Math.random() >= 0.5) { external = true; }
                }

                // randomly set color attribute
                if (Math.random() >= 0.5) {
                    color = '#' + Math.floor(Math.random() * 16777215).toString(16);
                }

                // randomly rotate some elements (less probability)
                if ((Math.random() >= 0.7) && rotate) {
                    const plusMinus = Math.random() >= 0.5 ? '' : '-';
                    r = Math.floor(Math.random() * Number(`${plusMinus}20`) + 1);
                }

                // set random weight
                weight = Math.floor((Math.random() * 5) + 1);

                // text = `哈哈-${weight}`;
                text = this.textDatas[i];
                // if (color) { text += '-color'; }
                // if (link) { text += '-link'; }
                // if (external) { text += '-external'; }

                const el: CloudData = {
                    text: this.textDatas,
                    weight: weight,
                    color: color,
                    link: null,
                    external: external,
                    rotate: r,
                };

                cd.push(el);
            }

            return this.textDatas;
        }
        return [];
    }
}
