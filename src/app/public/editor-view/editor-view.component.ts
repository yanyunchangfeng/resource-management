import {Component, ElementRef, EventEmitter, OnInit, Output, Renderer} from '@angular/core';
// import * as wangEditor from '../../../../node_modules/wangeditor/release/wangEditor.js';
declare var E: any;
@Component({
  selector: 'app-editor-view',
  templateUrl: './editor-view.component.html',
  styleUrls: ['./editor-view.component.css']
})
export class EditorViewComponent {
    private editor: any;
    @Output() onPostData = new EventEmitter();

    constructor(private el: ElementRef, private renderer: Renderer) { }

    ngAfterViewInit() {
        let E = window['wangEditor'];
        let editordom = this.el.nativeElement.querySelector('#editorElem');
        this.editor = new E(editordom);
        this.editor.customConfig.uploadImgShowBase64 = true;
        this.editor.customConfig.menus = [];
        this.editor.create();
    }

    clickHandle(): any {
        let data = this.editor.txt.html();
        return data;
    }
    setContent = (content) => {
        // console.log(content, content);
        this.editor.txt.html(content);
    }
}
