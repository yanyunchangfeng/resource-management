import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {environment} from '../../../environments/environment';


@Component({
  selector: 'app-view-equipment',
  templateUrl: './view-equipment.component.html',
  styleUrls: ['./view-equipment.component.scss']
})
export class ViewEquipmentComponent  implements OnInit {
    @Input() currentAsset;
    @Output() closeDetailMask = new EventEmitter();
    display;
    windowSize;
    width;
    title = '查看详情';
    constructor() { }

    ngOnInit() {
        this.display = true;
        this.windowSize = window.innerWidth;
        if(this.windowSize<1024){
            this.width = this.windowSize*0.9;
        }else if(this.windowSize>1500){
            this.width = this.windowSize*0.7;
        }else{
            this.width = this.windowSize*0.8;
        }
    }
    closeViewDetailMask(bool){
        this.closeDetailMask.emit(bool);
    }
}
