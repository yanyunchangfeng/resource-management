import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TreeNode} from 'primeng/primeng';
import {PublicService} from '../../services/public.service';

@Component({
  selector: 'app-personel-dialog',
  templateUrl: './personel-dialog.component.html',
  styleUrls: ['./personel-dialog.component.scss']
})
export class PersonelDialogComponent implements OnInit {

    @Output() dataEmitter = new EventEmitter();
    @Output() displayEmitter = new EventEmitter();
    display: boolean;
    orgs: TreeNode[];                      // 组织树的数据
    selected: TreeNode;                    // 被选中的节点
    tableDatas: any;                       // 表格数据

    eventData: any;

    constructor(private publicService: PublicService) { }

    ngOnInit() {
        this.display = true;
        // 查询组织树
        this.queryOrgTree('');
        // 查询人员表格数据
        this.queryPersonList('');
    }
    // 组织树懒加载
    nodeExpand(event) {
        if (event.node) {
            this.publicService.getOrgTree(event.node.oid).subscribe(data => {
                console.log(data);
                event.node.children = data;
            });
        }
    }
    // 组织树选中
    NodeSelect(event) {
        if (parseInt(event.node.dep) >= 1) {
            this.queryPersonList(event.node.oid);
        }
    }
    // 查询组织树数据
    queryOrgTree(oid) {
        this.publicService.getOrgTree(oid).subscribe(data => {
            this.orgs = data;
        });
    }
    // 查询人员表格数据
    queryPersonList(oid) {
        this.publicService.getPersonList(oid).subscribe(data => {
            this.tableDatas = data;
        });
    }

    handleRowSelect(event) {
        this.eventData = event.data;

    }

    sure = ()  => {
        this.dataEmitter.emit(this.eventData);
        this.displayEmitter.emit(false);
    };

    cancel = () =>  {
        this.displayEmitter.emit(false);
    };
}
