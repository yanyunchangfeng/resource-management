import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionTurnComponent } from './malfunction-turn.component';

describe('MalfunctionTurnComponent', () => {
  let component: MalfunctionTurnComponent;
  let fixture: ComponentFixture<MalfunctionTurnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionTurnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionTurnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
