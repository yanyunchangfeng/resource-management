import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionDealComponent } from './malfunction-deal.component';

describe('MalfunctionDealComponent', () => {
  let component: MalfunctionDealComponent;
  let fixture: ComponentFixture<MalfunctionDealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionDealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionDealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
