import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionChangeComponent } from './malfunction-change.component';

describe('MalfunctionChangeComponent', () => {
  let component: MalfunctionChangeComponent;
  let fixture: ComponentFixture<MalfunctionChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
