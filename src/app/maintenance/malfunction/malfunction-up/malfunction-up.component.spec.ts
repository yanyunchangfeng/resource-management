import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionUpComponent } from './malfunction-up.component';

describe('MalfunctionUpComponent', () => {
  let component: MalfunctionUpComponent;
  let fixture: ComponentFixture<MalfunctionUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
