import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionCloseViewComponent } from './malfunction-close-view.component';

describe('MalfunctionCloseViewComponent', () => {
  let component: MalfunctionCloseViewComponent;
  let fixture: ComponentFixture<MalfunctionCloseViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionCloseViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionCloseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
