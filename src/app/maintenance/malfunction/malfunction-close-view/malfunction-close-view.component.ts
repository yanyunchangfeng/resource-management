import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MaintenanceService} from '../../maintenance.service';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-malfunction-close-view',
  templateUrl: './malfunction-close-view.component.html',
  styleUrls: ['./malfunction-close-view.component.scss']
})
export class MalfunctionCloseViewComponent implements OnInit {

    sid: string;
    status: string;
    constructor( private activatedRouter: ActivatedRoute,
                 private router: Router,
                 private maintenanceService: MaintenanceService,
                 private eventBusService: EventBusService) { }
    ngOnInit() {
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart( this.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
}
