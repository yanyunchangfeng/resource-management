import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionSolvedComponent } from './malfunction-solved.component';

describe('MalfunctionSolvedComponent', () => {
  let component: MalfunctionSolvedComponent;
  let fixture: ComponentFixture<MalfunctionSolvedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionSolvedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionSolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
