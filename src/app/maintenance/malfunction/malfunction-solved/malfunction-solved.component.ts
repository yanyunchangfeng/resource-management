import { Component, OnInit } from '@angular/core';
import {Message, TreeNode} from 'primeng/primeng';
import {MaintenanceService} from '../../maintenance.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PublicService} from '../../../services/public.service';
import {PUblicMethod} from '../../../services/PUblicMethod';
import {EventBusService} from '../../../services/event-bus.service';
import {MatenanceObjModel} from '../../matenanceObj.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-malfunction-solved',
  templateUrl: './malfunction-solved.component.html',
  styleUrls: ['./malfunction-solved.component.scss']
})
export class MalfunctionSolvedComponent implements OnInit {

    formObj: MatenanceObjModel;
    options: Object = {
        '来源': [],
        '影响度': [],
        '紧急度': [],
        '故障级别': [],
        '所属系统': [],
        'departments': [],
    };
    defaultOption: Object = {
        'solve_per_name': null
    };
    display: boolean = false;
    filesTree4: TreeNode[] = [];
    selected: Array<any> = [];
    zh: any;
    message: Message[] = [];               // 交互提示弹出框
    myForm: FormGroup;


    constructor(private maintenanceService: MaintenanceService,
                private router: Router,
                private fb: FormBuilder,
                private publicService: PublicService,
                private activatedRouter: ActivatedRoute,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.zh = new PUblicMethod().initZh();
        this.formObj = new MatenanceObjModel();
        this.formObj.accept_time = PUblicMethod.formateEnrtyTime(this.activatedRouter.snapshot.paramMap.get('time'));
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.getTrickMessage(this.formObj.sid);
        this.getDepPerson();
        this.initForm();
        this.maintenanceService.getFlowChart( this.formObj.status ).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
    }
    getTrickMessage(sid) {
        this.maintenanceService.getTrick(sid).subscribe(res => {
            this.formObj.acceptor = res['acceptor'];
            this.formObj.acceptor_org = res['acceptor_org'];
            this.formObj.solve_per_name = res['acceptor'];
            this.formObj.solve_per = res['acceptor_pid'];
            this.formObj.solve_org_name = res['acceptor_org'];
            this.formObj.solve_org = res['acceptor_org_oid'];
        })
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
    getDepPerson(oid?) {
        this.publicService.getApprovers(oid).subscribe(res => {
            if (!res) {
                this.options['departments'] = [];
            }else {
                this.options['departments'] = res;
                this.formObj.solve_per = res[0]['pid'];
                this.formObj.solve_per_name = res[0]['name'];
            }
        });
    }
    initForm() {
        this.myForm = this.fb.group({
            'means': '',
            'reason': '',
            'solveOrgName': '',
            'finishTime': ''
        });
    }
    showTreeDialog( opType ) {
        switch (opType) {
            case 'department':
                this.display = true;
                this.publicService.getDepartmentDatas().subscribe(res => {
                    this.filesTree4 = res;
                });
        }

    }
    clearTreeDialog(opType) {
        switch (opType) {
            case 'submitter':
                this.formObj.submitter = '';
                this.formObj.submitter_pid = '';
                this.formObj.submitter_org_oid = '';
                this.formObj.submitter_org = '';
                this.formObj.submitter_phone = '';
                break;
            case 'department':
                this.formObj.solve_per = '';
                this.formObj.solve_per_name = '';
                this.formObj.solve_org = '';
                this.formObj.solve_org_name = '';
                break;
        }

    }
    closeTreeDialog() {
        this.display = false;
        this.formObj.solve_org_name = this.selected['label'];
        this.formObj.solve_org = this.selected['oid'];
        this.getDepPerson(this.selected['oid']);
    }
    nodeExpand(event) {
        if (event.node) {
            this.publicService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(res => {
                event.node.children  = res;
            });
        }
    }
    get means() {
        return this.myForm.controls['means'].untouched && this.myForm.controls['means'].hasError('required');
    }
    get reason() {
        return this.myForm.controls['reason'].untouched && this.myForm.controls['reason'].hasError('required');
    }
    get solveOrgName() {
        return this.myForm.controls['solveOrgName'].untouched && this.myForm.controls['solveOrgName'].hasError('required');
    }
    get finishTime() {
        return this.myForm.controls['finishTime'].untouched && this.myForm.controls['finishTime'].hasError('required');
    }
    onSelectedDepartment(event) {
        let msg = this.formObj.acceptor;
        this.formObj.solve_per_name = msg['name'];
        this.formObj.solve_per = msg['pid'];
    }
    save() {
        this.addSelectedName();
        this.filterOptionName();
        this.formObj.status = '';
        this.submitPost(this.formObj);
    }
    addSelectedName() {
        (!this.formObj.solve_per_name) && (this.formObj.solve_per_name = this.defaultOption['solve_per_name']);
    }
    filterOptionName() {
        if (this.formObj.solve_per_name['name']) {
            this.formObj.solve_per_name = this.formObj.solve_per_name['name'];
        }
    }
    showError() {
        this.message = [];
        this.message.push({severity: 'error', summary: '错误消息提示', detail: '请填写完整表单'});
    }
    submitPost(paper) {
        this.maintenanceService.solveTrick( paper ).subscribe(res => {
            if ( res === '00000' ) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '操作成功'});
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);

            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `操作失败${res}`});
            }
        });
    }
    setValidators() {
        this.myForm.controls['solveOrgName'].setValidators([Validators.required]);
        this.myForm.controls['reason'].setValidators([Validators.required]);
        this.myForm.controls['means'].setValidators([Validators.required]);
        this.myForm.controls['finishTime'].setValidators([Validators.required]);
        this.myForm.controls['solveOrgName'].updateValueAndValidity();
        this.myForm.controls['reason'].updateValueAndValidity();
        this.myForm.controls['means'].updateValueAndValidity();
        this.myForm.controls['finishTime'].updateValueAndValidity();
    }
    solved() {
        if ( !this.formObj.solve_org_name
            || !this.formObj.reason
            || !this.formObj.means
        ) {
            this.setValidators();
            this.showError();
        }else {
            this.formObj.status = '已解决';
            this.submitPost(this.formObj);
        }
    }

}
