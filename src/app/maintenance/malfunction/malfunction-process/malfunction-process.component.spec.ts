import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionProcessComponent } from './malfunction-process.component';

describe('MalfunctionProcessComponent', () => {
  let component: MalfunctionProcessComponent;
  let fixture: ComponentFixture<MalfunctionProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
