import { Component, OnInit } from '@angular/core';
import {Message, TreeNode} from 'primeng/primeng';
import {MaintenanceService} from '../../maintenance.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../../services/event-bus.service';
import {MatenanceObjModel} from "../../matenanceObj.model";

@Component({
  selector: 'app-malfunction-process',
  templateUrl: './malfunction-process.component.html',
  styleUrls: ['./malfunction-process.component.scss']
})
export class MalfunctionProcessComponent implements OnInit {


    formObj: MatenanceObjModel;
    message: Message[] = [];               // 交互提示弹出框

    constructor(private maintenanceService: MaintenanceService,
                private router: Router,
                private activatedRouter: ActivatedRoute,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.formObj = new MatenanceObjModel();
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
    deal() {
        this.formObj.status = '处理中';
        this.maintenanceService.dealTrick(this.formObj).subscribe(res => {
            if (res === '00000') {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '处理成功'});
                window.setTimeout(() => {
                    this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionSolved', {id: this.formObj.sid, status: this.formObj.status}]);
                }, 1100);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: '处理失败' + res});
            }
        });

    }

    up() {
        this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionUp', {id: this.formObj.sid, status: this.formObj.status}]);
    }
    turn() {
        this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionTurn', {id: this.formObj.sid, status: this.formObj.status}]);
    }

}
