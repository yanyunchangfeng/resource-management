import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionSolveComponent } from './malfunction-solve.component';

describe('MalfunctionSolveComponent', () => {
  let component: MalfunctionSolveComponent;
  let fixture: ComponentFixture<MalfunctionSolveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionSolveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionSolveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
