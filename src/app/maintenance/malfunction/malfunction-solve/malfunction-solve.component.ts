import { Component, OnInit } from '@angular/core';
import {EventBusService} from "../../../services/event-bus.service";
import {MaintenanceService} from "../../maintenance.service";

@Component({
  selector: 'app-malfunction-solve',
  templateUrl: './malfunction-solve.component.html',
  styleUrls: ['./malfunction-solve.component.scss']
})
export class MalfunctionSolveComponent implements OnInit {

    searchType = '处理中';            // 搜索类型
    eventDatas: Object;                   // 子组件返回的数据

    constructor(private eventBusService: EventBusService,
                private maintenanceServicen: MaintenanceService) { }

    ngOnInit() {
        this.maintenanceServicen.getFlowChart().subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
    }

    searchInfoEmitter(event) {
        console.log(event);
        this.eventDatas = event;
    }

}
