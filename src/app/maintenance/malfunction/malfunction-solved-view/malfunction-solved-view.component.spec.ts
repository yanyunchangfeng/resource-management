import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionSolvedViewComponent } from './malfunction-solved-view.component';

describe('MalfunctionSolvedViewComponent', () => {
  let component: MalfunctionSolvedViewComponent;
  let fixture: ComponentFixture<MalfunctionSolvedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionSolvedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionSolvedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
