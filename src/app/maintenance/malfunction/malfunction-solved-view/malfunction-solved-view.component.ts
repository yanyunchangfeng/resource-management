import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-malfunction-solved-view',
  templateUrl: './malfunction-solved-view.component.html',
  styleUrls: ['./malfunction-solved-view.component.scss']
})
export class MalfunctionSolvedViewComponent implements OnInit {


    constructor(  private router: Router,
                private activatedRouter: ActivatedRoute,
                private eventBusService: EventBusService) { }
    ngOnInit() {
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
}
