import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionCloseComponent } from './malfunction-close.component';

describe('MalfunctionCloseComponent', () => {
  let component: MalfunctionCloseComponent;
  let fixture: ComponentFixture<MalfunctionCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
