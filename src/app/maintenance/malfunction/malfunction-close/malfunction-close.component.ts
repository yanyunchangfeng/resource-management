import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/primeng';
import {MaintenanceService} from '../../maintenance.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PUblicMethod} from '../../../services/PUblicMethod';
import {EventBusService} from '../../../services/event-bus.service';
import {MatenanceObjModel} from '../../matenanceObj.model';

@Component({
  selector: 'app-malfunction-close',
  templateUrl: './malfunction-close.component.html',
  styleUrls: ['./malfunction-close.component.scss']
})
export class MalfunctionCloseComponent implements OnInit {
    options: Object = {
        'close_code': [],
    };
    defaultOption: Object = {
        'close_code': null
    };
    message: Message[] = [];               // 交互提示弹出框
    formObj: MatenanceObjModel;

    constructor(private maintenanceService: MaintenanceService,
                private router: Router,
                private activatedRouter: ActivatedRoute,
                private eventBusService: EventBusService) { }
    ngOnInit() {
        this.formObj = new MatenanceObjModel();
        this.initCloseCode();
        this.formObj.close_time = PUblicMethod.formateEnrtyTime(new Date());
        this.formObj.close_code = this.options['close_code'][0]['name'];
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
    }
    initCloseCode() {
        this.options['close_code'] = [
            {name: '成功解决', code: 'NY'},
            {name: '临时解决', code: 'RM'},
            {name: '不成功', code: 'LDN'},
            {name: '撤销', code: 'IST'}
        ];
    }
    addSelectedName() {
        (!this.formObj.fromx) && (this.formObj.fromx = this.defaultOption['fromx']);
        (!this.formObj.level) && (this.formObj.level = this.defaultOption['level']);
        (!this.formObj.bt_system) && (this.formObj.bt_system = this.defaultOption['bt_system']);
        (!this.formObj.influence) && (this.formObj.influence = this.defaultOption['influence']);
        (!this.formObj.urgency) && (this.formObj.urgency = this.defaultOption['urgency']);
        (!this.formObj.acceptor) && (this.formObj.acceptor = this.defaultOption['acceptor']);
    }
    filterOptionName() {
        if (this.formObj.close_code['name']) {
            this.formObj.close_code = this.formObj.close_code['name'];
        }
    }
    close() {
        this.addSelectedName();
        this.filterOptionName();
        if ( !this.formObj.close_code) {
            this.showError();
        }else {
            this.submitPost(this.formObj);
        }
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
    showError() {
        this.message = [];
        this.message.push({severity: 'error', summary: '错误消息提示', detail: '请填写完整表单'});
    }
    submitPost(paper) {
        this.maintenanceService.offTrick( paper ).subscribe(res => {
            if ( res === '00000' ) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '操作成功'});
                window.setTimeout(() => {
                  this.goBack();
                }, 1100);

            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `操作失败${res}`});
            }
        });
    }

}
