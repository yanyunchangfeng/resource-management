import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionAcceptComponent } from './malfunction-accept.component';

describe('MalfunctionAcceptComponent', () => {
  let component: MalfunctionAcceptComponent;
  let fixture: ComponentFixture<MalfunctionAcceptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionAcceptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionAcceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
