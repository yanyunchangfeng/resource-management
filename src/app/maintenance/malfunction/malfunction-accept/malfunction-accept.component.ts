import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/primeng';
import {MaintenanceService} from '../../maintenance.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../../services/event-bus.service';
import {MatenanceObjModel} from '../../matenanceObj.model';

@Component({
  selector: 'app-malfunction-accept',
  templateUrl: './malfunction-accept.component.html',
  styleUrls: ['./malfunction-accept.component.scss']
})
export class MalfunctionAcceptComponent implements OnInit {

    message: Message[] = [];               // 交互提示弹出框

    formObj: MatenanceObjModel;

    constructor(private maintenanceService: MaintenanceService,
                private router: Router,
                private activatedRouter: ActivatedRoute,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.formObj = new MatenanceObjModel();
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');



        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });

    }
    accept() {
       this.formObj.status = '待处理';
        this.maintenanceService.recieveOrRejectTrick(this.formObj).subscribe(res => {
            if ( res === '00000') {
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionProcess', {id: this.formObj.sid, status: this.formObj.status}]);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `故障单接受失败${res}`});
            }
        });
    }

    reject() {
        this.formObj.status = '待分配';
        console.dirxml(this.formObj);
        this.submitPost(this.formObj);
    }
    submitPost(paper) {
        this.maintenanceService.recieveOrRejectTrick( paper ).subscribe(res => {
            if ( res === '00000' ) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '操作成功'});
                window.setTimeout(() => {
                  this.goBack();
                }, 1100);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `操作失败${res}`});
            }
        });
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
}
