import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionClosedViewComponent } from './malfunction-closed-view.component';

describe('MalfunctionClosedViewComponent', () => {
  let component: MalfunctionClosedViewComponent;
  let fixture: ComponentFixture<MalfunctionClosedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionClosedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionClosedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
