import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-malfunction-closed-view',
  templateUrl: './malfunction-closed-view.component.html',
  styleUrls: ['./malfunction-closed-view.component.scss']
})
export class MalfunctionClosedViewComponent implements OnInit {


    constructor(private activatedRouter: ActivatedRoute,
                 private router: Router,
                 private eventBusService: EventBusService) { }

    ngOnInit() {
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }

}
