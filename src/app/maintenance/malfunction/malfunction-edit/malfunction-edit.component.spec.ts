import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionEditComponent } from './malfunction-edit.component';

describe('MalfunctionEditComponent', () => {
  let component: MalfunctionEditComponent;
  let fixture: ComponentFixture<MalfunctionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
