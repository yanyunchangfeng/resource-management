import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionOverviewComponent } from './malfunction-overview.component';

describe('MalfunctionOverviewComponent', () => {
  let component: MalfunctionOverviewComponent;
  let fixture: ComponentFixture<MalfunctionOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
