import {Component,  OnInit} from '@angular/core';
import {EventBusService} from '../../../services/event-bus.service';
import {MaintenanceService} from '../../maintenance.service';

@Component({
  selector: 'app-malfunction-overview',
  templateUrl: './malfunction-overview.component.html',
  styleUrls: ['./malfunction-overview.component.scss']
})
export class MalfunctionOverviewComponent implements OnInit {


    searchType = 'trouble_get';            // 搜索类型
    eventDatas: Object;                    // 子组件返回的数据

    constructor( private eventBusService: EventBusService,
                 private maltenanceService: MaintenanceService) { }
    ngOnInit() {
        this.maltenanceService.getFlowChart().subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
    }
    searchInfoEmitter(event) {
        this.eventDatas = event;
    }
}
