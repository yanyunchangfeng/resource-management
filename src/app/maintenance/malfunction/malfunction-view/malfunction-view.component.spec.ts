import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionViewComponent } from './malfunction-view.component';

describe('MalfunctionViewComponent', () => {
  let component: MalfunctionViewComponent;
  let fixture: ComponentFixture<MalfunctionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
