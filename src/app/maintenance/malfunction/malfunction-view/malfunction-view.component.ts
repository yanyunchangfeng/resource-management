import { Component, OnInit } from '@angular/core';
import {MaintenanceService} from '../../maintenance.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-malfunction-view',
  templateUrl: './malfunction-view.component.html',
  styleUrls: ['./malfunction-view.component.scss']
})
export class MalfunctionViewComponent implements OnInit {

    sid: string;
    status: string;
    constructor( private activatedRouter: ActivatedRoute,
                 private router: Router,
                 private maintenanceService: MaintenanceService,
                 private eventBusService: EventBusService) { }

    ngOnInit() {
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
}
