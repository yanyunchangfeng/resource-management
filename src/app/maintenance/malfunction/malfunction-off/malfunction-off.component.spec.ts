import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionOffComponent } from './malfunction-off.component';

describe('MalfunctionOffComponent', () => {
  let component: MalfunctionOffComponent;
  let fixture: ComponentFixture<MalfunctionOffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionOffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionOffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
