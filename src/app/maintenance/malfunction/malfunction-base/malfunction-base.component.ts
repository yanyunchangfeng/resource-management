import { Component, OnInit } from '@angular/core';
import {MaintenanceService} from '../../maintenance.service';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-malfunction-base',
  templateUrl: './malfunction-base.component.html',
  styleUrls: ['./malfunction-base.component.scss']
})
export class MalfunctionBaseComponent implements OnInit {
    flowChartDatas = [];                    // 流程图节点数据

    constructor(private eventBusService: EventBusService,
        private maintenanceService: MaintenanceService) { }

    ngOnInit() {
        this.maintenanceService.getFlowChart().subscribe(res => {
            this.flowChartDatas = res;
        });
        this.eventBusService.maintenance.subscribe(res => {
            this.flowChartDatas = res;
        });
    }

}
