import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionBaseComponent } from './malfunction-base.component';

describe('MalfunctionBaseComponent', () => {
  let component: MalfunctionBaseComponent;
  let fixture: ComponentFixture<MalfunctionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
