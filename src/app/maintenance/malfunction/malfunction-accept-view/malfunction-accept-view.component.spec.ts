import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionAcceptViewComponent } from './malfunction-accept-view.component';

describe('MalfunctionAcceptViewComponent', () => {
  let component: MalfunctionAcceptViewComponent;
  let fixture: ComponentFixture<MalfunctionAcceptViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionAcceptViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionAcceptViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
