import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-malfunction-accept-view',
  templateUrl: './malfunction-accept-view.component.html',
  styleUrls: ['./malfunction-accept-view.component.scss']
})
export class MalfunctionAcceptViewComponent implements OnInit {


    constructor(private activatedRouter: ActivatedRoute,
                private router: Router, ) { }

    ngOnInit() {
    }

    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }

}
