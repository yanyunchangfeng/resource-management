import { Component, OnInit } from '@angular/core';
import {MaintenanceService} from '../../maintenance.service';
import {StorageService} from '../../../services/storage.service';
import {Message, TreeNode} from 'primeng/primeng';
import {PublicService} from '../../../services/public.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import { PUblicMethod} from '../../../services/PUblicMethod';
import {EventBusService} from '../../../services/event-bus.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormobjModel} from '../../formobj.model';

@Component({
  selector: 'app-malfunction-add',
  templateUrl: './malfunction-add.component.html',
  styleUrls: ['./malfunction-add.component.scss']
})
export class MalfunctionAddComponent implements OnInit {

    formObj: FormobjModel;
    display: boolean = false;
    departementDisplay: boolean;           // 树显示控制
    filesTree4: TreeNode[] = [];
    selected: Array<any> = [];
    displayPersonel: boolean;              // 人员组织组件是否显示
    myForm: FormGroup;
    zh: Object;
    message: Message[] = [];               // 交互提示弹出框
    ip: string;
    formTitle = '创建故障单';
    uploadedFiles: any[] = [];
    maxDate: Date;
    dropdownOption = {
        fromx: [],
        level: [],
        bt_system: [],
        influence: [],
        urgency: [],
        acceptor: []
    };

    constructor(private maintenanceService: MaintenanceService,
                private router: Router,
                private activatedRouter: ActivatedRoute,
                private storageService: StorageService,
                private publicService: PublicService,
                private fb: FormBuilder,
                private eventBusService: EventBusService) {
    }

    ngOnInit() {
        this.initForm();
        this.formObj = new FormobjModel();
        this.zh = new PUblicMethod().initZh();
        this.maxDate = new Date();
        this.getDepPerson();
        this.getOptions();
        this.formObj.status = '新建';
        this.formTitle = '新建故障单';
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.formObj.create_time = PUblicMethod.formateEnrtyTime(new Date());
        this.formObj.occurrence_time = PUblicMethod.formateEnrtyTime(new Date());
        (!this.formObj.sid) && (this.getDepPerson());
        this.ip = environment.url.management;
        this.displayPersonel = false;
        if (this.formObj.sid) {
            this.formTitle = '编辑故障单';
            this.getTirckMessage(this.formObj.sid);
            this.maintenanceService.getFlowChart(this.formObj.status).subscribe(res => {
                this.eventBusService.maintenance.next(res);
            });
        }
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
    getOptions() {
        this.maintenanceService.getParamOptions().subscribe(res => {
            this.getPrority(res['影响度'][0]['name'], res['紧急度'][0]['name']);
            this.dropdownOption['fromx'] = PUblicMethod.formateDropDown(res['来源']);
            this.dropdownOption['level'] = PUblicMethod.formateDropDown(res['故障级别']);
            this.dropdownOption['bt_system'] = PUblicMethod.formateDropDown(res['所属系统']);
            this.dropdownOption['influence'] = PUblicMethod.formateDropDown(res['影响度']);
            this.dropdownOption['urgency'] = PUblicMethod.formateDropDown(res['紧急度']);
            this.formObj.fromx = res['来源'][0]['name'];
            this.formObj.level = res['故障级别'][0]['name'];
            this.formObj.bt_system = res['所属系统'][0]['name'];
            this.formObj.influence = res['影响度'][0]['name'];
            this.formObj.urgency = res['紧急度'][0]['name'];
        });
    }
    initForm() {
        this.myForm = this.fb.group({
            'submitter': '',
            'title': '',
            'content': '',
            'acceptor': ''
        });
    }
    showTreeDialog( opType ) {
        switch (opType) {
            case 'submitter':
                this.displayPersonel = true;
                break;
            case 'department':
                this.display = true;
                this.publicService.getDepartmentDatas().subscribe(res => {
                    this.filesTree4 = res;
                });
        }

    }
    clearTreeDialog(opType) {
        switch (opType) {
            case 'submitter':
                this.formObj.submitter = '';
                this.formObj.submitter_pid = '';
                this.formObj.submitter_org_oid = '';
                this.formObj.submitter_org = '';
                this.formObj.submitter_phone = '';
                break;
            case 'department':
                this.formObj.acceptor = '';
                this.formObj.acceptor_org = '';
                this.formObj.acceptor_org_oid = '';
                this.formObj.acceptor_pid = '';
                break;
        }

    }
    closeTreeDialog() {
        this.display = false;
        this.formObj.acceptor_org = this.selected['label'];
        this.formObj.acceptor_org_oid = this.selected['oid'];
        this.getDepPerson(this.selected['oid']);
    }
    nodeExpand(event) {
        if (event.node) {
            this.publicService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(res => {
                event.node.children  = res;
            });
        }
    }
    dataEmitter(event) {
        this.formObj.submitter = event.name;
        this.formObj.submitter_pid = event.pid;
        this.formObj.submitter_org_oid = event.oid;
        this.formObj.submitter_org = event.organization;
        this.formObj.submitter_phone = event.mobile;
    }
    displayEmitter(event) {
        this.displayPersonel = event;
    }
    onFocus() {
        if (this.formObj.influence['name']) {
            this.formObj.influence = this.formObj.influence['name'];
        }
        if (this.formObj.urgency['name']) {
            this.formObj.urgency = this.formObj.urgency['name'];
        }
        this.getPrority(this.formObj.influence, this.formObj.urgency);
    }
    getPrority(influency, urgency) {
        if ( influency && urgency ) {
            this.maintenanceService.getPrority( influency, urgency ).subscribe(res => {
                if (res) {
                    this.formObj.priority = res['priority'];
                    this.getDeadline(influency, urgency, res['priority']);
                }
            });
        }
    }
    getDeadline(influency, urgency, prority) {
        if ( influency && urgency && prority ) {
            this.maintenanceService.getDeadline( influency, urgency, prority, this.formObj.create_time).subscribe(res => {
                if (res) {
                    this.formObj.deadline = res['time_deadline'];
                }
            });
        }
    }
    onBeforeUpload(event) {
        let token = this.storageService.getToken('token');
        event.formData.append('access_token', token);
    }
    onUpload(event) {
        let xhrRespose = JSON.parse(event.xhr.response);
        if ( xhrRespose.errcode && xhrRespose.errcode === '00000' ) {
            this.formObj.attachments = xhrRespose['datas'];
            this.message = [];
            this.message.push({severity: 'success', summary: '消息提示', detail: '上传成功'});
        }else {
            this.message = [];
            this.message.push({severity: 'error', summary: '消息提示', detail: `上传失败${xhrRespose.errmsg}`});
        }
    }
    getDepPerson(oid?) {
        this.publicService.getApprovers(oid).subscribe(res => {
            if (!res) {
                this.dropdownOption.acceptor = [];
            }else {
                let newArray = PUblicMethod.formateDepDropDown(res);
                this.dropdownOption.acceptor = newArray;
                this.formObj.acceptor_pid = newArray[0]['value']['pid'];
                this.formObj.acceptor = newArray[0]['value']['name'];
            }
        });
    }
    onChange(event) {
        this.formObj.acceptor = event['value']['name'];
        this.formObj.acceptor_pid = event['value']['pid'];
    }
    save() {
        this.resetOccurtime();
        this.maintenanceService.saveTicket( this.formObj ).subscribe(res => {
            if ( res === '00000' ) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '保存成功'});
                window.setTimeout(() => {
                  this.goBack();
                }, 1100);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `保存失败${res}`});
            }
        });
    }
    resetOccurtime() {
        this.formObj.occurrence_time = PUblicMethod.formateEnrtyTime(this.formObj.occurrence_time);
    }

    get Submitter() {
        return this.myForm.controls['submitter'].untouched && this.myForm.controls['submitter'].hasError('required');
    }
    get title() {
        return this.myForm.controls['title'].untouched && this.myForm.controls['title'].hasError('required');
    }
    get content() {
        return this.myForm.controls['content'].untouched && this.myForm.controls['content'].hasError('required');
    }
    get acceptor() {
        return this.myForm.controls['acceptor'].untouched && this.myForm.controls['acceptor'].hasError('required');
    }
    setValidators() {
        this.myForm.controls['submitter'].setValidators([Validators.required]);
        this.myForm.controls['title'].setValidators([Validators.required]);
        this.myForm.controls['content'].setValidators([Validators.required]);
        this.myForm.controls['submitter'].updateValueAndValidity();
        this.myForm.controls['title'].updateValueAndValidity();
        this.myForm.controls['content'].updateValueAndValidity();
    }
    submitTrick() {
        if ( !this.formObj.submitter
            || !this.formObj.title
            || !this.formObj.content
        ) {
            this.showError();
            this.setValidators();
        }else {
            this.formObj.status = '待分配';
            this.submitPost(this.formObj);
        }
    }
    showError() {
        this.message = [];
        this.message.push({severity: 'error', summary: '错误消息提示', detail: '请填写完整表单'});
    }
    submitPost(paper) {
        this.resetOccurtime();
        this.maintenanceService.submitTicket( paper ).subscribe(res => {
            if ( res === '00000' ) {
                this.message = [];
                this.message.push({severity: 'success', summary: '消息提示', detail: '操作成功'});
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `操作失败${res}`});
            }
        });
    }
    submitAndDistribute() {
        if ( !this.formObj.submitter
            || !this.formObj.title
            || !this.formObj.content
            || !this.formObj.acceptor_org
        ) {
            this.showError();
            this.setValidators();
            this.myForm.controls['acceptor'].setValidators([Validators.required]);
            this.myForm.controls['acceptor'].updateValueAndValidity();
        }else {
            this.formObj.status = '待接受';
            this.submitPost(this.formObj);
        }
    }
    getTirckMessage(sid) {
        this.maintenanceService.getTrick(sid).subscribe(res => {
            // this.formObj = new FormobjModel(res);
            this.formObj.sid = res['sid'];
            this.formObj.fromx = res['fromx'];
            this.formObj.status = res['status'];
            this.formObj.submitter = res['submitter'];
            this.formObj.submitter_pid = res['submitter_pid'];
            this.formObj.submitter_org_oid = res['submitter_org_oid'];
            this.formObj.submitter_org = res['submitter_org'];
            this.formObj.submitter_phone = res['submitter_phone'];
            this.formObj.occurrence_time = res['occurrence_time'];
            this.formObj.deadline = res['deadline'];
            this.formObj.influence = res['influence'];
            this.formObj.urgency = res['urgency'];
            this.formObj.priority = res['priority'];
            this.formObj.level = res['level'];
            this.formObj.bt_system = res['bt_system'];
            this.formObj.addr = res['addr'];
            this.formObj.title = res['title'];
            this.formObj.content = res['content'];
            this.formObj.service = res['service'];
            this.formObj.devices = res['devices'];
            this.formObj.acceptor_org = res['acceptor_org'];
            this.formObj.acceptor_org_oid = res['acceptor_org_oid'];
            this.formObj.acceptor = res['acceptor'];
            this.formObj.acceptor_pid = res['acceptor_pid'];
            this.formObj.create_time = res['create_time'];
        });
    }
  }
