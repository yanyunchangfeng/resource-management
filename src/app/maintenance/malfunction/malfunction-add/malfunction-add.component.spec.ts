import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionAddComponent } from './malfunction-add.component';

describe('MalfunctionAddComponent', () => {
  let component: MalfunctionAddComponent;
  let fixture: ComponentFixture<MalfunctionAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
