import { Component, OnInit } from '@angular/core';
import {EventBusService} from "../../../services/event-bus.service";
import {MaintenanceService} from "../../maintenance.service";

@Component({
  selector: 'app-malfunction-distribute',
  templateUrl: './malfunction-distribute.component.html',
  styleUrls: ['./malfunction-distribute.component.scss']
})
export class MalfunctionDistributeComponent implements OnInit {

    searchType = '待分配';            // 搜索类型
    eventDatas: Object;                   // 子组件返回的数据

    constructor(private eventBusService: EventBusService,
                private maintenanceServicen: MaintenanceService) { }

    ngOnInit() {
        this.maintenanceServicen.getFlowChart().subscribe(res => {
           this.eventBusService.maintenance.next(res);
        });
    }

    searchInfoEmitter(event) {
        this.eventDatas = event;
    }
}
