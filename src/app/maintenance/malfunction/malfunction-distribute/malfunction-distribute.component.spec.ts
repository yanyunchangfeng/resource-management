import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionDistributeComponent } from './malfunction-distribute.component';

describe('MalfunctionDistributeComponent', () => {
  let component: MalfunctionDistributeComponent;
  let fixture: ComponentFixture<MalfunctionDistributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionDistributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionDistributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
