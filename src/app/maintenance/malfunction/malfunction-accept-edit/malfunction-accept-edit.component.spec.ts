import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionAcceptEditComponent } from './malfunction-accept-edit.component';

describe('MalfunctionAcceptEditComponent', () => {
  let component: MalfunctionAcceptEditComponent;
  let fixture: ComponentFixture<MalfunctionAcceptEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionAcceptEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionAcceptEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
