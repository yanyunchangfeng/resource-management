import {Component, OnInit} from '@angular/core';
import {MaintenanceService} from '../../../maintenance.service';
import {MatenanceObjModel} from '../../../matenanceObj.model';
import {ActivatedRoute} from '@angular/router';
import {EventBusService} from "../../../../services/event-bus.service";

@Component({
  selector: 'app-base-view',
  templateUrl: './base-view.component.html',
  styleUrls: ['./base-view.component.scss']
})
export class BaseViewComponent implements OnInit {
    sid: string;
    formObj: MatenanceObjModel;
    constructor(private maintenanceService: MaintenanceService,
                private eventBusService: EventBusService,
                private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.formObj = new MatenanceObjModel();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.initTrick(this.sid);
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
    }
    initTrick(s) {
        this.maintenanceService.getTrick(s).subscribe(res => {
            if (res) {
                this.formObj.sid = res['sid'];
                this.formObj.fromx = res['fromx'];
                this.formObj.status = res['status'];
                this.formObj.submitter = res['submitter'];
                this.formObj.submitter_org = res['submitter_org'];
                this.formObj.submitter_phone = res['submitter_phone'];
                this.formObj.create_time = res['create_time'];
                this.formObj.occurrence_time = res['occurrence_time'];
                this.formObj.deadline = res['deadline'];
                this.formObj.influence = res['influence'];
                this.formObj.urgency = res['urgency'];
                this.formObj.priority = res['priority'];
                this.formObj.level = res['level'];
                this.formObj.bt_system = res['bt_system'];
                this.formObj.addr = res['addr'];
                this.formObj.title = res['title'];
                this.formObj.content = res['content'];
                this.formObj.attachments = res['attachments'];
                this.formObj.service = res['service'];
                this.formObj.devices = res['devices'];
            }
        });
    }

}
