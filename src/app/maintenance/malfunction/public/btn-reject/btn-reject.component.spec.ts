import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnRejectComponent } from './btn-reject.component';

describe('BtnRejectComponent', () => {
  let component: BtnRejectComponent;
  let fixture: ComponentFixture<BtnRejectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnRejectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnRejectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
