import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfirmationService} from 'primeng/primeng';
import {MaintenanceService} from '../../../maintenance.service';
import {EventBusService} from '../../../../services/event-bus.service';

@Component({
  selector: 'app-btn-reject',
  templateUrl: './btn-reject.component.html',
  styleUrls: ['./btn-reject.component.css']
})
export class BtnRejectComponent implements OnInit {

    @Input() public data: any;
    @Output() public dataEmitter: EventEmitter<any> = new EventEmitter();

    dialogDisplay: boolean;
    fromSource: Object = {};

    constructor(private maintenanceService: MaintenanceService,
                private confirmationService: ConfirmationService,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.dialogDisplay = false;
    }

    reject(data) {
        this.dialogDisplay = true;
        this.fromSource = {
            'sid': data['sid'],
            'status': '待分配'
        };
        this.maintenanceService.getFlowChart(data.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
        this.confirmationService.confirm({
            message: '确认拒绝吗？',
            accept: () => {
                this.sure();
            }
        });
    }

    sure() {
        this.maintenanceService.recieveOrRejectTrick(this.fromSource).subscribe(res => {
            this.dataEmitter.emit(res);
            this.dialogDisplay = false;
        });
    }
}
