import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MaintenanceService} from '../../../maintenance.service';

@Component({
  selector: 'app-btn-off',
  templateUrl: './btn-off.component.html',
  styleUrls: ['./btn-off.component.scss']
})
export class BtnOffComponent implements OnInit {

    @Input() public data: any;
    @Output() public dataEmitter: EventEmitter<any> = new EventEmitter();

    dialogDisplay: boolean;
    fromSource: Object = {};

    constructor(private maintenanceService: MaintenanceService) { }

    ngOnInit() {
        this.dialogDisplay = false;
    }
    // off(data) {
    //     this.dialogDisplay = true;
    //     this.fromSource = {
    //         'sid': data['sid'],
    //         'close_code':  data['close_code']
    //     };
    // }
    //
    // sure() {
    //     this.maintenanceService.offTrick(this.fromSource).subscribe(res => {
    //         this.dataEmitter.emit(res);
    //         this.dialogDisplay = false;
    //     });
    // }
}
