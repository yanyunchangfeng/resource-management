import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnOffComponent } from './btn-off.component';

describe('BtnOffComponent', () => {
  let component: BtnOffComponent;
  let fixture: ComponentFixture<BtnOffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnOffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnOffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
