import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-btn-view',
  templateUrl: './btn-view.component.html',
  styleUrls: ['./btn-view.component.scss']
})
export class BtnViewComponent implements OnInit {
  @Input() public data: any;
  constructor(private router: Router) { }

  ngOnInit() {
  }
  view(data) {
    switch (data.status) {
        case '新建':
            this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionView', { id: data.sid, status: data.status} ]);
            break;
        case '待分配':
            this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionView', { id: data.sid, status: data.status} ]);
            break;
        case '待接受':
        case '待处理':
            this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionAcceptView', { id: data.sid, status: data.status} ]);
            break;
        case '已解决':
            this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionCloseView', { id: data.sid, status: data.status} ]);
            break;
        case '已关闭':
            this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionClosedView', { id: data.sid, status: data.status} ]);
            break;
        case '处理中':
            this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionSolvedView', { id: data.sid, status: data.status} ]);
            break;
    }
  }
}
