import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-btn-edit',
  templateUrl: './btn-edit.component.html',
  styleUrls: ['./btn-edit.component.scss']
})
export class BtnEditComponent implements OnInit {
  @Input() public data: any;
  constructor(private router: Router) { }

  ngOnInit() {
  }
  jumper() {
      switch (this.data.status) {
          case '新建':
              this.router.navigate([ '/index/maintenance/malfunctionBase/malfunctionAdd', { id: this.data.sid, status: this.data.status} ]);
              break;
          case '待分配':
              this.router.navigate([ '/index/maintenance/malfunctionBase/malfunctionAdd', { id: this.data.sid, status: this.data.status} ]);
              break;
          case '待接受':
              this.router.navigate([ '/index/maintenance/malfunctionBase/malfunctionAcceptEdit', { id: this.data.sid, status: this.data.status} ]);
              break;
      }
  }

}
