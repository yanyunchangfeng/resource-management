import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MaintenanceService} from '../../../maintenance.service';

@Component({
  selector: 'app-btn-solve',
  templateUrl: './btn-solve.component.html',
  styleUrls: ['./btn-solve.component.scss']
})
export class BtnSolveComponent implements OnInit {

    @Input() public data: any;
    @Output() public dataEmitter: EventEmitter<any> = new EventEmitter();

    dialogDisplay: boolean;
    fromSource: Object = {};

    constructor(private maintenanceService: MaintenanceService) { }

    ngOnInit() {
        this.dialogDisplay = false;
    }
    solve(data) {
        this.dialogDisplay = true;
        this.fromSource = {
            'sid': data['sid'],
            'solve_per': data['solve_per'],
            'solve_org': data['solve_org'],
            'reason': data['reason'],
            'means': data['means'],
            'finish_time': data['finish_time']
        };
    }

    sure() {
        this.maintenanceService.solveTrick(this.fromSource).subscribe(res => {
            this.dataEmitter.emit(res);
            this.dialogDisplay = false;
        });
    }
}
