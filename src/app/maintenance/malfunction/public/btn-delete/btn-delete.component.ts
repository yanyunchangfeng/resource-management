import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfirmationService} from 'primeng/primeng';
import {MaintenanceService} from '../../../maintenance.service';
import {EventBusService} from '../../../../services/event-bus.service';

@Component({
  selector: 'app-btn-delete',
  templateUrl: './btn-delete.component.html',
  styleUrls: ['./btn-delete.component.css']
})
export class BtnDeleteComponent implements OnInit {

    @Input() public data: any;
    @Output() public dataEmitter: EventEmitter<any> = new EventEmitter();

    dialogDisplay: boolean;
    sidArray: string[] = [];

    constructor(private maintenanceService: MaintenanceService,
                private confirmationService: ConfirmationService,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.dialogDisplay = false;
    }

    delete(data) {
        this.sidArray.length = 0;
        this.dialogDisplay = true;
        this.sidArray[this.sidArray.length] = data['sid'];
        this.maintenanceService.getFlowChart(data.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: () => {
                this.sure();
            }
        });
    }

    sure() {
        this.maintenanceService.deleteTrick(this.sidArray).subscribe(res => {
            this.dataEmitter.emit(res);
            this.dialogDisplay = false;
        });
    }
}
