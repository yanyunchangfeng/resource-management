import {Component, OnInit} from '@angular/core';
import {MaintenanceService} from '../../../maintenance.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatenanceObjModel} from '../../../matenanceObj.model';

@Component({
  selector: 'app-colse-base-view',
  templateUrl: './colse-base-view.component.html',
  styleUrls: ['./colse-base-view.component.scss']
})
export class ColseBaseViewComponent implements OnInit {
    sid: string;
    formObj: MatenanceObjModel;
    constructor(private maintenanceService: MaintenanceService,
                private router: Router,
                private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.formObj = new MatenanceObjModel();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initTrick(this.sid);
    }
    initTrick(s) {
        this.maintenanceService.getTrick(s).subscribe(res => {
            if (res) {
                this.formObj.solve_org_name = res['solve_org_name'];
                this.formObj.solve_per_name = res['solve_per_name'];
                this.formObj.process_time = res['process_time'];
                this.formObj.finish_time = res['finish_time'];
                this.formObj.reason = res['reason'];
                this.formObj.means = res['means'];
                this.formObj.acceptor_org = res['acceptor_org'];
                this.formObj.acceptor = res['acceptor'];
            }
        });
    }
    goBack() {
        this.router.navigate(['../malfunctionOverview'], {relativeTo: this.activatedRouter});
    }
}
