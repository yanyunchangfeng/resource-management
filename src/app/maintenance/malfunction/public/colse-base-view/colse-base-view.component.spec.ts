import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColseBaseViewComponent } from './colse-base-view.component';

describe('ColseBaseViewComponent', () => {
  let component: ColseBaseViewComponent;
  let fixture: ComponentFixture<ColseBaseViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColseBaseViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColseBaseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
