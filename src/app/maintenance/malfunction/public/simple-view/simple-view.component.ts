import {Component, Input, OnInit} from '@angular/core';
import {Message} from 'primeng/primeng';
import {MaintenanceService} from '../../../maintenance.service';
import {ActivatedRoute} from '@angular/router';
import {MatenanceObjModel} from '../../../matenanceObj.model';

@Component({
  selector: 'app-simple-view',
  templateUrl: './simple-view.component.html',
  styleUrls: ['./simple-view.component.scss']
})
export class SimpleViewComponent implements OnInit {
    sid: string;
    formObj: MatenanceObjModel;
    constructor(private maintenanceService: MaintenanceService,
                private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.formObj = new MatenanceObjModel();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initTrick(this.sid);
    }
    initTrick(s) {
        this.maintenanceService.getTrick(s).subscribe(res => {
            if (res) {
             this.formObj.acceptor = res['acceptor'];
             this.formObj.acceptor_org = res['acceptor_org'];
            }
        });
    }
}
