import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MaintenanceService} from '../../../maintenance.service';
import {ConfirmationService} from 'primeng/primeng';
import {EventBusService} from '../../../../services/event-bus.service';

@Component({
  selector: 'app-btn-receive',
  templateUrl: './btn-receive.component.html',
  styleUrls: ['./btn-receive.component.css']
})
export class BtnReceiveComponent implements OnInit {

    @Input() public data: any;
    @Output() public dataEmitter: EventEmitter<any> = new EventEmitter();

    dialogDisplay: boolean;
    fromSource: Object = {};

    constructor(private maintenanceService: MaintenanceService,
                private confirmationService: ConfirmationService,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.dialogDisplay = false;
    }

    receive(data) {
         this.dialogDisplay = true;
         this.fromSource = {
             'sid': data['sid'],
             'status': '待处理'
         };
        this.maintenanceService.getFlowChart(data.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
        this.confirmationService.confirm({
            message: '确认接受吗？',
            accept: () => {
                this.sure();
            }
        });
    }

    sure() {
        this.maintenanceService.recieveOrRejectTrick(this.fromSource).subscribe(res => {
            this.dataEmitter.emit(res);
            this.dialogDisplay = false;
        });
    }
}
