import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnReceiveComponent } from './btn-receive.component';

describe('BtnReceiveComponent', () => {
  let component: BtnReceiveComponent;
  let fixture: ComponentFixture<BtnReceiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnReceiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnReceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
