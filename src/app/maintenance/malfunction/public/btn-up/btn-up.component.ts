import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-btn-up',
  templateUrl: './btn-up.component.html',
  styleUrls: ['./btn-up.component.scss']
})
export class BtnUpComponent implements OnInit {
    @Input() public data: any;

    constructor() { }

    ngOnInit() {
    }
}
