import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MaintenanceService} from '../../../maintenance.service';
import {PUblicMethod} from "../../../../services/PUblicMethod";

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

    malfNumber: string;                    // 查询故障单号字段
    malfFrom: string;                      // 查询报障人字段
    malfOccur: any;                       // 查询发生时间字段
    malstatus: Array<object>;                             // 查询状态字段
    selectedStatus: string;                // 查询选中字段
    @Input() searchType: string;
    @Output() searchInfoEmitter: EventEmitter<Object>  = new EventEmitter();
    zh: any;                               // 汉化

    constructor(private maintenanceService: MaintenanceService) { }

    ngOnInit() {
        this.zh = new PUblicMethod().initZh();
        this.initFlowChart();
    }
    initFlowChart() {
        this.maintenanceService.getAllMalfunctionStatus().subscribe(res => {
            let newArray: Array<object> = PUblicMethod.formateDropDown(res);
            this.malstatus = newArray;
            this.selectedStatus = newArray[0]['value'];
        });
    }

    searchSchedule() {
        this.troubleGet();
    }

    troubleGet() {
        this.malfOccur = PUblicMethod.formateEnrtyTime(this.malfOccur);
        this.maintenanceService.getAllMainfunctionList('', '', this.malfNumber, this.malfFrom, this.malfOccur, this.selectedStatus).subscribe(res => {
            if (res) {
                this.searchInfoEmitter.emit(res);
            }
        });
    }
    onChange(event) {
        this.selectedStatus = event['value'];
    }
}
