import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleTimeComponent } from './simple-time.component';

describe('SimpleTimeComponent', () => {
  let component: SimpleTimeComponent;
  let fixture: ComponentFixture<SimpleTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
