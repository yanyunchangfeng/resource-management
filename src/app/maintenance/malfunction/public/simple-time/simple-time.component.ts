import { Component, OnInit } from '@angular/core';
import {MatenanceObjModel} from "../../../matenanceObj.model";
import {MaintenanceService} from "../../../maintenance.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-simple-time',
  templateUrl: './simple-time.component.html',
  styleUrls: ['./simple-time.component.scss']
})
export class SimpleTimeComponent implements OnInit {
    sid: string;
    formObj: MatenanceObjModel;
    constructor(private maintenanceService: MaintenanceService,
                private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.formObj = new MatenanceObjModel();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initTrick(this.sid);
    }
    initTrick(s) {
        this.maintenanceService.getTrick(s).subscribe(res => {
            if (res) {
                this.formObj.accept_time = res['accept_time'];
                this.formObj.assign_time = res['assign_time'];
            }
        });
    }
}
