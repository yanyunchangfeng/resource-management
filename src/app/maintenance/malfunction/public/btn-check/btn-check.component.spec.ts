import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnCheckComponent } from './btn-check.component';

describe('BtnCheckComponent', () => {
  let component: BtnCheckComponent;
  let fixture: ComponentFixture<BtnCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
