import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-btn-check',
  templateUrl: './btn-check.component.html',
  styleUrls: ['./btn-check.component.css']
})
export class BtnCheckComponent implements OnInit {
  @Input() public data: any;
  constructor() { }

  ngOnInit() {
  }
  check(data) {
    console.log(data);
  }
}
