import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleCloseComponent } from './simple-close.component';

describe('SimpleCloseComponent', () => {
  let component: SimpleCloseComponent;
  let fixture: ComponentFixture<SimpleCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
