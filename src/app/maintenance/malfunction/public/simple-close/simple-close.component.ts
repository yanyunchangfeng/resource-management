import { Component, OnInit } from '@angular/core';
import {MatenanceObjModel} from "../../../matenanceObj.model";
import {MaintenanceService} from "../../../maintenance.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-simple-close',
  templateUrl: './simple-close.component.html',
  styleUrls: ['./simple-close.component.scss']
})
export class SimpleCloseComponent implements OnInit {
    sid: string;
    formObj: MatenanceObjModel;
    constructor(private maintenanceService: MaintenanceService,
                private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.formObj = new MatenanceObjModel();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initTrick(this.sid);
    }
    initTrick(s) {
        this.maintenanceService.getTrick(s).subscribe(res => {
            if (res) {
               this.formObj.close_code = res['close_code'];
               this.formObj.close_time = res['close_time'];
            }
        });
    }


}
