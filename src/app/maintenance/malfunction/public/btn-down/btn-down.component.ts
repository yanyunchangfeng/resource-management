import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-btn-down',
  templateUrl: './btn-down.component.html',
  styleUrls: ['./btn-down.component.scss']
})
export class BtnDownComponent implements OnInit {
    @Input() public data: any;

    constructor() { }

    ngOnInit() {
    }

}
