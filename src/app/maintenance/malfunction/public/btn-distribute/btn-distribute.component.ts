import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-btn-distribute',
  templateUrl: './btn-distribute.component.html',
  styleUrls: ['./btn-distribute.component.scss']
})
export class BtnDistributeComponent implements OnInit {
  @Input() public data: any;
  constructor() { }

  ngOnInit() {
  }
  ditribute() {
    console.log(this.data);
  }
}
