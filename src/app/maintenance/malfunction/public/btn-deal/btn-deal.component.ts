import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfirmationService} from 'primeng/primeng';
import {MaintenanceService} from '../../../maintenance.service';
import {EventBusService} from '../../../../services/event-bus.service';



@Component({
  selector: 'app-btn-deal',
  templateUrl: './btn-deal.component.html',
  styleUrls: ['./btn-deal.component.css']
})
export class BtnDealComponent implements OnInit {

    @Input() public data: any;
    @Output() public dataEmitter: EventEmitter<any> = new EventEmitter();
    dialogDisplay: boolean;
    fromSource: Object = {};

    constructor(private maintenanceService: MaintenanceService,
                private confirmationService: ConfirmationService,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.dialogDisplay = false;
    }

    deal(data) {
        this.dialogDisplay = true;
        this.fromSource = {
            'sid': data['sid'],
            'solve_per': data['solve_per'],
            'solve_per_pid': data['pid'],
            'solve_org': data['solve_org'],
            'solve_org_oid': data['oid'],
            'reason': data['reason'],
            'means': data['means'],
            'finish_time': data['finish_time']
        };
        this.maintenanceService.getFlowChart(data.status).subscribe(res => {
            this.eventBusService.maintenance.next(res);
        });
        this.confirmationService.confirm({
            message: '确认处理吗？',
            accept: () => {
                this.sure();
            }
        });
    }

    sure() {
        this.maintenanceService.dealTrick(this.fromSource).subscribe(res => {
            this.dataEmitter.emit(res);
            this.dialogDisplay = false;
        });
    }
}
