import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionReceiveComponent } from './malfunction-receive.component';

describe('MalfunctionReceiveComponent', () => {
  let component: MalfunctionReceiveComponent;
  let fixture: ComponentFixture<MalfunctionReceiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionReceiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionReceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
