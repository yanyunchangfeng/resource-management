import { NgModule } from '@angular/core';
import {ShareModule} from '../shared/share.module';
import {MaintenanceRoutingModule} from './maintenance-routing.module';
import {MaintenanceService} from './maintenance.service';
import { MalfunctionComponent } from './basaldatas/malfunction/malfunction.component';
import { MalfunctionBaseComponent } from './malfunction/malfunction-base/malfunction-base.component';
import { MalfunctionAddComponent } from './malfunction/malfunction-add/malfunction-add.component';
import { MalfunctionDistributeComponent } from './malfunction/malfunction-distribute/malfunction-distribute.component';
import { MalfunctionReceiveComponent } from './malfunction/malfunction-receive/malfunction-receive.component';
import { MalfunctionDealComponent } from './malfunction/malfunction-deal/malfunction-deal.component';
import { MalfunctionChangeComponent } from './malfunction/malfunction-change/malfunction-change.component';
import { MalfunctionSolveComponent } from './malfunction/malfunction-solve/malfunction-solve.component';
import { MalfunctionOffComponent } from './malfunction/malfunction-off/malfunction-off.component';
import { MalfunctionOverviewComponent } from './malfunction/malfunction-overview/malfunction-overview.component';
import { MalfunctionEditComponent } from './malfunction/malfunction-edit/malfunction-edit.component';
import { PublicService } from '../services/public.service';
import { BtnEditComponent } from './malfunction/public/btn-edit/btn-edit.component';
import { BtnDeleteComponent } from './malfunction/public/btn-delete/btn-delete.component';
import { BtnViewComponent } from './malfunction/public/btn-view/btn-view.component';
import { BtnDistributeComponent } from './malfunction/public/btn-distribute/btn-distribute.component';
import { BtnReceiveComponent } from './malfunction/public/btn-receive/btn-receive.component';
import { BtnRejectComponent } from './malfunction/public/btn-reject/btn-reject.component';
import { BtnDealComponent } from './malfunction/public/btn-deal/btn-deal.component';
import { BtnSolveComponent } from './malfunction/public/btn-solve/btn-solve.component';
import { BtnOffComponent } from './malfunction/public/btn-off/btn-off.component';
import { SearchFormComponent } from './malfunction/public/search-form/search-form.component';
import { MalfunctionTableComponent } from './malfunction/public/malfunction-table/malfunction-table.component';
import { MalfunctionSolvedComponent } from './malfunction/malfunction-solved/malfunction-solved.component';
import { MalfunctionViewComponent } from './malfunction/malfunction-view/malfunction-view.component';
import { MalfunctionAcceptComponent } from './malfunction/malfunction-accept/malfunction-accept.component';
import { MalfunctionProcessComponent } from './malfunction/malfunction-process/malfunction-process.component';
import { MalfunctionUpComponent } from './malfunction/malfunction-up/malfunction-up.component';
import { MalfunctionTurnComponent } from './malfunction/malfunction-turn/malfunction-turn.component';
import { MalfunctionCloseComponent } from './malfunction/malfunction-close/malfunction-close.component';
import { PersonelDialogComponent } from './malfunction/public/personel-dialog/personel-dialog.component';
import { MalfunctionAcceptEditComponent } from './malfunction/malfunction-accept-edit/malfunction-accept-edit.component';
import { MalfunctionAcceptViewComponent } from './malfunction/malfunction-accept-view/malfunction-accept-view.component';
import { SimpleViewComponent } from './malfunction/public/simple-view/simple-view.component';
import { UploadFileViewComponent } from './malfunction/public/upload-file-view/upload-file-view.component';
import { BaseViewComponent } from './malfunction/public/base-view/base-view.component';
import { ColseBaseViewComponent } from './malfunction/public/colse-base-view/colse-base-view.component';
import { MalfunctionCloseViewComponent } from './malfunction/malfunction-close-view/malfunction-close-view.component';
import { MalfunctionClosedViewComponent } from './malfunction/malfunction-closed-view/malfunction-closed-view.component';
import { MalfunctionSolvedViewComponent } from './malfunction/malfunction-solved-view/malfunction-solved-view.component';
import { BtnUpComponent } from './malfunction/public/btn-up/btn-up.component';
import { BtnDownComponent } from './malfunction/public/btn-down/btn-down.component';
import {MatenanceObjModel} from "./matenanceObj.model";
import { SimpleTimeComponent } from './malfunction/public/simple-time/simple-time.component';
import { SimpleCloseComponent } from './malfunction/public/simple-close/simple-close.component';


@NgModule({
    imports: [
        ShareModule,
        MaintenanceRoutingModule,
    ],
    declarations: [
        MalfunctionComponent,
        MalfunctionBaseComponent,
        MalfunctionAddComponent,
        MalfunctionDistributeComponent,
        MalfunctionReceiveComponent,
        MalfunctionDealComponent,
        MalfunctionChangeComponent,
        MalfunctionSolveComponent,
        MalfunctionOffComponent,
        MalfunctionOverviewComponent,
        MalfunctionEditComponent,
        BtnEditComponent,
        BtnDeleteComponent,
        BtnViewComponent,
        BtnDistributeComponent,
        BtnReceiveComponent,
        BtnRejectComponent,
        BtnDealComponent,
        BtnSolveComponent,
        BtnOffComponent,
        SearchFormComponent,
        MalfunctionTableComponent,
        MalfunctionSolvedComponent,
        MalfunctionViewComponent,
        MalfunctionAcceptComponent,
        MalfunctionProcessComponent,
        MalfunctionUpComponent,
        MalfunctionTurnComponent,
        MalfunctionCloseComponent,
        PersonelDialogComponent,
        MalfunctionAcceptEditComponent,
        MalfunctionAcceptViewComponent,
        SimpleViewComponent,
        UploadFileViewComponent,
        BaseViewComponent,
        ColseBaseViewComponent,
        MalfunctionCloseViewComponent,
        MalfunctionClosedViewComponent,
        MalfunctionSolvedViewComponent,
        BtnUpComponent,
        BtnDownComponent,
        SimpleTimeComponent,
        SimpleCloseComponent,
    ],
    providers: [
        MaintenanceService,
        PublicService,
        MatenanceObjModel
    ]
})
export class MaintenanceModule { }
