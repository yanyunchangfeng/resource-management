export class MatenanceObjModel {
    sid: string = '';
    name: string = '';
    status: string = '';
    create_time: string = '';
    creator: string = '';
    creator_pid: string = '';
    submitter: string = '';
    submitter_pid: string = '';
    submitter_org: string = '';
    submitter_org_oid: string = '';
    submitter_phone: string = '';
    submitter_email: string = '';
    submitter_wechat: string = '';
    fromx: string = '';
    occurrence_time: string = '';
    deadline: string = '';
    influence: string = '';
    urgency: string = '';
    priority: string = '';
    bt_system: string = '';
    level: string = '';
    addr: string = '';
    title: string = '';
    content: string = '';
    service: string = '';
    devices: string = '';
    work_sheets: string = '';
    attachments: Array<Object> = [];
    assign_per: string = '';
    assign_per_pid: string = '';
    assign_time: string = '';
    reassign_per: string = '';
    reassign_per_pid: string = '';
    reassign_time: string = '';
    reassign_reason: string = '';
    acceptor: string = '';
    acceptor_pid: string = '';
    acceptor_org: string = '';
    acceptor_org_oid: string = '';
    accept_time: string = '';
    upgrade_per: string = '';
    upgrade_per_pid: string = '';
    upgrade_time: string = '';
    upgrade_reason: string =  '';
    processor: string =  '';
    processor_pid: string =  '';
    processor_org: string =  '';
    processor_org_oid: string =  '';
    process_time: string =  '';
    finish_time: any =  '';
    process_times: string =  '';
    working_hours: string =  '';
    solve_per: string =  '';
    solve_per_name: string =  '';
    solve_org: string =  '';
    solve_org_name: string =  '';
    reason: string =  '';
    means: string =  '';
    lasting: string =  '';
    close_time: string =  '';
    close_code: string =  '';

}
