import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MalfunctionComponent} from './basaldatas/malfunction/malfunction.component';
import {MalfunctionBaseComponent} from './malfunction/malfunction-base/malfunction-base.component';
import {MalfunctionAddComponent} from './malfunction/malfunction-add/malfunction-add.component';
import {MalfunctionChangeComponent} from './malfunction/malfunction-change/malfunction-change.component';
import {MalfunctionDealComponent} from './malfunction/malfunction-deal/malfunction-deal.component';
import {MalfunctionDistributeComponent} from './malfunction/malfunction-distribute/malfunction-distribute.component';
import {MalfunctionOffComponent} from './malfunction/malfunction-off/malfunction-off.component';
import {MalfunctionReceiveComponent} from './malfunction/malfunction-receive/malfunction-receive.component';
import {MalfunctionSolveComponent} from './malfunction/malfunction-solve/malfunction-solve.component';
import {MalfunctionOverviewComponent} from './malfunction/malfunction-overview/malfunction-overview.component';
import {MalfunctionEditComponent} from './malfunction/malfunction-edit/malfunction-edit.component';
import {MalfunctionSolvedComponent} from './malfunction/malfunction-solved/malfunction-solved.component';
import {MalfunctionViewComponent} from './malfunction/malfunction-view/malfunction-view.component';
import {MalfunctionAcceptComponent} from './malfunction/malfunction-accept/malfunction-accept.component';
import {MalfunctionProcessComponent} from './malfunction/malfunction-process/malfunction-process.component';
import {MalfunctionUpComponent} from './malfunction/malfunction-up/malfunction-up.component';
import {MalfunctionTurnComponent} from './malfunction/malfunction-turn/malfunction-turn.component';
import {MalfunctionCloseComponent} from './malfunction/malfunction-close/malfunction-close.component';
import {MalfunctionAcceptEditComponent} from './malfunction/malfunction-accept-edit/malfunction-accept-edit.component';
import {MalfunctionAcceptViewComponent} from './malfunction/malfunction-accept-view/malfunction-accept-view.component';
import {MalfunctionCloseViewComponent} from './malfunction/malfunction-close-view/malfunction-close-view.component';
import {MalfunctionClosedViewComponent} from './malfunction/malfunction-closed-view/malfunction-closed-view.component';
import {MalfunctionSolvedViewComponent} from './malfunction/malfunction-solved-view/malfunction-solved-view.component';


const routes: Routes = [
    {path: '', component: MalfunctionComponent},
    {path: 'malfunctionBasalDatas', component: MalfunctionComponent},
    {path: 'malfunctionBase', component: MalfunctionBaseComponent, children: [
        {path: 'malfunctionOverview', component: MalfunctionOverviewComponent},
        {path: 'malfunctionAdd', component: MalfunctionAddComponent},
        {path: 'malfunctionChange', component: MalfunctionChangeComponent},
        {path: 'malfuntionDeal', component: MalfunctionDealComponent},
        {path: 'malfunctionDistribute', component: MalfunctionDistributeComponent},
        {path: 'malfunctionOff', component: MalfunctionOffComponent},
        {path: 'malfunctionReceive', component: MalfunctionReceiveComponent},
        {path: 'malfunctionSolve', component: MalfunctionSolveComponent},
        {path: 'malfunctionEdit', component: MalfunctionEditComponent},
        {path: 'malfunctionSolved', component: MalfunctionSolvedComponent},
        {path: 'malfunctionSolvedView', component: MalfunctionSolvedViewComponent},
        {path: 'malfunctionView', component: MalfunctionViewComponent},
        {path: 'malfunctionAccept', component: MalfunctionAcceptComponent},
        {path: 'malfunctionAcceptEdit', component: MalfunctionAcceptEditComponent},
        {path: 'malfunctionAcceptView', component: MalfunctionAcceptViewComponent},
        {path: 'malfunctionProcess', component: MalfunctionProcessComponent},
        {path: 'malfunctionUp', component: MalfunctionUpComponent},
        {path: 'malfunctionTurn', component: MalfunctionTurnComponent},
        {path: 'malfunctionClose', component: MalfunctionCloseComponent},
        {path: 'malfunctionCloseView', component: MalfunctionCloseViewComponent},
        {path: 'malfunctionClosedView', component: MalfunctionClosedViewComponent}
    ]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MaintenanceRoutingModule { }
