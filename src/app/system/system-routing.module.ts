import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PersonManageComponent} from './person-manage/person-manage.component';
import {JurManageComponent} from "./jur-manage/jur-manage.component";

const route: Routes = [
  {path: 'person', component: PersonManageComponent},
  {path: 'jur', component: JurManageComponent},
];
@NgModule({
  imports: [
    RouterModule.forChild(route),

  ],
  exports: [RouterModule]
})
export class SystemRoutingModule {

}
