import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonManageComponent } from './person-manage/person-manage.component';
import {SystemRoutingModule} from "./system-routing.module";
import {SystemService} from "./system.service";
import {ShareModule} from "../shared/share.module";
import { OrgEditComponent } from './org-edit/org-edit.component';
import { PersonEditComponent } from './person-edit/person-edit.component';
import { CognateRoleComponent } from './cognate-role/cognate-role.component';
import { PasswordModifyComponent } from './password-modify/password-modify.component';
import { JurManageComponent } from './jur-manage/jur-manage.component';
import { JurEditComponent } from './jur-edit/jur-edit.component';
import { RoleEditComponent } from './role-edit/role-edit.component';
import { JurLookComponent } from './jur-look/jur-look.component';
import {AddInspectionPlanTreeComponent} from "./add-inspection-plan-tree/add-inspection-plan-tree.component";

@NgModule({
  imports: [
    ShareModule,
    CommonModule,
    SystemRoutingModule
  ],
  declarations: [PersonManageComponent, OrgEditComponent, PersonEditComponent, CognateRoleComponent, PasswordModifyComponent, JurManageComponent, JurEditComponent, RoleEditComponent, JurLookComponent,AddInspectionPlanTreeComponent,
],
  providers: [SystemService]
})
export class SystemModule { }
