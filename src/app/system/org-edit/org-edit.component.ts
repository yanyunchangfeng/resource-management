import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SystemService} from "../system.service";
import {StorageService} from "../../services/storage.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-org-edit',
  templateUrl: './org-edit.component.html',
  styleUrls: ['./org-edit.component.css']
})
export class OrgEditComponent implements OnInit {
  display;
  title;
  @Output() closeOrgEdit =new EventEmitter();
  @Output() addOrg = new EventEmitter(); //新增组织节点
  @Output() updateOrg = new EventEmitter(); //修改组织节点
  org: FormGroup;
  @Input() currentOrg;
  @Input() state;
  submitAddOrg = {//新增组织节点参数
    "oid": '',
    "name": '',
    "remark": '',
    "father": '',
    "dep": '',
    "del": ""
  };
  submitUpdateOrg = [//修改组织节点参数
    {
      "oid": '',
      "name": '',
      "remark": '',
      "dep": '',
      "father": ''
    }
  ];
  constructor(
    private systemService: SystemService,
    private storageService: StorageService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService
  ) {
    this.org = fb.group({
        orgid: [''],
        orgName: ['', Validators.required],
        orgRemark: [''],
      }
    )
  }

  ngOnInit() {
    this.display = true;

    if(this.state ==='add') {
      this.getCurrentNodeData();
      this.title = '新增组织';

    }else {
        this.title = '修改组织';
        this.submitUpdateOrg[0].oid = this.currentOrg.oid;
        this.submitUpdateOrg[0].father = this.currentOrg.father;
        this.submitUpdateOrg[0].dep = this.currentOrg.dep;
    }
  }

  closeOrgEditMask(bool){
    this.closeOrgEdit.emit(bool);
  }

  //确定
  formSubmit(bool){
    if(this.state ==='add'){
      this.addOrgNode(bool);
    }else{
      this.updateOrgNode(bool);
    }
  }

  //新增组织节点前，获取节点信息
  getCurrentNodeData(){
    this.systemService.getCurrentNodeData(this.currentOrg.oid).subscribe(data=>{
      this.submitAddOrg.oid = data.oid;
      this.submitAddOrg.father = data.father;
      this.submitAddOrg.dep = data.dep;
      this.currentOrg.oid = data.oid;
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      // this.confirmationService.confirm({
      //   message: message,
      //   rejectVisible:false,
      // })
    })
  }

  //新增组织节点
  addOrgNode(bool){
    this.systemService.addOrgNode(this.submitAddOrg).subscribe(()=>{
      this.addOrg.emit(bool);
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      // this.confirmationService.confirm({
      //   message: message,
      //   rejectVisible:false,
      // })
    })
  }

  //修改组织节点
  updateOrgNode(bool){
    this.submitUpdateOrg[0].name = this.currentOrg.name;
    this.submitUpdateOrg[0].remark = this.currentOrg.remark;
    this.systemService.updateOrgNode(this.submitUpdateOrg).subscribe(()=>{
      this.updateOrg.emit(this.currentOrg);
      this.closeOrgEdit.emit(bool);
    })
  }

}
