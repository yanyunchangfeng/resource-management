import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CognateRoleComponent } from './cognate-role.component';

describe('CognateRoleComponent', () => {
  let component: CognateRoleComponent;
  let fixture: ComponentFixture<CognateRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CognateRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CognateRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
