import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JurManageComponent } from './jur-manage.component';

describe('JurManageComponent', () => {
  let component: JurManageComponent;
  let fixture: ComponentFixture<JurManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JurManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JurManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
