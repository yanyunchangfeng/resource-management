import { Component, OnInit } from '@angular/core';
import {Message, TreeNode} from "primeng/primeng";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SystemService} from "../system.service";
import {StorageService} from "../../services/storage.service";

@Component({
  selector: 'app-jur-manage',
  templateUrl: './jur-manage.component.html',
  styleUrls: ['./jur-manage.component.css']
})
export class JurManageComponent implements OnInit {
  orgs: TreeNode[];//组织树的数据
  selected: TreeNode;                     // 被选中的节点
  selectedNode: any = {};                     // 组织树被选中的节点
  selectedCol: any = {};                     // 人员表格中被选中的行
  selectedRid;                     // 人员表格中被选中的行
  lookRid;                     // 人员表格中被选中的行
  roleState;                      //复用遮罩层的状态,新增角色还是编辑角色
  jurState;                      //复用遮罩层的状态,编辑权限还是查看权限
  tableDatas: any;                   // 角色表格数据
  person: FormGroup;
  msgs: Message[];//提示信息
  canAdd;//新增按钮是否可用
  showJurEditMask;//是否显示修改权限弹框
  showJurLookMask;//是否显示查看权限弹框
  showRoleEditMask;//是否显示新增或修改角色弹框
  showRoleDeleteMask: boolean;//是否显示删除人员弹框
  deleteRid;//删除角色的rid
  deleteRoleTip;//删除角色弹框文字
  selectedRoles = [];//选中的角色
  // deletemsgs: Message[];//删除提示
  showCognateRoleMask: boolean;//是否显示关联角色弹框

  constructor(
    private systemService: SystemService,
    private storageService: StorageService,
    private fb: FormBuilder
  ) {
    this.person = fb.group({
        perName: ['', Validators.required],
        pid: ['', Validators.required],
        idcard: ['', Validators.required],
        organization: ['', Validators.required],
        post: [''],
      }
    )
  }

  ngOnInit() {
    //查询组织树
    this.queryOrgTree('');
    //查询人员表格数据
    this.queryJurList('');
    this.canAdd = true;
    this.showJurEditMask = false;
    this.showRoleEditMask = false;
    this.showCognateRoleMask = false;
  }

  //弹出新增或者修改角色弹框
  showRoleEdit(datas) {
    this.systemService.judgeRoleEditPower().subscribe(res => {
      if(res['errcode'] == '00000'){//有权限
        this.showRoleEditMask = true;
        if(!datas){//新增
          this.selectedNode = this.selected;
          this.roleState = 'add';
        }else{//修改
          // this.selectedCol = datas;
          for( var key in datas ){
            if(datas[key]){
              this.selectedNode[key] =datas[key]
            }else {
              this.selectedNode[key]=''
            }
          }
          // console.log(this.selectedNode)
          this.roleState = 'update';
        }
      }else {//没权限
        this.showError(res);
      }
    });
  }

  //显示删除角色弹框
  showRoleDelete(datas){
    this.systemService.judgeRoleEditPower().subscribe(res =>{//判断当前用户是否有删除角色权限
      // console.log(res['errcode']);
      if(res['errcode'] == '00000'){//有权限
        this.deleteRoleTip = '确认删除该角色？';
        this.deleteRid = [];
        if(datas){//表格里的删除按钮
          this.showRoleDeleteMask = true;
          this.deleteRid.push(datas.rid);
        }else {//表格外面的删除按钮
          if(!this.selectedRoles.length){//没有选中任何角色
            this.showWarn('请先选择删除项！');
          }else{
            this.showRoleDeleteMask = true;
            for(let i = 0;i < this.selectedRoles.length;i++){
              this.deleteRid.push(this.selectedRoles[i].rid);
            }
          }
        }
      }else {//没权限
        this.showError(res);
      }
    });
  }

  //删除角色
  deleteRole(){
    this.systemService.deleteRole(this.deleteRid).subscribe(res => {
      if(res == '00000'){
        this.selectedRoles = [];
        this.showSuccess();
        this.showRoleDeleteMask = false;
        this.queryJurList(this.selected['oid']);
      }
    })
  }

  //关闭新增或者修改角色弹框
  closeRoleEdit(bool){
    this.showRoleEditMask = bool;
  }

  //新增角色成功
  addRole(bool){
    this.showRoleEditMask = bool;
    this.queryJurList(this.selected['oid']);
  }

  //修改角色成功
  updateRole(bool){
    this.showRoleEditMask = bool;
    // console.log(this.selected['oid']);
    this.queryJurList(this.selected['oid']);
  }

  //编辑权限或者查看权限弹框
  showJurEdit(datas,bool){
    if(bool){//编辑权限
      this.systemService.judgeRoleEditPower().subscribe(res =>{//判断当前用户是否有编辑权限
        if(res['errcode'] == '00000'){//有权限
          this.showJurEditMask = true;
          this.selectedRid = datas;
          this.jurState = 'update';
        }else {//没权限
          this.showError(res);
        }
      });
    }else {//查看角色
      this.showJurLookMask = true;
      this.lookRid = datas;
    }
  }

  //关闭编辑权限弹框
  closeJurEdit(bool){
    this.showJurEditMask = bool;
  }

  //关闭查看权限弹框
  closeJurLook(bool){
    this.showJurLookMask = bool;
  }

  //编辑权限成功
  updateJur(bool){
    this.showJurEditMask = bool;
    // this.queryOrgTree('');
  }

  // 组织树懒加载
  nodeExpand(event) {
    if (event.node) {
      this.systemService.getOrgTree(event.node.oid).subscribe(data => {
        // console.log(data);
        event.node.children = data;
      })
    }
  }

  // 组织树选中
  NodeSelect(event) {
    this.canAdd = false;
    // console.log(event.node.oid);
    if (parseInt(event.node.dep) >= 1) {
      this.queryJurList(event.node.oid);
      /*this.titleName = event.node.label;
      this.nodeFather = event.node.did;
      this.nodeDep = event.node.dep;
      this.did = event.node.did;
      this.dep = event.node.dep;
      this.canAdd = false;
      this.publicService.getMalfunctionBasalDatas(event.node.did, event.node.dep).subscribe(res => {
        this.tableDatas = res;
      });*/
    }
  }

  //查询组织树数据
  queryOrgTree(oid){
    this.systemService.getOrgTree(oid).subscribe(data => {
      // console.log(data);
      this.orgs = data;
    })
  }

  //查询人员表格数据
  queryJurList(oid){
    this.systemService.getJurList(oid).subscribe(data => {
      // console.log(data);
      this.tableDatas = data;
    })
  }

  showWarn(operator: string) {
    this.msgs = [];
    // this.msgs.push({severity:'warn', summary:'注意', detail:'您没有' + operator + '权限！'});
    this.msgs.push({severity:'warn', summary:'注意', detail:operator });
  }

  //成功提示
  showSuccess() {
    /*this.deletemsgs = [];
    this.deletemsgs.push({severity:'success', summary:'消息提示', detail:'删除成功'});*/
  }

  //错误提示
  showError(res) {
    this.msgs = [];
    this.msgs.push({severity:'error', summary:'消息提示', detail:'删除失败'+res['errmsg']});
  }
}
