import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JurEditComponent } from './jur-edit.component';

describe('JurEditComponent', () => {
  let component: JurEditComponent;
  let fixture: ComponentFixture<JurEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JurEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JurEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
