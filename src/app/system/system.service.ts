import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../services/storage.service';
import {environment} from '../../environments/environment';
declare var $: any; //引用jQuery
@Injectable()
export class SystemService {

  constructor(private http: HttpClient, private storageService: StorageService) { }

  //获取组织树数据
  getOrgTree(oid){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      'access_token': token,
      'type': 'get_suborg',
      'dep': '1',
      'id': oid
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      //构造primeNG tree的数据
      let datas = res['datas'];
      for (let i = 0; i < datas.length; i++){
        datas[i].label = datas[i].name;
        datas[i].leaf = false;
      }
      return datas;
    });
  }

  //获取人员数据
  getPersonList(oid){
    let token = this.storageService.getToken('token');
    let oids = [];
    if (oid){
      oids.push(oid);
    }
    return this.http.post(`${environment.url.management}/personnel`, {
      'access_token': token,
      'type': 'get_personnel',
      'id': [],
      'oid': oids
    }).map((res: Response) => {
      if (res['errcode'] == '00001'){//没有人员
        return [];
      }else if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    });
  }

  //新增组织节点前，获取节点信息
  getCurrentNodeData(id){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      'access_token': token,
      'type': 'add_org_id',
      'id': id
      // "id": treeNode.oid
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    });
  }
  //  获取组织树
  getDepartmentDatas(oid?, dep?) {
    (!oid) && (oid = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/org`,
      {
        "access_token": token,
        "type": "get_suborg",
        "id": oid,
        "dep": dep
      }
    ).map((res: Response) => {
      // let body = res.json();
      // if( body.errcode !== '00000') {
      //   return [];
      // }else {
      //   body['datas'].forEach(function (e) {
      //     e.label = e.name;
      //     delete e.name;
      //     e.leaf = false;
      //     e.data = e.name;
      //   });
      // }
      // return body['datas'];
      if( res['errcode'] !== '00000') {
        return [];
      }else {
        res['datas'].forEach(function (e) {
          e.label = e.name;
          delete e.name;
          e.leaf = false;
          e.data = e.name;
        });
      }
      return res['datas'];
    })
  }

  //判断用户是否有操作组织节点的权限
  judgeOrgEditPower(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      'access_token': token,
      'type': 'jur'
    }).map((res: Response) => {
      // console.log(res);
      return res;
    });
  }

  //新增组织节点
  addOrgNode(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      'access_token': token,
      'type': 'add_org',
      'data': data
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

  //修改组织节点
  updateOrgNode(datas){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      'access_token': token,
      'type': 'mod_org',
      'datas': datas
      /*"datas": [{
        "oid": $scope.curModifyId,
        "name": $scope.orgName,
        "remark": $scope.orgRemark,
        "dep": $scope.dep,
        "father": $scope.father
      }]*/
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

  //删除组织节点
  deleteOrgNode(ids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      'access_token': token,
      'type': 'del_org',
      'ids': ids,
      'isdelperson': '0'
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['errcode'];
    });
  }

  //判断用户是否有操作人员的权限
  judgePerEditPower(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      'access_token': token,
      'type': 'jur'
    }).map((res: Response) => {
      return res;
    });
  }

  //新增人员
  addPerson(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      'access_token': token,
      'type': 'add_personnel',
      'data': data
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

  //修改人员
  updatePerson(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      'access_token': token,
      'type': 'mod_personnel',
      'data': data
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

  //删除人员
  deletePerson(pids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      'access_token': token,
      'type': 'del_personnel',
      'id': pids
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['errcode'];
    });
  }

  //判断用户是否有操作角色的权限
  judgeRoleEditPower(){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'jur'
    }).map((res: Response) => {
      return res;
    });
  }

  //查看用户有什么可选的角色
  getPerRoles(pid){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'get_per_role_save',
      'id': pid
      // "id": $scope.pid
    }).map((res: Response) => {
      if (res['errcode'] == '00001'){//没有可选的角色
        return [];
      }else if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

  //保存关联角色
  saveCognateRoles(rids, pid){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'save_per_role',
      'datas': rids,
      'id': pid
      // "id": $scope.pid
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res;
    });
  }

  //修改密码
  passwordModify(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      'access_token': token,
      'type': 'mod_pwd',
      'data': data
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res;
    });
  }

  //获取角色数据
  getJurList(oid){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'get_role',
      'id': oid
    }).map((res: Response) => {
      if (res['errcode'] == '00001'){//没有角色
        return [];
      }else if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

  //新增角色前获取角色id
  getRoleId(oid){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'add_role_id',
      'id': oid
    }).map((res: Response) => {
      if (res['errcode'] == '00001'){//没有角色
        return [];
      }else if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    });
  }

  //新增角色
  addRole(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'add_role',
      'data': data
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

  //修改角色
  updateRole(data){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'mod_role',
      'datas': data
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

  //删除角色
  deleteRole(rids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'del_role',
      'ids': rids
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['errcode'];
    });
  }

  //获取所有权限树的数据
  getAllJurTreeData(mid, dep){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'get_per_menu',
      'sub': mid,
      'dep': dep,
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      //构造primeNG tree的数据
      let datas = res['datas'];
      for (let i = 0; i < datas.length; i++){
        datas[i].label = datas[i].name;
        datas[i].leaf = false;
      }
      return datas;
    });
  }

  //获取角色已拥有的权限树的数据
  getOwnJursTreeData(mid, dep, rid){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'get_role_jur',
      'rid': rid,
      'sub': mid,
      'dep': dep,
    }).map((res: Response) => {
      if (res['errcode'] == '00001'){
        return [];
      }else if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      //构造primeNG tree的数据
      let datas = res['datas'];
      for (let i = 0; i < datas.length; i++){
        datas[i].label = datas[i].name;
        datas[i].leaf = false;
      }
      return datas;
    });
  }

  //修改权限
  updateJur(rid, jids){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/jur`, {
      'access_token': token,
      'type': 'save_role_jur',
      'rid': rid,
      'jids': jids
    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }

}
