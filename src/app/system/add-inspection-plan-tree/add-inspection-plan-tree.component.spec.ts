import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInspectionPlanTreeComponent } from './add-inspection-plan-tree.component';

describe('AddInspectionPlanTreeComponent', () => {
  let component: AddInspectionPlanTreeComponent;
  let fixture: ComponentFixture<AddInspectionPlanTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInspectionPlanTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInspectionPlanTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
