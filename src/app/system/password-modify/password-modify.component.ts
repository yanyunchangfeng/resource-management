import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SystemService} from "../system.service";
import {StorageService} from "../../services/storage.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-password-modify',
  templateUrl: './password-modify.component.html',
  styleUrls: ['./password-modify.component.css']
})
export class PasswordModifyComponent implements OnInit {
  display;
  @Output() closePasswordMod =new EventEmitter();
  @Output() passwordModify =new EventEmitter();
  @Input() currentNum;
  user: FormGroup;
  submitPwdMod = [{//修改密码参数
    "pid": '',
    "pwd": '',
  }];

  constructor(
    private systemService: SystemService,
    private storageService: StorageService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService
  ) {
    this.user = fb.group({
        pid: [''],
        pwd: ['', Validators.required],
      }
    )
  }

  ngOnInit() {
    this.display = true;
    this.submitPwdMod[0].pid = this.currentNum;
  }

  //确定
  formSubmit(bool){
    this.submitPwdMod[0].pwd = this.user.value.pwd;
    this.systemService.passwordModify(this.submitPwdMod).subscribe(()=>{
      this.passwordModify.emit(bool);
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }

  closePassModMask(bool){
    this.closePasswordMod.emit(bool);
  }

}
