import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TreeNode} from "primeng/primeng";
import {SystemService} from "../system.service";

@Component({
  selector: 'app-jur-look',
  templateUrl: './jur-look.component.html',
  styleUrls: ['./jur-look.component.css']
})
export class JurLookComponent implements OnInit {
  display;
  ownJurs: TreeNode[];//已拥有权限树的数据
  title;
  @Output() closeJurLook =new EventEmitter();
  @Input() currentOrg;

  constructor(
    private systemService: SystemService,
  ) { }

  ngOnInit() {
    this.display = true;
    this.title = '查看权限';
    //查询已拥有权限树的数据
    this.queryOwnJurTree('','', this.currentOrg.rid);
  }

  // 已拥有权限树,懒加载
  ownNodeExpand(event) {
    if (event.node) {
      this.systemService.getOwnJursTreeData(event.node.mid, event.node.dep, this.currentOrg.rid).subscribe(data => {
        event.node.children = data;
      })
    }
  }

  closeJurLookMask(bool){
    this.closeJurLook.emit(bool);
  }

  //查询已拥有权限树数据
  queryOwnJurTree(mid,dep,rid){
    this.systemService.getOwnJursTreeData(mid,dep,rid).subscribe(datas => {
      this.ownJurs = datas;
    })
  }

}
