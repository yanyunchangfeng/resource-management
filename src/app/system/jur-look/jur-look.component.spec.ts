import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JurLookComponent } from './jur-look.component';

describe('JurLookComponent', () => {
  let component: JurLookComponent;
  let fixture: ComponentFixture<JurLookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JurLookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JurLookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
