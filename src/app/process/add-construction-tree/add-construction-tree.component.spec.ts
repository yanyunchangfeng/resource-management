import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddConstructionTreeComponent } from './add-construction-tree.component';

describe('AddConstructionTreeComponent', () => {
  let component: AddConstructionTreeComponent;
  let fixture: ComponentFixture<AddConstructionTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddConstructionTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddConstructionTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
