import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseConstructionOrderComponent } from './close-construction-order.component';

describe('CloseConstructionOrderComponent', () => {
  let component: CloseConstructionOrderComponent;
  let fixture: ComponentFixture<CloseConstructionOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseConstructionOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseConstructionOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
