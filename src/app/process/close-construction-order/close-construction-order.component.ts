import { Component, OnInit } from '@angular/core';
import {environment} from "../../../environments/environment";
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ProcessService} from "../process.service";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-close-construction-order',
  templateUrl: './close-construction-order.component.html',
  styleUrls: ['./close-construction-order.component.scss']
})
export class CloseConstructionOrderComponent implements OnInit {

  constructionForm;
  fb: FormBuilder = new FormBuilder();
  tabs;
  cols;
  asscitedCols;
  OperationLogCols;
  sid;
  approvedModel;
  currentContruction =[];
  dataSource;
  devices;
  totalRecords;
  tabIndex = 0;
  radioButton = [] ;
  file;
  fileattachments;
  approvedProcess;
  operations;
  statusNames;

  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private processService: ProcessService,
    private eventBusService:EventBusService
  ) { }

  ngOnInit() {
    this.statusNames = [
      {label:'成功解决',value:'成功解决'},
      {label:'临时解决',value:'临时解决'},
      {label:'不成功',value:'不成功'},
      {label:'取消',value:'取消'},
    ]
    this.tabs = [
      {label:'基本信息'},
      {label:'审批信息'},
      {label:'操作日志'}
    ];
    this.cols = [
      {field: 'ano', header: '设备编号'},
      {field: 'name', header: '设备名称'},
      {field: 'modle', header: '设备型号'},
      {field: 'location', header: '设备位置'},
      {field: 'manager', header: '责任人'},
      {field: 'status', header: '设备状态'},
    ];
    //审批页面审批信息
    this.asscitedCols = [
      {field: 'approve_time', header: '审批时间'},
      {field: 'approver', header: '审批人'},
      {field: 'approve_status', header: '审批状态'},
      {field: 'approve_remarks', header: '审批意见'},
      {field: 'approver', header: '责任人'},
    ];
    //审批页面操作日志
    this.OperationLogCols = [
      {field: 'operate_time', header: '操作时间'},
      {field: 'operator', header: '操作人'},
      {field: 'operate_content', header: '操作内容'},
      {field: 'operate_description', header: '操作说明'},
    ];
    this.constructionForm = this.fb.group({
      "sid": [''],  //施工单号
      "status": [''],  //状态
      "creator":[''],    //受理人
      "create_time":[''],   //受理时间
      "submitter":[''],   //申请人
      "submit_time":[''], //申请时间
      "submitter_org":'',  //所属组织
      "construction_plantime_start":'', //计划开始时间
      "submitter_phone":'', //电话
      "construction_plantime_end":'', //计划结束时间
      "construction_type":'', //施工类型
      "org_approver":'', //部门审批人
      "is_affected":'', //影响业务
      "construction_affecttime_start":'', //影响时间
      "construct_location":[], //施工区域
      "affected_location":[], //影响区域
      "construction_content":'', //施工内容
      "construction_finish_description":'', //施工内容详述
      "construction_apply_attachments":[], //上传附件
      "construction_acttime_start":[], //实施开始时间
      "construction_acttime_end":[], //预计结束时间
      "constructor_unit":[], //实施单位
      "constructor_per":[], //实施人员
      "construction_description":[], //施工结果描述
      "reviewor":[], //复核人
      "review_time":[], //复核时间
      "review_remarks":[], //复核说明
      "close_code":[], //结束代码
      "close_time":[], //结束时间
    });
    this.approvedModel = {
      "sid": "",
      "close_code": this.statusNames[0]['value'],
      "close_time": this.processService.getFormatTime(),
    };
    this.radioButton = [
      {label:'影响业务',value:true},
      {label:'不影响业务',value:false}
    ];
    this.queryConstructionApproved();
  }
  queryConstructionApproved(){
    this.route.queryParams.subscribe(parms=>{
      if(parms['sid']){
        this.sid =parms['sid'];
      }
      this.approvedModel.sid = this.sid;
      this.processService.constructionShow(this.sid).subscribe(data=>{
        this.currentContruction = data;
        this.approvedProcess = this.currentContruction['approval_process'];
        this.operations = this.currentContruction['operations'];

        console.log(this.currentContruction)
        this.dataSource = this.currentContruction['devices'];
        this.radioButton['label'] = this.currentContruction['is_affected'];
        this.file = this.currentContruction['construction_apply_attachments'];
        this.fileattachments = this.currentContruction['construction_finish_attachments'];
        this.devices = this.dataSource.slice(0,10);
        this.totalRecords = this.dataSource.length;
      })
    });
  }
  closeapprovedOption(status){
    this.approvedModel.status = status;
    this.currentContruction['status'] = '';
    this.eventBusService.flow.next(this.currentContruction['status']);
    this.processService.closeConstruction(this.approvedModel).subscribe(() =>{
      this.router.navigate(['../constructionoverview'],{relativeTo:this.route});
    })
  }
  changeTab(index){
    this.tabIndex = index;
  }
  goBack() {
    let sid =  this.currentContruction['sid'];
    this.currentContruction['status'] = "";
    this.eventBusService.flow.next(this.currentContruction['status']);
    this.router.navigate(['../constructionoverview'],{queryParams:{sid:sid,state:'viewDetail',title:'施工总览'},relativeTo:this.route})
  }
  download() {
    window.open(`${environment.url.management}/${this.file[0].path}`, '_blank');

  }
  downloadfinish() {
    window.open(`${environment.url.management}/${this.fileattachments[0].path}`, '_blank');
    // window.open(`${environment.url.management}/data/file/workflow/construction/报障管理技术设计文档(1).doc_1522139592`, '_blank');

  }

}
