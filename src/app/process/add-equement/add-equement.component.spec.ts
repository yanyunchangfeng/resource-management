import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEquementComponent } from './add-equement.component';

describe('AddEquementComponent', () => {
  let component: AddEquementComponent;
  let fixture: ComponentFixture<AddEquementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEquementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEquementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
