import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProcessService} from "../process.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-add-equement',
  templateUrl: './add-equement.component.html',
  styleUrls: ['./add-equement.component.css']
})
export class AddEquementComponent implements OnInit {
  display: boolean = false;
  @Output() closeEquement = new EventEmitter;
  cols;
  width;
  dataSource;
  totalRecords;
  assets;
  @Output() addDev = new EventEmitter();
  selectMaterial = [];
  queryModel;
  chooseCids = [];
  ConstructionData;
  @Input() equip :String;
  constructor(
    private processService: ProcessService,
    private comfirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.display = true;
    if(window.innerWidth<1024){
      this.width = window.innerWidth * 0.7;
    }else{
      this.width = window.innerWidth * 0.6;
    }
    this.cols = [
      {field: 'ano', header: '设备编号'},
      {field: 'name', header: '设备名称'},
      {field: 'asset_type', header: '设备类型'},
      {field: 'modle', header: '设备型号'},
      {field: 'location', header: '设备位置'},
      {field: 'manager', header: '责任人'},
      {field: 'status', header: '设备状态'},

    ];
    this.queryModel = {
      "condition": {
        "ano": "",
        "name": "",
        "status": "",
        "on_line_date_start": "",
        "on_line_date_end": "",
        "room": this.equip,
        "filter_anos": ""
      },
      "page": {
        "page_size": "10",
        "page_number": "1"
      }
    };
    this.queryAssets();

  }
  closeEquementMask(bool){
    this.closeEquement.emit(bool)
  }
  clearSearch() {
    this.queryModel.condition.name = '';
    this.queryModel.condition.ano = '';
    this.queryModel.condition.status = '';

  }

  //搜索功能
  suggestInsepectiones() {
    this.queryModel.page.page_number = '1'
    this.queryModel.page.page_size = '10';
    this.queryAssets()
  }

  //分页查询
  paginate(event) {
    this.chooseCids = []
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryAssets();
  }
  queryAssets() {
    this.processService.getAssets(this.queryModel).subscribe(data => {
      if(!data['items']){
        this.ConstructionData = [];
        this.selectMaterial =[];
        this.totalRecords = 0;
      }else{
        this.ConstructionData = data.items;
        this.totalRecords = data.page.total;
      }

    });
  }
  formSubmit(bool){
    this.addDev.emit(this.selectMaterial);
    // this.closeEquementMask(false);
  }
}
