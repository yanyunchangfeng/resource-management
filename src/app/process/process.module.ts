import { NgModule } from '@angular/core';
import { ConstructionFlowChartComponent } from './construction-flow-chart/construction-flow-chart.component';
import {ShareModule} from "../shared/share.module";
import {ProcessRoutingModule} from "./process-routing.module";
import {ProcessService} from "./process.service";
import {ConstructionApplicationComponent} from "./construction-application/construction-application.component";
import { ConstructionOverviewComponent } from './construction-overview/construction-overview.component';
import { ConstructionApprovedComponent } from './construction-approved/construction-approved.component';
import { AddEquementComponent } from './add-equement/add-equement.component';
import { PersonalDialogComponent } from './personal-dialog/personal-dialog.component';
import {PublicService} from "../services/public.service";
import { ConstructionViewDetailComponent } from './construction-view-detail/construction-view-detail.component';
import { ConstructionComponent } from './construction/construction.component';
import { ConstructionReviewComponent } from './construction-review/construction-review.component';
import { ConstructionCompletedComponent } from './construction-completed/construction-completed.component';
import { ConstructionLocationComponent } from './construction-location/construction-location.component';
import { CloseConstructionOrderComponent } from './close-construction-order/close-construction-order.component';
import { AddConstructionTreeComponent } from './add-construction-tree/add-construction-tree.component';
import { ConstructionRectificationComponent } from './construction-rectification/construction-rectification.component';
import { ConstructionSettingComponent } from './construction-setting/construction-setting.component';
import { WaitForHandleComponent } from './wait-for-handle/wait-for-handle.component';
import { AffectLocationComponent } from './affect-location/affect-location.component';
import { HandleFlowCharComponent } from './handle-flow-char/handle-flow-char.component';

@NgModule({
  imports: [
    ShareModule,
    ProcessRoutingModule,
  ],
  declarations: [
    ConstructionApplicationComponent,
    ConstructionFlowChartComponent,
    ConstructionOverviewComponent,
    ConstructionApprovedComponent,
    AddEquementComponent,
    PersonalDialogComponent,
    ConstructionViewDetailComponent,
    ConstructionComponent,
    ConstructionReviewComponent,
    ConstructionCompletedComponent,
    ConstructionLocationComponent,
    CloseConstructionOrderComponent,
    AddConstructionTreeComponent,
    ConstructionRectificationComponent,
    ConstructionSettingComponent,
    WaitForHandleComponent,
    AffectLocationComponent,
    HandleFlowCharComponent,

  ],
  providers:[ ProcessService,PublicService]

})
export class ProcessModule { }
