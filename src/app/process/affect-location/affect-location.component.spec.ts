import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectLocationComponent } from './affect-location.component';

describe('AffectLocationComponent', () => {
  let component: AffectLocationComponent;
  let fixture: ComponentFixture<AffectLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
