import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionViewDetailComponent } from './construction-view-detail.component';

describe('ConstructionViewDetailComponent', () => {
  let component: ConstructionViewDetailComponent;
  let fixture: ComponentFixture<ConstructionViewDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionViewDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
