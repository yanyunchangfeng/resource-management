import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ProcessService} from "../process.service";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "../../../environments/environment";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-construction-view-detail',
  templateUrl: './construction-view-detail.component.html',
  styleUrls: ['./construction-view-detail.component.scss']
})
export class ConstructionViewDetailComponent implements OnInit {
  constructionForm;
  fb: FormBuilder = new FormBuilder();
  tabs;
  cols;
  asscitedCols;
  OperationLogCols;
  sid;
  approvedModel;
  currentContruction =[];
  dataSource;
  devices;
  totalRecords;
  tabIndex = 0;
  radioButton = [] ;
  file;
  fileattachments;
  approvedProcess;
  operations;
  affected_location = [];
  location = [];

  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private processService: ProcessService,
    private eventBusService:EventBusService
  ) { }

  ngOnInit() {
    this.tabs = [
      {label:'基本信息'},
      {label:'审批信息'},
      {label:'操作日志'}
    ];
    this.cols = [
      {field: 'ano', header: '设备编号'},
      {field: 'name', header: '设备名称'},
      {field: 'modle', header: '设备型号'},
      {field: 'location', header: '设备位置'},
      {field: 'manager', header: '责任人'},
      {field: 'status', header: '设备状态'},
    ];
    //审批页面审批信息
    this.asscitedCols = [
      {field: 'approve_time', header: '审批时间'},
      {field: 'approver', header: '审批人'},
      {field: 'approve_status', header: '审批状态'},
      {field: 'approve_remarks', header: '审批意见'},
      {field: 'approver', header: '责任人'},
    ];
    //审批页面操作日志
    this.OperationLogCols = [
      {field: 'operate_time', header: '操作时间'},
      {field: 'operator', header: '操作人'},
      {field: 'operate_content', header: '操作内容'},
      {field: 'operate_description', header: '操作说明'},
    ];
    this.constructionForm = this.fb.group({

      "sid": [''],  //施工单号
      "status": [''],  //状态
      "creator":[''],    //受理人
      "create_time":[''],   //受理时间
      "submitter":[''],   //申请人
      "submit_time":[''], //申请时间
      "submitter_org":'',  //所属组织
      "construction_plantime_start":'', //计划开始时间
      "submitter_phone":'', //电话
      "construction_plantime_end":'', //计划结束时间
      "construction_type":'', //施工类型
      "org_approver":'', //部门审批人
      "is_affected":'', //影响业务
      "construction_affecttime_start":'', //影响时间
      "construction_affecttime_end":'', //影响时间
      "construct_location":[], //施工区域
      "affected_location":[], //影响区域
      "construction_content":'', //施工内容
      "construction_description":'', //施工内容详述
      "construction_apply_attachments":[], //上传附件
      "construction_acttime_start":[], //实施开始时间
      "construction_acttime_end":[], //预计结束时间
      "constructor_unit":[], //实施单位
      "constructor_per":[], //实施人员
      "construction_finish_description":[], //施工结果描述
      "construction_finish_attachments":[], //施工完工附件
      "approve_opinion":['',Validators.required], //审批意见
      "next_approver_org":[], //下一个审批人
      "review_remarks":[], //复核说明
      "close_code":[], //关闭代码
      "close_time":[], //关闭时间
    });
    this.approvedModel = {
      "sid": "",
      "approve_opinion": "",
      "status": "",
      "next_approver": "",
      "next_approver_pid": "",
      "next_approver_org": "",
      "next_approver_org_oid": ""
    };
    this.radioButton = [
      {label:'影响业务',value:true},
      {label:'不影响业务',value:false}
    ];
    this.queryConstructionApproved();
  }
  queryConstructionApproved(){
    this.route.queryParams.subscribe(parms=>{
      if(parms['sid']){
        this.sid =parms['sid'];
      }
      this.approvedModel.sid = this.sid;
      this.processService.constructionShow(this.sid).subscribe(data=>{
        this.currentContruction = data;
        this.currentContruction['construct_location'].map((item,index)=>{
          this.location.push(item.label);
          this.currentContruction['construct_location'] = this.location
        })
        this.currentContruction['affected_location'].map((item,index)=>{
          this.affected_location.push(item.label);
          this.currentContruction['affected_location'] = this.affected_location
        })
        this.approvedProcess = this.currentContruction['approval_process'];
        this.operations = this.currentContruction['operations'];
        this.dataSource = this.currentContruction['devices'];
        this.radioButton['value'] = this.currentContruction['is_affected'];
        this.file = this.currentContruction['construction_apply_attachments'];
        this.fileattachments = this.currentContruction['construction_finish_attachments'];
        if(!this.dataSource){
          this.dataSource = [];
          this.totalRecords = 0;
        }
        this.totalRecords = this.dataSource.length;
        this.devices = this.dataSource.slice(0,10);
      })
    });
  }

  changeTab(index){
    this.tabIndex = index;
  }
  goBack() {
    let sid =  this.currentContruction['sid'];
    this.currentContruction['status'] = "";
    this.eventBusService.flow.next(this.currentContruction['status']);
    this.router.navigate(['../constructionoverview'],{queryParams:{sid:sid,state:'viewDetail',title:'施工总览'},relativeTo:this.route})
  }
  download() {
    window.open(`${environment.url.management}/${this.file[0].path}`, '_blank');
  }
  downloadfinish() {
    window.open(`${environment.url.management}/${this.fileattachments[0].path}`, '_blank');

  }
}
