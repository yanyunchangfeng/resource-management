import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionOverviewComponent } from './construction-overview.component';

describe('ConstructionOverviewComponent', () => {
  let component: ConstructionOverviewComponent;
  let fixture: ComponentFixture<ConstructionOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
