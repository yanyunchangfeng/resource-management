import { Component, OnInit } from '@angular/core';
import {ProcessService} from "../process.service";
import {ConfirmationService} from "primeng/primeng";
import {ActivatedRoute, Router} from "@angular/router";
import {EventBusService} from "../../services/event-bus.service";
import {MessageService} from "primeng/components/common/messageservice";
import {BasePage} from "../../base.page";
@Component({
  selector: 'app-construction-overview',
  templateUrl: './construction-overview.component.html',
  styleUrls: ['./construction-overview.component.scss']
})
export class ConstructionOverviewComponent extends BasePage implements OnInit {
  cols;                     //定义表格表头
  queryModel;
  ConstructionData=[];
  totalRecords;
  selectMaterial = [];
  chooseCids = [];
  statusNames = [];
  statusCategory = [];
  filesTree4;
  categories;
  zh;
  ConstructionStatusData;
  viewState;
  constructor(
    private processService: ProcessService,
    public confirmationService: ConfirmationService,
    public messageService:MessageService,
    private eventBusService:EventBusService,
    private router:Router,
    private route:ActivatedRoute,

  ) {
    super(confirmationService,messageService)
  }

  ngOnInit() {

    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.statusNames = [
      {label: '', value: ''},
      {label: '在库', value: '在库'},
      {label: '已完成', value: '已完成'},
    ]
    this.statusCategory = [
      {label: '', value: ''},
      {label: '整机', value: '整机'},
      {label: '部件', value: '部件'},
      {label: '耗材', value: '耗材'},
      {label: '其他', value: '其他'},
    ]
    this.cols = [
      {field: 'submitter', header: '申请人'},
      {field: 'submitter_org', header: '部门'},
      {field: 'create_time', header: '申请时间'},
      {field: 'owner', header: '当前处理人'},
      {field: 'status', header: '状态'},
      // {field: 'construct_location', header: '施工区域'},
    ];
    this.queryModel = {
      "condition": {
        "sid": "",
        "submitter":"",
        "begin_time": "",
        "end_time": "",
        "status":"",
      },
      "page": {
        "page_size": "10",
        "page_number": "1"
      }
    };
    this.eventBusService.updateconstruction.subscribe(
      data=>{
        this.queryModel.condition.status = data;
        this.queryConstructionDate();
      }
    )
    this.queryConstructionDate();
    this.queryConstructionStatusDate();
  }
  clearSearch() {
    this.queryModel.condition.name = '';
    this.queryModel.condition.sid = '';
    this.queryModel.condition.begin_time = '';
    this.queryModel.condition.end_time = '';
  }

  //搜索功能
  suggestInsepectiones() {
    this.queryModel.page.page_number = '1'
    this.queryModel.page.page_size = '10';
    this.queryConstructionDate()
  }

  //分页查询
  paginate(event) {
    this.chooseCids = []
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryConstructionDate();
  }
  queryConstructionDate() {
    this.processService.queryProcess(this.queryModel).subscribe(data => {
      if(!data['items']){
        this.ConstructionData = [];
        this.selectMaterial =[];
        this.totalRecords = 0;
      }else{
        this.ConstructionData = data.items;
        for(let  i = 0 ;i<this.ConstructionData.length;i++){
          let temp = this.ConstructionData[i];
          temp['timeGroup'] = temp['construction_acttime_start']+'~'+temp['construction_acttime_end'];
        }
        this.totalRecords = data.page.total;
      }

    });
  }

  queryConstructionStatusDate(){
    this.processService.queryConstructionStatus().subscribe(data=>{
      if(!data){
        this.ConstructionStatusData = [];
      }else{
        this.ConstructionStatusData = data;
        this.ConstructionStatusData = this.processService.formatDropdownData(this.ConstructionStatusData)
      }
    });
  }
  deleteoption(current){
    //请求成功后删除本地数据
    this.chooseCids = [];
    this.chooseCids.push(current['sid']);
    this.confirmationService.confirm({
      message:'确认删除吗？',
      accept: () => {
        this.processService.deleteConstruction(this.chooseCids).subscribe(()=>{
          this.queryConstructionDate();
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })

      },
      reject:() =>{
      }
    });
  }
  updateOption(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../constructionapplication'],{queryParams:{'sid':sid,title:'编辑',state:'update'},relativeTo:this.route})
  }
  approvedOption(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../constructionapproved'],{queryParams:{'sid':sid,title:'审批'},relativeTo:this.route})
  }
  //施工
  constructionOption(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../construction'],{queryParams:{'sid':sid,title:'施工'},relativeTo:this.route})
  }
  //施工完成
  constructionComplate(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../constructioncompleted'],{queryParams:{'sid':sid,title:'施工完成'},relativeTo:this.route})
  }
  //施工复核
  constructionReview(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../constructionreview'],{queryParams:{'sid':sid,title:'施工复核'},relativeTo:this.route})
  }
  //施工整改
  constructionRectification(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../constructionrectification'],{queryParams:{'sid':sid,title:'施工整改'},relativeTo:this.route})
  }
  //关闭施工单
  closeconstruction(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../closeconstructionorder'],{queryParams:{'sid':sid,title:'关闭施工单'},relativeTo:this.route})
  }
  //查看
  viewContructionApproved(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../constructionviewdetail'],{queryParams:{sid:sid,state:'viewDetail',title:'查看'},relativeTo:this.route})
  }


}
