import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitForHandleComponent } from './wait-for-handle.component';

describe('WaitForHandleComponent', () => {
  let component: WaitForHandleComponent;
  let fixture: ComponentFixture<WaitForHandleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitForHandleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitForHandleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
