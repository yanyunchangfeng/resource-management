import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionRectificationComponent } from './construction-rectification.component';

describe('ConstructionRectificationComponent', () => {
  let component: ConstructionRectificationComponent;
  let fixture: ComponentFixture<ConstructionRectificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionRectificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionRectificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
