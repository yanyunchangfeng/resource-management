import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EventBusService} from "../../services/event-bus.service";
import {ProcessService} from "../process.service";

@Component({
  selector: 'app-handle-flow-char',
  templateUrl: './handle-flow-char.component.html',
  styleUrls: ['./handle-flow-char.component.scss']
})
export class HandleFlowCharComponent implements OnInit {
  flowCharts;
  childrenflowCharts;
  title = '待我处理';
  queryModel;
  ConstrucionCharData ;
  constructor(
    public router:Router,
    public route:ActivatedRoute,
    public eventBus:EventBusService,
    private processService: ProcessService
  ) { }

  ngOnInit() {
    this.queryModel={
      "status": "",
    };

    this.flowCharts = [
      {label:'开始',route:'',class:'cursor',stroke:"black",fill:'#ffffff',active:true,x:'0',y:'0',rx:'10',width:100,height:40,title:'开始',text:{x:'34',y:'24',label:'开始'},g:[{x1:'50',y1:'40',x2:'50',y2:'70',},{x1:'50',y1:'70',x2:'45',y2:'64'},{x1:'50',y1:'70',x2:'55',y2:'64'}]},
      {label:'施工申请',route:'constructionapplication',stroke:"black",fill:'#ffffff',active:false,x:'0',y:'70',width:100,height:40,title:'施工申请',text:{x:'20',y:'94',label:'施工申请'},g:[{x1:'50',y1:'140',x2:'50',y2:'110'},{x1:'50',y1:'140',x2:'45',y2:'134'},{x1:'50',y1:'140',x2:'55',y2:'134'}]},
      {label:'审批',route:'constructionoverview',status:"待审批",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'140',width:100,height:40,title:'施工审批',text:{x:'34',y:'164',label:'施工审批'},g:[{x1:'50',y1:'210',x2:'50',y2:'180'},{x1:'50',y1:'210',x2:'45',y2:'204'},{x1:'50',y1:'210',x2:'55',y2:'204'}]},
      {label:'标注影响设备',class:'stroke-dasharray', route:'',stroke:"black",fill:'#ffffff',active:true,x:'0',y:'210',width:100,height:40,title:'标注影响设备',text:{x:'8',y:'234',label:'标注影响设备'},g:[{x1:'50',y1:'280',x2:'50',y2:'250'},{x1:'50',y1:'280',x2:'45',y2:'274'},{x1:'50',y1:'280',x2:'55',y2:'274'}]},
      {label:'施工',route:'constructionoverview',status:"待施工",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'280',width:100,height:40,title:'施工',text:{x:'34',y:'304',label:'施工'},g:[{x1:'50',y1:'350',x2:'50',y2:'320'},{x1:'50',y1:'350',x2:'45',y2:'344'},{x1:'50',y1:'350',x2:'55',y2:'344'}]},
      {label:'施工完成',route:'constructionoverview',status:"施工中",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'350',width:100,height:40,title:'施工完成',text:{x:'20',y:'374',label:'施工完成'},g:[{x1:'50',y1:'420',x2:'50',y2:'390'},{x1:'50',y1:'420',x2:'45',y2:'414'},{x1:'50',y1:'420',x2:'55',y2:'414'}]},
      {label:'施工复核',route:'constructionoverview',status:"待复核",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'420',width:100,height:40,title:'施工复核',text:{x:'20',y:'444',label:'施工复核'},g:[{x1:'50',y1:'490',x2:'50',y2:'460'},{x1:'50',y1:'490',x2:'45',y2:'484'},{x1:'50',y1:'490',x2:'55',y2:'484'}]},
      {label:'施工整改',route:'',class:'stroke-dasharray',stroke:"black",fill:'ffffff',active:true,x:'0',y:'490',width:100,height:40,title:'更新设备状态',text:{x:'20',y:'514',label:'更新设备状态'},g:[{x1:'50',y1:'560',x2:'50',y2:'530'},{x1:'50',y1:'560',x2:'45',y2:'554'},{x1:'50',y1:'560',x2:'55',y2:'554'}]},
      {label:'更新设备状态',route:'constructionoverview',class:'stroke-dasharray',status:"施工整改",stroke:"black",fill:'#ffffff',active:true,x:'0',y:'560',width:100,height:40,title:'施工结束',text:{x:'8',y:'584',label:'施工结束'},g:[{x1:'50',y1:'630',x2:'50',y2:'600'},{x1:'50',y1:'630',x2:'45',y2:'624'},{x1:'50',y1:'630',x2:'55',y2:'624'}]},
      {label:'关闭施工单',route:'',stroke:"black",fill:'#ffffff',x:'0',y:'630',width:100,height:40,title:'',text:{x:'15',y:'654',label:'关闭'},g:[{x1:'50',y1:'700',x2:'50',y2:'670'},{x1:'50',y1:'700',x2:'45',y2:'694'},{x1:'50',y1:'700',x2:'55',y2:'694'}]},
      {label:'结束',route:'',stroke:"black",fill:'#ffffff',x:'0',y:'700',rx:'10',width:100,height:40,title:'',text:{x:'30',y:'724',label:'结束'}}
    ];
    this.childrenflowCharts = [
      {label:'',route:'',stroke:"black",fill:'#ffffff',active:true,x:'0',y:'0',title:'开始',text:{x:'30',y:'24',label:'1'}},
      {label:'',route:'',stroke:"black",fill:'#ffffff',active:false,x:'65',y:'70',width:35,height:12,title:'施工申请',text:{x:'80',y:'82',label:'2'}},
      {label:'3',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'140',width:35,height:12,title:'上架审批',text:{x:'80',y:'152',label:'1'}},
      {label:'', route:'',stroke:"black",fill:'#ffffff',active:true,x:'0',y:'210',title:'标注影响设备',text:{x:'8',y:'234',label:'标注影响设备'}},
      {label:'5',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'280',width:35,height:12,title:'信息复核',text:{x:'80',y:'292',label:'2'}},
      {label:'6',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'350',width:35,height:12,title:'完成上架',text:{x:'80',y:'362',label:'3'}},
      {label:'7',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'420',width:35,height:12,title:'施工复核',text:{x:'80',y:'432',label:'4'}},
      {label:'',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'490',width:35,height:12,title:'更新设备状态',text:{x:'80',y:'502',label:'更新设备状态'}},
      {label:'',route:'',stroke:"black",fill:'#ffffff',active:true,x:'65',y:'560',title:'施工复核',text:{x:'80',y:'572',label:'5'}},
      {label:'10',route:'',stroke:"black",fill:'#ffffff',x:'65',y:'630',width:35,height:12,title:'',text:{x:'80',y:'642',label:'10'}},
      {label:'',route:'',stroke:"black",fill:'#ffffff',x:'0',y:'700',title:'',text:{x:'30',y:'724',label:'11'}}

    ]
    this.eventBus.flow.subscribe(data=>{
      console.log(data)
      this.queryModel.status = data;
      this.queryConstructionDate();
    })
    this.route.queryParams.subscribe(params=>{
      if(params['title']){
        this.title = params['title'];
      }
    });
    this.queryConstructionDate();
  }
  queryConstructionDate(){
    this.processService.queryConstrucionChar(this.queryModel).subscribe(data=>{
      if(!this.ConstrucionCharData){
        this.ConstrucionCharData = [];
      }
      this.ConstrucionCharData = data;
      for(let i = 0;i<this.ConstrucionCharData.length;i++){
        let temp = this.ConstrucionCharData[i];
        this.flowCharts[i]['fill'] = temp['fill'] ;
        // this.childrenflowCharts[i]['label'] = temp['record'];
      }
      for(let i = 0;i<this.ConstrucionCharData.length;i++){
        let temp = this.ConstrucionCharData[i];
        this.childrenflowCharts[i]['label'] = temp['record'];
      }
    });
  }
  openPage(flow,index){
    if(index+1 === this.flowCharts.length)return
    for(let i = 0;i<this.flowCharts.length;i++){
      this.flowCharts[i].active = false;
    }
    this.flowCharts[index].active = true;
    this.queryConstructionDate();
    if(flow.status){
      this.eventBus.updateconstruction.next(flow.status)
    }
    if(flow.status){
      this.eventBus.updateconstruction.next(this.queryModel)
    }
    if(flow.route){
      this.router.navigate([flow.route],{relativeTo:this.route});
      this.title = flow.title;
    }
  }
}
