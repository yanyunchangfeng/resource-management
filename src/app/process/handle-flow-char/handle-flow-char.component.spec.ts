import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleFlowCharComponent } from './handle-flow-char.component';

describe('HandleFlowCharComponent', () => {
  let component: HandleFlowCharComponent;
  let fixture: ComponentFixture<HandleFlowCharComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandleFlowCharComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleFlowCharComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
