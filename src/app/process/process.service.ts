import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {StorageService} from "../services/storage.service";
import {environment} from "../../environments/environment";

@Injectable()
export class ProcessService {

  constructor( private http:HttpClient,
               private storageService: StorageService) { }
  searchChange(queryModel){
    return this.http.post(`${environment.url.management}/workflow`, {
      'access_token': '',
      'type': 'asset_diffield_get',
      'data': {
        'field': queryModel.field,
        'value': queryModel.value
      }


    }).map((res: Response) => {
      if (res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    });
  }
  //获取当前登录人接口
  queryPersonal(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`,{
      "access_token":token,
      "type":"get_personnel_user",
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }


  //查询资产
  getAssets(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/asset`,{
      "access_token":token,
      "type":"workflow_asset_get",
      "data": {
        "condition":{
          "ano":  material.condition.ano.trim(),
          "name":  material.condition.name.trim(),
          "status":  material.condition.status.trim(),
          "on_line_date_start":  material.condition.on_line_date_start.trim(),
          "on_line_date_end":  material.condition.on_line_date_end.trim(),
          "room": material.condition.room.trim(),
          "filter_anos":  material.condition.filter_anos.trim(),
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
//施工总览流程图接口
  queryConstrucionChar(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_workflow_get",
      "data": {
        "status": material.status.trim(),
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //施工类型查询接口
  queryConstructionInfo(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/foundationdata`,{
      "access_token":token,
      "type":"construction_config_get",
      "ids": [
        "施工类型",
      ]

    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }
  //施工总览获取接口
  queryProcess(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_get",
      "data": {
        "condition":{
          "sid": material.condition.sid.trim(),
          "submitter": material.condition.submitter.trim(),
          "begin_time": material.condition.begin_time.trim(),
          "end_time": material.condition.end_time.trim(),
          "status": material.condition.status.trim(),
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //待我处理总览获取接口
  queryProcessHandle(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_get_byowner",
      "data": {
        "condition":{
          "status": material.condition.status.trim(),
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //施工总览状态获取接口
  queryConstructionStatus(){
  let token =this.storageService.getToken('token');
  return this.http.post(`${environment.url.management}/workflow`,{
  "access_token":token,
  "type":"construction_statuslist_get",
  }).map((res:Response)=>{

  if(res['errcode']!== '00000'){
  throw new Error(res['errmsg'])
  }
  return res['datas'];
  })
  }
  //施工单新增接口
  addConstructionOrder(construction){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "construction_add",
      "data": {
        "sid":construction.sid,
        "status": construction.status,
        "creator": construction.creator,
        "create_time": construction.create_time,
        "submitter": construction.submitter,
        "submitter_pid": construction.submitter_pid,
        "submit_time": construction.submit_time,
        "submitter_org": construction.submitter_org,
        "submitter_org_oid": construction.submitter_org_oid,
        "construction_plantime_start": construction.construction_plantime_start,
        "submitter_phone": construction.submitter_phone,
        "construction_plantime_end": construction.construction_plantime_end,
        "construction_types": construction.construction_types,
        "org_approver": construction.org_approver,
        "org_approver_pid": construction.org_approver_pid,
        "is_affected":construction.is_affected,
        "construction_affecttime_start": construction.construction_affecttime_start,
        "construction_affecttime_end": construction.construction_affecttime_end,
        "construct_location": construction.construct_location,
        "affected_location":construction.affectLocation,
        "construction_content":construction.construction_content,
        "construction_description":construction.construction_description,
        "construction_apply_attachments": construction.construction_apply_attachments,
        "devices": construction.devices,
      }
    }).map((res: Response) => {

      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //施工保存接口
  queryConstructionSave(construction){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token": token,
      "type": "construction_save",
      "data": {
        "status": construction.status,
        "creator": construction.creator,
        "create_time": construction.create_time,
        "submitter": construction.submitter,
        "submit_time": construction.submit_time,
        "submitter_org": construction.submitter_org,
        "construction_plantime_start": construction.construction_plantime_start,
        "construction_plantime_end": construction.construction_plantime_end,
        "submitter_phone": construction.submitter_phone,
        "construction_time_end": construction.construction_time_end,
        "construction_types": construction.construction_types,
        "org_approver": construction.org_approver,
        "org_approver_pid": construction.org_approver_pid,
        "is_affected": construction.is_affected,
        "construction_affecttime_start": construction.construction_affecttime_start,
        "construction_affecttime_end": construction.construction_affecttime_end,
        "construct_location": construction.construct_location,
        "affected_location":construction.affected_location,
        "construction_content":construction.construction_content,
        "construction_description":construction.construction_description,
        "construction_apply_attachments": construction.construction_apply_attachments,
        "devices": construction.devices,
      }
    }).map((res: Response) => {

      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //施工申请修改接口
  queryConstructionMod(construction){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_mod",
      "data": {
        "sid":construction.sid,
        "status": construction.status,
        "creator": construction.creator,
        "create_time": construction.create_time,
        "submitter": construction.submitter,
        "submit_time": construction.submit_time,
        "submitter_org": construction.submitter_org,
        "construction_plantime_start": construction.construction_plantime_start,
        "construction_plantime_end": construction.construction_plantime_end,
        "submitter_phone": construction.submitter_phone,
        "construction_time_end": construction.construction_time_end,
        "construction_types": construction.construction_types,
        "org_approver": construction.org_approver,
        "org_approver_pid": construction.org_approver_pid,
        "is_affected": construction.is_affected,
        "construction_affecttime_start": construction.construction_affecttime_start,
        "construction_affecttime_end": construction.construction_affecttime_end,
        "construct_location": construction.construct_location,
        "affected_location":construction.affected_location,
        "construction_content":construction.construction_content,
        "construction_description":construction.construction_description,
        "construction_apply_attachments": construction.construction_apply_attachments,
        "devices": construction.devices,
      }
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }
  //施工申请删除接口
  deleteConstruction(id) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/workflow`, {
      "access_token":token,
      "type": "construction_del",
      "ids":id
    }).map((res:Response) => {
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //施工申请查看接口
  constructionShow(sid){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_get_byid",
      "id": sid
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //人员查看接口
  persoonalShow(oid){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`,{
      'access_token': token,
      'type': 'get_personnel_byoid',
      'id': [],
      'oid': oid
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //施工审批接口
  queryConstructionApproved(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_audit",
      "data": {
        "sid": storage.sid,
        "approve_remarks": storage.approve_remarks,
        "status":storage.status,
        "next_approver": storage.next_approver,
        "next_approver_pid":storage.next_approver_pid+"",
        "next_approver_org":storage.next_approver_org,
        "next_approver_org_oid":storage.next_approver_org_oid
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        // throw new Error(res['errmsg']);
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //施工管理单施工接口
  construction(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_process",
      "data": {
        "sid": storage.sid,
        "status": storage.status,
        "construction_description": storage.construction_description,
        "construction_acttime_start": storage.construction_acttime_start,
        "construction_acttime_end":storage.construction_acttime_end
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        // throw new Error(res['errmsg']);
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //施工管理单完工接口
  completedConstruction(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_finish",
      "data": {
        "sid": storage.sid,
        "status": storage.status,
        "constructor_name": storage.constructor_name,
        "constructor_pid": storage.constructor_pid,
        "constructor_unit":storage.constructor_unit,
        "constructor_unit_id":storage.constructor_unit_id+"",
        "construction_finish_description":storage.construction_finish_description,
        "construction_acttime_start":storage.construction_acttime_start,
        "construction_acttime_end":storage.construction_acttime_end,
        "construction_finish_attachments":storage.construction_finish_attachments,
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        // throw new Error(res['errmsg']);
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //施工管理单复核接口
  reviewConstruction(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_review",
      "data": {
        "sid": storage.sid,
        "status": storage.status,
        "review_remarks": storage.review_remarks,

      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        // throw new Error(res['errmsg']);
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //施工管理整改接口
  constructionRefectiction(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_fix",
      "data": {
        "sid": storage.sid,
        "status": storage.status,
        "constructor_name": storage.constructor_name,
        "constructor_pid": storage.constructor_pid,
        "constructor_unit":storage.constructor_unit,
        "constructor_unit_id":storage.constructor_unit_id+"",
        "construction_finish_description":storage.construction_finish_description,
        "construction_acttime_start":storage.construction_acttime_start,
        "construction_acttime_end":storage.construction_acttime_end,
        "construction_finish_attachments":storage.construction_finish_attachments,

      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        // throw new Error(res['errmsg']);
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //施工管理单关闭接口
  closeConstruction(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/workflow`,{
      "access_token":token,
      "type":"construction_close",
      "data": {
        "sid": storage.sid,
        "close_code": storage.close_code,
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //时间格式化
  getFormatTime(date?) {
    if (!date){
      date= new Date();
    }else{
      date = new Date(date)
    }
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let hour = date.getHours();
    let minutes = date.getMinutes();
    let second = date.getSeconds();
    day = day <= 9 ? "0" + day : day;
    month = month <= 9 ? "0" + month : month;
    hour = hour <= 9 ? "0" + hour : hour;
    minutes = minutes <= 9 ? "0" + minutes : minutes;
    second = second <= 9 ? "0" + second : second;
    return  year+"-"+month+"-" +day+" " +hour+":"+minutes+":"+second

  }
  //  下拉框转换
  formatDropdownData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = temp['code'];
      newarr.push(obj);
    }
    return newarr
  }
  //  搜索建议下拉框转换
  DropdownData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['field'] = temp['name'];
      obj['value'] = temp['code'];
      newarr.push(obj);
    }
    return newarr
  }
  //  下拉框转换
  formatDropdowntreeData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = temp['pid'];
      newarr.push(obj);
    }
    return newarr
  }
  //  申请框转换
  applicationDropdownapprover(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = temp['name'];

      newarr.push(obj);
    }
    return newarr
  }
  //  审批时下拉框转换
  approvedformatDropdowntreeData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = temp['name'];
      newarr.push(obj);
    }
    return newarr
  }
  //  获取组织树
  getDepartmentDatas(oid?, dep?) {
    (!oid) && (oid = '');
    (!dep) && (dep = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/org`,
      {
        "access_token": token,
        "type": "get_org_tree",
        "id": oid,
        "dep": dep
      }
    ).map((res: Response) => {

      if( res['errcode'] !== '00000') {
        return [];
      }else {
        res['datas'].forEach(function (e) {
          e.label = e.name;
          delete e.name;
          e.leaf = false;
          e.data = e.name;
        });
      }
      return res['datas'];
    })
  }
  //获取角色数据
  getJurList(oid){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`, {
      "access_token": token,
      "type": "get_personnel",
      "oid": [oid+""]
    }).map((res: Response) => {
      if(res['errcode'] == '00001'){//没有角色
        return [];
      }else if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //获取人员数据
  getPersonList(oid){
    let token = this.storageService.getToken('token');
    let oids = [];
    if(oid){
      oids.push(oid)
    }
    return this.http.post(`${environment.url.management}/personnel`, {
      "access_token": token,
      "type": "get_personnel",
      "id": [],
      "oid": oids
    }).map((res: Response) => {
      if(res['errcode'] == '00001'){//没有人员
        return [];
      }else if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }

}
