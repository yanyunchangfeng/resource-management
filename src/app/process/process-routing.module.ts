import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ConstructionFlowChartComponent} from "./construction-flow-chart/construction-flow-chart.component";
import {ConstructionApplicationComponent} from "./construction-application/construction-application.component";
import {ConstructionOverviewComponent} from "./construction-overview/construction-overview.component";
import {ConstructionApprovedComponent} from "./construction-approved/construction-approved.component";
import {ConstructionComponent} from "./construction/construction.component";
import {ConstructionReviewComponent} from "./construction-review/construction-review.component";
import {ConstructionCompletedComponent} from "./construction-completed/construction-completed.component";
import {ConstructionViewDetailComponent} from "./construction-view-detail/construction-view-detail.component";
import {CloseConstructionOrderComponent} from "./close-construction-order/close-construction-order.component";
import {ConstructionRectificationComponent} from "./construction-rectification/construction-rectification.component";
import {WaitForHandleComponent} from "./wait-for-handle/wait-for-handle.component";

const route: Routes = [
  {path: 'constructionoverview', component:ConstructionOverviewComponent },
  {path: 'flow', component: ConstructionFlowChartComponent,children:[
    {path: 'constructionoverview', component:ConstructionOverviewComponent },
    {path: 'constructionapplication', component:ConstructionApplicationComponent },//施工管理
    {path: 'constructionapproved', component:ConstructionApprovedComponent },//施工管理
    {path: 'construction', component:ConstructionComponent },//施工管理
    {path: 'constructionreview', component:ConstructionReviewComponent },//施工管理
    {path: 'constructioncompleted', component:ConstructionCompletedComponent },//施工管理
    {path: 'constructionviewdetail', component:ConstructionViewDetailComponent },//查看
    {path: 'closeconstructionorder', component:CloseConstructionOrderComponent },//查看
    {path: 'constructionrectification', component:ConstructionRectificationComponent },//整改
    {path: 'handle', component:WaitForHandleComponent },//待我处理,

  ]},

]
@NgModule({
  imports: [
    RouterModule.forChild(route)
  ],
  exports: [ RouterModule],
})
export class ProcessRoutingModule { }
