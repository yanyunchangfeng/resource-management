import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionCompletedComponent } from './construction-completed.component';

describe('ConstructionCompletedComponent', () => {
  let component: ConstructionCompletedComponent;
  let fixture: ComponentFixture<ConstructionCompletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionCompletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
