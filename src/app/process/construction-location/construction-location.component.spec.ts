import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionLocationComponent } from './construction-location.component';

describe('ConstructionLocationComponent', () => {
  let component: ConstructionLocationComponent;
  let fixture: ComponentFixture<ConstructionLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
