import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ProcessService} from "../process.service";
import {PublicService} from "../../services/public.service";
import {environment} from "../../../environments/environment";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-construction-approved',
  templateUrl: './construction-approved.component.html',
  styleUrls: ['./construction-approved.component.scss']
})
export class ConstructionApprovedComponent implements OnInit {
  constructionForm;
  fb: FormBuilder = new FormBuilder();
  beginTime;
  endTime;
  zh;
  cols;
  asscitedCols;
  OperationLogCols;
  tabs = [];
  tabIndex = 0;
  optButtons;
  sid;
  radioButton = [];
  approvedModel;
  currentContruction = [];
  dataSource;
  totalRecords;
  devices;
  tableDatas;
  queryModel;
  file;
  treeModel
  dropDownData;
  showAddConstruction: boolean = false;
  affected_location=[];
  location =[];
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private processService: ProcessService,
    private eventBusService:EventBusService,

  ) { }

  ngOnInit() {
    this.constructionForm = this.fb.group({
      "sid": [''],  //施工单号
      "status": [''],  //状态
      "creator":[''],    //受理人
      "create_time":[''],   //受理时间
      "submitter":[''],   //申请人
      "submit_time":[''], //申请时间
      "submitter_org":'',  //所属组织
      "construction_plantime_start":'', //计划开始时间
      "submitter_phone":'', //电话
      "construction_plantime_end":'', //计划结束时间
      "construction_type":'', //施工类型
      "org_approver":'', //部门审批人
      "is_affected":'', //影响业务
      "construction_affecttime_start":'', //影响时间
      "construct_location":[], //施工区域
      "affected_location":[], //影响区域
      "construction_content":'', //施工内容
      "construction_description":'', //施工内容
      "construction_apply_attachments":[], //上传附件
      "approve_opinion":['',Validators.required], //审批意见
      "next_approver":[], //下一个审批人
      "next_approver_org":[], //下一个审批角色
    });
    this.tabs = [
      {label:'基本信息'},
      {label:'审批信息'},
      {label:'操作日志'}
      ];
    this.radioButton = [
      {label:'影响业务',value:true},
      {label:'不影响业务',value:false}
    ];
    this.approvedModel = {
      "sid": "",
      "approve_remarks": "",
      "status": "",
      "next_approver": "",
      "next_approver_pid": "",
      "next_approver_org": "",
      "next_approver_org_oid": ""
    };
    this.queryModel={
      id:"",
      oid:""
    }
    this.treeModel={
      oid:""
    }
    this.optButtons=[
      {label:'驳回',status:'驳回'},
      {label:'拒绝',status:'拒绝'},
      {label:'通过',status:'通过'},
    ]
    this.beginTime = new Date();
    this.endTime = new Date();
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.cols = [
      {field: 'ano', header: '设备编号'},
      {field: 'name', header: '设备名称'},
      {field: 'modle', header: '设备型号'},
      {field: 'location', header: '设备位置'},
      {field: 'manager', header: '责任人'},
      {field: 'status', header: '设备状态'},
    ];
    //审批页面审批信息
    this.asscitedCols = [
      {field: 'approve_time', header: '审批时间'},
      {field: 'approver', header: '审批人'},
      {field: 'approve_status', header: '审批状态'},
      {field: 'approve_remarks', header: '审批意见'},
      {field: 'approver', header: '责任人'},
    ];
    //审批页面操作日志
    this.OperationLogCols = [
      {field: 'operate_time', header: '操作时间'},
      {field: 'operator', header: '操作人'},
      {field: 'operate_content', header: '操作内容'},
      {field: 'operate_description', header: '操作说明'},
    ];
    this.queryPersonList();
    this.queryConstructionApproved();
  }
  showAddConstructionMask(){
    this.showAddConstruction = !this.showAddConstruction;
  }
  closeConstruction(bool){
    this.showAddConstruction = bool;
  }
  addPlanOrg(org){
    this.approvedModel.next_approver_org_oid=[] ;
    this.approvedModel.next_approver_org = [];
    this.treeModel.oid =  this.approvedModel.next_approver_org_oid;
    console.log(this.treeModel.oid)
    for(let i = 0;i<org.length;i++){
      this.approvedModel.next_approver_org_oid.push(org[i]['oid'])
      this.approvedModel.next_approver_org.push(org[i]['label'])
    }
    this.approvedModel.next_approver_org =  this.approvedModel.next_approver_org.join(',');
    this.showAddConstruction = false;
    this.queryJurList('');
  }
  clearTreeDialog () {
    this.approvedModel.next_approver_org = '';
    this.approvedModel.next_approver_org_oid = [];
    this.approvedModel.next_approver = [];
    this.approvedModel.next_approver_pid = [];

  }
  //查询人员表格数据
  queryJurList(oid){
    this.processService.getJurList(this.treeModel.oid).subscribe(data => {
      this.tableDatas = data;
      console.log(this.tableDatas)
      this.dropDownData = this.processService.approvedformatDropdowntreeData(this.tableDatas);
      this.approvedModel.next_approver = this.dropDownData[0]['label']
      this.approvedModel.next_approver_pid = this.dropDownData[0]['value']
    })
  }
  queryConstructionApproved(){
    this.route.queryParams.subscribe(parms=>{
      if(parms['sid']){
        this.sid =parms['sid'];
      }
      this.approvedModel.sid = this.sid;
      this.processService.constructionShow(this.sid).subscribe(data=>{
        this.currentContruction = data;
        this.currentContruction['construct_location'].map((item,index)=>{
          this.location.push(item.label);
          this.currentContruction['construct_location'] = this.location
        })
        this.currentContruction['affected_location'].map((item,index)=>{
          this.affected_location.push(item.label);
          this.currentContruction['affected_location'] = this.affected_location
        })
        console.log(this.currentContruction)
        this.radioButton['value'] = this.currentContruction['is_affected'];
        this.dataSource = this.currentContruction['devices'];
        this.file = this.currentContruction['construction_apply_attachments'];
        if(!this.dataSource){
          this.dataSource = [];
          this.totalRecords = 0;
        }
        this.totalRecords = this.dataSource.length;
        this.devices = this.dataSource.slice(0,10);
      })
    });
  }
  approvedOption(status){
    this.approvedModel.status = status;
    this.currentContruction['status'] = '';
    this.eventBusService.flow.next(this.currentContruction['status']);
    this.processService.queryConstructionApproved(this.approvedModel).subscribe(() =>{
      this.router.navigate(['../constructionoverview'],{relativeTo:this.route});
    })
  }
  loadCarsLazy(event) {
    if(this.dataSource) {
      this.devices = this.dataSource.slice(event.first, (event.first + event.rows));
    }
  }
  // 查询人员表格数据
  queryPersonList() {
    this.processService.persoonalShow(this.queryModel).subscribe(data => {
      this.tableDatas = data;
      console.log(this.tableDatas);
      // this.queryModel.oid = this.tableDatas.oid;
    });
  }
  goBack() {
    let sid =  this.currentContruction['sid'];
    this.currentContruction['status'] = "";
    this.eventBusService.flow.next(this.currentContruction['status']);
    this.router.navigate(['../constructionoverview'],{queryParams:{sid:sid,state:'viewDetail',title:'施工总览'},relativeTo:this.route})
  }
  changeTab(index){
   this.tabIndex = index;
  }
  //是否影响业务
  affected(bool){
    console.log(bool);

    if(bool){
      //isaffected = bool
    }
  }
  download() {
    window.open(`${environment.url.management}/${this.file[0].path}`, '_blank');
  }

}
