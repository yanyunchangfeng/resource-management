import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionApprovedComponent } from './construction-approved.component';

describe('ConstructionApprovedComponent', () => {
  let component: ConstructionApprovedComponent;
  let fixture: ComponentFixture<ConstructionApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
