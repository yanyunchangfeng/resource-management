import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionFlowChartComponent } from './construction-flow-chart.component';

describe('ConstructionFlowChartComponent', () => {
  let component: ConstructionFlowChartComponent;
  let fixture: ComponentFixture<ConstructionFlowChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionFlowChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionFlowChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
