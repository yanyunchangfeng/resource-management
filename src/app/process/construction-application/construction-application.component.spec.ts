import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionApplicationComponent } from './construction-application.component';

describe('ConstructionManagementComponent', () => {
  let component: ConstructionApplicationComponent;
  let fixture: ComponentFixture<ConstructionApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
