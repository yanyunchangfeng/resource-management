import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionSettingComponent } from './construction-setting.component';

describe('ConstructionSettingComponent', () => {
  let component: ConstructionSettingComponent;
  let fixture: ComponentFixture<ConstructionSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
