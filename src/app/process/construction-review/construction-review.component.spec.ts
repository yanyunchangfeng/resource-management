import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionReviewComponent } from './construction-review.component';

describe('ConstructionReviewComponent', () => {
  let component: ConstructionReviewComponent;
  let fixture: ComponentFixture<ConstructionReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
