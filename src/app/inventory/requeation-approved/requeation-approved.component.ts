import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {timeValidator} from "../../validator/validators";
import {InventoryService} from "../inventory.service";
import {ActivatedRoute, Router} from "@angular/router";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-requeation-approved',
  templateUrl: './requeation-approved.component.html',
  styleUrls: ['./requeation-approved.component.css']
})
export class RequeationApprovedComponent implements OnInit {
  newInspectionPlan;
  cols;
  approvedModel;
  sid;
  currentMaterial=[];
  totalRecords;
  dataSource ;
  optButtons;
  datas;
  inventories = [];
  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private inventoryService:InventoryService,
    private eventBusService: EventBusService,
    fb:FormBuilder,

  ) {
    this.newInspectionPlan = fb.group({
      sid: [''],
      name: [''],
      department: [''],
      check: [''],
      type: [''],
      return_time: [''],
      people: [''],
      urgency: [''],
      remarks: [''],
      approval_opinions: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.cols = [
      {field: 'sid', header: '编号'},
      {field: 'name', header: '名称'},
      {field: 'catagory', header: '分类'},
      {field: 'brand', header: '品牌'},
      {field: 'model', header: '型号'},
      {field: 'specification', header: '规格'},
      {field: 'application_quantity', header: '数量'},
    ];
    this.approvedModel = {
      "sid": "",
      "approve_remarks": "",
      "status": ""
    }
    this.route.queryParams.subscribe(parms=>{
      if(parms['sid']){
        this.sid =parms['sid'];
      }
      this.approvedModel.sid = this.sid;
      this.inventoryService.queryOutbound(this.sid).subscribe(data=>{
        this.currentMaterial = data;
        console.log(this.currentMaterial)
        this.dataSource = this.currentMaterial['inventories'];
        console.log(this.dataSource)
        if( !this.dataSource ){
          this.dataSource = [];
        }
        this.inventories = this.dataSource.slice(0,10);
        this.totalRecords = this.dataSource.length;
      })
    });
    this.optButtons=[
      {label:'驳回',status:'驳回'},
      {label:'拒绝',status:'拒绝'},
      {label:'通过 ',status:'待出库'},
    ]
  }
  goBack() {
    let sid =  this.currentMaterial['sid'];
    this.currentMaterial['status'] = "";
    this.eventBusService.requstion.next(this.currentMaterial['status']);
    this.router.navigate(['../requestionOverview'],{queryParams:{sid:sid,state:'viewDetail',title:'借用总览'},relativeTo:this.route})
  }
  approvedOption(status){
    this.approvedModel.status = status;
    this.inventoryService.queryOutboundApproved(this.approvedModel).subscribe(() =>{
      this.currentMaterial['status'] = "";
      this.eventBusService.requstion.next(this.currentMaterial['status']);
      this.router.navigate(['../requestionOverview'],{relativeTo:this.route});
    })
  }
  loadCarsLazy(event) {
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    //imitate db connection over a network
    if(this.dataSource) {
      this.inventories = this.dataSource.slice(event.first, (event.first + event.rows));
    }
  }

}
