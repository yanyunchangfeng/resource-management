import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequeationApprovedComponent } from './requeation-approved.component';

describe('RequeationApprovedComponent', () => {
  let component: RequeationApprovedComponent;
  let fixture: ComponentFixture<RequeationApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequeationApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequeationApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
