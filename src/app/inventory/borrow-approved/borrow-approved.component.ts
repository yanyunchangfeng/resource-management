import { Component, OnInit } from '@angular/core';
import {InventoryService} from "../inventory.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-borrow-approved',
  templateUrl: './borrow-approved.component.html',
  styleUrls: ['./borrow-approved.component.scss']
})
export class BorrowApprovedComponent implements OnInit {
  zh;
  cols;
  approvedModel;
  sid;
  currentMaterial;
  totalRecords;
  dataSource ;
  optButtons;
  datas;
  evstatus;
  inventories = [];
  borrowapprovedForm: FormGroup;//表单验证声明
  fb:FormBuilder =  new FormBuilder();
  constructor(  private route:ActivatedRoute,
                private router:Router,
                private inventoryService:InventoryService,
                private eventBusService: EventBusService
) { }
  ngOnInit() {
    this.borrowapprovedForm = this.fb.group({
      'sid': [''],
      'types': [''],
      'creator': [''],
      'org': [''],
      'create_time':[''],
      'return_time': [''],
      'remarks':[''],
      'approve_remarks':['', Validators.required],
    });
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.cols = [
      {field: 'sid', header: '编号' ,editable:false},
      {field: 'name', header: '名称' ,editable:false},
      {field: 'catagory', header: '分类' ,editable:false},
      {field: 'brand', header: '品牌' ,editable:false},
      {field: 'model', header: '型号' ,editable:false},
      {field: 'specification', header: '规格' ,editable:false},
      {field: 'application_quantity', header: '数量' ,editable:false},
    ];
    this.approvedModel = {
      "sid": "",
      "approve_remarks": "",
      "status": ""
    }

    this.route.queryParams.subscribe(parms=>{
      // if(parms['sid'] ||parms['sid']){
        this.sid =parms['sid'];
        this.evstatus = parms['status'];
      // }
      this.approvedModel.sid = this.sid;
      this.inventoryService.queryOutbound(this.sid).subscribe(data=>{
        if( !this.dataSource ){
          this.dataSource = [];
        }
        this.currentMaterial = data;
        this.dataSource = this.currentMaterial.inventories;
        this.inventories = this.dataSource.slice(0,10);
        this.totalRecords = this.dataSource.length;

      })
    });
    this.optButtons=[
      {label:'驳回',status:'驳回'},
      {label:'拒绝',status:'拒绝'},
      {label:'通过',status:'待出库'},
    ]

  }
  approvedOption(status){
    this.approvedModel.status = status;
    this.inventoryService.queryOutboundApproved(this.approvedModel).subscribe(() =>{
      this.currentMaterial['status'] = "";
      this.eventBusService.borrow.next(this.currentMaterial['status']);
      this.router.navigate(['../outbound'],{relativeTo:this.route});
    })
  }
  loadCarsLazy(event) {
    if(this.dataSource) {
      this.inventories = this.dataSource.slice(event.first, (event.first + event.rows));
    }
  }
  approvedgoBack() {
    let sid =  this.currentMaterial['sid'];
    this.currentMaterial['status'] = "";
    this.eventBusService.borrow.next(this.currentMaterial['status']);
    this.router.navigate(['../outbound'],{queryParams:{sid:sid,state:'viewDetail',title:'借用总览'},relativeTo:this.route})
  }
}
