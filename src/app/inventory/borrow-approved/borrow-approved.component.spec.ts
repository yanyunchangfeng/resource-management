import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowApprovedComponent } from './borrow-approved.component';

describe('BorrowApprovedComponent', () => {
  let component: BorrowApprovedComponent;
  let fixture: ComponentFixture<BorrowApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
