import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-borrow-list',
  templateUrl: './borrow-list.component.html',
  styleUrls: ['./borrow-list.component.css']
})
export class BorrowListComponent implements OnInit {
  cols;
  dataSource = [];
  totalRecords;
  inspections = [];
  constructor() { }

  ngOnInit() {
    this.cols = [
      {field: 'sid', header: '编号' ,editable:false},
      {field: 'name', header: '名称' ,editable:false},
      {field: 'catagory', header: '分类' ,editable:false},
      {field: 'brand', header: '品牌' ,editable:false},
      {field: 'model', header: '型号' ,editable:false},
      {field: 'specification', header: '规格' ,editable:false},
      {field: 'quantity', header: '数量' ,editable:true},
      // {field: 'balance', header: '借用单中剩余归还数量' ,editable:false},
      // {field: 'return_quantity', header: '借用单中已归还数量' ,editable:false},
    ];
  }
  loadCarsLazy(event) {
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    //imitate db connection over a network
    setTimeout(() => {
      if(this.dataSource) {
        this.inspections = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 0);
  }
}
