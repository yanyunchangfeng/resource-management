import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {InventoryService} from "../inventory.service";
import {EventBusService} from "../../services/event-bus.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-borrow-management',
  templateUrl: './borrow-management.component.html',
  styleUrls: ['./borrow-management.component.scss']
})
export class BorrowManagementComponent implements OnInit {

  flowCharts;
  title = '借用总览';
  queryModel;
  MaterialCharData;
  constructor(
    public router:Router,
    public route:ActivatedRoute,
    public confirmationService:ConfirmationService,
    private inventoryService:InventoryService,
    private eventBus: EventBusService

  ) { }

  ngOnInit() {
    this.queryModel={
      "status": "",
    };
    this.flowCharts = [
      {label:'借用申请',route:'borrow',stroke:"#39B9C6",fill:'#357CA5',active:true,x:'0',y:'0',rx:'10',width:100,height:40,title:'借用申请',text:{x:'20',y:'24',label:'借用申请'},g:[{x1:'50',y1:'40',x2:'50',y2:'100'},{x1:'50',y1:'100',x2:'43',y2:'90'},{x1:'50',y1:'100',x2:'58',y2:'90'}]},
      {label:'借用审批',route:'outbound',status:"待审批",stroke:"#39B9C6",fill:'#357CA5',active:false,x:'0',y:'100',width:100,height:40,title:'借用审批',text:{x:'20',y:'124',label:'借用审批'},g:[{x1:'50',y1:'140',x2:'50',y2:'200'},{x1:'50',y1:'200',x2:'43',y2:'190'},{x1:'50',y1:'200',x2:'58',y2:'190'}]},
      {label:'借用',route:'outbound',status:"待借用",stroke:"#39B9C6",fill:'#357CA5',active:false,x:'0',y:'200',width:100,height:40,title:'借用',text:{x:'30',y:'224',label:'借用'},g:[{x1:'50',y1:'240',x2:'50',y2:'300'},{x1:'50',y1:'300',x2:'43',y2:'290'},{x1:'50',y1:'300',x2:'58',y2:'290'}]},
      {label:'续借',route:'outbound',status:"待归还",stroke:"#39B9C6",fill:'#357CA5',active:false,x:'0',y:'300',width:100,height:40,title:'续借',text:{x:'30',y:'324',label:'续借'},g:[{x1:'50',y1:'340',x2:'50',y2:'400'},{x1:'50',y1:'400',x2:'43',y2:'390'},{x1:'50',y1:'400',x2:'58',y2:'390'}]},
      {label:'归还',route:'outbound',status:"已归还",stroke:"#39B9C6",active:true,x:'0',y:'400',fill:'#357CA5',rx:'10',width:100,height:40,title:'',text:{x:'30',y:'424',label:'归还'}}
    ]
    this.route.queryParams.subscribe(params=>{
      if(params['title']){
        this.title = params['title']
      }
      if(params['sid']){
        this.inventoryService.borrowShow(params['sid']).subscribe(data=>{
          console.log(data)
        })
      }
    });
    this.eventBus.borrow.subscribe(data=>{
      console.log(data)
      this.queryModel.status = data;
      this.queryMaterialDate();
    })

    this.queryMaterialDate();
  }
  queryMaterialDate(){
    this.inventoryService.queryMaterialBorrow(this.queryModel).subscribe(data=>{
      if(!this.MaterialCharData){
        this.MaterialCharData = [];
      }
      this.MaterialCharData = data;
      for(let i = 0;i<this.MaterialCharData.length;i++){
        let temp = this.MaterialCharData[i];
        this.flowCharts[i]['fill'] = temp['fill'] ;
      }
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，请联系管理员或稍后重试'
      }else{
        message = err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })

  }
  openPage(flow,index){
    if(index+1 === this.flowCharts.length)return
    for(let i = 0;i<this.flowCharts.length;i++){
      this.flowCharts[i].active = false;
    }
    this.flowCharts[index].active = true;
    this.queryMaterialDate();

    if(flow.status){
      this.eventBus.updaterequestion.next(flow.status)
    }

    if(flow.route){
      this.router.navigate([flow.route],{relativeTo:this.route});
      // this.MaterialCharData = [];
      this.title = flow.title;
    }
  }
}
