import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentStorageComponent } from './equipment-storage.component';

describe('EquipmentStorageComponent', () => {
  let component: EquipmentStorageComponent;
  let fixture: ComponentFixture<EquipmentStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
