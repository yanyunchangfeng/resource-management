import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageApprovedComponent } from './storage-approved.component';

describe('StorageApprovedComponent', () => {
  let component: StorageApprovedComponent;
  let fixture: ComponentFixture<StorageApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorageApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorageApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
