import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Route, Router} from "@angular/router";
import {InventoryService} from "../inventory.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-storage-approved',
  templateUrl: './storage-approved.component.html',
  styleUrls: ['./storage-approved.component.scss']
})
export class StorageApprovedComponent implements OnInit {
  currentMaterial;
  totalRecords;
  dataSource;
  sid;
  cols;
  approvedModel;
  optButtons;
  datas;
  inventories =[];
  borrowapprovedForm: FormGroup;//表单验证声明
  fb:FormBuilder =  new FormBuilder();
  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private inventoryService:InventoryService,
    private eventBusService: EventBusService
  ) { }

  ngOnInit() {
    this.borrowapprovedForm = this.fb.group({
      'sid': [''],
      'creater': [''],
      'location':[''],
      'keeper': [''],
      'remarks':[''],
      'approve_remarks':['', Validators.required],
    });
    this.cols = [
      {field: 'pn', header: '编号'},
      {field: 'name', header: '名称'},
      {field: 'catagory', header: '分类'},
      {field: 'brand', header: '品牌'},
      {field: 'model', header: '型号'},
      {field: 'specification', header: '规格'},
      {field: 'quantity', header: '数量'},
      {field: 'supplier', header: '供应商'},
    ]
    this.approvedModel = {
      "sid": "",
      "approve_remarks": "",
      "status": ""
    }


    this.route.queryParams.subscribe(parms=>{
      if(parms['sid']){
        this.sid =parms['sid'];
      }
      this.approvedModel.sid = this.sid;
      this.inventoryService.queryStorage(this.sid).subscribe(data=>{
        this.currentMaterial = data;
        this.dataSource = this.currentMaterial['inventories']?this.currentMaterial['inventories']:[];
        if( !this.dataSource ){
          this.dataSource = [];
          this.totalRecords = 0;
        }
        // this.dataSource = this.currentMaterial.inventories;
        this.inventories = this.dataSource.slice(0,10);
        this.totalRecords = this.dataSource.length;
      })

    });

    this.optButtons=[
      {label:'驳回',status:'驳回'},
      {label:'拒绝',status:'拒绝'},
      {label:'通过',status:'待入库'},
    ]
  }
  loadCarsLazy(event) {
    setTimeout(() => {
      if(this.dataSource) {
        this.inventories = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 0);
  }

  goBack() {
    let sid =  this.currentMaterial['sid'];
    this.currentMaterial['status'] = "";
    this.eventBusService.flow.next(this.currentMaterial['status']);
    this.router.navigate(['../overview'],{queryParams:{sid:sid,state:'viewDetail',title:'入库审批'},relativeTo:this.route})
  }
  approvedOption(status){
    this.approvedModel.status = status;
    this.inventoryService.queryStorageApproved(this.approvedModel).subscribe(() =>{
      this.currentMaterial['status'] = "";
      this.eventBusService.flow.next(this.currentMaterial['status']);
      this.router.navigate(['../overview'],{relativeTo:this.route});
    })
  }

}
