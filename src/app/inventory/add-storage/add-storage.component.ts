import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InventoryService} from "../inventory.service";
import {ConfirmationService} from "primeng/primeng";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-storage',
  templateUrl: './add-storage.component.html',
  styleUrls: ['./add-storage.component.scss']
})
export class AddStorageComponent implements OnInit {
  @Output() closeAddStorage = new EventEmitter;
  @Output() addStorage = new EventEmitter();
  @Output() updateStorage = new EventEmitter();
  @Input() currentMeterial;
  @Input() state;
  title = '添加';
  display: boolean = false;
  showChooseMaterialNumber: boolean = false;//选择物料编号弹框
  zh;
  submitData;
  windowSize;
  width;
  storageForm: FormGroup;
  fb: FormBuilder = new FormBuilder();
  constructor(
    private inventoryService: InventoryService,
    private confirmationService: ConfirmationService,
  ) { }

  ngOnInit() {
    if(window.innerWidth<1024){
      this.width = window.innerWidth*0.7;
    }else{
      this.width = window.innerWidth*0.6;
    }
  this.storageForm = this.fb.group({
    'pn':['',Validators.required],
    'name':['',Validators.required],
    'catagory':['',Validators.required],
    'supplier':['',Validators.required],
    'brand':['',Validators.required],
    'model':['',Validators.required],
    'specification':['',Validators.required],
    'unit_cost':[''],
    'time_purchase':[''],
    'quantity':['',Validators.required],
  })
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.display = true;
    this.submitData = {
      "pn": "",
      "name": "",
      "catagory": "",
      "supplier": "",
      "brand": "",
      "model": "",
      "specification": "",
      "unit_cost":"",
      "time_purchase": "",
      "quantity": ""
    }
    if(this.state === 'update'){
      this.currentMeterial = this.inventoryService.deepClone(this.currentMeterial);
      this.title = '编辑';
    }
    if(this.state ==='show'){
      this.title = '查看';
    }
  }
  addDevice(bool){
    this.inventoryService.addStorage(this.submitData).subscribe(()=>{
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  };
  storageSave(bool){
     this.addStorage.emit(this.submitData)
  }

  formSubmit(bool){
    if(this.state ==='add'){
      this.storageSave(bool);
    }else if(this.state ==='update'){
      this.updateStorage.emit(this.currentMeterial);
    }
  }
  closeAddStorageMask(bool){
    this.closeAddStorage.emit(bool)
  }
  showChooseMaterialNumberMask(){
    this.showChooseMaterialNumber = !this.showChooseMaterialNumber;
  }
  closeChooseMaterialNumber(bool){
    this.showChooseMaterialNumber =bool;
  }
  addDev(metail){
    this.submitData['pn'] =metail['pn'];
    this.submitData['name'] =metail['name'];
    this.submitData['catagory'] =metail['catagory'];
    this.submitData['supplier'] =metail['supplier'];
    this.submitData['brand'] =metail['brand'];
    this.submitData['specification'] =metail['specification'];
    this.submitData['unit_cost'] =metail['unit_cost']?metail['unit_cost']:'';
    this.submitData['threshold_group'] =metail['threshold_group'];
    this.submitData['status'] =metail['status'];
    // this.submitData['time_purchase'] =metail['time_purchase'];
    this.submitData['model'] =metail['model'];
    this.showChooseMaterialNumber = false;
  }

}
