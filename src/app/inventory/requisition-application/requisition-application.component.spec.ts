import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitionApplicationComponent } from './requisition-application.component';

describe('RequisitionApplicationComponent', () => {
  let component: RequisitionApplicationComponent;
  let fixture: ComponentFixture<RequisitionApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisitionApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitionApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
