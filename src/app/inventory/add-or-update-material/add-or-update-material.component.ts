import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InventoryService} from "../inventory.service";
import {ConfirmationService} from "primeng/primeng";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-or-update-material',
  templateUrl: './add-or-update-material.component.html',
  styleUrls: ['./add-or-update-material.component.scss']
})
export class AddOrUpdateMaterialComponent implements OnInit {
  display: boolean = false;
  @Output() closeMaterial = new EventEmitter;
  @Output() addMaterial = new EventEmitter();
  @Output() updateDev = new EventEmitter();
  @Input() currentMeterial;
  @Input() state;
  materialForm: FormGroup;
  fb: FormBuilder =  new FormBuilder();
  categories;
  norms;
  material;
  title = '新增';
  submitData;
  threshold_group;
  constructor(
    private inventoryService: InventoryService,
    private confirmationService: ConfirmationService,

               ) { }

  ngOnInit() {
    this.materialForm = this.fb.group({
      'catagory':['',Validators.required],
      'pn':['',Validators.required],
      'name':['',Validators.required],
      'supplier':['',Validators.required],
      'brand':['',Validators.required],
      'model':['',Validators.required],
      'specification':['',Validators.required],
      // 'unit_cost':['',Validators.required],
      'threshold_group':['',Validators.required],
      'status':['',Validators.required],
    });
    this.submitData = {
      "catagory":'',
      "pn": "",
      "name": "",
      "supplier": "",
      "brand": "",
      "model": "",
      "specification": "",
      "unit_cost": "",
      "threshold_group": "",
      "status": ""
    }
    this.display = true;
    this.currentMeterial = this.inventoryService.deepClone(this.currentMeterial);
    if(this.state === 'update'){
      this.title = '编辑';
    }
    this.queryMeterial();
  }
  addDevice(bool){
    this.inventoryService.addMaterial(this.submitData).subscribe(()=>{
      this.addMaterial.emit(bool);
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，稍后重试'
      }else{
        message =err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  };

  updateDevice(bool){
    this.inventoryService.updateMaterial(this.currentMeterial).subscribe(()=>{
      this.updateDev.emit(this.currentMeterial);
      this.closeMaterial.emit(bool)
    })
  }
  queryMeterial(){
    this.inventoryService.queryMaterialInfo().subscribe(data =>{
      if(data['分类']){
        this.categories = data['分类']
        this.categories = this.inventoryService.formatDropdownData(this.categories)
        console.log(this.categories)
      }else{
        this.categories = [];
      }
      if(data['规格']){
        this.norms = data['规格'];
        this.norms = this.inventoryService.formatDropdownData(this.norms)
      }else{
        this.norms = [];
      }
      if(data['安全库存']){
        this.threshold_group = data['安全库存'];
        this.threshold_group = this.inventoryService.formatDropdownData(this.threshold_group)
      }else{
        this.threshold_group = [];
      }
      this.submitData.catagory = this.categories[0]['label'];
      this.submitData.specification = this.norms[0]['label'];
      this.submitData.threshold_group = this.threshold_group[0]['label'];
    });
  }
  formSubmit(bool){
    if(this.state ==='add'){
      this.addDevice(bool)
    }else{
      this.updateDevice(bool)
    }
  }
  closeMaterialMask(bool){
    this.closeMaterial.emit(bool)
  }

}
