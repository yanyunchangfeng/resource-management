import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrUpdateMaterialComponent } from './add-or-update-material.component';

describe('AddOrUpdateMaterialComponent', () => {
  let component: AddOrUpdateMaterialComponent;
  let fixture: ComponentFixture<AddOrUpdateMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrUpdateMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrUpdateMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
