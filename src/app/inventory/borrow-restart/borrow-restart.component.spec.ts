import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowRestartComponent } from './borrow-restart.component';

describe('BorrowRestartComponent', () => {
  let component: BorrowRestartComponent;
  let fixture: ComponentFixture<BorrowRestartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowRestartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowRestartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
