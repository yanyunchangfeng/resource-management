import {Component, Input, OnInit} from '@angular/core';
import {InventoryService} from "../inventory.service";
import {StorageService} from "../../services/storage.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmationService} from "primeng/primeng";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-inventory-location',
  templateUrl: './inventory-location.component.html',
  styleUrls: ['./inventory-location.component.scss']
})
export class InventoryLocationComponent implements OnInit {
  cols;
  queryModel;
  chooseCids = [];
  dataSource;
  materials;
  totalRecords;
  selectMaterial = [];
  treeState;
   sid;//单号
  evstatus;
  pnAnddataSource = {};
  // currentMaterial;
  showLocation: boolean = false;  //选择仓库管理员弹框

  constructor( private inventoryService: InventoryService ,
               private storageService: StorageService,
               private route:ActivatedRoute,
               private router:Router,
               private confirmationService: ConfirmationService,
               private  eventBusService: EventBusService

  ) {

  }

  ngOnInit() {
    this.queryModel={
      "condition":{
        "name": "",
        "catagory": "",
        "count": "",
        "status": ""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.cols = [
    {field: 'sid', header: '设备编号'},
      {field: 'name', header: '设备名称'},
      {field: 'catagory', header: '设备分类'},
      {field: 'brand', header: '品牌'},
      {field: 'model', header: '型号'},
      {field: 'specification', header: '规格'},
      {field: 'quantity', header: '数量'},
    ];
    this.route.queryParams.subscribe(params=>{
      if(params['sid']){
         this.sid = params['sid']
        this.evstatus = params['status']

      }
    })
    this.queryMaterialDate();
  }
  goBack() {
    let sid =  this.sid;
    this.evstatus = "";
    this.eventBusService.flow.next(this.evstatus);
    this.router.navigate(['../overview'],{queryParams:{sid:sid,state:'viewDetail',title:'入库总览'},relativeTo:this.route})
  }
  showLoactionMask(index){
    this.treeState = 'update';
    this.pnAnddataSource['sid'] = this.sid;
    this.pnAnddataSource['data'] = this.dataSource;
    // this.currentStorageOverView = this.MaterialData[index];
    this.storageService.setLocationIndex('locationIndex',index);
    this.showLocation = true;
  }
  closeLocationMask(bool){
    this.showLocation = bool;
  }
  clearTreeDialog () {
    this.materials.location = '';
    this.materials.location = [];
  }
  nodeTree(node){
    let localtionIndex = this.storageService.getLocationIndex('locationIndex');
    this.materials[localtionIndex].location = node['label'];
    this.materials[localtionIndex].location_id = node['did'];
    this.showLocation =false;
  }
  //确认入库
  sureApproved(){
    this.inventoryService.queryStorageSure(this.sid).subscribe(data=>{
      this.selectMaterial = [];
      this.evstatus = "";
      this.eventBusService.flow.next(this.evstatus);
      this.router.navigate(['../overview'],{queryParams:{'sid':this.sid,title:'入库总览'},relativeTo:this.route})


      // this.queryMaterialDate();

    })
  }
  //分页查询
  paginate(event) {
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryMaterialDate();
  }
  //同步更新页面数据
  updateLocation(){
    this.showLocation = false;
    this.queryMaterialDate()
  }
  queryMaterialDate(){
    this.inventoryService.queryStorage(this.sid).subscribe(data=>{
      if(!data['inventories']){
        this.dataSource = [];
        this.selectMaterial =[];
        this.totalRecords = 0;
      }else{
        this.dataSource = data['inventories'];
        console.log(this.dataSource)
        this.totalRecords = this.dataSource.length;
        this.materials = this.dataSource.slice(0,10);
      }
    });
  }
  loadCarsLazy(event) {
      if(this.dataSource) {
        this.materials = this.dataSource.slice(event.first, (event.first + event.rows));
      }
  }
}
