import { Injectable } from '@angular/core';
// import {Http,Response} from "@angular/http";
import {HttpClient} from "@angular/common/http";
import {StorageService} from "../services/storage.service";
import {environment} from "../../environments/environment";

@Injectable()
export class InventoryService {
  constructor(private http: HttpClient, private storageService: StorageService) {
  }
  //获取当前登录人接口
  queryPersonal(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/personnel`,{
      "access_token":token,
      "type":"get_personnel_user",
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }

  // 添加物料接口
  addMaterial(material) {
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`, {
      "access_token": token,
      "type": "material_add",
      "data": {
        "pn": material.pn,
        "name": material.name,
        "catagory": material.catagory,
        "supplier": material.supplier,
        "brand": material.brand,
        "model": material.model,
        "specification": material.specification,
        "unit_cost": material.unit_cost,
        "threshold_group": material.threshold_group,
        "status": material.status
      }
    }).map((res: Response) => {
      // if (res['errcode'] !== '00000') {
      //   // throw new Error(res['errmsg']);
      //   let reg = /No datas/;
      //   if (res['errmsg'].search(reg) !== -1) {
      //     return []
      //   }
      // }
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }

  //  修改物料接口
  updateMaterial(material){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token": token,
      "type": "material_mod",
      "data": {
        "pn": material.pn,
        "name": material.name,
        "catagory": material.catagory,
        "supplier": material.supplier,
        "brand": material.brand,
        "model": material.model,
        "specification": material.specification,
        "unit_cost": material.unit_cost,
        "threshold_group": material.threshold_group,
        "status": material.status
      }
    }).map((res:Response) =>{
      if (res['errcode'] !== '00000') {
        let reg = /No datas/;
        if (res['errmsg'].search(reg) !== -1) {
          return []
        }
      }
      return res['data'];
    })
  }
  //查询物料接口
  queryMaterials(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"material_get",
      "data": {
        "condition":{
          "pn": material.condition.pn.trim(),
          "name": material.condition.name.trim(),
          "status": material.condition.status.trim(),
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //查询物料接口
  queryMaterial(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"material_get",
      "data": {
        "condition":{
          "pn": material.condition.pn,
          "name": material.condition.name,
          // "status": material.condition.status.trim(),
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode']!=='00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //删除物料接口
  deleteInventory(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inventory`, {
      "access_token":token,
      "type": "material_del",
      "ids":ids
    }).map((res:Response) => {
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.data
      if (res['errcode'] !== '00000') {
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //库房管理配置信息获取接口
  queryMaterialInfo(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/foundationdata`,{
      "access_token":token,
      "type":"inventory_config_get",
      "ids": [
        "规格",
        "库存位置",
        "分类",
        "阈值分组",
        "安全库存",
      ]

    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //入库领用紧急程度
  queryMaterialInfocater(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/foundationdata`,{
      "access_token":token,
      "type":"inventory_config_get",
      "ids": [
        "紧急度"
      ]

    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }


//入库总览流程图接口
  queryMaterialChar(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"entering_workflow_get",
      "data": {
          "status": material.status.trim(),
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
//借用总览流程图接口
  queryMaterialBorrow(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"borrow_workflow_get",
      "data": {
          "status": material.status.trim(),
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
//领用总览流程图接口
  queryMaterialreceive(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"receive_workflow_get",
      "data": {
          "status": material.status.trim(),
      }
    }).map((res:Response)=>{
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['datas'];
    })
  }
  //查询库存总览分类接口
  queryStoragecatorgroy(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/foundationdata`,{
      "access_token":token,
      "type": "inventory_config_gettree",
      "id": "分类"
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
 //库存总览接口
  queryInventory(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"inventory_get",
      "data": {
        "condition":{
          "sid": material.condition.sid.trim(),
          "name": material.condition.name.trim(),
          "catagory": material.condition.catagory.trim(),
          "count": material.condition.count.trim(),
          "status": material.condition.status.trim(),
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
 // 入库总览所有状态获取接口
  queryInventoryStatus(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"entering_statuslist_get",
    }).map((res:Response)=>{

      if(res['errcode']!== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['datas'];
    })
  }
 // 借用总览所有状态获取接口
  queryBorrowStatus(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"borrow_statuslist_get",
    }).map((res:Response)=>{

      if(res['errcode']!== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['datas'];
    })
  }
 // 领用总览所有状态获取接口
  queryReceiveStatus(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"receive_statuslist_get",
    }).map((res:Response)=>{

      if(res['errcode']!== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['datas'];
    })
  }
 //入库总览接口
  queryStorageView(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type": "entering_get",
        "data": {
          "condition":{
          "sid": material.condition.sid.trim(),
          "status": material.condition.status.trim(),
          "begin_time": material.condition.begin_time.trim(),
          "end_time": material.condition.end_time.trim(),
          "creator": material.condition.creator.trim(),
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode'] !== '00000'){
        // throw new Error(res['errmsg']);
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }


  //入库申请新增接口
  addStorage(storage){
       let token = this.storageService.getToken('token');
       return this.http.post(`${environment.url.management}/inventory`,{
         "access_token":token ,
         "type": "entering_add",
         "data": {
           "sid": storage.sid,
           "location":storage.location,
           "location_id":storage.location_id,
           "keeper": storage.keeper,
           "keeper_pid": storage.keeper_pid,
           "remarks":storage.remarks,
           "inventories": storage.inventories
         }
       }).map(( res:Response) =>{
         if(res['errcode'] !=='00000'){
           throw new Error(res['errmsg'])
         }
         return res['data'];
       })
   }
  //  入库申请修改接口
  updateStorage(storage){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token": token,
      "type": "entering_mod",
      "data": {
        "sid": storage.sid,
        "create_time":storage.create_time,
        "location": storage.location,
        "location_id":storage.location_id,
        "keeper": storage.keeper,
        "keeper_pid": storage.keeper_pid,
        "status": storage.status,
        "creator": storage.creator,
        "creator_pid": storage.creator_pid,
        "approve_remarks": storage.approve_remarks,
        "audit_time": storage.audit_time,
        "entering_time": storage.entering_time,
        "remarks": storage.remarks,
        "begin_time": storage.begin_time,
        "end_time": storage.end_time,
        "inventories": storage.inventories
      }

    }).map((res:Response) =>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['data']
    })
  }
  //入库申请删除接口
  deleteStorage(id) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inventory`, {
      "access_token":token,
      "type": "entering_del",
      "ids":[id]
    }).map((res:Response) => {
      // let body = res.json();
      // if (body.errcode !== '00000') {
      //   throw new Error(body.errmsg);
      // }
      // return body.data
      if(res['errcode']!=='00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //查询入库申请接口
  queryStorage(id){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"entering_get_byid",
      "id": id
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //入库申请保存接口
  queryStorageSave(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"entering_save",
      "data": {
        "sid": storage.sid,
        "location": storage.location,
        "location_id":storage.location_id,
        "keeper": storage.keeper,
        "keeper_pid": storage.keeper_pid,
        "remarks": storage.remarks,
        // "time_purchase": storage.time_purchase,
        "inventories": storage.inventories
      }

    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //库存位置树接口
  //查询入库申请接口
  queryStorageLocationTree(){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/foundationdata`,{
      "access_token":token,
      "type": "inventory_config_gettree",
      "id": "库存位置"
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if (res['errmsg'].search(reg) !== -1) {
          return []
        }
      }
      return res['data'];
    })
  }
  //入库审批接口
  queryStorageApproved(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"entering_audit",
      "data": {
        "sid": storage.sid,
        "approve_remarks": storage.approve_remarks,
        "status": storage.status
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      // if(res['errcode'] !== '00000'){
      //   throw new Error(res['errmsg'])
      // }
      if(res['errcode'] !== '00000'){
        // throw new Error(res['errmsg']);
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //确认入库接口
  queryStorageSure(sid){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"entering_act",
      "data": {
        "sid": sid,
      }
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if (res['errmsg'].search(reg) !== -1) {
          return []
        }
      }
      return res['data'];
    })
  }
 //出库总览接口
  queryOutband(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type": "borrow_get",
      "data": {
        "condition":{
          "sid": material.condition.sid,
          "location": material.condition.location,
          "status": material.condition.status,
          "begin_time": material.condition.begin_time,
          "end_time": material.condition.end_time,
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
 //出库总览所有接口
  queryAllOutband(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type": "delivery_statuslist_get",

    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //确认出库接口
  queryBorrowSure(data){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_act",
      "data": {
        "sid": data.sid,
        "inventories": data.inventories,
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if (res['errmsg'].search(reg) !== -1) {
          return []
        }else{
          throw new Error(res['errmsg']);
        }
      }
      return res['data'];
    })
  }
  //出库删除接口
  deleteDelivery(ids) {
    let token = this.storageService.getToken('token')
    return this.http.post(`${environment.url.management}/inventory`, {
      "access_token":token,
      "type": "delivery_del",
      "ids":ids
    }).map((res:Response) => {
      if (res['errcode'] !== '00000') {
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //出库总览所有状态获取接口
  queryOutbandStatus(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type": "delivery_statuslist_get",
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }
  //出库流程获取接口
  queryOutbandChar(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type": "delivery_workflow_get",
      "data":{
        "type":material.type,
        "status":material.status
      }

    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //出库审批接口
  queryOutboundApproved(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_audit",
      "data": {
        "sid": storage.sid,
        "approve_remarks": storage. approve_remarks,
        "status": storage.status
      }
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }

  //查询出库申请接口
  queryOutbound(id){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_get_byid",
      "id": id
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //待我处理出库总览接口
  queryOutbandDeal(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type": "delivery_get_byowner",
      "data": {
        "condition":{
          "status": material.condition.status.trim(),
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      // let body = res.json();
      // if(body.errcode !== '00000'){
      //   throw new Error(body.errmsg)
      // }
      // return body.data;
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //借用申请保存接口
  queryDeliverySave(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_save",
      "data": {
        "sid": storage.sid,
        "type": storage.type,
        "return_time": storage.return_time,
        "urgency":storage.urgency,
        "remarks": storage.remarks,
        "create_time": storage.create_time,
        "inventories":storage.inventories
      }
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
        throw  new Error(res['errmsg'])
      }
      return res['data'];
    })
  }

  //	借用申请新增接口
  addDelivery(storage){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token ,
      "type": "delivery_add",
      "data": {
        "sid": storage.sid,
        "type": storage.type,
        "return_time": storage.return_time,
        "create_time": storage.create_time,
        "urgency":storage.urgency,
        "remarks":storage.remarks,
        "inventories": storage.inventories
      }
    }).map(( res:Response) =>{
      if(res['errcode'] !=='00000'){
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }
  //借用申请修改接口
  queryDeliveryMod(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_mod",
      "data": {
        "sid": storage.sid ,
        "type":   storage.type,
        "org": storage.org,
        "org_oid": storage.org_oid,
        "approver": storage.approver,
        "approver_pid": storage.approver_pid,
        "approve_remarks": storage.approve_remarks,
        "keeper": storage.keeper,
        "keeper_pid": storage.keeper_pid,
        "create_time": storage.create_time,
        "return_time": storage.return_time,
        "audit_time": storage.audit_time,
        "creator": storage.creator,
        "creator_pid": storage.creator_pid,
        "urgency": storage.urgency,
        "status": storage.status,
        "remarks": storage.remarks,
        "delivery_time": storage.delivery_time,
        "begin_time": storage.begin_time,
        "end_time": storage.end_time,
        "inventories":storage.inventories
      }
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }
  //借用申请查看接口
  borrowShow(sid){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_get_byid",
      "id": sid
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
 //续借接口
  restartqueryDelivery(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_renew",
      "data": {
        "sid": storage.sid,
        "return_time":storage.return_time,
        "renew_reason":storage.renew_reason
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if (res['errmsg'].search(reg) !== -1) {
          return []
        }
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }
 //全部借用归还接口
  DeliveryReturn(sid){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_return",
      "data": {
        "sid": sid,
        "inventories":[]
      }
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if (res['errmsg'].search(reg) !== -1) {
          return [];
        }
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
 //部分借用归还接口
  someDeliveryReturn(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_return",
      "data": {
        "sid": storage.sid,
        "inventories":storage.inventories
      }
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if (res['errmsg'].search(reg) !== -1) {
          return []
        }
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }

  //	领用申请新增接口
  requestionAddDelivery(storage){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token ,
      "type": "delivery_add",
      "data": {
        "sid": storage.sid,
        "type": storage.type,
        "create_time": storage.create_time,
        "urgency":storage.urgency,
        "remarks":storage.remarks,
        "approver":storage.approver,
        "approver_pid":storage.approver_pid,
        "org_approver ":storage.org_approver ,
        "inventories": storage.inventories
      }
    }).map(( res:Response) =>{
      if(res['errcode'] !=='00000'){
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }
  //领用申请修改接口
  updateDelivery(storage){
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token": token,
      "type": "delivery_mod",
      "data": {
        "type": storage.type,
        "org": storage.org,
        "org_oid": storage.org_oid,
        "approver":storage.approver,
        "approver_pid":storage.approver_pid,
        "operate_time": storage.operate_time,
        "return_time": storage.return_time,
        "operator": storage.operator,
        "operator_pid": storage.operator_pid,
        "urgency": storage.urgency,
        "remarks": storage.remarks,
        "inventoris": [
          {
            "pn": storage.pn,
            "name": storage.name,
            "catagory": storage.catagory,
            "status": storage.status,
            "model": storage.model,
            "brand": storage.brand,
            "specification": storage.specification,
            "location":storage.location,
            "keeper": storage.keeper,
            "keeper_pid": storage.keeper_pid,
            "count": storage.count
          }
        ]

      }
    }).map((res:Response) =>{
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //领用申请查询接口
  queryDelivery(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_get_byid",
      "id":material.id
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //领用总览接口
  queryRequestionView(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type": "receive_get",
      "data": {
        "condition":{
          "sid": material.condition.sid,
          "status": material.condition.status,
          "begin_time": material.condition.begin_time,
          "end_time": material.condition.end_time,
          "location": material.condition.location,
          "org": material.condition.org,
          "creator": material.condition.creator,
        },
        "page":{
          "page_size":material.page.page_size.trim(),
          "page_number":material.page.page_number.trim()
        }
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        // throw new Error(res['errmsg']);
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  //领用申请保存接口
  queryRequesttionDeliverySave(storage){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_save",
      "data": {
        "sid": storage.sid,
        "type": storage.type,
        "urgency":storage.urgency,
        "remarks": storage.remarks,
        "approver": storage.approver,
        "approver_pid": storage.approver_pid,
        "create_time": storage.create_time,
        "inventories":storage.inventories
      }
    }).map((res:Response)=>{

      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg'])
      }
      return res['data'];
    })
  }
  //入库位置调整接口
  modifiedLocation(material){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"entering_mod_location",
      "data":{
        "sid":material.sid,
        "inventories": material.inventories
      }
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        throw new Error(res['errmsg']);
      }
      return res['data'];
    })
  }
  //执行出库查看接口
  outboundShow(sid){
    let token =this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/inventory`,{
      "access_token":token,
      "type":"delivery_get_withonhandquantity_byid",
      "id": sid
    }).map((res:Response)=>{
      if(res['errcode'] !== '00000'){
        let reg = /No datas/;
        if(res['errmsg'].search(reg)!==-1){
          return [];
        }
      }
      return res['data'];
    })
  }
  deepClone(obj){
    var o,i,j,k;
    if(typeof(obj)!="object" || obj===null)return obj;
    if(obj instanceof(Array))
    {
      o=[];
      i=0;j=obj.length;
      for(;i<j;i++)
      {
        if(typeof(obj[i])=="object" && obj[i]!=null)
        {
          o[i]=this.deepClone(obj[i]);
        }
        else
        {
          o[i]=obj[i];
        }
      }
    }
    else
    {
      o={};
      for(i in obj)
      {
        if(typeof(obj[i])=="object" && obj[i]!=null)
        {
          o[i]=this.deepClone(obj[i]);
        }
        else
        {
          o[i]=obj[i];
        }
      }
    }
    return o;
  }
  formatDropdownData(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = temp['code'];
      newarr.push(obj);
    }
    return newarr
  }
  formatDropdownDatas(arr){
    let newarr = [];
    for(let i=0;i<arr.length;i++){
      let obj = {};
      let temp = arr[i];
      obj['label'] = temp['name'];
      obj['value'] = temp['name'];
      newarr.push(obj);
    }
    return newarr
  }
  changeObjectName(array, newName, oldName) {
    for (let i in array) {
      array[i][newName] = array[i][oldName];
      delete  array[i][oldName];
      this.changeObjectName( array[i].children, newName, oldName );
    }
    return array;
  }

  getFormatTime(date?) {
    if (!date){
      date= new Date();
    }else{
      date = new Date(date)
    }
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let hour = date.getHours();
    let minutes = date.getMinutes();
    let second = date.getSeconds();
    day = day <= 9 ? "0" + day : day;
    month = month <= 9 ? "0" + month : month;
    hour = hour <= 9 ? "0" + hour : hour;
    minutes = minutes <= 9 ? "0" + minutes : minutes;
    second = second <= 9 ? "0" + second : second;
    return  year+"-"+month+"-" +day+" " +hour+":"+minutes+":"+second

  }
}
