import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseMaterialNumberComponent } from './choose-material-number.component';

describe('ChooseMaterialNumberComponent', () => {
  let component: ChooseMaterialNumberComponent;
  let fixture: ComponentFixture<ChooseMaterialNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseMaterialNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseMaterialNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
