import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {InventoryService} from "../inventory.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-choose-material-number',
  templateUrl: './choose-material-number.component.html',
  styleUrls: ['./choose-material-number.component.scss']
})
export class ChooseMaterialNumberComponent implements OnInit {
    display: boolean =false;
    @Output() closeChooseMaterialNumber = new EventEmitter;
    @Output() addDev =  new EventEmitter();//新增

    cols;
    queryModel;
    totalRecords;
    MaterialData;
    chooseCids =[];
    selectMaterial=[];
  constructor( private inventoryService: InventoryService,
               private confirmationService: ConfirmationService ) { }

  ngOnInit() {
    this.display = true;
    this.queryModel={
      "condition":{
        "pn":"",
        "name":"",
        "status":"启用",
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.cols = [
      {field: 'pn', header: '物品编号'},
      {field: 'name', header: '物品名称'},
      {field: 'catagory', header: '物品分类'},
      {field: 'brand', header: '品牌'},
      {field: 'model', header: '型号'},
      {field: 'specification', header: '规格'},
      // {field: 'unit_cost', header: '单价'},
    ]
    this.queryMaterialDate();
  }

  clearSearch() {
    this.queryModel.condition.pn = '';
    this.queryModel.condition.name = '';
    this.queryModel.condition.status = '';


  }
  //搜索功能
  suggestInsepectiones(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryMaterialDate()
  }
  //分页查询
  paginate(event) {
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryMaterialDate();
  }
  queryMaterialDate(){
    this.inventoryService.queryMaterials(this.queryModel).subscribe(data=>{
      if(!this.MaterialData){
        this.MaterialData = [];
        this.selectMaterial =[];
        this.totalRecords = 0;

      }
      this.MaterialData = data.items;
      this.totalRecords = data.page.total;
    });
  }
  formSubmit(bool){
    // selectMaterial
     this.addDev.emit(this.selectMaterial[0])

    // this.closeChooseMaterialNumberMask(false);

  }
  closeChooseMaterialNumberMask(bool){
    this.closeChooseMaterialNumber.emit(bool);
  }
}
