import { Component, OnInit } from '@angular/core';
import {InventoryService} from "../inventory.service";
import {ConfirmationService} from "primeng/primeng";
import {ActivatedRoute, Router} from "@angular/router";
import {EventBusService} from "../../services/event-bus.service";
import {MessageService} from "primeng/components/common/messageservice";
import {BasePage} from "../../base.page";

@Component({
  selector: 'app-storage-overview',
  templateUrl: './storage-overview.component.html',
  styleUrls: ['./storage-overview.component.scss']
})
export class StorageOverviewComponent extends BasePage implements OnInit {
  cols;
  queryModel;
  chooseCids =[];
  MaterialData;
  totalRecords;
  selectMaterial=[];
  currentMaterial;
  statusNames = [];
  deletesid;
  zh;
  MaterialStatusData;
  constructor( private inventoryService: InventoryService,
               public confirmationService: ConfirmationService,
               public messageService:MessageService,
               private eventBusService:EventBusService,
               private router:Router,
               private route:ActivatedRoute,
) {
    super(confirmationService,messageService)
  }

  ngOnInit() {
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };

    this.queryModel={
      "condition":{
        "sid": "",
        "status": "",
        "begin_time": "",
        "end_time": "",
        "creator": ""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.cols = [
      // {field: 'sid', header: '入库单号'},
      {field: 'entering_time', header: '入库时间'},
      {field: 'creator', header: '入库经手人'},
      {field: 'location', header: '库存位置'},
      {field: 'status', header: '状态'},
      {field: 'approver', header: '审批人'},
      {field: 'audit_time', header: '审批时间'},
    ]
    this.eventBusService.updateborrow.subscribe(
      data=>{
        this.queryModel.condition.status = data;
        this.queryMaterialDate();
      }
    )
    this.queryMaterialDate();
    this.queryMaterialStatusDate();

  }
  clearSearch() {
    this.queryModel.condition.sid = '';
    this.queryModel.condition.begin_time = '';
    this.queryModel.condition.end_time = '';
    this.queryModel.condition.creator = '';

  }
  //搜索功能
  suggestInsepectiones(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryMaterialDate()
  }
  //分页查询
  paginate(event) {
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryMaterialDate();
  }
  queryMaterialDate(){
    this.inventoryService.queryStorageView(this.queryModel).subscribe(data=>{
      if(!data['items']){
        this.MaterialData = [];
        this.selectMaterial =[];
        this.totalRecords = 0;
      }else{
        this.MaterialData = data.items;

        this.totalRecords = data.page.total;
      }

    });
  }
  queryMaterialStatusDate(){
    this.inventoryService.queryInventoryStatus().subscribe(data=>{
      if(!data){
        this.MaterialStatusData = [];
      }else{
        this.MaterialStatusData = data;
        this.MaterialStatusData = this.inventoryService.formatDropdownData(this.MaterialStatusData)
      }
    });
  }
  updateOption(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../equipment'],{queryParams:{'sid':sid,title:'编辑',state:'update'},relativeTo:this.route})

  }
  //确认入库
  sureApproved(current){
    let sid = current['sid'];
    let status = current['status'];
    this.eventBusService.flow.next(current.status);
    this.confirmationService.confirm({
      message: '是否需要调整库存位置?',
      rejectVisible:true,
      accept: () => {
        this.router.navigate(['../location'],{queryParams:{'sid':sid,'status':status,title:'库存位置'},relativeTo:this.route})
      },
      reject:() =>{
        this.inventoryService.queryStorageSure(sid).subscribe(()=>{
          this.MaterialStatusData['status'] = "";
          this.eventBusService.flow.next(this.MaterialStatusData['status']);
          this.selectMaterial = [];
          this.queryMaterialDate();

        })
      }
    });
  }
  //审批
  storageApproved(current){
    let sid = current['sid'];
    this.eventBusService.flow.next(current.status);
    this.router.navigate(['../approved'],{queryParams:{'sid':sid},relativeTo:this.route})
  }
  deleteMaterial(current){
    let sid = current['sid'];
    //请求成功后删除本地数据
    this.confirm('确认删除吗?',()=>{this.deleteoption(sid)});
  }
  deleteoption(sid){
    this.inventoryService.deleteStorage(sid).subscribe(()=>{
      this.queryMaterialDate();
      this.alert('删除成功')
      // this.selectMaterial=[];
    },(err:Error)=>{
      let message ;
      if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
        message = '似乎网络出现了问题，请联系管理员或稍后重试'
      }else{
        message = err
      }
      this.confirmationService.confirm({
        message: message,
        rejectVisible:false,
      })
    })
  }
  //查看
  viewBorrowApproved(current){
    let sid = current['sid'];
    this.router.navigate(['../equipment'],{queryParams:{sid:sid,state:'viewDetail',title:'查看'},relativeTo:this.route})
  }
  onOperate(current) {
    let sid = current['sid'];
    // this.showViewDetailMask = !this.showViewDetailMask;
    this.router.navigate(['../equipment'],{queryParams:{sid:sid,state:'viewDetail',title:'查看'},relativeTo:this.route})
  }
}
