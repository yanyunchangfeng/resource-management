import { Component, OnInit } from '@angular/core';
import {InventoryService} from "../inventory.service";
import {ConfirmationService} from "primeng/primeng";
import {ActivatedRoute, Router} from "@angular/router";
import {StorageService} from "../../services/storage.service";
import {EventBusService} from "../../services/event-bus.service";
import {BasePage} from "../../base.page";
import {MessageService} from "primeng/components/common/messageservice";

@Component({
  selector: 'app-outbound-overview',
  templateUrl: './outbound-overview.component.html',
  styleUrls: ['./outbound-overview.component.scss']
})
export class OutboundOverviewComponent extends BasePage implements OnInit {
  cols;
  queryModel;
  chooseCids =[];
  MaterialData;
  totalRecords;
  selectMaterial=[];
  statusNames;
  zh;
  restartsid;
  currentMaterial;
  showRestart: boolean = false;
  currentBorrow;
  MaterialStatusData;
  constructor( private inventoryService: InventoryService,
               public confirmationService: ConfirmationService,
               public messageService:MessageService,
               private router:Router,
               private route:ActivatedRoute,
               private storageService: StorageService,
               private eventBusService: EventBusService
  ) {
    super(confirmationService,messageService);
  }
  ngOnInit() {
    this.zh = {
      firstDayOfWeek: 1,
      dayNames: [ "周一","周二","周三","周四","周五","周六","周日" ],
      dayNamesShort: [ "一","二","三","四","五","六","七" ],
      dayNamesMin: [ "一","二","三","四","五","六","七" ],
      monthNames: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月" ],
      monthNamesShort: [ "一","二","三","四","五","六","七","八","九","十","十一","十二" ],
    };
    this.statusNames = [
      {label: '全部', value: ''},
      {label: '新建', value: '新建'},
      {label: '待审批', value: '待审批'},
      {label: '待借用', value: '待借用'},
      {label: '待归还', value: '借用待归还'},
      {label: '已归还', value: '已归还'},
      {label: '拒绝', value: '拒绝'},
      {label: '驳回', value: '驳回'},
    ]
    this.queryModel={
      "condition":{
        "sid": "",
        "location": "",
        "status": "",
        "begin_time": "",
        "end_time": ""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.cols = [
      // {field: 'sid', header: '借用单号'},
      // {field: 'type', header: '申请类型'},
      {field: 'creator', header: '借用人'},
      {field: 'org', header: '部门名称'},
      {field: 'status', header: '状态'},
      {field: 'approver', header: '审批人'},
      {field: 'audit_time', header: '审批时间'},
      {field: 'return_time', header: '归还时间'},
    ];
    this.queryMaterialDate();
    this.eventBusService.updaterequestion.subscribe(
      data=>{
        this.queryModel.condition.status = data;
        this.queryMaterialDate();
      }
    )
    this.queryMaterialStatusDate();
  }
  updateOption(current){
    let sid = current['sid'];
    this.eventBusService.borrow.next(current.status);
    this.eventBusService.borrow.next(current.status);
    this.router.navigate(['../borrow'],{queryParams:{sid:sid,state:'update',title:'编辑'},relativeTo:this.route})
  }
  clearSearch() {
    this.queryModel.condition.begin_time = '';
    this.queryModel.condition.end_time = '';
    this.queryModel.condition.sid = '';
    this.queryModel.condition.org = '';


  }
  //查看
  viewBorrowApproved(current){
    let sid = current['sid'];
    this.eventBusService.borrow.next(current.status);
    this.router.navigate(['../borrow'],{queryParams:{sid:sid,state:'viewDetail',title:'查看'},relativeTo:this.route})
  }
  //搜索功能
  suggestInsepectiones(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryMaterialDate()
  }
  //分页查询
  paginate(event) {
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryMaterialDate();
  }
  queryMaterialStatusDate(){
    this.inventoryService.queryBorrowStatus().subscribe(data=>{
      if(!data){
        this.MaterialStatusData = [];
      }else{
        this.MaterialStatusData = data;
        this.MaterialStatusData = this.inventoryService.formatDropdownData(this.MaterialStatusData)
        console.log(this.MaterialStatusData)
      }
    });
  }
  queryMaterialDate(){
    this.inventoryService.queryOutband(this.queryModel).subscribe(data=>{
      if(!data['items']){
        this.MaterialData = [];
      }else{
        this.MaterialData = data.items;
        console.log(this.MaterialData)
        this.totalRecords = data.page.total;
      }
    });
  }
  deleteMaterial(current){
    // this.chooseCids = [];
    // for(let i = 0;i<this.selectMaterial.length;i++){
    //   this.chooseCids.push(this.selectMaterial[i]['sid'])
    // }
    //请求成功后删除本地数据
    this.chooseCids = [];
    this.chooseCids.push(current['sid']);
    this.confirmationService.confirm({
      message:'确认删除吗？',
      accept: () => {
        this.inventoryService.deleteDelivery(this.chooseCids).subscribe(()=>{
          this.queryMaterialDate();
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })

      },
      reject:() =>{
      }
    });

  }
  //审批
  borrowApproved(current){
    let sid = current['sid'];
    let status = current['status']
    this.eventBusService.borrow.next(current.status);
    this.router.navigate(['../borrowApproved'],{queryParams:{sid:sid,status:status,title:'审批'},relativeTo:this.route})
  }

  //借用归还
  deliveryReturn(current){
    let sid = current['sid'];
    this.eventBusService.borrow.next(current.status);

    this.confirmationService.confirm({
      message: '是否全部归还?',
      rejectVisible:true,
      accept: () => {
        this.inventoryService.DeliveryReturn(sid).subscribe(data=>{
          this.selectMaterial = [];
          this.queryMaterialDate();
        },(err:Error)=>{
          this.toastError(err);
        })
      },
      reject:() =>{}
    })
  }
  //部分借用归还
  someDeliveryReturn(current){
    let sid = current['sid'];
    this.eventBusService.borrow.next(current.status);
    this.router.navigate(['../borrow'],{queryParams:{sid:sid,state:'someReturn',title:'部分归还'},relativeTo:this.route})
  }
  //出库
  updateOutboundQuantity(current){
    let sid = current['sid'];
    this.eventBusService.borrow.next(current.status);
    this.router.navigate(['../borrow'],{queryParams:{sid:sid,state:'outbound',title:'出库'},relativeTo:this.route})
  }
  showRestartMask(borrow){
    this.currentBorrow = borrow;
    this.showRestart = !this.showRestart;
  }

  updateInventoryModel(bool){
    this.showRestart = bool;
    this.queryMaterialDate();
  }
  closeRestartMask(bool){
    this.showRestart = bool;
  }
}
