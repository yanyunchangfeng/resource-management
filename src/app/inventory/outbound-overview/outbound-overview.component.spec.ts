import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutboundOverViewComponent } from './outbound-overview.component';

describe('OutboundOverViewComponent', () => {
  let component: OutboundOverViewComponent;
  let fixture: ComponentFixture<OutboundOverViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutboundOverViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutboundOverViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
