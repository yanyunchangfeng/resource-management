import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {InventoryService} from "../inventory.service";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-flow-chart',
  templateUrl: './flow-chart.component.html',
  styleUrls: ['./flow-chart.component.scss']
})
export class FlowChartComponent implements OnInit {
  flowCharts;
  title = '入库总览';
  queryModel;
  MaterialCharData;
  constructor(
    public router:Router,
    public route:ActivatedRoute,
    public eventBus:EventBusService,
    private inventoryService: InventoryService
  ) { }

  ngOnInit() {
    this.queryModel={
        "status": "",
    };

    this.flowCharts = [
      {label:'入库申请',route:'equipment',stroke:"#39B9C6",fill:'#357CA5',active:true,x:'0',y:'0',rx:'10',width:100,height:40,title:'入库申请',text:{x:'20',y:'24',label:'入库申请'},g:[{x1:'50',y1:'40',x2:'50',y2:'100',},{x1:'50',y1:'100',x2:'43',y2:'90'},{x1:'50',y1:'100',x2:'58',y2:'90'}]},
      {label:'入库审批',route:'overview',status:"待审批",stroke:"#39B9C6",fill:'#357CA5',active:false,x:'0',y:'100',width:100,height:40,title:'入库审批',text:{x:'20',y:'124',label:'入库审批'},g:[{x1:'50',y1:'140',x2:'50',y2:'200'},{x1:'50',y1:'200',x2:'43',y2:'190'},{x1:'50',y1:'200',x2:'58',y2:'190'}]},
      {label:'库存位置',route:'overview',status:"待入库",stroke:"#39B9C6",fill:'#357CA5',active:true,x:'0',y:'200',width:100,height:40,title:'库存位置',text:{x:'20',y:'224',label:'制定库存位置'},g:[{x1:'50',y1:'240',x2:'50',y2:'300'},{x1:'50',y1:'300',x2:'43',y2:'290'},{x1:'50',y1:'300',x2:'58',y2:'290'}]},
      {label:'入库',route:'overview',stroke:"#39B9C6",fill:'#357CA5',x:'0',y:'300',rx:'10',width:100,height:40,title:'',text:{x:'30',y:'324',label:'入库'}}
    ]
    this.eventBus.flow.subscribe(data=>{
      console.log(data)
      this.queryModel.status = data;
      this.queryMaterialDate();
    })
    this.route.queryParams.subscribe(params=>{
      if(params['title']){
        this.title = params['title'];
      }
    });
    this.queryMaterialDate();
  }
  queryMaterialDate(){
    this.inventoryService.queryMaterialChar(this.queryModel).subscribe(data=>{
      if(!this.MaterialCharData){
        this.MaterialCharData = [];
      }
      this.MaterialCharData = data;
      for(let i = 0;i<this.MaterialCharData.length;i++){
        let temp = this.MaterialCharData[i];
        this.flowCharts[i]['fill'] = temp['fill'] ;
      }
    });
  }
  openPage(flow,index){
    if(index+1 === this.flowCharts.length)return
    for(let i = 0;i<this.flowCharts.length;i++){
      this.flowCharts[i].active = false;
    }
    this.flowCharts[index].active = true;
    this.queryMaterialDate();
    if(flow.status){
      this.eventBus.updateborrow.next(flow.status);
    }
    if(flow.route){
      this.router.navigate([flow.route],{relativeTo:this.route});
      this.title = flow.title;
    }
  }
}
