import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {InventoryOverviewComponent} from './inventory-overview/inventory-overview.component';
import {OutboundOverviewComponent} from './outbound-overview/outbound-overview.component';
import {StorageOverviewComponent} from './storage-overview/storage-overview.component';
import {EquipmentStorageComponent} from './equipment-storage/equipment-storage.component';
import {BorrowComponent} from './borrow/borrow.component';
import {RequisitionApplicationComponent} from './requisition-application/requisition-application.component';
import {MaterialMaintenanceComponent} from './material-maintenance/material-maintenance.component';
import {FlowChartComponent} from './flow-chart/flow-chart.component';
import {StorageApprovedComponent} from './storage-approved/storage-approved.component';
import {InventoryLocationComponent} from './inventory-location/inventory-location.component';
import {BorrowManagementComponent} from './borrow-management/borrow-management.component';
import {BorrowApprovedComponent} from './borrow-approved/borrow-approved.component';
import {RequestionManagementComponent} from './requestion-management/requestion-management.component';
import {RequeationApprovedComponent} from './requeation-approved/requeation-approved.component';
import {RequestionOverviewComponent} from './requestion-overview/requestion-overview.component';
// import {UpdateOutboundQuantityComponent} from "./update-outbound-quantity/update-outbound-quantity.component";

const route: Routes = [
  {path: 'inventory', component: InventoryOverviewComponent}, //库存总览
  {path: 'flow', component: FlowChartComponent, children: [
    {path: 'overview', component: StorageOverviewComponent}, //入库总览
    {path: 'approved', component: StorageApprovedComponent}, //入库审批
    {path: 'location', component: InventoryLocationComponent}, // 位置
    {path: 'equipment', component: EquipmentStorageComponent}, //入库申请
  ]}, //库存总览
  {path: 'borrowManage', component: BorrowManagementComponent, children: [
    {path: 'outbound', component: OutboundOverviewComponent}, //借用总览
    {path: 'borrow', component: BorrowComponent}, //借用申请
    {path: 'borrowApproved', component: BorrowApprovedComponent}, //借用审批
]},
  {path: 'requisitionmanage', component: RequestionManagementComponent, children: [
    {path: 'requestionOverview', component: RequestionOverviewComponent}, //领用总览
    {path: 'requisition', component: RequisitionApplicationComponent}, //领用申请
    {path: 'requisitionapproved', component: RequeationApprovedComponent}, //领用审批
    // {path: 'updateoutboundquantity', component: UpdateOutboundQuantityComponent}, //领用审批
  ]}, //领用

  {path: 'material', component: MaterialMaintenanceComponent}, //物料维护

];
@NgModule({
  imports: [RouterModule.forChild(route),
  ],
  exports: [RouterModule]
})
export class InventoryRoutingModule{

}
