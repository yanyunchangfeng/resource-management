import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestionManagementComponent } from './requestion-management.component';

describe('RequestionManagementComponent', () => {
  let component: RequestionManagementComponent;
  let fixture: ComponentFixture<RequestionManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestionManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestionManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
