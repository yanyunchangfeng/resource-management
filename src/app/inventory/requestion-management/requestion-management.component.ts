import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {InventoryService} from "../inventory.service";
import {EventBusService} from "../../services/event-bus.service";

@Component({
  selector: 'app-requestion-management',
  templateUrl: './requestion-management.component.html',
  styleUrls: ['./requestion-management.component.scss']
})
export class RequestionManagementComponent implements OnInit {


  flowCharts;
  title = '领用总览';
  MaterialCharData;
  queryModel;
  constructor(
    public router:Router,
    public route:ActivatedRoute,
    private inventoryService:InventoryService,
    private eventBus: EventBusService

  ) { }

  ngOnInit() {
    this.queryModel={
      "status": "",
    };
    this.flowCharts = [
      {label:'领用申请',route:'requisition',stroke:"#39B9C6",fill:'#357CA5',active:true,x:'0',y:'0',rx:'10',width:100,height:40,title:'领用申请',text:{x:'20',y:'24',label:'领用申请'},g:[{x1:'50',y1:'40',x2:'50',y2:'100'},{x1:'50',y1:'100',x2:'43',y2:'90'},{x1:'50',y1:'100',x2:'58',y2:'90'}]},
      {label:'领用审批',route:'requestionOverview',status:"待审批",stroke:"#39B9C6",fill:'#357CA5',active:false,x:'0',y:'100',width:100,height:40,title:'领用审批',text:{x:'20',y:'124',label:'领用审批'},g:[{x1:'50',y1:'140',x2:'50',y2:'200'},{x1:'50',y1:'200',x2:'43',y2:'190'},{x1:'50',y1:'200',x2:'58',y2:'190'}]},
      {label:'出库',route:'',active:true,x:'0',y:'200',rx:'10',stroke:"#39B9C6",fill:'#357CA5',width:100,height:40,title:'',text:{x:'34',y:'224',label:'归还'}}
    ];
    this.eventBus.requstion.subscribe(data=>{
      console.log(data)
      this.queryModel.status = data;
      this.queryMaterialDate();
    })
    this.route.queryParams.subscribe(params=>{
      if(params['title']){
        this.title = params['title']
      }

      if(params['sid']){
        this.inventoryService.queryDelivery(params['sid']).subscribe(data=>{
          console.log(data)
        })
      }

    });
    this.queryMaterialDate();
  }
  queryMaterialDate(){
    this.inventoryService.queryMaterialreceive(this.queryModel).subscribe(data=>{
      if(!this.MaterialCharData){
        this.MaterialCharData = [];
      }
      this.MaterialCharData = data;
      for(let i = 0;i<this.MaterialCharData.length;i++){
        let temp = this.MaterialCharData[i];
        this.flowCharts[i]['fill'] = temp['fill'] ;
      }
    });
  }
  openPage(flow,index){
    if(index+1 === this.flowCharts.length)return
    for(let i = 0;i<this.flowCharts.length;i++){
      this.flowCharts[i].active = false;
    }
    this.flowCharts[index].active = true;
    this.queryMaterialDate();
    if(flow.status){
      this.eventBus.updatereequipment.next(flow.status);
    }

    if(flow.route){
      this.router.navigate([flow.route],{relativeTo:this.route});
      this.title = flow.title;
    }
  }

}
