import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestionOverviewComponent } from './requestion-overview.component';

describe('RequestionOverviewComponent', () => {
  let component: RequestionOverviewComponent;
  let fixture: ComponentFixture<RequestionOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestionOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestionOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
