import { Component, OnInit } from '@angular/core';
import {InventoryService} from "../inventory.service";

@Component({
  selector: 'app-inventory-overview',
  templateUrl: './inventory-overview.component.html',
  styleUrls: ['./inventory-overview.component.scss']
})
export class InventoryOverviewComponent implements OnInit {
  cols;                     //定义表格表头
  queryModel;
  MaterialData;
  totalRecords;
  selectMaterial = [];
  chooseCids = [];
  statusNames = [];
  statusCategory = [];
  filesTree4;
  categories;

  constructor(private inventoryService: InventoryService) {
  }

  ngOnInit() {
    this.statusNames = [
      {label: '', value: ''},
      {label: '在库', value: '在库'},
      {label: '已完成', value: '已完成'},
    ]
    this.statusCategory = [
      {label: '', value: ''},
      {label: '整机', value: '整机'},
      {label: '部件', value: '部件'},
      {label: '耗材', value: '耗材'},
      {label: '其他', value: '其他'},
    ]
    this.cols = [
      // {field: 'id', header: '序号'},
      // {field: 'sid', header: '物品编号'},
      {field: 'name', header: '物品名称'},
      {field: 'catagory', header: '物品分类'},
      {field: 'model', header: '型号'},
      {field: 'specification', header: '规格'},
      {field: 'quantity', header: '数量'},
      {field: 'location', header: '库存位置'},
      {field: 'keeper', header: '库管员'},
      // {field: 'status', header: '状态'},
    ];
    this.queryModel = {
      "condition": {
        "name": "",
        "sid": "",
        "catagory": "",
        "count": "",
        "status": "",

      },
      "page": {
        "page_size": "10",
        "page_number": "1"
      }
    };
    this.queryMaterialDate();
    this.queryMeterial();
  }
  clearSearch() {
    this.queryModel.condition.name = '';
    this.queryModel.condition.sid = '';
    this.queryModel.condition.catagory = '';


  }

  //搜索功能
  suggestInsepectiones() {
    this.queryModel.page.page_number = '1'
    this.queryModel.page.page_size = '10';
    this.queryMaterialDate()
  }

  //分页查询
  paginate(event) {
    this.chooseCids = []
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryMaterialDate();
  }

  queryMaterialDate() {
    this.inventoryService.queryInventory(this.queryModel).subscribe(data => {
      if (!this.MaterialData) {
        this.MaterialData = [];
        this.selectMaterial = []
      }
      this.MaterialData = data.items;
      this.totalRecords = data.page.total;
    });
  }

  queryMeterial() {
    this.inventoryService.queryMaterialInfo().subscribe(data => {
      if (data['分类']) {
        this.categories = data['分类'];
        this.categories.unshift({name:'全部',code:''});
        this.categories = this.inventoryService.formatDropdownData(this.categories)
        console.log(this.categories)
      } else {
        this.categories = [];
      }
      // this.queryModel.catagory = this.categories[0]['label'];

    });
  }
}
