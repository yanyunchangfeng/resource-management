import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialMaintenanceComponent } from './material-maintenance.component';

describe('MaterialMaintenanceComponent', () => {
  let component: MaterialMaintenanceComponent;
  let fixture: ComponentFixture<MaterialMaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialMaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
