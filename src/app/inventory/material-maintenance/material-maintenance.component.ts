import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {InventoryService} from "../inventory.service";
import {ConfirmationService} from "primeng/primeng";
import {BasePage} from "../../base.page";
import {MessageService} from "primeng/components/common/messageservice";

@Component({
  selector: 'app-material-maintenance',
  templateUrl: './material-maintenance.component.html',
  styleUrls: ['./material-maintenance.component.scss']
})
export class MaterialMaintenanceComponent extends BasePage implements OnInit {
  showMaterial: boolean = false;
  cols;
  MaterialData;
  chooseCids =[];
  totalRecords;
  selectMaterial = [];
  currentMaterial;
  state;
  currentInspection;
  Materialstatus;
  constructor(
    private inventoryService: InventoryService,
    public confirmationService: ConfirmationService,
    public messageService:MessageService
    ) {
              super(confirmationService,messageService)
  }
 queryModel;
  ngOnInit() {

    this.queryModel={
      "condition":{
        "pn":"",
        "name":"",
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.cols = [
      {field: 'pn', header: '物品编号'},
      {field: 'name', header: '物品名称'},
      {field: 'catagory', header: '分类'},
      {field: 'supplier', header: '供应商'},
      {field: 'brand', header: '品牌'},
      {field: 'model', header: '型号'},
      {field: 'specification', header: '规格'},
      {field: 'status', header: '状态'},
      {field: 'threshold_group', header: '安全库存'},
    ];
    this.queryMaterialDate();
  }

  showMaterialMask(){
    this.state = 'add';
    this.showMaterial = !this.showMaterial;
  }
  closeMaterialMask(bool){
    this.showMaterial=(bool)
  }
  //添加成功
  addMaterial(bool){
    this.showMaterial = bool;
    this.queryMaterialDate()
  }
  //同步更新页面数据
  updateMaterial(){
    this.selectMaterial = []
    this.queryMaterialDate()
  }
  updateOption(){
    this.state = 'update'
    this.showMaterial = !this.showMaterial;
    this.currentMaterial = this.selectMaterial[0]
  }

  //分页查询
  paginate(event) {
    this.chooseCids =[];
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryMaterialDate();
  }

  queryMaterialDate(){
    this.inventoryService.queryMaterial(this.queryModel).subscribe(data=>{
        this.MaterialData = data.items;
      for(let i =0 ;i<this.MaterialData.length;i++){
        this.Materialstatus =this.MaterialData[i].status
      }

        this.totalRecords = data.page.total;
        this.selectMaterial = [];
      if(!data['items']){
        this.MaterialData = [];
        this.selectMaterial =[]
        // this.totalRecords = 0;
      }
    },(err:Error)=>{
      this.toastError(err)
    });
  }

  deleteMaterial(){
    this.chooseCids = [];
    for(let i = 0;i<this.selectMaterial.length;i++){
      this.chooseCids.push(this.selectMaterial[i]['pn']);
    }
    this.confirmationService.confirm({
      message:'确认删除吗?',
      accept: () => {
        this.inventoryService.deleteInventory(this.chooseCids).subscribe(()=>{
          this.queryMaterialDate();
          this.selectMaterial = [];
        },(err:Error)=>{
          let message ;
          if(JSON.parse(JSON.stringify(err)).status===0||JSON.parse(JSON.stringify(err)).status===504){
            message = '似乎网络出现了问题，请联系管理员或稍后重试'
          }else{
            message = err
          }
          this.confirmationService.confirm({
            message: message,
            rejectVisible:false,
          })
        })
      },
      reject:() =>{

      }
    });
    //请求成功后删除本地数据
  }
  enabled(current){
    console.log(current)
    this.currentMaterial = this.selectMaterial[0];
    this.currentMaterial.status = "启用";
    this.inventoryService.updateMaterial(this.currentMaterial).subscribe(()=>{
      this.queryMaterialDate();
      this.alert('启用成功');
    })
  }
  freeze(){
    this.currentMaterial = this.selectMaterial[0];
    this.currentMaterial.status = "冻结";
    this.inventoryService.updateMaterial(this.currentMaterial).subscribe(()=>{
      this.queryMaterialDate();
      this.alert('冻结成功')

    })
  }
}
