import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InventoryService} from "../inventory.service";

@Component({
  selector: 'app-borrow-items',
  templateUrl: './borrow-items.component.html',
  styleUrls: ['./borrow-items.component.scss']
})
export class BorrowItemsComponent implements OnInit {
  display: boolean =false;
  @Output() closeBorrowItemsMask = new EventEmitter;
  @Input() state;
  @Output() addDev =  new EventEmitter();//新增
  cols;
  queryModel;
  chooseCids =[];
  MaterialData;
  selectMaterial=[];
  totalRecords;
  windowSize;
  width;
  MaterialStatusData=[];
  constructor( private inventoryService: InventoryService) { }

  ngOnInit() {
    this.windowSize = window.innerWidth;
    if(this.windowSize<1024){
      this.width = this.windowSize*0.7;
    }else{
      this.width = this.windowSize*0.7;
    }
    this.display = true;
    this.cols = [
      {field: 'sid', header: '物品编号'},
      {field: 'name', header: '名称'},
      {field: 'catagory', header: '分类'},
      {field: 'status', header: '状态'},
      {field: 'quantity', header: '数量'},
      {field: 'brand', header: '品牌'},
      {field: 'model', header: '型号'},
      {field: 'location', header: '位置'},

    ];
    this.queryModel={
      "condition":{
        "sid":"",
        "name": "",
        "catagory": "",
        "count": "",
        "status": ""
      },
      "page":{
        "page_size":"10",
        "page_number":"1"
      }
    };
    this.queryMaterialDate();
    this.queryMaterialStatusDate();
  }
  clearSearch() {
    this.queryModel.condition.name = '';
    this.queryModel.condition.sid = '';


  }
  queryMaterialStatusDate(){
    this.inventoryService.queryReceiveStatus().subscribe(data=>{
      if(!data){
        this.MaterialStatusData = [];
      }else{
        this.MaterialStatusData = data;
        this.MaterialStatusData = this.inventoryService.formatDropdownData(this.MaterialStatusData)
        console.log(this.MaterialStatusData)
      }
    });
  }
  //搜索功能
  suggestInsepectiones(){
    this.queryModel.page.page_number ='1'
    this.queryModel.page.page_size ='10';
    this.queryMaterialDate()
  }
  //分页查询
  paginate(event) {
    this.chooseCids =[]
    this.queryModel.page.page_number = String(++event.page);
    this.queryModel.page.page_size = String(event.rows);
    this.queryMaterialDate();
  }
  queryMaterialDate(){
    this.inventoryService.queryInventory(this.queryModel).subscribe(data=>{
      if(!this.MaterialData){
        this.MaterialData = [];
        this.selectMaterial =[]
      }
      this.MaterialData = data.items;
      this.totalRecords = data.page.total;

    });
  }
  formSubmit(bool){
    this.addDev.emit(this.selectMaterial)
  }
  closeBorrowItems(bool){
    this.closeBorrowItemsMask.emit(bool);
  }
}
