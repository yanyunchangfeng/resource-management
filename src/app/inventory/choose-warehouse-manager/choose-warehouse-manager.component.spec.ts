import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseWarehouseManagerComponent } from './choose-warehouse-manager.component';

describe('ChooseWarehouseManagerComponent', () => {
  let component: ChooseWarehouseManagerComponent;
  let fixture: ComponentFixture<ChooseWarehouseManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseWarehouseManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseWarehouseManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
