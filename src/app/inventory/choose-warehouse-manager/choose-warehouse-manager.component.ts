import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-choose-warehouse-manager',
  templateUrl: './choose-warehouse-manager.component.html',
  styleUrls: ['./choose-warehouse-manager.component.css']
})
export class ChooseWarehouseManagerComponent implements OnInit {
  display:boolean = false;
  @Output() closeChooseWarehouse = new EventEmitter;
  cols;
  constructor() { }

  ngOnInit() {
    this.display =true;
    this.cols = [
      {field: 'Number', header: '账号'},
      {filed:'name', header: '名称'},
      {filed:'name', header: '名称'},
      {filed:'character', header: '角色'},
      {filed:'department', header: '部门'},

    ]
  }
  closeChooseWarehouseMask( bool){
    this.closeChooseWarehouse.emit(bool);
  }
}
