import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateOutboundQuantityComponent } from './update-outbound-quantity.component';

describe('UpdateOutboundQuantityComponent', () => {
  let component: UpdateOutboundQuantityComponent;
  let fixture: ComponentFixture<UpdateOutboundQuantityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateOutboundQuantityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateOutboundQuantityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
