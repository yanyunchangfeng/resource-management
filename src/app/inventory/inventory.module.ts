import { NgModule } from '@angular/core';
import { ShareModule} from '../shared/share.module';
import { InventoryRoutingModule} from './inventory-routing.module';
import { InventoryService} from './inventory.service';
import { OutboundOverviewComponent} from './outbound-overview/outbound-overview.component';
import { StorageOverviewComponent} from './storage-overview/storage-overview.component';
import { InventoryOverviewComponent} from './inventory-overview/inventory-overview.component';
import { EquipmentStorageComponent } from './equipment-storage/equipment-storage.component';
import { AddStorageComponent } from './add-storage/add-storage.component';
import { ChooseMaterialNumberComponent } from './choose-material-number/choose-material-number.component';
import { BorrowComponent } from './borrow/borrow.component';
import { RequisitionApplicationComponent } from './requisition-application/requisition-application.component';
import { ChooseWarehouseManagerComponent } from './choose-warehouse-manager/choose-warehouse-manager.component';
import { BorrowItemsComponent } from './borrow-items/borrow-items.component';
import { MaterialMaintenanceComponent } from './material-maintenance/material-maintenance.component';
import { AddOrUpdateMaterialComponent } from './add-or-update-material/add-or-update-material.component';
import { FlowChartComponent } from './flow-chart/flow-chart.component';
import { StorageApprovedComponent } from './storage-approved/storage-approved.component';
import { InventoryLocationComponent } from './inventory-location/inventory-location.component';
import { BorrowManagementComponent } from './borrow-management/borrow-management.component';
import { BorrowApprovedComponent } from './borrow-approved/borrow-approved.component';
import { RequestionManagementComponent } from './requestion-management/requestion-management.component';
import { RequestionOverviewComponent } from './requestion-overview/requestion-overview.component';
import { AddInventoryLocationComponent } from './add-inventory-location/add-inventory-location.component';
import { BorrowRestartComponent } from './borrow-restart/borrow-restart.component';
import { BorrowListComponent } from './borrow-list/borrow-list.component';
import { RequeationApprovedComponent } from './requeation-approved/requeation-approved.component';
import {PublicService} from '../services/public.service';
// import { UpdateOutboundQuantityComponent } from './update-outbound-quantity/update-outbound-quantity.component';

@NgModule({
  imports: [
    ShareModule,
    InventoryRoutingModule,
  ],
  declarations: [
    InventoryOverviewComponent,
    StorageOverviewComponent,
    OutboundOverviewComponent,
    EquipmentStorageComponent,
    AddStorageComponent,
    ChooseMaterialNumberComponent,
    BorrowComponent,
    RequisitionApplicationComponent,
    ChooseWarehouseManagerComponent,
    BorrowItemsComponent,
    MaterialMaintenanceComponent,
    AddOrUpdateMaterialComponent,
    FlowChartComponent,
    StorageApprovedComponent,
    InventoryLocationComponent,
    BorrowManagementComponent,
    BorrowApprovedComponent,
    RequestionManagementComponent,
    RequestionOverviewComponent,
    AddInventoryLocationComponent,
    BorrowRestartComponent,
    BorrowListComponent,
    RequeationApprovedComponent],

  providers: [InventoryService, PublicService]
})
export class InventoryModule {

}
