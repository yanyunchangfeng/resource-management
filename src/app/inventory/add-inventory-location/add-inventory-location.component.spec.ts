import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInventoryLocationComponent } from './add-inventory-location.component';

describe('AddInventoryLocationComponent', () => {
  let component: AddInventoryLocationComponent;
  let fixture: ComponentFixture<AddInventoryLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInventoryLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInventoryLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
