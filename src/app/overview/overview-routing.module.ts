import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SystemOverviewComponent} from './system-overview/system-overview.component';

const routes: Routes = [
    {path: 'sysoverview', component: SystemOverviewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OverviewRoutingModule { }
