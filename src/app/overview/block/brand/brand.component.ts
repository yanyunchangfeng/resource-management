import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit {
    @ViewChild('chart') chart: ElementRef;
    echart: any;
    option = {
        color: ['#6ACECE', '#507FD3', '#3CACC6', '#AC5CD6', '#AC8CD6', '#81BDC6', '#507FD3', '#6ACECE', '#6ACECE', '#3CACC6'],
        title: {
            text: '品牌统计',
            x: 'center',
            top: 20,
            textStyle: {
                fontSize: 14,
                fontWeight: 'normal'
            }
        },
        tooltip: {
            trigger: 'item',
            formatter: '{b} <br/> {c} ({d}%)'
        },
        legend: {
            orient: 'horizontal',
            y: 'bottom',
            data: ['华为', 'H3C', 'DELL', '联想', '博汇', '哈雷', 'PBI', 'TANDBERG', '九州', '其他']
        },
        series: [
            {
                name: '访问来源',
                type: 'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '30',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data: [
                    {value: 11, name: '华为'},
                    {value: 6, name: 'H3C'},
                    {value: 8, name: 'DELL'},
                    {value: 5, name: '联想'},
                    {value: 7, name: '博汇'},
                    {value: 8, name: '哈雷'},
                    {value: 5, name: 'PBI'},
                    {value: 6, name: 'TANDBERG'},
                    {value: 3, name: '九州'},
                    {value: 41, name: '其他'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

    constructor() { }

    ngOnInit() {
        this.echart = echarts.init(this.chart.nativeElement);
        this.echart.setOption(this.option);
    }
}
