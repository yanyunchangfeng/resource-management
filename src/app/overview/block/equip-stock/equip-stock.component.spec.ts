import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipStockComponent } from './equip-stock.component';

describe('EquipStockComponent', () => {
  let component: EquipStockComponent;
  let fixture: ComponentFixture<EquipStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
