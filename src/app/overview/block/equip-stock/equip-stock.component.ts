import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
@Component({
  selector: 'app-equip-stock',
  templateUrl: './equip-stock.component.html',
  styleUrls: ['./equip-stock.component.scss']
})
export class EquipStockComponent implements OnInit {
    @ViewChild('chart') chart: ElementRef;
    echart: any;
    option = {
        title: {
            text: '设备在库状态',
            x: 'center',
            top: 20,
            textStyle: {
                fontSize: 14,
                fontWeight: 'normal'
            }
        },
        color: ['#3CACC6'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data : [ 'A区在库', 'B区在库', 'C区在库', 'D区在库'],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                type: 'bar',
                barWidth: '40%',
                data: [100, 150, 82, 3]
            }
        ]
    };
    constructor() { }

    ngOnInit() {
        this.echart = echarts.init(this.chart.nativeElement);
        this.echart.setOption(this.option);
    }

}
