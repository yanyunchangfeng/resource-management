import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
@Component({
  selector: 'app-work-order',
  templateUrl: './work-order.component.html',
  styleUrls: ['./work-order.component.scss']
})
export class WorkOrderComponent implements OnInit {
    @ViewChild('chart') chart: ElementRef;
    echart: any;
    option = {
        color: ['#3CACC6', '#E2606D'],
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                crossStyle: {
                    color: '#999'
                }
            }
        },
        legend: {
            orient: 'horizontal',
            y: 'bottom',
            data: ['已处理', '未处理']
        },
        xAxis: [
            {
                type: 'category',
                data: ['报障单', '施工单', '上架单', '下架单', '出入单', '变更单', '巡检单'],
                axisPointer: {
                    type: 'shadow'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '单',
                min: 0,
                max: 35,
                interval: 7,
                axisLabel: {
                    formatter: '{value} '
                }
            }
        ],
        series: [
            {
                name: '已处理',
                type: 'bar',
                data: [20, 10, 20, 10, 12, 5, 30],
                barMaxWidth: '20%',
            },
            {
                name: '未处理',
                type: 'bar',
                data: [5, 3, 0, 0, 3, 0, 6],
                barMaxWidth: '20%',
            },
            // {
            //     name: '平均温度',
            //     type: 'line',
            //     yAxisIndex: 1,
            //     data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
            // }
        ]
    };


    constructor() { }

    ngOnInit() {
        this.echart = echarts.init(this.chart.nativeElement);
        this.echart.setOption(this.option);
    }


}
