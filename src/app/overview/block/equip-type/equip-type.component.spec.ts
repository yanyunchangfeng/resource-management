import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipTypeComponent } from './equip-type.component';

describe('EquipTypeComponent', () => {
  let component: EquipTypeComponent;
  let fixture: ComponentFixture<EquipTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
