import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
@Component({
  selector: 'app-equip-type',
  templateUrl: './equip-type.component.html',
  styleUrls: ['./equip-type.component.scss']
})
export class EquipTypeComponent implements OnInit {
    @ViewChild('chart') chart: ElementRef;
    echart: any;
    option = {
        color: ['#6ACECE', '#507FD3', '#3CACC6'],
        title: {
            text: '设备类型数量',
            x: 'center',
            top: 20,
            textStyle: {
                fontSize: 14,
                fontWeight: 'normal'
            }
        },
        tooltip: {
            trigger: 'item',
            formatter: '{b} <br/> {c} ({d}%)'
        },
        legend: {
            orient: 'horizontal',
            y: 'bottom',
            top: 'bottom',
            data: ['IT设备', '基础设施', '其他设备']
        },
        series: [
            {
                name: '访问来源',
                type: 'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '30',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data: [
                    {value: 400, name: 'IT设备'},
                    {value: 95, name: '基础设施'},
                    {value: 25, name: '其他设备'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

    constructor() { }

    ngOnInit() {
        this.echart = echarts.init(this.chart.nativeElement);
        this.echart.setOption(this.option);
    }


}
