import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefrigerationCapcityComponent } from './refrigeration-capcity.component';

describe('RefrigerationCapcityComponent', () => {
  let component: RefrigerationCapcityComponent;
  let fixture: ComponentFixture<RefrigerationCapcityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefrigerationCapcityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefrigerationCapcityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
