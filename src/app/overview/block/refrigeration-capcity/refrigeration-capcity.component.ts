import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
@Component({
  selector: 'app-refrigeration-capcity',
  templateUrl: './refrigeration-capcity.component.html',
  styleUrls: ['./refrigeration-capcity.component.scss']
})
export class RefrigerationCapcityComponent implements OnInit {
    @ViewChild('chart') chart: ElementRef;
    echart: any;
    option = {
        color: ['#3CACC6'],
        title: {
            text: '制冷容量',
            x: 'center',
            top: 20,
            textStyle: {
                fontSize: 14,
                fontWeight: 'normal'
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '8%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'value',
            boundaryGap: [0, 0.01]
        },
        yAxis: {
            type: 'category',
            data: ['可用制冷量', '额定制冷量']
        },
        series: [
            {
                type: 'bar',
                data: [480, 1500],
                barMaxWidth: '40%'
            }
        ]
    };


    constructor() { }

    ngOnInit() {
        this.echart = echarts.init(this.chart.nativeElement);
        this.echart.setOption(this.option);
    }

}
