import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinetCapcityComponent } from './cabinet-capcity.component';

describe('CabinetCapcityComponent', () => {
  let component: CabinetCapcityComponent;
  let fixture: ComponentFixture<CabinetCapcityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinetCapcityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinetCapcityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
