import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
@Component({
  selector: 'app-cabinet-capcity',
  templateUrl: './cabinet-capcity.component.html',
  styleUrls: ['./cabinet-capcity.component.scss']
})
export class CabinetCapcityComponent implements OnInit {
    @ViewChild('chart') chart: ElementRef;
    echart: any;
    option = {
        color: ['#3CACC6'],
        title: {
            text: '机柜容量',
            x: 'center',
            top: 20,
            textStyle: {
                fontSize: 14,
                fontWeight: 'normal'
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '8%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'value',
            boundaryGap: [0, 0.01]
        },
        yAxis: {
            type: 'category',
            data: ['可用机柜数', '总机柜数']
        },
        series: [
            {
                type: 'bar',
                data: [45, 400],
                barMaxWidth: '40%',
            }
        ]
    };


    constructor() { }

    ngOnInit() {
        this.echart = echarts.init(this.chart.nativeElement);
        this.echart.setOption(this.option);
    }

}
