import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinetPositionComponent } from './cabinet-position.component';

describe('CabinetPositionComponent', () => {
  let component: CabinetPositionComponent;
  let fixture: ComponentFixture<CabinetPositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinetPositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinetPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
