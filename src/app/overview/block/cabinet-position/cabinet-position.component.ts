import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
@Component({
  selector: 'app-cabinet-position',
  templateUrl: './cabinet-position.component.html',
  styleUrls: ['./cabinet-position.component.scss']
})
export class CabinetPositionComponent implements OnInit {
    @ViewChild('chart') chart: ElementRef;
    echart: any;
    option = {
        color: ['#6ACECE', '#507FD3', '#3CACC6', '#7658D6'],
        title: {
            text: '机柜区域容量',
            x: 'center',
            top: 20,
            textStyle: {
                fontSize: 14,
                fontWeight: 'normal'
            }
        },
        tooltip: {
            trigger: 'item',
            formatter: '{b} <br/> {c} ({d}%)'
        },
        legend: {
            orient: 'horizontal',
            y: 'bottom',
            top: 'bottom',
            data: ['A区', 'B区', 'C区', 'D区']
        },
        series: [
            {
                name: '访问来源',
                type: 'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '30',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data: [
                    {value: 70, name: 'A区'},
                    {value: 30, name: 'B区'},
                    {value: 50, name: 'C区'},
                    {value: 28, name: 'D区'},
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

    constructor() { }

    ngOnInit() {
        this.echart = echarts.init(this.chart.nativeElement);
        this.echart.setOption(this.option);
    }

}
