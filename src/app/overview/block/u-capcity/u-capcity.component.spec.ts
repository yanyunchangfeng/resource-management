import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UCapcityComponent } from './u-capcity.component';

describe('UCapcityComponent', () => {
  let component: UCapcityComponent;
  let fixture: ComponentFixture<UCapcityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UCapcityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UCapcityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
