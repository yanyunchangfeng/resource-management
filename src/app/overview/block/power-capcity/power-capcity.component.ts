import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as echarts from 'echarts';
@Component({
  selector: 'app-power-capcity',
  templateUrl: './power-capcity.component.html',
  styleUrls: ['./power-capcity.component.scss']
})
export class PowerCapcityComponent implements OnInit {
    @ViewChild('chart') chart: ElementRef;
    echart: any;
    option = {
        color: ['#3CACC6'],
        title: {
            text: '功率容量(KVA)',
            x: 'center',
            top: 20,
            textStyle: {
                fontSize: 14,
                fontWeight: 'normal'
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '8%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'value',
            boundaryGap: [0, 0.01]
        },
        yAxis: {
            type: 'category',
            data: ['可用功率', '总功率']
        },
        series: [
            {
                type: 'bar',
                data: [200, 1200],
                barMaxWidth: '40%',
            }
        ]
    };


    constructor() { }

    ngOnInit() {
        this.echart = echarts.init(this.chart.nativeElement);
        this.echart.setOption(this.option);
    }

}
