import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerCapcityComponent } from './power-capcity.component';

describe('PowerCapcityComponent', () => {
  let component: PowerCapcityComponent;
  let fixture: ComponentFixture<PowerCapcityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerCapcityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerCapcityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
