import { NgModule } from '@angular/core';

import { OverviewRoutingModule } from './overview-routing.module';
import { SystemOverviewComponent } from './system-overview/system-overview.component';
import {ShareModule} from '../shared/share.module';
import { EquipStockComponent } from './block/equip-stock/equip-stock.component';
import { CabinetPositionComponent } from './block/cabinet-position/cabinet-position.component';
import { EquipTypeComponent } from './block/equip-type/equip-type.component';
import { BrandComponent } from './block/brand/brand.component';
import { PowerCapcityComponent } from './block/power-capcity/power-capcity.component';
import { CabinetCapcityComponent } from './block/cabinet-capcity/cabinet-capcity.component';
import { UCapcityComponent } from './block/u-capcity/u-capcity.component';
import { RefrigerationCapcityComponent } from './block/refrigeration-capcity/refrigeration-capcity.component';
import { WorkOrderComponent } from './block/work-order/work-order.component';

@NgModule({
    imports: [
        ShareModule,
        OverviewRoutingModule
    ],
    declarations: [SystemOverviewComponent, EquipStockComponent, CabinetPositionComponent, EquipTypeComponent, BrandComponent, PowerCapcityComponent, CabinetCapcityComponent, UCapcityComponent, RefrigerationCapcityComponent, WorkOrderComponent]
})
export class OverviewModule { }
