import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccessMineOverviewComponent} from './access-mine-overview/access-mine-overview.component';
import {AccessBasicComponent} from './access-basic/access-basic.component';
import {AccessOverviewComponent} from './access-overview/access-overview.component';
import {CreateEditAccessComponent} from './create-edit-access/create-edit-access.component';
import {UnapprovalOverviewComponent} from './unapproval-overview/unapproval-overview.component';
import {UnreviewOverviewComponent} from './unreview-overview/unreview-overview.component';
import {FinishedOverviewComponent} from './finished-overview/finished-overview.component';
import {AccessApprovalComponent} from './access-approval/access-approval.component';
import {AccessReviewComponent} from './access-review/access-review.component';
import {ViewUnapprovalAccessComponent} from './view-unapproval-access/view-unapproval-access.component';
import {ViewUncloseAccessComponent} from './view-unclose-access/view-unclose-access.component';

const routes: Routes = [
    {path: 'mineoverview', component: AccessMineOverviewComponent},
    {path: 'basic', component: AccessBasicComponent, children: [
        {path: 'overview', component: AccessOverviewComponent},
        {path: 'mineoverview', component: AccessMineOverviewComponent},
        {path: 'createaccess', component: CreateEditAccessComponent},
        {path: 'unapprovaloverview', component: UnapprovalOverviewComponent},
        {path: 'unreviewoverview', component: UnreviewOverviewComponent},
        {path: 'finishedoverview', component: FinishedOverviewComponent},
        {path: 'approvalaccess', component: AccessApprovalComponent},
        {path: 'reviewaccess', component: AccessReviewComponent},
        {path: 'viewunapprovalaccess', component: ViewUnapprovalAccessComponent},
        {path: 'viewunreviewaccess', component: ViewUncloseAccessComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessRoutingModule { }
