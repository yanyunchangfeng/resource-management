import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessOverviewComponent } from './access-overview.component';

describe('AccessOverviewComponent', () => {
  let component: AccessOverviewComponent;
  let fixture: ComponentFixture<AccessOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
