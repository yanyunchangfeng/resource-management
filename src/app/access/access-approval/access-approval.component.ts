import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FormobjModel} from '../formobj.model';
import {PUblicMethod} from '../../services/PUblicMethod';
import {AccessService} from '../access.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/primeng';
import {BasePage} from '../../base.page';
import {UnaccessEquipTableComponent} from '../public/unaccess-equip-table/unaccess-equip-table.component';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-access-approval',
  templateUrl: './access-approval.component.html',
  styleUrls: ['./access-approval.component.scss']
})
export class AccessApprovalComponent extends  BasePage implements OnInit {
    @ViewChild('unaccessEquipTabel') unaccessEquipTabel: UnaccessEquipTableComponent;
    formTitle = '审批申请单';
    myForm: FormGroup;
    formObj: FormobjModel;

    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private accessService: AccessService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private eventBusService: EventBusService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new FormobjModel();
        this.initMyForm();
        this.getQueryParams();
    }

    initMyForm = () =>  {
        this.myForm = this.fb.group({
            approve_remarks: [null, Validators.required]
        });
    };

    goBack = () => {
        this.router.navigate(['../overview'], {relativeTo: this.activedRouter});
        this.eventBusService.accessFlowCharts.next('');
    };

    isFieldValid = name => {
        return PUblicMethod.isFieldValid(this.myForm, name);
    };

    operate = result => {
        this.formObj.status = result;
        if (!this.formObj.approver_remarks) {
            PUblicMethod.validateAllFormFields(this.myForm);
        } else {
            this.requestApprove();
        }
    };

    requestApprove = () =>  {
        this.accessService.requestApprove(this.formObj).subscribe(res => {
           if (res === '00000') {
                this.alert('操作成功');
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
           } else {
                this.alert(`操作失败${res}`, 'error');
           }
        });
    };

    unaccessTableDatasEimitter = device => {
        this.unaccessEquipTabel.tableDatas = device;
        this.unaccessEquipTabel.cols.splice(-1);
        this.unaccessEquipTabel.controlBtn = false;
    };

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.formObj.sid = params['sid']);
        });
    };

}
