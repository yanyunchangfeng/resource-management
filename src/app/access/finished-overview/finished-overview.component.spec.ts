import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishedOverviewComponent } from './finished-overview.component';

describe('FinishedOverviewComponent', () => {
  let component: FinishedOverviewComponent;
  let fixture: ComponentFixture<FinishedOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishedOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
