import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FormobjModel} from '../formobj.model';
import {PublicService} from '../../services/public.service';
import {UnaccessEquipTableComponent} from '../public/unaccess-equip-table/unaccess-equip-table.component';
import {AccessService} from '../access.service';
import {BasePage} from '../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';
import {ConfirmationService} from 'primeng/api';
import {PUblicMethod} from '../../services/PUblicMethod';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-access-review',
  templateUrl: './access-review.component.html',
  styleUrls: ['./access-review.component.scss']
})
export class AccessReviewComponent extends BasePage implements OnInit {
    @ViewChild('unaccessEquipTable') unaccessEquipTable: UnaccessEquipTableComponent;
    formTitle = '审核申请单';
    myForm: FormGroup;
    formObj: FormobjModel;
    auditor_result: string;


    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private publicService: PublicService,
                private accessService: AccessService,
                private eventBusService: EventBusService,
                public messageService: MessageService,
                public confirmationService: ConfirmationService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.formObj = new FormobjModel();
        this.initMyForm();
        this.auditor_result = '实物一致';
        this.initCreator();
    }

    initMyForm = () => {
        this.myForm = this.fb.group({
            plan_under_time: '',
            plan_finished_time: ''
        });
    };

    goBack = () => {
        this.router.navigate(['../overview'], {relativeTo: this.activedRouter});
        this.eventBusService.accessFlowCharts.next('');
    };

    initCreator = () => {
        this.publicService.getUser().subscribe(res => {
            // this.formObj.creator = res['name'];
            // this.formObj.creator_org = res['organization'];
        });
    };

    simpleReasonFormDatasEmitter = datas => {
        this.formObj = new FormobjModel(datas);
        this.unaccessEquipTable.tableDatas = datas['devices'];
        this.unaccessEquipTable.cols.splice(-1);
        this.unaccessEquipTable.controlBtn = false;
    };

    operate = result => {
        this.formObj.status = result;
        this.formObj.auditor_result = this.auditor_result;
        this.requestReview();
    };

    requestReview = () => {
        this.accessService.requestReview(this.formObj).subscribe(res => {
            if (res === '00000') {
                this.alert('操作成功');
                window.setTimeout(() => {
                    this.goBack();
                }, 1100);
            } else {
                this.alert(`操作失败${res}`, 'error');
            }
        });
    };
}
