import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessImportEquipsComponent } from './access-import-equips.component';

describe('AccessImportEquipsComponent', () => {
  let component: AccessImportEquipsComponent;
  let fixture: ComponentFixture<AccessImportEquipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessImportEquipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessImportEquipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
