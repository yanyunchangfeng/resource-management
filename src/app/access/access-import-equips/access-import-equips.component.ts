import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfirmationService} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';
import {BasePage} from '../../base.page';
import {PUblicMethod} from '../../services/PUblicMethod';
import {PublicService} from '../../services/public.service';
import {AssetObjModel} from '../assetObj.model';
import {Message} from 'primeng/api';

@Component({
  selector: 'app-access-import-equips',
  templateUrl: './access-import-equips.component.html',
  styleUrls: ['./access-import-equips.component.scss']
})
export class AccessImportEquipsComponent  extends BasePage implements OnInit {
    display: boolean = false;
    zh;
    width;
    windowSize;
    @Output() importedEquip: EventEmitter<any> = new EventEmitter();
    @Output() closeAddMask = new EventEmitter();
    @Output() updateDev = new EventEmitter();
    @Input() currentAsset;
    @Input() state;
    @Input() roomObj: object;
    title = '设备导入';
    submitAddAsset = new AssetObjModel();
    qureyModel = {
        field: '',
        value: ''
    };
    brandoptions;
    modleoptions;
    functionoptions;
    systemoptions;
    statusoptions;
    sizeoptions;
    roomoptions;
    cabinetoptions;
    locationoptions;
    belong_dapartoptions;
    belong_dapart_manageroptions;
    manage_dapartoptions;
    manageroptions;
    power_leveloptions;
    power_typeoptions;
    plug_typeoptions;
    power_redundant_modeloptions;
    power_sourceoptions;
    power_jack_position1options;
    power_jack_position2options;
    power_jack_position3options;
    power_jack_position4options;
    operating_systemoptions;
    maintain_venderoptions;
    maintain_vender_peopleoptions;

    msgs: Message[] = [];
    constructor(
        public confirmationService: ConfirmationService,
        public messageService: MessageService,
        private publicService: PublicService
    ) {
        super(confirmationService, messageService);
    }
    ngOnInit(){
        this.windowSize = window.innerWidth;
        if (this.windowSize < 1024){
            this.width = this.windowSize * 0.9;
        }else{
            this.width = this.windowSize * 0.8;
        }
        this.zh = new PUblicMethod().initZh();
        this.currentAsset = PUblicMethod.deepClone(this.currentAsset);
        if (this.state === 'add'){
            this.title = '添加设备';
        }
        this.display = true;

    }
    closeAddOrUpdateMask(bool){
        this.closeAddMask.emit(bool);
    }
    querySuggestData(obj, name){
        for (let key in obj){
            obj[key] += '';
            obj[key] = obj[key].trim();
        }
        this.publicService.searchChange(obj).subscribe(
            data => {
                switch (name){
                    case 'brand':
                        this.brandoptions = data ? data : [];
                        break;
                    case 'modle':
                        this.modleoptions = data ? data : [];
                        break;
                    case 'function':
                        this.functionoptions = data ? data : [];
                        break;
                    case 'system':
                        this.systemoptions = data ? data : [];
                        break;
                    case 'status':
                        this.statusoptions = data ? data : [];
                        break;
                    case 'size':
                        this.sizeoptions = data ? data : [];
                        break;
                    case 'room':
                        this.roomoptions = data ? data : [];
                        break;
                    case 'cabinet':
                        this.cabinetoptions = data ? data : [];
                        break;
                    case 'location':
                        this.locationoptions = data ? data : [];
                        break;
                    case 'belong_dapart':
                        this.belong_dapartoptions = data ? data : [];
                        break;
                    case 'belong_dapart_manager':
                        this.belong_dapart_manageroptions = data ? data : [];
                        break;
                    case 'manage_dapart':
                        this.manage_dapartoptions = data ? data : [];
                        break;
                    case 'manager':
                        this.manageroptions = data ? data : [];
                        break;
                    case 'power_level':
                        this.power_leveloptions = data ? data : [];
                        break;
                    case 'power_type':
                        this.power_typeoptions = data ? data : [];
                        break;
                    case 'plug_type':
                        this.plug_typeoptions = data ? data : [];
                        break;
                    case 'power_redundant_model':
                        this.power_redundant_modeloptions = data ? data : [];
                        break;
                    case 'power_source':
                        this.power_sourceoptions = data ? data : [];
                        break;
                    case 'power_jack_position1':
                        this.power_jack_position1options = data ? data : [];
                        break;
                    case 'power_jack_position2':
                        this.power_jack_position2options = data ? data : [];
                        break;
                    case 'power_jack_position3':
                        this.power_jack_position3options = data ? data : [];
                        break;
                    case 'power_jack_position4':
                        this.power_jack_position4options = data ? data : [];
                        break;
                    case 'operating_system':
                        this.operating_systemoptions = data ? data : [];
                        break;
                    case 'maintain_vender':
                        this.maintain_venderoptions = data ? data : [];
                        break;
                    case 'maintain_vender_people':
                        this.maintain_vender_peopleoptions = data ? data : [];
                }
            }
        );
    }
    searchSuggest(searchText, name){
        this.qureyModel = {
            field: name,
            value: searchText.query
        };
        this.querySuggestData(this.qureyModel, name);
    }

    updateDevice(bool){
        for (let key in this.currentAsset){
            this.currentAsset[key] = String(this.currentAsset[key]).trim();
        }
        this.publicService.updateAssets(this.currentAsset).subscribe(() => {
            this.updateDev.emit(bool);
        }, (err: Error) => {
            this.toastError(err);
        });
    }
    addDevice(bool){
        for (let key in this.submitAddAsset){
            this.submitAddAsset[key] = this.submitAddAsset[key].trim();
        }
        this.importEquip();
    }
    closeMask(bool){
        this.closeAddMask.emit(bool);
    }
    formSubmit(bool){
        if (this.state === 'update'){
            this.updateDevice(bool);
        }else{
            this.addDevice(bool);
        }
    }

    importEquip = () => {
        this.submitAddAsset.room = this.roomObj['did'];
        this.requestImprotEqupement();
    };

    showError = (msg, severity = 'success') => {
        this.messageService.add({severity: severity, summary: '消息提示', detail: msg});
    };

    clear = () => {
        this.messageService.clear();
    };

    requestImprotEqupement = () => {
        this.publicService.importEquipment(this.submitAddAsset).subscribe(res => {
           if (res['errcode'] === '00000') {
               this.importedEquip.emit(res['datas']);
               this.showError('设备导入成功！');
               this.closeAddOrUpdateMask(false);
           } else {
               this.showError(`设备导入失败！+ ${res['errmsg']}`, 'error');
           }
        });
    };
}
