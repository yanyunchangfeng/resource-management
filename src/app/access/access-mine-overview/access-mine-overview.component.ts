import { Component, OnInit } from '@angular/core';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-access-mine-overview',
  templateUrl: './access-mine-overview.component.html',
  styleUrls: ['./access-mine-overview.component.scss']
})
export class AccessMineOverviewComponent implements OnInit {
    searchType: string = 'mine';
    constructor(  private eventBusService: EventBusService) { }

    ngOnInit() {
        this.eventBusService.accessFlowCharts.next('');
    }

}
