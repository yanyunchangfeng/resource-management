import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessMineOverviewComponent } from './access-mine-overview.component';

describe('AccessMineOverviewComponent', () => {
  let component: AccessMineOverviewComponent;
  let fixture: ComponentFixture<AccessMineOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessMineOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessMineOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
