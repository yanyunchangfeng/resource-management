import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TestFormObjModel} from '../testFormObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AccessSimpleApprovalFormComponent} from '../public/access-simple-approval-form/access-simple-approval-form.component';
import {FormobjModel} from '../formobj.model';
import {UnaccessEquipTableComponent} from '../public/unaccess-equip-table/unaccess-equip-table.component';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-view-unclose-access',
  templateUrl: './view-unclose-access.component.html',
  styleUrls: ['./view-unclose-access.component.scss']
})
export class ViewUncloseAccessComponent implements OnInit {
    @ViewChild('accessSimpleApprovalForm') accessSimpleApprovalForm: AccessSimpleApprovalFormComponent;
    @ViewChild('unaccessEquipTable') unaccessEquipTable: UnaccessEquipTableComponent;
    formTitle = '查看申请单';
    myForm: FormGroup;
    formObj: TestFormObjModel;
    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.formObj = new TestFormObjModel();
        this.initMyForm();
    }
    initMyForm = ()  => {
        this.myForm = this.fb.group({
            plan_under_time: '',
            plan_finished_time: ''
        });
    };

    goBack = () => {
        this.eventBusService.accessFlowCharts.next('');
        this.router.navigate(['../overview'], {relativeTo: this.activedRouter});
    };

    simpleReasonFormDatas = datas => {
        this.accessSimpleApprovalForm.formObj = new FormobjModel(datas);
        this.unaccessEquipTable.tableDatas = datas['devices'];
        this.unaccessEquipTable.cols.splice(-1);
        this.unaccessEquipTable.controlBtn = false;
    }
}
