import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUncloseAccessComponent } from './view-unclose-access.component';

describe('ViewUncloseAccessComponent', () => {
  let component: ViewUncloseAccessComponent;
  let fixture: ComponentFixture<ViewUncloseAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUncloseAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUncloseAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
