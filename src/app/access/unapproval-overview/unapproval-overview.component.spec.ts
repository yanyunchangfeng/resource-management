import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnapprovalOverviewComponent } from './unapproval-overview.component';

describe('UnapprovalOverviewComponent', () => {
  let component: UnapprovalOverviewComponent;
  let fixture: ComponentFixture<UnapprovalOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnapprovalOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnapprovalOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
