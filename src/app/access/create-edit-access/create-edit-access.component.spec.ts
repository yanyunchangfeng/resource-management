import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditAccessComponent } from './create-edit-access.component';

describe('CreateEditAccessComponent', () => {
  let component: CreateEditAccessComponent;
  let fixture: ComponentFixture<CreateEditAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
