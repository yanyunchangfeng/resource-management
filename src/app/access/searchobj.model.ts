export class SearchobjModel {
    sid: string;
    creator: string;
    begin_time: string;
    end_time: string;
    status: string;
    page_size: string;
    page_number: string;

    constructor(obj?) {
        this.sid = obj && obj['sid'] || '';
        this.creator = obj && obj['creator'] || '';
        this.begin_time = obj && obj['begin_time'] || '';
        this.end_time = obj && obj['end_time'] || '';
        this.status = obj && obj['status'] || '';
        this.page_size = obj && obj['page_size'] || '10';
        this.page_number = obj && obj['page_number'] || '1';
    }
}
