import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../services/event-bus.service';
import {AccessService} from '../access.service';

@Component({
  selector: 'app-access-basic',
  templateUrl: './access-basic.component.html',
  styleUrls: ['./access-basic.component.scss']
})
export class AccessBasicComponent implements OnInit {
    flowChartDatas: Array<object> =  [];
    searchType: string = '';

    constructor(private router: Router,
                private activedRouter: ActivatedRoute,
                private eventBusService: EventBusService,
                private accessService: AccessService) { }

    ngOnInit() {
        this.getFlowChart();

        this.eventBusService.accessMineOverview.subscribe(data => {
            this.searchType = data;
        });

        this.eventBusService.accessFlowCharts.subscribe(status => {
              this.getFlowChart(status);
        });
    }

    jumper(num) {
        switch (num.toString()){
            case '1' :
                this.router.navigate(['./createaccess'], { relativeTo: this.activedRouter});
                break;
            case '2':
                this.router.navigate(['./unapprovaloverview'], {queryParams: {status: 'approve', searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '3':
                this.router.navigate(['./unreviewoverview'], {queryParams: {status: 'auditor', searchType: this.searchType}, relativeTo: this.activedRouter});
                break;
            case '4':
                this.router.navigate(['./finishedoverview'], {queryParams: {status: 'close', searchType: this.searchType}, relativeTo: this.activedRouter});
        }

    }

    getFlowChart = (status?) => {
        this.accessService.getFlowChart(status).subscribe(res => {
           this.flowChartDatas = res;
        });
    }

}
