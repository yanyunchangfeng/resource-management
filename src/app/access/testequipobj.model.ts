export class TestequipobjModel {
    id: string;
    name: string;
    position: string;
    constructor(obj?) {
        this.id = obj && obj.id || '';
        this.name = obj && obj.name || '';
        this.position = obj && obj.position || '';
    }
}