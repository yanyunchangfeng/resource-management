export class TestSearchFormModel {
    sid: string;
    applicant: string;
    status: string;

    constructor(obj?: any) {
        this.status = obj && obj.status || '';
        this.sid = obj && obj.sid || '';
        this.applicant = obj && obj.applicant || '';
    }
}