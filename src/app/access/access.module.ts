import { NgModule } from '@angular/core';

import { AccessRoutingModule } from './access-routing.module';
import { CreateEditAccessComponent } from './create-edit-access/create-edit-access.component';
import { AccessImportEquipsComponent } from './access-import-equips/access-import-equips.component';
import { AccessOverviewComponent } from './access-overview/access-overview.component';
import { AccessApprovalComponent } from './access-approval/access-approval.component';
import { AccessReviewComponent } from './access-review/access-review.component';
import { ViewUnapprovalAccessComponent } from './view-unapproval-access/view-unapproval-access.component';
import { ViewUncloseAccessComponent } from './view-unclose-access/view-unclose-access.component';
import { AccessSimpleReasonFormComponent } from './public/access-simple-reason-form/access-simple-reason-form.component';
import { AccessSimpleApprovalFormComponent } from './public/access-simple-approval-form/access-simple-approval-form.component';
import { AccessMineOverviewComponent } from './access-mine-overview/access-mine-overview.component';
import { AccessSearchFormComponent } from './public/access-search-form/access-search-form.component';
import { AccessOverviewTableComponent } from './public/access-overview-table/access-overview-table.component';
import { UnaccessEquipTableComponent } from './public/unaccess-equip-table/unaccess-equip-table.component';
import { UnapprovalOverviewComponent } from './unapproval-overview/unapproval-overview.component';
import { UnreviewOverviewComponent } from './unreview-overview/unreview-overview.component';
import { AccessBasicComponent } from './access-basic/access-basic.component';
import {ShareModule} from '../shared/share.module';
import { FinishedOverviewComponent } from './finished-overview/finished-overview.component';
import {PublicModule} from '../public/public.module';
import { AccessSelectEquipmentsComponent } from './public/access-select-equipments/access-select-equipments.component';
import {PublicService} from '../services/public.service';
import {AccessService} from './access.service';
import { BtnAccessEditComponent } from './public/btn-access-edit/btn-access-edit.component';
import { BtnAccessApproveComponent } from './public/btn-access-approve/btn-access-approve.component';
import { BtnAccessDeleteComponent } from './public/btn-access-delete/btn-access-delete.component';
import { BtnAccessReviewComponent } from './public/btn-access-review/btn-access-review.component';
import { BtnAccessRemoveComponent } from './public/btn-access-remove/btn-access-remove.component';

@NgModule({
  imports: [
      ShareModule,
      AccessRoutingModule,
      PublicModule
  ],
  declarations: [CreateEditAccessComponent, AccessImportEquipsComponent, AccessOverviewComponent, AccessApprovalComponent, AccessReviewComponent, ViewUnapprovalAccessComponent, ViewUncloseAccessComponent, AccessSimpleReasonFormComponent, AccessSimpleApprovalFormComponent, AccessMineOverviewComponent, AccessSearchFormComponent, AccessOverviewTableComponent, UnaccessEquipTableComponent, UnapprovalOverviewComponent, UnreviewOverviewComponent, AccessBasicComponent, FinishedOverviewComponent, AccessSelectEquipmentsComponent, BtnAccessEditComponent, BtnAccessApproveComponent, BtnAccessDeleteComponent, BtnAccessReviewComponent, BtnAccessRemoveComponent],
    providers: [
        PublicService,
        AccessService
    ]
})
export class AccessModule { }
