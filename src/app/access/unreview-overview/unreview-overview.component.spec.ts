import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnreviewOverviewComponent } from './unreview-overview.component';

describe('UnreviewOverviewComponent', () => {
  let component: UnreviewOverviewComponent;
  let fixture: ComponentFixture<UnreviewOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnreviewOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnreviewOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
