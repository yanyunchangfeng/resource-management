import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnAccessRemoveComponent } from './btn-access-remove.component';

describe('BtnAccessRemoveComponent', () => {
  let component: BtnAccessRemoveComponent;
  let fixture: ComponentFixture<BtnAccessRemoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnAccessRemoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnAccessRemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
