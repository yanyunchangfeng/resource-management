import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnAccessApproveComponent } from './btn-access-approve.component';

describe('BtnAccessApproveComponent', () => {
  let component: BtnAccessApproveComponent;
  let fixture: ComponentFixture<BtnAccessApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnAccessApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnAccessApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
