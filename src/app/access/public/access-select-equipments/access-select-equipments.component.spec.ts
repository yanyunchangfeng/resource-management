import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessSelectEquipmentsComponent } from './access-select-equipments.component';

describe('AccessSelectEquipmentsComponent', () => {
  let component: AccessSelectEquipmentsComponent;
  let fixture: ComponentFixture<AccessSelectEquipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessSelectEquipmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessSelectEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
