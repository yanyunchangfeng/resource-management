import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TestequipobjModel} from '../../testequipobj.model';
import {SelectEquipObjModel} from '../../../public/util/selectEquipObj.model';
import {PublicService} from '../../../services/public.service';

@Component({
  selector: 'app-access-select-equipments',
  templateUrl: './access-select-equipments.component.html',
  styleUrls: ['./access-select-equipments.component.scss']
})
export class AccessSelectEquipmentsComponent implements OnInit {
    @Input() display: boolean;
    @Input() roomDid: string;    // 房间归属：由调用的组件传入
    @Output() displayEmitter: EventEmitter<any> = new EventEmitter();
    @Output() selectedEmitter: EventEmitter<any> = new EventEmitter();
    allStatus: Array<object>;
    searchObj: SelectEquipObjModel;
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数
    selectEquip = [];
    width: string;
    equipObj: TestequipobjModel;
  totalRecords;

    constructor(private publicService: PublicService) {
        this.allStatus = [
            {
                'label': '全部',
                'value': '全部'
            },
            {
                'label': '在线',
                'value': '在线'
            },
            {
                'label': '不在线',
                'value': '不在线'
            },
            {
                'label': '在架',
                'value': '在架'
            }
        ];
    }

    ngOnInit() {
        this.searchObj = new SelectEquipObjModel();
        this.equipObj = new TestequipobjModel();
        this.initWidth();
    }

    initWidth = () => {
        let windowInnerWidth = window.innerWidth;
        if (windowInnerWidth < 1024) {
            this.width = (windowInnerWidth * 0.7).toString();
        }else {
            this.width = (windowInnerWidth * 0.7).toString();
        }
    };

    clearSearch = () => {
        this.searchObj.ano = '';
        this.searchObj.name = '';
        this.searchObj.status = '';
    };

    cancel = () =>{
        this.onHide();
    };

    sure = ()  => {
        this.selectedEmitter.emit(this.selectEquip);
        this.onHide();
    };

    onHide = () => {
        this.display = false;
        this.displayEmitter.emit(this.display);
        this.selectEquip = [];
        this.scheduleDatas = [];
    };

    searchSchedule = () => {
        // this.searchObj.room = this.roomDid;
        // console.log(this.searchObj);
       this.queryEqyupmentst();
    };

    resetPage = (res) => {
        if ( 'items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    };

    paginate = (event)  => {
      this.searchObj.page_number = String(++event.page);
      this.searchObj.page_size =  String(event.rows);
        this.queryEqyupmentst();
    };

    queryEqyupmentst = () => {
        this.publicService.queryEquipments(this.searchObj).subscribe(res => {
            if (res) {
                this.resetPage(res);
              this.totalRecords = res.page.total;

            }else {
                this.scheduleDatas = [];
            }
        });
    }

}
