import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessSearchFormComponent } from './access-search-form.component';

describe('AccessSearchFormComponent', () => {
  let component: AccessSearchFormComponent;
  let fixture: ComponentFixture<AccessSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessSearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
