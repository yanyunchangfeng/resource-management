import { Component, OnInit } from '@angular/core';
import {AccessService} from '../../access.service';
import {SearchobjModel} from '../../searchobj.model';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-access-search-form',
  templateUrl: './access-search-form.component.html',
  styleUrls: ['./access-search-form.component.scss']
})
export class AccessSearchFormComponent implements OnInit {

    allStatus: Array<object> = [];
    searchObj: SearchobjModel;

    constructor(private accessService: AccessService,
                private eventBusService: EventBusService) {
    }

    ngOnInit() {
        this.searchObj = new SearchobjModel();
        this.getAllAccessStatus();
    }
    getAllAccessStatus = () => {
        this.accessService.getAllAccessStatus().subscribe(res => {
            this.allStatus = res;
            this.searchObj.status = res[0]['value'];
        });
    }
    clearSearch() {
        this.searchObj.sid = '';
        this.searchObj.creator = '';
        this.searchObj.status = '';
    }
    searchSchedule() {
        this.eventBusService.accessSearchOverview.next(this.searchObj);
    }
}
