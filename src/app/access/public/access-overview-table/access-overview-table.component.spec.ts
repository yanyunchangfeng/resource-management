import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessOverviewTableComponent } from './access-overview-table.component';

describe('AccessOverviewTableComponent', () => {
  let component: AccessOverviewTableComponent;
  let fixture: ComponentFixture<AccessOverviewTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessOverviewTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessOverviewTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
