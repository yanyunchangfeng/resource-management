import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccessService} from '../../access.service';
import {SearchobjModel} from '../../searchobj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-access-overview-table',
  templateUrl: './access-overview-table.component.html',
  styleUrls: ['./access-overview-table.component.scss']
})
export class AccessOverviewTableComponent implements OnInit {
    @Input() searchType;
    searchObj: SearchobjModel;
    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数

    constructor(private accessService: AccessService,
                private activatedRoute: ActivatedRoute,
                private eventBusService: EventBusService,
                private router: Router) { }

    ngOnInit() {
        this.searchObj = new SearchobjModel();
        this.getQueryParam();
        this.getDatas();
        this.eventBusService.accessSearchOverview.subscribe(res => {
           this.searchObj = res;
           this.getAccessOverview();
        });
    }

    getQueryParam = () => {
        this.activatedRoute.queryParams.subscribe(res => {
            (res['status']) && (this.searchObj.status = res['status']);
            (res['searchType']) && (this.searchType = res['searchType']);
        });
    };

    getDatas = () => {
        if (this.searchType === 'all') {
            this.eventBusService.accessMineOverview.next('all');
            this.getAccessOverview();
        }
        if (this.searchType === 'mine') {
            this.eventBusService.accessMineOverview.next('mine');
            this.getMineAccessOverview();
        }
    };

    getAccessOverview = () => {
      this.accessService.getAcessOverview(this.searchObj).subscribe(res => {
          if (res) {
              this.resetPage(res);
          }else {
              this.scheduleDatas = [];
          }
      });
    };

    getMineAccessOverview = () => {
        this.accessService.getMineAcessOverview(this.searchObj).subscribe(res => {
            if (res) {
                this.resetPage(res);
            }else {
                this.scheduleDatas = [];
            }
        });
    };

    resetPage = res => {
        if ( 'items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    };

    paginate = event => {
        this.searchObj.page_size = event.rows.toString();
        this.searchObj.page_number = (event.page + 1).toString();
        this.getDatas();
    };

    onOperate = data => {
        switch (data.status_code){
            case 'new':
            case 'approve':
            case 'reject':
            case 'reject_no':
            case 'auditor':
                this.router.navigate(['../viewunapprovalaccess'], {queryParams: {sid: data.sid, status: data.status_code}, relativeTo: this.activatedRoute});
                break;
            case 'auditor_yes':
            case 'auditor_no':
            case 'approve_no':
                this.router.navigate(['../viewunreviewaccess'], {queryParams:  {sid: data.sid, status: data.status_code}, relativeTo: this.activatedRoute});
                break;
        }
    }

}
