import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessSimpleApprovalFormComponent } from './access-simple-approval-form.component';

describe('AccessSimpleApprovalFormComponent', () => {
  let component: AccessSimpleApprovalFormComponent;
  let fixture: ComponentFixture<AccessSimpleApprovalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessSimpleApprovalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessSimpleApprovalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
