import {Component, Input, OnInit} from '@angular/core';
import {TestFormObjModel} from '../../testFormObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AccessService} from '../../access.service';
import {FormobjModel} from '../../formobj.model';

@Component({
  selector: 'app-access-simple-approval-form',
  templateUrl: './access-simple-approval-form.component.html',
  styleUrls: ['./access-simple-approval-form.component.scss']
})
export class AccessSimpleApprovalFormComponent implements OnInit {
    formObj: FormobjModel;

    constructor() { }

    ngOnInit() {
        this.formObj = new FormobjModel();
    }

}
