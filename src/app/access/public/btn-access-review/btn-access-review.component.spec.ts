import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnAccessReviewComponent } from './btn-access-review.component';

describe('BtnAccessReviewComponent', () => {
  let component: BtnAccessReviewComponent;
  let fixture: ComponentFixture<BtnAccessReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnAccessReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnAccessReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
