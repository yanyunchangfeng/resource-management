import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-btn-access-review',
  templateUrl: './btn-access-review.component.html',
  styleUrls: ['./btn-access-review.component.css']
})
export class BtnAccessReviewComponent implements OnInit {
    @Input() public data: any;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute){ }

    ngOnInit() {
    }

    jumper = () => {
        this.router.navigate(['../reviewaccess'], {queryParams: {sid: this.data.sid, status: this.data.status_code} , relativeTo: this.activatedRoute});
    }
}
