import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnaccessEquipTableComponent } from './unaccess-equip-table.component';

describe('UnaccessEquipTableComponent', () => {
  let component: UnaccessEquipTableComponent;
  let fixture: ComponentFixture<UnaccessEquipTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnaccessEquipTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnaccessEquipTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
