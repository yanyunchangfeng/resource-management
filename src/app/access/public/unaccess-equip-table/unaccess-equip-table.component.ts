import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AccessService} from '../../access.service';

@Component({
  selector: 'app-unaccess-equip-table',
  templateUrl: './unaccess-equip-table.component.html',
  styleUrls: ['./unaccess-equip-table.component.scss']
})
export class UnaccessEquipTableComponent implements OnInit {
    @Input() searchType;
    @Input() controlBtn: boolean = true;
    @Output() tableDataEmitter: EventEmitter<any> = new EventEmitter();
    tableDatas: any[] = [];
    cols: any[] = [];
    dataSource;
    totalRecords;
   selectedCars3: any = [];


    constructor(private router: Router,
                private activedRouter: ActivatedRoute,
                private accessService: AccessService) { }

    ngOnInit() {
        // this.formObj = new FormobjModel();
        this.initCols();
    }

    initCols = () => {
        this.cols = [
            { field: '设备编号', header: '设备编号' },
            { field: '设备名称', header: '设备名称' },
            { field: '设备分类', header: '设备分类' },
            { field: '型号', header: '型号' },
            { field: '位置', header: '位置' },
            { field: '状态', header: '状态' },
            { field: '操作', header: '操作' }
        ];
    }

    remove = rowdata => {
        let findindex = this.tableDatas.findIndex(arrayItem => {
            return arrayItem['id'] === rowdata['id'];
        });
        if (!(findindex === -1)) {
           this.tableDatas.splice(findindex, 1);
        }
        this.tableDataEmitter.emit(this.tableDatas);
    };
  loadCarsLazy(event) {
    setTimeout(() => {
      if(this.dataSource) {
        this.tableDatas = this.dataSource.slice(event.first, (event.first + event.rows));
      }
    }, 0);
  }

}
