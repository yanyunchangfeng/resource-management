import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-btn-access-edit',
  templateUrl: './btn-access-edit.component.html',
  styleUrls: ['./btn-access-edit.component.css']
})
export class BtnAccessEditComponent implements OnInit {
    @Input() public data: any;
    label: string;
    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        (this.data.status_code === 'auditor_no') && (this.label = '重新申请');
        (!(this.data.status_code === 'auditor_no')) && (this.label = '编辑');
    }

    jumper = data =>  {
        if (data['status_code'] === 'auditor_no') {
            this.router.navigate(['../createaccess'], {queryParams: {sid: this.data.sid, status_code: this.data['status_code'], status: this.data.status_code} , relativeTo: this.activatedRoute});
        }else {
            this.router.navigate(['../createaccess'], {queryParams: {sid: this.data.sid, status: this.data.status_code} , relativeTo: this.activatedRoute});
        }
    }

}
