import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnAccessEditComponent } from './btn-access-edit.component';

describe('BtnAccessEditComponent', () => {
  let component: BtnAccessEditComponent;
  let fixture: ComponentFixture<BtnAccessEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnAccessEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnAccessEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
