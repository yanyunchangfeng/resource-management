import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnAccessDeleteComponent } from './btn-access-delete.component';

describe('BtnAccessDeleteComponent', () => {
  let component: BtnAccessDeleteComponent;
  let fixture: ComponentFixture<BtnAccessDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnAccessDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnAccessDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
