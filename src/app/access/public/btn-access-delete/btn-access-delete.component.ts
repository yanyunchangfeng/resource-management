import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AccessService} from '../../access.service';
import {EventBusService} from '../../../services/event-bus.service';
import {ConfirmationService} from 'primeng/primeng';
import {SearchobjModel} from '../../searchobj.model';
import {BasePage} from '../../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-btn-access-delete',
  templateUrl: './btn-access-delete.component.html',
  styleUrls: ['./btn-access-delete.component.css']
})
export class BtnAccessDeleteComponent extends BasePage implements OnInit {
    @Input() public data: any;
    @Output() public dataEmitter: EventEmitter<any> = new EventEmitter();
    sidArray: string[] = [];
    searchObj: SearchobjModel;
    constructor(private router: Router,
                private activatedRouter: ActivatedRoute,
                public messageService: MessageService,
                public confirmationService: ConfirmationService,
                private accessService: AccessService,
                private eventBusService: EventBusService) {
        super(confirmationService, messageService);
    }

    ngOnInit() {
        this.searchObj = new SearchobjModel();
    }
    delete = data => {
        this.sidArray.length = 0;
        this.sidArray[this.sidArray.length] = data['sid'];
        this.eventBusService.accessFlowCharts.next(data['status_code']);
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: () => {
                this.requestDelet();
                this.eventBusService.accessFlowCharts.next('');
            },
            reject: () => {
                this.eventBusService.accessFlowCharts.next('');
            }
        });
    };

    requestDelet = () => {
        this.accessService.requestDelete(this.sidArray).subscribe(res => {
            if (res === '00000') {
                this.alert('删除成功');
                this.eventBusService.accessSearchOverview.next(this.searchObj);
            }else {
                this.alert('删除失败');
            }
        })
    };
}
