import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AccessService} from '../../access.service';
import {FormobjModel} from '../../formobj.model';
import {EventBusService} from '../../../services/event-bus.service';

@Component({
  selector: 'app-access-simple-reason-form',
  templateUrl: './access-simple-reason-form.component.html',
  styleUrls: ['./access-simple-reason-form.component.scss']
})
export class AccessSimpleReasonFormComponent implements OnInit {
    @Output() unaccessTableDatasEimitter: EventEmitter<any> = new EventEmitter();
    @Output() simpleReasonFormDatas: EventEmitter<any> = new EventEmitter();
    formObj: FormobjModel;
    constructor(private router: Router,
                private activedRouter: ActivatedRoute,
                private accessService: AccessService,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.formObj = new FormobjModel();
        this.getQueryParams();
    }

    getQueryParams = () => {
        this.activedRouter.queryParams.subscribe(params => {
            (params['sid']) && (this.getMessageBySid(params['sid']));
            (params['status']) && (this.eventBusService.accessFlowCharts.next(params['status']));
        });
    };

    getMessageBySid = sid => {
        this.accessService.getMesageBySid(sid).subscribe(res => {
            this.formObj = new FormobjModel(res['data']);
            this.unaccessTableDatasEimitter.emit(res['data']['devices']);
            this.simpleReasonFormDatas.emit(res['data']);
        });
    };

}
