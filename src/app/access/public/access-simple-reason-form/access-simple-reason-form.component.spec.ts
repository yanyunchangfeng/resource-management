import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessSimpleReasonFormComponent } from './access-simple-reason-form.component';

describe('AccessSimpleReasonFormComponent', () => {
  let component: AccessSimpleReasonFormComponent;
  let fixture: ComponentFixture<AccessSimpleReasonFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessSimpleReasonFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessSimpleReasonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
