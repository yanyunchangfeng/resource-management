import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUnapprovalAccessComponent } from './view-unapproval-access.component';

describe('ViewUnapprovalAccessComponent', () => {
  let component: ViewUnapprovalAccessComponent;
  let fixture: ComponentFixture<ViewUnapprovalAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUnapprovalAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUnapprovalAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
