import {Component, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TestFormObjModel} from '../testFormObj.model';
import {ActivatedRoute, Router} from '@angular/router';
import {UnaccessEquipTableComponent} from '../public/unaccess-equip-table/unaccess-equip-table.component';
import {EventBusService} from '../../services/event-bus.service';

@Component({
  selector: 'app-view-unapproval-access',
  templateUrl: './view-unapproval-access.component.html',
  styleUrls: ['./view-unapproval-access.component.scss']
})
export class ViewUnapprovalAccessComponent implements OnInit {

    @ViewChild('unaccessEquipComponent') unaccessEquipComponent: UnaccessEquipTableComponent;
    formTitle = '查看申请单';
    myForm: FormGroup;
    formObj: TestFormObjModel;
    controlBtn: boolean = false;

    constructor(private fb: FormBuilder,
                private router: Router,
                private activedRouter: ActivatedRoute,
                private eventBusService: EventBusService) { }

    ngOnInit() {
        this.formObj = new TestFormObjModel();
        this.initMyForm();
    }

    initMyForm = () => {
        this.myForm = this.fb.group({
            plan_under_time: '',
            plan_finished_time: ''
        });
    };

    goBack = () => {
        this.router.navigate(['../overview'], {relativeTo: this.activedRouter});
        this.eventBusService.accessFlowCharts.next('');
    };

    unaccessTableDatasEimitter = devices => {
        this.unaccessEquipComponent.tableDatas = devices;
        this.unaccessEquipComponent.cols.splice(-1);
        this.unaccessEquipComponent.controlBtn = false;
    };
}
