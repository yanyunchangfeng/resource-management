import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../services/storage.service';

@Injectable()
export class AccessService {
    ip = environment.url.management;

    constructor(private http: HttpClient,
                private storageService: StorageService) { }

    //  获取、查询出入管理总览
    getAcessOverview(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_get',
                'data': {
                    'condition': {
                        'sid': obj['sid'],
                        'creator': obj['creator'],
                        'begin_time': obj['begin_time'],
                        'end_time': obj['end_time'],
                        'status': obj['status']

                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }

            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    //  获取带我处理出入管理
    getMineAcessOverview(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_get_byowner',
                'data': {
                    'condition': {
                        'status': obj['status']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }


            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    //  获取出入管理所有状态获取接口
    getAllAccessStatus() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_statuslist_get'
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

    //  请求单保存
    requestSave(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_save',
                'data': {
                    'registration_type': obj['registration_type'],
                    'content': obj['content'],
                    'plan_time_begin': obj['plan_time_begin'],
                    'plan_time_end': obj['plan_time_end'],
                    'room_name': obj['room_name'],
                    'room_did': obj['room_did'],
                    'approver': obj['approver'],
                    'approver_pid': obj['approver_pid'],
                    'approve_org': obj['approve_org'],
                    'approver_org_pid': obj['approver_org_pid'],
                    'devices': obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单保存
    requestSaveAfterEdited(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_mod_save',
                'data': {
                    'sid': obj['sid'],
                    'registration_type': obj['registration_type'],
                    'content': obj['content'],
                    'plan_time_begin': obj['plan_time_begin'],
                    'plan_time_end': obj['plan_time_end'],
                    'room_name': obj['room_name'],
                    'room_did': obj['room_did'],
                    'approver': obj['approver'],
                    'approver_pid': obj['approver_pid'],
                    'approve_org': obj['approve_org'],
                    'approver_org_pid': obj['approver_org_pid'],
                    'devices': obj['devices']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单提交
    requestSubmite(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_add',
                'data': {
                    'registration_type': obj['registration_type'],
                    'content': obj['content'],
                    'plan_time_begin': obj['plan_time_begin'],
                    'plan_time_end': obj['plan_time_end'],
                    'room_name': obj['room_name'],
                    'room_did': obj['room_did'],
                    'approver': obj['approver'],
                    'approver_pid': obj['approver_pid'],
                    'approve_org': obj['approve_org'],
                    'approver_org_pid': obj['approver_org_pid'],
                    'devices': obj['devices']
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单提交
    requestSubmitAfterEdit(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_mod_submit',
                'data': {
                    'sid': obj['sid'],
                    'registration_type': obj['registration_type'],
                    'content': obj['content'],
                    'plan_time_begin': obj['plan_time_begin'],
                    'plan_time_end': obj['plan_time_end'],
                    'room_name': obj['room_name'],
                    'room_did': obj['room_did'],
                    'approver': obj['approver'],
                    'approver_pid': obj['approver_pid'],
                    'approve_org': obj['approve_org'],
                    'approver_org_pid': obj['approver_org_pid'],
                    'devices': obj['devices']
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //  请求单信息获取
    getMesageBySid(trickSid: string) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_get_byid',
                'id': trickSid
            }
        ).map((res: Response) => {
            return res;
        });
    }

    // 请求单刪除
    requestDelete(ids: Array<string>) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_del',
                'ids': ids
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 请求单审批
    requestApprove(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_approve',
                'data': {
                    'sid': obj['sid'],
                    'approver_remarks': obj['approver_remarks'],
                    'status': obj['status']
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    // 请求单审核
    requestReview(obj: object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_auditor',
                'data': {
                    sid: obj['sid'],
                    auditor_remarks: obj['auditor_remarks'],
                    auditor_result: obj['auditor_result'],
                    status: obj['status']
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    //    流程图接口获取
    getFlowChart(status?) {
        status = status ? status : '';
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'registration_workflow_get',
                'data': {
                    'status': status
                }

            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    }

}
