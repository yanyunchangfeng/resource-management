import { Injectable } from '@angular/core';
import {StorageService} from './storage.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class PublicService {
  ip = environment.url.management;
  constructor(private storageService: StorageService,
              private http: HttpClient) { }

    /**
     * 获取组织接口
     * @param {string} oid
     * @returns {Observable<any>}
     * Created by LST 2018.1.4
     * 请勿随意修改
     */
  getApprovers(oid?: string) {
    (!oid) && (oid = '');
    let token = this.storageService.getToken('token');
    return this.http.post(
      `${environment.url.management}/onduty`,
      {
        'access_token': token,
        'type': 'duty_approver_get',
        'data': {
          'duty_org': oid
        }
      }
    ).map((res: Response) => {
      if (res['errcode'] !== '00000') {
        return [];
      }
      return res['datas'];
    });
  }

    /**
     * 获取故障管理基础数据配置树
     * @param did
     * @param dep
     * @returns {Observable<any>}
     *  Created by LST 2017.1.4
     *  请勿随意修改
     */
    getMalfunctionBasalDatas(did?, dep?) {
        (!did) && (did = '');
        (!dep) && (dep = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'trouble_tree_get',
                'data': {
                    'dep': dep,
                    'did': did
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.leaf = false;
                    e.data = e.name;
                });
            }
            return res['datas'];


        });

    }

  /**
   * 系统管理-人员管理：获取组织树数据
   * @param oid
   * @returns {Observable<any>}
   * Created by ZZH 2018-01-15
   */
  getOrgTree(oid) {
    let token = this.storageService.getToken('token');
    return this.http.post(`${environment.url.management}/org`, {
      'access_token': token,
      'type': 'get_suborg',
      'dep': '1',
      'id': oid
    }).map((res: Response) => {
      if (res['errcode'] !== '00000') {
        throw new Error(res['errmsg']);
      }
      // 构造primeNG tree的数据
      let datas = res['datas'];
      for (let i = 0; i < datas.length; i++) {
        datas[i].label = datas[i].name;
        datas[i].leaf = false;
      }
      return datas;
    });
  }

  /**
   * 系统管理-人员管理：获取人员数据
   * @param oid
   * @returns {Observable<any>}
   * Created by ZZH 2018-01-15
   */
  getPersonList(oid) {
    let token = this.storageService.getToken('token');
    let oids = [];
    if (oid) {
      oids.push(oid);
    }
    return this.http.post(`${environment.url.management}/personnel`, {
      'access_token': token,
      'type': 'get_personnel',
      'id': [],
      'oid': oids
    }).map((res: Response) => {
      if (res['errcode'] === '00001') { // 没有人员
        return [];
      }else if (res['errcode'] !== '00000') {
        throw new Error(res['errmsg']);
      }
      return res['data'];
    });
  }

    /**
     * 服务请求基础数据配置树
     * @param did
     * @param dep
     * @returns {Observable<any>}
     *  Created by LST 2018.1.19
     *  请勿随意修改
     */
    getRfsBasalDatas(did?, dep?) {
        (!did) && (did = '');
        (!dep) && (dep = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'servicerequest_tree_get',
                'data': {
                    'dep': dep,
                    'did': did
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.leaf = false;
                    e.data = e.name;
                });
            }
            return res['datas'];


        });

    }
    /**
     * 基础数据配置空间管理树
     * @param father
     * @returns {Observable<any>}
     *  Created by LST 2018.1.19
     *  请勿随意修改
     */
    getCapBasalDatas(father_did, sp_type_min?) {
        (!father_did) && (father_did = '');
        (!sp_type_min) && (sp_type_min = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_config_gettree_byfather',
                'data': {
                    'father_did': father_did,
                    'sp_type_min': sp_type_min
                }
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }else {
                return this.changeObjectName(res['datas'], 'label', 'name');
            }
        } );

    }
    changeObjectName(array, newName, oldName): Array<any> {
        for (let i = 0; i < array.length; i++) {
            // for(let i in array) {
                array[i][newName] = array[i][oldName];
                delete  array[i][oldName];
                if (array[i].children) {
                    this.changeObjectName( array[i].children, newName, oldName );
                }
            // }
        }
        return array;

    }
    /**
     * 获取登录用户基本信息
     * @returns {Observable<any>}
     * Created by LST 2018.1.23
     * 请勿随意修改
     */
    getUser() {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/personnel`,
            {
                'access_token': token,
                'type': 'get_personnel_user'
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }
    /**
     * 获取部门组织树
     * @param oid
     * @param dep
     * @returns {Observable<any>}
     * Created by LST 2018.1.23
     * 请勿随意修改
     */
    getDepartmentDatas(oid ? , dep ? ) {
        (!oid) && (oid = '');
        (!dep) && (dep = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/org`,
            {
                'access_token': token,
                'type': 'get_suborg',
                'id': oid,
                'dep': dep
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.leaf = false;
                    e.data = e.name;
                });
            }
            return res['datas'];
        });
    }

    /**
     * 获取空间管理的类型数据
     * @param type
     * @returns {Observable<any>}
     */
    getSpaceTypes(type) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_type_get_bysptype',
                'data': {
                    'sp_type': type
                }
            }
        ).map((res: Response) => {
           if (res['errcode'] !== '00000') {
               return [];
           }
           return res['datas'];
        });
    }

    /**
     * 请求单个节点信息
     * @param father
     * @returns {Observable<any>}
     */
    getSingleNode(did) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_config_get_byid',
                'id': did
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    /**
     * 新增空间节点、数据中心、楼栋、楼层
     * @param obj
     */
    addSpaceNode(obj) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_tree_add',
                'data': {
                    'father': obj['father'],
                    'father_did': obj['father_did'],
                    'name': obj['name'],
                    'did': obj['did'],
                    'sp_type': obj['sp_type'],
                    'status': obj['status'],
                    'remark': obj['remark'],
                    'custom1': '',
                    'custom2': ''
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    /**
     * 编辑空间节点、数据中心、楼栋、楼层
     * @param obj
     */
    editSpaceNode(obj) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_tree_mod',
                'data': {
                    'father': obj['father'],
                    'father_did': obj['father_did'],
                    'name': obj['name'],
                    'did': obj['did'],
                    'sp_type': obj['sp_type'],
                    'status': obj['status'],
                    'remark': obj['remark'],
                    'custom1': '',
                    'custom2': ''
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    /**
     * 根据类型获取节点信息
     * @param type
     * @returns {Observable<any>}
     */
    getNodeByType(type) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_config_getlist_byspacetype',
                'data': {
                    'sp_type': type
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.value = e;
                });
            }
            return res['datas'];
        });
    }

    /**
     * 新增房间或者模块间
     * @param obj
     * @returns {Observable<any>}
     */
    addRoomOrModule(obj) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_tree_add',
                'data': {
                    'father': obj['father'],
                    'father_did': obj['father_did'],
                    'name': obj['name'],
                    'sp_type': obj['sp_type'],
                    'room_type': obj['room_type'],
                    'status': obj['status'],
                    'planning_power': obj['planning_power'] && obj['planning_power'].toString() || '',
                    'planning_output_power': obj['planning_output_power'] ,
                    'ups_power': obj['ups_power'] && obj['ups_power'].toString() || '',
                    'planning_refrigerating_capacity': obj['planning_refrigerating_capacity'] && obj['planning_refrigerating_capacity'].toString() || '' ,
                    'planning_rack': obj['planning_rack'] && obj['planning_rack'].toString() || '',
                    'planning_network_port': obj['planning_network_port'] && obj['planning_network_port'].toString() || '',
                    'planning_network_bandwidth': obj['planning_network_bandwidth'] && obj['planning_network_bandwidth'].toString() || '',
                    'availability_level': obj['availability_level'],
                    'area': obj['area'] && obj['area'].toString() || '' ,
                    'room_manager': obj['room_manager'],
                    'room_manager_pid': obj['room_manager_pid'],
                    'room_manager_phone': obj['room_manager_phone'],
                    'threshold_power': obj['threshold_power'] && obj['threshold_power'].toString() || '',
                    'threshold_refrigeration': obj['threshold_refrigeration'] && obj['threshold_refrigeration'].toString() || '',
                    'threshold_rack': obj['threshold_rack'] && obj['threshold_rack'].toString() || '',
                    'threshold_network_port': obj['threshold_network_port'] && obj['threshold_network_port'].toString() || '',
                    'addtional_power': obj['addtional_power'] && obj['addtional_power'].toString() || '',
                    'addtional_refrigeration': obj['addtional_refrigeration'] && obj['addtional_refrigeration'].toString() || '',
                    'addtional_rack': obj['addtional_rack'] && obj['addtional_rack'].toString() || '',
                    'addtional_network_port': obj['addtional_network_port'] && obj['addtional_network_port'].toString() || ''
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['errcode'];
        });
    }
    /**
     * 编辑房间或者模块间
     * @param obj
     * @returns {Observable<any>}
     */
    editRoomOrModule(obj) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_tree_mod',
                'data': {
                    'father': obj['father'],
                    'father_did': obj['father_did'],
                    'name': obj['name'],
                    'did': obj['did'],
                    'sp_type': obj['sp_type'],
                    'room_type': obj['room_type'],
                    'status': obj['status'],
                    'planning_power': obj['planning_power'] && obj['planning_power'].toString() || '',
                    'planning_output_power': obj['planning_output_power'] ,
                    'ups_power': obj['ups_power'] && obj['ups_power'].toString() || '',
                    'planning_refrigerating_capacity': obj['planning_refrigerating_capacity'] && obj['planning_refrigerating_capacity'].toString() || '' ,
                    'planning_rack': obj['planning_rack'] && obj['planning_rack'].toString() || '',
                    'planning_network_port': obj['planning_network_port'] && obj['planning_network_port'].toString() || '',
                    'planning_network_bandwidth': obj['planning_network_bandwidth'] && obj['planning_network_bandwidth'].toString() || '',
                    'availability_level': obj['availability_level'],
                    'area': obj['area'] && obj['area'].toString() || '' ,
                    'room_manager': obj['room_manager'],
                    'room_manager_pid': obj['room_manager_pid'],
                    'room_manager_phone': obj['room_manager_phone'],
                    'threshold_power': obj['threshold_power'] && obj['threshold_power'].toString() || '',
                    'threshold_refrigeration': obj['threshold_refrigeration'] && obj['threshold_refrigeration'].toString() || '',
                    'threshold_rack': obj['threshold_rack'] && obj['threshold_rack'].toString() || '',
                    'threshold_network_port': obj['threshold_network_port'] && obj['threshold_network_port'].toString() || '',
                    'addtional_power': obj['addtional_power'] && obj['addtional_power'].toString() || '',
                    'addtional_refrigeration': obj['addtional_refrigeration'] && obj['addtional_refrigeration'].toString() || '',
                    'addtional_rack': obj['addtional_rack'] && obj['addtional_rack'].toString() || '',
                    'addtional_network_port': obj['addtional_network_port'] && obj['addtional_network_port'].toString() || ''
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
    /**
     * 新增机柜列
     * @param obj
     * @returns {Observable<any>}
     */
    addCabinet(obj) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_tree_add',
                'data': {
                    'father': obj['father'] ,
                    'father_did': obj['father_did'] ,
                    'name': obj['name'] ,
                    'sp_type': obj['sp_type'] ,
                    'status': obj['status'] ,
                    'planning_power': obj['planning_power'].toString() ,
                    'planning_pdu': obj['planning_pdu'].toString() ,
                    'planning_rack': obj['planning_rack'].toString() ,
                    'planning_network_port': obj['planning_network_port'].toString() ,
                    'threshold_power': obj['threshold_power'].toString() ,
                    'threshold_pdu': obj['threshold_pdu'].toString() ,
                    'threshold_rack': obj['threshold_rack'] ,
                    'threshold_network_port': obj['threshold_network_port'].toString() ,
                    'addtional_power': obj['addtional_power'].toString() ,
                    'addtional_pdu': obj['addtional_pdu'].toString() ,
                    'addtional_rack': obj['addtional_rack'].toString() ,
                    'addtional_network_port': obj['addtional_network_port'].toString()
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['errcode'];
        });
    }
    /**
     * 编辑机柜列
     * @param obj
     * @returns {Observable<any>}
     */
    editCabinet(obj) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_tree_mod',
                'data': {
                    'father': obj['father'] ,
                    'father_did': obj['father_did'] ,
                    'name': obj['name'] ,
                    'did': obj['did'],
                    'sp_type': obj['sp_type'] ,
                    'status': obj['status'] ,
                    'planning_power': obj['planning_power'].toString() ,
                    'planning_pdu': obj['planning_pdu'].toString() ,
                    'planning_rack': obj['planning_rack'].toString() ,
                    'planning_network_port': obj['planning_network_port'].toString() ,
                    'threshold_power': obj['threshold_power'].toString() ,
                    'threshold_pdu': obj['threshold_pdu'].toString() ,
                    'threshold_rack': obj['threshold_rack'].toString() ,
                    'threshold_network_port': obj['threshold_network_port'].toString() ,
                    'addtional_power': obj['addtional_power'].toString() ,
                    'addtional_pdu': obj['addtional_pdu'].toString() ,
                    'addtional_rack': obj['addtional_rack'].toString() ,
                    'addtional_network_port': obj['addtional_network_port'].toString()
                }
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }
    /**
     * 删除空间树
     * @param obj
     * @returns {Observable<any>}
     */
    deleteSpaceNode(array) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/foundationdata`,
            {
                'access_token': token,
                'type': 'capacity_space_tree_del',
                'ids': array
            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    }

    /**
     * 知识库完整树
     * @param father
     * @returns {Observable<any>}
     *  Created by LST 2018.3.29
     *  请勿随意修改
     */
    getKlgBasalDatas(did?, dep?) {
        (!did) && (did = '1');
        (!dep) && (dep = '');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_config_gettree',
                'id': did
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }else {
                return this.changeObjectName(res['datas'], 'label', 'name');
            }
        });

    }

    // 获取联动分类接口
    getClassifyDatas(father_did?) {
        (!father_did) && (father_did = '1');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_gettree_byfather',
                'id': father_did
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.leaf = false;
                    e.data = e.name;
                });
            }
            return res['datas'];
        });

    }

    // 根据父节点获取此节点下的子节点
    getChildrenNodeDpFatherID(father_did?) {
        (!father_did) && (father_did = '1');
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/knowledge`,
            {
                'access_token': token,
                'type': 'knowledge_tree_byfather',
                'id': father_did
            }
        ).map((res: Response) => {
            if ( res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });

    }

    // 设备导入搜索建议
    searchChange(queryModel){
        let token = this.storageService.getToken('token');
        return this.http.post(`${environment.url.management}/asset`, {
            'access_token': token,
            'type': 'asset_diffield_get',
            'datas': {
                'field': queryModel.field,
                'value': queryModel.value
            }
        }).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    }
    // 设备添加
    addAssets(asset){
        let token = this.storageService.getToken('token');
        return this.http.post(`${environment.url.management}/asset`, {
            'access_token': token,
            'type': 'asset_addorupd',
            'datas': {
                'number': asset.number,
                'ano': asset.ano,
                'father': asset.father,
                'card_position': asset.card_position,
                'asset_no': asset.asset_no,
                'name': asset.name,
                'en_name': asset.en_name,
                'brand': asset.brand,
                'modle': asset.modle,
                'function': asset.function,
                'system': asset.system,
                'status': asset.status,
                'on_line_datetime': asset.on_line_datetime,
                'size': asset.size,
                'room': asset.room,
                'cabinet': asset.cabinet,
                'location': asset.location,
                'belong_dapart': asset.belong_dapart,
                'belong_dapart_manager': asset.belong_dapart_manager,
                'belong_dapart_phone': asset.belong_dapart_phone,
                'manage_dapart': asset.manage_dapart,
                'manager': asset.manager,
                'manager_phone': asset.manager_phone,
                'power_level': asset.power_level,
                'power_type': asset.power_type,
                'power_rate': asset.power_rate,
                'plug_type': asset.plug_type,
                'power_count': asset.power_count,
                'power_redundant_model': asset.power_redundant_model,
                'power_source': asset.power_source,
                'power_jack_position1': asset.power_jack_position1,
                'power_jack_position2': asset.power_jack_position2,
                'power_jack_position3': asset.power_jack_position3,
                'power_jack_position4': asset.power_jack_position4,
                'ip': asset.ip,
                'operating_system': asset.operating_system,
                'up_connect': asset.up_connect,
                'down_connects': asset.down_connects,
                'maintain_vender': asset.maintain_vender,
                'maintain_vender_people': asset.maintain_vender_people,
                'maintain_vender_phone': asset.maintain_vender_phone,
                'maintain_starttime': asset.maintain_starttime,
                'maintain_endtime': asset.maintain_endtime,
                'function_info': asset.function_info,
                'remarks': asset.remarks,
            }
        }).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    }
    // 修改
    updateAssets(asset) {
        let token = this.storageService.getToken('token');
        return this.http.post(`${environment.url.management}/asset`, {
            access_token: token,
            type: 'asset_addorupd',
            datas: {
                id: Number(asset.id),
                number: asset.number,
                ano: asset.ano,
                father: asset.father,
                card_position: asset.card_position,
                asset_no: asset.asset_no,
                name: asset.name,
                en_name: asset.en_name,
                brand: asset.brand,
                modle: asset.modle,
                function: asset.function,
                system: asset.system,
                status: asset.status,
                on_line_datetime: asset.on_line_datetime,
                size: asset.size,
                room: asset.room,
                cabinet: asset.cabinet,
                location: asset.location,
                belong_dapart: asset.belong_dapart,
                belong_dapart_manager: asset.belong_dapart_manager,
                belong_dapart_phone: asset.belong_dapart_phone,
                manage_dapart: asset.manage_dapart,
                manager: asset.manager,
                manager_phone: asset.manager_phone,
                power_level: asset.power_level,
                power_type: asset.power_type,
                power_rate: asset.power_rate,
                plug_type: asset.plug_type,
                power_count: asset.power_count,
                power_redundant_model: asset.power_redundant_model,
                power_source: asset.power_source,
                power_jack_position1: asset.power_jack_position1,
                power_jack_position2: asset.power_jack_position2,
                power_jack_position3: asset.power_jack_position3,
                power_jack_position4: asset.power_jack_position4,
                ip: asset.ip,
                operating_system: asset.operating_system,
                up_connect: asset.up_connect,
                down_connects: asset.down_connects,
                maintain_vender: asset.maintain_vender,
                maintain_vender_people: asset.maintain_vender_people,
                maintain_vender_phone: asset.maintain_vender_phone,
                maintain_starttime: asset.maintain_starttime,
                maintain_endtime: asset.maintain_endtime,
                function_info: asset.function_info,
                remarks: asset.remarks,
            }
        }).map((res: Response) => {
            if (res['errcode'] !== '00000'){
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    }

    //  设备选择搜索列表
    queryEquipments(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'workflow_asset_get',
                'data': {
                    'condition': {
                        'ano': obj['ano'],
                        'name': obj['name'],
                        'status': obj['status'],
                        'on_line_date_start': obj['on_line_date_start'],
                        'on_line_date_end': obj['on_line_date_end'],
                        'room': obj['room'],
                        'filter_anos': obj['filter_anos']
                    },
                    'page': {
                        'page_size': obj['page_size'],
                        'page_number': obj['page_number']
                    }
                }


            }
        ).map((res: Response) => {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    }

    // 设备导入
    importEquipment(obj: Object) {
        let token = this.storageService.getToken('token');
        return this.http.post(
            `${this.ip}/workflow`,
            {
                'access_token': token,
                'type': 'workflow_asset_add',
                'datas': {
                    id: obj['id'],
                    number: obj['number'],
                    ano: obj['ano'],
                    father: obj['father'],
                    card_position: obj['card_position'],
                    asset_no: obj['asset_no'],
                    name: obj['name'],
                    en_name: obj['en_name'],
                    brand: obj['brand'],
                    modle: obj['modle'],
                    function: obj['function'],
                    system: obj['system'],
                    status: obj['status'],
                    on_line_datetime: obj['on_line_datetime'],
                    size: obj['size'],
                    room: obj['room'],
                    cabinet: obj['cabinet'],
                    location: obj['location'],
                    belong_dapart: obj['belong_dapart'],
                    belong_dapart_manager: obj['belong_dapart_manager'],
                    belong_dapart_phone: obj['belong_dapart_phone'],
                    manage_dapart: obj['manage_dapart'],
                    manager: obj['manager'],
                    manager_phone: obj['manager_phone'],
                    power_level: obj['power_level'],
                    power_type: obj['power_type'],
                    power_rate: obj['power_rate'],
                    plug_type: obj['plug_type'],
                    power_count: obj['power_count'],
                    power_redundant_model: obj['power_redundant_model'],
                    power_source: obj['power_source'],
                    power_jack_position1: obj['power_jack_position1'],
                    power_jack_position2: obj['power_jack_position2'],
                    power_jack_position3: obj['power_jack_position3'],
                    power_jack_position4: obj['power_jack_position4'],
                    ip: obj['ip'],
                    operating_system: obj['operating_system'],
                    up_connect: obj['up_connect'],
                    down_connects: obj['down_connects'],
                    maintain_vender: obj['maintain_vender'],
                    maintain_vender_people: obj['maintain_vender_people'],
                    maintain_vender_phone: obj['maintain_vender_phone'],
                    maintain_starttime: obj['maintain_starttime'],
                    maintain_endtime: obj['maintain_endtime'],
                    function_info: obj['function_info'],
                    remarks: obj['remarks']
                }

            }
        ).map((res: Response) => {
            return res;
        });
    }
}
