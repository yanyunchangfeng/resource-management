import {Injectable} from '@angular/core';
import {CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route} from '@angular/router';
import {LoginService} from '../login/login.service';
import {environment} from "../../environments/environment";

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

    constructor(private loginService: LoginService,
              private router: Router) { }

    canActivate = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean => {
        const url: string = state.url;
        return this.checkLogin(url);
    };
    canActivateChild = (route: ActivatedRouteSnapshot,state: RouterStateSnapshot): boolean => {
        return this.canActivate(route, state);
    };

    canLoad = (route: Route): boolean => {
        const url = `/${route.path}`;
        return this.checkLogin(url);
    };

    checkLogin = (url: string): boolean => {
        if (window.sessionStorage.getItem('route')){
        }
        if (this.loginService.isLoginIn()) {
          return true;
        }
        // Store the attempted URL for redirecting
        this.loginService.redirectUrl = url;
        // Navigate to the login page with extras
        this.router.navigate(['/login']);
        return false;
    }
}


