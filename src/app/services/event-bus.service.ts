import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
/**
 * 事件总线，组件之间可以通过这个服务进行通讯
 */
@Injectable()
export class EventBusService {
    public topToggleBtn: Subject <boolean> = new Subject<boolean>();
    public menuActice: Subject <boolean> = new Subject<boolean>();
    public maintenance: Subject <any> = new Subject<any>();
    public rfs: Subject<any> = new Subject<any>();
    public flow: Subject<any> = new Subject<any>(); // 入库流程图
    public borrow: Subject<any> = new Subject<any>(); // 领用流程图
    public requstion: Subject<any> = new Subject<any>(); // 借用流程图
    public space: Subject<any> = new Subject<any>();
    public updateborrow: Subject<any> = new Subject<any>(); // 入库根据状态查询不同的数据
    public updaterequestion: Subject<any> = new Subject<any>(); // 借用根据状态查询不同的数据
    public updatereequipment: Subject<any> = new Subject<any>(); // 领用根据状态查询不同的数据
    public updateconstruction: Subject<any> = new Subject<any>(); // 施工管理根据状态查询不同的数据
    public updateshelf: Subject<any> = new Subject<any>(); // 上架管理根据状态查询不同的数据
    public handover: Subject<any> = new Subject<any>();
    public tagcloud: Subject<any> = new Subject<any>();
    public klgdelete: Subject<any> = new Subject<any>();
    public klgFlowCharts: Subject<any> = new Subject<any>();
    public klgdeletedevice: Subject<any> = new Subject<any>();
    public accessMineOverview: Subject<any> = new Subject<any>();
    public accessSearchOverview: Subject<any> = new Subject<any>();
    public accessFlowCharts: Subject<any> = new Subject<any>();
    public variationFlowCharts: Subject<any> = new Subject<any>();
    public variationMineOverview: Subject<any> = new Subject<any>();
    public underShelfOverview: Subject<any> = new Subject<any>();
    public underShelfFlowCharts: Subject<any> = new Subject<any>();
    constructor() { }
}
