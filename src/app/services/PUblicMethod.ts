import {AbstractControl, FormControl, FormGroup} from '@angular/forms';

export class PUblicMethod {

    initZh(): Object {
        return {
            firstDayOfWeek: 1,
            dayNames: [ '周一', '周二', '周三', '周四', '周五', '周六', '周日' ],
            dayNamesShort: [ '一', '二', '三', '四', '五', '六', '七' ],
            dayNamesMin: [ '一', '二', '三', '四', '五', '六', '七' ],
            monthNames: [ '一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月' ],
            monthNamesShort: [ '一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二' ],
        };
    }

    /**
     * 日期格式化 1966-01-8 12:12:12
     * @param date
     */
    static formateEnrtyTime(time) {
        let date: any;
        if (typeof time === 'string') {
            let str = time.replace(/-/g, '/');
            date = new Date(Date.parse(str));
        }else {
            date = new Date(time);
        }
        let y = date.getFullYear();
        let m = PUblicMethod.pad(date.getMonth() + 1);
        let d = PUblicMethod.pad(date.getDate());
        let min = PUblicMethod.pad(date.getMinutes());
        let s = PUblicMethod.pad(date.getSeconds());
        let h = PUblicMethod.pad(date.getHours());
        return y + '-' + m + '-' + d + ' ' + h + ':' + min + ':' + s;
    }
    /**
     * 日期格式化 12:12:12 -> 1966-01-8 12:12:12
     * @param date
     */
    static formateTimeToEntryTime(time: string) {
        let date = new Date();
        let y = date.getFullYear();
        let m = PUblicMethod.pad(date.getMonth() + 1);
        let d = PUblicMethod.pad(date.getDate());
        return y + '/' + m + '/' + d + ' ' + time;
    }

    /**
     * 日期格式化 1996-06-09
     * @param date
     * @returns {string}
     */
    static formateDateTime(time) {
        let date = new Date(time);
        let y = date.getFullYear();
        let m = PUblicMethod.pad(date.getMonth() + 1);
        let d = PUblicMethod.pad(date.getDate());
        return y + '-' + m + '-' + d;
    }
    /**
     * 日期格式化 12:12:12
     * @param date
     * @returns {string}
     */
    static formateMiniteTime(time) {
        let reg = new RegExp('\\d*:\\d*:\\d*');
        return PUblicMethod.formateEnrtyTime(time).match(reg)[0];
    }
    /**
     * pad
     * @param n
     * @returns {any}
     */
    static  pad(n) {
        if ( n < 10 ) {
            return '0' + n;
        }
        return n;
    }
    /**
     * 格式化下拉菜单数据
     * 适用于： {name:xxx, code:xxx}
     * @param {Array<Object>} arr
     * @returns {Array<Object>}
     */
    static  formateDropDown(arr: Array<Object>): Array<Object> {
        let newArr = [];
        arr.map(v => {
            let obj = {};
            obj['label'] = v['name'];
            obj['value'] = v['code'];
            newArr.push(obj);
        });
        return newArr;
    }
    /**
     * 格式化下拉菜单数据
     * 适用于： {name:xxx, code:xxx}
     * @param {Array<Object>} arr
     * @returns {Array<Object>}
     */
    static  formateFlexDropDown(arr: Array<Object>, label: string, value: string): Array<Object> {
        let newArr = [];
        arr.map(v => {
            let obj = {};
            obj['label'] = v[label];
            obj['value'] = {
                'label': v[label],
                'value': v[value]
            };
            newArr.push(obj);
        });
        return newArr;
    }
    /**
     * 格式化下拉菜单数据
     * 适用于 {mobile: xxx, name: xxx, oid: xxx, photo1:xxx, pid: xxx, post:xxx}
     * @param {Array<Object>} arr
     * @returns {Array<Object>}
     */
    static formateDepDropDown(arr: Array<Object>): Array<object> {
        let newArr = [];
        arr.map(v => {
           let obj = {};
           obj['label'] = v['name'];
           obj['value'] = {
               name: v['name'],
               mobile: v['mobile'],
               oid: v['oid'],
               photo1: v['photo1'],
               pid: v['pid'],
               post: v['post'],
               did: v['did']
           };
           newArr[newArr.length] = obj;
        });
        return newArr;
    }

    /**
     * 用于表单动态验证
     * @param {FormGroup} formGroup
     */
    static validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({onlySelf: true});
            }else if (control instanceof  FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    /**
     * can use this when formgroup not nested formgroup
     * @param {FormGroup} form
     * @param {string} field
     * @returns {boolean}
     */
    static isFieldValid(form: FormGroup, field: string): boolean {
        return !form.get(field).valid && form.get(field).touched;
    }

    /**
     * can use this when formgroup nested formgroup
     * @param {AbstractControl} form
     * @param {string} field
     * @returns {boolean}
     */
    static isFieldGroupValid(form: AbstractControl, field: string): boolean {
        return !form.get(field).valid && form.get(field).touched;
    }

    /**
     * for button which is relative with one of formcontrol
     * @param {FormGroup} form
     * @param {string} field
     * @returns {boolean}
     */
    static isFieldValueValid(form: FormGroup, field: string): boolean {
        return form.get(field).valid;
    }

    /**
     * 用于手动导入设备
     * @param obj
     * @returns {any}
     */
    static deepClone(obj){
        let o, i, j, k;
         if (!(typeof(obj) === 'object') || obj === null){
             return obj;
         }
        if (obj instanceof (Array)) {
            o = [];
            i = 0; j = obj.length;
            for (; i < j; i++) {
                if (typeof(obj[i]) === 'object' && obj[i] != null) {
                    o[i] = this.deepClone(obj[i]);
                }else{
                    o[i] = obj[i];
                }
            }
        } else {
            o = {};
            for (i in obj) {
                if (typeof(obj[i]) === 'object' && obj[i] != null) {
                    o[i] = this.deepClone(obj[i]);
                } else {
                    o[i] = obj[i];
                }
            }
        }
        return o;
    }

}
