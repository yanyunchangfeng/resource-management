import {Injectable} from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }
  setUserName(key, val) {
    window.sessionStorage.setItem(key, val);
  }
  getUserName(key) {
    return window.sessionStorage.getItem(key);
  }
  removeUserName(key) {
    window.sessionStorage.removeItem(key);
  }
  setToken(key, val) {
    window.sessionStorage.setItem(key, val);
  }
  getToken(key) {
    return window.sessionStorage.getItem(key);
  }
  removeToken(key) {
    window.sessionStorage.removeItem(key);
  }
  setMenu(key, val) {
    if (typeof val === 'object') {
      val = JSON.stringify(val);
    }
    window.sessionStorage.setItem(key, val);
  }
  getMenu(key) {
    return window.sessionStorage.getItem(key);
  }
  setAssetIndex(key, val) {
    window.sessionStorage.setItem(key, val );
  }
  getAssetIndex(key) {
    return window.sessionStorage.getItem(key);
  }
  setCurrnetAsset(key, val) {
    if (typeof val === 'object') {
      val = JSON.stringify(val);
    }
    window.sessionStorage.setItem(key, val);
  }
  getCurrentAsset(key) {
    return window.sessionStorage.getItem(key);
  }
  // 设置报障处理当前操作的下标
  setReportId(key, val) {
    window.sessionStorage.setItem(key, val );
  }
  // 获取报障处理当前操作的下标
  getReportId(key) {
    return window.sessionStorage.getItem(key);
  }
  // 设置修改巡检周期当前操作的下标
  setCurrnetInspectionIndex(key, val) {
    window.sessionStorage.setItem(key, val );
  }
  getCurrentInspectionIndex(key) {
    return window.sessionStorage.getItem(key);
  }
  setCurrentShifCid(key, val){
    window.sessionStorage.setItem(key, val);
  }
  getCurrentShiftCid(key){
    return window.sessionStorage.getItem(key);
  }
  setCurrentInspectionObjctCid(key, val){
    window.sessionStorage.setItem(key, val);
  }
  getCurrentInspectionObjectCid(key){
    return window.sessionStorage.getItem(key);
  }
  setCurrentInspectionMissionCid(key, val){
    window.sessionStorage.setItem(key, val);
  }
  getCurrentInspectionMissionCid(key){
    return window.sessionStorage.getItem(key);
  }
 setCurrentMenuItem(key, val){
    if (typeof val === 'object'){
      val = JSON.stringify(val);
    }
    window.sessionStorage.setItem(key, val);
  }
  getCurrentMenuItem(key){
    return window.sessionStorage.getItem(key);
  }
  removeCurrentMenuItem(key){
    window.sessionStorage.removeItem(key);
  }
  setCurrentStorage(key, val){
    window.sessionStorage.setItem(key, val);
  }
  getCurrentStorage(key){
    return window.sessionStorage.getItem(key);
  }
  setLocationIndex(key, val){
    window.sessionStorage.setItem(key, val);
  }
  getLocationIndex(key){
    return window.sessionStorage.getItem(key);
  }
  setEnvironmentUrl(key, val){
    window.sessionStorage.setItem(key, val);
  }
  getEnvironmentUrl(key){
    return window.sessionStorage.getItem(key);
  }
  setCurrentInspectionPointobj(key,val){
    if(typeof val=='object'){
      val = JSON.stringify(val);
    }
    window.sessionStorage.setItem(key,val)
  }
  getCurrentInspectionPointobj(key){
    return window.sessionStorage.getItem(key);
  }

  setCurrentShelf(key, val){
    window.sessionStorage.setItem(key, val);
  }
  getCurrentShelf(key){
    return window.sessionStorage.getItem(key);
  }
}
