import { NgModule } from '@angular/core';
import {ShareModule} from '../shared/share.module';
import { PublicService } from '../services/public.service';
import {MalfunctionBaseInventoryComponent} from "../inventorymaintenance/malfunctioninventory/malfunction-base-inventory/malfunction-base-inventory.component";
import {MalfunctionInventoryChangeComponent} from "../inventorymaintenance/malfunctioninventory/malfunction-inventory-change/malfunction-inventory-change.component";
import {MalfunctionInventoryOverviewComponent} from "../inventorymaintenance/malfunctioninventory/malfunction-inventory-overview/malfunction-inventory-overview.component";
import {MalfunctionInventoryComponent} from "../inventorymaintenance/malfunction-inventory/malfunction-inventory.component";
import { MalfunctionAddInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-add-inventory/malfunction-add-inventory.component';
import { MalfunctionDealInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-deal-inventory/malfunction-deal-inventory.component';
import { MalfunctionDistributeInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-distribute-inventory/malfunction-distribute-inventory.component';
import { MalfunctionReceiveInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-receive-inventory/malfunction-receive-inventory.component';
import { MalfunctionOffInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-off-inventory/malfunction-off-inventory.component';
import { MalfunctionSolveInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-solve-inventory/malfunction-solve-inventory.component';
import { MalfunctionEditInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-edit-inventory/malfunction-edit-inventory.component';
import { MalfunctionSolvedInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-solved-inventory/malfunction-solved-inventory.component';
import { MalfunctionViewInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-view-inventory/malfunction-view-inventory.component';
import { MalfunctionAcceptInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-accept-inventory/malfunction-accept-inventory.component';
import { MalfunctionProcessInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-process-inventory/malfunction-process-inventory.component';
import { MalfunctionAcceptEditInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-accept-edit-inventory/malfunction-accept-edit-inventory.component';
import { MalfunctionAcceptViewInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-accept-view-inventory/malfunction-accept-view-inventory.component';
import { MalfunctionUpInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-up-inventory/malfunction-up-inventory.component';
import { MalfunctionTurnInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-turn-inventory/malfunction-turn-inventory.component';
import { MalfunctionCloseInventoryComponent } from '../inventorymaintenance/malfunctioninventory/malfunction-close-inventory/malfunction-close-inventory.component';
import {InventorymaintenanceRoutingModule} from "./inventorymaintenance-routing.module";
import {InventorymaintenanceService} from "./inventorymaintenance.service";
import {BaseViewComponent} from "./malfunctioninventory/public/base-view/base-view.component";
import {ColseBaseViewComponent} from "./malfunctioninventory/public/colse-base-view/colse-base-view.component";
import {MalfunctionTableComponent} from "./malfunctioninventory/public/malfunction-table/malfunction-table.component";
import {PersonelDialogComponent} from "./malfunctioninventory/public/personel-dialog/personel-dialog.component";
import {SearchFormComponent} from "./malfunctioninventory/public/search-form/search-form.component";
import {SimpleViewComponent} from "./malfunctioninventory/public/simple-view/simple-view.component";
import {UploadFileViewComponent} from "./malfunctioninventory/public/upload-file-view/upload-file-view.component";
import {BtnCheckComponent} from "./public/btn-check/btn-check.component";
import {BtnDealComponent} from "./public/btn-deal/btn-deal.component";
import {BtnDeleteComponent} from "./public/btn-delete/btn-delete.component";
import {BtnDistributeComponent} from "./public/btn-distribute/btn-distribute.component";
import {BtnDownComponent} from "./public/btn-down/btn-down.component";
import {BtnEditComponent} from "./public/btn-edit/btn-edit.component";
import {BtnOffComponent} from "./public/btn-off/btn-off.component";
import {BtnReceiveComponent} from "./public/btn-receive/btn-receive.component";
import {BtnRejectComponent} from "./public/btn-reject/btn-reject.component";
import {BtnSolveComponent} from "./public/btn-solve/btn-solve.component";
import {BtnUpComponent} from "./public/btn-up/btn-up.component";
import {BtnViewComponent} from "./public/btn-view/btn-view.component";



@NgModule({
  imports: [
    ShareModule,
    InventorymaintenanceRoutingModule
  ],
  declarations: [
    MalfunctionBaseInventoryComponent,
    MalfunctionInventoryChangeComponent,
    MalfunctionInventoryOverviewComponent,
    MalfunctionInventoryComponent,
    MalfunctionAddInventoryComponent,
    MalfunctionDealInventoryComponent,
    MalfunctionDistributeInventoryComponent,
    MalfunctionReceiveInventoryComponent,
    MalfunctionOffInventoryComponent,
    MalfunctionSolveInventoryComponent,
    MalfunctionEditInventoryComponent,
    MalfunctionSolvedInventoryComponent,
    MalfunctionViewInventoryComponent,
    MalfunctionAcceptInventoryComponent,
    MalfunctionProcessInventoryComponent,
    MalfunctionAcceptEditInventoryComponent,
    MalfunctionAcceptViewInventoryComponent,
    MalfunctionUpInventoryComponent,
    MalfunctionTurnInventoryComponent,
    MalfunctionCloseInventoryComponent,
    BaseViewComponent,
    ColseBaseViewComponent,
    MalfunctionTableComponent,
    PersonelDialogComponent,
    SearchFormComponent,
    SimpleViewComponent,
    UploadFileViewComponent,
    BtnCheckComponent,
    BtnDealComponent,
    BtnDeleteComponent,
    BtnDistributeComponent,
    BtnDownComponent,
    BtnEditComponent,
    BtnOffComponent,
    BtnReceiveComponent,
    BtnRejectComponent,
    BtnSolveComponent,
    BtnUpComponent,
    BtnViewComponent
  ],
  providers: [
    InventorymaintenanceService,
    PublicService
  ]
})
export class InventorymaintenanceModule { }
