import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {MalfunctionBaseInventoryComponent} from "./malfunctioninventory/malfunction-base-inventory/malfunction-base-inventory.component";
import {MalfunctionInventoryComponent} from "./malfunction-inventory/malfunction-inventory.component";
import {MalfunctionInventoryOverviewComponent} from "./malfunctioninventory/malfunction-inventory-overview/malfunction-inventory-overview.component";
import {MalfunctionAddInventoryComponent} from "./malfunctioninventory/malfunction-add-inventory/malfunction-add-inventory.component";
import {MalfunctionInventoryChangeComponent} from "./malfunctioninventory/malfunction-inventory-change/malfunction-inventory-change.component";
import {MalfunctionDealInventoryComponent} from "./malfunctioninventory/malfunction-deal-inventory/malfunction-deal-inventory.component";
import {MalfunctionDistributeInventoryComponent} from "./malfunctioninventory/malfunction-distribute-inventory/malfunction-distribute-inventory.component";
import {MalfunctionOffInventoryComponent} from "./malfunctioninventory/malfunction-off-inventory/malfunction-off-inventory.component";
import {MalfunctionReceiveInventoryComponent} from "./malfunctioninventory/malfunction-receive-inventory/malfunction-receive-inventory.component";
import {MalfunctionSolveInventoryComponent} from "./malfunctioninventory/malfunction-solve-inventory/malfunction-solve-inventory.component";
import {MalfunctionEditInventoryComponent} from "./malfunctioninventory/malfunction-edit-inventory/malfunction-edit-inventory.component";
import {MalfunctionSolvedInventoryComponent} from "./malfunctioninventory/malfunction-solved-inventory/malfunction-solved-inventory.component";
import {MalfunctionViewInventoryComponent} from "./malfunctioninventory/malfunction-view-inventory/malfunction-view-inventory.component";
import {MalfunctionAcceptInventoryComponent} from "./malfunctioninventory/malfunction-accept-inventory/malfunction-accept-inventory.component";
import {MalfunctionAcceptEditInventoryComponent} from "./malfunctioninventory/malfunction-accept-edit-inventory/malfunction-accept-edit-inventory.component";
import {MalfunctionAcceptViewInventoryComponent} from "./malfunctioninventory/malfunction-accept-view-inventory/malfunction-accept-view-inventory.component";
import {MalfunctionProcessInventoryComponent} from "./malfunctioninventory/malfunction-process-inventory/malfunction-process-inventory.component";
import {MalfunctionUpInventoryComponent} from "./malfunctioninventory/malfunction-up-inventory/malfunction-up-inventory.component";
import {MalfunctionTurnInventoryComponent} from "./malfunctioninventory/malfunction-turn-inventory/malfunction-turn-inventory.component";
import {MalfunctionCloseInventoryComponent} from "./malfunctioninventory/malfunction-close-inventory/malfunction-close-inventory.component";

const routes: Routes = [
  {path: '', component: MalfunctionInventoryComponent},
  {path: 'malfunctionInventoryBasalDatas', component: MalfunctionInventoryComponent},

  {path: 'malfunctioninventory', component: MalfunctionBaseInventoryComponent, children: [
    {path: '', component: MalfunctionInventoryOverviewComponent},
    {path: 'malfunctionAddInventory', component: MalfunctionAddInventoryComponent},
    {path: 'malfunctioninventoryChange', component: MalfunctionInventoryChangeComponent},
    {path: 'malfuntionDealInventory', component: MalfunctionDealInventoryComponent},
    {path: 'malfunctionDistributeInventory', component: MalfunctionDistributeInventoryComponent},
    {path: 'malfunctionOffInventory', component: MalfunctionOffInventoryComponent},
    {path: 'malfunctionReceiveInventory', component: MalfunctionReceiveInventoryComponent},
    {path: 'malfunctionSolveInventory', component: MalfunctionSolveInventoryComponent},
    {path: 'malfunctionEditInventory', component: MalfunctionEditInventoryComponent},
    {path: 'malfunctionSolvedInventory', component: MalfunctionSolvedInventoryComponent},
    {path: 'malfunctionViewInventory', component: MalfunctionViewInventoryComponent},
    {path: 'malfunctionAcceptInventory', component: MalfunctionAcceptInventoryComponent},
    {path: 'malfunctionAcceptEditInventory', component: MalfunctionAcceptEditInventoryComponent},
    {path: 'malfunctionAcceptViewInventory', component: MalfunctionAcceptViewInventoryComponent},
    {path: 'malfunctionProcessInventory', component: MalfunctionProcessInventoryComponent},
    {path: 'malfunctionUpInventory', component: MalfunctionUpInventoryComponent},
    {path: 'malfunctionTurnInventory', component: MalfunctionTurnInventoryComponent},
    {path: 'malfunctionCloseInventory', component: MalfunctionCloseInventoryComponent}
  ]}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventorymaintenanceRoutingModule { }
