export class PUblicMethod {

    initZh() {
        return {
            firstDayOfWeek: 1,
            dayNames: [ '周一', '周二', '周三', '周四', '周五', '周六', '周日' ],
            dayNamesShort: [ '一', '二', '三', '四', '五', '六', '七' ],
            dayNamesMin: [ '一', '二', '三', '四', '五', '六', '七' ],
            monthNames: [ '一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月' ],
            monthNamesShort: [ '一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二' ],
        };
    }

    /**
     * 日期格式化 1966-01-8 12:12:12
     * @param date
     */
    static formateEnrtyTime(time) {
        let date = new Date(time);
        let y = date.getFullYear();
        let m = PUblicMethod.pad(date.getMonth() + 1);
        let d = PUblicMethod.pad(date.getDate());
        let min = PUblicMethod.pad(date.getMinutes());
        let s = PUblicMethod.pad(date.getSeconds());
        let h = PUblicMethod.pad(date.getHours());
        return y + '-' + m + '-' + d + ' ' + h + ':' + min + ':' + s;
    }

    /**
     * 日期格式化 1996-06-09
     * @param date
     * @returns {string}
     */
    static formateDateTime(time) {
        let date = new Date(time);
        let y = date.getFullYear();
        let m = PUblicMethod.pad(date.getMonth() + 1);
        let d = PUblicMethod.pad(date.getDate());
        return y + '-' + m + '-' + d;
    }
    /**
     * 日期pad
     * @param n
     * @returns {any}
     */
    static  pad(n) {
        if ( n < 10 ) {
            return '0' + n;
        }
        return n;
    }
}