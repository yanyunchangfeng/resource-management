import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnDistributeComponent } from './btn-distribute.component';

describe('BtnDistributeComponent', () => {
  let component: BtnDistributeComponent;
  let fixture: ComponentFixture<BtnDistributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnDistributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnDistributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
