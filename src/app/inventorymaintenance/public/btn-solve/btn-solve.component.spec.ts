import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnSolveComponent } from './btn-solve.component';

describe('BtnSolveComponent', () => {
  let component: BtnSolveComponent;
  let fixture: ComponentFixture<BtnSolveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnSolveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnSolveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
