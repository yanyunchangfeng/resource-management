import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnDealComponent } from './btn-deal.component';

describe('BtnDealComponent', () => {
  let component: BtnDealComponent;
  let fixture: ComponentFixture<BtnDealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnDealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnDealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
