import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionViewInventoryComponent } from './malfunction-view-inventory.component';

describe('MalfunctionViewInventoryComponent', () => {
  let component: MalfunctionViewInventoryComponent;
  let fixture: ComponentFixture<MalfunctionViewInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionViewInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionViewInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
