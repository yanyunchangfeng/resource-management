import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {EventBusService} from "../../../services/event-bus.service";
import {InventorymaintenanceService} from "../../inventorymaintenance.service";

@Component({
  selector: 'app-malfunction-view-inventory',
  templateUrl: './malfunction-view-inventory.component.html',
  styleUrls: ['./malfunction-view-inventory.component.scss']
})
export class MalfunctionViewInventoryComponent implements OnInit {


  sid: string;
  status: string;
  constructor( private activatedRouter: ActivatedRoute,
               private maintenanceService: InventorymaintenanceService,
               private eventBusService: EventBusService) { }

  ngOnInit() {
    this.sid = this.activatedRouter.snapshot.paramMap.get('id');
    this.status = this.activatedRouter.snapshot.paramMap.get('status');
    this.maintenanceService.getFlowChart(this.status).subscribe(res => {
      this.eventBusService.maintenance.next(res);
    });
  }

}
