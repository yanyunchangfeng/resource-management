import { Component, OnInit } from '@angular/core';
import {EventBusService} from "../../../services/event-bus.service";
import {ActivatedRoute} from "@angular/router";
import {Message} from "primeng/primeng";
import {InventorymaintenanceService} from "../../inventorymaintenance.service";

@Component({
  selector: 'app-malfunction-accept-view-inventory',
  templateUrl: './malfunction-accept-view-inventory.component.html',
  styleUrls: ['./malfunction-accept-view-inventory.component.scss']
})
export class MalfunctionAcceptViewInventoryComponent implements OnInit {

  initStatus = '';                       // 故障单状态
  sid: string;
  resObj: Object;
  message: Message[] = [];               // 交互提示弹出框
  msg: Message[] = [];                   // 验证弹出框
  submmitterTime: string;
  acceptTime: string;

  constructor(private maintenanceService: InventorymaintenanceService,
              private activatedRouter: ActivatedRoute,
              private eventBusService: EventBusService) { }

  ngOnInit() {
    this.sid = this.activatedRouter.snapshot.paramMap.get('id');
    this.initStatus = this.activatedRouter.snapshot.paramMap.get('status');
    this.initTrick(this.sid);
    this.maintenanceService.getFlowChart(this.initStatus).subscribe(res => {
      this.eventBusService.maintenance.next(res);
    });
  }
  initTrick(s) {
    this.maintenanceService.getTrick(s).subscribe(res => {
      let data = res['data'];
      if (res['errcode'] === '00000') {
        this.resObj = data;
        this.submmitterTime = this.resObj['assign_time'];
        this.acceptTime = this.resObj['accept_time'];
      }else {
        this.message = [];
        this.message.push({severity: 'error', summary: '消息提示', detail: `获取故障单信息失败${res}`});
      }
    });
  }

}
