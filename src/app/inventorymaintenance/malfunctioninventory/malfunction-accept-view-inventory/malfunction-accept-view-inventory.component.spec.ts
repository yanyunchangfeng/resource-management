import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionAcceptViewInventoryComponent } from './malfunction-accept-view-inventory.component';

describe('MalfunctionAcceptViewInventoryComponent', () => {
  let component: MalfunctionAcceptViewInventoryComponent;
  let fixture: ComponentFixture<MalfunctionAcceptViewInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionAcceptViewInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionAcceptViewInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
