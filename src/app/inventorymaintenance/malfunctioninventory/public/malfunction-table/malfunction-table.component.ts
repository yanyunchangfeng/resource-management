import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ConfirmationService, Message} from 'primeng/primeng';
import {Router} from '@angular/router';
import {InventorymaintenanceService} from "../../../inventorymaintenance.service";
import {BasePage} from '../../../../base.page';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-malfunction-table',
  templateUrl: './malfunction-table.component.html',
  styleUrls: ['./malfunction-table.component.scss']
})
/**
 * Created by LST 2018.1.3
 * Date of last change 2018.1.3 16:24
 */
export class MalfunctionTableComponent implements OnInit, OnChanges {

    @Input() eventDatas: Object;
    @Input() searchType: string;

    scheduleDatas = [];                    // 表格渲染数据
    total: string;                         // 记录总条数
    page_size: string;                     // 总的分页数
    page_total: string;                    // 每页记录条数
    status: string;                        // 状态字段
    msgs: Message[] = [];


    constructor(private maintenanceService: InventorymaintenanceService,
                private router: Router) {
    }

    ngOnInit() {
        this.initGetList();
    }

    ngOnChanges(changes: SimpleChanges): void {
        // console.log(changes);
        let res =  changes['eventDatas']['currentValue'];
        if ( res ) {
            if (res) {
                this.scheduleDatas = res.items;
                if (res['page']) {
                    this.total = res['page']['total'];
                    this.page_size = res['page']['page_size'];
                    this.page_total = res['page']['page_total'];
                }
            }else {
                this.scheduleDatas = [];
            }
        }
    }

    initGetList() {
        if ( this.searchType === 'trouble_get') {
            this.getList(this.status = '');
        }else {
            this.getList(this.status = this.searchType);
        }
    }

    getList(status) {
        this.maintenanceService.getAllMainfunctionList('', '', '', '', '', status).subscribe(res => {
            console.log(res);
            if (res) {
                this.scheduleDatas = res.items;
                this.total = res['page']['total'];
                this.page_size = res['page']['page_size'];
                this.page_total = res['page']['page_total'];
            }else {
                this.scheduleDatas = [];
            }
        });
    }

    paginate(event) {
        this.maintenanceService.getAllMainfunctionList(event.rows.toString(), (event.page + 1).toString(), '', '', '', this.status).subscribe( res => {
            if (res) {
                this.scheduleDatas = res.items;
                this.total = res['page']['total'];
                this.page_size = res['page']['page_size'];
                this.page_total = res['page']['page_total'];
            }else {
                this.scheduleDatas = [];
            }
        });
    }

    deletDataEmitter(event) {
        if (event.errcode === '00000') {
            this.msgs = [];
            this.msgs.push({severity: 'success', summary: '消息提示', detail: '删除成功'});
            this.initGetList();
        }else {
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: '消息提示', detail: '删除失败' + event});
        }
    }

    reciveDataEmitter(event) {
        console.log(event);
        if (event.errcode === '00000') {
            this.msgs = [];
            this.msgs.push({severity: 'success', summary: '消息提示', detail: '接受成功'});
            this.initGetList();
        }else {
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: '消息提示', detail: '接收失败' + event});
        }
    }

    rejectDataEmitter(event) {
        if (event.errcode === '00000') {
            this.msgs = [];
            this.msgs.push({severity: 'success', summary: '消息提示', detail: '拒绝成功'});
            this.initGetList();
        }else {
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: '消息提示', detail: '拒绝失败' + event});
        }
    }

    onOperate(data) {
        console.log(data);
        switch (data.status) {
            case '新建' :
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionEdit', { id: data.sid, status: data.status}]);
                break;
            case '待分配' :
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionChange', { id: data.sid, status: data.status}]);
                break;
            case '待接受' :
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionAccept', { id: data.sid, status: data.status}]);
                break;
            case '待处理' :
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionProcess', { id: data.sid, status: data.status}]);
                break;
            case '处理中' :
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionSolved', { id: data.sid, status: data.status}]);
                break;
            case '已解决' :
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionClose', { id: data.sid, status: data.status}]);
                break;
            // case '关闭' :
            //     this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionClose', { id: data.sid, status: data.status}]);
            //     break;
        }
    }


}
