import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonelDialogComponent } from './personel-dialog.component';

describe('PersonelDialogComponent', () => {
  let component: PersonelDialogComponent;
  let fixture: ComponentFixture<PersonelDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonelDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonelDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
