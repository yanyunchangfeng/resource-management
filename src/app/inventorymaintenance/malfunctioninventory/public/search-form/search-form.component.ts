import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InventorymaintenanceService} from "../../../inventorymaintenance.service";

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

    malfNumber: string;                    // 查询故障单号字段
    malfFrom: string;                      // 查询报障人字段
    malfOccur: Date;                       // 查询发生时间字段
    malstatus: string;                     // 查询状态字段
    selectedStatus: string;                // 查询选中字段
    @Input() searchType: string;
    @Output() searchInfoEmitter: EventEmitter<Object>  = new EventEmitter();

    zh: any;                               // 汉化

    constructor(private maintenanceService: InventorymaintenanceService) { }

    ngOnInit() {
        this.zh = this.initZh();
        this.initFlowChart();
    }

    initZh(): object {
        return {
            firstDayOfWeek: 1,
            dayNames: [ '周一', '周二', '周三', '周四', '周五', '周六', '周日' ],
            dayNamesShort: [ '一', '二', '三', '四', '五', '六', '七' ],
            dayNamesMin: [ '一', '二', '三', '四', '五', '六', '七' ],
            monthNames: [ '一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月' ],
            monthNamesShort: [ '一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二' ],
        };
    }

    initFlowChart() {
        this.maintenanceService.getAllMalfunctionStatus().subscribe(res => {
            this.malstatus = res;
        });
    }

    searchSchedule() {
        console.log(this.selectedStatus);
        this.troubleGet();
    }

    troubleGet() {
        this.maintenanceService.getAllMainfunctionList('', '', this.malfNumber, this.malfFrom, this.malfOccur, this.selectedStatus['name']).subscribe(res => {
            if (res) {
                console.dir(res);
                this.searchInfoEmitter.emit(res);
            }
        });
    }
}
