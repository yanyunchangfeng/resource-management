import {Component, Input, OnInit} from '@angular/core';
import {Message} from 'primeng/primeng';
import {ActivatedRoute} from '@angular/router';
import {InventorymaintenanceService} from "../../../inventorymaintenance.service";

@Component({
  selector: 'app-colse-base-view',
  templateUrl: './colse-base-view.component.html',
  styleUrls: ['./colse-base-view.component.scss']
})
export class ColseBaseViewComponent implements OnInit {

    @Input() sid: string;

    message: Message[] = [];               // 交互提示弹出框
    resObj: Object = {};

    constructor(private maintenanceService: InventorymaintenanceService,
                private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.initTrick(this.sid);
    }
    initTrick(s) {
        this.maintenanceService.getTrick(s).subscribe(res => {
            let data = res['data'];
            if (res['errcode'] === '00000') {
                this.resObj = data;
            }else {
                this.message = [];
                this.message.push({severity: 'error', summary: '消息提示', detail: `获取故障单信息失败${res}`});
            }
        });
    }

}
