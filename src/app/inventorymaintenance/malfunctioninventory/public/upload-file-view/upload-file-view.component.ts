import {Component, Input, OnInit} from '@angular/core';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-upload-file-view',
  templateUrl: './upload-file-view.component.html',
  styleUrls: ['./upload-file-view.component.scss']
})
export class UploadFileViewComponent implements OnInit {

    @Input() file;
    constructor() { }

    ngOnInit() {
    }
    download() {
        window.open(`${environment.url.management}/${this.file.path}`, '_blank');
    }
}
