import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionEditInventoryComponent } from './malfunction-edit-inventory.component';

describe('MalfunctionEditInventoryComponent', () => {
  let component: MalfunctionEditInventoryComponent;
  let fixture: ComponentFixture<MalfunctionEditInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionEditInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionEditInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
