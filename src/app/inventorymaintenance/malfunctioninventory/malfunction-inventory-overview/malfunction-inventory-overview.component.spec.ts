import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionInventoryOverviewComponent } from './malfunction-inventory-overview.component';

describe('MalfunctionInventoryOverviewComponent', () => {
  let component: MalfunctionInventoryOverviewComponent;
  let fixture: ComponentFixture<MalfunctionInventoryOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionInventoryOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionInventoryOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
