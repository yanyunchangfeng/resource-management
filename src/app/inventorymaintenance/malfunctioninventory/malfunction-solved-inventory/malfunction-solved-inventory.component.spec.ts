import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionSolvedInventoryComponent } from './malfunction-solved-inventory.component';

describe('MalfunctionSolvedInventoryComponent', () => {
  let component: MalfunctionSolvedInventoryComponent;
  let fixture: ComponentFixture<MalfunctionSolvedInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionSolvedInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionSolvedInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
