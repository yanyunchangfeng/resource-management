import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionInventoryChangeComponent } from './malfunction-inventory-change.component';

describe('MalfunctionInventoryChangeComponent', () => {
  let component: MalfunctionInventoryChangeComponent;
  let fixture: ComponentFixture<MalfunctionInventoryChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionInventoryChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionInventoryChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
