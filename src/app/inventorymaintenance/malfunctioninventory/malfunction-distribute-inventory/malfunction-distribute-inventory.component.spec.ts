import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionDistributeInventoryComponent } from './malfunction-distribute-inventory.component';

describe('MalfunctionDistributeInventoryComponent', () => {
  let component: MalfunctionDistributeInventoryComponent;
  let fixture: ComponentFixture<MalfunctionDistributeInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionDistributeInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionDistributeInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
