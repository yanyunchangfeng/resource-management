import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-malfunction-distribute-inventory',
  templateUrl: './malfunction-distribute-inventory.component.html',
  styleUrls: ['./malfunction-distribute-inventory.component.scss']
})
export class MalfunctionDistributeInventoryComponent implements OnInit {

  searchType = '待分配';            // 搜索类型
  eventDatas: Object;                   // 子组件返回的数据

  constructor() { }

  ngOnInit() {
  }

  searchInfoEmitter(event) {
    console.log(event);
    this.eventDatas = event;
  }

}
