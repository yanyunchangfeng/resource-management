import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-malfunction-deal-inventory',
  templateUrl: './malfunction-deal-inventory.component.html',
  styleUrls: ['./malfunction-deal-inventory.component.scss']
})
export class MalfunctionDealInventoryComponent implements OnInit {
  searchType = '待处理';          // 搜索类型
  eventDatas: Object;                   // 子组件返回的数据

  constructor() { }

  ngOnInit() {
  }

  searchInfoEmitter(event) {
    this.eventDatas = event;
  }

}
