import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionDealInventoryComponent } from './malfunction-deal-inventory.component';

describe('MalfunctionDealInventoryComponent', () => {
  let component: MalfunctionDealInventoryComponent;
  let fixture: ComponentFixture<MalfunctionDealInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionDealInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionDealInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
