import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionAddInventoryComponent } from './malfunction-add-inventory.component';

describe('MalfunctionAddInventoryComponent', () => {
  let component: MalfunctionAddInventoryComponent;
  let fixture: ComponentFixture<MalfunctionAddInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionAddInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionAddInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
