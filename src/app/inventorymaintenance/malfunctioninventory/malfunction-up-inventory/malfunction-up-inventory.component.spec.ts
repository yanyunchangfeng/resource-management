import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionUpInventoryComponent } from './malfunction-up-inventory.component';

describe('MalfunctionUpInventoryComponent', () => {
  let component: MalfunctionUpInventoryComponent;
  let fixture: ComponentFixture<MalfunctionUpInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionUpInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionUpInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
