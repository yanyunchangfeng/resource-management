import { Component, OnInit } from '@angular/core';
import {PUblicMethod} from "../../public/PUblicMethod";

@Component({
  selector: 'app-malfunction-solve-inventory',
  templateUrl: './malfunction-solve-inventory.component.html',
  styleUrls: ['./malfunction-solve-inventory.component.scss']
})
export class MalfunctionSolveInventoryComponent implements OnInit {

  zh: any;
  sid: string;
  startDealTime = PUblicMethod.formateEnrtyTime(new Date());
  initStatus = '';                       // 故障单状态
  malfNumber = '';                       // 服务单号


  searchType = '处理中';            // 搜索类型
  eventDatas: Object;                   // 子组件返回的数据

  constructor() { }

  ngOnInit() {
  }

  searchInfoEmitter(event) {
    console.log(event);
    this.eventDatas = event;
  }

}
