import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionSolveInventoryComponent } from './malfunction-solve-inventory.component';

describe('MalfunctionSolveInventoryComponent', () => {
  let component: MalfunctionSolveInventoryComponent;
  let fixture: ComponentFixture<MalfunctionSolveInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionSolveInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionSolveInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
