import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionBaseInventoryComponent } from './malfunction-base-inventory.component';

describe('MalfunctionBaseInventoryComponent', () => {
  let component: MalfunctionBaseInventoryComponent;
  let fixture: ComponentFixture<MalfunctionBaseInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionBaseInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionBaseInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
