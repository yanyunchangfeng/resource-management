import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-malfunction-off-inventory',
  templateUrl: './malfunction-off-inventory.component.html',
  styleUrls: ['./malfunction-off-inventory.component.scss']
})
export class MalfunctionOffInventoryComponent implements OnInit {

  searchType = '已解决';    // 搜索类型
  eventDatas: Object;     // 子组件返回的数据

  constructor() { }

  ngOnInit() {
  }

  searchInfoEmitter(event) {
    console.log(event);
    this.eventDatas = event;
  }

}
