import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionOffInventoryComponent } from './malfunction-off-inventory.component';

describe('MalfunctionOffInventoryComponent', () => {
  let component: MalfunctionOffInventoryComponent;
  let fixture: ComponentFixture<MalfunctionOffInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionOffInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionOffInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
