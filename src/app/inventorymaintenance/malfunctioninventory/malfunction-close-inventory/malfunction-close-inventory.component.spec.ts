import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionCloseInventoryComponent } from './malfunction-close-inventory.component';

describe('MalfunctionCloseInventoryComponent', () => {
  let component: MalfunctionCloseInventoryComponent;
  let fixture: ComponentFixture<MalfunctionCloseInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionCloseInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionCloseInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
