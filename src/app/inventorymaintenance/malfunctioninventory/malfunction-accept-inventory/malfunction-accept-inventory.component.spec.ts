import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionAcceptInventoryComponent } from './malfunction-accept-inventory.component';

describe('MalfunctionAcceptInventoryComponent', () => {
  let component: MalfunctionAcceptInventoryComponent;
  let fixture: ComponentFixture<MalfunctionAcceptInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionAcceptInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionAcceptInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
