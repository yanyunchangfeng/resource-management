import { Component, OnInit } from '@angular/core';
import {EventBusService} from "../../../services/event-bus.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Message} from "primeng/primeng";
import {InventorymaintenanceService} from "../../inventorymaintenance.service";

@Component({
  selector: 'app-malfunction-accept-inventory',
  templateUrl: './malfunction-accept-inventory.component.html',
  styleUrls: ['./malfunction-accept-inventory.component.scss']
})
export class MalfunctionAcceptInventoryComponent implements OnInit {

  message: Message[] = [];               // 交互提示弹出框
  sid: string;
  initStatus = '';                       // 故障单状态
  resObj: Object;
  assign_time: string;
  accept_time: string;

  constructor(private maintenanceService: InventorymaintenanceService,
              private router: Router,
              private activatedRouter: ActivatedRoute,
              private eventBusService: EventBusService) { }

  ngOnInit() {
    this.sid = this.activatedRouter.snapshot.paramMap.get('id');
    this.initStatus = this.activatedRouter.snapshot.paramMap.get('status');
    this.initTrick(this.sid);
    this.maintenanceService.getFlowChart(this.initStatus).subscribe(res => {
      this.eventBusService.maintenance.next(res);
    });

  }
  initTrick(s) {
    this.maintenanceService.getTrick(s).subscribe(res => {
      let data = res['data'];
      if (res['errcode'] === '00000') {
        this.resObj = data;
        console.log(this.resObj['assign_time']);
        this.assign_time = this.resObj['assign_time'];
        this.accept_time = this.resObj['accept_time'];
        console.log(555);
      }else {
        this.message = [];
        this.message.push({severity: 'error', summary: '消息提示', detail: `获取故障单信息失败${res}`});
      }
    });
  }
  accept() {
    let fromSource = {
      'sid': this.sid,
      'status': '待处理'
    };
    this.maintenanceService.recieveOrRejectTrick(fromSource).subscribe(res => {
      if ( res === '00000') {
        this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionProcess', {id: this.sid, status: this.initStatus}]);
      }else {
        this.message = [];
        this.message.push({severity: 'error', summary: '消息提示', detail: `故障单接受失败${res}`});
      }
    });
  }

  // 拒绝故障单
  submitTrick() {
    let paper = {
      'sid': this.sid,
      'status': '待分配'
    };
    this.submitPost(paper);
  }
  submitPost(paper) {
    this.maintenanceService.recieveOrRejectTrick( paper ).subscribe(res => {
      if ( res === '00000' ) {
        this.message = [];
        this.message.push({severity: 'success', summary: '消息提示', detail: '操作成功'});
        window.setTimeout(() => {
          this.router.navigateByUrl('/index/maintenance/malfunctionBase');
        }, 1100);
      }else {
        this.message = [];
        this.message.push({severity: 'error', summary: '消息提示', detail: `操作失败${res}`});
      }
    });
  }

}
