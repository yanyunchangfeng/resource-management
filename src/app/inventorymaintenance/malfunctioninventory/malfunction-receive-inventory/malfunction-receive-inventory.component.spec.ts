import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionReceiveInventoryComponent } from './malfunction-receive-inventory.component';

describe('MalfunctionReceiveInventoryComponent', () => {
  let component: MalfunctionReceiveInventoryComponent;
  let fixture: ComponentFixture<MalfunctionReceiveInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionReceiveInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionReceiveInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
