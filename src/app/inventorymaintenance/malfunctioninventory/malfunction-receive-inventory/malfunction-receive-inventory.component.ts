import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-malfunction-receive-inventory',
  templateUrl: './malfunction-receive-inventory.component.html',
  styleUrls: ['./malfunction-receive-inventory.component.scss']
})
export class MalfunctionReceiveInventoryComponent implements OnInit {
  searchType = '待接受';            // 搜索类型
  eventDatas: Object;                    // 子组件返回的数据

  constructor() { }

  ngOnInit() {
  }

  searchInfoEmitter(event) {
    this.eventDatas = event;
  }

}
