import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionAcceptEditInventoryComponent } from './malfunction-accept-edit-inventory.component';

describe('MalfunctionAcceptEditInventoryComponent', () => {
  let component: MalfunctionAcceptEditInventoryComponent;
  let fixture: ComponentFixture<MalfunctionAcceptEditInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionAcceptEditInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionAcceptEditInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
