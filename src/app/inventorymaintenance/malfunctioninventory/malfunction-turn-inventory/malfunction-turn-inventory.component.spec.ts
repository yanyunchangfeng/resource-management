import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionTurnInventoryComponent } from './malfunction-turn-inventory.component';

describe('MalfunctionTurnInventoryComponent', () => {
  let component: MalfunctionTurnInventoryComponent;
  let fixture: ComponentFixture<MalfunctionTurnInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionTurnInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionTurnInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
