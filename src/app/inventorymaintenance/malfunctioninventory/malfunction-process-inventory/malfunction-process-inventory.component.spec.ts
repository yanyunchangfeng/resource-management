import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionProcessInventoryComponent } from './malfunction-process-inventory.component';

describe('MalfunctionProcessInventoryComponent', () => {
  let component: MalfunctionProcessInventoryComponent;
  let fixture: ComponentFixture<MalfunctionProcessInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionProcessInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionProcessInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
