import { Component, OnInit } from '@angular/core';
import {EventBusService} from "../../../services/event-bus.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Message, TreeNode} from "primeng/primeng";
import {InventorymaintenanceService} from "../../inventorymaintenance.service";

@Component({
  selector: 'app-malfunction-process-inventory',
  templateUrl: './malfunction-process-inventory.component.html',
  styleUrls: ['./malfunction-process-inventory.component.scss']
})
export class MalfunctionProcessInventoryComponent implements OnInit {
  resObj: Object;
  assign_time: string;
  accept_time: string;
  sid: string;
  initStatus = '';                       // 故障单状态
  malfNumber = '';                       // 服务单号

  malDepPersonOptions: any = [];         // 分配个人可选数据
  malDepPerson = '';                     // 分配个人

  reporter: TreeNode[] = [];             // 报告人
  zh: any;
  uploadedFiles = [];
  ip: string;                            // 127.0.0.1:80
  message: Message[] = [];               // 交互提示弹出框
  msg: Message[] = [];                   // 验证弹出框

  belongSystem: TreeNode[] = [];         // 所属系统
  locationOccur: TreeNode[] = [];        // 发生地点
  relativeCategore: TreeNode[] = [];     // 关联目录
  relativeEquip: TreeNode[] = [];        // 关联设备编号
  selectedDepartment: TreeNode[] = [];   // 分配至部门
  departementDisplay: boolean;           // 树显示控制
  filesTree4: TreeNode[];                // 树的可选值
  selected: TreeNode[];                  // 树的选中值
  dialogHeader: string;                  // 组织树弹框标题
  selectedMode: string;                  // 树的选择模式

  constructor(private maintenanceService: InventorymaintenanceService,
              private router: Router,
              private activatedRouter: ActivatedRoute,
              private eventBusService: EventBusService) { }

  ngOnInit() {
    this.sid = this.activatedRouter.snapshot.paramMap.get('id');
    this.initStatus = this.activatedRouter.snapshot.paramMap.get('status');
    this.initTrick(this.sid);
    this.maintenanceService.getFlowChart(this.initStatus).subscribe(res => {
      this.eventBusService.maintenance.next(res);
    });
  }
  initTrick(s) {
    this.maintenanceService.getTrick(s).subscribe(res => {
      let data = res['data'];
      if (res['errcode'] === '00000') {
        this.resObj = data;
        this.assign_time = this.resObj['assign_time'];
        this.accept_time = this.resObj['accept_time'];
      }else {
        this.message = [];
        this.message.push({severity: 'error', summary: '消息提示', detail: `获取故障单信息失败${res}`});
      }
    });
  }

  //  处理故障单
  deal() {
    let fromSource = {
      'sid': this.sid,
      'solve_per': '',
      'solve_per_pid': '',
      'solve_org': '',
      'solve_org_oid': '',
      'reason': '',
      'means': '',
      'finish_time': ''
    };
    this.maintenanceService.dealTrick(fromSource).subscribe(res => {
      console.log(res);
      if (res === '00000') {
        this.message = [];
        this.message.push({severity: 'success', summary: '消息提示', detail: '处理成功'});
        this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionSolved', {id: this.sid, status: this.initStatus, time: new Date()}]);
      }else {
        this.message = [];
        this.message.push({severity: 'error', summary: '消息提示', detail: '处理失败' + res});
      }
    });

  }
  // 升级故障单
  up() {
    this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionUp', {id: this.sid, status: this.initStatus}]);
  }
  turn() {
    this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionTurn', {id: this.sid, status: this.initStatus}]);
  }

}
