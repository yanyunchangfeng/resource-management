import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalfunctionInventoryComponent } from './malfunction-inventory.component';

describe('MalfunctionInventoryComponent', () => {
  let component: MalfunctionInventoryComponent;
  let fixture: ComponentFixture<MalfunctionInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalfunctionInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalfunctionInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
