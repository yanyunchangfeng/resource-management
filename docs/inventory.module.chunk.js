webpackJsonp(["inventory.module"],{

/***/ "../../../../../src/app/inventory/add-inventory-location/add-inventory-location.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"位置选择\" [(visible)]=\"display\" modal=\"modal\" width=\"500\" [responsive]=\"true\" (onHide)=\"closeLocationMask(false)\" class=\"tree\">\n  <p-tree [value]=\"filesTree4\"\n          selectionMode=\"single\"\n          [(selection)]=\"selectedFile\"\n          *ngIf=\"treeState === 'add'\"\n  ></p-tree>\n  <p-tree [value]=\"filesTree4\"\n          selectionMode=\"single\"\n          [(selection)]=\"selectedFile\"\n          *ngIf=\"treeState === 'update'\"\n  ></p-tree>\n  <!--{{selectedFile? selectedFile.label :'none'}}-->\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" (click)=\"formSubmit(false)\" label=\"确定\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeLocationMask(false)\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/add-inventory-location/add-inventory-location.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/add-inventory-location/add-inventory-location.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddInventoryLocationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddInventoryLocationComponent = (function () {
    function AddInventoryLocationComponent(inventoryService, route, storageService) {
        this.inventoryService = inventoryService;
        this.route = route;
        this.storageService = storageService;
        this.closeMask = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.addTree = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.updateTree = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.updateDev = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //库存位置
    }
    AddInventoryLocationComponent.prototype.ngOnInit = function () {
        this.display = true;
        if (this.treeState === 'update') {
            this.pnAnddataSource = this.inventoryService.deepClone(this.pnAnddataSource);
            this.submitData = {
                'sid': this.pnAnddataSource['sid'],
                "inventories": this.pnAnddataSource['data']
            };
        }
        this.queryMeterial();
    };
    AddInventoryLocationComponent.prototype.queryMeterial = function () {
        var _this = this;
        this.inventoryService.queryStorageLocationTree().subscribe(function (data) {
            _this.treeData = data;
            console.log(_this.treeData['father']);
            var nodeArr = [];
            nodeArr.push(_this.treeData);
            _this.filesTree4 = _this.inventoryService.changeObjectName(nodeArr, 'label', 'name');
            console.log(_this.selectedFile);
        });
    };
    // selectedTree($event){
    //    console.log(this.selectedFile)
    // }
    AddInventoryLocationComponent.prototype.formSubmit = function (bool) {
        var _this = this;
        if (this.treeState === 'update') {
            var index = this.storageService.getLocationIndex('locationIndex');
            this.submitData.inventories[index]['location'] = this.selectedFile.label;
            this.submitData.inventories[index]['location_id'] = this.selectedFile.did;
            this.inventoryService.modifiedLocation(this.submitData).subscribe(function (data) {
                _this.updateDev.emit(_this.submitData);
            }, function (err) {
                alert(err);
            });
        }
        else {
            this.addTree.emit(this.selectedFile);
        }
    };
    AddInventoryLocationComponent.prototype.closeLocationMask = function (bool) {
        this.closeMask.emit(bool);
    };
    AddInventoryLocationComponent.prototype.deepTree = function (tree, label) {
        for (var i in tree) {
            if (tree[i]['label'] === label) {
                return { obj: tree[i] };
            }
            if (Number(i) == tree.length - 1) {
                this.deepTree(tree[i]['children'], label);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddInventoryLocationComponent.prototype, "closeMask", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddInventoryLocationComponent.prototype, "addTree", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddInventoryLocationComponent.prototype, "updateTree", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AddInventoryLocationComponent.prototype, "pnAnddataSource", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AddInventoryLocationComponent.prototype, "treeState", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AddInventoryLocationComponent.prototype, "sid", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddInventoryLocationComponent.prototype, "updateDev", void 0);
    AddInventoryLocationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-inventory-location',
            template: __webpack_require__("../../../../../src/app/inventory/add-inventory-location/add-inventory-location.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/add-inventory-location/add-inventory-location.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */]])
    ], AddInventoryLocationComponent);
    return AddInventoryLocationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/add-or-update-material/add-or-update-material.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog [(visible)]=\"display\" modal=\"modal\"  [responsive]=\"true\" [width]=\"800\" (onHide)=\"closeMaterialMask(false)\">\n  <p-header>\n    {{title}}\n  </p-header>\n  <div class=\"ui-g\">\n    <div class=\"ui-g-12\">\n      <div class=\"file-box\">\n        <!--<form class=\"form-horizontal\" [formGroup]=\"addCycle\">-->\n        <form class=\"form-horizontal\" [formGroup]=\"materialForm\"  >\n          <div class=\"form-group\" >\n            <label  class=\"col-sm-2 control-label\"><span>*</span>物品编号:</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" formControlName=\"pn\"   name=\"pn\" class=\"form-control\" [(ngModel)]=\"submitData.pn\" *ngIf=\"state ==='add'\"   >\n              <input type=\"text\" formControlName=\"pn\" name=\"pn\" class=\"form-control\" [(ngModel)]=\"currentMeterial.pn\" *ngIf=\"state ==='update'\" readonly>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!materialForm.controls['pn'].valid&&(!materialForm.controls['pn'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                物品编号必填\n              </div>\n            </div>\n            <label  class=\"col-sm-2 control-label\"><span>*</span>物品名称:</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\"  formControlName=\"name\" name=\"reportTypewq\" class=\"form-control\" [(ngModel)]=\"submitData.name\" *ngIf=\"state ==='add'\" >\n              <input type=\"text\"  formControlName=\"name\" name=\"reportTypewq\" class=\"form-control\" [(ngModel)]=\"currentMeterial.name\" *ngIf=\"state ==='update'\" >\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!materialForm.controls['name'].valid&&(!materialForm.controls['name'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                物品名称必填\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label  class=\"col-sm-2 control-label\"><span></span>分类:</label>\n            <div class=\"col-sm-4 ui-fluid\">\n              <p-dropdown [autoWidth]=\"false\" formControlName=\"catagory\" [options]=\"categories\"  name=\"fenlei\" [(ngModel)]=\"submitData.catagory\" *ngIf=\"state ==='add'\"></p-dropdown>\n              <p-dropdown [autoWidth]=\"false\" formControlName=\"catagory\" [options]=\"categories\"  name=\"fenlei\" [(ngModel)]=\"currentMeterial.catagory\" *ngIf=\"state ==='update'\" ></p-dropdown>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!materialForm.controls['catagory'].valid&&(!materialForm.controls['catagory'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                分类必填\n              </div>\n            </div>\n            <label  class=\"col-sm-2 control-label\"><span>*</span>供应商:</label>\n            <!--supplier-->\n            <div class=\"col-sm-4 ui-fluid\">\n              <input type=\"text\" formControlName=\"supplier\" name=\"supplier\" class=\"form-control\" [(ngModel)]=\"submitData.supplier\" *ngIf=\"state ==='add'\" >\n              <input type=\"text\" formControlName=\"supplier\" name=\"supplier\" class=\"form-control\" [(ngModel)]=\"currentMeterial.supplier\"  *ngIf=\"state ==='update'\">\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!materialForm.controls['supplier'].valid&&(!materialForm.controls['supplier'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                供应商必填\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\" >\n            <label  class=\"col-sm-2 control-label\"><span>*</span>品牌:</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" formControlName=\"brand\" name=\"reportTypew\" class=\"form-control\" [(ngModel)]=\"submitData.brand\"  *ngIf=\"state ==='add'\">\n              <input type=\"text\" formControlName=\"brand\" name=\"reportTypew\" class=\"form-control\" [(ngModel)]=\"currentMeterial.brand\" *ngIf=\"state ==='update'\" >\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!materialForm.controls['brand'].valid&&(!materialForm.controls['brand'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                品牌必填\n              </div>\n            </div>\n            <label  class=\"col-sm-2 control-label\"><span>*</span>型号:</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" formControlName=\"model\" name=\"reportType1\" class=\"form-control\"  [(ngModel)]=\"submitData.model\" *ngIf=\"state ==='add'\"  >\n              <input type=\"text\" formControlName=\"model\" name=\"reportType1\" class=\"form-control\"  [(ngModel)]=\"currentMeterial.model\" *ngIf=\"state ==='update'\" >\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!materialForm.controls['model'].valid&&(!materialForm.controls['model'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                型号必填\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\" >\n            <label  class=\"col-sm-2 control-label\">规格:</label>\n            <div class=\"col-sm-4 ui-fluid\">\n              <p-dropdown formControlName=\"specification\" [autoWidth]=\"false\" [options]=\"norms\" name=\"guige\"  [(ngModel)]=\"submitData.specification\" class=\"inspection\" *ngIf=\"state ==='add'\"></p-dropdown>\n              <p-dropdown formControlName=\"specification\" [autoWidth]=\"false\" [options]=\"norms\" name=\"guige\"  [(ngModel)]=\"currentMeterial.specification\" class=\"inspection\" *ngIf=\"state ==='update'\"></p-dropdown>\n            </div>\n            <label  class=\"col-sm-2 control-label\"><span>*</span>状态:</label>\n            <div class=\"col-sm-4 ui-fluid\">\n              <p-radioButton formControlName=\"status\" name=\"group\" value=\"启用\" label=\"启用\"  inputId=\"preopt3\" [(ngModel)]=\"submitData.status\" *ngIf=\"state ==='add'\"></p-radioButton>\n              <p-radioButton formControlName=\"status\" name=\"group\" value=\"冻结\" label=\"冻结\"  inputId=\"preopt4\" [(ngModel)]=\"submitData.status\" *ngIf=\"state ==='add'\"></p-radioButton>\n              <p-radioButton formControlName=\"status\" name=\"group\" value=\"启用\" label=\"启用\"  inputId=\"preopt3\" [(ngModel)]=\"currentMeterial.status\" *ngIf=\"state ==='update'\"></p-radioButton>\n              <p-radioButton formControlName=\"status\" name=\"group\" value=\"冻结\" label=\"冻结\"  inputId=\"preopt4\" [(ngModel)]=\"currentMeterial.status\" *ngIf=\"state ==='update'\"></p-radioButton>\n            </div>\n          </div>\n          <div class=\"form-group\" >\n            <label  class=\"col-sm-2 control-label\"><span>*</span>安全库存:</label>\n            <div class=\"col-sm-4 ui-fluid\">\n              <p-dropdown [autoWidth]=\"false\" formControlName=\"threshold_group\" [options]=\"threshold_group\"   [(ngModel)]=\"submitData.threshold_group\" *ngIf=\"state ==='add'\"></p-dropdown>\n              <p-dropdown [autoWidth]=\"false\" formControlName=\"threshold_group\" [options]=\"threshold_group\"   [(ngModel)]=\"currentMeterial.threshold_group\" *ngIf=\"state ==='update'\"></p-dropdown>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!materialForm.controls['threshold_group'].valid&&(!materialForm.controls['threshold_group'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                库存阈值分组必填\n              </div>\n            </div>\n\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" label=\"确定\"  [disabled]=\"!materialForm.valid\" (click)=\"formSubmit(false)\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\"  label=\"取消\" (click)=\"closeMaterialMask(false)\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/add-or-update-material/add-or-update-material.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/deep/ .ui-button-icon-only .ui-button-text {\n  padding: 0.18em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/add-or-update-material/add-or-update-material.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddOrUpdateMaterialComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddOrUpdateMaterialComponent = (function () {
    function AddOrUpdateMaterialComponent(inventoryService, confirmationService) {
        this.inventoryService = inventoryService;
        this.confirmationService = confirmationService;
        this.display = false;
        this.closeMaterial = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"];
        this.addMaterial = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.updateDev = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.fb = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]();
        this.title = '新增';
    }
    AddOrUpdateMaterialComponent.prototype.ngOnInit = function () {
        this.materialForm = this.fb.group({
            'catagory': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'pn': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'name': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'supplier': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'brand': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'model': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'specification': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            // 'unit_cost':['',Validators.required],
            'threshold_group': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'status': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
        });
        this.submitData = {
            "catagory": '',
            "pn": "",
            "name": "",
            "supplier": "",
            "brand": "",
            "model": "",
            "specification": "",
            "unit_cost": "",
            "threshold_group": "",
            "status": ""
        };
        this.display = true;
        this.currentMeterial = this.inventoryService.deepClone(this.currentMeterial);
        if (this.state === 'update') {
            this.title = '编辑';
        }
        this.queryMeterial();
    };
    AddOrUpdateMaterialComponent.prototype.addDevice = function (bool) {
        var _this = this;
        this.inventoryService.addMaterial(this.submitData).subscribe(function () {
            _this.addMaterial.emit(bool);
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    AddOrUpdateMaterialComponent.prototype.updateDevice = function (bool) {
        var _this = this;
        this.inventoryService.updateMaterial(this.currentMeterial).subscribe(function () {
            _this.updateDev.emit(_this.currentMeterial);
            _this.closeMaterial.emit(bool);
        });
    };
    AddOrUpdateMaterialComponent.prototype.queryMeterial = function () {
        var _this = this;
        this.inventoryService.queryMaterialInfo().subscribe(function (data) {
            if (data['分类']) {
                _this.categories = data['分类'];
                _this.categories = _this.inventoryService.formatDropdownData(_this.categories);
                console.log(_this.categories);
            }
            else {
                _this.categories = [];
            }
            if (data['规格']) {
                _this.norms = data['规格'];
                _this.norms = _this.inventoryService.formatDropdownData(_this.norms);
            }
            else {
                _this.norms = [];
            }
            if (data['安全库存']) {
                _this.threshold_group = data['安全库存'];
                _this.threshold_group = _this.inventoryService.formatDropdownData(_this.threshold_group);
            }
            else {
                _this.threshold_group = [];
            }
            _this.submitData.catagory = _this.categories[0]['label'];
            _this.submitData.specification = _this.norms[0]['label'];
            _this.submitData.threshold_group = _this.threshold_group[0]['label'];
        });
    };
    AddOrUpdateMaterialComponent.prototype.formSubmit = function (bool) {
        if (this.state === 'add') {
            this.addDevice(bool);
        }
        else {
            this.updateDevice(bool);
        }
    };
    AddOrUpdateMaterialComponent.prototype.closeMaterialMask = function (bool) {
        this.closeMaterial.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddOrUpdateMaterialComponent.prototype, "closeMaterial", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddOrUpdateMaterialComponent.prototype, "addMaterial", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddOrUpdateMaterialComponent.prototype, "updateDev", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AddOrUpdateMaterialComponent.prototype, "currentMeterial", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AddOrUpdateMaterialComponent.prototype, "state", void 0);
    AddOrUpdateMaterialComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-or-update-material',
            template: __webpack_require__("../../../../../src/app/inventory/add-or-update-material/add-or-update-material.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/add-or-update-material/add-or-update-material.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"]])
    ], AddOrUpdateMaterialComponent);
    return AddOrUpdateMaterialComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/add-storage/add-storage.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"{{title}}\" [(visible)]=\"display\" modal=\"modal\" [width]=\"width\" [responsive]=\"true\" (onHide)=\"closeAddStorageMask(false)\">\n  <div class=\"content-section  GridDemo \">\n\n    <form class=\"form-horizontal storage\" [formGroup]=\"storageForm\">\n        <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n          <div class=\"ui-grid-row margin-bottom-1vw\">\n            <div class=\"ui-grid-col-2 text-right \">\n              <label ><span>*</span>物品编号:</label>\n            </div>\n            <div [ngClass]=\"state!=='show'&&state!=='update'?'ui-grid-col-2':'ui-grid-col-3'\">\n              <input formControlName=\"pn\"  name=\"pn\"  type=\"text\" pInputText   placeholder=\"物品编号\" [(ngModel)]=\"submitData.pn\" *ngIf=\"state ==='add'\">\n              <input formControlName=\"pn\"  name=\"pn\"  type=\"text\" pInputText   placeholder=\"物品编号\" [(ngModel)]=\"currentMeterial.pn\" *ngIf=\"state ==='update'\">\n              <input formControlName=\"pn\"  name=\"pn\"  type=\"text\" pInputText   placeholder=\"物品编号\" [(ngModel)]=\"currentMeterial.pn\" *ngIf=\"state ==='show'\" readonly>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['pn'].valid&&(!storageForm.controls['pn'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                物品编号必填\n              </div>\n            </div>\n            <div class=\"ui-grid-col-1\" *ngIf=\"state!=='show'&& state!=='update'\">\n              <button pButton  type=\"button\" (click)=\"showChooseMaterialNumberMask()\" label=\"选择\" ></button>\n            </div>\n            <div class=\"ui-grid-col-2 text-right\">\n              <label ><span>*</span>设备名称:</label>\n            </div>\n            <div class=\"ui-grid-col-3\">\n              <input formControlName=\"name\"  name=\"name\"  type=\"text\" pInputText   placeholder=\"设备名称\" [(ngModel)]=\"submitData.name\" *ngIf=\"state ==='add'\">\n              <input formControlName=\"name\"  name=\"name\"  type=\"text\" pInputText   placeholder=\"设备名称\" [(ngModel)]=\"currentMeterial.name\" *ngIf=\"state ==='update'\" >\n              <input formControlName=\"name\"  name=\"name\"  type=\"text\" pInputText   placeholder=\"设备名称\" [(ngModel)]=\"currentMeterial.name\" *ngIf=\"state ==='show'\"  readonly>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['name'].valid&&(!storageForm.controls['name'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                设备名称必填\n              </div>\n            </div>\n          </div>\n          <div class=\"ui-grid-row margin-bottom-1vw\">\n            <div class=\"ui-grid-col-2 text-right\">\n              <label ><span>*</span>物品分类:</label>\n            </div>\n            <div class=\"ui-grid-col-3\">\n              <!--<p-dropdown   [autoWidth]=\"false\"></p-dropdown>-->\n              <input formControlName=\"catagory\" name=\"catagory\"  type=\"text\" pInputText   placeholder=\"物品分类\" [(ngModel)]=\"submitData.catagory\" *ngIf=\"state ==='add'\">\n              <input formControlName=\"catagory\" name=\"catagory\"  type=\"text\" pInputText   placeholder=\"物品分类\" [(ngModel)]=\"currentMeterial.catagory\" *ngIf=\"state ==='update'\">\n              <input formControlName=\"catagory\" name=\"catagory\"  type=\"text\" pInputText   placeholder=\"物品分类\" [(ngModel)]=\"currentMeterial.catagory\" *ngIf=\"state ==='show'\" readonly>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['catagory'].valid&&(!storageForm.controls['catagory'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                物品分类必填\n              </div>\n            </div>\n            <div class=\"ui-grid-col-2 text-right\">\n              <label ><span>*</span>供应商:</label>\n            </div>\n\n            <div class=\"ui-grid-col-3\">\n              <input formControlName=\"supplier\" type=\"text\" pInputText  name=\"supplier\" placeholder=\"供应商\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"submitData.supplier\" *ngIf=\"state ==='add'\"/>\n              <input formControlName=\"supplier\" type=\"text\" pInputText  name=\"supplier\" placeholder=\"供应商\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"currentMeterial.supplier\" *ngIf=\"state ==='update'\"/>\n              <input formControlName=\"supplier\" type=\"text\" pInputText  name=\"supplier\" placeholder=\"供应商\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"currentMeterial.supplier\" *ngIf=\"state ==='show'\" readonly/>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['supplier'].valid&&(!storageForm.controls['supplier'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                供应商必填\n              </div>\n            </div>\n          </div>\n          <div class=\"ui-grid-row margin-bottom-1vw\">\n            <div class=\"ui-grid-col-2 text-right\">\n              <label ><span>*</span>品牌:</label>\n            </div>\n            <div class=\"ui-grid-col-3\">\n              <input formControlName=\"brand\" type=\"text\" pInputText  name=\"brand\" placeholder=\"品牌\"\n                    class=\"cursor_not_allowed\" [(ngModel)]=\"submitData.brand\" *ngIf=\"state ==='add'\"/>\n              <input formControlName=\"brand\" type=\"text\" pInputText  name=\"brand\" placeholder=\"品牌\"\n                    class=\"cursor_not_allowed\" [(ngModel)]=\"currentMeterial.brand\" *ngIf=\"state ==='update'\"/>\n              <input formControlName=\"brand\" type=\"text\" pInputText  name=\"brand\" placeholder=\"品牌\"\n                    class=\"cursor_not_allowed\" [(ngModel)]=\"currentMeterial.brand\" *ngIf=\"state ==='show'\" readonly/>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['brand'].valid&&(!storageForm.controls['brand'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                品牌必填\n              </div>\n            </div>\n            <div class=\"ui-grid-col-2 text-right\">\n              <label ><span>*</span>型号</label>\n            </div>\n            <div class=\"ui-grid-col-3\">\n              <input formControlName=\"model\" type=\"text\" pInputText  name=\"model\" placeholder=\"型号\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"submitData.model\" *ngIf=\"state ==='add'\"/>\n              <input formControlName=\"model\" type=\"text\" pInputText  name=\"model\" placeholder=\"型号\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"currentMeterial.model\" *ngIf=\"state ==='update'\"/>\n              <input formControlName=\"model\" type=\"text\" pInputText  name=\"model\" placeholder=\"型号\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"currentMeterial.model\" *ngIf=\"state ==='show'\" readonly/>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['model'].valid&&(!storageForm.controls['model'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                型号必填\n              </div>\n            </div>\n          </div>\n          <div class=\"ui-grid-row margin-bottom-1vw\">\n            <div class=\"ui-grid-col-2 text-right\">\n              <label ><span>*</span>规格:</label>\n            </div>\n            <div class=\"ui-grid-col-3\">\n              <input formControlName=\"specification\" type=\"text\" pInputText  name=\"specification\" placeholder=\"规格\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"submitData.specification\" *ngIf=\"state ==='add'\"/>\n              <input formControlName=\"specification\" type=\"text\" pInputText  name=\"specification\" placeholder=\"规格\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"currentMeterial.specification\" *ngIf=\"state ==='update'\"/>\n              <input formControlName=\"specification\" type=\"text\" pInputText  name=\"specification\" placeholder=\"规格\"\n                      class=\"cursor_not_allowed\"  [(ngModel)]=\"currentMeterial.specification\" *ngIf=\"state ==='show'\"/>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['specification'].valid&&(!storageForm.controls['specification'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                规格必填\n              </div>\n            </div>\n            <div class=\"ui-grid-col-2 text-right\">\n              <label >采购时间:</label>\n            </div>\n            <div class=\"ui-grid-col-3\">\n              <p-calendar formControlName=\"time_purchase\" name=\"begin_time\" [showTime]=\"true\"  dateFormat=\"yy-mm-dd\" [showSeconds]=\"true\"  readonlyInput=\"true\"  dataType=\"string\" [(ngModel)]=\"submitData.time_purchase\"  [showIcon]=\"true\" [locale]=\"zh\" *ngIf=\"state ==='add'\"></p-calendar>\n              <p-calendar formControlName=\"time_purchase\" name=\"begin_time\" [showTime]=\"true\"  dateFormat=\"yy-mm-dd\" [showSeconds]=\"true\"  readonlyInput=\"true\"  dataType=\"string\" [(ngModel)]=\"currentMeterial.time_purchase\"  [showIcon]=\"true\" [locale]=\"zh\" *ngIf=\"state ==='update'\"></p-calendar >\n              <input formControlName=\"time_purchase\" type=\"text\" pInputText  name=\"specification\" placeholder=\"采购时间\"\n                     class=\"cursor_not_allowed\"  [(ngModel)]=\"currentMeterial.time_purchase\" *ngIf=\"state ==='show'\" readonly/>\n            </div>\n          </div>\n          <div class=\"ui-grid-row margin-bottom-1vw\">\n            <div class=\"ui-grid-col-2 text-right\">\n              <label ><span>*</span>数量:</label>\n            </div>\n            <div class=\"ui-grid-col-3\">\n              <input formControlName=\"quantity\" type=\"number\" pInputText  name=\"number\" placeholder=\"数量\" [(ngModel)]=\"submitData.quantity\"\n                      class=\"cursor_not_allowed\"  *ngIf=\"state ==='add'\" />\n              <input formControlName=\"quantity\" type=\"number\" pInputText  name=\"number\" placeholder=\"数量\" [(ngModel)]=\"currentMeterial.quantity\"\n                      class=\"cursor_not_allowed\"  *ngIf=\"state ==='update'\" />\n              <input formControlName=\"quantity\" type=\"text\" pInputText  name=\"number\" placeholder=\"数量\" [(ngModel)]=\"currentMeterial.quantity\"\n                      class=\"cursor_not_allowed\"  *ngIf=\"state ==='show'\"  readonly/>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['quantity'].valid&&(!storageForm.controls['quantity'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                数量必填\n              </div>\n            </div>\n            <div class=\"ui-grid-col-2 text-right\">\n              <label ><span></span>单价:</label>\n            </div>\n            <div class=\"ui-grid-col-3 text-right\">\n              <input formControlName=\"unit_cost\" type=\"text\" pInputText  name=\"cost\" placeholder=\"单价\" [(ngModel)]=\"submitData.unit_cost\"\n                     class=\"cursor_not_allowed\"  *ngIf=\"state ==='add'\" />\n              <input formControlName=\"unit_cost\" type=\"text\" pInputText  name=\"cost\" placeholder=\"单价\" [(ngModel)]=\"currentMeterial.unit_cost\"\n                     class=\"cursor_not_allowed\"  *ngIf=\"state ==='update'\" />\n              <input formControlName=\"unit_cost\" type=\"text\" pInputText  name=\"cost\" placeholder=\"单价\" [(ngModel)]=\"currentMeterial.unit_cost\"\n                     class=\"cursor_not_allowed\"  *ngIf=\"state ==='show'\"  readonly/>\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!storageForm.controls['unit_cost'].valid&&(!storageForm.controls['unit_cost'].untouched)\" >\n                <i class=\"fa fa-close\"></i>\n                单价必填\n              </div>\n            </div>\n\n          </div>\n        </div>\n    </form>\n  </div>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\"  label=\"确定\" (click)=\"formSubmit()\" *ngIf=\"state!=='show'\" [disabled]=\"!storageForm.valid\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeAddStorageMask(false)\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n<app-choose-material-number\n  *ngIf=\"showChooseMaterialNumber\"\n  (closeChooseMaterialNumber)=\"closeChooseMaterialNumber($event)\"\n  (addDev)=\"addDev($event)\"\n></app-choose-material-number>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/add-storage/add-storage.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "label > span {\n  color: red; }\n\n.storage /deep/ .ui-messages-error {\n  color: red;\n  background-color: white;\n  border-color: white; }\n\n.storage {\n  min-height: 300px;\n  overflow: auto; }\n\n.storage input {\n  border-bottom: 1px solid #D6D6D6; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/add-storage/add-storage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddStorageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddStorageComponent = (function () {
    function AddStorageComponent(inventoryService, confirmationService) {
        this.inventoryService = inventoryService;
        this.confirmationService = confirmationService;
        this.closeAddStorage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"];
        this.addStorage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.updateStorage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.title = '添加';
        this.display = false;
        this.showChooseMaterialNumber = false; //选择物料编号弹框
        this.fb = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]();
    }
    AddStorageComponent.prototype.ngOnInit = function () {
        if (window.innerWidth < 1024) {
            this.width = window.innerWidth * 0.7;
        }
        else {
            this.width = window.innerWidth * 0.6;
        }
        this.storageForm = this.fb.group({
            'pn': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'name': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'catagory': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'supplier': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'brand': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'model': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'specification': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'unit_cost': [''],
            'time_purchase': [''],
            'quantity': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
        });
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            dayNamesShort: ["一", "二", "三", "四", "五", "六", "七"],
            dayNamesMin: ["一", "二", "三", "四", "五", "六", "七"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
        };
        this.display = true;
        this.submitData = {
            "pn": "",
            "name": "",
            "catagory": "",
            "supplier": "",
            "brand": "",
            "model": "",
            "specification": "",
            "unit_cost": "",
            "time_purchase": "",
            "quantity": ""
        };
        if (this.state === 'update') {
            this.currentMeterial = this.inventoryService.deepClone(this.currentMeterial);
            this.title = '编辑';
        }
        if (this.state === 'show') {
            this.title = '查看';
        }
    };
    AddStorageComponent.prototype.addDevice = function (bool) {
        var _this = this;
        this.inventoryService.addStorage(this.submitData).subscribe(function () {
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    AddStorageComponent.prototype.storageSave = function (bool) {
        this.addStorage.emit(this.submitData);
    };
    AddStorageComponent.prototype.formSubmit = function (bool) {
        if (this.state === 'add') {
            this.storageSave(bool);
        }
        else if (this.state === 'update') {
            this.updateStorage.emit(this.currentMeterial);
        }
    };
    AddStorageComponent.prototype.closeAddStorageMask = function (bool) {
        this.closeAddStorage.emit(bool);
    };
    AddStorageComponent.prototype.showChooseMaterialNumberMask = function () {
        this.showChooseMaterialNumber = !this.showChooseMaterialNumber;
    };
    AddStorageComponent.prototype.closeChooseMaterialNumber = function (bool) {
        this.showChooseMaterialNumber = bool;
    };
    AddStorageComponent.prototype.addDev = function (metail) {
        this.submitData['pn'] = metail['pn'];
        this.submitData['name'] = metail['name'];
        this.submitData['catagory'] = metail['catagory'];
        this.submitData['supplier'] = metail['supplier'];
        this.submitData['brand'] = metail['brand'];
        this.submitData['specification'] = metail['specification'];
        this.submitData['unit_cost'] = metail['unit_cost'] ? metail['unit_cost'] : '';
        this.submitData['threshold_group'] = metail['threshold_group'];
        this.submitData['status'] = metail['status'];
        // this.submitData['time_purchase'] =metail['time_purchase'];
        this.submitData['model'] = metail['model'];
        this.showChooseMaterialNumber = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddStorageComponent.prototype, "closeAddStorage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddStorageComponent.prototype, "addStorage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddStorageComponent.prototype, "updateStorage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AddStorageComponent.prototype, "currentMeterial", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AddStorageComponent.prototype, "state", void 0);
    AddStorageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-storage',
            template: __webpack_require__("../../../../../src/app/inventory/add-storage/add-storage.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/add-storage/add-storage.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"]])
    ], AddStorageComponent);
    return AddStorageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/borrow-approved/borrow-approved.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"content-section implementation GridDemo clearfixes borrowapproved\">\n  <form [formGroup]=\"borrowapprovedForm\" >\n    <p-panel>\n      <p-header>\n        <div class=\"ui-helper-clearfix\">\n          <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n          <button  pButton type=\"button\"  label=\"返回\" (click)=\"approvedgoBack()\" [ngStyle]=\"{'width':'75px'}\" ></button>\n          <button pButton [ngStyle]=\"{'float':'right'}\"   *ngFor=\"let optbutton of optButtons\"  label=\"{{optbutton.label}}\" [disabled]=\"!borrowapprovedForm.valid\" (click)=\"approvedOption(optbutton.status)\"></button>\n\n        </div>\n      </p-header>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >借用单号:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"sid\"  name=\"sid\"  type=\"text\" pInputText   value=\"{{currentMaterial?.sid}}\" readonly>\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >申请类型:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"types\" name=\"types\"  type=\"text\" pInputText    value=\"{{currentMaterial?.type}}\" readonly>\n\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >借用人:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"creator\" name=\"creator\"  type=\"text\" pInputText    value=\"{{currentMaterial?.creator}}\" readonly>\n\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >部门名称:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"org\"  name=\"org\"  type=\"text\" pInputText    value=\"{{currentMaterial?.org}}\" readonly>\n\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >借用时间:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"create_time\" name=\"create_time\"  type=\"text\" pInputText    value=\"{{currentMaterial?.create_time}}\" readonly>\n\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >归还时间:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"return_time\"  name=\"return_time\"  type=\"text\" pInputText    value=\"{{currentMaterial?.return_time}}\" readonly>\n\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >借用原因:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"remarks\" name=\"remarks\"  type=\"text\" pInputText    value=\"{{currentMaterial?.remarks}}\" readonly>\n\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label ><span>*</span>审批意见:</label>\n          </div>\n          <div class=\"ui-grid-col-11\">\n            <textarea formControlName=\"approve_remarks\" pInputTextarea type=\"text\" name=\"approve_remarks\"  [(ngModel)]=\"approvedModel['approve_remarks']\"  ></textarea>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!borrowapprovedForm.controls['approve_remarks'].valid&&(!borrowapprovedForm.controls['approve_remarks'].untouched)\" >\n              <i class=\"fa fa-close\"></i>\n              审批意见必填\n            </div>\n          </div>\n        </div>\n      </div>\n    </p-panel>\n  </form>\n  <div class=\"ui-g\">\n    <div class=\"mysearch\">\n      借用列表\n    </div>\n  </div>\n\n  <!--<label></label>-->\n\n  <p-dataTable emptyMessage=\"没有数据\" [value]=\"inventories\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n               [totalRecords]=\"totalRecords\" (onLazyLoad)=\"loadCarsLazy($event)\" [stacked]=\"stacked\" [responsive]=\"true\" >\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n  </p-dataTable>\n</div>\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-approved/borrow-approved.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/deep/.ui-widget-header {\n  background-color: white;\n  background: linear-gradient(to bottom, #ffffff 0%, #ffffff 100%); }\n\n.mysearch {\n  margin-bottom: 20px;\n  padding: 10px;\n  margin-top: 20px; }\n\nlabel > span {\n  color: red; }\n\n.borrowapproved /deep/ .ui-messages-error {\n  color: red;\n  background-color: white;\n  border-color: white; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-approved/borrow-approved.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BorrowApprovedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BorrowApprovedComponent = (function () {
    function BorrowApprovedComponent(route, router, inventoryService, eventBusService) {
        this.route = route;
        this.router = router;
        this.inventoryService = inventoryService;
        this.eventBusService = eventBusService;
        this.inventories = [];
        this.fb = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]();
    }
    BorrowApprovedComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.borrowapprovedForm = this.fb.group({
            'sid': [''],
            'types': [''],
            'creator': [''],
            'org': [''],
            'create_time': [''],
            'return_time': [''],
            'remarks': [''],
            'approve_remarks': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
        });
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            dayNamesShort: ["一", "二", "三", "四", "五", "六", "七"],
            dayNamesMin: ["一", "二", "三", "四", "五", "六", "七"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
        };
        this.cols = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'application_quantity', header: '数量', editable: false },
        ];
        this.approvedModel = {
            "sid": "",
            "approve_remarks": "",
            "status": ""
        };
        this.route.queryParams.subscribe(function (parms) {
            // if(parms['sid'] ||parms['sid']){
            _this.sid = parms['sid'];
            _this.evstatus = parms['status'];
            // }
            _this.approvedModel.sid = _this.sid;
            _this.inventoryService.queryOutbound(_this.sid).subscribe(function (data) {
                if (!_this.dataSource) {
                    _this.dataSource = [];
                }
                _this.currentMaterial = data;
                _this.dataSource = _this.currentMaterial.inventories;
                _this.inventories = _this.dataSource.slice(0, 10);
                _this.totalRecords = _this.dataSource.length;
            });
        });
        this.optButtons = [
            { label: '驳回', status: '驳回' },
            { label: '拒绝', status: '拒绝' },
            { label: '通过', status: '待出库' },
        ];
    };
    BorrowApprovedComponent.prototype.approvedOption = function (status) {
        var _this = this;
        this.approvedModel.status = status;
        this.inventoryService.queryOutboundApproved(this.approvedModel).subscribe(function () {
            _this.currentMaterial['status'] = "";
            _this.eventBusService.borrow.next(_this.currentMaterial['status']);
            _this.router.navigate(['../outbound'], { relativeTo: _this.route });
        });
    };
    BorrowApprovedComponent.prototype.loadCarsLazy = function (event) {
        if (this.dataSource) {
            this.inventories = this.dataSource.slice(event.first, (event.first + event.rows));
        }
    };
    BorrowApprovedComponent.prototype.approvedgoBack = function () {
        var sid = this.currentMaterial['sid'];
        this.currentMaterial['status'] = "";
        this.eventBusService.borrow.next(this.currentMaterial['status']);
        this.router.navigate(['../outbound'], { queryParams: { sid: sid, state: 'viewDetail', title: '借用总览' }, relativeTo: this.route });
    };
    BorrowApprovedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-borrow-approved',
            template: __webpack_require__("../../../../../src/app/inventory/borrow-approved/borrow-approved.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/borrow-approved/borrow-approved.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */]])
    ], BorrowApprovedComponent);
    return BorrowApprovedComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/borrow-items/borrow-items.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"物品清单\" [(visible)]=\"display\" modal=\"modal\" [width]=\"width\"  [responsive]=\"true\" (onHide)=\"closeBorrowItems(false)\">\n  <div class=\"content-section  GridDemo clearfixes borrowitems\">\n    <form>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n        <form>\n        <div class=\"mysearch\">\n          <div class=\"ui-g\">\n            <div class=\"ui-g-5 ui-sm-12\">\n              <label class=\"ui-g-2 ui-sm-5  text-right\">物品编号：</label>\n              <div class=\"ui-g-4 ui-sm-7 ui-fluid  ui-no-padding-right-15px \">\n                <input  pInputText type=\"text\" name=\"status\" placeholder=\"物品编号\"  [(ngModel)]=\"queryModel.condition.sid\" />\n              </div>\n              <label class=\"ui-g-2 ui-sm-5 text-right\">物品名称：</label>\n              <div class=\"ui-g-4  ui-sm-7 ui-fluid\">\n                <input pInputText type=\"text\" name=\"sid\" placeholder=\"物品名称\" [(ngModel)]=\"queryModel.condition.name\" />\n              </div>\n\n            </div>\n            <div class=\"ui-g-3 ui-sm-12\">\n              <div class=\"ui-g-4 ui-sm-12 option\">\n                <button pButton type=\"button\" label=\"查询\"  (click)=\"suggestInsepectiones()\" ></button>\n              </div>\n              <div class=\"ui-g-4 ui-sm-12 option\">\n                <button pButton label=\"清空\"  (click)=\"clearSearch()\" ></button>\n              </div>\n            </div>\n          </div>\n        </div>\n        </form>\n        <p-dataTable [value]=\"MaterialData\"\n                     [totalRecords]=\"totalRecords\"  [(selection)]=\"selectMaterial\">\n          <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n          <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n          <ng-template pTemplate=\"emptymessage\">\n            当前没有数据\n          </ng-template>\n        </p-dataTable>\n        <p-paginator rows=\"10\" [totalRecords]=\"totalRecords\" [rowsPerPageOptions]=\"[10,20,30]\" (onPageChange)=\"paginate($event)\"></p-paginator>\n      </div>\n    </form>\n  </div>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\"  label=\"确定\" (click)=\"formSubmit(false)\" [disabled]=\"selectMaterial.length===0\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeBorrowItems(false)\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-items/borrow-items.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".borrowitems {\n  height: 400px;\n  overflow: auto;\n  overflow-x: hidden; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-items/borrow-items.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BorrowItemsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BorrowItemsComponent = (function () {
    function BorrowItemsComponent(inventoryService) {
        this.inventoryService = inventoryService;
        this.display = false;
        this.closeBorrowItemsMask = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"];
        this.addDev = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //新增
        this.chooseCids = [];
        this.selectMaterial = [];
        this.MaterialStatusData = [];
    }
    BorrowItemsComponent.prototype.ngOnInit = function () {
        this.windowSize = window.innerWidth;
        if (this.windowSize < 1024) {
            this.width = this.windowSize * 0.7;
        }
        else {
            this.width = this.windowSize * 0.7;
        }
        this.display = true;
        this.cols = [
            { field: 'sid', header: '物品编号' },
            { field: 'name', header: '名称' },
            { field: 'catagory', header: '分类' },
            { field: 'status', header: '状态' },
            { field: 'quantity', header: '数量' },
            { field: 'brand', header: '品牌' },
            { field: 'model', header: '型号' },
            { field: 'location', header: '位置' },
        ];
        this.queryModel = {
            "condition": {
                "sid": "",
                "name": "",
                "catagory": "",
                "count": "",
                "status": ""
            },
            "page": {
                "page_size": "10",
                "page_number": "1"
            }
        };
        this.queryMaterialDate();
        this.queryMaterialStatusDate();
    };
    BorrowItemsComponent.prototype.clearSearch = function () {
        this.queryModel.condition.name = '';
        this.queryModel.condition.sid = '';
    };
    BorrowItemsComponent.prototype.queryMaterialStatusDate = function () {
        var _this = this;
        this.inventoryService.queryReceiveStatus().subscribe(function (data) {
            if (!data) {
                _this.MaterialStatusData = [];
            }
            else {
                _this.MaterialStatusData = data;
                _this.MaterialStatusData = _this.inventoryService.formatDropdownData(_this.MaterialStatusData);
                console.log(_this.MaterialStatusData);
            }
        });
    };
    //搜索功能
    BorrowItemsComponent.prototype.suggestInsepectiones = function () {
        this.queryModel.page.page_number = '1';
        this.queryModel.page.page_size = '10';
        this.queryMaterialDate();
    };
    //分页查询
    BorrowItemsComponent.prototype.paginate = function (event) {
        this.chooseCids = [];
        this.queryModel.page.page_number = String(++event.page);
        this.queryModel.page.page_size = String(event.rows);
        this.queryMaterialDate();
    };
    BorrowItemsComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryInventory(this.queryModel).subscribe(function (data) {
            if (!_this.MaterialData) {
                _this.MaterialData = [];
                _this.selectMaterial = [];
            }
            _this.MaterialData = data.items;
            _this.totalRecords = data.page.total;
        });
    };
    BorrowItemsComponent.prototype.formSubmit = function (bool) {
        this.addDev.emit(this.selectMaterial);
    };
    BorrowItemsComponent.prototype.closeBorrowItems = function (bool) {
        this.closeBorrowItemsMask.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], BorrowItemsComponent.prototype, "closeBorrowItemsMask", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BorrowItemsComponent.prototype, "state", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], BorrowItemsComponent.prototype, "addDev", void 0);
    BorrowItemsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-borrow-items',
            template: __webpack_require__("../../../../../src/app/inventory/borrow-items/borrow-items.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/borrow-items/borrow-items.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */]])
    ], BorrowItemsComponent);
    return BorrowItemsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/borrow-list/borrow-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-list/borrow-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes\">\n\n  <label>借用列表</label>\n  <p-dataTable [value]=\"inspections\" (onLazyLoad)=\"loadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" [editable]=\"true\"  >\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n    <ng-template pTemplate=\"emptymessage\" >\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-list/borrow-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BorrowListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BorrowListComponent = (function () {
    function BorrowListComponent() {
        this.dataSource = [];
        this.inspections = [];
    }
    BorrowListComponent.prototype.ngOnInit = function () {
        this.cols = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'quantity', header: '数量', editable: true },
        ];
    };
    BorrowListComponent.prototype.loadCarsLazy = function (event) {
        //in a real application, make a remote request to load data using state metadata from event
        //event.first = First row offset
        //event.rows = Number of rows per page
        //event.sortField = Field name to sort with
        //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
        var _this = this;
        //imitate db connection over a network
        setTimeout(function () {
            if (_this.dataSource) {
                _this.inspections = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 0);
    };
    BorrowListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-borrow-list',
            template: __webpack_require__("../../../../../src/app/inventory/borrow-list/borrow-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/borrow-list/borrow-list.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BorrowListComponent);
    return BorrowListComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/borrow-management/borrow-management.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n  <div>\n    <span class=\"feature-title\">库存管理<span class=\"gt\">&gt;</span>{{title}}  </span>\n  </div>\n</div>\n<div class=\"content-section implementation GridDemo\">\n  <div class=\"parent \">\n    <div style=\"position: absolute;top: 20px;left:0\">\n      <svg width=\"200\" height=\"450\"  >\n        <ng-container *ngFor=\"let flow of flowCharts;let i =index\">\n          <g class=\"rect-text\" (click)=\"openPage(flow,i)\" >\n            <rect [attr.width]=\"flow.width\" [ngStyle]=\"{'cursor':'pointer'}\" [attr.height]=\"flow.height\" [attr.x]=\"flow.x\" [attr.y]=\"flow.y\" [attr.stroke]=\"flow.stroke\" [attr.fill]=\"flow.fill\" [attr.status]=\"flow.status\"  [attr.rx]=\"flow.rx\" ></rect>\n            <text [attr.x]=\"flow.text.x\" [attr.y]=\"flow.text.y\">{{flow.label}}</text>\n          </g>\n            <g stroke=\"#39B9C6\" stroke-width=\"0.8\">\n            <line *ngFor=\"let g of flow.g\" [attr.x1]=\"g.x1\" [attr.y1]=\"g.y1\" [attr.x2]=\"g.x2\" [attr.y2]=\"g.y2\"></line>\n          </g>\n        </ng-container>\n      </svg>\n    </div>\n    <div class=\"node-right \">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-management/borrow-management.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".node-right {\n  margin-left: 135px;\n  min-height: 480px; }\n\n.parent {\n  position: relative; }\n\n.rect-text {\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-management/borrow-management.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BorrowManagementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BorrowManagementComponent = (function () {
    function BorrowManagementComponent(router, route, confirmationService, inventoryService, eventBus) {
        this.router = router;
        this.route = route;
        this.confirmationService = confirmationService;
        this.inventoryService = inventoryService;
        this.eventBus = eventBus;
        this.title = '借用总览';
    }
    BorrowManagementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.queryModel = {
            "status": "",
        };
        this.flowCharts = [
            { label: '借用申请', route: 'borrow', stroke: "#39B9C6", fill: '#357CA5', active: true, x: '0', y: '0', rx: '10', width: 100, height: 40, title: '借用申请', text: { x: '20', y: '24', label: '借用申请' }, g: [{ x1: '50', y1: '40', x2: '50', y2: '100' }, { x1: '50', y1: '100', x2: '43', y2: '90' }, { x1: '50', y1: '100', x2: '58', y2: '90' }] },
            { label: '借用审批', route: 'outbound', status: "待审批", stroke: "#39B9C6", fill: '#357CA5', active: false, x: '0', y: '100', width: 100, height: 40, title: '借用审批', text: { x: '20', y: '124', label: '借用审批' }, g: [{ x1: '50', y1: '140', x2: '50', y2: '200' }, { x1: '50', y1: '200', x2: '43', y2: '190' }, { x1: '50', y1: '200', x2: '58', y2: '190' }] },
            { label: '借用', route: 'outbound', status: "待借用", stroke: "#39B9C6", fill: '#357CA5', active: false, x: '0', y: '200', width: 100, height: 40, title: '借用', text: { x: '30', y: '224', label: '借用' }, g: [{ x1: '50', y1: '240', x2: '50', y2: '300' }, { x1: '50', y1: '300', x2: '43', y2: '290' }, { x1: '50', y1: '300', x2: '58', y2: '290' }] },
            { label: '续借', route: 'outbound', status: "待归还", stroke: "#39B9C6", fill: '#357CA5', active: false, x: '0', y: '300', width: 100, height: 40, title: '续借', text: { x: '30', y: '324', label: '续借' }, g: [{ x1: '50', y1: '340', x2: '50', y2: '400' }, { x1: '50', y1: '400', x2: '43', y2: '390' }, { x1: '50', y1: '400', x2: '58', y2: '390' }] },
            { label: '归还', route: 'outbound', status: "已归还", stroke: "#39B9C6", active: true, x: '0', y: '400', fill: '#357CA5', rx: '10', width: 100, height: 40, title: '', text: { x: '30', y: '424', label: '归还' } }
        ];
        this.route.queryParams.subscribe(function (params) {
            if (params['title']) {
                _this.title = params['title'];
            }
            if (params['sid']) {
                _this.inventoryService.borrowShow(params['sid']).subscribe(function (data) {
                    console.log(data);
                });
            }
        });
        this.eventBus.borrow.subscribe(function (data) {
            console.log(data);
            _this.queryModel.status = data;
            _this.queryMaterialDate();
        });
        this.queryMaterialDate();
    };
    BorrowManagementComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryMaterialBorrow(this.queryModel).subscribe(function (data) {
            if (!_this.MaterialCharData) {
                _this.MaterialCharData = [];
            }
            _this.MaterialCharData = data;
            for (var i = 0; i < _this.MaterialCharData.length; i++) {
                var temp = _this.MaterialCharData[i];
                _this.flowCharts[i]['fill'] = temp['fill'];
            }
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，请联系管理员或稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    BorrowManagementComponent.prototype.openPage = function (flow, index) {
        if (index + 1 === this.flowCharts.length)
            return;
        for (var i = 0; i < this.flowCharts.length; i++) {
            this.flowCharts[i].active = false;
        }
        this.flowCharts[index].active = true;
        this.queryMaterialDate();
        if (flow.status) {
            this.eventBus.updaterequestion.next(flow.status);
        }
        if (flow.route) {
            this.router.navigate([flow.route], { relativeTo: this.route });
            // this.MaterialCharData = [];
            this.title = flow.title;
        }
    };
    BorrowManagementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-borrow-management',
            template: __webpack_require__("../../../../../src/app/inventory/borrow-management/borrow-management.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/borrow-management/borrow-management.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_2__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], BorrowManagementComponent);
    return BorrowManagementComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/borrow-restart/borrow-restart.component.html":
/***/ (function(module, exports) {

module.exports = "\n<p-dialog [(visible)]=\"display\" modal=\"modal\"  [responsive]=\"true\" [width]=\"width\" (onHide)=\"closeRestartMask(false)\">\n  <p-header>\n    续借\n  </p-header>\n  <div class=\"ui-g restart\">\n    <div class=\"ui-g-12\">\n      <div class=\"file-box\">\n        <form class=\"form-horizontal\" [formGroup]=\"borrowRestartForm\" >\n          <div class=\"form-group\">\n            <label  class=\"col-sm-2 control-label\"><span>*</span>续借原因:</label>\n            <div class=\"col-sm-8 ui-fluid\">\n              <textarea formControlName=\"remarsk\" name=\"remarks\" pInputTextarea type=\"text\" [(ngModel)]=\"InventoryData.remarks\"></textarea>\n\n              <!--<input type=\"text\" name=\"reportType\" class=\"form-control\" [(ngModel)]=\"currentInspection.name\" *ngIf=\"state ==='update'\" formControlName=\"contentName\">-->\n              <!--<input type=\"text\" name=\"reportType\" class=\"form-control\" [(ngModel)]=\"submitAddInspection.name\" *ngIf=\"state ==='add'\" formControlName=\"contentName\">-->\n            </div>\n          </div>\n          <div class=\"form-group time\">\n            <label  class=\"col-sm-2  control-label\"><span>*</span>归还时间:</label>\n            <div class=\"col-sm-8 ui-fluid\">\n              <p-calendar  [showIcon]=\"true\" [locale]=\"zh\" name=\"return_time\"\n                           formControlName=\"return_time\"\n                           [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\" [required]=\"true\" dataType=\"string\"\n                           [(ngModel)]=\"InventoryData.return_time\" [minDate]=\"minDate\">\n              </p-calendar>\n              <!--<p-dropdown [options]=\"inspectionNames\" [(ngModel)]=\"currentInspection.inspection_cycle_cid\" [autoWidth]=\"false\" name=\"cycle\" *ngIf=\"state ==='update'\" formControlName=\"cycleName\"></p-dropdown>-->\n              <!--<p-dropdown [options]=\"inspectionNames\" [(ngModel)]=\"submitAddInspection.inspection_cycle_cid\" [autoWidth]=\"false\" name=\"cycle\" *ngIf=\"state ==='add'\" formControlName=\"cycleName\"></p-dropdown>-->\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" label=\"确定\" (click)=\"updateInventoryContent()\" [disabled]=\"!borrowRestartForm.valid\"  ></button>\n    <button type=\"button\" pButton icon=\"fa-close\"  label=\"取消\" (click)=\"closeRestartMask(false)\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-restart/borrow-restart.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".restart {\n  height: 200px; }\n\nlabel > span {\n  color: red; }\n\n.time {\n  margin-top: 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/borrow-restart/borrow-restart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BorrowRestartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BorrowRestartComponent = (function () {
    function BorrowRestartComponent(inventoryService, eventBusService, confirmationService, fb) {
        this.inventoryService = inventoryService;
        this.eventBusService = eventBusService;
        this.confirmationService = confirmationService;
        this.fb = fb;
        this.display = false;
        this.closeRestart = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"];
        this.updateDev = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    BorrowRestartComponent.prototype.ngOnInit = function () {
        this.borrowRestartForm = this.fb.group({
            'remarsk': ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            'return_time': ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
        });
        this.windowSize = window.innerWidth;
        if (this.windowSize < 1366) {
            this.width = this.windowSize * 0.4;
        }
        else {
            this.width = this.windowSize * 0.4;
        }
        this.display = true;
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            dayNamesShort: ["一", "二", "三", "四", "五", "六", "七"],
            dayNamesMin: ["一", "二", "三", "四", "五", "六", "七"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
        };
        this.InventoryData = {
            "sid": this.currentBorrow.sid,
            "type": "",
            "return_time": "",
            "urgency": "",
            "remarks": "",
            "inventories": []
        };
        this.dateAdd('');
        this.minDate = new Date(this.nextStartDate);
    };
    BorrowRestartComponent.prototype.updateInventoryContent = function () {
        var _this = this;
        this.inventoryService.restartqueryDelivery(this.InventoryData).subscribe(function (data) {
            _this.eventBusService.borrow.next(_this.currentBorrow.status);
            _this.updateDev.emit(false);
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，请联系管理员或稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    BorrowRestartComponent.prototype.dateAdd = function (startDate) {
        startDate = new Date(this.currentBorrow['return_time']);
        startDate = +startDate + 1000 * 60 * 60 * 24;
        startDate = new Date(startDate);
        this.nextStartDate = startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate();
        return this.nextStartDate;
    };
    BorrowRestartComponent.prototype.closeRestartMask = function (bool) {
        this.closeRestart.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], BorrowRestartComponent.prototype, "closeRestart", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], BorrowRestartComponent.prototype, "updateDev", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BorrowRestartComponent.prototype, "currentBorrow", void 0);
    BorrowRestartComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-borrow-restart',
            template: __webpack_require__("../../../../../src/app/inventory/borrow-restart/borrow-restart.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/borrow-restart/borrow-restart.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"]])
    ], BorrowRestartComponent);
    return BorrowRestartComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/borrow/borrow.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes borrow\" >\n\n  <form *ngIf=\"viewState ==='apply' \" [formGroup]=\"inventoryborrowForm\"  >\n    <p-panel >\n      <p-header>\n        <div class=\"ui-helper-clearfix\">\n          <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n          <button  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\" [ngStyle]=\"{'width':'100px'}\" ></button>\n          <button  pButton type=\"submit\" [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"保存\" (click)=\"formSave()\"   ></button>\n          <button  pButton type=\"submit\" [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"提交\" (click)=\"formSubmit()\" ></button>\n        </div>\n      </p-header>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\" >\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >借用单号:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"sid\" name=\"sid\"  type=\"text\" pInputText   placeholder=\"自动生成\" readonly  />\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label>申请类型:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <p-dropdown formControlName=\"type\"  [options]=\"statusNames\" [autoWidth]=\"false\" name=\"cycle\"  [(ngModel)]=\"submitData.type\"  readonly=\"true\"></p-dropdown>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >借用人:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"people\"  type=\"text\" pInputText  name=\"allowed\"\n                   readonly class=\"cursor_not_allowed\"   value=\"{{personalData.name}}\" />\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >部门名称:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input  formControlName=\"inspectiondepartment\" type=\"text\" pInputText  name=\"inspectiondepartment\" placeholder=\"自动生成\"\n                    readonly class=\"cursor_not_allowed\"  [(ngModel)]=\"submitData.org\"/>\n          </div>\n\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\" formGroupName=\"timeGroup\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label ><span>*</span>借用时间:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <p-calendar\n              formControlName=\"startTime\"\n              [showIcon]=\"true\"\n              [locale]=\"zh\"\n              [styleClass]=\"'schedule-add'\"\n              dateFormat=\"yy-mm-dd\"\n              [required]=\"true\"\n              dataType=\"string\"\n              [minDate]=\"nowDate\"\n\n              [(ngModel)]=\"submitData.create_time\" >\n            </p-calendar>\n            <div [hidden]=\"inventoryborrowForm.get(['timeGroup','startTime']).valid||inventoryborrowForm.get(['timeGroup','startTime']).untouched\">\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  [hidden]=\"!inventoryborrowForm.hasError('required',['timeGroup','startTime'])\">\n                <i class=\"fa fa-close\"></i>\n                借用时间必填\n              </div>\n            </div>\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label ><span>*</span>归还时间:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <p-calendar\n              formControlName=\"endTime\"\n              [showIcon]=\"true\" [locale]=\"zh\" name=\"return_time\"\n              [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\" [required]=\"true\" dataType=\"string\"\n              [minDate]=\"nowDate\" [(ngModel)]=\"submitData.return_time\"\n            >\n            </p-calendar>\n            <div [hidden]=\"inventoryborrowForm.get(['timeGroup','endTime']).valid||inventoryborrowForm.get(['timeGroup','endTime']).untouched\">\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  [hidden]=\"!inventoryborrowForm.hasError('required',['timeGroup','endTime'])\" >\n                <i class=\"fa fa-close\"></i>\n                归还时间必填\n              </div>\n            </div>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"inventoryborrowForm.hasError('time','timeGroup')\" >\n              <i class=\"fa fa-close\"></i>\n              {{inventoryborrowForm.getError('time','timeGroup')?.descxxx}}\n            </div>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label > <span>*</span>借用原因:</label>\n          </div>\n          <div class=\"ui-grid-col-11\">\n            <textarea formControlName=\"remarks\"  name=\"remarks\" pInputTextarea type=\"text\" [(ngModel)]=\"submitData.remarks\" ></textarea>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!inventoryborrowForm.controls['remarks'].valid&&(!inventoryborrowForm.controls['remarks'].untouched)\" >\n              <i class=\"fa fa-close\"></i>\n              借用原因必填\n            </div>\n          </div>\n        </div>\n      </div>\n    </p-panel>\n    <label></label>\n    <div class=\"ui-grid-row margin-bottom-1vw\" >\n      <div class=\"ui-grid-col-2 ui-g-offset-10 text-right\">\n        <button pButton type=\"button\" label=\"移除\" (click)=\"deleteStorage()\" [ngStyle]=\"{'width':'100px'}\" [disabled]=\"selectInsepections.length===0 || selectInsepections.length >1\" ></button>\n        <button pButton  type=\"button\" (click)=\"showBorrowItemsMask()\" [ngStyle]=\"{'width':'100px'}\" label=\"物品选择\" ></button>\n      </div>\n    </div>\n  </form>\n  <form  *ngIf=\"viewState === 'update' \"  >\n    <p-panel >\n      <p-header>\n        <div class=\"ui-helper-clearfix\">\n          <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n          <button  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\" [ngStyle]=\"{'width':'100px'}\" ></button>\n          <button  pButton type=\"submit\" [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"保存\" (click)=\"formSave()\" label=\"保存\"   ></button>\n          <button  pButton type=\"submit\" [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"提交\" (click)=\"formSubmit()\" ></button>\n        </div>\n      </p-header>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\" >\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >借用单号:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input  name=\"sid\"  type=\"text\" pInputText    readonly  value=\"{{borrowData?.sid}}\" />\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >申请类型:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <p-dropdown  [options]=\"statusNames\" [autoWidth]=\"false\" name=\"cycle\"  [(ngModel)]=\"submitData.type\"  readonly=\"true\" ></p-dropdown>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >借用人:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input  type=\"text\" pInputText  name=\"allowed\"\n                    readonly class=\"cursor_not_allowed\" value=\"{{personalData.name}}\"/>\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >部门名称:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input type=\"text\" pInputText  name=\"inspectiondepartment\"\n                   readonly class=\"cursor_not_allowed\"  value=\"{{borrowData.org}}\"/>\n          </div>\n\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label ><span>*</span>借用时间:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <p-calendar\n              [showIcon]=\"true\"\n              [minDate]=\"nowDate\"\n              [locale]=\"zh\"\n              name=\"create_time\"\n              [styleClass]=\"'schedule-add'\"\n              dateFormat=\"yy-mm-dd\"\n              [required]=\"true\"\n              dataType=\"string\"\n              [minDate]=\"minDate\"\n              (onSelect) = \"startTimeSelected(beginTime)\"\n              [(ngModel)]=\"borrowData.create_time\" >\n            </p-calendar>\n            <div [hidden]=\"inventoryborrowForm.get(['timeGroup','startTime']).valid||inventoryborrowForm.get(['timeGroup','startTime']).untouched\">\n              <div class=\"ui-message ui-messages-error ui-corner-all\"  [hidden]=\"!inventoryborrowForm.hasError('required',['timeGroup','startTime'])\">\n                <i class=\"fa fa-close\"></i>\n                借用时间必填\n              </div>\n            </div>\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label ><span>*</span>归还时间:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n\n            <p-calendar   [showIcon]=\"true\" [locale]=\"zh\" name=\"return_time\"\n                          [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\" [required]=\"true\" dataType=\"string\"\n                          [minDate]=\"minDate\" [(ngModel)]=\"borrowData.return_time\"\n            >\n            </p-calendar>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label > <span>*</span>借用原因:</label>\n          </div>\n          <div class=\"ui-grid-col-11\">\n            <textarea  name=\"remarks\" pInputTextarea type=\"text\" [(ngModel)]=\"borrowData.remarks\"></textarea>\n          </div>\n        </div>\n      </div>\n    </p-panel>\n    <label></label>\n    <div class=\"ui-grid-row margin-bottom-1vw\" *ngIf=\"viewState!=='someReturn'\">\n      <div class=\"ui-grid-col-2 ui-g-offset-10 text-right\">\n        <button pButton type=\"button\" label=\"移除\" (click)=\"updatedeleteStorage()\" [ngStyle]=\"{'width':'100px'}\" [disabled]=\"selectInsepections.length===0 || selectInsepections.length >1\" *ngIf=\"viewState !== 'viewDetail'\"></button>\n        <button pButton   (click)=\"showBorrowItemsMask()\" [ngStyle]=\"{'width':'100px'}\" label=\"物品选择\" *ngIf=\"viewState !== 'viewDetail'\"></button>\n      </div>\n    </div>\n  </form>\n  <form *ngIf=\"viewState === 'viewDetail' \" >\n    <p-panel >\n      <p-header>\n        <div class=\"ui-helper-clearfix\">\n          <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n          <button  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\" [ngStyle]=\"{'width':'100px'}\" ></button>\n\n        </div>\n      </p-header>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\" >\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label >借用单号:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <input  name=\"sid\"  type=\"text\" pInputText    readonly  value=\"{{borrowData?.sid}}\"  />\n          </div>\n          <div class=\"ui-grid-col-2 text-right\">\n            <label >申请类型:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <p-dropdown  [options]=\"statusNames\" [autoWidth]=\"false\" name=\"cycle\"  [(ngModel)]=\"submitData.type\"  readonly=\"true\" ></p-dropdown>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label >借用人:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <input  type=\"text\" pInputText  name=\"allowed\"\n                    readonly class=\"cursor_not_allowed\" value=\"{{personalData.name}}\"  />\n          </div>\n          <div class=\"ui-grid-col-2 text-right\">\n            <label >部门名称:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <input  type=\"text\" pInputText  name=\"inspectiondepartment\"\n                    readonly class=\"cursor_not_allowed\" [(ngModel)]=\"submitData.org\" />\n          </div>\n\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label ><span>*</span>借用时间:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <input   type=\"text\" pInputText  name=\"create_times\"\n                     readonly class=\"cursor_not_allowed\"  [(ngModel)]=\"borrowData.create_time\"/>\n          </div>\n          <div class=\"ui-grid-col-2 text-right\">\n            <label ><span>*</span>归还时间:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n\n            <input  type=\"text\" pInputText  name=\"return_time\"\n                    readonly class=\"cursor_not_allowed\"  [(ngModel)]=\"borrowData.return_time\"/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label > <span>*</span>借用原因:</label>\n          </div>\n          <div class=\"ui-grid-col-8\">\n            <textarea  name=\"remarks\" pInputTextarea type=\"text\" [(ngModel)]=\"borrowData.remarks\" readonly></textarea>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\" *ngIf=\"borrowData.status !=='新建' && borrowData.status !=='待审批'\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label > <span>*</span>审批意见:</label>\n          </div>\n          <div class=\"ui-grid-col-8\">\n            <textarea  name=\"approval_opinions\" pInputTextarea type=\"text\" [(ngModel)]=\"borrowData.approve_remarks\" readonly></textarea>\n          </div>\n        </div>\n      </div>\n    </p-panel>\n\n  </form>\n  <form *ngIf=\"viewState === 'outbound' \" >\n    <p-panel >\n      <p-header>\n        <div class=\"ui-helper-clearfix\">\n          <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n          <button  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\" [ngStyle]=\"{'width':'100px'}\" ></button>\n\n        </div>\n      </p-header>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\" >\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label >借用单号:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <input  name=\"sid\"  type=\"text\" pInputText    readonly  value=\"{{borrowData?.sid}}\"  />\n          </div>\n          <div class=\"ui-grid-col-2 text-right\">\n            <label >申请类型:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <p-dropdown  [options]=\"statusNames\" [autoWidth]=\"false\" name=\"cycle\"  [(ngModel)]=\"submitData.type\"  readonly=\"true\" ></p-dropdown>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label >借用人:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <input  type=\"text\" pInputText  name=\"allowed\"\n                    readonly class=\"cursor_not_allowed\" value=\"{{personalData.name}}\"  />\n          </div>\n          <div class=\"ui-grid-col-2 text-right\">\n            <label >部门名称:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <input  type=\"text\" pInputText  name=\"inspectiondepartment\"\n                    readonly class=\"cursor_not_allowed\" [(ngModel)]=\"submitData.org\" />\n          </div>\n\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label ><span>*</span>借用时间:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n            <input   type=\"text\" pInputText  name=\"create_times\"\n                     readonly class=\"cursor_not_allowed\"  [(ngModel)]=\"borrowData.create_time\"/>\n          </div>\n          <div class=\"ui-grid-col-2 text-right\">\n            <label ><span>*</span>归还时间:</label>\n          </div>\n          <div class=\"ui-grid-col-3\">\n\n            <input  type=\"text\" pInputText  name=\"return_time\"\n                    readonly class=\"cursor_not_allowed\"  [(ngModel)]=\"borrowData.return_time\"/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label > <span>*</span>借用原因:</label>\n          </div>\n          <div class=\"ui-grid-col-8\">\n            <textarea  name=\"remarks\" pInputTextarea type=\"text\" [(ngModel)]=\"borrowData.remarks\" readonly></textarea>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\" *ngIf=\"borrowData.status !=='新建' && borrowData.status !=='待审批'\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <label > <span>*</span>审批意见:</label>\n          </div>\n          <div class=\"ui-grid-col-8\">\n            <textarea  name=\"approval_opinions\" pInputTextarea type=\"text\" [(ngModel)]=\"borrowData.approve_remarks\" readonly></textarea>\n          </div>\n        </div>\n      </div>\n    </p-panel>\n    <label></label>\n    <div class=\"ui-grid-row margin-bottom-1vw\" >\n      <div class=\"ui-grid-col-2 ui-g-offset-10 text-right\">\n        <button pButton  type=\"button\" (click)=\"sureApproved()\" [ngStyle]=\"{'width':'100px'}\" label=\"出库\"  [disabled]=\"selectInsepections.length === 0\"></button>\n      </div>\n    </div>\n  </form>\n  <label></label>\n  <label><span>*</span>借用列表</label>\n  <p-dataTable [value]=\"inspections\" (onLazyLoad)=\"loadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" [editable]=\"true\"  *ngIf=\"viewState ==='apply'\" [rowsPerPageOptions]=\"[5,10,20]\" class=\"borrowapplication\">\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n    <ng-template pTemplate=\"emptymessage\" >\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" [editable]=\"true\" *ngIf=\"viewState === 'update'\"class=\"update\">\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of update\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\"  *ngIf=\"viewState === 'viewDetail'\">\n    <!--<p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>-->\n    <p-column  *ngFor=\"let col of coles\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\"  *ngIf=\"viewState === 'someReturn'\"  [editable]=\"true\" class=\"someReturn\">\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of somecols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" [editable]=\"true\" *ngIf=\"viewState === 'outbound'\"  class=\"outbound\" >\n\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of updatecols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <label></label>\n  <div class=\"ui-grid-row margin-bottom-1vw\" >\n    <div class=\"ui-grid-col-2 ui-g-offset-10 text-right\">\n      <button pButton  type=\"submit\" label=\"归还\" *ngIf=\" viewState == 'someReturn'\" [disabled]=\"selectInsepections.length===0\" (click)=\"returnBorrow()\"></button>\n    </div>\n  </div>\n</div>\n\n<app-borrow-items\n  *ngIf=\"showBorrowItems\"\n  (closeBorrowItemsMask)=\"closeBorrowItemsMask($event)\"\n  (addDev)=\"addDev($event)\"\n></app-borrow-items>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/borrow/borrow.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (min-width: 1024px) {\n  .institution /deep/ table thead tr th:last-child {\n    width: 25%; } }\n\n@media screen and (min-width: 1024px) {\n  .outbound /deep/ table tbody tr td:nth-child(9) {\n    width: 25%;\n    color: #00c0ef;\n    cursor: pointer; } }\n\n@media screen and (min-width: 1024px) {\n  .borrowapplication /deep/ table tbody tr td:nth-child(8) {\n    color: #00c0ef;\n    cursor: pointer; } }\n\n@media screen and (min-width: 1024px) {\n  .someReturn /deep/ table tbody tr td:last-child {\n    color: #00c0ef;\n    cursor: pointer; } }\n\n@media screen and (min-width: 1024px) {\n  .update /deep/ table tbody tr td:last-child {\n    color: #00c0ef;\n    cursor: pointer; } }\n\n/deep/.ui-widget-header {\n  background-color: white;\n  background: linear-gradient(to bottom, #ffffff 0%, #ffffff 100%); }\n\nlabel > span {\n  color: red; }\n\n.borrow /deep/ .ui-messages-error {\n  color: red;\n  background-color: white;\n  border-color: white; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/borrow/borrow.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BorrowComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__validator_validators__ = __webpack_require__("../../../../../src/app/validator/validators.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var BorrowComponent = (function (_super) {
    __extends(BorrowComponent, _super);
    function BorrowComponent(inventoryService, confirmationService, messageService, storageService, router, route, fb, eventBusService) {
        var _this = _super.call(this, confirmationService, messageService) || this;
        _this.inventoryService = inventoryService;
        _this.confirmationService = confirmationService;
        _this.messageService = messageService;
        _this.storageService = storageService;
        _this.router = router;
        _this.route = route;
        _this.fb = fb;
        _this.eventBusService = eventBusService;
        _this.showBorrowItems = false; //借用物品弹框
        _this.statusNames = [];
        _this.dataSource = [];
        _this.editDataSource = [];
        _this.editInspections = [];
        _this.selectInsepections = [];
        _this.inspections = [];
        _this.borrowData = [];
        _this.button = false;
        _this.personalData = [];
        _this.inventoryborrowForm = _this.fb.group({
            'sid': [''],
            'type': [''],
            'people': [''],
            'inspectiondepartment': [''],
            'inspectiondepartmentname': [''],
            timeGroup: _this.fb.group({
                startTime: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required],
                endTime: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required],
            }, { validator: __WEBPACK_IMPORTED_MODULE_6__validator_validators__["i" /* timeValidator */] }),
            'remarks': ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required],
        });
        return _this;
    }
    BorrowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nowDate = new Date();
        this.button = true;
        this.viewState = 'apply';
        this.beginTime = new Date();
        this.cols = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'quantity', header: '数量', editable: true },
        ];
        this.update = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'balance', header: '数量', editable: true },
        ];
        this.coles = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'application_quantity', header: '申请数量', editable: false },
            { field: 'quantity', header: '实际借用数量', editable: false },
            { field: 'balance', header: '剩余归还数', editable: false },
        ];
        this.somecols = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'balance', header: '待归还数', editable: false },
            { field: 'return_quantity', header: '本次归还', editable: true },
        ];
        this.updatecols = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'application_quantity', header: '申请数量', editable: false },
            { field: 'quantity', header: '实际出库数量', editable: true },
            { field: 'onhand_quantity', header: '当前库存', editable: false },
        ];
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            dayNamesShort: ["一", "二", "三", "四", "五", "六", "七"],
            dayNamesMin: ["一", "二", "三", "四", "五", "六", "七"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
        };
        this.statusNames = [
            { label: '借用', value: '借用' },
        ];
        this.submitData = {
            "sid": "",
            "type": this.statusNames[0]['value'],
            "return_time": "",
            "create_time": "",
            "urgency": "",
            "remarks": "",
            "org": "",
            "inventories": []
        };
        this.submitData.create_people = this.storageService.getUserName('userName');
        this.querylogin();
        this.route.queryParams.subscribe(function (params) {
            _this.viewState = 'apply';
            if (params['sid'] && params['state'] && params['title']) {
                _this.viewState = params['state'];
                _this.inventoryService.outboundShow(params['sid']).subscribe(function (data) {
                    _this.borrowData = data;
                    // console.log(this.borrowData);
                    _this.editDataSource = _this.borrowData['inventories'] ? _this.borrowData['inventories'] : [];
                    if (!_this.editDataSource) {
                        _this.editDataSource = [];
                        _this.editInspections = [];
                        _this.totalRecords = 0;
                    }
                    _this.totalRecords = _this.editDataSource.length;
                    _this.editInspections = _this.editDataSource.slice(0, 10);
                });
            }
        });
    };
    BorrowComponent.prototype.goBack = function () {
        var sid = this.borrowData['sid'];
        // this.currentMaterial['status'] = "";
        // this.eventBusService.borrow.next(this.currentMaterial['status']);
        this.borrowData['status'] = "";
        this.eventBusService.borrow.next(this.borrowData['status']);
        this.router.navigate(['../outbound'], { queryParams: { sid: sid, state: 'viewDetail', title: '借用总览' }, relativeTo: this.route });
    };
    //查询登录人
    BorrowComponent.prototype.querylogin = function () {
        var _this = this;
        this.inventoryService.queryPersonal().subscribe(function (data) {
            _this.personalData = data;
            _this.submitData['org'] = _this.personalData['organization'];
        });
    };
    // 提交
    BorrowComponent.prototype.addStorageSubmint = function (submitData, dataSource) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < dataSource.length; i++) {
            var temp = dataSource[i];
            var obj = {};
            obj['pn'] = temp['pn'];
            obj['sid'] = temp['sid'];
            obj['quantity'] = temp['quantity'];
            obj['location'] = temp['location'];
            obj['location_id'] = temp['location_id'];
            obj['keeper'] = temp['keeper'];
            obj['keeprer_pid'] = temp['keeprer_pid'];
            inventories.push(obj);
        }
        submitData.inventories = inventories;
        this.inventoryService.addDelivery(submitData).subscribe(function () {
            _this.router.navigate(['../outbound'], { queryParams: { title: '借用总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    //保存
    BorrowComponent.prototype.addDeviceSave = function (submitData, dataSource) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < dataSource.length; i++) {
            var temp = dataSource[i];
            var obj = {};
            obj['pn'] = temp['pn'];
            obj['sid'] = temp['sid'];
            obj['location'] = temp['location'];
            obj['location_id'] = temp['location_id'];
            obj['keeper'] = temp['keeper'];
            obj['keeprer_pid'] = temp['keeprer_pid'];
            obj['quantity'] = temp['quantity'];
            inventories.push(obj);
        }
        submitData.inventories = inventories;
        this.inventoryService.queryDeliverySave(submitData).subscribe(function (data) {
            _this.router.navigate(['../outbound'], { queryParams: { title: '借用总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    //修改保存方法
    BorrowComponent.prototype.updateDeviceSave = function (submitData, dataSource) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < dataSource.length; i++) {
            var temp = dataSource[i];
            var obj = {};
            obj['pn'] = temp['pn'];
            obj['sid'] = temp['sid'];
            obj['location'] = temp['location'];
            obj['location_id'] = temp['location_id'];
            obj['keeper'] = temp['keeper'];
            obj['keeprer_pid'] = temp['keeprer_pid'];
            obj['quantity'] = temp['quantity'];
            obj['delivery_sid'] = temp['delivery_sid'];
            obj['borrower'] = temp['borrower'];
            obj['borrower_pid'] = temp['borrower_pid'];
            obj['return_quantity'] = temp['return_quantity'];
            obj['balance'] = temp['balance'];
            obj['name'] = temp['name'];
            obj['catagory'] = temp['catagory'];
            obj['supplier'] = temp['supplier'];
            obj['brand'] = temp['brand'];
            obj['model'] = temp['model'];
            obj['specification'] = temp['specification'];
            inventories.push(obj);
        }
        submitData.inventories = inventories;
        this.inventoryService.queryDeliveryMod(submitData).subscribe(function (data) {
            _this.router.navigate(['../outbound'], { queryParams: { title: '借用总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    BorrowComponent.prototype.deleteStorage = function () {
        for (var i = 0; i < this.selectInsepections.length; i++) {
            for (var j = 0; j < this.dataSource.length; j++) {
                if (this.selectInsepections[i]['sid'] === this.dataSource[j]['sid']) {
                    this.dataSource.splice(j, 1);
                    this.totalRecords = this.dataSource.length;
                    this.inspections = this.dataSource.slice(0, 10);
                    this.selectInsepections = [];
                }
            }
        }
    };
    BorrowComponent.prototype.updatedeleteStorage = function () {
        for (var i = 0; i < this.selectInsepections.length; i++) {
            for (var j = 0; j < this.editDataSource.length; j++) {
                if (this.selectInsepections[i]['sid'] === this.editDataSource[j]['sid']) {
                    this.editDataSource.splice(j, 1);
                    this.totalRecords = this.editDataSource.length;
                    this.editInspections = this.editDataSource.slice(0, 10);
                    this.selectInsepections = [];
                }
            }
        }
    };
    BorrowComponent.prototype.loadCarsLazy = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.dataSource) {
                _this.inspections = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 0);
    };
    BorrowComponent.prototype.addDev = function (metail) {
        this.showBorrowItems = false;
        // let hashTable = {};
        // let newArr = [];
        // for(let i=0,l=metail.length;i<l;i++) {
        //   if(!hashTable[metail[i]]) {
        //     hashTable[metail[i]] = true;
        //     newArr.push(metail[i]);
        //   }
        //   return newArr;
        // }
        for (var i = 0; i < metail.length; i++) {
            this.dataSource.push(metail[i]);
            this.editDataSource.push(metail[i]);
        }
        this.totalRecords = this.dataSource.length;
        this.totalRecords = this.editDataSource.length;
        this.inspections = this.dataSource.slice(0, 10);
        console.log(this.inspections);
        this.editInspections = this.editDataSource.slice(0, 10);
    };
    BorrowComponent.prototype.formSubmit = function () {
        if (this.viewState === 'apply') {
            this.addStorageSubmint(this.submitData, this.dataSource);
        }
        else if (this.viewState === 'update') {
            this.addStorageSubmint(this.borrowData, this.editDataSource);
        }
    };
    BorrowComponent.prototype.formSave = function () {
        if (this.viewState === 'apply') {
            this.addDeviceSave(this.submitData, this.dataSource);
        }
        else if (this.viewState === 'update') {
            this.updateDeviceSave(this.borrowData, this.editDataSource);
        }
    };
    BorrowComponent.prototype.startTimeSelected = function (t) {
        this.endTime = t;
    };
    BorrowComponent.prototype.showBorrowItemsMask = function (state) {
        this.state = 'apply';
        this.showBorrowItems = !this.showBorrowItems;
    };
    BorrowComponent.prototype.closeBorrowItemsMask = function (bool) {
        this.showBorrowItems = bool;
    };
    BorrowComponent.prototype.returnBorrow = function () {
        var _this = this;
        var obj = {};
        obj['sid'] = this.borrowData['sid'];
        obj['inventories'] = this.selectInsepections;
        console.log(this.selectInsepections);
        this.confirmationService.confirm({
            message: '是否归还?',
            rejectVisible: true,
            accept: function () {
                _this.inventoryService.someDeliveryReturn(obj).subscribe(function (data) {
                    _this.selectInsepections = [];
                    _this.router.navigate(['../outbound'], { queryParams: { title: '借用总览' }, relativeTo: _this.route });
                }, function (err) {
                    _this.toastError(err);
                });
            },
            reject: function () { }
        });
    };
    BorrowComponent.prototype.editloadCarsLazy = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.editDataSource) {
                _this.editInspections = _this.editDataSource.slice(event.first, (event.first + event.rows));
            }
        }, 0);
    };
    //确认出库
    BorrowComponent.prototype.sureApproved = function () {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < this.selectInsepections.length; i++) {
            var temp = this.selectInsepections[i];
            inventories.push(temp);
        }
        var queryModel = {
            "sid": this.borrowData['sid'],
            "inventories": inventories
        };
        this.eventBusService.borrow.next(this.borrowData['status']);
        this.confirmationService.confirm({
            message: '确定出库吗?',
            rejectVisible: true,
            accept: function () {
                _this.inventoryService.queryBorrowSure(queryModel).subscribe(function () {
                    _this.borrowData['status'] = "";
                    _this.eventBusService.borrow.next(_this.borrowData['status']);
                    _this.router.navigate(['../outbound'], { relativeTo: _this.route });
                }, function (err) {
                    _this.confirmationService.confirm({
                        message: err['message'],
                        rejectVisible: false,
                    });
                });
            },
            reject: function () { }
        });
    };
    BorrowComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-borrow',
            template: __webpack_require__("../../../../../src/app/inventory/borrow/borrow.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/borrow/borrow.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__["MessageService"],
            __WEBPACK_IMPORTED_MODULE_4__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_9__services_event_bus_service__["a" /* EventBusService */]])
    ], BorrowComponent);
    return BorrowComponent;
}(__WEBPACK_IMPORTED_MODULE_7__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/inventory/choose-material-number/choose-material-number.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"选择物品编号\" [(visible)]=\"display\" modal=\"modal\" width=\"1000\" [responsive]=\"true\" (onHide)=\"closeChooseMaterialNumberMask(false)\">\n  <div class=\"content-section  GridDemo clearfixes\">\n    <form class=\"content\">\n        <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n          <div class=\"mysearch\">\n            <div class=\"ui-g\">\n              <div class=\"ui-g-8 ui-sm-12\">\n                <label class=\"ui-g-2 ui-sm-5  text-right\">物品编号：</label>\n                <div class=\"ui-g-4 ui-sm-7 ui-fluid  ui-no-padding-right-15px \">\n                  <input  name=\"inspectionName\"  type=\"text\" pInputText   placeholder=\"物品编号\" [(ngModel)]=\"queryModel.condition.pn\" >\n\n                </div>\n                <label class=\"ui-g-2 ui-sm-5 text-right\">物品名称：</label>\n                <div class=\"ui-g-4  ui-sm-7 ui-fluid\">\n                  <input  name=\"inspectionName\"  type=\"text\" pInputText   placeholder=\"物品名称\" [(ngModel)]=\"queryModel.condition.name\">\n\n                </div>\n\n              </div>\n              <div class=\"ui-g-3 ui-sm-12\">\n                <div class=\"ui-g-4 ui-sm-12 option\">\n                  <button pButton type=\"button\" label=\"查询\"  (click)=\"suggestInsepectiones()\" ></button>\n                </div>\n                <div class=\"ui-g-4 ui-sm-12 option\">\n                  <button pButton label=\"清空\"  (click)=\"clearSearch()\" ></button>\n                </div>\n              </div>\n            </div>\n          </div>\n\n        </div>\n      <p-dataTable [value]=\"MaterialData\"\n                   [totalRecords]=\"totalRecords\"  [(selection)]=\"selectMaterial\">\n        <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n        <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n        <ng-template pTemplate=\"emptymessage\">\n          当前没有数据\n        </ng-template>\n\n      </p-dataTable>\n      <p-paginator rows=\"10\" [totalRecords]=\"totalRecords\" [rowsPerPageOptions]=\"[10,20,30]\" (onPageChange)=\"paginate($event)\"></p-paginator>\n    </form>\n  </div>\n  <p-footer>\n    <!--[disabled]=\"selectMaterial.length===0||selectMaterial.length>1\"-->\n    <button type=\"button\" pButton icon=\"fa-check\"  label=\"确定\" (click)=\"formSubmit(false)\" [disabled]=\"selectMaterial.length===0||selectMaterial.length>1\" ></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeChooseMaterialNumberMask(false)\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/choose-material-number/choose-material-number.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".content {\n  height: 300px;\n  overflow: auto;\n  overflow-x: hidden; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/choose-material-number/choose-material-number.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChooseMaterialNumberComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChooseMaterialNumberComponent = (function () {
    function ChooseMaterialNumberComponent(inventoryService, confirmationService) {
        this.inventoryService = inventoryService;
        this.confirmationService = confirmationService;
        this.display = false;
        this.closeChooseMaterialNumber = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"];
        this.addDev = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //新增
        this.chooseCids = [];
        this.selectMaterial = [];
    }
    ChooseMaterialNumberComponent.prototype.ngOnInit = function () {
        this.display = true;
        this.queryModel = {
            "condition": {
                "pn": "",
                "name": "",
                "status": "启用",
            },
            "page": {
                "page_size": "10",
                "page_number": "1"
            }
        };
        this.cols = [
            { field: 'pn', header: '物品编号' },
            { field: 'name', header: '物品名称' },
            { field: 'catagory', header: '物品分类' },
            { field: 'brand', header: '品牌' },
            { field: 'model', header: '型号' },
            { field: 'specification', header: '规格' },
        ];
        this.queryMaterialDate();
    };
    ChooseMaterialNumberComponent.prototype.clearSearch = function () {
        this.queryModel.condition.pn = '';
        this.queryModel.condition.name = '';
        this.queryModel.condition.status = '';
    };
    //搜索功能
    ChooseMaterialNumberComponent.prototype.suggestInsepectiones = function () {
        this.queryModel.page.page_number = '1';
        this.queryModel.page.page_size = '10';
        this.queryMaterialDate();
    };
    //分页查询
    ChooseMaterialNumberComponent.prototype.paginate = function (event) {
        this.chooseCids = [];
        this.queryModel.page.page_number = String(++event.page);
        this.queryModel.page.page_size = String(event.rows);
        this.queryMaterialDate();
    };
    ChooseMaterialNumberComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryMaterials(this.queryModel).subscribe(function (data) {
            if (!_this.MaterialData) {
                _this.MaterialData = [];
                _this.selectMaterial = [];
                _this.totalRecords = 0;
            }
            _this.MaterialData = data.items;
            _this.totalRecords = data.page.total;
        });
    };
    ChooseMaterialNumberComponent.prototype.formSubmit = function (bool) {
        // selectMaterial
        this.addDev.emit(this.selectMaterial[0]);
        // this.closeChooseMaterialNumberMask(false);
    };
    ChooseMaterialNumberComponent.prototype.closeChooseMaterialNumberMask = function (bool) {
        this.closeChooseMaterialNumber.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ChooseMaterialNumberComponent.prototype, "closeChooseMaterialNumber", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ChooseMaterialNumberComponent.prototype, "addDev", void 0);
    ChooseMaterialNumberComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-choose-material-number',
            template: __webpack_require__("../../../../../src/app/inventory/choose-material-number/choose-material-number.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/choose-material-number/choose-material-number.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"]])
    ], ChooseMaterialNumberComponent);
    return ChooseMaterialNumberComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/choose-warehouse-manager/choose-warehouse-manager.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/choose-warehouse-manager/choose-warehouse-manager.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"选择仓库管理员\" [(visible)]=\"display\" modal=\"modal\" width=\"1000\" [responsive]=\"true\" (onHide)=\"closeChooseWarehouseMask(false)\">\n  <div class=\"content-section implementation GridDemo clearfixes\">\n    <form>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1\">\n            <label >账号</label>\n          </div>\n          <div class=\"ui-grid-col-2\">\n            <input  name=\"inspectionName\"  type=\"text\" pInputText   placeholder=\"物料编号\" >\n          </div>\n          <div class=\"ui-grid-col-1\">\n            <label >名称</label>\n          </div>\n          <div class=\"ui-grid-col-2\">\n            <input  name=\"inspectionName\"  type=\"text\" pInputText   placeholder=\"物料名称\" >\n          </div>\n          <!--<div class=\"ui-grid-col-1\">-->\n            <!--<label >物料分类</label>-->\n          <!--</div>-->\n          <!--<div class=\"ui-grid-col-2\">-->\n            <!--<input  name=\"inspectionName\"  type=\"text\" pInputText   placeholder=\"物料分类\" >-->\n          <!--</div>-->\n          <div class=\"ui-grid-col-1\">\n            <button pButton  type=\"button\" (click)=\"showAddInspectionPlanMask()\" label=\"查询\"></button>\n          </div>\n        </div>\n        <p-dataTable [value]=\"inspections\"\n                     [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\">\n          <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n          <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n          <p-column header=\"操作\">\n            <!--<ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">-->\n            <!--<button pButton type=\"button\" label=\"修改\" (click)=\"updateOption(inspections[i])\"></button>-->\n            <!--<button pButton type=\"button\" label=\"关联巡检对象\" (click)=\"showAssociatedInspectionMask(inspections[i])\"></button>-->\n            <!--</ng-template>-->\n          </p-column>\n          <ng-template pTemplate=\"emptymessage\">\n            当前没有数据\n          </ng-template>\n        </p-dataTable>\n        <p-paginator rows=\"10\"  [rowsPerPageOptions]=\"[10,20,30]\" ></p-paginator>\n      </div>\n    </form>\n\n  </div>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\"  label=\"确定\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeChooseWarehouseMask(false)\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/choose-warehouse-manager/choose-warehouse-manager.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChooseWarehouseManagerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ChooseWarehouseManagerComponent = (function () {
    function ChooseWarehouseManagerComponent() {
        this.display = false;
        this.closeChooseWarehouse = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"];
    }
    ChooseWarehouseManagerComponent.prototype.ngOnInit = function () {
        this.display = true;
        this.cols = [
            { field: 'Number', header: '账号' },
            { filed: 'name', header: '名称' },
            { filed: 'name', header: '名称' },
            { filed: 'character', header: '角色' },
            { filed: 'department', header: '部门' },
        ];
    };
    ChooseWarehouseManagerComponent.prototype.closeChooseWarehouseMask = function (bool) {
        this.closeChooseWarehouse.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ChooseWarehouseManagerComponent.prototype, "closeChooseWarehouse", void 0);
    ChooseWarehouseManagerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-choose-warehouse-manager',
            template: __webpack_require__("../../../../../src/app/inventory/choose-warehouse-manager/choose-warehouse-manager.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/choose-warehouse-manager/choose-warehouse-manager.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ChooseWarehouseManagerComponent);
    return ChooseWarehouseManagerComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/equipment-storage/equipment-storage.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes\">\n    <form class=\"form-horizontal storage\" [formGroup]=\"inventoryForm\"   *ngIf=\"viewState === 'apply' \" >\n      <p-panel >\n        <p-header>\n          <div class=\"ui-helper-clearfix\">\n            <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n              <button class=\"save\"  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\"  > </button>\n            <button class=\"save\" pButton type=\"submit\" [ngStyle]=\"{'float':'right'}\" (click)=\"formSave()\" label=\"保存\"  ></button>\n            <button class=\"save\"  pButton type=\"submit\" [ngStyle]=\"{'float':'right'}\" label=\"提交\" (click)=\"formSubmit()\"   ></button>\n          </div>\n        </p-header>\n        <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 ui-padding-10px text-right\">\n                <label >入库单号:</label>\n              </div>\n              <div class=\"ui-grid-col-5\">\n                <input  formControlName=\"sid\"  name=\"inspectionName\"   type=\"text\" pInputText   placeholder=\"系统自动获取\" [(ngModel)]=\"submitData.sid\"  readonly>\n              </div>\n              <div class=\"ui-grid-col-1 text-right\">\n                <label >经手人:</label>\n              </div>\n              <div class=\"ui-grid-col-5\">\n                <input formControlName=\"keeper_pid\"  name=\"keeper_pid\"  type=\"text\" pInputText   placeholder=\"入库经手人\" [(ngModel)]=\"personalData.name\" readonly >\n              </div>\n            </div>\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 text-right\">\n                <label ><span>*</span>库存位置:</label>\n              </div>\n              <div class=\"ui-grid-col-3\">\n                <input formControlName=\"inspectiondepartment\"  type=\"text\" pInputText  name=\"inspectiondepartment\" placeholder=\"库存位置\"\n                       readonly class=\"cursor_not_allowed\"  [(ngModel)]=\"submitData.location\" />\n                <div class=\"ui-message ui-messages-error ui-corner-all \"   *ngIf=\"!inventoryForm.controls['inspectiondepartment'].valid&&(!inventoryForm.controls['inspectiondepartment'].untouched)\" >\n                  <i class=\"fa fa-close\"></i>\n                  库存位置必填\n                </div>\n              </div>\n              <div class=\"ui-grid-col-1\">\n                <button pButton  type=\"button\" (click)=\"showLoactionMask('add')\" label=\"选择\" ></button>\n              </div>\n              <div class=\"ui-grid-col-1\">\n                <button pButton  (click)=\"clearTreeDialog()\" label=\"清空\" ></button>\n              </div>\n              <div class=\"ui-grid-col-1 text-right\">\n                <label >仓库管理员:</label>\n              </div>\n              <div class=\"ui-grid-col-3\">\n                <input  formControlName=\"keeper\" type=\"text\" pInputText  name=\"keeper\" placeholder=\"仓库管理员\"\n                        readonly class=\"cursor_not_allowed\"  *ngIf=\"viewState === 'apply'\"/>\n              </div>\n              <div class=\"ui-grid-col-1\">\n                <button pButton  type=\"button\"  label=\"选择\"  name=\"fdsffe\"  ></button>\n              </div>\n              <div class=\"ui-grid-col-1\">\n                <button pButton  type=\"button\"  label=\"清空\" name=\"fdsf\"  ></button>\n              </div>\n            </div>\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 text-right\">\n                <label ><span></span>简述:</label>\n              </div>\n              <div class=\"ui-grid-col-11\">\n                <textarea formControlName=\"remarks\" name=\"jianshu\" pInputTextarea type=\"text\" [(ngModel)]=\"submitData.remarks\" ></textarea>\n              </div>\n            </div>\n        </div>\n      </p-panel>\n\n    <div class=\"ui-grid-row margin-bottom-1vw\">\n      <div class=\"ui-grid-col-2\">\n      </div>\n    </div>\n    <label></label>\n  <div class=\"\" >\n    <div class=\"ui-g\">\n      <div class=\"ui-g-10 ui-sm-12\" >\n        <div class=\"ui-g-2  ui-sm-5 ui-fluid\">\n        </div>\n        <div class=\"ui-g-2 ui-sm-5 ui-fluid  ui-no-padding-right-15px \">\n        </div>\n      </div>\n      <div class=\"ui-g-2 ui-sm-12\" >\n        <div class=\"ui-g-6  ui-sm-6  ui-fluid\">\n          <button   pButton   (click)=\"deleteStorage()\" label=\"移除\" [disabled]=\"selectInsepections.length===0\" ></button>\n        </div>\n        <div class=\"ui-g-6 ui-sm-6 ui-fluid  ui-no-padding-right-15px \">\n          <button pButton   (click)=\"showAddStorageMask('add')\" label=\"物品选择\" ></button>\n        </div>\n      </div>\n\n    </div>\n  </div>\n  <label><span>*</span>入库列表</label>\n  <p-dataTable [value]=\"inspections\" (onLazyLoad)=\"loadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\"  *ngIf=\"viewState === 'apply'\">\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n    <p-column header=\"操作\">\n      <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n      <button pButton type=\"button\" label=\"编辑\" (click)=\"updateOption(dataSource[i],'update',i)\"></button>\n      <button pButton type=\"button\" label=\"查看\" (click)=\"updateOption(dataSource[i],'show',i)\"></button>\n      </ng-template>\n    </p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  </form>\n    <form class=\"form-horizontal storage\" [formGroup]=\"inventoryForm\" *ngIf=\"viewState === 'update' \" >\n      <p-panel >\n        <p-header>\n          <div class=\"ui-helper-clearfix\">\n            <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n              <button class=\"save\"  pButton type=\"button\"  label=\"返回\" (click)=\"updategoBack()\"  > </button>\n            <button class=\"save\" pButton type=\"submit\" [ngStyle]=\"{'float':'right'}\" (click)=\"formSave()\" label=\"保存\" ></button>\n            <button class=\"save\"  pButton type=\"submit\" [ngStyle]=\"{'float':'right'}\" label=\"提交\" (click)=\"formSubmit()\"  ></button>\n          </div>\n        </p-header>\n        <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 ui-padding-10px text-right\">\n                <label >入库单号:</label>\n              </div>\n              <div class=\"ui-grid-col-5\">\n                <input  formControlName=\"sid\"  name=\"inspectionName\"   type=\"text\" pInputText   placeholder=\"入库单号\"  value=\"{{borrowData?.sid}}\" readonly>\n              </div>\n              <div class=\"ui-grid-col-1 text-right\">\n                <label >经手人:</label>\n              </div>\n              <div class=\"ui-grid-col-5\">\n                <input formControlName=\"keeper_pid\"  name=\"keeper_pid\"  type=\"text\" pInputText   placeholder=\"入库经手人\" value=\"{{borrowData?.creator}}\" >\n              </div>\n            </div>\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 text-right\">\n                <label ><span>*</span>库存位置:</label>\n              </div>\n              <div class=\"ui-grid-col-3\">\n                <input formControlName=\"inspectiondepartment\"  type=\"text\" pInputText  name=\"inspectiondepartment\" placeholder=\"库存位置\"\n                       readonly class=\"cursor_not_allowed\"  [(ngModel)]=\"borrowData.location\"/>\n                <div class=\"ui-message ui-messages-error ui-corner-all \"   *ngIf=\"!inventoryForm.controls['inspectiondepartment'].valid&&(!inventoryForm.controls['inspectiondepartment'].untouched)\" >\n                  <i class=\"fa fa-close\"></i>\n                  库存位置必填\n                </div>\n              </div>\n              <div class=\"ui-grid-col-1\">\n                <button pButton  type=\"button\" (click)=\"showLoactionMask()\" label=\"选择\" ></button>\n              </div>\n              <div class=\"ui-grid-col-1\">\n                <button pButton  (click)=\"clearTreeDialog()\" label=\"清空\" ></button>\n              </div>\n              <div class=\"ui-grid-col-1 text-right\">\n                <label >仓库管理员:</label>\n              </div>\n              <div class=\"ui-grid-col-3\">\n                <input  formControlName=\"keeper\" type=\"text\" pInputText  name=\"keeper\" placeholder=\"仓库管理员\"\n                        readonly class=\"cursor_not_allowed\"  />\n              </div>\n              <div class=\"ui-grid-col-1\">\n                <button pButton  type=\"button\"  label=\"选择\"  name=\"fdsffe\"  ></button>\n              </div>\n              <div class=\"ui-grid-col-1\">\n                <button pButton  type=\"button\"  label=\"清空\" name=\"ds\" ></button>\n              </div>\n            </div>\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 text-right\">\n                <label ><span></span>简述:</label>\n              </div>\n              <div class=\"ui-grid-col-11\">\n                <textarea formControlName=\"remarks\" name=\"jianshu\" pInputTextarea type=\"text\" [(ngModel)]=\"borrowData.remarks\" ></textarea>\n                <!--<div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!inventoryForm.controls['remarks'].valid&&(!inventoryForm.controls['remarks'].untouched)\" >-->\n                  <!--<i class=\"fa fa-close\"></i>-->\n                  <!--简述信息必填-->\n                <!--</div>-->\n              </div>\n            </div>\n\n\n        </div>\n\n      </p-panel>\n  <div class=\"\" >\n    <div class=\"ui-g\">\n      <div class=\"ui-g-10 ui-sm-12\" >\n        <div class=\"ui-g-2  ui-sm-5 ui-fluid\">\n        </div>\n        <div class=\"ui-g-2 ui-sm-5 ui-fluid  ui-no-padding-right-15px \">\n        </div>\n      </div>\n      <div class=\"ui-g-2 ui-sm-12\" >\n        <div class=\"ui-g-6  ui-sm-6  ui-fluid\">\n          <button   pButton   (click)=\"updateDeleteStorage()\" label=\"移除\" [disabled]=\"selectInsepections.length===0\" ></button>\n        </div>\n        <div class=\"ui-g-6 ui-sm-6 ui-fluid  ui-no-padding-right-15px \">\n          <button pButton   (click)=\"showAddStorageMask('update')\" label=\"物品选择\" ></button>\n        </div>\n      </div>\n\n    </div>\n  </div>\n  <label><span>*</span>入库列表</label>\n\n  <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" >\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n    <p-column header=\"操作\">\n      <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n      <button pButton type=\"button\" label=\"编辑\" (click)=\"updateOption(editDataSource[i],'update',i)\"></button>\n      <button pButton type=\"button\" label=\"查看\" (click)=\"updateOption(editDataSource[i],'show',i)\"></button>\n      </ng-template>\n    </p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n\n  </form>\n    <form class=\"form-horizontal storage\" [formGroup]=\"inventoryForm\"  *ngIf=\"viewState === 'viewDetail' \">\n      <p-panel >\n        <p-header>\n          <div class=\"ui-helper-clearfix\">\n            <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n              <button class=\"save\"  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\"  > </button>\n          </div>\n        </p-header>\n        <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 ui-padding-10px text-right\">\n                <label >入库单号:</label>\n              </div>\n              <div class=\"ui-grid-col-5\">\n                <input  formControlName=\"sid\"  name=\"inspectionName\"   type=\"text\" pInputText   placeholder=\"入库单号\"  value=\"{{borrowData?.sid}}\" readonly>\n              </div>\n              <div class=\"ui-grid-col-1 text-right\">\n                <label >经手人:</label>\n              </div>\n              <div class=\"ui-grid-col-5\">\n                <input formControlName=\"keeper_pid\"  name=\"keeper_pid\"  type=\"text\" pInputText   placeholder=\"入库经手人\" value=\"{{borrowData?.creator}}\" readonly>\n              </div>\n            </div>\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 text-right\">\n                <label ><span></span>库存位置:</label>\n              </div>\n              <div class=\"ui-grid-col-5\">\n                <input formControlName=\"inspectiondepartment\"  type=\"text\" pInputText  name=\"inspectiondepartment\" placeholder=\"库存位置\"\n                       readonly class=\"cursor_not_allowed\"  [(ngModel)]=\"borrowData.location\"  />\n              </div>\n              <div class=\"ui-grid-col-1 text-right\">\n                <label >仓库管理员:</label>\n              </div>\n              <div class=\"ui-grid-col-5\">\n                <input  formControlName=\"keeper\" type=\"text\" pInputText  name=\"keeper\" placeholder=\"仓库管理员\"\n                        readonly class=\"cursor_not_allowed\" />\n              </div>\n            </div>\n            <div class=\"ui-grid-row margin-bottom-1vw\">\n              <div class=\"ui-grid-col-1 text-right\">\n                <label ><span></span>简述:</label>\n              </div>\n              <div class=\"ui-grid-col-11\">\n                <textarea formControlName=\"remarks\"  readonly name=\"jianshu\" pInputTextarea type=\"text\" [(ngModel)]=\"borrowData.remarks\" ></textarea>\n              </div>\n            </div>\n            <div class=\"ui-grid-row margin-bottom-1vw\" *ngIf=\"borrowData.status !=='新建'&& borrowData.status !=='待审批'\">\n              <div class=\"ui-grid-col-1 text-right\">\n                <label ><span></span>审批意见:</label>\n              </div>\n              <div class=\"ui-grid-col-11\">\n                <textarea formControlName=\"approve_remarks\"  readonly name=\"jianshu\" pInputTextarea type=\"text\" [(ngModel)]=\"borrowData.approve_remarks\" ></textarea>\n              </div>\n            </div>\n\n\n        </div>\n\n      </p-panel>\n\n    <div class=\"ui-grid-row margin-bottom-1vw\">\n      <div class=\"ui-grid-col-2\">\n      </div>\n    </div>\n    <label></label>\n  <div class=\"\" *ngIf=\"viewState !== 'viewDetail'&& viewState !== 'update'\">\n    <div class=\"ui-g\">\n      <div class=\"ui-g-10 ui-sm-12\" >\n        <div class=\"ui-g-2  ui-sm-5 ui-fluid\">\n        </div>\n        <div class=\"ui-g-2 ui-sm-5 ui-fluid  ui-no-padding-right-15px \">\n        </div>\n      </div>\n      <div class=\"ui-g-2 ui-sm-12\" >\n        <div class=\"ui-g-6  ui-sm-6  ui-fluid\">\n          <button   pButton   (click)=\"deleteStorage()\" label=\"删除\"[disabled]=\"selectInsepections.length===0\" *ngIf=\"viewState !== 'viewDetail'&& viewState !== 'update'\"></button>\n        </div>\n        <div class=\"ui-g-6 ui-sm-6 ui-fluid  ui-no-padding-right-15px \">\n          <button pButton   (click)=\"showAddStorageMask()\" label=\"物品选择\" *ngIf=\"viewState !== 'viewDetail'&& viewState !== 'update'\"></button>\n        </div>\n      </div>\n\n    </div>\n  </div>\n  <label><span></span>入库列表</label>\n  <p-dataTable [value]=\"inspections\" (onLazyLoad)=\"loadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\"  *ngIf=\"viewState === 'apply'\">\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n    <p-column header=\"操作\">\n      <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n      <button pButton type=\"button\" label=\"编辑\" (click)=\"updateOption(dataSource[i],'update',i)\"></button>\n      <button pButton type=\"button\" label=\"查看\" (click)=\"updateOption(dataSource[i],'show',i)\"></button>\n      </ng-template>\n    </p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" *ngIf=\"viewState === 'update' \">\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n    <p-column header=\"操作\">\n      <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n      <button pButton type=\"button\" label=\"编辑\" (click)=\"updateOption(editDataSource[i],'update',i)\"></button>\n      <button pButton type=\"button\" label=\"查看\" (click)=\"updateOption(editDataSource[i],'show',i)\"></button>\n      </ng-template>\n    </p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" *ngIf=\"viewState === 'viewDetail' \">\n    <!--<p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>-->\n    <p-column  *ngFor=\"let col of coles\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <label></label>\n  <div class=\"ui-grid-row margin-bottom-1vw\">\n    <div class=\"ui-grid-col-2\">\n    </div>\n  </div>\n  </form>\n</div>\n<app-add-storage\n  *ngIf=\"showAddStorage\"\n  (addStorage)=\"addStorage($event)\"\n  [state]=\"state\"\n  [currentMeterial]=\"currentMaterial\"\n  (updateStorage)=\"updateStorage($event)\"\n  (closeAddStorage)=\"closeAddStorageMask($event)\">\n</app-add-storage>\n\n<app-choose-warehouse-manager\n  *ngIf=\"showWarehouseManager\"\n  (closeChooseWarehouse)=\"closeWarehouseManagerMask($event)\">\n</app-choose-warehouse-manager>\n\n<app-add-inventory-location *ngIf=\"showLocation\" (closeMask)=\"closeLocationMask($event)\" (addTree)=\"nodeTree($event)\" [treeState]=\"treeState\"></app-add-inventory-location>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/equipment-storage/equipment-storage.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/deep/.ui-widget-header {\n  background-color: white;\n  background: linear-gradient(to bottom, #ffffff 0%, #ffffff 100%); }\n\n.mysearch {\n  margin-bottom: 20px;\n  background: #fff; }\n\nlabel > span {\n  color: red; }\n\n.storage /deep/ .ui-messages-error {\n  color: #ab1a0f;\n  background-color: white;\n  border-color: white; }\n\n.save {\n  width: 100px; }\n\n.curser {\n  cursor: pointer; }\n\n.storage input {\n  border-bottom: 1px solid #D6D6D6; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/equipment-storage/equipment-storage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EquipmentStorageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EquipmentStorageComponent = (function () {
    function EquipmentStorageComponent(inventoryService, confirmationService, storageService, router, route, eventBusService) {
        this.inventoryService = inventoryService;
        this.confirmationService = confirmationService;
        this.storageService = storageService;
        this.router = router;
        this.route = route;
        this.eventBusService = eventBusService;
        this.showAddStorage = false; //新增录入
        this.showWarehouseManager = false; //选择仓库管理员弹框
        this.showLocation = false; //选择仓库管理员弹框
        this.dataSource = [];
        this.borrowData = [];
        this.inspections = [];
        this.selectInsepections = [];
        this.title = '入库申请';
        this.editDataSource = [];
        this.personalData = [];
        this.fb = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]();
    }
    EquipmentStorageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.inventoryForm = this.fb.group({
            'sid': [''],
            'keeper_pid': [''],
            'inspectiondepartment': ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required],
            'keeper': [''],
            'remarks': [''],
            'approve_remarks': [''],
        });
        this.submitData = {
            "sid": "",
            "location": "",
            "location_id": "",
            "keeper": "",
            "keeper_pid": "",
            "remarks": "",
            "inventories": []
        };
        this.viewState = 'apply'; // 入库申请状态
        this.cols = [
            { field: 'pn', header: '编号' },
            { field: 'name', header: '名称' },
            { field: 'catagory', header: '分类' },
            { field: 'brand', header: '品牌' },
            { field: 'model', header: '型号' },
            { field: 'specification', header: '规格' },
            { field: 'quantity', header: '数量' },
            { field: 'supplier', header: '供应商' },
        ];
        this.coles = [
            { field: 'pn', header: '编号' },
            { field: 'name', header: '名称' },
            { field: 'catagory', header: '分类' },
            { field: 'brand', header: '品牌' },
            { field: 'model', header: '型号' },
            { field: 'specification', header: '规格' },
            { field: 'quantity', header: '数量' },
            { field: 'supplier', header: '供应商' },
            { field: 'location', header: '库存位置' },
        ];
        this.route.queryParams.subscribe(function (params) {
            _this.viewState = 'apply';
            if (params['sid'] && params['title'] && params['state']) {
                _this.title = params['title'];
                _this.sid = params['sid'];
                _this.viewState = params['state'];
                _this.inventoryService.queryStorage(_this.sid).subscribe(function (data) {
                    _this.borrowData = data;
                    console.log(_this.borrowData);
                    _this.editDataSource = _this.borrowData['inventories'] ? _this.borrowData['inventories'] : [];
                    if (!_this.editDataSource) {
                        _this.editDataSource = [];
                        _this.editInspections = [];
                        _this.totalRecords = 0;
                    }
                    _this.editInspections = _this.editDataSource.slice(0, 10);
                    _this.totalRecords = _this.editDataSource.length;
                });
            }
        });
        this.querylogin();
    };
    EquipmentStorageComponent.prototype.updategoBack = function () {
        var sid = this.borrowData['sid'];
        this.borrowData['status'] = "";
        this.eventBusService.flow.next(this.borrowData['status']);
        this.router.navigate(['../overview'], { queryParams: { sid: sid, state: 'viewDetail', title: '入库总览' }, relativeTo: this.route });
    };
    EquipmentStorageComponent.prototype.goBack = function () {
        var sid = this.borrowData['sid'];
        this.borrowData['status'] = "";
        this.eventBusService.flow.next(this.borrowData['status']);
        this.router.navigate(['../overview'], { queryParams: { sid: sid, state: 'viewDetail', title: '入库总览' }, relativeTo: this.route });
    };
    //查询登录人
    EquipmentStorageComponent.prototype.querylogin = function () {
        var _this = this;
        this.inventoryService.queryPersonal().subscribe(function (data) {
            _this.personalData = data;
            console.log(_this.personalData);
        });
    };
    EquipmentStorageComponent.prototype.showAddStorageMask = function (state) {
        this.state = 'add';
        this.showAddStorage = true;
    };
    EquipmentStorageComponent.prototype.closeAddStorageMask = function (bool) {
        this.showAddStorage = bool;
    };
    EquipmentStorageComponent.prototype.showLoactionMask = function () {
        this.treeState = 'add';
        this.showLocation = true;
    };
    EquipmentStorageComponent.prototype.updateOption = function (currentMaterial, state, index) {
        this.state = state;
        this.currentMaterial = currentMaterial;
        console.log(this.currentMaterial);
        this.storageService.setCurrentStorage('storageindex', index);
        this.showAddStorage = true;
    };
    // 提交
    EquipmentStorageComponent.prototype.addStorageSubmint = function (submitData, dataSource) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < dataSource.length; i++) {
            var temp = dataSource[i];
            var obj = {};
            obj['pn'] = temp['pn'];
            obj['unit_cost'] = temp['unit_cost'];
            obj['quantity'] = temp['quantity'] + "";
            obj['time_purchase'] = temp['time_purchase'];
            inventories.push(obj);
        }
        submitData.inventories = inventories;
        this.inventoryService.addStorage(submitData).subscribe(function () {
            _this.router.navigate(['../overview'], { queryParams: { title: '入库总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    EquipmentStorageComponent.prototype.addStorage = function (storage) {
        this.dataSource.push(storage);
        this.inspections = this.dataSource.slice(0, 10);
        this.totalRecords = this.dataSource.length;
        this.editDataSource.push(storage);
        this.editInspections = this.editDataSource.slice(0, 10);
        this.totalRecords = this.editDataSource.length;
        this.showAddStorage = false;
    };
    EquipmentStorageComponent.prototype.updateStorage = function (storage) {
        var index = this.storageService.getCurrentStorage('storageindex');
        if (this.viewState === 'update') {
            // this.editDataSource.push(storage);
            this.editDataSource[index] = storage;
            this.editInspections = this.editDataSource.slice(0, 10);
            this.showAddStorage = false;
        }
        this.dataSource[index] = storage;
        this.inspections = this.dataSource.slice(0, 10);
        this.showAddStorage = false;
    };
    EquipmentStorageComponent.prototype.loadCarsLazy = function (event) {
        //in a real application, make a remote request to load data using state metadata from event
        //event.first = First row offset
        //event.rows = Number of rows per page
        //event.sortField = Field name to sort with
        //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
        var _this = this;
        //imitate db connection over a network
        setTimeout(function () {
            if (_this.dataSource) {
                _this.inspections = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 0);
    };
    EquipmentStorageComponent.prototype.editloadCarsLazy = function (event) {
        //in a real application, make a remote request to load data using state metadata from event
        //event.first = First row offset
        //event.rows = Number of rows per page
        //event.sortField = Field name to sort with
        //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
        var _this = this;
        //imitate db connection over a network
        setTimeout(function () {
            if (_this.editDataSource) {
                _this.editInspections = _this.editDataSource.slice(event.first, (event.first + event.rows));
            }
        }, 0);
    };
    //保存
    EquipmentStorageComponent.prototype.addDeviceSave = function (data) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < this.dataSource.length; i++) {
            var temp = this.dataSource[i];
            var obj = {};
            obj['pn'] = temp['pn'];
            obj['unit_cost'] = temp['unit_cost'];
            obj['quantity'] = temp['quantity'] + "";
            obj['time_purchase'] = temp['time_purchase'];
            inventories.push(obj);
        }
        data.inventories = inventories;
        this.inventoryService.queryStorageSave(data).subscribe(function (data) {
            _this.router.navigate(['../overview'], { queryParams: { title: '入库总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    EquipmentStorageComponent.prototype.deleteStorage = function () {
        for (var i = 0; i < this.selectInsepections.length; i++) {
            for (var j = 0; j < this.dataSource.length; j++) {
                if (this.selectInsepections[i]['sid'] === this.dataSource[j]['sid']) {
                    this.dataSource.splice(j, 1);
                    this.inspections = this.dataSource.slice(0, 10);
                    this.selectInsepections = [];
                }
            }
        }
    };
    EquipmentStorageComponent.prototype.updateDeleteStorage = function () {
        var editDataSource = this.editDataSource.slice();
        for (var i = 0; i < this.selectInsepections.length; i++) {
            for (var j = 0; j < editDataSource.length; j++) {
                if (this.selectInsepections[i]['pn'] === editDataSource[j]['pn']) {
                    this.editDataSource.splice(j, 1);
                    this.editInspections = this.editDataSource.slice(0, 10);
                    break;
                }
            }
        }
        this.selectInsepections = [];
    };
    EquipmentStorageComponent.prototype.nodeTree = function (node) {
        this.submitData.location = node['label'];
        this.submitData.location_id = node['did'];
        this.borrowData['location'] = node['label'];
        this.borrowData['location_id'] = node['did'];
        this.showLocation = false;
    };
    //  仓库管理员
    EquipmentStorageComponent.prototype.showWarehouseManagerMask = function () {
        this.showWarehouseManager = !this.showWarehouseManager;
    };
    EquipmentStorageComponent.prototype.closeWarehouseManagerMask = function (bool) {
        this.showWarehouseManager = bool;
    };
    EquipmentStorageComponent.prototype.closeLocationMask = function (bool) {
        this.showLocation = bool;
    };
    //修改保存方法
    EquipmentStorageComponent.prototype.updateDeviceSave = function (submitData, dataSource) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < dataSource.length; i++) {
            var temp = dataSource[i];
            var obj = {};
            obj['sid'] = temp['sid'];
            obj['location'] = temp['location'];
            obj['location_id'] = temp['location_id'];
            obj['keeper'] = temp['keeper'];
            obj['keeprer_pid'] = temp['keeprer_pid'];
            obj['unit_cost'] = temp['unit_cost'];
            obj['quantity'] = temp['quantity'] + "";
            obj['pn'] = temp['pn'];
            obj['name'] = temp['name'];
            obj['catagory'] = temp['catagory'];
            obj['supplier'] = temp['supplier'];
            obj['brand'] = temp['brand'];
            obj['model'] = temp['model'];
            obj['time_purchase'] = temp['time_purchase'];
            obj['specification'] = temp['specification'];
            inventories.push(obj);
        }
        submitData.inventories = inventories;
        this.inventoryService.updateStorage(submitData).subscribe(function (data) {
            _this.router.navigate(['../overview'], { queryParams: { title: '入库总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    EquipmentStorageComponent.prototype.formSave = function () {
        if (this.viewState === 'apply') {
            this.addDeviceSave(this.submitData);
        }
        else if (this.viewState === 'update') {
            this.updateDeviceSave(this.borrowData, this.editDataSource);
            // this.addDeviceSave(this.storageData)
        }
    };
    EquipmentStorageComponent.prototype.formSubmit = function () {
        if (this.viewState === 'apply') {
            this.addStorageSubmint(this.submitData, this.dataSource);
        }
        else if (this.viewState === 'update') {
            this.addStorageSubmint(this.borrowData, this.editDataSource);
        }
    };
    EquipmentStorageComponent.prototype.clearTreeDialog = function () {
        this.submitData.location = '';
        this.submitData.location_id = '';
        this.borrowData['location'] = '';
        this.borrowData['location_id'] = '';
    };
    EquipmentStorageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-equipment-storage',
            template: __webpack_require__("../../../../../src/app/inventory/equipment-storage/equipment-storage.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/equipment-storage/equipment-storage.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_6__services_event_bus_service__["a" /* EventBusService */]])
    ], EquipmentStorageComponent);
    return EquipmentStorageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/flow-chart/flow-chart.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n  <div>\n    <span class=\"feature-title\">库存管理<span class=\"gt\">&gt;</span>{{title}}  </span>\n  </div>\n\n</div>\n<div class=\"content-section implementation GridDemo\">\n  <div class=\"parent\">\n    <div style=\"position: absolute;top: 20px;left:0\">\n\n      <svg width=\"100\" height=\"600\">\n        <ng-container *ngFor=\"let flow of flowCharts;let i =index\">\n          <g class=\"rect-text\" (click)=\"openPage(flow,i)\" >\n            <rect [ngStyle]=\"{'cursor':'pointer'}\" [attr.width]=\"flow.width\"[attr.height]=\"flow.height\" [attr.stroke]=\"flow.stroke\" [attr.x]=\"flow.x\" [attr.y]=\"flow.y\" [attr.rx]=\"flow.rx\" [attr.fill]=\"flow.fill\" [attr.status]=\"flow.status\" ></rect>\n            <text [attr.x]=\"flow.text.x\" [attr.y]=\"flow.text.y\">{{flow.label}}</text>\n          </g>\n          <g stroke=\"#39B9C6\" stroke-width=\"0.8\">\n            <line *ngFor=\"let g of flow.g\" [attr.x1]=\"g.x1\" [attr.y1]=\"g.y1\" [attr.x2]=\"g.x2\" [attr.y2]=\"g.y2\"></line>\n          </g>\n        </ng-container>\n      </svg>\n    </div>\n    <div class=\"node-right\">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/flow-chart/flow-chart.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".node-right {\n  margin-left: 135px;\n  min-height: 400px; }\n\n.parent {\n  position: relative; }\n\n.rect-text {\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/flow-chart/flow-chart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FlowChartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FlowChartComponent = (function () {
    function FlowChartComponent(router, route, eventBus, inventoryService) {
        this.router = router;
        this.route = route;
        this.eventBus = eventBus;
        this.inventoryService = inventoryService;
        this.title = '入库总览';
    }
    FlowChartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.queryModel = {
            "status": "",
        };
        this.flowCharts = [
            { label: '入库申请', route: 'equipment', stroke: "#39B9C6", fill: '#357CA5', active: true, x: '0', y: '0', rx: '10', width: 100, height: 40, title: '入库申请', text: { x: '20', y: '24', label: '入库申请' }, g: [{ x1: '50', y1: '40', x2: '50', y2: '100', }, { x1: '50', y1: '100', x2: '43', y2: '90' }, { x1: '50', y1: '100', x2: '58', y2: '90' }] },
            { label: '入库审批', route: 'overview', status: "待审批", stroke: "#39B9C6", fill: '#357CA5', active: false, x: '0', y: '100', width: 100, height: 40, title: '入库审批', text: { x: '20', y: '124', label: '入库审批' }, g: [{ x1: '50', y1: '140', x2: '50', y2: '200' }, { x1: '50', y1: '200', x2: '43', y2: '190' }, { x1: '50', y1: '200', x2: '58', y2: '190' }] },
            { label: '库存位置', route: 'overview', status: "待入库", stroke: "#39B9C6", fill: '#357CA5', active: true, x: '0', y: '200', width: 100, height: 40, title: '库存位置', text: { x: '20', y: '224', label: '制定库存位置' }, g: [{ x1: '50', y1: '240', x2: '50', y2: '300' }, { x1: '50', y1: '300', x2: '43', y2: '290' }, { x1: '50', y1: '300', x2: '58', y2: '290' }] },
            { label: '入库', route: 'overview', stroke: "#39B9C6", fill: '#357CA5', x: '0', y: '300', rx: '10', width: 100, height: 40, title: '', text: { x: '30', y: '324', label: '入库' } }
        ];
        this.eventBus.flow.subscribe(function (data) {
            console.log(data);
            _this.queryModel.status = data;
            _this.queryMaterialDate();
        });
        this.route.queryParams.subscribe(function (params) {
            if (params['title']) {
                _this.title = params['title'];
            }
        });
        this.queryMaterialDate();
    };
    FlowChartComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryMaterialChar(this.queryModel).subscribe(function (data) {
            if (!_this.MaterialCharData) {
                _this.MaterialCharData = [];
            }
            _this.MaterialCharData = data;
            for (var i = 0; i < _this.MaterialCharData.length; i++) {
                var temp = _this.MaterialCharData[i];
                _this.flowCharts[i]['fill'] = temp['fill'];
            }
        });
    };
    FlowChartComponent.prototype.openPage = function (flow, index) {
        if (index + 1 === this.flowCharts.length)
            return;
        for (var i = 0; i < this.flowCharts.length; i++) {
            this.flowCharts[i].active = false;
        }
        this.flowCharts[index].active = true;
        this.queryMaterialDate();
        if (flow.status) {
            this.eventBus.updateborrow.next(flow.status);
        }
        if (flow.route) {
            this.router.navigate([flow.route], { relativeTo: this.route });
            this.title = flow.title;
        }
    };
    FlowChartComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-flow-chart',
            template: __webpack_require__("../../../../../src/app/inventory/flow-chart/flow-chart.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/flow-chart/flow-chart.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_2__inventory_service__["a" /* InventoryService */]])
    ], FlowChartComponent);
    return FlowChartComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/inventory-location/inventory-location.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes location\">\r\n  <div class=\"ui-g \">\r\n    <div class=\"ui-g-12\">\r\n      <p-dataTable emptyMessage=\"没有数据\" [value]=\"materials\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\r\n                   [totalRecords]=\"totalRecords\" (onLazyLoad)=\"loadCarsLazy($event)\" [stacked]=\"stacked\" [responsive]=\"true\" >\r\n        <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\r\n        <p-column header=\"库存位置\">\r\n          <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\r\n            <div class=\"ui-g\">\r\n              <div class=\"ui-g-6  ui-fluid\">\r\n                <input  type=\"text\" pInputText  name=\"inspectiondepartment\"\r\n                        readonly  [(ngModel)]=\"dataSource[i].location\"  />\r\n              </div>\r\n              <div class=\"ui-g-6\">\r\n                <button  pButton  type=\"button\" (click)=\"showLoactionMask(i)\" label=\"选择\"></button>\r\n              </div>\r\n            </div>\r\n          </ng-template>\r\n        </p-column>\r\n      </p-dataTable>\r\n    </div>\r\n    <div class=\"ui-grid-col-6\">\r\n      <button  pButton  type=\"button\" (click)=\"sureApproved()\" label=\"确认入库\"></button>\r\n      <button  pButton  type=\"button\"  (click)=\"goBack()\" label=\"取消\"></button>\r\n    </div>\r\n  </div>\r\n</div>\r\n  <app-add-inventory-location\r\n    *ngIf=\"showLocation\"\r\n    (closeMask)=\"closeLocationMask($event)\"\r\n    (sureButton)=\"nodeTree($event)\"\r\n    [pnAnddataSource]=\"pnAnddataSource\"\r\n    [treeState]=\"treeState\" [sid]=\"sid\"\r\n    (updateDev)=\"updateLocation($event)\"\r\n  ></app-add-inventory-location>\r\n  <!--<app-add-inventory-location *ngIf=\"showLocation\" (closeMask)=\"closeLocationMask($event)\" (addTree)=\"nodeTree($event)\" [treeState]=\"treeState\"></app-add-inventory-location>-->\r\n"

/***/ }),

/***/ "../../../../../src/app/inventory/inventory-location/inventory-location.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (min-width: 1366px) and (max-width: 1920px) {\n  .location /deep/ table thead tr th:last-child {\n    width: 20%; }\n  /deep/ table thead tr th:nth-child(1) {\n    width: 20%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/inventory-location/inventory-location.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InventoryLocationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var InventoryLocationComponent = (function () {
    function InventoryLocationComponent(inventoryService, storageService, route, router, confirmationService, eventBusService) {
        this.inventoryService = inventoryService;
        this.storageService = storageService;
        this.route = route;
        this.router = router;
        this.confirmationService = confirmationService;
        this.eventBusService = eventBusService;
        this.chooseCids = [];
        this.selectMaterial = [];
        this.pnAnddataSource = {};
        // currentMaterial;
        this.showLocation = false; //选择仓库管理员弹框
    }
    InventoryLocationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.queryModel = {
            "condition": {
                "name": "",
                "catagory": "",
                "count": "",
                "status": ""
            },
            "page": {
                "page_size": "10",
                "page_number": "1"
            }
        };
        this.cols = [
            { field: 'sid', header: '设备编号' },
            { field: 'name', header: '设备名称' },
            { field: 'catagory', header: '设备分类' },
            { field: 'brand', header: '品牌' },
            { field: 'model', header: '型号' },
            { field: 'specification', header: '规格' },
            { field: 'quantity', header: '数量' },
        ];
        this.route.queryParams.subscribe(function (params) {
            if (params['sid']) {
                _this.sid = params['sid'];
                _this.evstatus = params['status'];
            }
        });
        this.queryMaterialDate();
    };
    InventoryLocationComponent.prototype.goBack = function () {
        var sid = this.sid;
        this.evstatus = "";
        this.eventBusService.flow.next(this.evstatus);
        this.router.navigate(['../overview'], { queryParams: { sid: sid, state: 'viewDetail', title: '入库总览' }, relativeTo: this.route });
    };
    InventoryLocationComponent.prototype.showLoactionMask = function (index) {
        this.treeState = 'update';
        this.pnAnddataSource['sid'] = this.sid;
        this.pnAnddataSource['data'] = this.dataSource;
        // this.currentStorageOverView = this.MaterialData[index];
        this.storageService.setLocationIndex('locationIndex', index);
        this.showLocation = true;
    };
    InventoryLocationComponent.prototype.closeLocationMask = function (bool) {
        this.showLocation = bool;
    };
    InventoryLocationComponent.prototype.clearTreeDialog = function () {
        this.materials.location = '';
        this.materials.location = [];
    };
    InventoryLocationComponent.prototype.nodeTree = function (node) {
        var localtionIndex = this.storageService.getLocationIndex('locationIndex');
        this.materials[localtionIndex].location = node['label'];
        this.materials[localtionIndex].location_id = node['did'];
        this.showLocation = false;
    };
    //确认入库
    InventoryLocationComponent.prototype.sureApproved = function () {
        var _this = this;
        this.inventoryService.queryStorageSure(this.sid).subscribe(function (data) {
            _this.selectMaterial = [];
            _this.evstatus = "";
            _this.eventBusService.flow.next(_this.evstatus);
            _this.router.navigate(['../overview'], { queryParams: { 'sid': _this.sid, title: '入库总览' }, relativeTo: _this.route });
            // this.queryMaterialDate();
        });
    };
    //分页查询
    InventoryLocationComponent.prototype.paginate = function (event) {
        this.chooseCids = [];
        this.queryModel.page.page_number = String(++event.page);
        this.queryModel.page.page_size = String(event.rows);
        this.queryMaterialDate();
    };
    //同步更新页面数据
    InventoryLocationComponent.prototype.updateLocation = function () {
        this.showLocation = false;
        this.queryMaterialDate();
    };
    InventoryLocationComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryStorage(this.sid).subscribe(function (data) {
            if (!data['inventories']) {
                _this.dataSource = [];
                _this.selectMaterial = [];
                _this.totalRecords = 0;
            }
            else {
                _this.dataSource = data['inventories'];
                console.log(_this.dataSource);
                _this.totalRecords = _this.dataSource.length;
                _this.materials = _this.dataSource.slice(0, 10);
            }
        });
    };
    InventoryLocationComponent.prototype.loadCarsLazy = function (event) {
        if (this.dataSource) {
            this.materials = this.dataSource.slice(event.first, (event.first + event.rows));
        }
    };
    InventoryLocationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-inventory-location',
            template: __webpack_require__("../../../../../src/app/inventory/inventory-location/inventory-location.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/inventory-location/inventory-location.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_5__services_event_bus_service__["a" /* EventBusService */]])
    ], InventoryLocationComponent);
    return InventoryLocationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/inventory-overview/inventory-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \">\n  <div>\n    <span class=\"feature-title\">库存管理<span class=\"gt\">&gt;</span>库存总览  </span>\n  </div>\n\n</div>\n<div class=\"content-section implementation GridDemo clearfixes inventooryOverview\">\n  <form>\n  <div class=\"mysearch\">\n    <div class=\"ui-g\">\n      <div class=\"ui-g-5 ui-sm-12\">\n        <label class=\"ui-g-2 ui-sm-5  text-right\">物品编号:</label>\n        <div class=\"ui-g-4 ui-sm-7 ui-fluid  ui-no-padding-right-15px \">\n          <input  pInputText type=\"text\" name=\"status\" placeholder=\"物品编号\"  [(ngModel)]=\"queryModel.condition.sid\" />\n        </div>\n        <label class=\"ui-g-2 ui-sm-5 text-right\">物品名称:</label>\n        <div class=\"ui-g-4  ui-sm-7 ui-fluid\">\n          <input pInputText type=\"text\" name=\"sid\" placeholder=\"物品名称\" [(ngModel)]=\"queryModel.condition.name\" />\n        </div>\n\n      </div>\n      <div class=\"ui-g-4 ui-sm-12\">\n        <label class=\"ui-g-2 ui-sm-5 ui-g-offset-1 text-right\">物品分类:</label>\n        <div class=\"ui-g-6  ui-sm-7 ui-fluid\">\n          <p-dropdown [autoWidth]=\"false\"  [options]=\"categories\"  name=\"fenlei\" [(ngModel)]=\"queryModel.condition.catagory\" ></p-dropdown>\n        </div>\n      </div>\n      <div class=\"ui-g-3 ui-sm-12\">\n        <div class=\"ui-g-6 ui-sm-12 option\">\n          <button pButton type=\"button\" label=\"查询\" ngClass=\"ui-sm-12\" (click)=\"suggestInsepectiones()\" ></button>\n          <button pButton label=\"清空\" ngClass=\"ui-sm-12\" (click)=\"clearSearch()\"></button>\n        </div>\n      </div>\n    </div>\n  </div>\n  </form>\n\n  <p-dataTable [value]=\"MaterialData\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectMaterial\">\n    <p-column  field=\"sid\" header=\"物品编号\" [sortable]=\"true\" >\n      <ng-template let-data=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n        <span>{{data.sid}}</span>\n      </ng-template>\n    </p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"  [sortable]=\"true\"></p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <p-paginator rows=\"10\" [totalRecords]=\"totalRecords\" [rowsPerPageOptions]=\"[10,20,30]\" (onPageChange)=\"paginate($event)\"></p-paginator>\n\n</div>\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/inventory-overview/inventory-overview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".mysearch {\n  margin-bottom: 20px;\n  padding: 10px;\n  background: #fff; }\n\n.inventooryOverview /deep/ .ui-datatable th.ui-state-active {\n  background: #EFF0F3;\n  color: #555555; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/inventory-overview/inventory-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InventoryOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InventoryOverviewComponent = (function () {
    function InventoryOverviewComponent(inventoryService) {
        this.inventoryService = inventoryService;
        this.selectMaterial = [];
        this.chooseCids = [];
        this.statusNames = [];
        this.statusCategory = [];
    }
    InventoryOverviewComponent.prototype.ngOnInit = function () {
        this.statusNames = [
            { label: '', value: '' },
            { label: '在库', value: '在库' },
            { label: '已完成', value: '已完成' },
        ];
        this.statusCategory = [
            { label: '', value: '' },
            { label: '整机', value: '整机' },
            { label: '部件', value: '部件' },
            { label: '耗材', value: '耗材' },
            { label: '其他', value: '其他' },
        ];
        this.cols = [
            // {field: 'id', header: '序号'},
            // {field: 'sid', header: '物品编号'},
            { field: 'name', header: '物品名称' },
            { field: 'catagory', header: '物品分类' },
            { field: 'model', header: '型号' },
            { field: 'specification', header: '规格' },
            { field: 'quantity', header: '数量' },
            { field: 'location', header: '库存位置' },
            { field: 'keeper', header: '库管员' },
        ];
        this.queryModel = {
            "condition": {
                "name": "",
                "sid": "",
                "catagory": "",
                "count": "",
                "status": "",
            },
            "page": {
                "page_size": "10",
                "page_number": "1"
            }
        };
        this.queryMaterialDate();
        this.queryMeterial();
    };
    InventoryOverviewComponent.prototype.clearSearch = function () {
        this.queryModel.condition.name = '';
        this.queryModel.condition.sid = '';
        this.queryModel.condition.catagory = '';
    };
    //搜索功能
    InventoryOverviewComponent.prototype.suggestInsepectiones = function () {
        this.queryModel.page.page_number = '1';
        this.queryModel.page.page_size = '10';
        this.queryMaterialDate();
    };
    //分页查询
    InventoryOverviewComponent.prototype.paginate = function (event) {
        this.chooseCids = [];
        this.queryModel.page.page_number = String(++event.page);
        this.queryModel.page.page_size = String(event.rows);
        this.queryMaterialDate();
    };
    InventoryOverviewComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryInventory(this.queryModel).subscribe(function (data) {
            if (!_this.MaterialData) {
                _this.MaterialData = [];
                _this.selectMaterial = [];
            }
            _this.MaterialData = data.items;
            _this.totalRecords = data.page.total;
        });
    };
    InventoryOverviewComponent.prototype.queryMeterial = function () {
        var _this = this;
        this.inventoryService.queryMaterialInfo().subscribe(function (data) {
            if (data['分类']) {
                _this.categories = data['分类'];
                _this.categories.unshift({ name: '全部', code: '' });
                _this.categories = _this.inventoryService.formatDropdownData(_this.categories);
                console.log(_this.categories);
            }
            else {
                _this.categories = [];
            }
            // this.queryModel.catagory = this.categories[0]['label'];
        });
    };
    InventoryOverviewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-inventory-overview',
            template: __webpack_require__("../../../../../src/app/inventory/inventory-overview/inventory-overview.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/inventory-overview/inventory-overview.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */]])
    ], InventoryOverviewComponent);
    return InventoryOverviewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/inventory-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InventoryRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inventory_overview_inventory_overview_component__ = __webpack_require__("../../../../../src/app/inventory/inventory-overview/inventory-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__outbound_overview_outbound_overview_component__ = __webpack_require__("../../../../../src/app/inventory/outbound-overview/outbound-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__storage_overview_storage_overview_component__ = __webpack_require__("../../../../../src/app/inventory/storage-overview/storage-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__equipment_storage_equipment_storage_component__ = __webpack_require__("../../../../../src/app/inventory/equipment-storage/equipment-storage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__borrow_borrow_component__ = __webpack_require__("../../../../../src/app/inventory/borrow/borrow.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__requisition_application_requisition_application_component__ = __webpack_require__("../../../../../src/app/inventory/requisition-application/requisition-application.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__material_maintenance_material_maintenance_component__ = __webpack_require__("../../../../../src/app/inventory/material-maintenance/material-maintenance.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__flow_chart_flow_chart_component__ = __webpack_require__("../../../../../src/app/inventory/flow-chart/flow-chart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__storage_approved_storage_approved_component__ = __webpack_require__("../../../../../src/app/inventory/storage-approved/storage-approved.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__inventory_location_inventory_location_component__ = __webpack_require__("../../../../../src/app/inventory/inventory-location/inventory-location.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__borrow_management_borrow_management_component__ = __webpack_require__("../../../../../src/app/inventory/borrow-management/borrow-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__borrow_approved_borrow_approved_component__ = __webpack_require__("../../../../../src/app/inventory/borrow-approved/borrow-approved.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__requestion_management_requestion_management_component__ = __webpack_require__("../../../../../src/app/inventory/requestion-management/requestion-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__requeation_approved_requeation_approved_component__ = __webpack_require__("../../../../../src/app/inventory/requeation-approved/requeation-approved.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__requestion_overview_requestion_overview_component__ = __webpack_require__("../../../../../src/app/inventory/requestion-overview/requestion-overview.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















// import {UpdateOutboundQuantityComponent} from "./update-outbound-quantity/update-outbound-quantity.component";
var route = [
    { path: 'inventory', component: __WEBPACK_IMPORTED_MODULE_2__inventory_overview_inventory_overview_component__["a" /* InventoryOverviewComponent */] },
    { path: 'flow', component: __WEBPACK_IMPORTED_MODULE_9__flow_chart_flow_chart_component__["a" /* FlowChartComponent */], children: [
            { path: 'overview', component: __WEBPACK_IMPORTED_MODULE_4__storage_overview_storage_overview_component__["a" /* StorageOverviewComponent */] },
            { path: 'approved', component: __WEBPACK_IMPORTED_MODULE_10__storage_approved_storage_approved_component__["a" /* StorageApprovedComponent */] },
            { path: 'location', component: __WEBPACK_IMPORTED_MODULE_11__inventory_location_inventory_location_component__["a" /* InventoryLocationComponent */] },
            { path: 'equipment', component: __WEBPACK_IMPORTED_MODULE_5__equipment_storage_equipment_storage_component__["a" /* EquipmentStorageComponent */] },
        ] },
    { path: 'borrowManage', component: __WEBPACK_IMPORTED_MODULE_12__borrow_management_borrow_management_component__["a" /* BorrowManagementComponent */], children: [
            { path: 'outbound', component: __WEBPACK_IMPORTED_MODULE_3__outbound_overview_outbound_overview_component__["a" /* OutboundOverviewComponent */] },
            { path: 'borrow', component: __WEBPACK_IMPORTED_MODULE_6__borrow_borrow_component__["a" /* BorrowComponent */] },
            { path: 'borrowApproved', component: __WEBPACK_IMPORTED_MODULE_13__borrow_approved_borrow_approved_component__["a" /* BorrowApprovedComponent */] },
        ] },
    { path: 'requisitionmanage', component: __WEBPACK_IMPORTED_MODULE_14__requestion_management_requestion_management_component__["a" /* RequestionManagementComponent */], children: [
            { path: 'requestionOverview', component: __WEBPACK_IMPORTED_MODULE_16__requestion_overview_requestion_overview_component__["a" /* RequestionOverviewComponent */] },
            { path: 'requisition', component: __WEBPACK_IMPORTED_MODULE_7__requisition_application_requisition_application_component__["a" /* RequisitionApplicationComponent */] },
            { path: 'requisitionapproved', component: __WEBPACK_IMPORTED_MODULE_15__requeation_approved_requeation_approved_component__["a" /* RequeationApprovedComponent */] },
        ] },
    { path: 'material', component: __WEBPACK_IMPORTED_MODULE_8__material_maintenance_material_maintenance_component__["a" /* MaterialMaintenanceComponent */] },
];
var InventoryRoutingModule = (function () {
    function InventoryRoutingModule() {
    }
    InventoryRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["RouterModule"].forChild(route),
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["RouterModule"]]
        })
    ], InventoryRoutingModule);
    return InventoryRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/inventory.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryModule", function() { return InventoryModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_share_module__ = __webpack_require__("../../../../../src/app/shared/share.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inventory_routing_module__ = __webpack_require__("../../../../../src/app/inventory/inventory-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__outbound_overview_outbound_overview_component__ = __webpack_require__("../../../../../src/app/inventory/outbound-overview/outbound-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__storage_overview_storage_overview_component__ = __webpack_require__("../../../../../src/app/inventory/storage-overview/storage-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__inventory_overview_inventory_overview_component__ = __webpack_require__("../../../../../src/app/inventory/inventory-overview/inventory-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__equipment_storage_equipment_storage_component__ = __webpack_require__("../../../../../src/app/inventory/equipment-storage/equipment-storage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__add_storage_add_storage_component__ = __webpack_require__("../../../../../src/app/inventory/add-storage/add-storage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__choose_material_number_choose_material_number_component__ = __webpack_require__("../../../../../src/app/inventory/choose-material-number/choose-material-number.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__borrow_borrow_component__ = __webpack_require__("../../../../../src/app/inventory/borrow/borrow.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__requisition_application_requisition_application_component__ = __webpack_require__("../../../../../src/app/inventory/requisition-application/requisition-application.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__choose_warehouse_manager_choose_warehouse_manager_component__ = __webpack_require__("../../../../../src/app/inventory/choose-warehouse-manager/choose-warehouse-manager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__borrow_items_borrow_items_component__ = __webpack_require__("../../../../../src/app/inventory/borrow-items/borrow-items.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__material_maintenance_material_maintenance_component__ = __webpack_require__("../../../../../src/app/inventory/material-maintenance/material-maintenance.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__add_or_update_material_add_or_update_material_component__ = __webpack_require__("../../../../../src/app/inventory/add-or-update-material/add-or-update-material.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__flow_chart_flow_chart_component__ = __webpack_require__("../../../../../src/app/inventory/flow-chart/flow-chart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__storage_approved_storage_approved_component__ = __webpack_require__("../../../../../src/app/inventory/storage-approved/storage-approved.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__inventory_location_inventory_location_component__ = __webpack_require__("../../../../../src/app/inventory/inventory-location/inventory-location.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__borrow_management_borrow_management_component__ = __webpack_require__("../../../../../src/app/inventory/borrow-management/borrow-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__borrow_approved_borrow_approved_component__ = __webpack_require__("../../../../../src/app/inventory/borrow-approved/borrow-approved.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__requestion_management_requestion_management_component__ = __webpack_require__("../../../../../src/app/inventory/requestion-management/requestion-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__requestion_overview_requestion_overview_component__ = __webpack_require__("../../../../../src/app/inventory/requestion-overview/requestion-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__add_inventory_location_add_inventory_location_component__ = __webpack_require__("../../../../../src/app/inventory/add-inventory-location/add-inventory-location.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__borrow_restart_borrow_restart_component__ = __webpack_require__("../../../../../src/app/inventory/borrow-restart/borrow-restart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__borrow_list_borrow_list_component__ = __webpack_require__("../../../../../src/app/inventory/borrow-list/borrow-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__requeation_approved_requeation_approved_component__ = __webpack_require__("../../../../../src/app/inventory/requeation-approved/requeation-approved.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




























// import { UpdateOutboundQuantityComponent } from './update-outbound-quantity/update-outbound-quantity.component';
var InventoryModule = (function () {
    function InventoryModule() {
    }
    InventoryModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__shared_share_module__["a" /* ShareModule */],
                __WEBPACK_IMPORTED_MODULE_2__inventory_routing_module__["a" /* InventoryRoutingModule */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__inventory_overview_inventory_overview_component__["a" /* InventoryOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_5__storage_overview_storage_overview_component__["a" /* StorageOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_4__outbound_overview_outbound_overview_component__["a" /* OutboundOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_7__equipment_storage_equipment_storage_component__["a" /* EquipmentStorageComponent */],
                __WEBPACK_IMPORTED_MODULE_8__add_storage_add_storage_component__["a" /* AddStorageComponent */],
                __WEBPACK_IMPORTED_MODULE_9__choose_material_number_choose_material_number_component__["a" /* ChooseMaterialNumberComponent */],
                __WEBPACK_IMPORTED_MODULE_10__borrow_borrow_component__["a" /* BorrowComponent */],
                __WEBPACK_IMPORTED_MODULE_11__requisition_application_requisition_application_component__["a" /* RequisitionApplicationComponent */],
                __WEBPACK_IMPORTED_MODULE_12__choose_warehouse_manager_choose_warehouse_manager_component__["a" /* ChooseWarehouseManagerComponent */],
                __WEBPACK_IMPORTED_MODULE_13__borrow_items_borrow_items_component__["a" /* BorrowItemsComponent */],
                __WEBPACK_IMPORTED_MODULE_14__material_maintenance_material_maintenance_component__["a" /* MaterialMaintenanceComponent */],
                __WEBPACK_IMPORTED_MODULE_15__add_or_update_material_add_or_update_material_component__["a" /* AddOrUpdateMaterialComponent */],
                __WEBPACK_IMPORTED_MODULE_16__flow_chart_flow_chart_component__["a" /* FlowChartComponent */],
                __WEBPACK_IMPORTED_MODULE_17__storage_approved_storage_approved_component__["a" /* StorageApprovedComponent */],
                __WEBPACK_IMPORTED_MODULE_18__inventory_location_inventory_location_component__["a" /* InventoryLocationComponent */],
                __WEBPACK_IMPORTED_MODULE_19__borrow_management_borrow_management_component__["a" /* BorrowManagementComponent */],
                __WEBPACK_IMPORTED_MODULE_20__borrow_approved_borrow_approved_component__["a" /* BorrowApprovedComponent */],
                __WEBPACK_IMPORTED_MODULE_21__requestion_management_requestion_management_component__["a" /* RequestionManagementComponent */],
                __WEBPACK_IMPORTED_MODULE_22__requestion_overview_requestion_overview_component__["a" /* RequestionOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_23__add_inventory_location_add_inventory_location_component__["a" /* AddInventoryLocationComponent */],
                __WEBPACK_IMPORTED_MODULE_24__borrow_restart_borrow_restart_component__["a" /* BorrowRestartComponent */],
                __WEBPACK_IMPORTED_MODULE_25__borrow_list_borrow_list_component__["a" /* BorrowListComponent */],
                __WEBPACK_IMPORTED_MODULE_26__requeation_approved_requeation_approved_component__["a" /* RequeationApprovedComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_3__inventory_service__["a" /* InventoryService */], __WEBPACK_IMPORTED_MODULE_27__services_public_service__["a" /* PublicService */]]
        })
    ], InventoryModule);
    return InventoryModule;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/inventory.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InventoryService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import {Http,Response} from "@angular/http";



var InventoryService = (function () {
    function InventoryService(http, storageService) {
        this.http = http;
        this.storageService = storageService;
    }
    //获取当前登录人接口
    InventoryService.prototype.queryPersonal = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/personnel", {
            "access_token": token,
            "type": "get_personnel_user",
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    // 添加物料接口
    InventoryService.prototype.addMaterial = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "material_add",
            "data": {
                "pn": material.pn,
                "name": material.name,
                "catagory": material.catagory,
                "supplier": material.supplier,
                "brand": material.brand,
                "model": material.model,
                "specification": material.specification,
                "unit_cost": material.unit_cost,
                "threshold_group": material.threshold_group,
                "status": material.status
            }
        }).map(function (res) {
            // if (res['errcode'] !== '00000') {
            //   // throw new Error(res['errmsg']);
            //   let reg = /No datas/;
            //   if (res['errmsg'].search(reg) !== -1) {
            //     return []
            //   }
            // }
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //  修改物料接口
    InventoryService.prototype.updateMaterial = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "material_mod",
            "data": {
                "pn": material.pn,
                "name": material.name,
                "catagory": material.catagory,
                "supplier": material.supplier,
                "brand": material.brand,
                "model": material.model,
                "specification": material.specification,
                "unit_cost": material.unit_cost,
                "threshold_group": material.threshold_group,
                "status": material.status
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //查询物料接口
    InventoryService.prototype.queryMaterials = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "material_get",
            "data": {
                "condition": {
                    "pn": material.condition.pn.trim(),
                    "name": material.condition.name.trim(),
                    "status": material.condition.status.trim(),
                },
                "page": {
                    "page_size": material.page.page_size.trim(),
                    "page_number": material.page.page_number.trim()
                }
            }
        }).map(function (res) {
            // let body = res.json();
            // if(body.errcode !== '00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.data;
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //查询物料接口
    InventoryService.prototype.queryMaterial = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "material_get",
            "data": {
                "condition": {
                    "pn": material.condition.pn,
                    "name": material.condition.name,
                },
                "page": {
                    "page_size": material.page.page_size.trim(),
                    "page_number": material.page.page_number.trim()
                }
            }
        }).map(function (res) {
            // let body = res.json();
            // if(body.errcode !== '00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.data;
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //删除物料接口
    InventoryService.prototype.deleteInventory = function (ids) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "material_del",
            "ids": ids
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //   throw new Error(body.errmsg);
            // }
            // return body.data
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //库房管理配置信息获取接口
    InventoryService.prototype.queryMaterialInfo = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/foundationdata", {
            "access_token": token,
            "type": "inventory_config_get",
            "ids": [
                "规格",
                "库存位置",
                "分类",
                "阈值分组",
                "安全库存",
            ]
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //入库领用紧急程度
    InventoryService.prototype.queryMaterialInfocater = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/foundationdata", {
            "access_token": token,
            "type": "inventory_config_get",
            "ids": [
                "紧急度"
            ]
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //入库总览流程图接口
    InventoryService.prototype.queryMaterialChar = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_workflow_get",
            "data": {
                "status": material.status.trim(),
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //借用总览流程图接口
    InventoryService.prototype.queryMaterialBorrow = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "borrow_workflow_get",
            "data": {
                "status": material.status.trim(),
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //领用总览流程图接口
    InventoryService.prototype.queryMaterialreceive = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "receive_workflow_get",
            "data": {
                "status": material.status.trim(),
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //查询库存总览分类接口
    InventoryService.prototype.queryStoragecatorgroy = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/foundationdata", {
            "access_token": token,
            "type": "inventory_config_gettree",
            "id": "分类"
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //库存总览接口
    InventoryService.prototype.queryInventory = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "inventory_get",
            "data": {
                "condition": {
                    "sid": material.condition.sid.trim(),
                    "name": material.condition.name.trim(),
                    "catagory": material.condition.catagory.trim(),
                    "count": material.condition.count.trim(),
                    "status": material.condition.status.trim(),
                },
                "page": {
                    "page_size": material.page.page_size.trim(),
                    "page_number": material.page.page_number.trim()
                }
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    // 入库总览所有状态获取接口
    InventoryService.prototype.queryInventoryStatus = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_statuslist_get",
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    // 借用总览所有状态获取接口
    InventoryService.prototype.queryBorrowStatus = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "borrow_statuslist_get",
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    // 领用总览所有状态获取接口
    InventoryService.prototype.queryReceiveStatus = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "receive_statuslist_get",
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //入库总览接口
    InventoryService.prototype.queryStorageView = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_get",
            "data": {
                "condition": {
                    "sid": material.condition.sid.trim(),
                    "status": material.condition.status.trim(),
                    "begin_time": material.condition.begin_time.trim(),
                    "end_time": material.condition.end_time.trim(),
                    "creator": material.condition.creator.trim(),
                },
                "page": {
                    "page_size": material.page.page_size.trim(),
                    "page_number": material.page.page_number.trim()
                }
            }
        }).map(function (res) {
            // let body = res.json();
            // if(body.errcode !== '00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.data;
            if (res['errcode'] !== '00000') {
                // throw new Error(res['errmsg']);
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //入库申请新增接口
    InventoryService.prototype.addStorage = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_add",
            "data": {
                "sid": storage.sid,
                "location": storage.location,
                "location_id": storage.location_id,
                "keeper": storage.keeper,
                "keeper_pid": storage.keeper_pid,
                "remarks": storage.remarks,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //  入库申请修改接口
    InventoryService.prototype.updateStorage = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_mod",
            "data": {
                "sid": storage.sid,
                "create_time": storage.create_time,
                "location": storage.location,
                "location_id": storage.location_id,
                "keeper": storage.keeper,
                "keeper_pid": storage.keeper_pid,
                "status": storage.status,
                "creator": storage.creator,
                "creator_pid": storage.creator_pid,
                "approve_remarks": storage.approve_remarks,
                "audit_time": storage.audit_time,
                "entering_time": storage.entering_time,
                "remarks": storage.remarks,
                "begin_time": storage.begin_time,
                "end_time": storage.end_time,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            // let body = res.json();
            // if(body.errcode !== '00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.data
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //入库申请删除接口
    InventoryService.prototype.deleteStorage = function (id) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_del",
            "ids": [id]
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //   throw new Error(body.errmsg);
            // }
            // return body.data
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //查询入库申请接口
    InventoryService.prototype.queryStorage = function (id) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_get_byid",
            "id": id
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //入库申请保存接口
    InventoryService.prototype.queryStorageSave = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_save",
            "data": {
                "sid": storage.sid,
                "location": storage.location,
                "location_id": storage.location_id,
                "keeper": storage.keeper,
                "keeper_pid": storage.keeper_pid,
                "remarks": storage.remarks,
                // "time_purchase": storage.time_purchase,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //库存位置树接口
    //查询入库申请接口
    InventoryService.prototype.queryStorageLocationTree = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/foundationdata", {
            "access_token": token,
            "type": "inventory_config_gettree",
            "id": "库存位置"
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //入库审批接口
    InventoryService.prototype.queryStorageApproved = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_audit",
            "data": {
                "sid": storage.sid,
                "approve_remarks": storage.approve_remarks,
                "status": storage.status
            }
        }).map(function (res) {
            // let body = res.json();
            // if(body.errcode !== '00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.data;
            // if(res['errcode'] !== '00000'){
            //   throw new Error(res['errmsg'])
            // }
            if (res['errcode'] !== '00000') {
                // throw new Error(res['errmsg']);
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //确认入库接口
    InventoryService.prototype.queryStorageSure = function (sid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_act",
            "data": {
                "sid": sid,
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //出库总览接口
    InventoryService.prototype.queryOutband = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "borrow_get",
            "data": {
                "condition": {
                    "sid": material.condition.sid,
                    "location": material.condition.location,
                    "status": material.condition.status,
                    "begin_time": material.condition.begin_time,
                    "end_time": material.condition.end_time,
                },
                "page": {
                    "page_size": material.page.page_size.trim(),
                    "page_number": material.page.page_number.trim()
                }
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //出库总览所有接口
    InventoryService.prototype.queryAllOutband = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_statuslist_get",
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //确认出库接口
    InventoryService.prototype.queryBorrowSure = function (data) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_act",
            "data": {
                "sid": data.sid,
                "inventories": data.inventories,
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
                else {
                    throw new Error(res['errmsg']);
                }
            }
            return res['data'];
        });
    };
    //出库删除接口
    InventoryService.prototype.deleteDelivery = function (ids) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_del",
            "ids": ids
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //出库总览所有状态获取接口
    InventoryService.prototype.queryOutbandStatus = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_statuslist_get",
        }).map(function (res) {
            // let body = res.json();
            // if(body.errcode !== '00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.data;
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //出库流程获取接口
    InventoryService.prototype.queryOutbandChar = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_workflow_get",
            "data": {
                "type": material.type,
                "status": material.status
            }
        }).map(function (res) {
            // let body = res.json();
            // if(body.errcode !== '00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.data;
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //出库审批接口
    InventoryService.prototype.queryOutboundApproved = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_audit",
            "data": {
                "sid": storage.sid,
                "approve_remarks": storage.approve_remarks,
                "status": storage.status
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //查询出库申请接口
    InventoryService.prototype.queryOutbound = function (id) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_get_byid",
            "id": id
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //待我处理出库总览接口
    InventoryService.prototype.queryOutbandDeal = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_get_byowner",
            "data": {
                "condition": {
                    "status": material.condition.status.trim(),
                },
                "page": {
                    "page_size": material.page.page_size.trim(),
                    "page_number": material.page.page_number.trim()
                }
            }
        }).map(function (res) {
            // let body = res.json();
            // if(body.errcode !== '00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.data;
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //借用申请保存接口
    InventoryService.prototype.queryDeliverySave = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_save",
            "data": {
                "sid": storage.sid,
                "type": storage.type,
                "return_time": storage.return_time,
                "urgency": storage.urgency,
                "remarks": storage.remarks,
                "create_time": storage.create_time,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //	借用申请新增接口
    InventoryService.prototype.addDelivery = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_add",
            "data": {
                "sid": storage.sid,
                "type": storage.type,
                "return_time": storage.return_time,
                "create_time": storage.create_time,
                "urgency": storage.urgency,
                "remarks": storage.remarks,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //借用申请修改接口
    InventoryService.prototype.queryDeliveryMod = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_mod",
            "data": {
                "sid": storage.sid,
                "type": storage.type,
                "org": storage.org,
                "org_oid": storage.org_oid,
                "approver": storage.approver,
                "approver_pid": storage.approver_pid,
                "approve_remarks": storage.approve_remarks,
                "keeper": storage.keeper,
                "keeper_pid": storage.keeper_pid,
                "create_time": storage.create_time,
                "return_time": storage.return_time,
                "audit_time": storage.audit_time,
                "creator": storage.creator,
                "creator_pid": storage.creator_pid,
                "urgency": storage.urgency,
                "status": storage.status,
                "remarks": storage.remarks,
                "delivery_time": storage.delivery_time,
                "begin_time": storage.begin_time,
                "end_time": storage.end_time,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //借用申请查看接口
    InventoryService.prototype.borrowShow = function (sid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_get_byid",
            "id": sid
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //续借接口
    InventoryService.prototype.restartqueryDelivery = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_renew",
            "data": {
                "sid": storage.sid,
                "return_time": storage.return_time,
                "renew_reason": storage.renew_reason
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //全部借用归还接口
    InventoryService.prototype.DeliveryReturn = function (sid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_return",
            "data": {
                "sid": sid,
                "inventories": []
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //部分借用归还接口
    InventoryService.prototype.someDeliveryReturn = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_return",
            "data": {
                "sid": storage.sid,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //	领用申请新增接口
    InventoryService.prototype.requestionAddDelivery = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_add",
            "data": {
                "sid": storage.sid,
                "type": storage.type,
                "create_time": storage.create_time,
                "urgency": storage.urgency,
                "remarks": storage.remarks,
                "approver": storage.approver,
                "approver_pid": storage.approver_pid,
                "org_approver ": storage.org_approver,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //领用申请修改接口
    InventoryService.prototype.updateDelivery = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_mod",
            "data": {
                "type": storage.type,
                "org": storage.org,
                "org_oid": storage.org_oid,
                "approver": storage.approver,
                "approver_pid": storage.approver_pid,
                "operate_time": storage.operate_time,
                "return_time": storage.return_time,
                "operator": storage.operator,
                "operator_pid": storage.operator_pid,
                "urgency": storage.urgency,
                "remarks": storage.remarks,
                "inventoris": [
                    {
                        "pn": storage.pn,
                        "name": storage.name,
                        "catagory": storage.catagory,
                        "status": storage.status,
                        "model": storage.model,
                        "brand": storage.brand,
                        "specification": storage.specification,
                        "location": storage.location,
                        "keeper": storage.keeper,
                        "keeper_pid": storage.keeper_pid,
                        "count": storage.count
                    }
                ]
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //领用申请查询接口
    InventoryService.prototype.queryDelivery = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_get_byid",
            "id": material.id
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //领用总览接口
    InventoryService.prototype.queryRequestionView = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "receive_get",
            "data": {
                "condition": {
                    "sid": material.condition.sid,
                    "status": material.condition.status,
                    "begin_time": material.condition.begin_time,
                    "end_time": material.condition.end_time,
                    "location": material.condition.location,
                    "org": material.condition.org,
                    "creator": material.condition.creator,
                },
                "page": {
                    "page_size": material.page.page_size.trim(),
                    "page_number": material.page.page_number.trim()
                }
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                // throw new Error(res['errmsg']);
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    //领用申请保存接口
    InventoryService.prototype.queryRequesttionDeliverySave = function (storage) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_save",
            "data": {
                "sid": storage.sid,
                "type": storage.type,
                "urgency": storage.urgency,
                "remarks": storage.remarks,
                "approver": storage.approver,
                "approver_pid": storage.approver_pid,
                "create_time": storage.create_time,
                "inventories": storage.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //入库位置调整接口
    InventoryService.prototype.modifiedLocation = function (material) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "entering_mod_location",
            "data": {
                "sid": material.sid,
                "inventories": material.inventories
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //执行出库查看接口
    InventoryService.prototype.outboundShow = function (sid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/inventory", {
            "access_token": token,
            "type": "delivery_get_withonhandquantity_byid",
            "id": sid
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
            }
            return res['data'];
        });
    };
    InventoryService.prototype.deepClone = function (obj) {
        var o, i, j, k;
        if (typeof (obj) != "object" || obj === null)
            return obj;
        if (obj instanceof (Array)) {
            o = [];
            i = 0;
            j = obj.length;
            for (; i < j; i++) {
                if (typeof (obj[i]) == "object" && obj[i] != null) {
                    o[i] = this.deepClone(obj[i]);
                }
                else {
                    o[i] = obj[i];
                }
            }
        }
        else {
            o = {};
            for (i in obj) {
                if (typeof (obj[i]) == "object" && obj[i] != null) {
                    o[i] = this.deepClone(obj[i]);
                }
                else {
                    o[i] = obj[i];
                }
            }
        }
        return o;
    };
    InventoryService.prototype.formatDropdownData = function (arr) {
        var newarr = [];
        for (var i = 0; i < arr.length; i++) {
            var obj = {};
            var temp = arr[i];
            obj['label'] = temp['name'];
            obj['value'] = temp['code'];
            newarr.push(obj);
        }
        return newarr;
    };
    InventoryService.prototype.formatDropdownDatas = function (arr) {
        var newarr = [];
        for (var i = 0; i < arr.length; i++) {
            var obj = {};
            var temp = arr[i];
            obj['label'] = temp['name'];
            obj['value'] = temp['name'];
            newarr.push(obj);
        }
        return newarr;
    };
    InventoryService.prototype.changeObjectName = function (array, newName, oldName) {
        for (var i in array) {
            array[i][newName] = array[i][oldName];
            delete array[i][oldName];
            this.changeObjectName(array[i].children, newName, oldName);
        }
        return array;
    };
    InventoryService.prototype.getFormatTime = function (date) {
        if (!date) {
            date = new Date();
        }
        else {
            date = new Date(date);
        }
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        var hour = date.getHours();
        var minutes = date.getMinutes();
        var second = date.getSeconds();
        day = day <= 9 ? "0" + day : day;
        month = month <= 9 ? "0" + month : month;
        hour = hour <= 9 ? "0" + hour : hour;
        minutes = minutes <= 9 ? "0" + minutes : minutes;
        second = second <= 9 ? "0" + second : second;
        return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + second;
    };
    InventoryService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */]])
    ], InventoryService);
    return InventoryService;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/material-maintenance/material-maintenance.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n  <div>\n    <span class=\"feature-title\">库存管理<span class=\"gt\">&gt;</span>物料维护  </span>\n  </div>\n</div>\n<div class=\"content-section implementation GridDemo clearfixes material\">\n  <div class=\"mysearch\">\n    <div class=\"ui-g\">\n      <button pButton type=\"button\"  label=\"新增\"  (click)=\"showMaterialMask()\" ></button>\n      <button pButton type=\"button\"  label=\"编辑\"  (click)=\"updateOption()\" [disabled]=\"selectMaterial.length===0||selectMaterial.length>1\"></button>\n      <button pButton type=\"button\"  label=\"删除\"  (click)=\"deleteMaterial()\" [disabled]=\"selectMaterial.length === 0\"></button>\n      <button pButton type=\"button\"  label=\"启用\"  (click)=\"enabled()\" [disabled]=\"selectMaterial.length===0|| selectMaterial.length>1 \"  ></button>\n      <button pButton type=\"button\"  label=\"冻结\"  (click)=\"freeze()\" [disabled]=\"selectMaterial.length===0 ||selectMaterial.length>1 \" ></button>\n\n    </div>\n  </div>\n\n    <div class=\"mybutton ui-g-12 ui-md-8 ui-lg-8\">\n\n    </div>\n\n\n  <div style=\"clear: both\"></div>\n  <p-dataTable [value]=\"MaterialData\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectMaterial\">\n    <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"  [sortable]=\"true\"></p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n    <!--{{selectMaterial}}-->\n  </p-dataTable>\n  <p-paginator rows=\"10\" [totalRecords]=\"totalRecords\" [rowsPerPageOptions]=\"[10,20,30]\" (onPageChange)=\"paginate($event)\"></p-paginator>\n\n</div>\n<app-add-or-update-material\n  *ngIf=\"showMaterial\"\n  (closeMaterial)=\"closeMaterialMask($event)\"\n  [currentMeterial]=\"currentMaterial\"\n  (addMaterial)=\"addMaterial($event)\"\n  [state]=\"state\"\n  (updateDev)=\"updateMaterial($event)\"\n></app-add-or-update-material>\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/material-maintenance/material-maintenance.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".mysearch {\n  margin-bottom: 20px;\n  background: #fff;\n  min-height: 70px;\n  padding-top: 20px;\n  padding-right: 20px; }\n\n.material /deep/ .ui-datatable th.ui-state-active {\n  color: #555555;\n  background-color: #EFF0F3; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/material-maintenance/material-maintenance.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaterialMaintenanceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MaterialMaintenanceComponent = (function (_super) {
    __extends(MaterialMaintenanceComponent, _super);
    function MaterialMaintenanceComponent(inventoryService, confirmationService, messageService) {
        var _this = _super.call(this, confirmationService, messageService) || this;
        _this.inventoryService = inventoryService;
        _this.confirmationService = confirmationService;
        _this.messageService = messageService;
        _this.showMaterial = false;
        _this.chooseCids = [];
        _this.selectMaterial = [];
        return _this;
    }
    MaterialMaintenanceComponent.prototype.ngOnInit = function () {
        this.queryModel = {
            "condition": {
                "pn": "",
                "name": "",
            },
            "page": {
                "page_size": "10",
                "page_number": "1"
            }
        };
        this.cols = [
            { field: 'pn', header: '物品编号' },
            { field: 'name', header: '物品名称' },
            { field: 'catagory', header: '分类' },
            { field: 'supplier', header: '供应商' },
            { field: 'brand', header: '品牌' },
            { field: 'model', header: '型号' },
            { field: 'specification', header: '规格' },
            { field: 'status', header: '状态' },
            { field: 'threshold_group', header: '安全库存' },
        ];
        this.queryMaterialDate();
    };
    MaterialMaintenanceComponent.prototype.showMaterialMask = function () {
        this.state = 'add';
        this.showMaterial = !this.showMaterial;
    };
    MaterialMaintenanceComponent.prototype.closeMaterialMask = function (bool) {
        this.showMaterial = (bool);
    };
    //添加成功
    MaterialMaintenanceComponent.prototype.addMaterial = function (bool) {
        this.showMaterial = bool;
        this.queryMaterialDate();
    };
    //同步更新页面数据
    MaterialMaintenanceComponent.prototype.updateMaterial = function () {
        this.selectMaterial = [];
        this.queryMaterialDate();
    };
    MaterialMaintenanceComponent.prototype.updateOption = function () {
        this.state = 'update';
        this.showMaterial = !this.showMaterial;
        this.currentMaterial = this.selectMaterial[0];
    };
    //分页查询
    MaterialMaintenanceComponent.prototype.paginate = function (event) {
        this.chooseCids = [];
        this.queryModel.page.page_number = String(++event.page);
        this.queryModel.page.page_size = String(event.rows);
        this.queryMaterialDate();
    };
    MaterialMaintenanceComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryMaterial(this.queryModel).subscribe(function (data) {
            _this.MaterialData = data.items;
            for (var i = 0; i < _this.MaterialData.length; i++) {
                _this.Materialstatus = _this.MaterialData[i].status;
            }
            _this.totalRecords = data.page.total;
            _this.selectMaterial = [];
            if (!data['items']) {
                _this.MaterialData = [];
                _this.selectMaterial = [];
                // this.totalRecords = 0;
            }
        }, function (err) {
            _this.toastError(err);
        });
    };
    MaterialMaintenanceComponent.prototype.deleteMaterial = function () {
        var _this = this;
        this.chooseCids = [];
        for (var i = 0; i < this.selectMaterial.length; i++) {
            this.chooseCids.push(this.selectMaterial[i]['pn']);
        }
        this.confirmationService.confirm({
            message: '确认删除吗?',
            accept: function () {
                _this.inventoryService.deleteInventory(_this.chooseCids).subscribe(function () {
                    _this.queryMaterialDate();
                    _this.selectMaterial = [];
                }, function (err) {
                    var message;
                    if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                        message = '似乎网络出现了问题，请联系管理员或稍后重试';
                    }
                    else {
                        message = err;
                    }
                    _this.confirmationService.confirm({
                        message: message,
                        rejectVisible: false,
                    });
                });
            },
            reject: function () {
            }
        });
        //请求成功后删除本地数据
    };
    MaterialMaintenanceComponent.prototype.enabled = function (current) {
        var _this = this;
        console.log(current);
        this.currentMaterial = this.selectMaterial[0];
        this.currentMaterial.status = "启用";
        this.inventoryService.updateMaterial(this.currentMaterial).subscribe(function () {
            _this.queryMaterialDate();
            _this.alert('启用成功');
        });
    };
    MaterialMaintenanceComponent.prototype.freeze = function () {
        var _this = this;
        this.currentMaterial = this.selectMaterial[0];
        this.currentMaterial.status = "冻结";
        this.inventoryService.updateMaterial(this.currentMaterial).subscribe(function () {
            _this.queryMaterialDate();
            _this.alert('冻结成功');
        });
    };
    MaterialMaintenanceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-material-maintenance',
            template: __webpack_require__("../../../../../src/app/inventory/material-maintenance/material-maintenance.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/material-maintenance/material-maintenance.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__["MessageService"]])
    ], MaterialMaintenanceComponent);
    return MaterialMaintenanceComponent;
}(__WEBPACK_IMPORTED_MODULE_3__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/inventory/outbound-overview/outbound-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes outbound\">\n  <div class=\"mysearch\">\n   <div class=\"ui-g\">\n    <div class=\"ui-g-3 ui-sm-12\">\n      <label class=\"ui-g-3 ui-sm-5 text-right\" >借用单号:</label>\n      <div class=\"ui-g-9  ui-sm-7 ui-fluid\">\n        <input pInputText type=\"text\" name=\"sid\" placeholder=\"借用单号\" [(ngModel)]=\"queryModel.condition.sid\" />\n      </div>\n    </div>\n    <div class=\"ui-g-3 ui-sm-12\">\n      <label class=\"ui-g-3 ui-sm-5 text-right\">部门名称:</label>\n      <div class=\"ui-g-9  ui-sm-7 ui-fluid\">\n        <input pInputText type=\"text\" name=\"sid\" placeholder=\"部门名称\" [(ngModel)]=\"queryModel.condition.org\" />\n      </div>\n    </div>\n    <div class=\"ui-g-2 ui-sm-12\">\n      <label  class=\"ui-g-3 ui-sm-5 text-right\">状态:</label>\n      <div class=\"ui-g-9  ui-sm-9 ui-fluid \">\n        <p-dropdown [options]=\"MaterialStatusData\" [(ngModel)]=\"queryModel.condition.status\" [autoWidth]=\"false\" name=\"cycle\" (onChange)=\"suggestInsepectiones()\" ></p-dropdown>\n      </div>\n    </div>\n    <div class=\"ui-g-2 ui-sm-12\">\n      <div class=\"ui-g-12 ui-sm-12 option\">\n        <button pButton type=\"button\" label=\"查询\" ngClass=\"ui-sm-12\" (click)=\"suggestInsepectiones()\" ></button>\n        <button pButton label=\"清空\" ngClass=\"ui-sm-12\" (click)=\"clearSearch()\"></button>\n      </div>\n    </div>\n  </div>\n  </div>\n  <p-dataTable [value]=\"MaterialData\"\n               [totalRecords]=\"totalRecords\"  [(selection)]=\"selectMaterial\">\n    <p-column  field=\"sid\" header=\"借用单号\" [sortable]=\"true\" >\n      <ng-template let-data=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n        <span (click)=\"viewBorrowApproved(data)\" class=\"curser\">{{data.sid}}</span>\n      </ng-template>\n    </p-column>\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"  [sortable]=\"true\"></p-column>\n    <p-column header=\"操作\">\n      <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n        <button pButton type=\"button\"  label=\"编辑\"  (click)=\"updateOption(MaterialData[i])\" *ngIf=\"MaterialData[i]['status'] === '新建' ||MaterialData[i]['status'] === '驳回'\" ></button>\n        <button pButton type=\"button\"  label=\"审批\"  (click)=\" borrowApproved(MaterialData[i])\" *ngIf=\"MaterialData[i]['status'] === '待审批'\"  ></button>\n        <button pButton type=\"button\"  label=\"出库\" (click)=\"updateOutboundQuantity(MaterialData[i])\" *ngIf=\"MaterialData[i]['status'] === '待借用'\"></button>\n        <button pButton type=\"button\"  label=\"续借\" (click)=\"showRestartMask(MaterialData[i],i)\" *ngIf=\"MaterialData[i]['status'] === '待归还'\" ></button>\n        <button pButton type=\"button\"  label=\"部分归还\"   (click)=\"someDeliveryReturn(MaterialData[i],i)\" *ngIf=\"MaterialData[i]['status'] === '待归还'\"></button>\n        <button pButton type=\"button\"  label=\"全部归还\"   (click)=\"deliveryReturn(MaterialData[i],i)\" *ngIf=\"MaterialData[i]['status'] === '待归还'\"></button>\n        <button pButton type=\"button\"  label=\"删除\" (click)=\"deleteMaterial(MaterialData[i])\"  *ngIf=\"MaterialData[i]['status'] === '新建'\"  ></button>\n        <button pButton type=\"button\"  label=\"查看\"   (click)=\" viewBorrowApproved(MaterialData[i])\"  ></button>\n      </ng-template>\n    </p-column>\n    <ng-template pTemplate=\"emptymessage\">\n      当前没有数据\n    </ng-template>\n  </p-dataTable>\n  <p-paginator rows=\"10\" [totalRecords]=\"totalRecords\" [rowsPerPageOptions]=\"[10,20,30]\" (onPageChange)=\"paginate($event)\"></p-paginator>\n</div>\n\n<app-borrow-restart\n  *ngIf=\"showRestart\"\n  (closeRestart)=\"closeRestartMask($event)\"\n  (updateDev)=\"updateInventoryModel($event)\"\n  [currentBorrow]=\"currentBorrow\"\n></app-borrow-restart>\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/outbound-overview/outbound-overview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (min-width: 1024px) {\n  .outbound /deep/ table thead tr th:last-child {\n    width: 22%; }\n  .outbound /deep/ table thead tr th:nth-child(7) {\n    width: 15%; }\n  .outbound /deep/ table thead tr th:nth-child(2) {\n    width: 8%; } }\n\n.mysearch {\n  margin-bottom: 20px;\n  padding: 10px;\n  background: #fff; }\n\n.outbound /deep/ .ui-datatable th.ui-state-active {\n  background: #39b9c6;\n  color: black; }\n\n.curser {\n  color: #39b9c6;\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/outbound-overview/outbound-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OutboundOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var OutboundOverviewComponent = (function (_super) {
    __extends(OutboundOverviewComponent, _super);
    function OutboundOverviewComponent(inventoryService, confirmationService, messageService, router, route, storageService, eventBusService) {
        var _this = _super.call(this, confirmationService, messageService) || this;
        _this.inventoryService = inventoryService;
        _this.confirmationService = confirmationService;
        _this.messageService = messageService;
        _this.router = router;
        _this.route = route;
        _this.storageService = storageService;
        _this.eventBusService = eventBusService;
        _this.chooseCids = [];
        _this.selectMaterial = [];
        _this.showRestart = false;
        return _this;
    }
    OutboundOverviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            dayNamesShort: ["一", "二", "三", "四", "五", "六", "七"],
            dayNamesMin: ["一", "二", "三", "四", "五", "六", "七"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
        };
        this.statusNames = [
            { label: '全部', value: '' },
            { label: '新建', value: '新建' },
            { label: '待审批', value: '待审批' },
            { label: '待借用', value: '待借用' },
            { label: '待归还', value: '借用待归还' },
            { label: '已归还', value: '已归还' },
            { label: '拒绝', value: '拒绝' },
            { label: '驳回', value: '驳回' },
        ];
        this.queryModel = {
            "condition": {
                "sid": "",
                "location": "",
                "status": "",
                "begin_time": "",
                "end_time": ""
            },
            "page": {
                "page_size": "10",
                "page_number": "1"
            }
        };
        this.cols = [
            // {field: 'sid', header: '借用单号'},
            // {field: 'type', header: '申请类型'},
            { field: 'creator', header: '借用人' },
            { field: 'org', header: '部门名称' },
            { field: 'status', header: '状态' },
            { field: 'approver', header: '审批人' },
            { field: 'audit_time', header: '审批时间' },
            { field: 'return_time', header: '归还时间' },
        ];
        this.queryMaterialDate();
        this.eventBusService.updaterequestion.subscribe(function (data) {
            _this.queryModel.condition.status = data;
            _this.queryMaterialDate();
        });
        this.queryMaterialStatusDate();
    };
    OutboundOverviewComponent.prototype.updateOption = function (current) {
        var sid = current['sid'];
        this.eventBusService.borrow.next(current.status);
        this.eventBusService.borrow.next(current.status);
        this.router.navigate(['../borrow'], { queryParams: { sid: sid, state: 'update', title: '编辑' }, relativeTo: this.route });
    };
    OutboundOverviewComponent.prototype.clearSearch = function () {
        this.queryModel.condition.begin_time = '';
        this.queryModel.condition.end_time = '';
        this.queryModel.condition.sid = '';
        this.queryModel.condition.org = '';
    };
    //查看
    OutboundOverviewComponent.prototype.viewBorrowApproved = function (current) {
        var sid = current['sid'];
        this.eventBusService.borrow.next(current.status);
        this.router.navigate(['../borrow'], { queryParams: { sid: sid, state: 'viewDetail', title: '查看' }, relativeTo: this.route });
    };
    //搜索功能
    OutboundOverviewComponent.prototype.suggestInsepectiones = function () {
        this.queryModel.page.page_number = '1';
        this.queryModel.page.page_size = '10';
        this.queryMaterialDate();
    };
    //分页查询
    OutboundOverviewComponent.prototype.paginate = function (event) {
        this.chooseCids = [];
        this.queryModel.page.page_number = String(++event.page);
        this.queryModel.page.page_size = String(event.rows);
        this.queryMaterialDate();
    };
    OutboundOverviewComponent.prototype.queryMaterialStatusDate = function () {
        var _this = this;
        this.inventoryService.queryBorrowStatus().subscribe(function (data) {
            if (!data) {
                _this.MaterialStatusData = [];
            }
            else {
                _this.MaterialStatusData = data;
                _this.MaterialStatusData = _this.inventoryService.formatDropdownData(_this.MaterialStatusData);
                console.log(_this.MaterialStatusData);
            }
        });
    };
    OutboundOverviewComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryOutband(this.queryModel).subscribe(function (data) {
            if (!data['items']) {
                _this.MaterialData = [];
            }
            else {
                _this.MaterialData = data.items;
                console.log(_this.MaterialData);
                _this.totalRecords = data.page.total;
            }
        });
    };
    OutboundOverviewComponent.prototype.deleteMaterial = function (current) {
        var _this = this;
        // this.chooseCids = [];
        // for(let i = 0;i<this.selectMaterial.length;i++){
        //   this.chooseCids.push(this.selectMaterial[i]['sid'])
        // }
        //请求成功后删除本地数据
        this.chooseCids = [];
        this.chooseCids.push(current['sid']);
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: function () {
                _this.inventoryService.deleteDelivery(_this.chooseCids).subscribe(function () {
                    _this.queryMaterialDate();
                }, function (err) {
                    var message;
                    if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                        message = '似乎网络出现了问题，请联系管理员或稍后重试';
                    }
                    else {
                        message = err;
                    }
                    _this.confirmationService.confirm({
                        message: message,
                        rejectVisible: false,
                    });
                });
            },
            reject: function () {
            }
        });
    };
    //审批
    OutboundOverviewComponent.prototype.borrowApproved = function (current) {
        var sid = current['sid'];
        var status = current['status'];
        this.eventBusService.borrow.next(current.status);
        this.router.navigate(['../borrowApproved'], { queryParams: { sid: sid, status: status, title: '审批' }, relativeTo: this.route });
    };
    //借用归还
    OutboundOverviewComponent.prototype.deliveryReturn = function (current) {
        var _this = this;
        var sid = current['sid'];
        this.eventBusService.borrow.next(current.status);
        this.confirmationService.confirm({
            message: '是否全部归还?',
            rejectVisible: true,
            accept: function () {
                _this.inventoryService.DeliveryReturn(sid).subscribe(function (data) {
                    _this.selectMaterial = [];
                    _this.queryMaterialDate();
                }, function (err) {
                    _this.toastError(err);
                });
            },
            reject: function () { }
        });
    };
    //部分借用归还
    OutboundOverviewComponent.prototype.someDeliveryReturn = function (current) {
        var sid = current['sid'];
        this.eventBusService.borrow.next(current.status);
        this.router.navigate(['../borrow'], { queryParams: { sid: sid, state: 'someReturn', title: '部分归还' }, relativeTo: this.route });
    };
    //出库
    OutboundOverviewComponent.prototype.updateOutboundQuantity = function (current) {
        var sid = current['sid'];
        this.eventBusService.borrow.next(current.status);
        this.router.navigate(['../borrow'], { queryParams: { sid: sid, state: 'outbound', title: '出库' }, relativeTo: this.route });
    };
    OutboundOverviewComponent.prototype.showRestartMask = function (borrow) {
        this.currentBorrow = borrow;
        this.showRestart = !this.showRestart;
    };
    OutboundOverviewComponent.prototype.updateInventoryModel = function (bool) {
        this.showRestart = bool;
        this.queryMaterialDate();
    };
    OutboundOverviewComponent.prototype.closeRestartMask = function (bool) {
        this.showRestart = bool;
    };
    OutboundOverviewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-outbound-overview',
            template: __webpack_require__("../../../../../src/app/inventory/outbound-overview/outbound-overview.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/outbound-overview/outbound-overview.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__["MessageService"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_4__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_5__services_event_bus_service__["a" /* EventBusService */]])
    ], OutboundOverviewComponent);
    return OutboundOverviewComponent;
}(__WEBPACK_IMPORTED_MODULE_6__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/inventory/requeation-approved/requeation-approved.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/requeation-approved/requeation-approved.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes\">\n  <form class=\"form-horizontal\" [formGroup]=\"newInspectionPlan\"  >\n    <p-panel >\n      <p-header>\n        <div class=\"ui-helper-clearfix\">\n          <button  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\" [ngStyle]=\"{'width':'75px'}\" ></button>\n          <button [ngStyle]=\"{'float':'right'}\" pButton  *ngFor=\"let optbutton of optButtons\" (click)=\"approvedOption(optbutton.status)\" label=\"{{optbutton.label}}\" [disabled]=\"!newInspectionPlan.valid\" ></button>\n\n          <!--<button  pButton type=\"submit\"  label=\"提交\" *ngIf=\"viewState !== 'viewDetail'\"></button>-->\n          <!--<button  pButton type=\"submit\" [ngStyle]=\"{'float':'right'}\" label=\"保存\" (click)=\"formSave()\" label=\"保存\" *ngIf=\"viewState !== 'viewDetail'\"  ></button>-->\n        </div>\n      </p-header>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n        <div class=\"form-group ui-fluid  \" >\n          <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>领用单号:</label>\n          <div class=\"col-md-5 col-sm-5 \">\n            <input  name=\"inspectionName\" formControlName=\"sid\" type=\"text\" pInputText    readonly value=\"{{currentMaterial?.sid}}\">\n          </div>\n          <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>紧急程度:</label>\n          <div class=\" col-md-5 col-sm-5\">\n            <div class=\"ui-g-12 ui-fluid-no-padding\">\n              <input  name=\"inspectionName\" formControlName=\"urgency\" type=\"text\" pInputText   readonly value=\"{{currentMaterial?.urgency}}\">\n            </div>\n          </div>\n        </div>\n        <div class=\"form-group ui-fluid  \" >\n          <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>领用人:</label>\n          <div class=\"col-md-5 col-sm-5 \">\n\n            <input  name=\"inspectionName\" formControlName=\"name\" type=\"text\" pInputText    readonly value=\"{{currentMaterial?.creator}}\" >\n          </div>\n          <label  class=\" col-md-1 col-sm-1 control-label\"><span></span>部门名称:</label>\n          <div class=\" col-md-5 col-sm-5\">\n            <div class=\"ui-g-12 ui-fluid-no-padding\">\n              <input type=\"text\" pInputText formControlName=\"department\" name=\"inspectiondepartment\" placeholder=\"部门名称自动生成\"\n                     readonly class=\"cursor_not_allowed\" [(ngModel)]=\"currentMaterial.org\"  />\n            </div>\n          </div>\n        </div>\n        <div class=\"form-group ui-fluid  \" >\n          <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>申请时间:</label>\n          <div class=\"col-md-5 col-sm-5 \">\n            <input formControlName=\"return_time\" pInputText type=\"text\"  name=\"people\"    [(ngModel)]=\"currentMaterial.create_time\" readonly/>\n          </div>\n          <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>部门审批人:</label>\n          <div class=\" col-md-5 col-sm-5\">\n            <div class=\"ui-g-12 ui-fluid-no-padding\">\n              <input formControlName=\"people\" pInputText type=\"text\"  name=\"shenpi\" [(ngModel)]=\"currentMaterial.approver\"   class=\"no-border\" readonly/>\n\n            </div>\n          </div>\n        </div>\n        <div class=\"form-group ui-fluid \" >\n          <label  class=\"col-md-1 col-sm-1 col-lg-1   control-label\"><span>*</span>领用说明:</label>\n          <div class=\"col-md-11 col-sm-11 \">\n            <textarea pInputTextarea type=\"text\"  formControlName=\"remarks\" name=\"content\" [(ngModel)]=\"currentMaterial.remarks\" readonly></textarea>\n          </div>\n        </div>\n        <div class=\"form-group ui-fluid \" >\n          <label  class=\"col-md-1 col-sm-1 col-lg-1   control-label\"><span>*</span>审批意见:</label>\n          <div class=\"col-md-11 col-sm-11 \">\n            <textarea pInputTextarea type=\"text\"  formControlName=\"approval_opinions\" name=\"content\"  [(ngModel)]=\"approvedModel['approve_remarks']\" ></textarea>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!newInspectionPlan.controls['approval_opinions'].valid&&(!newInspectionPlan.controls['approval_opinions'].untouched)\" >\n              <i class=\"fa fa-close\"></i>\n              审批意见必填\n            </div>\n          </div>\n        </div>\n      </div>\n    </p-panel>\n  </form>\n  <label></label>\n  <div class=\"ui-g\">\n    <div class=\"ui-g-12\">\n      <label>领用列表</label>\n      <p-dataTable emptyMessage=\"没有数据\" [value]=\"inventories\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n                   [totalRecords]=\"totalRecords\" (onLazyLoad)=\"loadCarsLazy($event)\" [stacked]=\"stacked\" [responsive]=\"true\" >\n        <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n      </p-dataTable>\n      <div class=\"ui-grid-row margin-bottom-1vw\">\n        <div class=\"ui-grid-col-3\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/requeation-approved/requeation-approved.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequeationApprovedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RequeationApprovedComponent = (function () {
    function RequeationApprovedComponent(route, router, inventoryService, eventBusService, fb) {
        this.route = route;
        this.router = router;
        this.inventoryService = inventoryService;
        this.eventBusService = eventBusService;
        this.currentMaterial = [];
        this.inventories = [];
        this.newInspectionPlan = fb.group({
            sid: [''],
            name: [''],
            department: [''],
            check: [''],
            type: [''],
            return_time: [''],
            people: [''],
            urgency: [''],
            remarks: [''],
            approval_opinions: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
        });
    }
    RequeationApprovedComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.cols = [
            { field: 'sid', header: '编号' },
            { field: 'name', header: '名称' },
            { field: 'catagory', header: '分类' },
            { field: 'brand', header: '品牌' },
            { field: 'model', header: '型号' },
            { field: 'specification', header: '规格' },
            { field: 'application_quantity', header: '数量' },
        ];
        this.approvedModel = {
            "sid": "",
            "approve_remarks": "",
            "status": ""
        };
        this.route.queryParams.subscribe(function (parms) {
            if (parms['sid']) {
                _this.sid = parms['sid'];
            }
            _this.approvedModel.sid = _this.sid;
            _this.inventoryService.queryOutbound(_this.sid).subscribe(function (data) {
                _this.currentMaterial = data;
                console.log(_this.currentMaterial);
                _this.dataSource = _this.currentMaterial['inventories'];
                console.log(_this.dataSource);
                if (!_this.dataSource) {
                    _this.dataSource = [];
                }
                _this.inventories = _this.dataSource.slice(0, 10);
                _this.totalRecords = _this.dataSource.length;
            });
        });
        this.optButtons = [
            { label: '驳回', status: '驳回' },
            { label: '拒绝', status: '拒绝' },
            { label: '通过 ', status: '待出库' },
        ];
    };
    RequeationApprovedComponent.prototype.goBack = function () {
        var sid = this.currentMaterial['sid'];
        this.currentMaterial['status'] = "";
        this.eventBusService.requstion.next(this.currentMaterial['status']);
        this.router.navigate(['../requestionOverview'], { queryParams: { sid: sid, state: 'viewDetail', title: '借用总览' }, relativeTo: this.route });
    };
    RequeationApprovedComponent.prototype.approvedOption = function (status) {
        var _this = this;
        this.approvedModel.status = status;
        this.inventoryService.queryOutboundApproved(this.approvedModel).subscribe(function () {
            _this.currentMaterial['status'] = "";
            _this.eventBusService.requstion.next(_this.currentMaterial['status']);
            _this.router.navigate(['../requestionOverview'], { relativeTo: _this.route });
        });
    };
    RequeationApprovedComponent.prototype.loadCarsLazy = function (event) {
        //in a real application, make a remote request to load data using state metadata from event
        //event.first = First row offset
        //event.rows = Number of rows per page
        //event.sortField = Field name to sort with
        //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
        //imitate db connection over a network
        if (this.dataSource) {
            this.inventories = this.dataSource.slice(event.first, (event.first + event.rows));
        }
    };
    RequeationApprovedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-requeation-approved',
            template: __webpack_require__("../../../../../src/app/inventory/requeation-approved/requeation-approved.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/requeation-approved/requeation-approved.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]])
    ], RequeationApprovedComponent);
    return RequeationApprovedComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/requestion-management/requestion-management.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n\n  <div>\n    <span class=\"feature-title\">库存管理<span class=\"gt\">&gt;</span>{{title}}  </span>\n  </div>\n</div>\n<div class=\"content-section implementation GridDemo\">\n\n  <div class=\"parent\">\n    <div style=\"position: absolute;top: 20px;left:0\">\n      <svg width=\"150\" height=\"800\">\n        <ng-container *ngFor=\"let flow of flowCharts;let i =index\">\n          <!--<rect [ngStyle]=\"{'width':flow.width,'height':flow.height,'x':flow.x,'y':flow.y,'rx':flow.rx,'fill':flow.active?flow.fill:'#5BC0DE'}\" (click)=\"openPage(flow,i)\"></rect>-->\n          <g class=\"rect-text\" (click)=\"openPage(flow,i)\" >\n          <rect [attr.width]=\"flow.width\" [ngStyle]=\"{'cursor':'pointer'}\"[attr.height]=\"flow.height\" [attr.stroke]=\"flow.stroke\" [attr.x]=\"flow.x\" [attr.y]=\"flow.y\"  [attr.rx]= \"flow.rx\" [attr.fill]=\"flow.fill\" [attr.status]=\"flow.status\"    ></rect>\n          <text [attr.x]=\"flow.text.x\" [attr.y]=\"flow.text.y\">{{flow.label}}</text>\n          </g>\n            <g stroke=\"#39B9C6\" stroke-width=\"0.8\">\n            <line *ngFor=\"let g of flow.g\" [attr.x1]=\"g.x1\" [attr.y1]=\"g.y1\" [attr.x2]=\"g.x2\" [attr.y2]=\"g.y2\"></line>\n          </g>\n        </ng-container>\n      </svg>\n    </div>\n    <div class=\"node-right \">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/inventory/requestion-management/requestion-management.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".node-right {\n  margin-left: 135px; }\n\n.parent {\n  position: relative; }\n\n.rect-text {\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/requestion-management/requestion-management.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestionManagementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RequestionManagementComponent = (function () {
    function RequestionManagementComponent(router, route, inventoryService, eventBus) {
        this.router = router;
        this.route = route;
        this.inventoryService = inventoryService;
        this.eventBus = eventBus;
        this.title = '领用总览';
    }
    RequestionManagementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.queryModel = {
            "status": "",
        };
        this.flowCharts = [
            { label: '领用申请', route: 'requisition', stroke: "#39B9C6", fill: '#357CA5', active: true, x: '0', y: '0', rx: '10', width: 100, height: 40, title: '领用申请', text: { x: '20', y: '24', label: '领用申请' }, g: [{ x1: '50', y1: '40', x2: '50', y2: '100' }, { x1: '50', y1: '100', x2: '43', y2: '90' }, { x1: '50', y1: '100', x2: '58', y2: '90' }] },
            { label: '领用审批', route: 'requestionOverview', status: "待审批", stroke: "#39B9C6", fill: '#357CA5', active: false, x: '0', y: '100', width: 100, height: 40, title: '领用审批', text: { x: '20', y: '124', label: '领用审批' }, g: [{ x1: '50', y1: '140', x2: '50', y2: '200' }, { x1: '50', y1: '200', x2: '43', y2: '190' }, { x1: '50', y1: '200', x2: '58', y2: '190' }] },
            { label: '出库', route: '', active: true, x: '0', y: '200', rx: '10', stroke: "#39B9C6", fill: '#357CA5', width: 100, height: 40, title: '', text: { x: '34', y: '224', label: '归还' } }
        ];
        this.eventBus.requstion.subscribe(function (data) {
            console.log(data);
            _this.queryModel.status = data;
            _this.queryMaterialDate();
        });
        this.route.queryParams.subscribe(function (params) {
            if (params['title']) {
                _this.title = params['title'];
            }
            if (params['sid']) {
                _this.inventoryService.queryDelivery(params['sid']).subscribe(function (data) {
                    console.log(data);
                });
            }
        });
        this.queryMaterialDate();
    };
    RequestionManagementComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryMaterialreceive(this.queryModel).subscribe(function (data) {
            if (!_this.MaterialCharData) {
                _this.MaterialCharData = [];
            }
            _this.MaterialCharData = data;
            for (var i = 0; i < _this.MaterialCharData.length; i++) {
                var temp = _this.MaterialCharData[i];
                _this.flowCharts[i]['fill'] = temp['fill'];
            }
        });
    };
    RequestionManagementComponent.prototype.openPage = function (flow, index) {
        if (index + 1 === this.flowCharts.length)
            return;
        for (var i = 0; i < this.flowCharts.length; i++) {
            this.flowCharts[i].active = false;
        }
        this.flowCharts[index].active = true;
        this.queryMaterialDate();
        if (flow.status) {
            this.eventBus.updatereequipment.next(flow.status);
        }
        if (flow.route) {
            this.router.navigate([flow.route], { relativeTo: this.route });
            this.title = flow.title;
        }
    };
    RequestionManagementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-requestion-management',
            template: __webpack_require__("../../../../../src/app/inventory/requestion-management/requestion-management.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/requestion-management/requestion-management.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_2__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], RequestionManagementComponent);
    return RequestionManagementComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/requestion-overview/requestion-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes requistView\">\n <form>\n  <div class=\"mysearch\">\n    <div class=\"ui-g\">\n      <div class=\"ui-g-5 ui-sm-12\">\n        <label class=\"ui-g-3 ui-sm-5 text-right\">领用单号：</label>\n        <div class=\"ui-g-3  ui-sm-7 ui-fluid\">\n          <input pInputText type=\"text\" name=\"sid\" placeholder=\"领用单号\" [(ngModel)]=\"queryModel.condition.sid\" />\n        </div>\n        <label class=\"ui-g-2 ui-sm-5 ui-no-padding-left-15px text-right\">领用人:</label>\n        <div class=\"ui-g-3 ui-sm-7 ui-fluid   \">\n          <input  pInputText type=\"text\" name=\"creator\" placeholder=\"领用人\" [(ngModel)]=\"queryModel.condition.creator\"  />\n        </div>\n      </div>\n      <div class=\"ui-g-3 ui-sm-12\">\n        <label class=\"ui-g-5 ui-sm-5 text-right\">部门名称:</label>\n        <div class=\"ui-g-6  ui-sm-7 ui-fluid\">\n          <input  pInputText type=\"text\" name=\"department\" placeholder=\"部门名称\" [(ngModel)]=\"queryModel.condition.org\"  />\n        </div>\n      </div>\n      <div class=\"ui-g-3 ui-sm-12\">\n        <label  class=\"ui-g-2 ui-sm-5 text-right\">状态:</label>\n        <div class=\"ui-g-5  ui-sm-7 ui-fluid \">\n          <p-dropdown [options]=\"MaterialStatusData\"  [autoWidth]=\"false\" name=\"cycle\" (onChange)=\"suggestInsepectiones()\" [(ngModel)]=\"queryModel.condition.status\"></p-dropdown>\n        </div>\n        <div class=\"ui-g-5 ui-sm-12 option\">\n          <button pButton type=\"button\" label=\"查询\" ngClass=\"ui-sm-12\" (click)=\"suggestInsepectiones()\" ></button>\n          <button pButton  label=\"清空\" ngClass=\"ui-sm-12\" (click)=\"clearSearch()\"  ></button>\n        </div>\n      </div>\n    </div>\n  </div>\n </form>\n  <div class=\"ui-g\">\n    <div class=\"ui-g-12 requestion\">\n      <p-dataTable [value]=\"MaterialData\"\n                   [totalRecords]=\"totalRecords\"  [(selection)]=\"selectMaterial\">\n        <p-column  field=\"sid\" header=\"领用单号\" [sortable]=\"true\" >\n          <ng-template let-data=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n            <span (click)=\"viewBorrowApproved(data)\" class=\"curser\">{{data.sid}}</span>\n          </ng-template>\n        </p-column>\n        <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"  [sortable]=\"true\"></p-column>\n        <p-column header=\"操作\">\n          <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n            <button pButton type=\"button\"  label=\"编辑\"  (click)=\"updateOption(MaterialData[i])\" *ngIf=\"MaterialData[i]['status'] === '新建' || MaterialData[i]['status'] === '驳回'\" ></button>\n            <button pButton type=\"button\"  label=\"审批\"  (click)=\" borrowApproved(MaterialData[i])\" *ngIf=\"MaterialData[i]['status'] === '待审批'\"  ></button>\n            <button pButton type=\"button\"  label=\"出库\" (click)=\"updateOutboundRequestion(MaterialData[i])\" *ngIf=\"MaterialData[i]['status'] === '待领用'\"></button>\n            <button pButton type=\"button\"  label=\"删除\" (click)=\"deleteMaterial(MaterialData[i])\"  *ngIf=\"MaterialData[i]['status'] === '新建'\"  ></button>\n            <button pButton type=\"button\"  label=\"查看\"   (click)=\" viewBorrowApproved(MaterialData[i])\"  ></button>\n          </ng-template>\n        </p-column>\n        <ng-template pTemplate=\"emptymessage\">\n          当前没有数据\n        </ng-template>\n      </p-dataTable>\n      <p-paginator rows=\"10\" [totalRecords]=\"totalRecords\" [rowsPerPageOptions]=\"[10,20,30]\" (onPageChange)=\"paginate($event)\"></p-paginator>\n    </div>\n  </div>\n\n</div>\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/requestion-overview/requestion-overview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (min-width: 1024px) {\n  .requestion /deep/ table thead tr th:last-child {\n    width: 23%; } }\n\n.mysearch {\n  margin-bottom: 20px;\n  padding: 10px;\n  background: #fff; }\n\n.requistView /deep/ .ui-datatable th.ui-state-active {\n  color: #555555;\n  background-color: #EFF0F3; }\n\n.curser {\n  color: #39b9c6;\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/requestion-overview/requestion-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestionOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RequestionOverviewComponent = (function () {
    function RequestionOverviewComponent(inventoryService, router, route, eventBusService, confirmationService) {
        this.inventoryService = inventoryService;
        this.router = router;
        this.route = route;
        this.eventBusService = eventBusService;
        this.confirmationService = confirmationService;
        this.chooseCids = [];
        this.selectMaterial = [];
    }
    RequestionOverviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.statusNames = [
            { label: '', value: '' },
            { label: '新建', value: '新建' },
            { label: '待审批', value: '待审批' },
            { label: '待出库', value: '待出库' },
            { label: '已完成', value: '已完成' },
            { label: '拒绝', value: '拒绝' },
            { label: '驳回', value: '驳回' },
        ];
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            dayNamesShort: ["一", "二", "三", "四", "五", "六", "七"],
            dayNamesMin: ["一", "二", "三", "四", "五", "六", "七"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
        };
        this.queryModel = {
            "condition": {
                "sid": "",
                "location": "",
                "status": "",
                "begin_time": "",
                "end_time": "",
                "org": "",
                "creator": ""
            },
            "page": {
                "page_size": "10",
                "page_number": "1"
            }
        };
        this.cols = [
            // {field: 'sid', header: '领用单号'},
            // {field: 'type', header: '申请类型'},
            { field: 'creator', header: '领用人' },
            { field: 'org', header: '部门名称' },
            { field: 'status', header: '状态' },
            { field: 'approver', header: '审批人' },
            { field: 'audit_time', header: '审批时间' },
        ];
        this.eventBusService.updatereequipment.subscribe(function (data) {
            _this.queryModel.condition.status = data;
            _this.queryMaterialDate();
        });
        this.queryMaterialDate();
        this.queryMaterialStatusDate();
    };
    RequestionOverviewComponent.prototype.clearSearch = function () {
        this.queryModel.condition.begin_time = '';
        this.queryModel.condition.end_time = '';
        this.queryModel.condition.sid = '';
        this.queryModel.condition.org = '';
        this.queryModel.condition.creator = '';
    };
    //搜索功能
    RequestionOverviewComponent.prototype.suggestInsepectiones = function () {
        this.queryModel.page.page_number = '1';
        this.queryModel.page.page_size = '10';
        this.queryMaterialDate();
    };
    //分页查询
    RequestionOverviewComponent.prototype.paginate = function (event) {
        this.chooseCids = [];
        this.queryModel.page.page_number = String(++event.page);
        this.queryModel.page.page_size = String(event.rows);
        this.queryMaterialDate();
    };
    RequestionOverviewComponent.prototype.queryMaterialStatusDate = function () {
        var _this = this;
        this.inventoryService.queryReceiveStatus().subscribe(function (data) {
            if (!data) {
                _this.MaterialStatusData = [];
            }
            else {
                _this.MaterialStatusData = data;
                _this.MaterialStatusData = _this.inventoryService.formatDropdownData(_this.MaterialStatusData);
                console.log(_this.MaterialStatusData);
            }
        });
    };
    RequestionOverviewComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryRequestionView(this.queryModel).subscribe(function (data) {
            if (!data['items']) {
                _this.MaterialData = [];
                // this.selectMaterial =[];
                // this.totalRecords = 0;
            }
            else {
                _this.MaterialData = data.items;
                console.log(_this.MaterialData);
                _this.totalRecords = data.page.total;
            }
        });
    };
    //审批
    RequestionOverviewComponent.prototype.borrowApproved = function (current) {
        var sid = current['sid'];
        this.eventBusService.requstion.next(current.status);
        this.router.navigate(['../requisitionapproved'], { queryParams: { sid: sid }, relativeTo: this.route });
    };
    // //确认出库
    // sureApproved(current){
    //   let sid = current['sid'];
    //   this.eventBusService.requstion.next(current.status);
    //   this.confirmationService.confirm({
    //     message: '确定出库吗?',
    //     accept: () => {
    //       this.inventoryService.queryBorrowSure(sid).subscribe(data=>{
    //         this.selectMaterial = [];
    //         this.queryMaterialDate();
    //       },(err:Error)=>{
    //         this.confirmationService.confirm({
    //           message: err['message'],
    //           rejectVisible:false,
    //         })
    //       })
    //     },
    //     reject:() =>{}
    //   })
    // }
    //编辑
    RequestionOverviewComponent.prototype.updateOption = function (current) {
        var sid = current['sid'];
        this.eventBusService.requstion.next(current.status);
        this.router.navigate(['../requisition'], { queryParams: { sid: sid, state: 'update', title: '编辑' }, relativeTo: this.route });
    };
    //查看
    RequestionOverviewComponent.prototype.viewBorrowApproved = function (current) {
        var sid = current['sid'];
        this.eventBusService.requstion.next(current.status);
        this.router.navigate(['../requisition'], { queryParams: { sid: sid, state: 'viewDetail', title: '查看' }, relativeTo: this.route });
    };
    //出库
    RequestionOverviewComponent.prototype.updateOutboundRequestion = function (current) {
        var sid = current['sid'];
        this.eventBusService.requstion.next(current.status);
        this.router.navigate(['../requisition'], { queryParams: { sid: sid, state: 'outboundRequestion', title: '出库' }, relativeTo: this.route });
    };
    RequestionOverviewComponent.prototype.deleteMaterial = function (current) {
        var _this = this;
        //请求成功后删除本地数据
        this.chooseCids = [];
        this.chooseCids.push(current['sid']);
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: function () {
                _this.inventoryService.deleteDelivery(_this.chooseCids).subscribe(function () {
                    _this.queryMaterialDate();
                }, function (err) {
                    var message;
                    if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                        message = '似乎网络出现了问题，请联系管理员或稍后重试';
                    }
                    else {
                        message = err;
                    }
                    _this.confirmationService.confirm({
                        message: message,
                        rejectVisible: false,
                    });
                });
            },
            reject: function () {
            }
        });
    };
    RequestionOverviewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-requestion-overview',
            template: __webpack_require__("../../../../../src/app/inventory/requestion-overview/requestion-overview.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/requestion-overview/requestion-overview.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__["ConfirmationService"]])
    ], RequestionOverviewComponent);
    return RequestionOverviewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/requisition-application/requisition-application.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes\">\n    <form class=\"form-horizontal\" [formGroup]=\"newInspectionPlan\"  *ngIf=\"viewState ==='apply'\" >\n      <p-panel >\n        <p-header>\n          <div class=\"ui-helper-clearfix\">\n            <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n            <button  pButton  [ngStyle]=\"{'width':'100px'}\" label=\"返回\" (click)=\"goBack()\"   ></button>\n            <button  pButton  [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"保存\" (click)=\"formSave()\"    ></button>\n            <button  pButton  [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"提交\" (click)=\"formSubmit()\" ></button>\n          </div>\n        </p-header>\n        <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n          <div class=\"form-group ui-fluid  \" >\n            <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>领用单号:</label>\n            <div class=\"col-md-5 col-sm-5 \">\n              <input  name=\"inspectionName\" formControlName=\"sid\" type=\"text\" pInputText   placeholder=\"领用单号自动生成\" readonly >\n            </div>\n            <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>紧急程度:</label>\n            <div class=\" col-md-5 col-sm-5\">\n              <div class=\"ui-g-12 ui-fluid-no-padding\">\n                <p-dropdown [autoWidth]=\"false\"  [options]=\"urgency\" formControlName=\"type\"  name=\"fenlei\" [(ngModel)]=\"submitRequiestData.urgency\"></p-dropdown>\n                <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!newInspectionPlan.controls['type'].valid&&(!newInspectionPlan.controls['type'].untouched)\" >\n                  <i class=\"fa fa-close\"></i>\n                  领用类型必填\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group ui-fluid  \" >\n            <label  class=\"col-sm-1 col-md-1 col-lg-1  control-label\"><span></span>领用人:</label>\n            <div class=\"col-md-5 col-sm-5 \">\n              <input  name=\"inspectionName\" formControlName=\"name\" type=\"text\" pInputText    readonly value=\"{{personalData.name}}\">\n            </div>\n            <label  class=\" col-md-1 col-sm-1 control-label\"><span></span>部门名称:</label>\n            <div class=\" col-md-5 col-sm-5\">\n              <div class=\"ui-g-12 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText formControlName=\"department\" name=\"inspectiondepartment\" placeholder=\"部门名称自动生成\"\n                       readonly class=\"cursor_not_allowed\"  *ngIf=\"viewState ==='apply'\" value=\"{{submitRequiestData.org}}\"/>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group ui-fluid  \" >\n            <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>申请时间:</label>\n            <div class=\"col-md-5 col-sm-5 \">\n              <input formControlName=\"return_time\" pInputText type=\"text\"  name=\"people\"    class=\"no-border\" [(ngModel)]=\"submitRequiestData.create_time\" readonly/>\n            </div>\n            <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>部门审批人:</label>\n            <div class=\" col-md-5 col-sm-5\">\n              <div class=\"ui-g-12 ui-fluid-no-padding\">\n                <p-dropdown [autoWidth]=\"false\"  [options]=\"approver\" formControlName=\"check\"  name=\"check\" [(ngModel)]=\"submitRequiestData.approver\" ></p-dropdown>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group ui-fluid \" >\n            <label  class=\"col-md-1 col-sm-1 col-lg-1   control-label\"><span>*</span>领用说明:</label>\n              <div class=\"col-md-11 col-sm-11 col-lg-11 \">\n                <textarea pInputTextarea type=\"text\"  formControlName=\"remarks\" name=\"content\" [(ngModel)]=\"submitRequiestData.remarks\"    ></textarea>\n                <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!newInspectionPlan.controls['remarks'].valid&&(!newInspectionPlan.controls['remarks'].untouched)\" >\n                  <i class=\"fa fa-close\"></i>\n                  领用说明必填\n                </div>\n               </div>\n          </div>\n\n        </div>\n      </p-panel>\n      <div class=\"ui-g\">\n        <div class=\"ui-g-10 ui-sm-12\" >\n          <div class=\"ui-g-2  ui-sm-5 ui-fluid\">\n          </div>\n          <div class=\"ui-g-2 ui-sm-5 ui-fluid  ui-no-padding-right-15px \">\n          </div>\n        </div>\n        <div class=\"ui-g-2 ui-sm-12\" >\n          <div class=\"ui-g-6  ui-sm-6  ui-fluid\">\n            <button   pButton   label=\"移除\" (click)=\"deleteStorage()\" [disabled]=\"selectInsepections.length===0 || selectInsepections.length > 1\"></button>\n          </div>\n          <div class=\"ui-g-6 ui-sm-6 ui-fluid  ui-no-padding-right-15px \">\n            <button pButton type=\"button\"  (click)=\"showAddStorageMask()\" label=\"物品选择\"></button>\n          </div>\n        </div>\n      </div>\n    </form>\n\n     <form class=\"form-horizontal\" [formGroup]=\"newInspectionPlan\" *ngIf=\"viewState ==='update'\">\n       <p-panel >\n         <p-header>\n           <div class=\"ui-helper-clearfix\">\n             <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n             <button  pButton  [ngStyle]=\"{'width':'100px'}\" label=\"返回\" (click)=\"goBack()\"   ></button>\n             <button  pButton  [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"保存\" (click)=\"formSave()\"   ></button>\n             <button  pButton  [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"提交\" (click)=\"formSubmit()\" ></button>\n           </div>\n         </p-header>\n         <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n           <div class=\"form-group ui-fluid  \" >\n             <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>领用单号:</label>\n             <div class=\"col-md-5 col-sm-5 \">\n               <input  name=\"inspectionName\" formControlName=\"sid\" type=\"text\" pInputText   placeholder=\"领用单号自动生成\" readonly  value=\"{{borrowData?.sid}}\">\n             </div>\n             <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>紧急程度:</label>\n             <div class=\" col-md-5 col-sm-5\">\n               <div class=\"ui-g-12 ui-fluid-no-padding\">\n                 <p-dropdown [autoWidth]=\"false\"  [options]=\"urgency\" formControlName=\"type\"  name=\"fenlei\" [(ngModel)]=\"borrowData.urgency\" ></p-dropdown>\n                 <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!newInspectionPlan.controls['type'].valid&&(!newInspectionPlan.controls['type'].untouched)\" >\n                   <i class=\"fa fa-close\"></i>\n                   领用类型必填\n                 </div>\n               </div>\n             </div>\n           </div>\n           <div class=\"form-group ui-fluid  \" >\n             <label  class=\"col-sm-1 col-md-1 col-lg-1  control-label\"><span></span>领用人:</label>\n             <div class=\"col-md-5 col-sm-5 \">\n               <input  name=\"inspectionName\" formControlName=\"name\" type=\"text\" pInputText    readonly value=\"{{personalData.name}}\" >\n             </div>\n             <label  class=\" col-md-1 col-sm-1 control-label\"><span></span>部门名称:</label>\n             <div class=\" col-md-5 col-sm-5\">\n               <div class=\"ui-g-12 ui-fluid-no-padding\">\n                 <input type=\"text\" pInputText formControlName=\"department\" name=\"inspectiondepartment\" placeholder=\"部门名称自动生成\"\n                        readonly class=\"cursor_not_allowed\" value=\"{{submitRequiestData.org}}\"/>\n               </div>\n             </div>\n           </div>\n           <div class=\"form-group ui-fluid  \" >\n             <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>申请时间:</label>\n             <div class=\"col-md-5 col-sm-5 \">\n               <input formControlName=\"return_time\" pInputText type=\"text\"  name=\"people\"    class=\"no-border\" [(ngModel)]=\"submitRequiestData.create_time\" >\n             </div>\n             <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>部门审批人:</label>\n             <div class=\" col-md-5 col-sm-5\">\n               <div class=\"ui-g-12 ui-fluid-no-padding\">\n                 <p-dropdown [autoWidth]=\"false\"  [options]=\"approver\" formControlName=\"check\"  name=\"check\" [(ngModel)]=\"borrowData.approver\"  ></p-dropdown>\n               </div>\n             </div>\n           </div>\n           <div class=\"form-group ui-fluid \" >\n             <label  class=\"col-md-1 col-sm-1 col-lg-1   control-label\"><span>*</span>领用说明:</label>\n               <div class=\"col-md-11 col-sm-11 col-lg-11 \">\n                 <textarea pInputTextarea type=\"text\"  formControlName=\"remarks\" name=\"content\" [(ngModel)]=\"borrowData.remarks\"  ></textarea>\n                 <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!newInspectionPlan.controls['remarks'].valid&&(!newInspectionPlan.controls['remarks'].untouched)\" >\n                   <i class=\"fa fa-close\"></i>\n                   领用说明必填\n                 </div>\n                </div>\n           </div>\n         </div>\n       </p-panel>\n       <div class=\"ui-g\">\n         <div class=\"ui-g-10 ui-sm-12\" >\n           <div class=\"ui-g-2  ui-sm-5 ui-fluid\">\n           </div>\n           <div class=\"ui-g-2 ui-sm-5 ui-fluid  ui-no-padding-right-15px \">\n           </div>\n         </div>\n         <div class=\"ui-g-2 ui-sm-12\" >\n           <div class=\"ui-g-6  ui-sm-6  ui-fluid\">\n             <button   pButton   label=\"移除\" (click)=\"updatedeleteStorage()\" [disabled]=\"selectInsepections.length===0 || selectInsepections.length >1\"></button>\n           </div>\n           <div class=\"ui-g-6 ui-sm-6 ui-fluid  ui-no-padding-right-15px \">\n             <button pButton type=\"button\"  (click)=\"showAddStorageMask()\" label=\"物品选择\"></button>\n           </div>\n         </div>\n       </div>\n     </form>\n\n      <form class=\"form-horizontal\" [formGroup]=\"newInspectionPlan\" *ngIf=\"viewState ==='viewDetail'\">\n        <p-panel >\n          <p-header>\n            <div class=\"ui-helper-clearfix\">\n              <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n              <button  pButton  [ngStyle]=\"{'width':'100px'}\" label=\"返回\" (click)=\"goBack()\"   ></button>\n            </div>\n          </p-header>\n          <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n            <div class=\"form-group ui-fluid  \" >\n              <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>领用单号:</label>\n              <div class=\"col-md-5 col-sm-5 \">\n                <input  name=\"inspectionName\" formControlName=\"sid\" type=\"text\" pInputText   placeholder=\"领用单号自动生成\" readonly  value=\"{{borrowData?.sid}}\">\n              </div>\n              <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>紧急程度:</label>\n              <div class=\" col-md-5 col-sm-5\">\n                <div class=\"ui-g-12 ui-fluid-no-padding\">\n                  <input  name=\"fenlei\" formControlName=\"name\" type=\"text\" pInputText    readonly value=\"{{borrowData.urgency}}\" >\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group ui-fluid  \" >\n              <label  class=\"col-sm-1 col-md-1 col-lg-1  control-label\"><span></span>领用人:</label>\n              <div class=\"col-md-5 col-sm-5 \">\n                <input  name=\"inspectionName\" formControlName=\"name\" type=\"text\" pInputText    readonly value=\"{{personalData.name}}\" >\n              </div>\n              <label  class=\" col-md-1 col-sm-1 control-label\"><span></span>部门名称:</label>\n              <div class=\" col-md-5 col-sm-5\">\n                <div class=\"ui-g-12 ui-fluid-no-padding\">\n                  <input type=\"text\" pInputText formControlName=\"department\" name=\"inspectiondepartment\" placeholder=\"部门名称自动生成\"\n                         readonly class=\"cursor_not_allowed\"   value=\"{{submitRequiestData.org}}\" />\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group ui-fluid  \" >\n              <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>申请时间:</label>\n              <div class=\"col-md-5 col-sm-5 \">\n                <input formControlName=\"return_time\" pInputText type=\"text\"  name=\"people\"    class=\"no-border\" [(ngModel)]=\"submitRequiestData.create_time\" readonly>\n              </div>\n              <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>部门审批人:</label>\n              <div class=\" col-md-5 col-sm-5\">\n                <div class=\"ui-g-12 ui-fluid-no-padding\">\n                  <input formControlName=\"check\" pInputText type=\"text\"  name=\"check\"    class=\"no-border\" [(ngModel)]=\"borrowData.approver\"  readonly>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group ui-fluid \" >\n              <label  class=\"col-md-1 col-sm-1 col-lg-1   control-label\"><span>*</span>领用说明:</label>\n                <div class=\"col-md-11 col-sm-11 col-lg-11 \">\n                  <textarea pInputTextarea type=\"text\"  formControlName=\"remarks\" name=\"content\" [(ngModel)]=\"borrowData.remarks\" readonly></textarea>\n                 </div>\n            </div>\n            <div class=\"form-group ui-fluid \" *ngIf=\"borrowData.status !== '新建' && borrowData.status !== '待审批'\" >\n              <label  class=\"col-md-1 col-sm-1 col-lg-1   control-label\">审批意见:</label>\n                <div class=\"col-md-11 col-sm-11 col-lg-11 \">\n                  <textarea pInputTextarea type=\"text\"  formControlName=\"approve_remarks\" name=\"content\" [(ngModel)]=\"borrowData.approve_remarks\"  readonly></textarea>\n                 </div>\n            </div>\n          </div>\n        </p-panel>\n      </form>\n      <form class=\"form-horizontal\" [formGroup]=\"newInspectionPlan\" *ngIf=\"viewState ==='outboundRequestion'\">\n        <p-panel >\n          <p-header>\n            <div class=\"ui-helper-clearfix\">\n              <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n              <button  pButton  [ngStyle]=\"{'width':'100px'}\" label=\"返回\" (click)=\"goBack()\"   ></button>\n            </div>\n          </p-header>\n          <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n            <div class=\"form-group ui-fluid  \" >\n              <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>领用单号:</label>\n              <div class=\"col-md-5 col-sm-5 \">\n                <input  name=\"inspectionName\" formControlName=\"sid\" type=\"text\" pInputText   placeholder=\"领用单号自动生成\" readonly  value=\"{{borrowData?.sid}}\">\n              </div>\n              <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>紧急程度:</label>\n              <div class=\" col-md-5 col-sm-5\">\n                <div class=\"ui-g-12 ui-fluid-no-padding\">\n                  <input  name=\"fenlei\" formControlName=\"name\" type=\"text\" pInputText    readonly value=\"{{submitRequiestData.urgency}}\" >\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group ui-fluid  \" >\n              <label  class=\"col-sm-1 col-md-1 col-lg-1  control-label\"><span></span>领用人:</label>\n              <div class=\"col-md-5 col-sm-5 \">\n                <input  name=\"inspectionName\" formControlName=\"name\" type=\"text\" pInputText    readonly value=\"{{personalData.name}}\" >\n              </div>\n              <label  class=\" col-md-1 col-sm-1 control-label\"><span></span>部门名称:</label>\n              <div class=\" col-md-5 col-sm-5\">\n                <div class=\"ui-g-12 ui-fluid-no-padding\">\n                  <input type=\"text\" pInputText formControlName=\"department\" name=\"inspectiondepartment\" placeholder=\"部门名称自动生成\"\n                         readonly class=\"cursor_not_allowed\"   value=\"{{submitRequiestData.org}}\" />\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group ui-fluid  \" >\n              <label  class=\"col-sm-1 col-md-1 col-lg-1   control-label\"><span></span>申请时间:</label>\n              <div class=\"col-md-5 col-sm-5 \">\n                <input formControlName=\"return_time\" pInputText type=\"text\"  name=\"people\"    class=\"no-border\" [(ngModel)]=\"submitRequiestData.create_time\" readonly>\n              </div>\n              <label  class=\" col-md-1 col-sm-1 control-label\"><span>*</span>部门审批人:</label>\n              <div class=\" col-md-5 col-sm-5\">\n                <div class=\"ui-g-12 ui-fluid-no-padding\">\n                  <input formControlName=\"check\" pInputText type=\"text\"  name=\"check\"    class=\"no-border\" [(ngModel)]=\"borrowData.approver\"  readonly>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group ui-fluid \" >\n              <label  class=\"col-md-1 col-sm-1 col-lg-1   control-label\"><span>*</span>领用说明:</label>\n                <div class=\"col-md-11 col-sm-11 col-lg-11 \">\n                  <textarea pInputTextarea type=\"text\"  formControlName=\"remarks\" name=\"content\" [(ngModel)]=\"borrowData.remarks\" readonly></textarea>\n                 </div>\n            </div>\n            <div class=\"form-group ui-fluid \" *ngIf=\"borrowData.status !== '新建' && borrowData.status !== '待审批'\" >\n              <label  class=\"col-md-1 col-sm-1 col-lg-1   control-label\">审批意见:</label>\n                <div class=\"col-md-11 col-sm-11 col-lg-11 \">\n                  <textarea pInputTextarea type=\"text\"  formControlName=\"approve_remarks\" name=\"content\" [(ngModel)]=\"borrowData.approve_remarks\"  readonly></textarea>\n                 </div>\n            </div>\n          </div>\n        </p-panel>\n        <div class=\"ui-g\">\n          <div class=\"ui-g-10 ui-sm-12\" >\n            <div class=\"ui-g-2  ui-sm-5 ui-fluid\">\n            </div>\n            <div class=\"ui-g-2 ui-sm-5 ui-fluid  ui-no-padding-right-15px \">\n            </div>\n          </div>\n          <div class=\"ui-g-2 ui-sm-12\" >\n            <div class=\"ui-g-6  ui-sm-6  ui-fluid\">\n              <!--<button   pButton   label=\"移除\" (click)=\"deleteStorage()\" [disabled]=\"selectInsepections.length===0\"></button>-->\n            </div>\n            <div class=\"ui-g-6 ui-sm-6 ui-fluid  ui-no-padding-right-15px \">\n              <button pButton type=\"button\"  (click)=\"sureApproved()\" label=\"出库\" [disabled]=\"selectInsepections.length === 0\"></button>\n            </div>\n          </div>\n        </div>\n      </form>\n  <div class=\"ui-g\">\n  <div class=\"ui-g-12\">\n    <label><span>*</span>领用列表</label>\n    <p-dataTable [value]=\"inspections\"  emptyMessage=\"没有数据\" (onLazyLoad)=\"loadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n                 [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" [editable]=\"true\" *ngIf=\"viewState ==='apply'\" class=\"application_apply\">\n      <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n      <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n    </p-dataTable>\n    <p-dataTable [value]=\"editInspections\" emptyMessage=\"没有数据\"  (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n                 [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\" [editable]=\"true\" *ngIf=\"viewState ==='update'\" class=\"application_update\">\n      <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n      <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n    </p-dataTable>\n    <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n                 [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\"  *ngIf=\"viewState ==='viewDetail'\" >\n      <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n      <p-column  *ngFor=\"let col of colsDetail\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n      <ng-template pTemplate=\"emptymessage\" >\n        当前没有数据\n      </ng-template>\n    </p-dataTable>\n    <p-dataTable [value]=\"editInspections\" (onLazyLoad)=\"editloadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n                 [totalRecords]=\"totalRecords\"  [(selection)]=\"selectInsepections\"  *ngIf=\"viewState ==='outboundRequestion'\" [editable]=\"true\" class=\"outboundRequestion\" >\n      <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n      <p-column  *ngFor=\"let col of updateRequestioncols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [editable]=\"col.editable\"></p-column>\n      <ng-template pTemplate=\"emptymessage\" >\n        当前没有数据\n      </ng-template>\n    </p-dataTable>\n  </div>\n\n  </div>\n</div>\n\n\n\n\n<app-borrow-items\n  *ngIf=\"showBorrowItems\"\n  (closeBorrowItemsMask)=\"closeBorrowItemsMask($event)\"\n  (addDev)=\"addDev($event)\"\n></app-borrow-items>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/requisition-application/requisition-application.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".mysearch {\n  margin-bottom: 20px;\n  padding: 10px;\n  background: #fff; }\n\nlabel > span {\n  color: red; }\n\n/deep/.ui-widget-header {\n  background-color: white;\n  background: linear-gradient(to bottom, #ffffff 0%, #ffffff 100%); }\n\n@media screen and (min-width: 1024px) {\n  .application_apply /deep/ table tbody tr td:nth-child(8) {\n    color: #00c0ef;\n    cursor: pointer; } }\n\n@media screen and (min-width: 1024px) {\n  .application_update /deep/ table tbody tr td:nth-child(8) {\n    color: #00c0ef;\n    cursor: pointer; } }\n\n@media screen and (min-width: 1024px) {\n  .outboundRequestion /deep/ table tbody tr td:nth-child(9) {\n    color: #00c0ef;\n    cursor: pointer; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/requisition-application/requisition-application.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequisitionApplicationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import {ReportService} from "../../report/report.service";





var RequisitionApplicationComponent = (function () {
    function RequisitionApplicationComponent(inventoryService, router, route, confirmationService, publicService, fb, eventBusService) {
        this.inventoryService = inventoryService;
        this.router = router;
        this.route = route;
        this.confirmationService = confirmationService;
        this.publicService = publicService;
        this.eventBusService = eventBusService;
        this.showBorrowItems = false;
        this.personalData = [];
        this.selectMaterial = [];
        this.inspections = [];
        this.editDataSource = [];
        this.editInspections = [];
        this.borrowData = [];
        this.selectInsepections = [];
        this.title = '领用申请';
        this.qureyModel = {
            label: '',
            value: ''
        };
        this.dataSource = [];
        this.newInspectionPlan = fb.group({
            sid: [''],
            name: [''],
            department: [''],
            check: [''],
            type: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            return_time: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            urgency: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            remarks: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            approve_remarks: [''],
        });
    }
    RequisitionApplicationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.viewState = 'apply';
        // 查询人员表格数据
        this.queryPersonList('');
        this.queryMeterial();
        this.cols = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'quantity', header: '数量', editable: true },
        ];
        this.colsDetail = [
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'application_quantity', header: '数量', editable: true },
            { field: 'quantity', header: '实际出库数量', editable: true },
        ];
        this.updateRequestioncols = [
            // {field: 'quantity', header: '数量',editable:true},
            { field: 'sid', header: '编号', editable: false },
            { field: 'name', header: '名称', editable: false },
            { field: 'catagory', header: '分类', editable: false },
            { field: 'brand', header: '品牌', editable: false },
            { field: 'model', header: '型号', editable: false },
            { field: 'specification', header: '规格', editable: false },
            { field: 'application_quantity', header: '申请数量', editable: false },
            { field: 'quantity', header: '实际出库数量', editable: true },
            { field: 'onhand_quantity', header: '当前库存', editable: false },
        ];
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            dayNamesShort: ["一", "二", "三", "四", "五", "六", "七"],
            dayNamesMin: ["一", "二", "三", "四", "五", "六", "七"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
        };
        this.statusNames = [
            { label: '领用', value: '领用' },
        ];
        this.submitRequiestData = {
            "sid": "",
            "type": this.statusNames[0]['value'],
            "create_time": this.inventoryService.getFormatTime(),
            "urgency": "",
            "remarks": "",
            "approver": "",
            "approver_pid": "",
            "approve_remarks": "",
            "org": '',
            "inventories": []
        };
        this.querylogin();
        this.route.queryParams.subscribe(function (params) {
            _this.viewState = 'apply';
            if (params['sid'] && params['state'] && params['title']) {
                _this.sid = params['sid'];
                _this.title = params['title'];
                _this.viewState = params['state'];
                _this.inventoryService.outboundShow(params['sid']).subscribe(function (data) {
                    _this.borrowData = data;
                    console.log(_this.borrowData);
                    _this.editDataSource = _this.borrowData['inventories'] ? _this.borrowData['inventories'] : [];
                    if (!_this.editDataSource) {
                        _this.editDataSource = [];
                        _this.editInspections = [];
                        _this.totalRecords = 0;
                    }
                    _this.totalRecords = _this.editDataSource.length;
                    _this.editInspections = _this.editDataSource.slice(0, 10);
                });
            }
        });
        // this.queryMaterialDate();
    };
    RequisitionApplicationComponent.prototype.goBack = function () {
        this.borrowData['status'] = "";
        this.eventBusService.requstion.next(this.borrowData['status']);
        this.router.navigate(['../requestionOverview'], { relativeTo: this.route });
    };
    //查询登录人
    RequisitionApplicationComponent.prototype.querylogin = function () {
        var _this = this;
        this.inventoryService.queryPersonal().subscribe(function (data) {
            _this.personalData = data;
            console.log(_this.personalData);
            _this.submitRequiestData['org'] = _this.personalData['organization'];
        });
    };
    // 查询人员表格数据
    RequisitionApplicationComponent.prototype.queryPersonList = function (oid) {
        var _this = this;
        this.publicService.getPersonList(oid).subscribe(function (data) {
            // console.log(data);
            _this.approver = data;
            _this.approver = _this.inventoryService.formatDropdownDatas(_this.approver);
            _this.submitRequiestData.approver = _this.approver[0]['label'];
            // console.log(this.submitRequiestData.approver);
        });
    };
    // 提交
    RequisitionApplicationComponent.prototype.addStorageSubmint = function (submitData, dataSource) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < dataSource.length; i++) {
            var temp = dataSource[i];
            var obj = {};
            obj['sid'] = temp['sid'];
            obj['pn'] = temp['pn'];
            obj['borrower'] = temp['borrower'];
            obj['borrower_pid'] = temp['borrower_pid'];
            obj['location'] = temp['location'];
            obj['location_id'] = temp['location_id'];
            obj['keeper'] = temp['keeper'];
            obj['keeprer_pid'] = temp['keeprer_pid'];
            obj['quantity'] = temp['quantity'];
            inventories.push(obj);
        }
        submitData.inventories = inventories;
        this.inventoryService.requestionAddDelivery(submitData).subscribe(function () {
            _this.router.navigate(['../requestionOverview'], { queryParams: { title: '领用总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    //保存
    RequisitionApplicationComponent.prototype.addDeviceSave = function (data) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < this.dataSource.length; i++) {
            var temp = this.dataSource[i];
            var obj = {};
            obj['sid'] = temp['sid'];
            obj['pn'] = temp['pn'];
            obj['location'] = temp['location'];
            obj['location_id'] = temp['location_id'];
            obj['keeper'] = temp['keeper'];
            obj['keeprer_pid'] = temp['keeprer_pid'];
            obj['quantity'] = temp['quantity'];
            inventories.push(obj);
        }
        data.inventories = inventories;
        this.inventoryService.queryRequesttionDeliverySave(data).subscribe(function (data) {
            _this.router.navigate(['../requestionOverview'], { queryParams: { title: '领用总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    //修改保存方法
    RequisitionApplicationComponent.prototype.updateDeviceSave = function (submitData, dataSource) {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < dataSource.length; i++) {
            var temp = dataSource[i];
            var obj = {};
            obj['pn'] = temp['pn'];
            obj['sid'] = temp['sid'];
            obj['location'] = temp['location'];
            obj['location_id'] = temp['location_id'];
            obj['keeper'] = temp['keeper'];
            obj['keeprer_pid'] = temp['keeprer_pid'];
            obj['quantity'] = temp['quantity'];
            obj['delivery_sid'] = temp['delivery_sid'];
            obj['borrower'] = temp['borrower'];
            obj['borrower_pid'] = temp['borrower_pid'];
            obj['return_quantity'] = temp['return_quantity'];
            obj['balance'] = temp['balance'];
            obj['name'] = temp['name'];
            obj['catagory'] = temp['catagory'];
            obj['supplier'] = temp['supplier'];
            obj['brand'] = temp['brand'];
            obj['model'] = temp['model'];
            obj['specification'] = temp['specification'];
            inventories.push(obj);
        }
        submitData.inventories = inventories;
        this.inventoryService.queryDeliveryMod(submitData).subscribe(function (data) {
            _this.router.navigate(['../requestionOverview'], { queryParams: { title: '领用总览' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    RequisitionApplicationComponent.prototype.queryMeterial = function () {
        var _this = this;
        this.inventoryService.queryMaterialInfocater().subscribe(function (data) {
            if (data['紧急度']) {
                _this.urgency = data['紧急度'];
                _this.urgency = _this.inventoryService.formatDropdownData(_this.urgency);
            }
            else {
                _this.urgency = [];
            }
            _this.submitRequiestData.urgency = _this.urgency[0]['label'];
        });
    };
    RequisitionApplicationComponent.prototype.deleteStorage = function () {
        for (var i = 0; i < this.selectInsepections.length; i++) {
            for (var j = 0; j < this.dataSource.length; j++) {
                if (this.selectInsepections[i]['sid'] === this.dataSource[j]['sid']) {
                    this.dataSource.splice(j, 1);
                    this.totalRecords = this.dataSource.length;
                    this.inspections = this.dataSource.slice(0, 10);
                    this.selectInsepections = [];
                }
            }
        }
    };
    RequisitionApplicationComponent.prototype.updatedeleteStorage = function () {
        for (var i = 0; i < this.selectInsepections.length; i++) {
            for (var j = 0; j < this.editDataSource.length; j++) {
                if (this.selectInsepections[i]['sid'] === this.editDataSource[j]['sid']) {
                    this.editDataSource.splice(j, 1);
                    this.totalRecords = this.dataSource.length;
                    this.editInspections = this.editDataSource.slice(0, 10);
                    this.selectInsepections = [];
                }
            }
        }
    };
    RequisitionApplicationComponent.prototype.loadCarsLazy = function (event) {
        //in a real application, make a remote request to load data using state metadata from event
        //event.first = First row offset
        //event.rows = Number of rows per page
        //event.sortField = Field name to sort with
        //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
        var _this = this;
        //imitate db connection over a network
        setTimeout(function () {
            if (_this.dataSource) {
                _this.inspections = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 0);
    };
    RequisitionApplicationComponent.prototype.editloadCarsLazy = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.editDataSource) {
                _this.editInspections = _this.editDataSource.slice(event.first, (event.first + event.rows));
            }
        }, 0);
    };
    RequisitionApplicationComponent.prototype.formSubmit = function () {
        if (this.viewState === 'apply') {
            this.addStorageSubmint(this.submitRequiestData, this.dataSource);
        }
        else if (this.viewState === 'update') {
            this.addStorageSubmint(this.borrowData, this.editDataSource);
        }
    };
    RequisitionApplicationComponent.prototype.formSave = function () {
        if (this.viewState === 'apply') {
            this.addDeviceSave(this.submitRequiestData);
        }
        else if (this.viewState === 'update') {
            this.updateDeviceSave(this.borrowData, this.editDataSource);
        }
    };
    RequisitionApplicationComponent.prototype.searchSuggest = function (searchText, name) {
        var _this = this;
        this.qureyModel = {
            label: name,
            value: searchText.query
        };
        for (var key in this.qureyModel) {
            this.qureyModel[key] = this.qureyModel[key].trim();
        }
        this.inventoryService.queryInventory(this.qureyModel).subscribe(function (data) {
            switch (name) {
                case 'sid':
                    _this.brandoptions = data;
                    console.log(_this.brandoptions);
                    break;
            }
        });
    };
    RequisitionApplicationComponent.prototype.addDev = function (metail) {
        this.showBorrowItems = false;
        for (var i = 0; i < metail.length; i++) {
            var temp = metail[i];
            this.dataSource.push(temp);
            this.editDataSource.push(temp);
        }
        this.totalRecords = this.dataSource.length;
        this.totalRecords = this.editDataSource.length;
        this.inspections = this.dataSource.slice(0, 10);
        this.editInspections = this.editDataSource.slice(0, 10);
    };
    RequisitionApplicationComponent.prototype.showAddStorageMask = function () {
        this.showBorrowItems = !this.showBorrowItems;
    };
    RequisitionApplicationComponent.prototype.closeBorrowItemsMask = function (bool) {
        this.showBorrowItems = bool;
    };
    //确认出库
    RequisitionApplicationComponent.prototype.sureApproved = function () {
        var _this = this;
        var inventories = [];
        for (var i = 0; i < this.selectInsepections.length; i++) {
            var temp = this.selectInsepections[i];
            inventories.push(temp);
        }
        var queryModel = {
            "sid": this.borrowData['sid'],
            "inventories": inventories
        };
        this.confirmationService.confirm({
            message: '确定出库吗?',
            rejectVisible: true,
            accept: function () {
                _this.inventoryService.queryBorrowSure(queryModel).subscribe(function () {
                    _this.borrowData['status'] = "";
                    _this.eventBusService.requstion.next(_this.borrowData['status']);
                    _this.router.navigate(['../requestionOverview'], { relativeTo: _this.route });
                }, function (err) {
                    _this.confirmationService.confirm({
                        message: err['message'],
                        rejectVisible: false,
                    });
                });
            },
            reject: function () { }
        });
    };
    RequisitionApplicationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-requisition-application',
            template: __webpack_require__("../../../../../src/app/inventory/requisition-application/requisition-application.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/requisition-application/requisition-application.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_5__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_6__services_event_bus_service__["a" /* EventBusService */]])
    ], RequisitionApplicationComponent);
    return RequisitionApplicationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/storage-approved/storage-approved.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes\">\n  <form [formGroup]=\"borrowapprovedForm\"  >\n    <p-panel >\n      <p-header>\n        <div class=\"ui-helper-clearfix\">\n          <span class=\"ui-panel-title\" style=\"font-size:16px;display:inline-block;margin-top:2px\"></span>\n          <button class=\"save\"  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\"  > </button>\n          <button class=\"save\" pButton [ngStyle]=\"{'float':'right'}\"  *ngFor=\"let optbutton of optButtons\"  label=\"{{optbutton.label}}\" [disabled]=\"!borrowapprovedForm.valid\" (click)=\"approvedOption(optbutton.status)\"></button>\n        </div>\n      </p-header>\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >入库单号:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"sid\"  name=\"sid\"  type=\"text\" pInputText   placeholder=\"入库单号\" value=\"{{currentMaterial?.sid}}\" readonly>\n          </div>\n\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >入库经手人:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input formControlName=\"creater\" name=\"creater\"  type=\"text\" pInputText   placeholder=\"入库经手人\" value=\"{{currentMaterial?.creator}}\"readonly >\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >库存位置:</label>\n          </div>\n          <div class=\"ui-grid-col-5\">\n            <input  name=\"location\" formControlName=\"location\"  type=\"text\" pInputText   placeholder=\"库存位置\" value=\"{{currentMaterial?.location}}\" readonly >\n\n          </div>\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >仓库管理员:</label>\n          </div>\n          <div class=\"ui-grid-col-5 text-right\">\n            <input formControlName=\"keeper\" type=\"text\" pInputText  name=\"keeper\" placeholder=\"仓库管理员\"\n                   readonly class=\"cursor_not_allowed\"  value=\"{{currentMaterial?.keeper}}\" readonly />\n\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label >简述:</label>\n          </div>\n          <div class=\"ui-grid-col-11\">\n            <textarea formControlName=\"remarks\" pInputTextarea name=\"remarks\" type=\"text\" value=\"{{currentMaterial?.remarks}}\"readonly></textarea>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-1 text-right\">\n            <label ><span>*</span>审批意见</label>\n          </div>\n          <div class=\"ui-grid-col-11\">\n            <textarea formControlName=\"approve_remarks\" pInputTextarea type=\"text\" name=\"approve_remarks\"  [(ngModel)]=\"approvedModel['approve_remarks']\" ></textarea>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!borrowapprovedForm.controls['approve_remarks'].valid&&(!borrowapprovedForm.controls['approve_remarks'].untouched)\" >\n              <i class=\"fa fa-close\"></i>\n              审批意见必填\n            </div>\n          </div>\n        </div>\n      </div>\n    </p-panel>\n  </form>\n <div class=\"ui-g\">\n   <div class=\"mysearch\">\n     <label>入库列表</label>\n   </div>\n </div>\n\n  <!--<p-dataTable emptyMessage=\"没有数据\" [value]=\"dataSource\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[10,20,30]\"-->\n               <!--[totalRecords]=\"totalRecords\" (onLazyLoad)=\"loadCarsLazy($event)\" [stacked]=\"stacked\" [responsive]=\"true\" >-->\n    <p-dataTable emptymessage=\"当前没有数据\" [value]=\"inventories\" (onLazyLoad)=\"loadCarsLazy($event)\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\"\n                 [totalRecords]=\"totalRecords\"  >\n    <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n\n    </p-dataTable>\n</div>\n<app-add-storage\n  *ngIf=\"showAddStorage\"\n  (closeAddStorage)=\"closeAddStorageMask($event)\">\n</app-add-storage>\n<app-choose-warehouse-manager\n  *ngIf=\"showWarehouseManager\"\n  (closeChooseWarehouse)=\"closeWarehouseManagerMask($event)\">\n\n</app-choose-warehouse-manager>\n\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/storage-approved/storage-approved.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/deep/.ui-widget-header {\n  background-color: white;\n  background: linear-gradient(to bottom, #ffffff 0%, #ffffff 100%); }\n\n.mysearch {\n  margin-bottom: 20px;\n  padding: 10px;\n  margin-top: 15px; }\n\nlabel > span {\n  color: red; }\n\n.save {\n  width: 100px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/storage-approved/storage-approved.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageApprovedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StorageApprovedComponent = (function () {
    function StorageApprovedComponent(route, router, inventoryService, eventBusService) {
        this.route = route;
        this.router = router;
        this.inventoryService = inventoryService;
        this.eventBusService = eventBusService;
        this.inventories = [];
        this.fb = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]();
    }
    StorageApprovedComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.borrowapprovedForm = this.fb.group({
            'sid': [''],
            'creater': [''],
            'location': [''],
            'keeper': [''],
            'remarks': [''],
            'approve_remarks': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
        });
        this.cols = [
            { field: 'pn', header: '编号' },
            { field: 'name', header: '名称' },
            { field: 'catagory', header: '分类' },
            { field: 'brand', header: '品牌' },
            { field: 'model', header: '型号' },
            { field: 'specification', header: '规格' },
            { field: 'quantity', header: '数量' },
            { field: 'supplier', header: '供应商' },
        ];
        this.approvedModel = {
            "sid": "",
            "approve_remarks": "",
            "status": ""
        };
        this.route.queryParams.subscribe(function (parms) {
            if (parms['sid']) {
                _this.sid = parms['sid'];
            }
            _this.approvedModel.sid = _this.sid;
            _this.inventoryService.queryStorage(_this.sid).subscribe(function (data) {
                _this.currentMaterial = data;
                _this.dataSource = _this.currentMaterial['inventories'] ? _this.currentMaterial['inventories'] : [];
                if (!_this.dataSource) {
                    _this.dataSource = [];
                    _this.totalRecords = 0;
                }
                // this.dataSource = this.currentMaterial.inventories;
                _this.inventories = _this.dataSource.slice(0, 10);
                _this.totalRecords = _this.dataSource.length;
            });
        });
        this.optButtons = [
            { label: '驳回', status: '驳回' },
            { label: '拒绝', status: '拒绝' },
            { label: '通过', status: '待入库' },
        ];
    };
    StorageApprovedComponent.prototype.loadCarsLazy = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.dataSource) {
                _this.inventories = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 0);
    };
    StorageApprovedComponent.prototype.goBack = function () {
        var sid = this.currentMaterial['sid'];
        this.currentMaterial['status'] = "";
        this.eventBusService.flow.next(this.currentMaterial['status']);
        this.router.navigate(['../overview'], { queryParams: { sid: sid, state: 'viewDetail', title: '入库审批' }, relativeTo: this.route });
    };
    StorageApprovedComponent.prototype.approvedOption = function (status) {
        var _this = this;
        this.approvedModel.status = status;
        this.inventoryService.queryStorageApproved(this.approvedModel).subscribe(function () {
            _this.currentMaterial['status'] = "";
            _this.eventBusService.flow.next(_this.currentMaterial['status']);
            _this.router.navigate(['../overview'], { relativeTo: _this.route });
        });
    };
    StorageApprovedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-storage-approved',
            template: __webpack_require__("../../../../../src/app/inventory/storage-approved/storage-approved.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/storage-approved/storage-approved.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */]])
    ], StorageApprovedComponent);
    return StorageApprovedComponent;
}());



/***/ }),

/***/ "../../../../../src/app/inventory/storage-overview/storage-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo clearfixes storageOverview\">\n  <div class=\"mysearch\">\n    <form>\n    <div class=\"ui-g\">\n      <div class=\"ui-g-4 ui-sm-12\">\n        <label class=\"ui-g-2 ui-sm-5 text-right\">入库单号:</label>\n        <div class=\"ui-g-4  ui-sm-7 ui-fluid\">\n          <input pInputText type=\"text\" name=\"sid\" placeholder=\"入库单号\" [(ngModel)]=\"queryModel.condition.sid\" />\n        </div>\n        <label class=\"ui-g-2 ui-sm-5 ui-no-padding-left-15px text-right\">经手人:</label>\n        <div class=\"ui-g-4 ui-sm-7 ui-fluid  ui-no-padding-right-15px \">\n          <input  pInputText type=\"text\" name=\"status\" placeholder=\"经手人\"  [(ngModel)]=\"queryModel.condition.creator\" />\n        </div>\n      </div>\n      <div class=\"ui-g-4 ui-sm-12\">\n        <label class=\"ui-g-3 ui-sm-5 text-right\">入库时间:</label>\n        <div class=\"ui-g-4  ui-sm-7 ui-fluid\">\n          <p-calendar  [showIcon]=\"true\" [locale]=\"zh\" name=\"time_start\"\n                       [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\"  dataType=\"string\"\n                       [minDate]=\"minDate\" [(ngModel)]=\"queryModel.condition.begin_time\"  >\n          </p-calendar>\n        </div>\n        <label class=\"ui-g-1 ui-sm-5\" >至:</label>\n        <div class=\"ui-g-4  ui-sm-7 ui-fluid \">\n          <p-calendar  [showIcon]=\"true\" [locale]=\"zh\" name=\"end_time\"\n                       [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\" dataType=\"string\"\n                       [minDate]=\"minDate\" [(ngModel)]=\"queryModel.condition.end_time\" >\n          </p-calendar>\n        </div>\n      </div>\n\n      <div class=\"ui-g-4 ui-sm-12\">\n        <label  class=\"ui-g-3 ui-sm-5 text-right\">状态:</label>\n        <div class=\"ui-g-5  ui-sm-7 ui-fluid \">\n          <p-dropdown [options]=\"MaterialStatusData\" [(ngModel)]=\"queryModel.condition.status\" [autoWidth]=\"false\" name=\"cycle\" (onChange)=\"suggestInsepectiones()\" ></p-dropdown>\n        </div>\n        <div class=\"ui-g-4 ui-sm-12 option\">\n          <button pButton type=\"button\" label=\"查询\" ngClass=\"ui-sm-12\" (click)=\"suggestInsepectiones()\" ></button>\n          <button pButton  label=\"清空\" ngClass=\"ui-sm-12\"  (click)=\"clearSearch()\"></button>\n        </div>\n\n      </div>\n    </div>\n    </form>\n  </div>\n  <div class=\"ui-g\">\n  <div class=\"ui-g-12 ui-md-12 ui-lg-12\">\n    <p-dataTable [value]=\"MaterialData\"\n                 [totalRecords]=\"totalRecords\"  [(selection)]=\"selectMaterial\" >\n      <!--<p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\" ></p-column>-->\n      <p-column  field=\"sid\" header=\"入库单号\" [sortable]=\"true\" >\n        <ng-template let-data=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n          <span (click)=\"viewBorrowApproved(data)\" class=\"curser\">{{data.sid}}</span>\n        </ng-template>\n      </p-column>\n      <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"  [sortable]=\"true\"></p-column>\n      <ng-template pTemplate=\"emptymessage\">\n        当前没有数据\n      </ng-template>\n      <p-column header=\"操作\">\n        <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n          <button pButton type=\"button\"  label=\"编辑\"  (click)=\"updateOption(MaterialData[i])\" *ngIf=\"MaterialData[i]['status'] === '新建' || MaterialData[i]['status'] === '驳回'\"  ></button>\n          <button pButton type=\"button\"  label=\"删除\"  (click)=\"deleteMaterial(MaterialData[i])\"  *ngIf=\"MaterialData[i]['status'] === '新建'\"   ></button>\n          <button pButton type=\"button\"  label=\"审批\" (click)=\"storageApproved(MaterialData[i])\"  *ngIf=\"MaterialData[i]['status'] === '待审批'\" ></button>\n          <button pButton type=\"button\"  label=\"入库\"  (click)=\"sureApproved(MaterialData[i])\"  *ngIf=\"MaterialData[i]['status'] === '待入库'\"  ></button>\n          <button pButton type=\"button\"  label=\"查看\" (click)=\"viewBorrowApproved(MaterialData[i])\"     ></button>\n        </ng-template>\n      </p-column>\n    </p-dataTable>\n    <p-paginator rows=\"10\" [totalRecords]=\"totalRecords\" [rowsPerPageOptions]=\"[10,20,30]\" (onPageChange)=\"paginate($event)\"></p-paginator>\n  </div>\n  </div>\n</div>\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/inventory/storage-overview/storage-overview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (min-width: 1024px) {\n  .storageOverview /deep/ table thead tr th:last-child {\n    width: 18%; } }\n\n.mysearch {\n  margin-bottom: 20px;\n  padding: 10px;\n  background: #fff; }\n\n.storageOverview /deep/ .ui-datatable th.ui-state-active {\n  color: #555555;\n  background-color: #EFF0F3; }\n\n.curser {\n  color: #39b9c6;\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inventory/storage-overview/storage-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inventory_service__ = __webpack_require__("../../../../../src/app/inventory/inventory.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var StorageOverviewComponent = (function (_super) {
    __extends(StorageOverviewComponent, _super);
    function StorageOverviewComponent(inventoryService, confirmationService, messageService, eventBusService, router, route) {
        var _this = _super.call(this, confirmationService, messageService) || this;
        _this.inventoryService = inventoryService;
        _this.confirmationService = confirmationService;
        _this.messageService = messageService;
        _this.eventBusService = eventBusService;
        _this.router = router;
        _this.route = route;
        _this.chooseCids = [];
        _this.selectMaterial = [];
        _this.statusNames = [];
        return _this;
    }
    StorageOverviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            dayNamesShort: ["一", "二", "三", "四", "五", "六", "七"],
            dayNamesMin: ["一", "二", "三", "四", "五", "六", "七"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
        };
        this.queryModel = {
            "condition": {
                "sid": "",
                "status": "",
                "begin_time": "",
                "end_time": "",
                "creator": ""
            },
            "page": {
                "page_size": "10",
                "page_number": "1"
            }
        };
        this.cols = [
            // {field: 'sid', header: '入库单号'},
            { field: 'entering_time', header: '入库时间' },
            { field: 'creator', header: '入库经手人' },
            { field: 'location', header: '库存位置' },
            { field: 'status', header: '状态' },
            { field: 'approver', header: '审批人' },
            { field: 'audit_time', header: '审批时间' },
        ];
        this.eventBusService.updateborrow.subscribe(function (data) {
            _this.queryModel.condition.status = data;
            _this.queryMaterialDate();
        });
        this.queryMaterialDate();
        this.queryMaterialStatusDate();
    };
    StorageOverviewComponent.prototype.clearSearch = function () {
        this.queryModel.condition.sid = '';
        this.queryModel.condition.begin_time = '';
        this.queryModel.condition.end_time = '';
        this.queryModel.condition.creator = '';
    };
    //搜索功能
    StorageOverviewComponent.prototype.suggestInsepectiones = function () {
        this.queryModel.page.page_number = '1';
        this.queryModel.page.page_size = '10';
        this.queryMaterialDate();
    };
    //分页查询
    StorageOverviewComponent.prototype.paginate = function (event) {
        this.chooseCids = [];
        this.queryModel.page.page_number = String(++event.page);
        this.queryModel.page.page_size = String(event.rows);
        this.queryMaterialDate();
    };
    StorageOverviewComponent.prototype.queryMaterialDate = function () {
        var _this = this;
        this.inventoryService.queryStorageView(this.queryModel).subscribe(function (data) {
            if (!data['items']) {
                _this.MaterialData = [];
                _this.selectMaterial = [];
                _this.totalRecords = 0;
            }
            else {
                _this.MaterialData = data.items;
                _this.totalRecords = data.page.total;
            }
        });
    };
    StorageOverviewComponent.prototype.queryMaterialStatusDate = function () {
        var _this = this;
        this.inventoryService.queryInventoryStatus().subscribe(function (data) {
            if (!data) {
                _this.MaterialStatusData = [];
            }
            else {
                _this.MaterialStatusData = data;
                _this.MaterialStatusData = _this.inventoryService.formatDropdownData(_this.MaterialStatusData);
            }
        });
    };
    StorageOverviewComponent.prototype.updateOption = function (current) {
        var sid = current['sid'];
        this.eventBusService.flow.next(current.status);
        this.router.navigate(['../equipment'], { queryParams: { 'sid': sid, title: '编辑', state: 'update' }, relativeTo: this.route });
    };
    //确认入库
    StorageOverviewComponent.prototype.sureApproved = function (current) {
        var _this = this;
        var sid = current['sid'];
        var status = current['status'];
        this.eventBusService.flow.next(current.status);
        this.confirmationService.confirm({
            message: '是否需要调整库存位置?',
            rejectVisible: true,
            accept: function () {
                _this.router.navigate(['../location'], { queryParams: { 'sid': sid, 'status': status, title: '库存位置' }, relativeTo: _this.route });
            },
            reject: function () {
                _this.inventoryService.queryStorageSure(sid).subscribe(function () {
                    _this.MaterialStatusData['status'] = "";
                    _this.eventBusService.flow.next(_this.MaterialStatusData['status']);
                    _this.selectMaterial = [];
                    _this.queryMaterialDate();
                });
            }
        });
    };
    //审批
    StorageOverviewComponent.prototype.storageApproved = function (current) {
        var sid = current['sid'];
        this.eventBusService.flow.next(current.status);
        this.router.navigate(['../approved'], { queryParams: { 'sid': sid }, relativeTo: this.route });
    };
    StorageOverviewComponent.prototype.deleteMaterial = function (current) {
        var _this = this;
        var sid = current['sid'];
        //请求成功后删除本地数据
        this.confirm('确认删除吗?', function () { _this.deleteoption(sid); });
    };
    StorageOverviewComponent.prototype.deleteoption = function (sid) {
        var _this = this;
        this.inventoryService.deleteStorage(sid).subscribe(function () {
            _this.queryMaterialDate();
            _this.alert('删除成功');
            // this.selectMaterial=[];
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，请联系管理员或稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    //查看
    StorageOverviewComponent.prototype.viewBorrowApproved = function (current) {
        var sid = current['sid'];
        this.router.navigate(['../equipment'], { queryParams: { sid: sid, state: 'viewDetail', title: '查看' }, relativeTo: this.route });
    };
    StorageOverviewComponent.prototype.onOperate = function (current) {
        var sid = current['sid'];
        // this.showViewDetailMask = !this.showViewDetailMask;
        this.router.navigate(['../equipment'], { queryParams: { sid: sid, state: 'viewDetail', title: '查看' }, relativeTo: this.route });
    };
    StorageOverviewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-storage-overview',
            template: __webpack_require__("../../../../../src/app/inventory/storage-overview/storage-overview.component.html"),
            styles: [__webpack_require__("../../../../../src/app/inventory/storage-overview/storage-overview.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__inventory_service__["a" /* InventoryService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__["MessageService"],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"]])
    ], StorageOverviewComponent);
    return StorageOverviewComponent;
}(__WEBPACK_IMPORTED_MODULE_6__base_page__["a" /* BasePage */]));



/***/ })

});
//# sourceMappingURL=inventory.module.chunk.js.map