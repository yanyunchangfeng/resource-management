webpackJsonp(["system.module"],{

/***/ "../../../../../src/app/system/add-inspection-plan-tree/add-inspection-plan-tree.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"部门选择\" [(visible)]=\"display\" modal=\"modal\" width=\"width\" [responsive]=\"true\" (onHide)=\"closeInspectionMask(false)\">\n  <p-tree [value]=\"filesTree4\"\n          selectionMode=\"checkbox\"\n          [(selection)]=\"selected\"\n          (onNodeExpand)=\"nodeExpand($event)\"\n  ></p-tree>\n  <!--<div>Selected Nodes: <span *ngFor=\"let file of selected\">{{file.label}} </span></div>-->\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" (click)=\"formSubmit()\" label=\"确定\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeInspectionMask(false)\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/system/add-inspection-plan-tree/add-inspection-plan-tree.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/add-inspection-plan-tree/add-inspection-plan-tree.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddInspectionPlanTreeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddInspectionPlanTreeComponent = (function () {
    function AddInspectionPlanTreeComponent(systemService) {
        this.systemService = systemService;
        this.closeAddMask = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.addTree = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddInspectionPlanTreeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.windowSize = window.innerWidth;
        if (this.windowSize < 1024) {
            this.width = this.windowSize * 0.9;
        }
        else {
            this.width = this.windowSize * 0.8;
        }
        this.display = true;
        this.queryModel = {
            'id': '',
            'dep': ''
        };
        this.systemService.getDepartmentDatas().subscribe(function (data) {
            if (!data) {
                data = [];
            }
            _this.filesTree4 = data;
        });
    };
    //关闭遮罩层
    AddInspectionPlanTreeComponent.prototype.closeInspectionMask = function (bool) {
        this.closeAddMask.emit(bool);
    };
    // 组织树懒加载
    AddInspectionPlanTreeComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.systemService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    AddInspectionPlanTreeComponent.prototype.formSubmit = function () {
        var arr = [];
        for (var _i = 0, _a = this.selected; _i < _a.length; _i++) {
            var key = _a[_i];
            var obj = {};
            obj['label'] = key['label'];
            obj['oid'] = key['oid'];
            arr.push(obj);
        }
        console.log(arr);
        this.addTree.emit(arr);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('expandingTree'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__["Tree"])
    ], AddInspectionPlanTreeComponent.prototype, "expandingTree", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddInspectionPlanTreeComponent.prototype, "closeAddMask", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AddInspectionPlanTreeComponent.prototype, "addTree", void 0);
    AddInspectionPlanTreeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-inspection-plan-tree',
            template: __webpack_require__("../../../../../src/app/system/add-inspection-plan-tree/add-inspection-plan-tree.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/add-inspection-plan-tree/add-inspection-plan-tree.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__system_service__["a" /* SystemService */]])
    ], AddInspectionPlanTreeComponent);
    return AddInspectionPlanTreeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/cognate-role/cognate-role.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\r\n  margin-bottom: 1vw; }\r\n\r\n.padding-tblr {\r\n  padding: .25em .5em; }\r\n\r\n.start_red {\r\n  color: red; }\r\n\r\n.birthday {\r\n  display: inline-block; }\r\n\r\n@media screen and (max-width: 1366px) {\r\n  .ui-grid-col-1 {\r\n    width: 11.33333%; } }\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/cognate-role/cognate-role.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"{{title}}\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"400\" [minWidth]=\"200\" (onHide)=\"closeCogRoleMask(false)\">\n  <!--<p-panel header=\"{{title}}\">-->\n    <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n      <form>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-12\">\n            <p-dataTable [value]=\"tableDatas\"\n                         [responsive]=\"true\"  id=\"manageTable\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[10,15,20]\" [(selection)]=\"selectedRoles\">\n              <!--<p-footer>-->\n              <!--<p-paginator rows=\"10\" totalRecords=\"120\" [rowsPerPageOptions]=\"[10,20,30]\" pageLinkSize=\"10\"></p-paginator>-->\n              <!--</p-footer>-->\n              <p-column selectionMode=\"multiple\" ></p-column>\n              <p-column field=\"rid\" header=\"编号\" [sortable]=\"true\"></p-column>\n              <p-column field=\"name\" header=\"名称\" [sortable]=\"true\"></p-column>\n              <ng-template pTemplate=\"emptymessage\">\n                当前没有数据\n              </ng-template>\n            </p-dataTable>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\"></div>\n          <div class=\"ui-grid-col-3 padding-tblr\">\n            <button pButton type=\"submit\" label=\"确认\" (click)=\"formSubmit(false)\" *ngIf=\"state === 'cognate'\"></button>\n          </div>\n          <div class=\"ui-grid-col-3 padding-tblr\">\n            <button pButton type=\"button\" label=\"取消\" (click)=\"closeCogRoleMask(false)\"></button>\n          </div>\n        </div>\n      </form>\n    </div>\n  <!--</p-panel>-->\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/system/cognate-role/cognate-role.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CognateRoleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CognateRoleComponent = (function () {
    function CognateRoleComponent(systemService, storageService, fb, confirmationService) {
        this.systemService = systemService;
        this.storageService = storageService;
        this.fb = fb;
        this.confirmationService = confirmationService;
        this.closeCognateRole = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.cognateRole = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //保存关联角色
        this.selectedRoles = []; //选中的角色
    }
    CognateRoleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.display = true;
        this.systemService.getPerRoles(this.currentPid.pid).subscribe(function (data) {
            if (_this.state == 'cognate') {
                _this.title = _this.currentPid.name + '关联角色';
                _this.tableDatas = data;
                //选中已拥有的角色
                for (var i = 0; i < data.length; i++) {
                    if (data[i].checked) {
                        _this.selectedRoles.push(data[i]);
                    }
                }
            }
            else {
                _this.title = _this.currentPid.name + '角色';
                //只显示已拥有的角色
                var dataFilters = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].checked) {
                        dataFilters.push(data[i]);
                    }
                }
                _this.tableDatas = dataFilters;
            }
        });
    };
    //确定
    CognateRoleComponent.prototype.formSubmit = function (bool) {
        var _this = this;
        this.systemService.saveCognateRoles(this.selectedRoles, this.currentPid.pid).subscribe(function () {
            _this.cognateRole.emit(bool);
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    CognateRoleComponent.prototype.closeCogRoleMask = function (bool) {
        this.closeCognateRole.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], CognateRoleComponent.prototype, "closeCognateRole", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], CognateRoleComponent.prototype, "cognateRole", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CognateRoleComponent.prototype, "currentPid", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], CognateRoleComponent.prototype, "state", void 0);
    CognateRoleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-cognate-role',
            template: __webpack_require__("../../../../../src/app/system/cognate-role/cognate-role.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/cognate-role/cognate-role.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__system_service__["a" /* SystemService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["ConfirmationService"]])
    ], CognateRoleComponent);
    return CognateRoleComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/jur-edit/jur-edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\r\n  margin-bottom: 1vw; }\r\n\r\n.padding-tblr {\r\n  padding: .25em .5em; }\r\n\r\n.start_red {\r\n  color: red; }\r\n\r\n.birthday {\r\n  display: inline-block; }\r\n\r\n@media screen and (max-width: 1366px) {\r\n  .ui-grid-col-1 {\r\n    width: 11.33333%; } }\r\n.ui-dialog.ui-widget-content {\r\n  top: 100px !important; }\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/jur-edit/jur-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"{{title}}\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"800\" [minWidth]=\"200\" (onHide)=\"closeJurEditMask(false)\">\n  <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px;max-height: 300px;overflow-y: auto;\">\n    <!--<form [formGroup]=\"jur\">-->\n      <div class=\"ui-grid-row margin-bottom-1vw\">\n        <div class=\"ui-grid-col-6\">\n          <h4 style=\"margin-bottom: 15px;\">所有权限</h4>\n          <p-tree [value]=\"allJurs\" selectionMode=\"checkbox\" [(selection)]=\"selected\" (onNodeExpand)=\"nodeExpand($event)\" (onNodeSelect) = \"NodeSelect($event)\" (onNodeUnselect) = \"onNodeUnselect($event)\"></p-tree>\n        </div>\n        <div class=\"ui-grid-col-6\">\n          <h4 style=\"margin-bottom: 15px;\">已选权限</h4>\n          <p-tree [value]=\"ownJurs\" selectionMode=\"single\" (onNodeExpand)=\"ownNodeExpand($event)\"></p-tree>\n        </div>\n      </div>\n    <!--</form>-->\n  </div>\n  <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\">\n    <div class=\"ui-grid-row margin-bottom-1vw\">\n      <div class=\"ui-grid-col-6\"></div>\n      <div class=\"ui-grid-col-3 padding-tblr\">\n        <button pButton type=\"submit\" label=\"确认\" (click)=\"formSubmit(false)\"></button>\n      </div>\n      <div class=\"ui-grid-col-3 padding-tblr\">\n        <button pButton type=\"button\" label=\"取消\" (click)=\"closeJurEditMask(false)\"></button>\n      </div>\n    </div>\n  </div>\n</p-dialog>\n\n<p-growl [(value)]=\"msgs\"></p-growl>\n"

/***/ }),

/***/ "../../../../../src/app/system/jur-edit/jur-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JurEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JurEditComponent = (function () {
    function JurEditComponent(systemService, storageService, fb, confirmationService) {
        this.systemService = systemService;
        this.storageService = storageService;
        this.fb = fb;
        this.confirmationService = confirmationService;
        this.selectedNodeData = []; //已选中权限的数据
        this.selectedNodeId = []; //已选中权限的ID
        this.closeJurEdit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.updateJur = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //修改角色
    }
    JurEditComponent.prototype.ngOnInit = function () {
        this.display = true;
        //查询所有权限树的数据
        this.queryAllJurTree('', '');
        //查询已拥有权限树的数据
        this.queryOwnJurTree('', '', this.currentOrg.rid);
        // console.log(this.currentOrg)
        if (this.state == 'update') {
            this.title = '编辑权限';
        }
    };
    //确定
    JurEditComponent.prototype.formSubmit = function (bool) {
        if (this.state === 'update') {
            this.updateJurNode(bool);
        }
    };
    //修改权限
    JurEditComponent.prototype.updateJurNode = function (bool) {
        var _this = this;
        this.selectedNodeId = [];
        this.getSelectedNodeId(this.selectedNodeData);
        if (!this.selectedNodeId.length) {
            this.showWarn('请选择权限！');
        }
        else {
            this.systemService.updateJur(this.currentOrg.rid, this.selectedNodeId).subscribe(function () {
                _this.updateJur.emit(_this.currentOrg);
                _this.closeJurEdit.emit(bool);
            });
        }
    };
    JurEditComponent.prototype.closeJurEditMask = function (bool) {
        this.closeJurEdit.emit(bool);
    };
    // 所有权限树,懒加载
    JurEditComponent.prototype.nodeExpand = function (event) {
        if (event.node && !event.node.children) {
            this.systemService.getAllJurTreeData(event.node.mid, event.node.dep).subscribe(function (data) {
                // console.log(data);
                event.node.children = data;
            });
        }
    };
    // 已拥有权限树,懒加载
    JurEditComponent.prototype.ownNodeExpand = function (event) {
        if (event.node && !event.node.children) {
            this.systemService.getOwnJursTreeData(event.node.mid, event.node.dep, this.currentOrg.rid).subscribe(function (data) {
                // console.log(data);
                event.node.children = data;
            });
        }
    };
    // 所有权限树,选中
    JurEditComponent.prototype.NodeSelect = function (event) {
        this.selectedNodeData = [];
        var treeData = this.allJurs;
        for (var i = 0; i < this.selected.length; i++) {
            this.checkedNode(treeData, this.selected[i].id, true);
        }
        if (this.selected.length) {
            this.getSelectedNodeData(treeData, this.selectedNodeData);
            this.ownJurs = this.selectedNodeData;
        }
        else {
            this.ownJurs = [];
        }
    };
    // 所有权限树,取消选中
    JurEditComponent.prototype.onNodeUnselect = function (event) {
        this.selectedNodeData = [];
        var treeData = this.allJurs;
        this.checkedNode(treeData, event.node.id, false);
        if (this.selected.length) {
            this.getSelectedNodeData(treeData, this.selectedNodeData);
            this.ownJurs = this.selectedNodeData;
        }
        else {
            this.ownJurs = [];
        }
    };
    //复制对象
    JurEditComponent.prototype.copyObj = function (source) {
        var retObj = {};
        for (var key in source) {
            retObj[key] = source[key];
        }
        return retObj;
    };
    //获取选中节点的数据
    JurEditComponent.prototype.getSelectedNodeData = function (data, cdata) {
        for (var i = 0, len = data.length; i < len; i++) {
            if (data[i].checked) {
                cdata.push(this.copyObj(data[i]));
                cdata[cdata.length - 1].children = [];
                if (Object.prototype.toString.call(data[i].children) === '[object Array]') {
                    this.getSelectedNodeData(data[i].children, cdata[cdata.length - 1].children);
                }
            }
        }
    };
    //选中或者取消节点
    JurEditComponent.prototype.checkedNode = function (data, id, bool) {
        for (var i = 0, len = data.length; i < len; i++) {
            if (data[i].id == id) {
                data[i].checked = bool;
                if (bool) {
                    this.selectedParentNode(data[i], bool);
                }
                else {
                    this.noSelectedParentNode(data, bool);
                    // break;
                }
            }
            else if (Object.prototype.toString.call(data[i].children) === '[object Array]') {
                this.checkedNode(data[i].children, id, bool);
            }
        }
    };
    //取消选中父节点
    JurEditComponent.prototype.noSelectedParentNode = function (data, bool) {
        for (var j = 0; j < data.length; j++) {
            if (data[j].checked && data[j].parent) {
                data[j].parent.checked = !bool;
                break;
            }
            else {
                data[j].parent.checked = bool;
            }
        }
        if (data[0].parent && data[0].parent.parent) {
            this.noSelectedParentNode(data[0].parent.parent.children, bool);
        }
    };
    //选中父节点
    JurEditComponent.prototype.selectedParentNode = function (data, bool) {
        if (data.parent) {
            data.parent.checked = bool;
            if (data.parent.parent) {
                this.selectedParentNode(data.parent, bool);
            }
        }
    };
    //获取选中节点的id
    JurEditComponent.prototype.getSelectedNodeId = function (data) {
        for (var i = 0, len = data.length; i < len; i++) {
            if (data[i].checked) {
                this.selectedNodeId.push(data[i].id);
                if (Object.prototype.toString.call(data[i].children) === '[object Array]') {
                    this.getSelectedNodeId(data[i].children);
                }
            }
        }
    };
    //查询所有权限树数据
    JurEditComponent.prototype.queryAllJurTree = function (mid, dep) {
        var _this = this;
        this.systemService.getAllJurTreeData(mid, dep).subscribe(function (datas) {
            // console.log(datas);
            _this.allJurs = datas;
        });
    };
    //查询已拥有权限树数据
    JurEditComponent.prototype.queryOwnJurTree = function (mid, dep, rid) {
        var _this = this;
        this.systemService.getOwnJursTreeData(mid, dep, rid).subscribe(function (datas) {
            console.log(datas);
            _this.ownJurs = datas;
        });
    };
    JurEditComponent.prototype.showWarn = function (operator) {
        this.msgs = [];
        this.msgs.push({ severity: 'warn', summary: '注意', detail: operator });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], JurEditComponent.prototype, "closeJurEdit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], JurEditComponent.prototype, "updateJur", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], JurEditComponent.prototype, "currentOrg", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], JurEditComponent.prototype, "state", void 0);
    JurEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-jur-edit',
            template: __webpack_require__("../../../../../src/app/system/jur-edit/jur-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/jur-edit/jur-edit.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__system_service__["a" /* SystemService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["ConfirmationService"]])
    ], JurEditComponent);
    return JurEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/jur-look/jur-look.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\r\n  margin-bottom: 1vw; }\r\n\r\n.padding-tblr {\r\n  padding: .25em .5em; }\r\n\r\n.start_red {\r\n  color: red; }\r\n\r\n.birthday {\r\n  display: inline-block; }\r\n\r\n@media screen and (max-width: 1366px) {\r\n  .ui-grid-col-1 {\r\n    width: 11.33333%; } }\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/jur-look/jur-look.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"{{title}}\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"600\" [minWidth]=\"200\" (onHide)=\"closeJurLookMask(false)\">\n  <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n    <!--<form [formGroup]=\"jur\">-->\n    <div class=\"ui-grid-row margin-bottom-1vw\">\n      <div class=\"ui-grid-col-12\">\n        <!--<h4>已选权限</h4>-->\n        <!--<p-tree [value]=\"ownJurs\" selectionMode=\"single\" (onNodeExpand)=\"ownNodeExpand($event)\"></p-tree>-->\n        <p-tree [value]=\"ownJurs\" (onNodeExpand)=\"ownNodeExpand($event)\" selectionMode=\"single\"></p-tree>\n      </div>\n    </div>\n    <div class=\"ui-grid-row margin-bottom-1vw\">\n      <div class=\"ui-grid-col-6\"></div>\n      <div class=\"ui-grid-col-3 padding-tblr\">\n        <button pButton type=\"submit\" label=\"确认\" (click)=\"formSubmit(false)\" *ngIf=\"false\"></button>\n      </div>\n      <div class=\"ui-grid-col-3 padding-tblr\">\n        <button pButton type=\"button\" label=\"取消\" (click)=\"closeJurLookMask(false)\"></button>\n      </div>\n    </div>\n    <!--</form>-->\n  </div>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/system/jur-look/jur-look.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JurLookComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var JurLookComponent = (function () {
    function JurLookComponent(systemService) {
        this.systemService = systemService;
        this.closeJurLook = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    JurLookComponent.prototype.ngOnInit = function () {
        this.display = true;
        this.title = '查看权限';
        //查询已拥有权限树的数据
        this.queryOwnJurTree('', '', this.currentOrg.rid);
    };
    // 已拥有权限树,懒加载
    JurLookComponent.prototype.ownNodeExpand = function (event) {
        if (event.node) {
            this.systemService.getOwnJursTreeData(event.node.mid, event.node.dep, this.currentOrg.rid).subscribe(function (data) {
                event.node.children = data;
            });
        }
    };
    JurLookComponent.prototype.closeJurLookMask = function (bool) {
        this.closeJurLook.emit(bool);
    };
    //查询已拥有权限树数据
    JurLookComponent.prototype.queryOwnJurTree = function (mid, dep, rid) {
        var _this = this;
        this.systemService.getOwnJursTreeData(mid, dep, rid).subscribe(function (datas) {
            _this.ownJurs = datas;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], JurLookComponent.prototype, "closeJurLook", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], JurLookComponent.prototype, "currentOrg", void 0);
    JurLookComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-jur-look',
            template: __webpack_require__("../../../../../src/app/system/jur-look/jur-look.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/jur-look/jur-look.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__system_service__["a" /* SystemService */]])
    ], JurLookComponent);
    return JurLookComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/jur-manage/jur-manage.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\r\n  margin-bottom: 1vw; }\r\n\r\n.padding-tblr {\r\n  padding: .25em .5em; }\r\n\r\n.start_red {\r\n  color: red; }\r\n\r\n.birthday {\r\n  display: inline-block; }\r\n\r\n@media screen and (max-width: 1366px) {\r\n  .ui-grid-col-1 {\r\n    width: 11.33333%; } }\r\n.btn_add {\r\n  margin-bottom: 10px; }\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/jur-manage/jur-manage.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \" >\n  <div>\n    <span class=\"feature-title\">系统管理 | <small>权限管理</small> </span>\n  </div>\n</div>\n<div class=\"content-section implementation GridDemo report\">\n  <div class=\"ui-g\">\n    <div class=\"ui-g-4\">\n      <h4 style=\"margin-bottom: 15px;\">组织</h4>\n      <p-tree [value]=\"orgs\" selectionMode=\"single\" [(selection)]=\"selected\" (onNodeExpand)=\"nodeExpand($event)\"(onNodeSelect) = \"NodeSelect($event)\"></p-tree>\n    </div>\n    <div class=\"ui-g-8\">\n      <h4>{{ titleName }}</h4>\n      <div class=\"ui-grid-row text_aligin_right\">\n        <button pButton type=\"button\" class=\"btn_add\" (click)=\"showRoleEdit()\" label=\"新增\" icon=\"fa-plus\" [disabled]=\"canAdd\"></button>\n        <button pButton type=\"button\" class=\"btn_add\" (click)=\"showRoleDelete()\" label=\"删除\" icon=\"fa-close\"></button>\n\n        <div class=\"ui-widget-header\" style=\"padding:4px 10px;border-bottom: 0 none\">\n          <i class=\"fa fa-search\" style=\"margin:4px 4px 0 0\"></i>\n          <input #gb type=\"text\" pInputText size=\"50\" placeholder=\"查询\">\n        </div>\n\n        <p-dataTable [value]=\"tableDatas\"\n                     [responsive]=\"true\"  id=\"manageTable\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[10,15,20]\" [(selection)]=\"selectedRoles\" [globalFilter]=\"gb\">\n          <!--<p-footer>-->\n          <!--<p-paginator rows=\"10\" totalRecords=\"120\" [rowsPerPageOptions]=\"[10,20,30]\" pageLinkSize=\"10\"></p-paginator>-->\n          <!--</p-footer>-->\n          <p-column selectionMode=\"multiple\" ></p-column>\n          <p-column field=\"rid\" header=\"编号\" [sortable]=\"true\"></p-column>\n          <p-column field=\"name\" header=\"角色\" [sortable]=\"true\"></p-column>\n          <p-column field=\"remark\" header=\"备注\" [sortable]=\"true\"></p-column>\n          <p-column field=\"color\" header=\"操作\" [style]=\"{'width':'12vw'}\">\n            <ng-template let-datas=\"rowData\" pTemplate=\"operator\">\n              <button pButton type=\"button\"  (click)=\"showRoleEdit(datas)\" label=\"编辑\"  class=\"ui-button-info\"></button>\n              <button pButton type=\"button\" *ngIf=\"datas.rid != '0001'\" (click)=\"showRoleDelete(datas)\" label=\"删除\"  class=\"ui-button-info\"></button>\n              <button pButton type=\"button\"  (click)=\"showJurEdit(datas,true)\" label=\"编辑权限\"  class=\"ui-button-info\"></button>\n              <button pButton type=\"button\"  (click)=\"showJurEdit(datas,false)\" label=\"查看权限\"   class=\"ui-button-info\"></button>\n            </ng-template>\n          </p-column>\n          <ng-template pTemplate=\"emptymessage\">\n            当前没有数据\n          </ng-template>\n        </p-dataTable>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<p-growl [(value)]=\"msgs\"></p-growl>\n\n<!-- 删除角色弹框 -->\n<p-growl [(value)]=\"deletemsgs\"></p-growl>\n<p-dialog header=\"删除确认框\" [(visible)]=\"showRoleDeleteMask\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n  <p>{{deleteRoleTip}}</p>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" (click)=\"deleteRole()\" label=\"确定\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"showRoleDeleteMask=false\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n\n<!-- 新增和编辑角色弹框 -->\n<app-role-edit [currentOrg]=\"selectedNode\" [state]=\"roleState\" (closeRoleEdit)=\"closeRoleEdit($event)\" (addRole)=\"addRole($event)\" (updateRole)=\"updateRole($event)\" *ngIf=\"showRoleEditMask\"></app-role-edit>\n\n<!-- 编辑权限弹框 -->\n<app-jur-edit [currentOrg]=\"selectedRid\" [state]=\"jurState\" (closeJurEdit)=\"closeJurEdit($event)\" (updateJur)=\"updateJur($event)\" *ngIf=\"showJurEditMask\"></app-jur-edit>\n\n<!-- 查看权限弹框 -->\n<app-jur-look [currentOrg]=\"lookRid\" (closeJurLook)=\"closeJurLook($event)\" *ngIf=\"showJurLookMask\"></app-jur-look>\n"

/***/ }),

/***/ "../../../../../src/app/system/jur-manage/jur-manage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JurManageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var JurManageComponent = (function () {
    function JurManageComponent(systemService, storageService, fb) {
        this.systemService = systemService;
        this.storageService = storageService;
        this.fb = fb;
        this.selectedNode = {}; // 组织树被选中的节点
        this.selectedCol = {}; // 人员表格中被选中的行
        this.selectedRoles = []; //选中的角色
        this.person = fb.group({
            perName: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            pid: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            idcard: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            organization: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            post: [''],
        });
    }
    JurManageComponent.prototype.ngOnInit = function () {
        //查询组织树
        this.queryOrgTree('');
        //查询人员表格数据
        this.queryJurList('');
        this.canAdd = true;
        this.showJurEditMask = false;
        this.showRoleEditMask = false;
        this.showCognateRoleMask = false;
    };
    //弹出新增或者修改角色弹框
    JurManageComponent.prototype.showRoleEdit = function (datas) {
        var _this = this;
        this.systemService.judgeRoleEditPower().subscribe(function (res) {
            if (res['errcode'] == '00000') {
                _this.showRoleEditMask = true;
                if (!datas) {
                    _this.selectedNode = _this.selected;
                    _this.roleState = 'add';
                }
                else {
                    // this.selectedCol = datas;
                    for (var key in datas) {
                        if (datas[key]) {
                            _this.selectedNode[key] = datas[key];
                        }
                        else {
                            _this.selectedNode[key] = '';
                        }
                    }
                    // console.log(this.selectedNode)
                    _this.roleState = 'update';
                }
            }
            else {
                _this.showError(res);
            }
        });
    };
    //显示删除角色弹框
    JurManageComponent.prototype.showRoleDelete = function (datas) {
        var _this = this;
        this.systemService.judgeRoleEditPower().subscribe(function (res) {
            // console.log(res['errcode']);
            if (res['errcode'] == '00000') {
                _this.deleteRoleTip = '确认删除该角色？';
                _this.deleteRid = [];
                if (datas) {
                    _this.showRoleDeleteMask = true;
                    _this.deleteRid.push(datas.rid);
                }
                else {
                    if (!_this.selectedRoles.length) {
                        _this.showWarn('请先选择删除项！');
                    }
                    else {
                        _this.showRoleDeleteMask = true;
                        for (var i = 0; i < _this.selectedRoles.length; i++) {
                            _this.deleteRid.push(_this.selectedRoles[i].rid);
                        }
                    }
                }
            }
            else {
                _this.showError(res);
            }
        });
    };
    //删除角色
    JurManageComponent.prototype.deleteRole = function () {
        var _this = this;
        this.systemService.deleteRole(this.deleteRid).subscribe(function (res) {
            if (res == '00000') {
                _this.selectedRoles = [];
                _this.showSuccess();
                _this.showRoleDeleteMask = false;
                _this.queryJurList(_this.selected['oid']);
            }
        });
    };
    //关闭新增或者修改角色弹框
    JurManageComponent.prototype.closeRoleEdit = function (bool) {
        this.showRoleEditMask = bool;
    };
    //新增角色成功
    JurManageComponent.prototype.addRole = function (bool) {
        this.showRoleEditMask = bool;
        this.queryJurList(this.selected['oid']);
    };
    //修改角色成功
    JurManageComponent.prototype.updateRole = function (bool) {
        this.showRoleEditMask = bool;
        // console.log(this.selected['oid']);
        this.queryJurList(this.selected['oid']);
    };
    //编辑权限或者查看权限弹框
    JurManageComponent.prototype.showJurEdit = function (datas, bool) {
        var _this = this;
        if (bool) {
            this.systemService.judgeRoleEditPower().subscribe(function (res) {
                if (res['errcode'] == '00000') {
                    _this.showJurEditMask = true;
                    _this.selectedRid = datas;
                    _this.jurState = 'update';
                }
                else {
                    _this.showError(res);
                }
            });
        }
        else {
            this.showJurLookMask = true;
            this.lookRid = datas;
        }
    };
    //关闭编辑权限弹框
    JurManageComponent.prototype.closeJurEdit = function (bool) {
        this.showJurEditMask = bool;
    };
    //关闭查看权限弹框
    JurManageComponent.prototype.closeJurLook = function (bool) {
        this.showJurLookMask = bool;
    };
    //编辑权限成功
    JurManageComponent.prototype.updateJur = function (bool) {
        this.showJurEditMask = bool;
        // this.queryOrgTree('');
    };
    // 组织树懒加载
    JurManageComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.systemService.getOrgTree(event.node.oid).subscribe(function (data) {
                // console.log(data);
                event.node.children = data;
            });
        }
    };
    // 组织树选中
    JurManageComponent.prototype.NodeSelect = function (event) {
        this.canAdd = false;
        // console.log(event.node.oid);
        if (parseInt(event.node.dep) >= 1) {
            this.queryJurList(event.node.oid);
            /*this.titleName = event.node.label;
            this.nodeFather = event.node.did;
            this.nodeDep = event.node.dep;
            this.did = event.node.did;
            this.dep = event.node.dep;
            this.canAdd = false;
            this.publicService.getMalfunctionBasalDatas(event.node.did, event.node.dep).subscribe(res => {
              this.tableDatas = res;
            });*/
        }
    };
    //查询组织树数据
    JurManageComponent.prototype.queryOrgTree = function (oid) {
        var _this = this;
        this.systemService.getOrgTree(oid).subscribe(function (data) {
            // console.log(data);
            _this.orgs = data;
        });
    };
    //查询人员表格数据
    JurManageComponent.prototype.queryJurList = function (oid) {
        var _this = this;
        this.systemService.getJurList(oid).subscribe(function (data) {
            // console.log(data);
            _this.tableDatas = data;
        });
    };
    JurManageComponent.prototype.showWarn = function (operator) {
        this.msgs = [];
        // this.msgs.push({severity:'warn', summary:'注意', detail:'您没有' + operator + '权限！'});
        this.msgs.push({ severity: 'warn', summary: '注意', detail: operator });
    };
    //成功提示
    JurManageComponent.prototype.showSuccess = function () {
        /*this.deletemsgs = [];
        this.deletemsgs.push({severity:'success', summary:'消息提示', detail:'删除成功'});*/
    };
    //错误提示
    JurManageComponent.prototype.showError = function (res) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '消息提示', detail: '删除失败' + res['errmsg'] });
    };
    JurManageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-jur-manage',
            template: __webpack_require__("../../../../../src/app/system/jur-manage/jur-manage.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/jur-manage/jur-manage.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__system_service__["a" /* SystemService */],
            __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]])
    ], JurManageComponent);
    return JurManageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/org-edit/org-edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\r\n  margin-bottom: 1vw; }\r\n\r\n.padding-tblr {\r\n  padding: .25em .5em; }\r\n\r\n.start_red {\r\n  color: red; }\r\n\r\n.birthday {\r\n  display: inline-block; }\r\n\r\n@media screen and (max-width: 1366px) {\r\n  .ui-grid-col-1 {\r\n    width: 11.33333%; } }\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/org-edit/org-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"（注：* 为必填项）\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"400\" [minWidth]=\"200\" (onHide)=\"closeOrgEditMask(false)\">\n  <p-panel header=\"{{title}}\">\n    <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n      <form [formGroup]=\"org\">\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-3\">\n            <label for=\"\" >组织编号</label>\n          </div>\n          <div class=\"ui-grid-col-9\" id=\"\">\n            <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"orgid\" pInputText [(ngModel)]=\"currentOrg.oid\" *ngIf=\"state ==='add'\" readonly/>\n            <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"orgid\" pInputText [(ngModel)]=\"currentOrg.oid\" *ngIf=\"state ==='update'\" readonly/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-3\">\n            <label for=\"\" >组织名称</label>\n            <label ngClass=\"start_red\">*</label>\n          </div>\n          <div class=\"ui-grid-col-9\" id=\"\">\n            <input  class=\"cursor_not_allowed\" formControlName=\"orgName\" type=\"text\" pInputText [(ngModel)]=\"submitAddOrg.name\" *ngIf=\"state ==='add'\"/>\n            <input  class=\"cursor_not_allowed\" formControlName=\"orgName\" type=\"text\" pInputText [(ngModel)]=\"currentOrg.name\" *ngIf=\"state ==='update'\"/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-3\">\n            <label for=\"\" >组织备注</label>\n          </div>\n          <div class=\"ui-grid-col-9\" id=\"\">\n            <input  class=\"cursor_not_allowed\" formControlName=\"orgRemark\" type=\"text\" pInputText [(ngModel)]=\"submitAddOrg.remark\" *ngIf=\"state ==='add'\"/>\n            <input  class=\"cursor_not_allowed\" formControlName=\"orgRemark\" type=\"text\" pInputText [(ngModel)]=\"currentOrg.remark\" *ngIf=\"state ==='update'\"/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\"></div>\n          <div class=\"ui-grid-col-3 padding-tblr\">\n            <button pButton type=\"submit\" label=\"确认\" (click)=\"formSubmit(false)\" [disabled]=\"!org.valid\"></button>\n          </div>\n          <div class=\"ui-grid-col-3 padding-tblr\">\n            <button pButton type=\"button\" label=\"取消\" (click)=\"closeOrgEditMask(false)\"></button>\n          </div>\n        </div>\n      </form>\n    </div>\n  </p-panel>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/system/org-edit/org-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrgEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OrgEditComponent = (function () {
    function OrgEditComponent(systemService, storageService, fb, confirmationService) {
        this.systemService = systemService;
        this.storageService = storageService;
        this.fb = fb;
        this.confirmationService = confirmationService;
        this.closeOrgEdit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.addOrg = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //新增组织节点
        this.updateOrg = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //修改组织节点
        this.submitAddOrg = {
            "oid": '',
            "name": '',
            "remark": '',
            "father": '',
            "dep": '',
            "del": ""
        };
        this.submitUpdateOrg = [
            {
                "oid": '',
                "name": '',
                "remark": '',
                "dep": '',
                "father": ''
            }
        ];
        this.org = fb.group({
            orgid: [''],
            orgName: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            orgRemark: [''],
        });
    }
    OrgEditComponent.prototype.ngOnInit = function () {
        this.display = true;
        if (this.state === 'add') {
            this.getCurrentNodeData();
            this.title = '新增组织';
        }
        else {
            this.title = '修改组织';
            this.submitUpdateOrg[0].oid = this.currentOrg.oid;
            this.submitUpdateOrg[0].father = this.currentOrg.father;
            this.submitUpdateOrg[0].dep = this.currentOrg.dep;
        }
    };
    OrgEditComponent.prototype.closeOrgEditMask = function (bool) {
        this.closeOrgEdit.emit(bool);
    };
    //确定
    OrgEditComponent.prototype.formSubmit = function (bool) {
        if (this.state === 'add') {
            this.addOrgNode(bool);
        }
        else {
            this.updateOrgNode(bool);
        }
    };
    //新增组织节点前，获取节点信息
    OrgEditComponent.prototype.getCurrentNodeData = function () {
        var _this = this;
        this.systemService.getCurrentNodeData(this.currentOrg.oid).subscribe(function (data) {
            _this.submitAddOrg.oid = data.oid;
            _this.submitAddOrg.father = data.father;
            _this.submitAddOrg.dep = data.dep;
            _this.currentOrg.oid = data.oid;
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            // this.confirmationService.confirm({
            //   message: message,
            //   rejectVisible:false,
            // })
        });
    };
    //新增组织节点
    OrgEditComponent.prototype.addOrgNode = function (bool) {
        var _this = this;
        this.systemService.addOrgNode(this.submitAddOrg).subscribe(function () {
            _this.addOrg.emit(bool);
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            // this.confirmationService.confirm({
            //   message: message,
            //   rejectVisible:false,
            // })
        });
    };
    //修改组织节点
    OrgEditComponent.prototype.updateOrgNode = function (bool) {
        var _this = this;
        this.submitUpdateOrg[0].name = this.currentOrg.name;
        this.submitUpdateOrg[0].remark = this.currentOrg.remark;
        this.systemService.updateOrgNode(this.submitUpdateOrg).subscribe(function () {
            _this.updateOrg.emit(_this.currentOrg);
            _this.closeOrgEdit.emit(bool);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], OrgEditComponent.prototype, "closeOrgEdit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], OrgEditComponent.prototype, "addOrg", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], OrgEditComponent.prototype, "updateOrg", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], OrgEditComponent.prototype, "currentOrg", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], OrgEditComponent.prototype, "state", void 0);
    OrgEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-org-edit',
            template: __webpack_require__("../../../../../src/app/system/org-edit/org-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/org-edit/org-edit.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__system_service__["a" /* SystemService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["ConfirmationService"]])
    ], OrgEditComponent);
    return OrgEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/password-modify/password-modify.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\r\n  margin-bottom: 1vw; }\r\n\r\n.padding-tblr {\r\n  padding: .25em .5em; }\r\n\r\n.start_red {\r\n  color: red; }\r\n\r\n.birthday {\r\n  display: inline-block; }\r\n\r\n@media screen and (max-width: 1366px) {\r\n  .ui-grid-col-1 {\r\n    width: 11.33333%; } }\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/password-modify/password-modify.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"修改密码\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"400\" [minWidth]=\"200\" (onHide)=\"closePassModMask(false)\">\n  <!--<p-panel header=\"{{title}}\">-->\n    <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n      <form [formGroup]=\"user\">\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-3\">\n            <label for=\"\" >人员编号</label>\n          </div>\n          <div class=\"ui-grid-col-9\" id=\"\">\n            <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"pid\" pInputText [(ngModel)]=\"currentNum\" readonly/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-3\">\n            <label for=\"\" >新密码</label>\n            <label ngClass=\"start_red\">*</label>\n          </div>\n          <div class=\"ui-grid-col-9\" id=\"\">\n            <input  class=\"cursor_not_allowed\" formControlName=\"pwd\" type=\"password\" pInputText/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\"></div>\n          <div class=\"ui-grid-col-3 padding-tblr\">\n            <button pButton type=\"submit\" label=\"确认\" (click)=\"formSubmit(false)\" [disabled]=\"!user.valid\"></button>\n          </div>\n          <div class=\"ui-grid-col-3 padding-tblr\">\n            <button pButton type=\"button\" label=\"取消\" (click)=\"closePassModMask(false)\"></button>\n          </div>\n        </div>\n      </form>\n    </div>\n  <!--</p-panel>-->\n</p-dialog>\n\n"

/***/ }),

/***/ "../../../../../src/app/system/password-modify/password-modify.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordModifyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PasswordModifyComponent = (function () {
    function PasswordModifyComponent(systemService, storageService, fb, confirmationService) {
        this.systemService = systemService;
        this.storageService = storageService;
        this.fb = fb;
        this.confirmationService = confirmationService;
        this.closePasswordMod = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.passwordModify = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.submitPwdMod = [{
                "pid": '',
                "pwd": '',
            }];
        this.user = fb.group({
            pid: [''],
            pwd: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
        });
    }
    PasswordModifyComponent.prototype.ngOnInit = function () {
        this.display = true;
        this.submitPwdMod[0].pid = this.currentNum;
    };
    //确定
    PasswordModifyComponent.prototype.formSubmit = function (bool) {
        var _this = this;
        this.submitPwdMod[0].pwd = this.user.value.pwd;
        this.systemService.passwordModify(this.submitPwdMod).subscribe(function () {
            _this.passwordModify.emit(bool);
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    PasswordModifyComponent.prototype.closePassModMask = function (bool) {
        this.closePasswordMod.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PasswordModifyComponent.prototype, "closePasswordMod", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PasswordModifyComponent.prototype, "passwordModify", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PasswordModifyComponent.prototype, "currentNum", void 0);
    PasswordModifyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-password-modify',
            template: __webpack_require__("../../../../../src/app/system/password-modify/password-modify.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/password-modify/password-modify.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__system_service__["a" /* SystemService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["ConfirmationService"]])
    ], PasswordModifyComponent);
    return PasswordModifyComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/person-edit/person-edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\r\n.margin-bottom-1vw {\r\n  margin-bottom: 1vw; }\r\n\r\n.padding-tblr {\r\n  padding: .25em .5em; }\r\n\r\n.start_red {\r\n  color: red; }\r\n\r\n.birthday {\r\n  display: inline-block; }\r\n\r\n@media screen and (max-width: 1366px) {\r\n  .ui-grid-col-1 {\r\n    width: 11.33333%; } }\r\n/*填写项对齐*/\r\n.ui-grid-col-6, .addr {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex; }\r\n\r\n.ui-grid-row label {\r\n  -webkit-box-flex: 1.7;\r\n      -ms-flex: 1.7;\r\n          flex: 1.7;\r\n  text-align: right;\r\n  padding-right: 10px; }\r\n\r\n#sex label {\r\n  -webkit-box-flex: 0.29;\r\n      -ms-flex: 0.29;\r\n          flex: 0.29; }\r\n\r\n/*上传图片*/\r\n.img_btn {\r\n  color: #555;\r\n  font-weight: 400;\r\n  /* position: absolute;\r\n   top: 8px;*/\r\n  float: left;\r\n  border: 1px solid #bbb;\r\n  padding: 2px 8px; }\r\n\r\n.file_upload {\r\n  opacity: 0;\r\n  display: inline-block; }\r\n\r\n.upload_item {\r\n  /*flex: 0 !important;\r\n  text-align: left !important;*/ }\r\n\r\n.imgage_preview {\r\n  max-width: 13vw;\r\n  max-height: 13vw;\r\n  margin-top: 1vw;\r\n  vertical-align: middle; }\r\n.control-error-message {\r\n    display: inline-block;\r\n    text-align: center;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/person-edit/person-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"（注：* 为必填项）\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"800\" [minWidth]=\"200\" (onHide)=\"closePersonEditMask(false)\">\n  <p-panel header=\"{{title}}\">\n    <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n      <form [formGroup]=\"person\">\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\" id=\"\">\n            <label for=\"\" >\n              姓名：\n              <span class=\"start_red\">*</span>\n            </label>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"perName\"\n                   pInputText *ngIf=\"state ==='add'\" id=\"\" [style.width.%]=\"75\"/>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"perName\"\n                   pInputText [(ngModel)]=\"currentPer.name\" *ngIf=\"state ==='update'\" id=\"\" [style.width.%]=\"75\"/>\n\n          </div>\n          <div class=\"ui-grid-col-6\" id=\"schduleCreater\">\n            <label for=\"\" >\n              帐号：\n              <span class=\"start_red\">*</span>\n            </label>\n            <input  class=\"cursor_not_allowed\"\n                    formControlName=\"pid\"\n                    type=\"text\" pInputText *ngIf=\"state ==='add'\" id=\"\"\n                    [style.width.%]=\"75\"/>\n            <input  class=\"cursor_not_allowed\"\n                    formControlName=\"pid\"\n                    type=\"text\" pInputText [(ngModel)]=\"currentPer.pid\" *ngIf=\"state ==='update'\" id=\"\"\n                    [style.width.%]=\"75\" readonly/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row\" *ngIf=\"!person.controls['perName'].untouched && (!person.controls['perName'].valid) || (!person.controls['pid'].valid)\">\n            <div class=\"ui-grid-col-6 \" *ngIf=\"!person.controls['perName'].untouched && (!person.controls['perName'].valid)\">\n                <div class=\"ui-message ui-messages-error ui-corner-all \"  *ngIf=\"person.hasError('required','perName')&&(!person.controls['perName'].untouched)\">\n                  <i class=\"fa fa-close\"></i>\n                  姓名必填\n                </div>\n                <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!person.hasError('required','perName')&&person.hasError('name','perName')&&(!person.controls['perName'].untouched)\" >\n                  <i class=\"fa fa-close\"></i>\n                  {{person.getError('name', 'perName')?.descxxx}}\n                </div>\n            </div>\n            <div class=\"ui-grid-col-6\"  *ngIf=\"!person.controls['pid'].valid\">\n                <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"person.hasError('required','pid')&&(!person.controls['pid'].untouched)\" >\n                    <i class=\"fa fa-close\"></i>\n                    帐号必填\n                </div>\n                <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!person.hasError('required','pid')&&person.hasError('code','pid')&&(!person.controls['pid'].untouched)\" >\n                    <i class=\"fa fa-close\"></i>\n                    {{person.getError('code', 'pid')?.descxxx}}\n                </div>\n            </div>\n        </div>\n\n\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\" >\n            <label for=\"\" >\n              密码：\n              <span class=\"start_red\">*</span>\n            </label>\n            <input class=\"cursor_not_allowed\"\n                   type=\"password\" formControlName=\"pwd\"\n                   pInputText *ngIf=\"state ==='add'\" [style.width.%]=\"75\"\n            />\n            <input class=\"cursor_not_allowed\"\n                   type=\"password\" formControlName=\"pwd\"\n                   pInputText [(ngModel)]=\"currentPer.pwd\" *ngIf=\"state ==='update'\"  [style.width.%]=\"75\" readonly\n            />\n          </div>\n          <div class=\"ui-grid-col-6\" id=\"sex\">\n            <label>性别：</label>\n            <p-radioButton name=\"group1\" value=\"男\" label=\"男\" formControlName=\"sex\" inputId=\"opt1\" *ngIf=\"state ==='add'\"></p-radioButton>\n            <p-radioButton name=\"group1\" value=\"男\" label=\"男\" formControlName=\"sex\" inputId=\"opt1\" [(ngModel)]=\"currentPer.sex\" *ngIf=\"state ==='update'\"></p-radioButton>\n            <p-radioButton name=\"group1\" value=\"女\" label=\"女\" formControlName=\"sex\"  inputId=\"opt1\" *ngIf=\"state ==='add'\"></p-radioButton>\n            <p-radioButton name=\"group1\" value=\"女\" label=\"女\" formControlName=\"sex\"  inputId=\"opt1\" [(ngModel)]=\"currentPer.sex\" *ngIf=\"state ==='update'\"></p-radioButton>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\"\n             *ngIf=\"!person.controls['pwd'].untouched && (!person.controls['pwd'].valid) && state ==='add'\">\n          <div class=\"ui-grid-col-6\">\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"person.hasError('required','pwd')&&(!person.controls['pwd'].untouched)\">\n              <i class=\"fa fa-close\"></i>\n              密码必填\n            </div>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!person.hasError('required','pwd')&&(!person.hasError('minLength','pwd')||!person.hasError('maxLength','pwd')||!person.hasError('password','pwd'))&&(!person.controls['pwd'].untouched)\" >\n              <i class=\"fa fa-close\"></i>\n              密码长度必须不少于6\n            </div>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!person.hasError('required','pwd')&&(!person.hasError('minLength','pwd')&&!person.hasError('maxLength','pwd')||!person.hasError('password','pwd'))&&(!person.controls['pwd'].untouched)\" >\n              <i class=\"fa fa-close\"></i>\n              密码长度必须不大于18\n            </div>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!person.hasError('required','pwd')&&(!person.hasError('minLength','pwd')&&!person.hasError('maxLength','pwd')||!person.hasError('password','pwd'))&&(!person.controls['pwd'].untouched)\" >\n              <i class=\"fa fa-close\"></i>\n              {{person.getError('password', 'pwd')?.descxxx}}\n            </div>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\" >\n            <label for=\"\" >\n              身份证：\n            </label>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"idcard\"\n                   pInputText *ngIf=\"state ==='add'\"  [style.width.%]=\"75\" (change)=\"idcardChange($event)\"/>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"idcard\"\n                   pInputText [(ngModel)]=\"currentPer.idcard\" *ngIf=\"state ==='update'\"  [style.width.%]=\"75\" (change)=\"idcardChange($event)\"/>\n\n          </div>\n          <div class=\"ui-grid-col-6\" >\n            <label for=\"\" >\n              生日：\n            </label>\n            <p-calendar [showIcon]=\"true\" dateFormat=\"yy-mm-dd\" [(ngModel)]=\"currentPer.birthday\"  *ngIf=\"state ==='add'\" [style.width.%]=\"75\" ngClass=\"birthday\" formControlName=\"birthday\"></p-calendar>\n            <p-calendar [showIcon]=\"true\" dateFormat=\"yy-mm-dd\" [(ngModel)]=\"currentPer.birthday\" *ngIf=\"state ==='update'\" [style.width.%]=\"75\" ngClass=\"birthday\" formControlName=\"birthday\"></p-calendar>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\"\n             *ngIf=\"!person.controls['idcard'].untouched && (!person.controls['idcard'].valid)\">\n          <div class=\"ui-grid-col-6\">\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"person.hasError('idcard','idcard')&&(!person.controls['idcard'].untouched)\">\n              <i class=\"fa fa-close\"></i>\n              非法的身份证号\n            </div>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-4\">\n            <label for=\"\" >\n            </label>\n            <label for=\"\" >\n              组织：\n              <span class=\"start_red\">*</span>\n            </label>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"organization\"\n                   pInputText [(ngModel)]=\"currentPer.label\"  [style.width.%]=\"65\" readonly/>\n          </div>\n          <div class=\"ui-grid-col-1\">\n            <button pButton  type=\"button\" (click)=\"showAddInspectionPlanMask()\" label=\"选择\"></button>\n          </div>\n          &nbsp;\n          <div class=\"ui-grid-col-1\">\n            <button pButton  type=\"button\" (click)=\"clearTreeDialog()\" label=\"清空\"></button>\n          </div>\n          <div class=\"ui-grid-col-6\">\n            <!--<label for=\"\">职位</label>-->\n            <label for=\"\" >\n              职位：\n            </label>\n            <input  class=\"cursor_not_allowed\"\n                    formControlName=\"post\"\n                    type=\"text\" pInputText *ngIf=\"state ==='add'\"\n                    [style.width.%]=\"75\"/>\n            <input  class=\"cursor_not_allowed\"\n                    formControlName=\"post\"\n                    type=\"text\" pInputText [(ngModel)]=\"currentPer.post\" *ngIf=\"state ==='update'\"\n                    [style.width.%]=\"75\"/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\">\n            <label for=\"\" >\n              手机：\n              <span class=\"start_red\">*</span>\n            </label>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"mobile\"\n                   pInputText *ngIf=\"state ==='add'\" [style.width.%]=\"75\"\n            />\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"mobile\"\n                   pInputText [(ngModel)]=\"currentPer.mobile\" *ngIf=\"state ==='update'\" [style.width.%]=\"75\"\n            />\n\n          </div>\n          <div class=\"ui-grid-col-6\">\n            <label for=\"\" >\n              电话：\n            </label>\n            <input  class=\"cursor_not_allowed\"\n                    formControlName=\"tel\"\n                    type=\"text\" pInputText *ngIf=\"state ==='add'\"\n                    [style.width.%]=\"75\"/>\n            <input  class=\"cursor_not_allowed\"\n                    formControlName=\"tel\"\n                    type=\"text\" pInputText [(ngModel)]=\"currentPer.tel\" *ngIf=\"state ==='update'\"\n                    [style.width.%]=\"75\"/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\"\n             *ngIf=\"!person.controls['mobile'].untouched && (!person.controls['mobile'].valid || !person.controls['tel'].valid)\">\n          <div class=\"ui-grid-col-6\">\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"person.hasError('required','mobile')\">\n              <i class=\"fa fa-close\"></i>\n              手机号必填\n            </div>\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!person.hasError('required','mobile')&&person.hasError('phone','mobile')\" >\n              <i class=\"fa fa-close\"></i>\n              {{person.getError('phone', 'mobile')?.descxxx}}\n            </div>\n          </div>\n          <div class=\"ui-grid-col-6\">\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"person.hasError('phone','tel')&&!person.controls['tel'].untouched\" >\n              <i class=\"fa fa-close\"></i>\n              非法的电话号码\n            </div>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\">\n            <label for=\"\" >\n              邮箱：\n            </label>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"email\"\n                   pInputText *ngIf=\"state ==='add'\" [style.width.%]=\"75\"/>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"email\"\n                   pInputText [(ngModel)]=\"currentPer.email\" *ngIf=\"state ==='update'\"  [style.width.%]=\"75\"\n            />\n\n          </div>\n          <div class=\"ui-grid-col-6\">\n            <label for=\"\" >\n              微信号：\n            </label>\n            <input  class=\"cursor_not_allowed\"\n                    formControlName=\"wechat\"\n                    type=\"text\" pInputText *ngIf=\"state ==='add'\"\n                    [style.width.%]=\"75\"/>\n            <input  class=\"cursor_not_allowed\"\n                    formControlName=\"wechat\"\n                    type=\"text\" pInputText [(ngModel)]=\"currentPer.wechat\" *ngIf=\"state ==='update'\"\n                    [style.width.%]=\"75\"/>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\"\n             *ngIf=\"!person.controls['email'].untouched && !person.controls['email'].valid\">\n          <div class=\"ui-grid-col-6\">\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"person.hasError('email','email')\">\n              <i class=\"fa fa-close\"></i>\n              非法的邮箱\n            </div>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\" formGroupName=\"dateGroup\">\n          <div class=\"ui-grid-col-6\" >\n            <label for=\"\" >\n              注册日期：\n            </label>\n            <p-calendar [showIcon]=\"true\" [maxDate]=\"maxDate\" dateFormat=\"yy-mm-dd\" *ngIf=\"state ==='add'\" [style.width.%]=\"75\" ngClass=\"birthday\" formControlName=\"registedate\" [(ngModel)]=\"registedate\" dataType=\"string\"></p-calendar>\n            <p-calendar [showIcon]=\"true\" dateFormat=\"yy-mm-dd\" [(ngModel)]=\"currentPer.registedate\" *ngIf=\"state ==='update'\" [style.width.%]=\"75\" ngClass=\"birthday\" formControlName=\"registedate\" dataType=\"string\" disabled=\"true\"></p-calendar>\n          </div>\n          <div class=\"ui-grid-col-6\" >\n            <label for=\"\" >\n              失效日期：\n            </label>\n            <p-calendar [showIcon]=\"true\" [minDate]=\"minDate\" dateFormat=\"yy-mm-dd\" *ngIf=\"state ==='add'\" [style.width.%]=\"75\" ngClass=\"birthday\" formControlName=\"validdate\" dataType=\"string\"></p-calendar>\n            <p-calendar [showIcon]=\"true\" [minDate]=\"minDate\" dateFormat=\"yy-mm-dd\" [(ngModel)]=\"currentPer.validdate\" *ngIf=\"state ==='update'\" [style.width.%]=\"75\" ngClass=\"birthday\" formControlName=\"validdate\" dataType=\"string\"></p-calendar>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\">\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"person.hasError('date','dateGroup')\">\n              <i class=\"fa fa-close\"></i>\n              {{person.getError('date','dateGroup')?.descxxx}}\n            </div>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-12 addr\">\n            <label for=\"\" >\n              地址：\n            </label>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"addr\"\n                   pInputText *ngIf=\"state ==='add'\"  [style.width.%]=\"88\"\n            />\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"addr\"\n                   pInputText [(ngModel)]=\"currentPer.addr\" *ngIf=\"state ==='update'\"  [style.width.%]=\"88\"\n            />\n\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-6\">\n            <label for=\"\" >图片一：</label>\n            <label class=\"upload_item\">\n              <span class=\"img_btn\">上传图片</span>\n              <input type=\"file\"  value=\"选择文件\" class=\"file_upload\" id=\"upimg_1\" (change)=\"fileChange($event,1)\"/>\n              <img [src]=\"imgUrl_1\" alt=\"\" class=\"imgage_preview\">\n            </label>\n          </div>\n          <div class=\"ui-grid-col-6\">\n            <label for=\"\">图片二：</label>\n            <label class=\"upload_item\">\n              <span class=\"img_btn\">上传图片</span>\n              <input type=\"file\"  value=\"选择文件\" class=\"file_upload\" id=\"upimg_1\" (change)=\"fileChange($event,2)\"/>\n              <img [src]=\"imgUrl_2\" alt=\"\" class=\"imgage_preview\">\n            </label>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-12 addr\">\n            <label for=\"\" >备注：</label>\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"remark1\"\n                   pInputText *ngIf=\"state ==='add'\" id=\"\" [style.width.%]=\"88\"\n            />\n            <input class=\"cursor_not_allowed\"\n                   type=\"text\" formControlName=\"remark1\"\n                   pInputText [(ngModel)]=\"currentPer.remark1\" *ngIf=\"state ==='update'\" id=\"\" [style.width.%]=\"88\"\n            />\n\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\"\n             *ngIf=\"!person.controls['remark1'].untouched && !person.controls['remark1'].valid\">\n          <div class=\"ui-grid-col-6\">\n            <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!person.hasError('maxLength','remark1')\">\n              <i class=\"fa fa-close\"></i>\n              不超过100字符\n            </div>\n          </div>\n        </div>\n        <div class=\"ui-grid-row margin-bottom-1vw\">\n          <div class=\"ui-grid-col-8\"></div>\n          <div class=\"ui-grid-col-2 padding-tblr\">\n            <button pButton type=\"submit\" label=\"确认\" (click)=\"formSubmit(false)\" [disabled]=\"!person.controls['perName'].valid || !person.controls['pid'].valid || !person.controls['pwd'].valid || !person.controls['mobile'].valid\" *ngIf=\"state ==='add'\"></button>\n            <button pButton type=\"submit\" label=\"确认\" (click)=\"formSubmit(false)\" [disabled]=\"!person.controls['perName'].valid || !person.controls['pid'].valid || !person.controls['mobile'].valid\" *ngIf=\"state ==='update'\"></button>\n          </div>\n          <div class=\"ui-grid-col-2 padding-tblr\">\n            <button pButton type=\"button\" label=\"取消\" (click)=\"closePersonEditMask(false)\"></button>\n          </div>\n\n        </div>\n      </form>\n\n    </div>\n  </p-panel>\n</p-dialog>\n<app-add-inspection-plan-tree\n  *ngIf=\"showAddPlanMask\"\n  (closeAddMask)=\"closeAddPlanTreeMask($event)\"\n  (addTree)=\"addPlanOrg($event)\">\n</app-add-inspection-plan-tree>\n"

/***/ }),

/***/ "../../../../../src/app/system/person-edit/person-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__validator_validators__ = __webpack_require__("../../../../../src/app/validator/validators.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PersonEditComponent = (function () {
    function PersonEditComponent(systemService, storageService, fb, confirmationService, sanitizer) {
        this.systemService = systemService;
        this.storageService = storageService;
        this.fb = fb;
        this.confirmationService = confirmationService;
        this.sanitizer = sanitizer;
        this.showAddPlanMask = false; //添加弹出框
        this.closePersonEdit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.addPer = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //新增人员
        this.updatePer = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //修改人员
        this.uploadedFiles = [];
        this.submitAddPerson = [
            {
                "pid": '',
                "oid": '',
                "name": '',
                "pwd": '',
                "sex": '',
                "birthday": '',
                "registedate": '',
                "validdate": '',
                "organization": '',
                "post": '',
                "tel": '',
                "mobile": '',
                "addr": '',
                "email": '',
                "idcard": '',
                "license": '',
                "wechat": '',
                "photo1": '',
                "photo2": '',
                "remark1": '',
                "remark2": '',
                "remark3": '',
                "remark4": ''
            }
        ];
        this.submitUpdatePerson = [
            {
                "pid": '',
                "oid": '',
                "name": '',
                "sex": '',
                "birthday": '',
                "registedate": '',
                "validdate": '',
                "organization": '',
                "post": '',
                "tel": '',
                "mobile": '',
                "addr": '',
                "email": '',
                "idcard": '',
                "license": '',
                "wechat": '',
                "photo1": '',
                "photo2": '',
                "remark1": '',
                "remark2": '',
                "remark3": '',
                "remark4": ''
            }
        ];
        this.person = fb.group({
            perName: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_6__validator_validators__["d" /* nameValidator */]]],
            pid: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_6__validator_validators__["h" /* pidValidator */]]],
            oid: [''],
            pwd: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].minLength(6), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].maxLength(18), __WEBPACK_IMPORTED_MODULE_6__validator_validators__["e" /* nullValidator */]]],
            sex: [''],
            idcard: ['', __WEBPACK_IMPORTED_MODULE_6__validator_validators__["c" /* idcardValidator */]],
            license: [''],
            birthday: [''],
            dateGroup: fb.group({
                registedate: [''],
                validdate: [''],
            }, { validator: __WEBPACK_IMPORTED_MODULE_6__validator_validators__["a" /* dateValidator */] }),
            /*registedate: [''],
            validdate: [''],*/
            addr: [''],
            organization: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            post: [''],
            mobile: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_6__validator_validators__["g" /* phoneValidator */]]],
            tel: ['', __WEBPACK_IMPORTED_MODULE_6__validator_validators__["g" /* phoneValidator */]],
            email: ['', __WEBPACK_IMPORTED_MODULE_6__validator_validators__["b" /* emailValidator */]],
            wechat: [''],
            photo1: [''],
            photo2: [''],
            remark1: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].maxLength(100)],
        });
    }
    PersonEditComponent.prototype.ngOnInit = function () {
        this.registedate = new Date();
        this.display = true;
        //日期格式转换为yyyy-mm-dd
        if (this.currentPer.birthday) {
            this.currentPer.birthday = __WEBPACK_IMPORTED_MODULE_7__services_PUblicMethod__["a" /* PUblicMethod */].formateDateTime(this.currentPer.birthday);
        }
        if (this.currentPer.registedate) {
            this.currentPer.registedate = __WEBPACK_IMPORTED_MODULE_7__services_PUblicMethod__["a" /* PUblicMethod */].formateDateTime(this.currentPer.registedate);
        }
        if (this.currentPer.validdate) {
            this.currentPer.validdate = __WEBPACK_IMPORTED_MODULE_7__services_PUblicMethod__["a" /* PUblicMethod */].formateDateTime(this.currentPer.validdate);
        }
        this.minDate = new Date();
        this.maxDate = new Date();
        // console.log(this.currentPer);
        this.submitAddPerson[0].oid = this.currentPer.oid;
        this.submitUpdatePerson[0].oid = this.currentPer.oid;
        this.submitAddPerson[0].organization = this.currentPer.label;
        if (this.state === 'add') {
            this.title = '新增人员信息';
        }
        else {
            //图片赋初始值
            this.imgUrl_1 = this.currentPer.photo1;
            this.imgUrl_2 = this.currentPer.photo2;
            this.submitUpdatePerson[0].photo1 = this.currentPer.photo1;
            this.submitUpdatePerson[0].photo2 = this.currentPer.photo2;
            this.title = '修改人员信息';
        }
    };
    PersonEditComponent.prototype.showAddInspectionPlanMask = function () {
        this.showAddPlanMask = !this.showAddPlanMask;
    };
    //取消遮罩层
    PersonEditComponent.prototype.closeAddPlanTreeMask = function (bool) {
        this.showAddPlanMask = bool;
    };
    PersonEditComponent.prototype.clearTreeDialog = function () {
        this.currentPer.label = '';
    };
    PersonEditComponent.prototype.addPlanOrg = function (org) {
        this.currentPer.oid = org[0].oid;
        this.currentPer.label = org[0].label;
        // this.submitData.organs_names = [];
        // for(let i = 0;i<org.length;i++){
        //   this.submitData.organs_ids.push(org[i]['oid'])
        //   this.submitData.organs_names.push(org[i]['label'])
        // }
        // this.submitData.orggans_name =  this.submitData.organs_names.join(',');
        this.showAddPlanMask = false;
    };
    PersonEditComponent.prototype.closePersonEditMask = function (bool) {
        this.closePersonEdit.emit(bool);
    };
    //确定
    PersonEditComponent.prototype.formSubmit = function (bool) {
        if (this.state === 'add') {
            this.submitAddPerson[0].name = this.person.value.perName;
            this.submitAddPerson[0].pid = this.person.value.pid;
            this.submitAddPerson[0].pwd = this.person.value.pwd;
            this.submitAddPerson[0].mobile = this.person.value.mobile;
            this.submitAddPerson[0].addr = this.person.value.addr;
            this.submitAddPerson[0].sex = this.person.value.sex;
            this.submitAddPerson[0].birthday = this.person.value.birthday;
            this.submitAddPerson[0].registedate = this.person.value.dateGroup.registedate;
            this.submitAddPerson[0].validdate = this.person.value.dateGroup.validdate;
            this.submitAddPerson[0].organization = this.person.value.organization;
            this.submitAddPerson[0].post = this.person.value.post;
            this.submitAddPerson[0].tel = this.person.value.tel;
            this.submitAddPerson[0].email = this.person.value.email;
            this.submitAddPerson[0].idcard = this.person.value.idcard;
            this.submitAddPerson[0].wechat = this.person.value.wechat;
            this.submitAddPerson[0].remark1 = this.person.value.remark1;
            this.addPerson(bool);
        }
        else {
            this.submitUpdatePerson[0].name = this.person.value.perName;
            this.submitUpdatePerson[0].pid = this.person.value.pid;
            this.submitUpdatePerson[0].mobile = this.person.value.mobile;
            this.submitUpdatePerson[0].addr = this.person.value.addr;
            this.submitUpdatePerson[0].sex = this.person.value.sex;
            this.submitUpdatePerson[0].birthday = this.person.value.birthday;
            this.submitUpdatePerson[0].registedate = this.person.value.dateGroup.registedate;
            this.submitUpdatePerson[0].validdate = this.person.value.dateGroup.validdate;
            this.submitUpdatePerson[0].organization = this.person.value.organization;
            this.submitUpdatePerson[0].post = this.person.value.post;
            this.submitUpdatePerson[0].tel = this.person.value.tel;
            this.submitUpdatePerson[0].email = this.person.value.email;
            this.submitUpdatePerson[0].idcard = this.person.value.idcard;
            this.submitUpdatePerson[0].wechat = this.person.value.wechat;
            this.submitUpdatePerson[0].remark1 = this.person.value.remark1;
            this.updatePerson(bool);
        }
    };
    //新增人员
    PersonEditComponent.prototype.addPerson = function (bool) {
        var _this = this;
        this.systemService.addPerson(this.submitAddPerson).subscribe(function () {
            _this.addPer.emit(bool);
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    //修改人员
    PersonEditComponent.prototype.updatePerson = function (bool) {
        var _this = this;
        console.log(this.submitUpdatePerson[0]);
        this.systemService.updatePerson(this.submitUpdatePerson).subscribe(function () {
            _this.updatePer.emit(_this.currentPer);
            _this.closePersonEdit.emit(bool);
        });
    };
    //图片上传
    PersonEditComponent.prototype.fileChange = function (event, num) {
        var that = this;
        var file = event.target.files[0];
        var imgUrl = window.URL.createObjectURL(file);
        var sanitizerUrl = this.sanitizer.bypassSecurityTrustUrl(imgUrl);
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (theFile) {
            if (num == 1) {
                that.submitAddPerson[0].photo1 = theFile.target['result']; //base64编码,用一个变量存储
                that.submitUpdatePerson[0].photo1 = theFile.target['result']; //base64编码,用一个变量存储
            }
            else {
                that.submitAddPerson[0].photo2 = theFile.target['result']; //base64编码,用一个变量存储
                that.submitUpdatePerson[0].photo2 = theFile.target['result']; //base64编码,用一个变量存储
            }
        };
        if (num == 1) {
            this.imgUrl_1 = sanitizerUrl;
        }
        else {
            this.imgUrl_2 = sanitizerUrl;
        }
    };
    //输入身份证，获取生日日期
    PersonEditComponent.prototype.idcardChange = function (e) {
        if (this.person.get('idcard').valid) {
            var birthday = this.getBirthday(e.target.value);
            this.currentPer.birthday = birthday;
        }
    };
    //根据身份证获取生日日期
    PersonEditComponent.prototype.getBirthday = function (psidno) {
        var birthdayno, birthdaytemp;
        if (psidno.length == 18) {
            birthdayno = psidno.substring(6, 14);
        }
        else if (psidno.length == 15) {
            birthdaytemp = psidno.substring(6, 12);
            birthdayno = "19" + birthdaytemp;
        }
        else {
            alert("错误的身份证号码，请核对！");
            return false;
        }
        var birthday = birthdayno.substring(0, 4) + "-" + birthdayno.substring(4, 6) + "-" + birthdayno.substring(6, 8);
        return birthday;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PersonEditComponent.prototype, "closePersonEdit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PersonEditComponent.prototype, "addPer", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PersonEditComponent.prototype, "updatePer", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PersonEditComponent.prototype, "currentPer", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PersonEditComponent.prototype, "state", void 0);
    PersonEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-person-edit',
            template: __webpack_require__("../../../../../src/app/system/person-edit/person-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/person-edit/person-edit.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__system_service__["a" /* SystemService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["DomSanitizer"]])
    ], PersonEditComponent);
    return PersonEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/person-manage/person-manage.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \" >\n  <div>\n    <span class=\"feature-title\">系统管理 | <small>人员管理</small> </span>\n  </div>\n</div>\n<div class=\"content-section myimplementation GridDemo report\">\n  <div class=\"ui-g\">\n    <div class=\"ui-g-3\">\n      <h4 style=\"margin-bottom: 15px;\">组织</h4>\n      <p-tree [value]=\"orgs\" selectionMode=\"single\" [(selection)]=\"selected\" (onNodeExpand)=\"nodeExpand($event)\"(onNodeSelect) = \"NodeSelect($event)\" [contextMenu]=\"cm\"></p-tree>\n      <p-contextMenu #cm [model]=\"items\"></p-contextMenu>\n    </div>\n    <div class=\"ui-g-9\">\n      <h4>{{ titleName }}</h4>\n      <div class=\"ui-grid-row text_aligin_right\">\n        <button pButton type=\"button\" class=\"btn_add\" (click)=\"showPerEdit()\" label=\"新增\" icon=\"fa-plus\" [disabled]=\"canAdd\"></button>\n        <button pButton type=\"button\" class=\"btn_add\" (click)=\"showPerDelete()\" label=\"删除\" icon=\"fa-close\"></button>\n\n        <div class=\"ui-widget-header\" style=\"padding:4px 10px;border-bottom: 0 none\">\n          <i class=\"fa fa-search\" style=\"margin:4px 4px 0 0\"></i>\n          <input #gb type=\"text\" pInputText size=\"50\" placeholder=\"查询\">\n        </div>\n\n        <p-dataTable [value]=\"tableDatas\"\n                     [responsive]=\"true\"  id=\"manageTable\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[10,15,20]\" totalRecords=\"{{totalRecords}}\" (onLazyLoad)=\"loadTableData($event)\" [(selection)]=\"selectedPers\" [globalFilter]=\"gb\">\n          <!--<p-footer>-->\n          <!--<p-paginator rows=\"10\" totalRecords=\"120\" [rowsPerPageOptions]=\"[10,20,30]\" pageLinkSize=\"10\"></p-paginator>-->\n          <!--</p-footer>-->\n          <p-column selectionMode=\"multiple\" ></p-column>\n          <p-column field=\"pid\" header=\"帐号\" [sortable]=\"true\"></p-column>\n          <p-column field=\"name\" header=\"姓名\" [sortable]=\"true\"></p-column>\n          <p-column field=\"mobile\" header=\"手机\" [sortable]=\"true\"></p-column>\n          <p-column field=\"organization\" header=\"组织\" [sortable]=\"true\"></p-column>\n          <p-column field=\"post\" header=\"职位\" [sortable]=\"true\"></p-column>\n          <p-column field=\"color\" header=\"操作\" [style]=\"{'width':'12vw'}\">\n            <ng-template let-datas=\"rowData\" pTemplate=\"operator\">\n              <button pButton type=\"button\"  (click)=\"showPerEdit(datas)\" label=\"编辑\"  class=\"ui-button-info\"></button>\n              <button pButton type=\"button\"  (click)=\"showPerDelete(datas)\" label=\"删除\"  class=\"ui-button-info\"></button>\n              <button pButton type=\"button\" *ngIf=\"datas.pid != 'admin'\" (click)=\"showCogRoleMask(datas,true)\" label=\"关联角色\"  class=\"ui-button-info\"></button>\n              <button pButton type=\"button\"  (click)=\"showCogRoleMask(datas,false)\" label=\"查看角色\"   class=\"ui-button-info\"></button>\n              <button pButton type=\"button\"  (click)=\"modifyPassword(datas)\" label=\"修改密码\"   class=\"ui-button-info\"></button>\n            </ng-template>\n          </p-column>\n          <ng-template pTemplate=\"emptymessage\">\n            当前没有数据\n          </ng-template>\n        </p-dataTable>\n      </div>\n    </div>\n  </div>\n  <!--<div class=\"ui-grid-row\">\n    <div class=\"ui-grid-col-3\">\n      <h4>组织</h4>\n      <p-tree [value]=\"orgs\" selectionMode=\"single\" [(selection)]=\"selected\" (onNodeExpand)=\"nodeExpand($event)\"(onNodeSelect) = \"NodeSelect($event)\"\n      ></p-tree>\n    </div>\n    <div class=\"ui-grid-col-1\" style=\"height: 1px;\"></div>\n    <div class=\"ui-grid-col-8\">\n      <h4>{{ titleName }}</h4>\n      <div class=\"ui-grid-row text_aligin_right\">\n        <button pButton type=\"button\" class=\"btn_add\" (click)=\"add()\" label=\"新增\" icon=\"fa-plus\" [disabled]=\"canAdd\"></button>\n        <p-dataTable [value]=\"tableDatas\"\n                     [responsive]=\"true\"  id=\"manageTable\">\n          &lt;!&ndash;<p-footer>&ndash;&gt;\n          &lt;!&ndash;<p-paginator rows=\"10\" totalRecords=\"120\" [rowsPerPageOptions]=\"[10,20,30]\" pageLinkSize=\"10\"></p-paginator>&ndash;&gt;\n          &lt;!&ndash;</p-footer>&ndash;&gt;\n          <p-column selectionMode=\"multiple\" ></p-column>\n          <p-column field=\"pid\" header=\"帐号\" [sortable]=\"true\"></p-column>\n          <p-column field=\"name\" header=\"姓名\" [sortable]=\"true\"></p-column>\n          <p-column field=\"mobile\" header=\"手机\" [sortable]=\"true\"></p-column>\n          <p-column field=\"organization\" header=\"组织\" [sortable]=\"true\"></p-column>\n          <p-column field=\"pid\" header=\"职位\" [sortable]=\"true\"></p-column>\n          <p-column field=\"color\" header=\"操作项\" [style]=\"{'width':'12vw'}\">\n            <ng-template let-datas=\"rowData\" pTemplate=\"operator\">\n              <button pButton type=\"button\"  (click)=\"edit(datas)\" label=\"编辑\"  class=\"ui-button-info\"></button>\n              <button pButton type=\"button\"  (click)=\"frezzeOrActive(datas)\" label=\"启用\"  class=\"ui-button-info\" *ngIf=\"datas.status == '冻结'\"></button>\n              <button pButton type=\"button\"  (click)=\"frezzeOrActive(datas)\" label=\"冻结\"  class=\"ui-button-info\" *ngIf=\"datas.status == '启用'\"></button>\n              <button pButton type=\"button\"  (click)=\"delete(datas)\" label=\"删除\"   class=\"ui-button-info\"></button>\n              &lt;!&ndash;<button pButton type=\"button\"  (click)=\"view(datas)\" label=\"查看\"   class=\"ui-button-info\"></button>&ndash;&gt;\n            </ng-template>\n          </p-column>\n          <ng-template pTemplate=\"emptymessage\">\n            当前没有数据\n          </ng-template>\n        </p-dataTable>\n      </div>\n    </div>\n  </div>-->\n\n  <!--<p-dialog header=\"新增\" [(visible)]=\"addDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n    <div class=\"ui-grid-row\">\n      <p-messages [(value)]=\"msgs\"></p-messages>\n      <div class=\"ui-grid-col-12\">\n        <div class=\"ui-grid-col-2\">\n          <label for=\"\">名称</label>\n        </div>\n        <div class=\"ui-grid-col-10\">\n          <input  type=\"text\" pInputText  [style.width.%]=\"90\" placeholder=\"请输入名称\" [(ngModel)]=\"dataName\" required=\"required\"/>\n        </div>\n      </div>\n      <div class=\"ui-grid-col-12\">\n        <div class=\"ui-grid-col-2\">\n          <label for=\"\">状态</label>\n        </div>\n        <div class=\"ui-grid-col-10\">\n\n          <p-radioButton name=\"group\" value=\"启用\" label=\"启用\" [(ngModel)]=\"val1\" inputId=\"preopt3\"></p-radioButton>\n          <p-radioButton name=\"group\" value=\"冻结\" label=\"冻结\" [(ngModel)]=\"val1\" inputId=\"preopt4\"></p-radioButton>\n\n        </div>\n      </div>\n    </div>\n    <p-footer>\n      <button type=\"button\" pButton icon=\"fa-check\" (click)=\"ansureAddDialog()\" label=\"新增\" *ngIf=\"addOrEdit == 'add'\"></button>\n      <button type=\"button\" pButton icon=\"fa-check\" (click)=\"ansureEditDialog()\" label=\"编辑\" *ngIf=\"addOrEdit == 'edit'\"></button>\n      <button type=\"button\" pButton icon=\"fa-close\" (click)=\"addDisplay=false\" label=\"取消\"></button>\n    </p-footer>\n  </p-dialog>\n  <p-growl [(value)]=\"msgPop\"></p-growl>-->\n</div>\n\n\n<p-growl [(value)]=\"msgs\"></p-growl>\n\n<!-- 删除组织弹框 -->\n<p-growl [(value)]=\"deletemsgs\"></p-growl>\n<p-dialog header=\"删除确认框\" [(visible)]=\"showOrgDeleteMask\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n  <p>{{deleteOrgTip}}</p>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" (click)=\"deleteOrg()\" label=\"确定\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"showOrgDeleteMask=false\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n\n<!-- 删除人员弹框 -->\n<p-growl [(value)]=\"deletemsgs\"></p-growl>\n<p-dialog header=\"删除确认框\" [(visible)]=\"showPerDeleteMask\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n  <p>{{deletePerTip}}</p>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" (click)=\"deletePer()\" label=\"确定\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"showPerDeleteMask=false\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n\n<!-- 新增和修改组织弹框 -->\n<app-org-edit [currentOrg]=\"selectedNode\" [state]=\"state\" (closeOrgEdit)=\"closeOrgEdit($event)\" (addOrg)=\"addOrgNode($event)\" (updateOrg)=\"updateOrgNode($event)\" *ngIf=\"showOrgEditMask\"></app-org-edit>\n\n<!-- 新增和编辑人员弹框 -->\n<app-person-edit [currentPer]=\"selectedCol\" [state]=\"personState\" (closePersonEdit)=\"closePersonEdit($event)\" (addPer)=\"addPersonNode($event)\" (updatePer)=\"updatePersonNode($event)\" *ngIf=\"showPersonEditMask\"></app-person-edit>\n\n<!-- 关联角色和查看角色弹框 -->\n<app-cognate-role [currentPid]=\"selectedPid\" [state]=\"roleState\" (closeCognateRole)=\"closeCognateRole($event)\"  (cognateRole)=\"cognateRoleBack($event)\"*ngIf=\"showCognateRoleMask\"></app-cognate-role>\n\n<!-- 修改密码弹框 -->\n<app-password-modify [currentNum]=\"selectedNum\" (closePasswordMod)=\"closePasswordMod($event)\"  (passwordModify)=\"passwordModifyBack($event)\"*ngIf=\"showPasswordModMask\"></app-password-modify>\n"

/***/ }),

/***/ "../../../../../src/app/system/person-manage/person-manage.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\n  margin-bottom: 1vw; }\n\n.padding-tblr {\n  padding: .25em .5em; }\n\n.start_red {\n  color: red; }\n\n.birthday {\n  display: inline-block; }\n\n@media screen and (max-width: 1366px) {\n  .ui-grid-col-1 {\n    width: 11.33333%; } }\n\n.btn_add {\n  margin-bottom: 10px; }\n\n.myimplementation {\n  padding: 20px; }\n\n#manageTable /deep/ table {\n  border: red 1px solid; }\n  #manageTable /deep/ table thead tr th:nth-child(1) {\n    width: 2.6%; }\n  #manageTable /deep/ table thead tr th:nth-child(7) {\n    width: 34% !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/person-manage/person-manage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonManageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PersonManageComponent = (function () {
    function PersonManageComponent(systemService, storageService, fb) {
        this.systemService = systemService;
        this.storageService = storageService;
        this.fb = fb;
        this.selectedCol = {}; // 人员表格中被选中的行
        this.selectedPers = []; //选中的人员
        this.person = fb.group({
            perName: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            pid: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            idcard: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            organization: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            post: [''],
        });
    }
    PersonManageComponent.prototype.ngOnInit = function () {
        var _this = this;
        //查询组织树
        this.queryOrgTree('');
        //查询人员表格数据
        this.queryPersonList('');
        this.canAdd = true;
        this.showOrgEditMask = false;
        this.showOrgDeleteMask = false;
        this.showPersonEditMask = false;
        this.showCognateRoleMask = false;
        //右击组织节点的弹框列表
        this.items = [
            { label: '新增', icon: 'fa-plus', command: function (event) { return _this.showOrgEdit(_this.selected, true); } },
            { label: '修改', icon: 'fa-edit', command: function (event) { return _this.showOrgEdit(_this.selected, false); } },
            { label: '删除', icon: 'fa-close', command: function (event) { return _this.showOrgDelete(_this.selected); } },
        ];
    };
    //表格数据懒加载
    PersonManageComponent.prototype.loadTableData = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.dataSource) {
                _this.tableDatas = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 250);
    };
    // 弹出新增或者修改节点弹框
    PersonManageComponent.prototype.showOrgEdit = function (node, flag) {
        var _this = this;
        this.systemService.judgeOrgEditPower().subscribe(function (res) {
            // console.log(res['errcode']);
            if (res['errcode'] == '00000') {
                _this.showOrgEditMask = true;
                _this.selectedNode = node;
                if (flag) {
                    _this.state = 'add';
                }
                else {
                    _this.state = 'update';
                }
            }
            else {
                _this.showError(res);
            }
        });
    };
    //关闭新增或者修改节点弹框
    PersonManageComponent.prototype.closeOrgEdit = function (bool) {
        this.showOrgEditMask = bool;
    };
    //新增组织节点成功
    PersonManageComponent.prototype.addOrgNode = function (bool) {
        this.showOrgEditMask = bool;
        this.queryOrgTree('');
    };
    //修改组织节点成功
    PersonManageComponent.prototype.updateOrgNode = function (bool) {
        this.showOrgEditMask = bool;
        this.queryOrgTree('');
    };
    //显示删除组织节点弹框
    PersonManageComponent.prototype.showOrgDelete = function (node) {
        var _this = this;
        if (node.oid == 1) {
            this.showWarn('不可删除根组织！');
        }
        else {
            this.systemService.judgeOrgEditPower().subscribe(function (res) {
                console.log(res['errcode']);
                if (res['errcode'] == '00000') {
                    _this.systemService.getPersonList(node.oid).subscribe(function (data) {
                        if (data.length == 0) {
                            _this.deleteOrgTip = '请确认是否删除组织？';
                        }
                        else {
                            _this.deleteOrgTip = '存在关联人员，删除后组织人员将归并于上一级组织，请确认是否删除？';
                        }
                        _this.showOrgDeleteMask = true;
                        _this.deleteOid = node.oid;
                    }, function (err) {
                        console.log(err);
                    });
                }
                else {
                    _this.showError(res);
                }
            });
        }
    };
    //删除组织节点
    PersonManageComponent.prototype.deleteOrg = function () {
        var _this = this;
        this.systemService.deleteOrgNode([this.deleteOid]).subscribe(function (res) {
            if (res == '00000') {
                _this.showSuccess();
                _this.showOrgDeleteMask = false;
                _this.queryOrgTree('');
            }
        });
    };
    //弹出新增或者修改人员弹框
    PersonManageComponent.prototype.showPerEdit = function (datas) {
        var _this = this;
        this.systemService.judgePerEditPower().subscribe(function (res) {
            // console.log(res['errcode']);
            if (res['errcode'] == '00000') {
                // console.log(this.selected);
                _this.showPersonEditMask = true;
                if (!datas) {
                    // this.selectedCol = this.selected;
                    _this.selectedCol['label'] = _this.selected.label;
                    _this.selectedCol['oid'] = _this.selected['oid'];
                    _this.selectedCol['birthday'] = '';
                    _this.personState = 'add';
                }
                else {
                    // this.selectedCol = datas;
                    for (var key in datas) {
                        if (datas[key]) {
                            _this.selectedCol[key] = datas[key];
                        }
                        else {
                            _this.selectedCol[key] = '';
                        }
                    }
                    _this.selectedCol['label'] = _this.selectedCol['organization'];
                    _this.personState = 'update';
                }
            }
            else {
                _this.showError(res);
            }
        });
    };
    //显示删除人员弹框
    PersonManageComponent.prototype.showPerDelete = function (datas) {
        var _this = this;
        var deleteFlag = true;
        if (datas) {
            this.selectedPers = [];
            if (datas.oid == 1 && datas.name == '系统管理员') {
                deleteFlag = false;
            }
        }
        else {
            if (!this.selectedPers.length) {
                this.showWarn('请先选择删除项！');
            }
            for (var i = 0; i < this.selectedPers.length; i++) {
                if (this.selectedPers[i].oid == 1 && this.selectedPers[i].name == '系统管理员') {
                    deleteFlag = false;
                    break;
                }
            }
        }
        if (!deleteFlag) {
            this.showWarn('不可删除系统管理员！');
        }
        else {
            this.systemService.judgePerEditPower().subscribe(function (res) {
                if (res['errcode'] == '00000') {
                    _this.deletePerTip = '确认删除该人员？';
                    _this.deletePid = [];
                    if (datas) {
                        _this.showPerDeleteMask = true;
                        _this.deletePid.push(datas.pid);
                    }
                    else {
                        if (!_this.selectedPers.length) {
                            _this.showWarn('请先选择删除项！');
                        }
                        else {
                            _this.showPerDeleteMask = true;
                            for (var i = 0; i < _this.selectedPers.length; i++) {
                                _this.deletePid.push(_this.selectedPers[i].pid);
                            }
                        }
                    }
                }
                else {
                    _this.showError(res);
                }
            });
        }
    };
    //删除人员
    PersonManageComponent.prototype.deletePer = function () {
        var _this = this;
        this.systemService.deletePerson(this.deletePid).subscribe(function (res) {
            if (res == '00000') {
                _this.selectedPers = [];
                _this.showSuccess();
                _this.showPerDeleteMask = false;
                _this.queryPersonList(_this.selected ? _this.selected['oid'] : '');
            }
        });
    };
    //关闭新增或者修改人员弹框
    PersonManageComponent.prototype.closePersonEdit = function (bool) {
        this.showPersonEditMask = bool;
    };
    //新增人员成功
    PersonManageComponent.prototype.addPersonNode = function (bool) {
        this.showPersonEditMask = bool;
        this.queryPersonList(this.selected['oid']);
    };
    //修改人员成功
    PersonManageComponent.prototype.updatePersonNode = function (bool) {
        this.showPersonEditMask = bool;
        if (this.selected) {
            this.queryPersonList(this.selected['oid']);
        }
        else {
            this.queryPersonList('');
        }
    };
    //关联角色和查看角色
    PersonManageComponent.prototype.showCogRoleMask = function (datas, bool) {
        var _this = this;
        console.log(datas);
        if (bool) {
            this.systemService.judgeRoleEditPower().subscribe(function (res) {
                if (res['errcode'] == '00000') {
                    _this.showCognateRoleMask = true;
                    _this.selectedPid = datas;
                    _this.roleState = 'cognate';
                }
                else {
                    _this.showError(res);
                }
            });
        }
        else {
            this.showCognateRoleMask = true;
            this.selectedPid = datas;
            this.roleState = 'look';
        }
    };
    //关闭关联角色弹框
    PersonManageComponent.prototype.closeCognateRole = function (bool) {
        this.showCognateRoleMask = bool;
    };
    //关联角色成功
    PersonManageComponent.prototype.cognateRoleBack = function (bool) {
        this.showCognateRoleMask = bool;
        // this.queryOrgTree('');
    };
    //修改密码
    PersonManageComponent.prototype.modifyPassword = function (datas) {
        var _this = this;
        this.systemService.judgePerEditPower().subscribe(function (res) {
            if (res['errcode'] == '00000') {
                console.log(datas);
                _this.showPasswordModMask = true;
                _this.selectedNum = datas.pid;
            }
            else {
                _this.showError(res);
            }
        });
    };
    //关闭修改密码弹框
    PersonManageComponent.prototype.closePasswordMod = function (bool) {
        this.showPasswordModMask = bool;
    };
    //修改密码成功
    PersonManageComponent.prototype.passwordModifyBack = function (bool) {
        this.showPasswordModMask = bool;
    };
    // 组织树懒加载
    PersonManageComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.systemService.getOrgTree(event.node.oid).subscribe(function (data) {
                console.log(data);
                event.node.children = data;
            });
        }
    };
    // 组织树选中
    PersonManageComponent.prototype.NodeSelect = function (event) {
        this.canAdd = false;
        if (parseInt(event.node.dep) >= 1) {
            this.selectedPers = [];
            this.queryPersonList(event.node.oid);
        }
    };
    //查询组织树数据
    PersonManageComponent.prototype.queryOrgTree = function (oid) {
        var _this = this;
        this.systemService.getOrgTree(oid).subscribe(function (data) {
            console.log(data);
            _this.orgs = data;
        });
    };
    //查询人员表格数据
    PersonManageComponent.prototype.queryPersonList = function (oid) {
        var _this = this;
        this.systemService.getPersonList(oid).subscribe(function (data) {
            // console.log(data);
            _this.dataSource = data;
            _this.totalRecords = data.length;
            _this.tableDatas = _this.dataSource.slice(0, 10);
        });
    };
    PersonManageComponent.prototype.showWarn = function (operator) {
        this.msgs = [];
        // this.msgs.push({severity:'warn', summary:'注意', detail:'您没有' + operator + '权限！'});
        this.msgs.push({ severity: 'warn', summary: '注意', detail: operator });
    };
    //成功提示
    PersonManageComponent.prototype.showSuccess = function () {
        this.deletemsgs = [];
        this.deletemsgs.push({ severity: 'success', summary: '消息提示', detail: '删除成功' });
    };
    //错误提示
    PersonManageComponent.prototype.showError = function (res) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '消息提示', detail: res['errmsg'] });
    };
    PersonManageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-person-manage',
            template: __webpack_require__("../../../../../src/app/system/person-manage/person-manage.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/person-manage/person-manage.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__system_service__["a" /* SystemService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]])
    ], PersonManageComponent);
    return PersonManageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/role-edit/role-edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\r\n  margin-bottom: 1vw; }\r\n\r\n.padding-tblr {\r\n  padding: .25em .5em; }\r\n\r\n.start_red {\r\n  color: red; }\r\n\r\n.birthday {\r\n  display: inline-block; }\r\n\r\n@media screen and (max-width: 1366px) {\r\n  .ui-grid-col-1 {\r\n    width: 11.33333%; } }\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/system/role-edit/role-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"{{title}}\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"400\" [minWidth]=\"200\" (onHide)=\"closeRoleEditMask(false)\">\n  <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n    <form [formGroup]=\"role\">\n      <div class=\"ui-grid-row margin-bottom-1vw\">\n        <div class=\"ui-grid-col-3\">\n          <label for=\"\" >角色ID</label>\n        </div>\n        <div class=\"ui-grid-col-9\" id=\"\">\n          <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"rid\" pInputText [(ngModel)]=\"submitAddRole.rid\" *ngIf=\"state ==='add'\" readonly/>\n          <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"rid\" pInputText [(ngModel)]=\"currentOrg.rid\" *ngIf=\"state ==='update'\" readonly/>\n        </div>\n      </div>\n      <div class=\"ui-grid-row margin-bottom-1vw\">\n        <div class=\"ui-grid-col-3\">\n          <label for=\"\" >角色名称</label>\n        </div>\n        <div class=\"ui-grid-col-9\" id=\"\">\n          <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"name\" pInputText [(ngModel)]=\"submitAddRole.name\" *ngIf=\"state ==='add'\"/>\n          <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"name\" pInputText [(ngModel)]=\"currentOrg.name\" *ngIf=\"state ==='update'\"/>\n        </div>\n      </div>\n      <div class=\"ui-grid-row margin-bottom-1vw\">\n        <div class=\"ui-grid-col-3\">\n          <label for=\"\" >角色备注</label>\n        </div>\n        <div class=\"ui-grid-col-9\" id=\"\">\n          <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"remark\" pInputText [(ngModel)]=\"submitAddRole.remark\" *ngIf=\"state ==='add'\"/>\n          <input class=\"cursor_not_allowed\" type=\"text\" formControlName=\"remark\" pInputText [(ngModel)]=\"currentOrg.remark\" *ngIf=\"state ==='update'\"/>\n        </div>\n      </div>\n      <div class=\"ui-grid-row margin-bottom-1vw\">\n        <div class=\"ui-grid-col-6\"></div>\n        <div class=\"ui-grid-col-3 padding-tblr\">\n          <button pButton type=\"submit\" label=\"确认\" (click)=\"formSubmit(false)\"></button>\n        </div>\n        <div class=\"ui-grid-col-3 padding-tblr\">\n          <button pButton type=\"button\" label=\"取消\" (click)=\"closeRoleEditMask(false)\"></button>\n        </div>\n      </div>\n    </form>\n  </div>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/system/role-edit/role-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoleEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RoleEditComponent = (function () {
    function RoleEditComponent(systemService, storageService, fb, confirmationService) {
        this.systemService = systemService;
        this.storageService = storageService;
        this.fb = fb;
        this.confirmationService = confirmationService;
        this.closeRoleEdit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.addRole = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //新增角色
        this.updateRole = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //修改角色
        this.submitAddRole = {
            "rid": '',
            "name": '',
            "remark": '',
            "oid": ''
        };
        this.submitUpdateRole = [
            {
                "rid": '',
                "name": '',
                "remark": '',
                "oid": ''
            }
        ];
        this.role = fb.group({
            rid: [''],
            name: [''],
            remark: [''],
        });
    }
    RoleEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.display = true;
        // console.log(this.currentOrg)
        if (this.state == 'add') {
            this.title = '新增角色';
            this.submitAddRole.oid = this.currentOrg.oid;
            //获取角色id
            this.systemService.getRoleId(this.currentOrg.oid).subscribe(function (data) {
                // console.log(data);
                _this.submitAddRole.rid = data;
            });
        }
        else {
            this.title = '修改角色';
            this.submitUpdateRole[0].oid = this.currentOrg.oid;
            this.submitUpdateRole[0].rid = this.currentOrg.rid;
        }
    };
    //确定
    RoleEditComponent.prototype.formSubmit = function (bool) {
        if (this.state === 'add') {
            this.submitAddRole.name = this.role.value.name;
            this.submitAddRole.remark = this.role.value.remark;
            this.addRoleNode(bool);
        }
        else {
            this.submitUpdateRole[0].name = this.role.value.name;
            this.submitUpdateRole[0].remark = this.role.value.remark;
            this.updateRoleNode(bool);
        }
    };
    //新增角色
    RoleEditComponent.prototype.addRoleNode = function (bool) {
        var _this = this;
        this.systemService.addRole(this.submitAddRole).subscribe(function () {
            _this.addRole.emit(bool);
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    //修改角色
    RoleEditComponent.prototype.updateRoleNode = function (bool) {
        var _this = this;
        this.systemService.updateRole(this.submitUpdateRole).subscribe(function () {
            _this.updateRole.emit(_this.currentOrg);
            _this.closeRoleEdit.emit(bool);
        });
    };
    RoleEditComponent.prototype.closeRoleEditMask = function (bool) {
        this.closeRoleEdit.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], RoleEditComponent.prototype, "closeRoleEdit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], RoleEditComponent.prototype, "addRole", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], RoleEditComponent.prototype, "updateRole", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], RoleEditComponent.prototype, "currentOrg", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], RoleEditComponent.prototype, "state", void 0);
    RoleEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-role-edit',
            template: __webpack_require__("../../../../../src/app/system/role-edit/role-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/system/role-edit/role-edit.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__system_service__["a" /* SystemService */],
            __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["ConfirmationService"]])
    ], RoleEditComponent);
    return RoleEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/system/system-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SystemRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__person_manage_person_manage_component__ = __webpack_require__("../../../../../src/app/system/person-manage/person-manage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__jur_manage_jur_manage_component__ = __webpack_require__("../../../../../src/app/system/jur-manage/jur-manage.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var route = [
    { path: 'person', component: __WEBPACK_IMPORTED_MODULE_2__person_manage_person_manage_component__["a" /* PersonManageComponent */] },
    { path: 'jur', component: __WEBPACK_IMPORTED_MODULE_3__jur_manage_jur_manage_component__["a" /* JurManageComponent */] },
];
var SystemRoutingModule = (function () {
    function SystemRoutingModule() {
    }
    SystemRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild(route),
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]]
        })
    ], SystemRoutingModule);
    return SystemRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/system/system.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SystemModule", function() { return SystemModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__person_manage_person_manage_component__ = __webpack_require__("../../../../../src/app/system/person-manage/person-manage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__system_routing_module__ = __webpack_require__("../../../../../src/app/system/system-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__system_service__ = __webpack_require__("../../../../../src/app/system/system.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_share_module__ = __webpack_require__("../../../../../src/app/shared/share.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__org_edit_org_edit_component__ = __webpack_require__("../../../../../src/app/system/org-edit/org-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__person_edit_person_edit_component__ = __webpack_require__("../../../../../src/app/system/person-edit/person-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__cognate_role_cognate_role_component__ = __webpack_require__("../../../../../src/app/system/cognate-role/cognate-role.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__password_modify_password_modify_component__ = __webpack_require__("../../../../../src/app/system/password-modify/password-modify.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__jur_manage_jur_manage_component__ = __webpack_require__("../../../../../src/app/system/jur-manage/jur-manage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__jur_edit_jur_edit_component__ = __webpack_require__("../../../../../src/app/system/jur-edit/jur-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__role_edit_role_edit_component__ = __webpack_require__("../../../../../src/app/system/role-edit/role-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__jur_look_jur_look_component__ = __webpack_require__("../../../../../src/app/system/jur-look/jur-look.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__add_inspection_plan_tree_add_inspection_plan_tree_component__ = __webpack_require__("../../../../../src/app/system/add-inspection-plan-tree/add-inspection-plan-tree.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var SystemModule = (function () {
    function SystemModule() {
    }
    SystemModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_5__shared_share_module__["a" /* ShareModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__system_routing_module__["a" /* SystemRoutingModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__person_manage_person_manage_component__["a" /* PersonManageComponent */], __WEBPACK_IMPORTED_MODULE_6__org_edit_org_edit_component__["a" /* OrgEditComponent */], __WEBPACK_IMPORTED_MODULE_7__person_edit_person_edit_component__["a" /* PersonEditComponent */], __WEBPACK_IMPORTED_MODULE_8__cognate_role_cognate_role_component__["a" /* CognateRoleComponent */], __WEBPACK_IMPORTED_MODULE_9__password_modify_password_modify_component__["a" /* PasswordModifyComponent */], __WEBPACK_IMPORTED_MODULE_10__jur_manage_jur_manage_component__["a" /* JurManageComponent */], __WEBPACK_IMPORTED_MODULE_11__jur_edit_jur_edit_component__["a" /* JurEditComponent */], __WEBPACK_IMPORTED_MODULE_12__role_edit_role_edit_component__["a" /* RoleEditComponent */], __WEBPACK_IMPORTED_MODULE_13__jur_look_jur_look_component__["a" /* JurLookComponent */], __WEBPACK_IMPORTED_MODULE_14__add_inspection_plan_tree_add_inspection_plan_tree_component__["a" /* AddInspectionPlanTreeComponent */],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_4__system_service__["a" /* SystemService */]]
        })
    ], SystemModule);
    return SystemModule;
}());



/***/ }),

/***/ "../../../../../src/app/system/system.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SystemService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SystemService = (function () {
    function SystemService(http, storageService) {
        this.http = http;
        this.storageService = storageService;
    }
    //获取组织树数据
    SystemService.prototype.getOrgTree = function (oid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/org", {
            'access_token': token,
            'type': 'get_suborg',
            'dep': '1',
            'id': oid
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            //构造primeNG tree的数据
            var datas = res['datas'];
            for (var i = 0; i < datas.length; i++) {
                datas[i].label = datas[i].name;
                datas[i].leaf = false;
            }
            return datas;
        });
    };
    //获取人员数据
    SystemService.prototype.getPersonList = function (oid) {
        var token = this.storageService.getToken('token');
        var oids = [];
        if (oid) {
            oids.push(oid);
        }
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/personnel", {
            'access_token': token,
            'type': 'get_personnel',
            'id': [],
            'oid': oids
        }).map(function (res) {
            if (res['errcode'] == '00001') {
                return [];
            }
            else if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //新增组织节点前，获取节点信息
    SystemService.prototype.getCurrentNodeData = function (id) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/org", {
            'access_token': token,
            'type': 'add_org_id',
            'id': id
            // "id": treeNode.oid
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //  获取组织树
    SystemService.prototype.getDepartmentDatas = function (oid, dep) {
        (!oid) && (oid = '');
        (!dep) && (dep = '');
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/org", {
            "access_token": token,
            "type": "get_suborg",
            "id": oid,
            "dep": dep
        }).map(function (res) {
            // let body = res.json();
            // if( body.errcode !== '00000') {
            //   return [];
            // }else {
            //   body['datas'].forEach(function (e) {
            //     e.label = e.name;
            //     delete e.name;
            //     e.leaf = false;
            //     e.data = e.name;
            //   });
            // }
            // return body['datas'];
            if (res['errcode'] !== '00000') {
                return [];
            }
            else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.leaf = false;
                    e.data = e.name;
                });
            }
            return res['datas'];
        });
    };
    //判断用户是否有操作组织节点的权限
    SystemService.prototype.judgeOrgEditPower = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/org", {
            'access_token': token,
            'type': 'jur'
        }).map(function (res) {
            // console.log(res);
            return res;
        });
    };
    //新增组织节点
    SystemService.prototype.addOrgNode = function (data) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/org", {
            'access_token': token,
            'type': 'add_org',
            'data': data
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //修改组织节点
    SystemService.prototype.updateOrgNode = function (datas) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/org", {
            'access_token': token,
            'type': 'mod_org',
            'datas': datas
            /*"datas": [{
              "oid": $scope.curModifyId,
              "name": $scope.orgName,
              "remark": $scope.orgRemark,
              "dep": $scope.dep,
              "father": $scope.father
            }]*/
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //删除组织节点
    SystemService.prototype.deleteOrgNode = function (ids) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/org", {
            'access_token': token,
            'type': 'del_org',
            'ids': ids,
            'isdelperson': '0'
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['errcode'];
        });
    };
    //判断用户是否有操作人员的权限
    SystemService.prototype.judgePerEditPower = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/personnel", {
            'access_token': token,
            'type': 'jur'
        }).map(function (res) {
            return res;
        });
    };
    //新增人员
    SystemService.prototype.addPerson = function (data) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/personnel", {
            'access_token': token,
            'type': 'add_personnel',
            'data': data
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //修改人员
    SystemService.prototype.updatePerson = function (data) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/personnel", {
            'access_token': token,
            'type': 'mod_personnel',
            'data': data
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //删除人员
    SystemService.prototype.deletePerson = function (pids) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/personnel", {
            'access_token': token,
            'type': 'del_personnel',
            'id': pids
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['errcode'];
        });
    };
    //判断用户是否有操作角色的权限
    SystemService.prototype.judgeRoleEditPower = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'jur'
        }).map(function (res) {
            return res;
        });
    };
    //查看用户有什么可选的角色
    SystemService.prototype.getPerRoles = function (pid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'get_per_role_save',
            'id': pid
            // "id": $scope.pid
        }).map(function (res) {
            if (res['errcode'] == '00001') {
                return [];
            }
            else if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //保存关联角色
    SystemService.prototype.saveCognateRoles = function (rids, pid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'save_per_role',
            'datas': rids,
            'id': pid
            // "id": $scope.pid
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res;
        });
    };
    //修改密码
    SystemService.prototype.passwordModify = function (data) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/personnel", {
            'access_token': token,
            'type': 'mod_pwd',
            'data': data
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res;
        });
    };
    //获取角色数据
    SystemService.prototype.getJurList = function (oid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'get_role',
            'id': oid
        }).map(function (res) {
            if (res['errcode'] == '00001') {
                return [];
            }
            else if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //新增角色前获取角色id
    SystemService.prototype.getRoleId = function (oid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'add_role_id',
            'id': oid
        }).map(function (res) {
            if (res['errcode'] == '00001') {
                return [];
            }
            else if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //新增角色
    SystemService.prototype.addRole = function (data) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'add_role',
            'data': data
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //修改角色
    SystemService.prototype.updateRole = function (data) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'mod_role',
            'datas': data
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //删除角色
    SystemService.prototype.deleteRole = function (rids) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'del_role',
            'ids': rids
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['errcode'];
        });
    };
    //获取所有权限树的数据
    SystemService.prototype.getAllJurTreeData = function (mid, dep) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'get_per_menu',
            'sub': mid,
            'dep': dep,
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            //构造primeNG tree的数据
            var datas = res['datas'];
            for (var i = 0; i < datas.length; i++) {
                datas[i].label = datas[i].name;
                datas[i].leaf = false;
            }
            return datas;
        });
    };
    //获取角色已拥有的权限树的数据
    SystemService.prototype.getOwnJursTreeData = function (mid, dep, rid) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'get_role_jur',
            'rid': rid,
            'sub': mid,
            'dep': dep,
        }).map(function (res) {
            if (res['errcode'] == '00001') {
                return [];
            }
            else if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            //构造primeNG tree的数据
            var datas = res['datas'];
            for (var i = 0; i < datas.length; i++) {
                datas[i].label = datas[i].name;
                datas[i].leaf = false;
            }
            return datas;
        });
    };
    //修改权限
    SystemService.prototype.updateJur = function (rid, jids) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/jur", {
            'access_token': token,
            'type': 'save_role_jur',
            'rid': rid,
            'jids': jids
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    SystemService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */]])
    ], SystemService);
    return SystemService;
}());



/***/ })

});
//# sourceMappingURL=system.module.chunk.js.map