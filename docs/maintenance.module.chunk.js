webpackJsonp(["maintenance.module"],{

/***/ "../../../../../src/app/maintenance/basaldatas/malfunction/malfunction.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <div>\n            <span class=\"feature-title\">基础数据<span class=\"gt\">&gt;</span>故障管理</span>\n        </div>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"malfunctionBasalDatas\">\n    <div class=\"ui-grid-row\">\n        <div class=\"ui-grid-col-6\">\n            <h4>数据配置</h4>\n            <p-tree [value]=\"malfunctionDatas\"\n                    selectionMode=\"single\"\n                    [(selection)]=\"selected\"\n                    (onNodeExpand)=\"nodeExpand($event)\"\n                    (onNodeSelect) = \"NodeSelect($event)\"\n            ></p-tree>\n        </div>\n        <div class=\"ui-grid-col-6\">\n            <h4>{{ titleName }}</h4>\n            <div class=\"ui-grid-row text_aligin_right\">\n                <button pButton type=\"button\" class=\"btn_add\" (click)=\"add()\" label=\"新增\" icon=\"fa-plus\" [disabled]=\"canAdd\"></button>\n              <p-dataTable id=\"manageTable\" class=\"report\" [value]=\"malfunctionModel\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n                           [totalRecords]=\"totalRecords\" (onLazyLoad)=\"loadCarsLazy($event)\" [stacked]=\"stacked\" [responsive]=\"true\" [editable]=\"true\">\n                    <!--<p-footer>-->\n                        <!--<p-paginator rows=\"10\" totalRecords=\"120\" [rowsPerPageOptions]=\"[10,20,30]\" pageLinkSize=\"10\"></p-paginator>-->\n                    <!--</p-footer>-->\n                    <p-column selectionMode=\"multiple\" ></p-column>\n                    <p-column field=\"label\" header=\"名称\" [sortable]=\"true\"></p-column>\n                    <p-column field=\"status\" header=\"状态\" [sortable]=\"true\"></p-column>\n                    <p-column field=\"color\" header=\"操作\" [style]=\"{'width':'12vw'}\">\n                        <ng-template let-datas=\"rowData\" pTemplate=\"operator\">\n                            <button pButton type=\"button\"  (click)=\"edit(datas)\" label=\"编辑\"></button>\n                            <button pButton type=\"button\"  (click)=\"frezzeOrActive(datas)\" label=\"启用\"  *ngIf=\"datas.status == '冻结'\"></button>\n                            <button pButton type=\"button\"  (click)=\"frezzeOrActive(datas)\" label=\"冻结\"  *ngIf=\"datas.status == '启用'\"></button>\n                            <button pButton type=\"button\"  (click)=\"delete(datas)\" label=\"删除\" ></button>\n                            <button pButton type=\"button\"  (click)=\"view(datas)\" label=\"查看\"   ></button>\n                        </ng-template>\n                    </p-column>\n                    <ng-template pTemplate=\"emptymessage\">\n                        当前没有数据\n                    </ng-template>\n                </p-dataTable>\n            </div>\n        </div>\n    </div>\n\n    <p-dialog header=\"{{ title }}\" [(visible)]=\"addDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <div class=\"ui-grid-row\">\n            <p-messages [(value)]=\"msgs\"></p-messages>\n            <form [formGroup]=\"myForm\">\n                <div class=\"ui-grid-col-12\">\n                    <div class=\"ui-grid-col-2\">\n                        <label for=\"\">名称</label>\n                    </div>\n                    <div class=\"ui-grid-col-10\">\n                        <input  formControlName=\"nodeName\" type=\"text\" pInputText  [style.width.%]=\"90\" placeholder=\"请输入名称\" [(ngModel)]=\"dataName\" required=\"required\"  [class.my-dirty]=\"isDirty\"/>\n                    </div>\n                </div>\n                <div class=\"ui-grid-col-12\">\n                    <div class=\"ui-grid-col-2\">\n                        <label for=\"\">状态</label>\n                    </div>\n                    <div class=\"ui-grid-col-10\">\n                        <p-radioButton formControlName=\"nodeActive\" name=\"group\" value=\"启用\" label=\"启用\" [(ngModel)]=\"val1\" inputId=\"preopt3\"></p-radioButton>\n                        <p-radioButton formControlName=\"nodeUnactive\" name=\"group\" value=\"冻结\" label=\"冻结\" [(ngModel)]=\"val1\" inputId=\"preopt4\"></p-radioButton>\n                    </div>\n                </div>\n            </form>\n\n        </div>\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"ansureAddDialog()\" label=\"新增\" *ngIf=\"addOrEdit == 'add'\"></button>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"ansureEditDialog()\" label=\"编辑\" *ngIf=\"addOrEdit == 'edit'\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"cancelMask(false)\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-dialog header=\"删除确认框\" [(visible)]=\"dialogDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        确认删除吗？\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"sureDelete()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"dialogDisplay=false\" label=\"取消\" class=\"ui-button-secondary\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"msgPop\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/basaldatas/malfunction/malfunction.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n#malfunctionBasalDatas div div:nth-child(1) h4 {\n  background: lightgray;\n  text-align: center;\n  padding: 0.4em;\n  border-right: 1px solid darkgray; }\n\n#malfunctionBasalDatas div div:nth-child(1) p-tree /deep/ div {\n  width: 100%;\n  border: none; }\n\n#malfunctionBasalDatas div div:nth-child(2) h4 {\n  background: lightgray;\n  text-align: center;\n  padding: 0.4em;\n  border-right: 1px solid darkgray;\n  height: 1.9em; }\n\n#malfunctionBasalDatas div div:nth-child(2) p-dataTable /deep/ div div table thead tr th:nth-child(1) {\n  width: 2.4em; }\n\n#malfunctionBasalDatas div div:nth-child(2) p-dataTable /deep/ div div table tbody tr td {\n  text-align: center; }\n\n.text_aligin_right {\n  text-align: right; }\n\n.btn_add {\n  margin: .5em;\n  font-size: 16px; }\n\np-dataTable /deep/ table thead tr th:nth-child(4) {\n  width: 32% !important; }\n\n@media screen and (max-width: 1440px) {\n  p-dataTable /deep/ table thead tr th:nth-child(4) {\n    width: 46% !important; } }\n\n@media screen and (max-width: 1366px) {\n  p-dataTable /deep/ table thead tr th:nth-child(4) {\n    width: 49% !important; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/basaldatas/malfunction/malfunction.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MalfunctionComponent = (function () {
    function MalfunctionComponent(maintenanceService, publicService, fb) {
        this.maintenanceService = maintenanceService;
        this.publicService = publicService;
        this.fb = fb;
        this.malfunctionModel = [];
        this.msgs = []; // 表单验证提示
        this.idsArray = []; // 数据
    }
    MalfunctionComponent.prototype.ngOnInit = function () {
        this.initForm();
        this.getNodeTree();
        this.val1 = '启用';
        this.addDisplay = false;
        this.canAdd = true;
        this.dialogDisplay = false;
        this.isDisable = false;
    };
    MalfunctionComponent.prototype.getNodeTree = function () {
        var _this = this;
        this.publicService.getMalfunctionBasalDatas().subscribe(function (res) {
            _this.malfunctionDatas = res;
        });
    };
    MalfunctionComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            'nodeName': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'nodeActive': '',
            'nodeUnactive': ''
        });
    };
    Object.defineProperty(MalfunctionComponent.prototype, "isDirty", {
        get: function () {
            var valid = !this.myForm.controls['nodeName'].untouched && !this.myForm.controls['nodeName'].value;
            var validAgain = this.myForm.controls['nodeName'].untouched && !this.myForm.controls['nodeName'].value;
            // let result;
            // console.log('valid----->', valid);
            // console.log('validAgain----->', validAgain);
            return !(valid || validAgain);
        },
        enumerable: true,
        configurable: true
    });
    // 组织树懒加载
    MalfunctionComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getMalfunctionBasalDatas(event.node.did, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    // 组织树选中
    MalfunctionComponent.prototype.NodeSelect = function (event) {
        var _this = this;
        // console.log(event);
        // console.log(parseInt(event.node.label));
        if (parseInt(event.node.dep) >= 1) {
            this.titleName = event.node.label;
            this.nodeFather = event.node.did;
            this.nodeDep = event.node.dep;
            this.did = event.node.did;
            this.dep = event.node.dep;
            this.canAdd = false;
            this.publicService.getMalfunctionBasalDatas(event.node.did, event.node.dep).subscribe(function (res) {
                _this.tableDatas = res;
                _this.totalRecords = _this.tableDatas.length;
                _this.malfunctionModel = _this.tableDatas.slice(0, 10);
            });
        }
    };
    MalfunctionComponent.prototype.loadCarsLazy = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.tableDatas) {
                _this.malfunctionModel = _this.tableDatas.slice(event.first, (event.first + event.rows));
            }
        }, 250);
    };
    MalfunctionComponent.prototype.cancelMask = function (bool) {
        this.addDisplay = false;
        this.msgs = [];
    };
    MalfunctionComponent.prototype.add = function () {
        this.myForm.get('nodeName').enable({ onlySelf: true, emitEvent: true });
        this.addDisplay = true;
        this.addOrEdit = 'add';
        this.dataName = '';
        this.title = '新增';
    };
    MalfunctionComponent.prototype.ansureAddDialog = function () {
        var _this = this;
        if (!this.dataName) {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
        }
        else {
            this.maintenanceService.addMalfunctionBasalDatas(this.dataName, this.val1, this.nodeFather, this.nodeDep).subscribe(function (res) {
                // console.log(res);
                if (res === '00000') {
                    _this.msgPop = [];
                    _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '新增成功' });
                    _this.getNodeTree();
                }
                else {
                    _this.msgPop = [];
                    _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '新增失败' + '\n' + res });
                }
            });
            this.addDisplay = false;
        }
    };
    MalfunctionComponent.prototype.edit = function (data) {
        // console.log(data);
        this.myForm.get('nodeName').enable({ onlySelf: true, emitEvent: true });
        this.dataName = data.label;
        this.nodeDid = data.did;
        this.nodeDep = data.dep;
        this.addDisplay = true;
        this.addOrEdit = 'edit';
        this.val1 = data.status;
        this.title = '编辑';
    };
    MalfunctionComponent.prototype.ansureEditDialog = function () {
        var _this = this;
        if (!this.dataName) {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
        }
        else {
            this.maintenanceService.editMalfunctionBasalDatas(this.dataName, this.val1, this.nodeDid, this.nodeDep, this.nodeFather).subscribe(function (res) {
                // console.log(res);
                if (res === '00000') {
                    _this.msgPop = [];
                    _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '编辑成功' });
                    _this.getNodeTree();
                    _this.getNodeTree();
                }
                else {
                    _this.msgPop = [];
                    _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '编辑失败' + '\n' + res });
                }
            });
            this.addDisplay = false;
        }
    };
    // refreshDataTable() {
    //     this.publicService.getMalfunctionBasalDatas(this.did, this.dep).subscribe(res => {
    //         this.tableDatas = res;
    //     });
    // }
    MalfunctionComponent.prototype.frezzeOrActive = function (data) {
        var _this = this;
        // console.log(data);
        var status = '';
        (data.status === '启用') && (status = '冻结');
        (data.status === '冻结') && (status = '启用');
        this.maintenanceService.editMalfunctionBasalDatas(data.label, status, data.did, data.dep, data.father).subscribe(function (res) {
            if (res === '00000') {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: status + '成功' });
                _this.getNodeTree();
            }
            else {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: status + '失败' + '\n' + res });
            }
        });
    };
    MalfunctionComponent.prototype.delete = function (data) {
        this.dialogDisplay = true;
        this.idsArray = [];
        this.idsArray.push(data.did);
    };
    MalfunctionComponent.prototype.sureDelete = function () {
        var _this = this;
        this.maintenanceService.deleteMalfunctionDatas(this.idsArray).subscribe(function (res) {
            // console.log(res);
            if (res === '00000') {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '删除成功' });
                _this.getNodeTree();
            }
            else {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '删除失败' + '\n' + res });
            }
            _this.dialogDisplay = false;
        });
    };
    MalfunctionComponent.prototype.view = function (data) {
        // console.log(data);
        this.isDisable = true;
        this.title = '查看';
        this.addDisplay = true;
        this.dataName = data.label;
        this.val1 = data.status;
        this.addOrEdit = '';
        this.myForm.get('nodeName').disable({ onlySelf: true, emitEvent: true });
    };
    MalfunctionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction',
            template: __webpack_require__("../../../../../src/app/maintenance/basaldatas/malfunction/malfunction.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/basaldatas/malfunction/malfunction.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]])
    ], MalfunctionComponent);
    return MalfunctionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/formobj.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormobjModel; });
var FormobjModel = (function () {
    function FormobjModel(obj) {
        this.sid = obj && obj['sid'] || '';
        this.name = obj && obj['name'] || '';
        this.status = obj && obj['status'] || '';
        this.create_time = obj && obj['create_time'] || '';
        this.creator = obj && obj['creator'] || '';
        this.creator_pid = obj && obj['creator_pid'] || '';
        this.submitter = obj && obj['submitter'] || '';
        this.submitter_pid = obj && obj['submitter_pid'] || '';
        this.submitter_org = obj && obj['submitter_org'] || '';
        this.submitter_org_oid = obj && obj['submitter_org_oid'] || '';
        this.submitter_phone = obj && obj['submitter_phone'] || '';
        this.submitter_email = obj && obj['submitter_email'] || '';
        this.submitter_wechat = obj && obj['submitter_wechat'] || '';
        this.fromx = obj && obj['fromx'] || '';
        this.occurrence_time = obj && obj['occurrence_time'] || '';
        this.deadline = obj && obj['deadline'] || '';
        this.influence = obj && obj['influence'] || '';
        this.urgency = obj && obj['urgency'] || '';
        this.priority = obj && obj['priority'] || '';
        this.bt_system = obj && obj['bt_system'] || '';
        this.level = obj && obj['level'] || '';
        this.addr = obj && obj['addr'] || '';
        this.title = obj && obj['title'] || '';
        this.content = obj && obj['content'] || '';
        this.service = obj && obj['service'] || '';
        this.devices = obj && obj['devices'] || '';
        this.work_sheets = obj && obj['work_sheets'] || '';
        this.attachments = obj && obj['attachments'] || [];
        this.assign_per = obj && obj['assign_per'] || '';
        this.assign_per_pid = obj && obj['assign_per_pid'] || '';
        this.assign_time = obj && obj['assign_time'] || '';
        this.reassign_per = obj && obj['reassign_per'] || '';
        this.reassign_per_pid = obj && obj['reassign_per_pid'] || '';
        this.reassign_time = obj && obj['reassign_time'] || '';
        this.reassign_reason = obj && obj['reassign_reason'] || '';
        this.acceptor = obj && obj['acceptor'] || '';
        this.acceptor_pid = obj && obj['acceptor_pid'] || '';
        this.acceptor_org = obj && obj['acceptor_org'] || '';
        this.acceptor_org_oid = obj && obj['acceptor_org_oid'] || '';
        this.accept_time = obj && obj['accept_time'] || '';
        this.upgrade_per = obj && obj['upgrade_per'] || '';
        this.upgrade_per_pid = obj && obj['upgrade_per_pid'] || '';
        this.upgrade_time = obj && obj['upgrade_time'] || '';
        this.upgrade_reason = obj && obj['upgrade_reason'] || '';
        this.processor = obj && obj['processor'] || '';
        this.processor_pid = obj && obj['processor_pid'] || '';
        this.processor_org = obj && obj['processor_org'] || '';
        this.processor_org_oid = obj && obj['processor_org_oid'] || '';
        this.process_time = obj && obj['process_time'] || '';
        this.finish_time = obj && obj['finish_time'] || '';
        this.process_times = obj && obj['process_times'] || '';
        this.working_hours = obj && obj['working_hours'] || '';
        this.solve_per = obj && obj['solve_per'] || '';
        this.solve_per_name = obj && obj['solve_per_name'] || '';
        this.solve_org = obj && obj['solve_org'] || '';
        this.solve_org_name = obj && obj['solve_org_name'] || '';
        this.reason = obj && obj['reason'] || '';
        this.means = obj && obj['means'] || '';
        this.lasting = obj && obj['lasting'] || '';
        this.close_time = obj && obj['close_time'] || '';
        this.close_code = obj && obj['close_code'] || '';
    }
    return FormobjModel;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/maintenance-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaintenanceRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basaldatas_malfunction_malfunction_component__ = __webpack_require__("../../../../../src/app/maintenance/basaldatas/malfunction/malfunction.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__malfunction_malfunction_base_malfunction_base_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-base/malfunction-base.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__malfunction_malfunction_add_malfunction_add_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-add/malfunction-add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__malfunction_malfunction_change_malfunction_change_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-change/malfunction-change.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__malfunction_malfunction_deal_malfunction_deal_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-deal/malfunction-deal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__malfunction_malfunction_distribute_malfunction_distribute_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-distribute/malfunction-distribute.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__malfunction_malfunction_off_malfunction_off_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-off/malfunction-off.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__malfunction_malfunction_receive_malfunction_receive_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-receive/malfunction-receive.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__malfunction_malfunction_solve_malfunction_solve_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solve/malfunction-solve.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__malfunction_malfunction_overview_malfunction_overview_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-overview/malfunction-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__malfunction_malfunction_edit_malfunction_edit_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-edit/malfunction-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__malfunction_malfunction_solved_malfunction_solved_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solved/malfunction-solved.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__malfunction_malfunction_view_malfunction_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-view/malfunction-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__malfunction_malfunction_accept_malfunction_accept_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept/malfunction-accept.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__malfunction_malfunction_process_malfunction_process_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-process/malfunction-process.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__malfunction_malfunction_up_malfunction_up_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-up/malfunction-up.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__malfunction_malfunction_turn_malfunction_turn_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-turn/malfunction-turn.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__malfunction_malfunction_close_malfunction_close_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-close/malfunction-close.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__malfunction_malfunction_accept_edit_malfunction_accept_edit_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept-edit/malfunction-accept-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__malfunction_malfunction_accept_view_malfunction_accept_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept-view/malfunction-accept-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__malfunction_malfunction_close_view_malfunction_close_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-close-view/malfunction-close-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__malfunction_malfunction_closed_view_malfunction_closed_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-closed-view/malfunction-closed-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__malfunction_malfunction_solved_view_malfunction_solved_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solved-view/malfunction-solved-view.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__basaldatas_malfunction_malfunction_component__["a" /* MalfunctionComponent */] },
    { path: 'malfunctionBasalDatas', component: __WEBPACK_IMPORTED_MODULE_2__basaldatas_malfunction_malfunction_component__["a" /* MalfunctionComponent */] },
    { path: 'malfunctionBase', component: __WEBPACK_IMPORTED_MODULE_3__malfunction_malfunction_base_malfunction_base_component__["a" /* MalfunctionBaseComponent */], children: [
            { path: 'malfunctionOverview', component: __WEBPACK_IMPORTED_MODULE_11__malfunction_malfunction_overview_malfunction_overview_component__["a" /* MalfunctionOverviewComponent */] },
            { path: 'malfunctionAdd', component: __WEBPACK_IMPORTED_MODULE_4__malfunction_malfunction_add_malfunction_add_component__["a" /* MalfunctionAddComponent */] },
            { path: 'malfunctionChange', component: __WEBPACK_IMPORTED_MODULE_5__malfunction_malfunction_change_malfunction_change_component__["a" /* MalfunctionChangeComponent */] },
            { path: 'malfuntionDeal', component: __WEBPACK_IMPORTED_MODULE_6__malfunction_malfunction_deal_malfunction_deal_component__["a" /* MalfunctionDealComponent */] },
            { path: 'malfunctionDistribute', component: __WEBPACK_IMPORTED_MODULE_7__malfunction_malfunction_distribute_malfunction_distribute_component__["a" /* MalfunctionDistributeComponent */] },
            { path: 'malfunctionOff', component: __WEBPACK_IMPORTED_MODULE_8__malfunction_malfunction_off_malfunction_off_component__["a" /* MalfunctionOffComponent */] },
            { path: 'malfunctionReceive', component: __WEBPACK_IMPORTED_MODULE_9__malfunction_malfunction_receive_malfunction_receive_component__["a" /* MalfunctionReceiveComponent */] },
            { path: 'malfunctionSolve', component: __WEBPACK_IMPORTED_MODULE_10__malfunction_malfunction_solve_malfunction_solve_component__["a" /* MalfunctionSolveComponent */] },
            { path: 'malfunctionEdit', component: __WEBPACK_IMPORTED_MODULE_12__malfunction_malfunction_edit_malfunction_edit_component__["a" /* MalfunctionEditComponent */] },
            { path: 'malfunctionSolved', component: __WEBPACK_IMPORTED_MODULE_13__malfunction_malfunction_solved_malfunction_solved_component__["a" /* MalfunctionSolvedComponent */] },
            { path: 'malfunctionSolvedView', component: __WEBPACK_IMPORTED_MODULE_24__malfunction_malfunction_solved_view_malfunction_solved_view_component__["a" /* MalfunctionSolvedViewComponent */] },
            { path: 'malfunctionView', component: __WEBPACK_IMPORTED_MODULE_14__malfunction_malfunction_view_malfunction_view_component__["a" /* MalfunctionViewComponent */] },
            { path: 'malfunctionAccept', component: __WEBPACK_IMPORTED_MODULE_15__malfunction_malfunction_accept_malfunction_accept_component__["a" /* MalfunctionAcceptComponent */] },
            { path: 'malfunctionAcceptEdit', component: __WEBPACK_IMPORTED_MODULE_20__malfunction_malfunction_accept_edit_malfunction_accept_edit_component__["a" /* MalfunctionAcceptEditComponent */] },
            { path: 'malfunctionAcceptView', component: __WEBPACK_IMPORTED_MODULE_21__malfunction_malfunction_accept_view_malfunction_accept_view_component__["a" /* MalfunctionAcceptViewComponent */] },
            { path: 'malfunctionProcess', component: __WEBPACK_IMPORTED_MODULE_16__malfunction_malfunction_process_malfunction_process_component__["a" /* MalfunctionProcessComponent */] },
            { path: 'malfunctionUp', component: __WEBPACK_IMPORTED_MODULE_17__malfunction_malfunction_up_malfunction_up_component__["a" /* MalfunctionUpComponent */] },
            { path: 'malfunctionTurn', component: __WEBPACK_IMPORTED_MODULE_18__malfunction_malfunction_turn_malfunction_turn_component__["a" /* MalfunctionTurnComponent */] },
            { path: 'malfunctionClose', component: __WEBPACK_IMPORTED_MODULE_19__malfunction_malfunction_close_malfunction_close_component__["a" /* MalfunctionCloseComponent */] },
            { path: 'malfunctionCloseView', component: __WEBPACK_IMPORTED_MODULE_22__malfunction_malfunction_close_view_malfunction_close_view_component__["a" /* MalfunctionCloseViewComponent */] },
            { path: 'malfunctionClosedView', component: __WEBPACK_IMPORTED_MODULE_23__malfunction_malfunction_closed_view_malfunction_closed_view_component__["a" /* MalfunctionClosedViewComponent */] }
        ] }
];
var MaintenanceRoutingModule = (function () {
    function MaintenanceRoutingModule() {
    }
    MaintenanceRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]]
        })
    ], MaintenanceRoutingModule);
    return MaintenanceRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/maintenance.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaintenanceModule", function() { return MaintenanceModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_share_module__ = __webpack_require__("../../../../../src/app/shared/share.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_routing_module__ = __webpack_require__("../../../../../src/app/maintenance/maintenance-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__basaldatas_malfunction_malfunction_component__ = __webpack_require__("../../../../../src/app/maintenance/basaldatas/malfunction/malfunction.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__malfunction_malfunction_base_malfunction_base_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-base/malfunction-base.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__malfunction_malfunction_add_malfunction_add_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-add/malfunction-add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__malfunction_malfunction_distribute_malfunction_distribute_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-distribute/malfunction-distribute.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__malfunction_malfunction_receive_malfunction_receive_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-receive/malfunction-receive.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__malfunction_malfunction_deal_malfunction_deal_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-deal/malfunction-deal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__malfunction_malfunction_change_malfunction_change_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-change/malfunction-change.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__malfunction_malfunction_solve_malfunction_solve_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solve/malfunction-solve.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__malfunction_malfunction_off_malfunction_off_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-off/malfunction-off.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__malfunction_malfunction_overview_malfunction_overview_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-overview/malfunction-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__malfunction_malfunction_edit_malfunction_edit_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-edit/malfunction-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__malfunction_public_btn_edit_btn_edit_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-edit/btn-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__malfunction_public_btn_delete_btn_delete_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-delete/btn-delete.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__malfunction_public_btn_view_btn_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-view/btn-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__malfunction_public_btn_distribute_btn_distribute_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-distribute/btn-distribute.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__malfunction_public_btn_receive_btn_receive_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-receive/btn-receive.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__malfunction_public_btn_reject_btn_reject_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-reject/btn-reject.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__malfunction_public_btn_deal_btn_deal_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-deal/btn-deal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__malfunction_public_btn_solve_btn_solve_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-solve/btn-solve.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__malfunction_public_btn_off_btn_off_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-off/btn-off.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__malfunction_public_search_form_search_form_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/search-form/search-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__malfunction_public_malfunction_table_malfunction_table_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/malfunction-table/malfunction-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__malfunction_malfunction_solved_malfunction_solved_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solved/malfunction-solved.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__malfunction_malfunction_view_malfunction_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-view/malfunction-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__malfunction_malfunction_accept_malfunction_accept_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept/malfunction-accept.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__malfunction_malfunction_process_malfunction_process_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-process/malfunction-process.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__malfunction_malfunction_up_malfunction_up_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-up/malfunction-up.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__malfunction_malfunction_turn_malfunction_turn_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-turn/malfunction-turn.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__malfunction_malfunction_close_malfunction_close_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-close/malfunction-close.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__malfunction_public_personel_dialog_personel_dialog_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/personel-dialog/personel-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__malfunction_malfunction_accept_edit_malfunction_accept_edit_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept-edit/malfunction-accept-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__malfunction_malfunction_accept_view_malfunction_accept_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept-view/malfunction-accept-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__malfunction_public_simple_view_simple_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-view/simple-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__malfunction_public_upload_file_view_upload_file_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/upload-file-view/upload-file-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__malfunction_public_base_view_base_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/base-view/base-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__malfunction_public_colse_base_view_colse_base_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/colse-base-view/colse-base-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__malfunction_malfunction_close_view_malfunction_close_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-close-view/malfunction-close-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__malfunction_malfunction_closed_view_malfunction_closed_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-closed-view/malfunction-closed-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__malfunction_malfunction_solved_view_malfunction_solved_view_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solved-view/malfunction-solved-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__malfunction_public_btn_up_btn_up_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-up/btn-up.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__malfunction_public_btn_down_btn_down_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-down/btn-down.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__malfunction_public_simple_time_simple_time_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-time/simple-time.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__malfunction_public_simple_close_simple_close_component__ = __webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-close/simple-close.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















































var MaintenanceModule = (function () {
    function MaintenanceModule() {
    }
    MaintenanceModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__shared_share_module__["a" /* ShareModule */],
                __WEBPACK_IMPORTED_MODULE_2__maintenance_routing_module__["a" /* MaintenanceRoutingModule */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__basaldatas_malfunction_malfunction_component__["a" /* MalfunctionComponent */],
                __WEBPACK_IMPORTED_MODULE_5__malfunction_malfunction_base_malfunction_base_component__["a" /* MalfunctionBaseComponent */],
                __WEBPACK_IMPORTED_MODULE_6__malfunction_malfunction_add_malfunction_add_component__["a" /* MalfunctionAddComponent */],
                __WEBPACK_IMPORTED_MODULE_7__malfunction_malfunction_distribute_malfunction_distribute_component__["a" /* MalfunctionDistributeComponent */],
                __WEBPACK_IMPORTED_MODULE_8__malfunction_malfunction_receive_malfunction_receive_component__["a" /* MalfunctionReceiveComponent */],
                __WEBPACK_IMPORTED_MODULE_9__malfunction_malfunction_deal_malfunction_deal_component__["a" /* MalfunctionDealComponent */],
                __WEBPACK_IMPORTED_MODULE_10__malfunction_malfunction_change_malfunction_change_component__["a" /* MalfunctionChangeComponent */],
                __WEBPACK_IMPORTED_MODULE_11__malfunction_malfunction_solve_malfunction_solve_component__["a" /* MalfunctionSolveComponent */],
                __WEBPACK_IMPORTED_MODULE_12__malfunction_malfunction_off_malfunction_off_component__["a" /* MalfunctionOffComponent */],
                __WEBPACK_IMPORTED_MODULE_13__malfunction_malfunction_overview_malfunction_overview_component__["a" /* MalfunctionOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_14__malfunction_malfunction_edit_malfunction_edit_component__["a" /* MalfunctionEditComponent */],
                __WEBPACK_IMPORTED_MODULE_16__malfunction_public_btn_edit_btn_edit_component__["a" /* BtnEditComponent */],
                __WEBPACK_IMPORTED_MODULE_17__malfunction_public_btn_delete_btn_delete_component__["a" /* BtnDeleteComponent */],
                __WEBPACK_IMPORTED_MODULE_18__malfunction_public_btn_view_btn_view_component__["a" /* BtnViewComponent */],
                __WEBPACK_IMPORTED_MODULE_19__malfunction_public_btn_distribute_btn_distribute_component__["a" /* BtnDistributeComponent */],
                __WEBPACK_IMPORTED_MODULE_20__malfunction_public_btn_receive_btn_receive_component__["a" /* BtnReceiveComponent */],
                __WEBPACK_IMPORTED_MODULE_21__malfunction_public_btn_reject_btn_reject_component__["a" /* BtnRejectComponent */],
                __WEBPACK_IMPORTED_MODULE_22__malfunction_public_btn_deal_btn_deal_component__["a" /* BtnDealComponent */],
                __WEBPACK_IMPORTED_MODULE_23__malfunction_public_btn_solve_btn_solve_component__["a" /* BtnSolveComponent */],
                __WEBPACK_IMPORTED_MODULE_24__malfunction_public_btn_off_btn_off_component__["a" /* BtnOffComponent */],
                __WEBPACK_IMPORTED_MODULE_25__malfunction_public_search_form_search_form_component__["a" /* SearchFormComponent */],
                __WEBPACK_IMPORTED_MODULE_26__malfunction_public_malfunction_table_malfunction_table_component__["a" /* MalfunctionTableComponent */],
                __WEBPACK_IMPORTED_MODULE_27__malfunction_malfunction_solved_malfunction_solved_component__["a" /* MalfunctionSolvedComponent */],
                __WEBPACK_IMPORTED_MODULE_28__malfunction_malfunction_view_malfunction_view_component__["a" /* MalfunctionViewComponent */],
                __WEBPACK_IMPORTED_MODULE_29__malfunction_malfunction_accept_malfunction_accept_component__["a" /* MalfunctionAcceptComponent */],
                __WEBPACK_IMPORTED_MODULE_30__malfunction_malfunction_process_malfunction_process_component__["a" /* MalfunctionProcessComponent */],
                __WEBPACK_IMPORTED_MODULE_31__malfunction_malfunction_up_malfunction_up_component__["a" /* MalfunctionUpComponent */],
                __WEBPACK_IMPORTED_MODULE_32__malfunction_malfunction_turn_malfunction_turn_component__["a" /* MalfunctionTurnComponent */],
                __WEBPACK_IMPORTED_MODULE_33__malfunction_malfunction_close_malfunction_close_component__["a" /* MalfunctionCloseComponent */],
                __WEBPACK_IMPORTED_MODULE_34__malfunction_public_personel_dialog_personel_dialog_component__["a" /* PersonelDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_35__malfunction_malfunction_accept_edit_malfunction_accept_edit_component__["a" /* MalfunctionAcceptEditComponent */],
                __WEBPACK_IMPORTED_MODULE_36__malfunction_malfunction_accept_view_malfunction_accept_view_component__["a" /* MalfunctionAcceptViewComponent */],
                __WEBPACK_IMPORTED_MODULE_37__malfunction_public_simple_view_simple_view_component__["a" /* SimpleViewComponent */],
                __WEBPACK_IMPORTED_MODULE_38__malfunction_public_upload_file_view_upload_file_view_component__["a" /* UploadFileViewComponent */],
                __WEBPACK_IMPORTED_MODULE_39__malfunction_public_base_view_base_view_component__["a" /* BaseViewComponent */],
                __WEBPACK_IMPORTED_MODULE_40__malfunction_public_colse_base_view_colse_base_view_component__["a" /* ColseBaseViewComponent */],
                __WEBPACK_IMPORTED_MODULE_41__malfunction_malfunction_close_view_malfunction_close_view_component__["a" /* MalfunctionCloseViewComponent */],
                __WEBPACK_IMPORTED_MODULE_42__malfunction_malfunction_closed_view_malfunction_closed_view_component__["a" /* MalfunctionClosedViewComponent */],
                __WEBPACK_IMPORTED_MODULE_43__malfunction_malfunction_solved_view_malfunction_solved_view_component__["a" /* MalfunctionSolvedViewComponent */],
                __WEBPACK_IMPORTED_MODULE_44__malfunction_public_btn_up_btn_up_component__["a" /* BtnUpComponent */],
                __WEBPACK_IMPORTED_MODULE_45__malfunction_public_btn_down_btn_down_component__["a" /* BtnDownComponent */],
                __WEBPACK_IMPORTED_MODULE_47__malfunction_public_simple_time_simple_time_component__["a" /* SimpleTimeComponent */],
                __WEBPACK_IMPORTED_MODULE_48__malfunction_public_simple_close_simple_close_component__["a" /* SimpleCloseComponent */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__maintenance_service__["a" /* MaintenanceService */],
                __WEBPACK_IMPORTED_MODULE_15__services_public_service__["a" /* PublicService */],
                __WEBPACK_IMPORTED_MODULE_46__matenanceObj_model__["a" /* MatenanceObjModel */]
            ]
        })
    ], MaintenanceModule);
    return MaintenanceModule;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/maintenance.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaintenanceService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import {Http, Response} from '@angular/http';



var MaintenanceService = (function () {
    function MaintenanceService(http, storageService) {
        this.http = http;
        this.storageService = storageService;
        this.ip = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management;
    }
    //    新增故障管理基础数据
    MaintenanceService.prototype.addMalfunctionBasalDatas = function (name, status, father, dep) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!father) && (father = '');
        (!dep) && (dep = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/foundationdata", {
            'access_token': token,
            'type': 'trouble_tree_add',
            'data': {
                'name': name,
                'remark': '',
                'del': '',
                'dep': dep,
                'father': father,
                'status': status,
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    编辑/冻结/启用故障管理基础数据
    MaintenanceService.prototype.editMalfunctionBasalDatas = function (name, status, did, dep, father) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!did) && (did = '');
        (!dep) && (dep = '');
        (!father) && (father = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/foundationdata", {
            'access_token': token,
            'type': 'trouble_tree_mod',
            'data': {
                'did': did,
                'name': name,
                'remark': '',
                'del': '',
                'dep': dep,
                'father': father,
                'status': status,
            }
        }).map(function (res) {
            // let body = res.json();
            // if ( body['errcode'] !== '00000') {
            //     return body['errmsg'];
            // }
            // return body['errcode'];
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    删除故障管理基础数据
    MaintenanceService.prototype.deleteMalfunctionDatas = function (ids) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/foundationdata", {
            'access_token': token,
            'type': 'trouble_tree_del',
            'ids': ids
        }).map(function (res) {
            // let body = res.json();
            // if ( body['errcode'] !== '00000') {
            //     return body['errmsg'];
            // }
            // return body['errcode'];
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //  获取服务请求基础数据配置树
    MaintenanceService.prototype.getServiceRequestBasalDatas = function (id, dep) {
        (!id) && (id = '');
        (!dep) && (dep = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/org", {
            'access_token': token,
            'type': 'servicerequest_tree_get',
            'data': {
                'dep': dep,
                'id': id
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.leaf = false;
                    e.data = e.name;
                });
            }
            return res['datas'];
        });
    };
    //    新增服务请求基础数据
    MaintenanceService.prototype.addServiceRequestBasalDatas = function (name, status, father, dep) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!father) && (father = '');
        (!dep) && (dep = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/org", {
            'access_token': token,
            'type': 'servicerequest_tree_add',
            'data': {
                'name': name,
                'remark': '',
                'del': '',
                'dep': dep,
                'father': father,
                'status': status,
            }
        }).map(function (res) {
            // let body = res.json();
            // if ( body['errcode'] !== '00000') {
            //     return body['errmsg'];
            // }
            // return body['errcode'];
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    编辑/冻结/启用服务请求基础数据
    MaintenanceService.prototype.editRequestBasalDatas = function (name, status, did, dep, father) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!did) && (did = '');
        (!dep) && (dep = '');
        (!father) && (father = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/org", {
            'access_token': token,
            'type': 'servicerequest_tree_mod',
            'data': {
                'did': did,
                'name': name,
                'remark': '',
                'del': '',
                'dep': dep,
                'father': father,
                'status': status,
            }
        }).map(function (res) {
            // let body = res.json();
            // if ( body['errcode'] !== '00000') {
            //     return body['errmsg'];
            // }
            // return body['errcode'];
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    删除故障管理基础数据
    MaintenanceService.prototype.deleteServiceRequestDatas = function (ids) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/org", {
            'access_token': token,
            'type': 'servicerequest_tree_del',
            'ids': ids
        }).map(function (res) {
            // let body = res.json();
            // if ( body['errcode'] !== '00000') {
            //     return body['errmsg'];
            // }
            // return body['errcode'];
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //  3.故障管理所有状态获取接口
    MaintenanceService.prototype.getAllMalfunctionStatus = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_statuslist_get'
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    };
    //    流程图接口获取
    MaintenanceService.prototype.getFlowChart = function (status) {
        (!status) && (status = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_workflow_get',
            'data': {
                'status': status
            }
        }).map(function (res) {
            // let body = res.json();
            // if ( body.errcode !== '00000') {
            //     return [];
            // }
            // return body['datas'];
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    };
    //  获取、查询故障管理总览列表接口
    MaintenanceService.prototype.getAllMainfunctionList = function (size, number, sid, submitter, time, status) {
        (!number) && (number = '1');
        (!size) && (size = '10');
        (!sid) && (sid = '');
        (!submitter) && (submitter = '');
        (!time) && (time = '');
        (!status) && (status = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_get',
            'data': {
                'condition': {
                    'sid': sid,
                    'submitter': submitter,
                    'occurrence_time': time,
                    'status': status
                },
                'page': {
                    'page_size': size,
                    'page_number': number
                }
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    };
    //  获取组织树
    MaintenanceService.prototype.getDepartmentDatas = function (oid, dep) {
        (!oid) && (oid = '');
        (!dep) && (dep = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/org", {
            'access_token': token,
            'type': 'get_suborg',
            'id': oid,
            'dep': dep
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.leaf = false;
                    e.data = e.name;
                });
            }
            return res['datas'];
        });
    };
    //    故障管理来源、影响度、紧急度接口
    MaintenanceService.prototype.getParamOptions = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/foundationdata", {
            'access_token': token,
            'type': 'trouble_config_get',
            'ids': [
                '来源',
                '影响度',
                '紧急度',
                '故障级别',
                '所属系统'
            ]
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    };
    //  故障管理优先级获取接口
    MaintenanceService.prototype.getPrority = function (impact, uergency) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/foundationdata", {
            'access_token': token,
            'type': 'trouble_priority_get',
            'data': {
                'influence': impact,
                'urgency': uergency
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    };
    //  获取最终期限
    MaintenanceService.prototype.getDeadline = function (influency, urgency, prority, time_source) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/foundationdata", {
            'access_token': token,
            'type': 'trouble_deadline_get',
            'data': {
                'time_source': time_source,
                'influence': influency,
                'urgency': urgency,
                'priority': prority
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    };
    //  故障单保存
    MaintenanceService.prototype.saveTicket = function (ticket) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_draft',
            'data': {
                'sid': ticket['sid'],
                'name': ticket['name'],
                'fromx': ticket['fromx'],
                'status': ticket['status'],
                'submitter': ticket['submitter'],
                'submitter_pid': ticket['submitter_pid'],
                'submitter_org': ticket['submitter_org'],
                'submitter_org_oid': ticket['submitter_org_oid'],
                'submitter_phone': ticket['submitter_phone'],
                'occurrence_time': ticket['occurrence_time'],
                'deadline': ticket['deadline'],
                'influence': ticket['influence'],
                'urgency': ticket['urgency'],
                'priority': ticket['priority'],
                'level': ticket['level'],
                'bt_system': ticket['bt_system'],
                'addr': ticket['addr'],
                'service': ticket['service'],
                'devices': ticket['devices'],
                'title': ticket['title'],
                'content': ticket['content'],
                'attachments': ticket['attachments'],
                'acceptor': ticket['acceptor'],
                'acceptor_pid': ticket['acceptor_pid'],
                'acceptor_org': ticket['acceptor_org'],
                'acceptor_org_oid': ticket['acceptor_org_oid']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //  故障单提交、提交并分配
    MaintenanceService.prototype.submitTicket = function (ticket) {
        var token = this.storageService.getToken('token');
        (!ticket['fromx']) && (ticket['fromx'] = '');
        (!ticket['level']) && (ticket['level'] = '');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_add',
            'data': {
                'sid': ticket['sid'],
                'name': ticket['name'],
                'fromx': ticket['fromx'],
                'status': ticket['status'],
                'submitter': ticket['submitter'],
                'submitter_pid': ticket['submitter_pid'],
                'submitter_org': ticket['submitter_org'],
                'submitter_org_oid': ticket['submitter_org_oid'],
                'submitter_phone': ticket['submitter_phone'],
                'occurrence_time': ticket['occurrence_time'],
                'deadline': ticket['deadline'],
                'influence': ticket['influence'],
                'urgency': ticket['urgency'],
                'priority': ticket['priority'],
                'level': ticket['level'],
                'bt_system': ticket['bt_system'],
                'addr': ticket['addr'],
                'service': ticket['service'],
                'devices': ticket['devices'],
                'title': ticket['title'],
                'content': ticket['content'],
                'attachments': ticket['attachments'],
                'acceptor': ticket['acceptor'],
                'acceptor_pid': ticket['acceptor_pid'],
                'acceptor_org': ticket['acceptor_org'],
                'acceptor_org_oid': ticket['acceptor_org_oid']
            }
        }).map(function (res) {
            // let body = res.json();
            // if ( body.errcode !== '00000') {
            //     return body['errmsg'];
            // }
            // return body['errcode'];
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //  故障单刪除
    MaintenanceService.prototype.deleteTrick = function (ids) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_del',
            'ids': ids
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    故障单信息获取
    MaintenanceService.prototype.getTrick = function (trickSid) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_get_bysid',
            'id': trickSid
        }).map(function (res) {
            return res['data'];
        });
    };
    //    故障单接受、拒绝
    MaintenanceService.prototype.recieveOrRejectTrick = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_accept',
            'data': {
                'sid': obj['sid'],
                'status': obj['status']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //   故障单处理
    MaintenanceService.prototype.dealTrick = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_process',
            'data': {
                'sid': obj['sid'],
                'solve_per': obj['solve_per'],
                'solve_per_pid': obj['solve_per_pid'],
                'solve_org': obj['solve_org'],
                'solve_org_oid': obj['solve_org_oid'],
                'reason': obj['reason'],
                'means': obj['means'],
                'finish_time': obj['finish_time'],
                'status': obj['status']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    故障单解决
    MaintenanceService.prototype.solveTrick = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_finish',
            'data': {
                'sid': obj['sid'],
                'solve_per': obj['solve_per'],
                'solve_per_pid': obj['solve_per_pid'],
                'solve_org': obj['solve_org'],
                'solve_org_oid': obj['solve_org_oid'],
                'reason': obj['reason'],
                'means': obj['means'],
                'finish_time': __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(obj['finish_time']),
                'status': obj['status']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    故障单关闭
    MaintenanceService.prototype.offTrick = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_close',
            'data': {
                'sid': obj['sid'],
                'close_code': obj['close_code']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //  故障单升级
    MaintenanceService.prototype.upTrick = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_upgrade',
            'data': {
                'sid': obj['sid'],
                'processor': obj['processor'],
                'processor_pid': obj['processor_pid'],
                'processor_org': obj['processor_org'],
                'processor_org_oid': obj['processor_org_oid'],
                'reassign_reason': obj['reassign_reason'],
                'upgrade_reason': obj['upgrade_reason']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    故障单转派
    MaintenanceService.prototype.turnTrick = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_reassign',
            'data': {
                'sid': obj['sid'],
                'processor': obj['processor'],
                'processor_pid': obj['processor_pid'],
                'processor_org': obj['processor_org'],
                'processor_org_oid': obj['processor_org_oid'],
                'reassign_reason': obj['reassign_reason']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    故障单解决保存
    MaintenanceService.prototype.solvedSubmit = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_finish',
            'data': {
                'sid': obj['sid'],
                'solve_per': obj['solve_per'],
                'solve_org': obj['solve_org'],
                'reason': obj['reason'],
                'means': obj['means'],
                'finish_time': obj['finish_time']
            }
        }).map(function (res) {
            // let body = res.json();
            // if ( body['errcode'] !== '00000') {
            //     return body['errmsg'];
            // }
            // return body['errcode'];
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    // 故障单分配
    MaintenanceService.prototype.changeTrick = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/workflow", {
            'access_token': token,
            'type': 'trouble_assign',
            'data': {
                'sid': obj['sid'],
                'acceptor': obj['acceptor'],
                'acceptor_pid': obj['acceptor_pid'],
                'acceptor_org': obj['acceptor_org'],
                'acceptor_org_oid': obj['acceptor_org_oid'],
                'status': obj['status']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    MaintenanceService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1__services_storage_service__["a" /* StorageService */]])
    ], MaintenanceService);
    return MaintenanceService;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept-edit/malfunction-accept-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"myContent\" >\r\n    <div class=\"title col-sm-12\">\r\n        <div class=\"col-sm-1\">\r\n            <span>{{formTitle}}</span>\r\n        </div>\r\n        <div class=\"col-sm-11 pull-right\">\r\n            <button type=\"button\"\r\n                    class=\"pull-right\"\r\n                    pButton\r\n                    (click)=\"goBack()\"\r\n                    icon=\"fa-close\"\r\n                    style=\"width: 30px\"></button>\r\n        </div>\r\n    </div>\r\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-2 control-label\">服务单号：</label>\r\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\r\n                <input type=\"text\" pInputText\r\n                       class=\"form-control cursor_not_allowed\"\r\n                       placeholder=\"自动生成\"\r\n                       readonly\r\n                       [value]=\"formObj.sid\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-2 control-label\">来源：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\r\n                <p-dropdown [options]=\"options['来源']\"\r\n                            [(ngModel)]=\"formObj.fromx\"\r\n                            optionLabel=\"name\"\r\n                            [placeholder]=\"defaultOption['fromx']\"\r\n                            [ngModelOptions]=\"{standalone: true}\"\r\n                            [style]=\"{'width':'100%'}\"\r\n                ></p-dropdown>\r\n            </div>\r\n            <label class=\"col-sm-2 control-label\">状态：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\r\n                <input type=\"text\"\r\n                       class=\" form-control cursor_not_allowed\"\r\n                       pInputText\r\n                       placeholder=\"自动生成\"\r\n                       [(ngModel)]=\"formObj.status\"\r\n                       [ngModelOptions]=\"{standalone: true}\"\r\n                       readonly />\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>报障人：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\r\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\r\n                    <input type=\"text\" pInputText\r\n                           placeholder=\"请选择报障人\"\r\n                           readonly\r\n                           formControlName=\"submitter\"\r\n                           [(ngModel)]=\"formObj.submitter\"\r\n                           class=\" form-control cursor_not_allowed\"\r\n                    />\r\n                    <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"Submitter\">\r\n                        <i class=\"fa fa-close\"></i>\r\n                        不能为空\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog('submitter')\" label=\"选择\"></button>\r\n                </div>\r\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"clearTreeDialog('submitter')\" label=\"清空\"></button>\r\n                </div>\r\n            </div>\r\n            <label class=\"col-sm-2 control-label\">所属组织：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\r\n                <input type=\"text\"\r\n                       class=\" form-control cursor_not_allowed\"\r\n                       pInputText\r\n                       placeholder=\"自动生成\"\r\n                       [(ngModel)]=\"formObj.submitter_org\"\r\n                       [ngModelOptions]=\"{standalone: true}\"\r\n                       readonly />\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-2 control-label\">联系电话：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\r\n                <input type=\"text\"\r\n                       class=\" form-control cursor_not_allowed\"\r\n                       pInputText\r\n                       placeholder=\"自动生成\"\r\n                       [(ngModel)]=\"formObj.submitter_phone\"\r\n                       [ngModelOptions]=\"{standalone: true}\"\r\n                       readonly />\r\n            </div>\r\n            <label class=\"col-sm-2 control-label\">受理时间：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\r\n                <input type=\"text\"\r\n                       class=\" form-control cursor_not_allowed\"\r\n                       pInputText\r\n                       placeholder=\"自动生成\"\r\n                       [(ngModel)]=\"formObj.create_time\"\r\n                       [ngModelOptions]=\"{standalone: true}\"\r\n                       readonly />\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>发生时间：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\r\n                <p-calendar [(ngModel)]=\"formObj.occurrence_time\"\r\n                            [showIcon]=\"true\"\r\n                            [locale]=\"zh\"\r\n                            name=\"end_time\"\r\n                            dateFormat=\"yy-mm-dd\"\r\n                            [required]=\"true\"\r\n                            [ngModelOptions]=\"{standalone: true}\"\r\n                            [showSeconds]=\"true\"\r\n                            [maxDate]=\"maxDate\"\r\n                            [showTime]=\"true\">\r\n                </p-calendar>\r\n            </div>\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>最终期限：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\r\n                <input type=\"text\"\r\n                       [ngModelOptions]=\"{standalone: true}\"\r\n                       class=\" form-control cursor_not_allowed\"\r\n                       pInputText\r\n                       placeholder=\"自动生成\"\r\n                       [(ngModel)]=\"formObj.deadline\"\r\n                       readonly />\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>影响度：\r\n            </label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\r\n                <p-dropdown [options]=\"options['影响度']\"\r\n                            [(ngModel)]=\"formObj.influence\"\r\n                            optionLabel=\"name\"\r\n                            (onFocus)=\"onFocus()\"\r\n                            [placeholder]=\"defaultOption['influence']\"\r\n                            [ngModelOptions]=\"{standalone: true}\"\r\n                            [style]=\"{'width':'100%'}\"\r\n                ></p-dropdown>\r\n            </div>\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>紧急度：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\r\n                <p-dropdown [options]=\"options['紧急度']\"\r\n                            [(ngModel)]=\"formObj.urgency\"\r\n                            optionLabel=\"name\"\r\n                            (onFocus)=\"onFocus()\"\r\n                            [placeholder]=\"defaultOption['urgency']\"\r\n                            [ngModelOptions]=\"{standalone: true}\"\r\n                            [style]=\"{'width':'100%'}\"\r\n                ></p-dropdown>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group \">\r\n            <label class=\"col-sm-2 control-label\">优先级：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\r\n                <input type=\"text\"\r\n                       class=\" form-control cursor_not_allowed\"\r\n                       pInputText\r\n                       placeholder=\"自动生成\"\r\n                       [(ngModel)]=\"formObj.priority\"\r\n                       [ngModelOptions]=\"{standalone: true}\"\r\n                       readonly />\r\n            </div>\r\n            <label class=\"col-sm-2 control-label\">故障级别：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\r\n                <p-dropdown [options]=\"options['故障级别']\"\r\n                            [(ngModel)]=\"formObj.level\"\r\n                            optionLabel=\"name\"\r\n                            (onFocus)=\"onFocus()\"\r\n                            [placeholder]=\"defaultOption['level']\"\r\n                            [ngModelOptions]=\"{standalone: true}\"\r\n                            [style]=\"{'width':'100%'}\"\r\n                ></p-dropdown>\r\n            </div>\r\n\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>所属系统：\r\n            </label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\r\n                <p-dropdown [options]=\"options['所属系统']\"\r\n                            [(ngModel)]=\"formObj.bt_system\"\r\n                            optionLabel=\"name\"\r\n                            [placeholder]=\"defaultOption['bt_system']\"\r\n                            [ngModelOptions]=\"{standalone: true}\"\r\n                            [style]=\"{'width':'100%'}\"\r\n                ></p-dropdown>\r\n            </div>\r\n            <label class=\"col-sm-2 control-label\">发生地点：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\r\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\r\n                    <input type=\"text\" pInputText\r\n                           readonly\r\n                           [(ngModel)]=\"formObj.addr\"\r\n                           [ngModelOptions]=\"{standalone: true}\"\r\n                           class=\" form-control cursor_not_allowed\"\r\n                    />\r\n                </div>\r\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" label=\"选择\"></button>\r\n                </div>\r\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" label=\"清空\"></button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n\r\n        <div class=\"form-group \">\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>故障标题：</label>\r\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\r\n                <input type=\"text\" pInputText\r\n                       formControlName=\"title\"\r\n                       [(ngModel)]=\"formObj.title\"/>\r\n                <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"title\">\r\n                    <i class=\"fa fa-close\"></i>\r\n                    不能为空\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        <div class=\"form-group \">\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>故障描述：\r\n            </label>\r\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\r\n                 <textarea [rows]=\"5\" pInputTextarea\r\n                           autoResize=\"autoResize\"\r\n                           formControlName=\"content\"\r\n                           [(ngModel)]=\"formObj.content\"></textarea>\r\n                <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"content\">\r\n                    <i class=\"fa fa-close\"></i>\r\n                    不能为空\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group \">\r\n            <label class=\"col-sm-2 control-label\">\r\n                上传附件：\r\n            </label>\r\n            <div class=\"col-sm-9 ui-fluid-no-padding \">\r\n                <p-fileUpload name=\"file\" url=\"{{ip}}/workflow/servicerequest/upload\"\r\n                              multiple=\"multiple\"\r\n                              accept=\"image/*,application/*,text/*\"\r\n                              chooseLabel=\"选择\"\r\n                              uploadLabel=\"上传\"\r\n                              cancelLabel=\"取消\"\r\n                              maxFileSize=\"3145728\"\r\n                              (onUpload)=\"onUpload($event)\"\r\n                              (onBeforeUpload)=\"onBeforeUpload($event)\"\r\n                              #form>\r\n                    <ng-template pTemplate=\"content\">\r\n                        <ul *ngIf=\"uploadedFiles?.length\">\r\n                            <li *ngFor=\"let file of uploadedFiles\">{{file.name}} - {{file.size}} bytes</li>\r\n                        </ul>\r\n                    </ng-template>\r\n                </p-fileUpload>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group \">\r\n            <label class=\"col-sm-2 control-label\">\r\n                关联服务目录：\r\n            </label>\r\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\r\n                <div class=\"col-sm-10 ui-fluid ui-fluid-no-padding\">\r\n                    <input type=\"text\" pInputText\r\n                           name=\"department\" placeholder=\"请选择关联服务目录\"\r\n                           readonly\r\n                           class=\"form-control cursor_not_allowed\"/>\r\n                </div>\r\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\"  label=\"选择\"></button>\r\n                </div>\r\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\"  label=\"清空\"></button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group \">\r\n            <label  class=\"col-sm-2 control-label\">\r\n                关联设备编号：\r\n            </label>\r\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\r\n                <div class=\"col-sm-10 ui-fluid ui-fluid-no-padding\">\r\n                    <input type=\"text\" pInputText\r\n                           name=\"department\" placeholder=\"请选择关联设备编号\"\r\n                           readonly\r\n                           class=\"form-control cursor_not_allowed\"/>\r\n                </div>\r\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\"  label=\"选择\"></button>\r\n                </div>\r\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\"  label=\"清空\"></button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-2 control-label\">\r\n                <span ngClass=\"start_red\">*</span>分配至部门：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\r\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\r\n                    <input type=\"text\" pInputText\r\n                           readonly\r\n                           [(ngModel)]=\"formObj.acceptor_org\"\r\n                           formControlName=\"acceptor\"\r\n                           class=\" form-control cursor_not_allowed\"\r\n                    />\r\n                    <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"acceptor\">\r\n                        <i class=\"fa fa-close\"></i>\r\n                        不能为空\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog('department')\" label=\"选择\"></button>\r\n                </div>\r\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"clearTreeDialog('department')\" label=\"清空\"></button>\r\n                </div>\r\n            </div>\r\n            <label class=\"col-sm-2 control-label\">分配至个人：</label>\r\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\r\n                <p-dropdown [options]=\"options['departments']\"\r\n                            [(ngModel)]=\"formObj.acceptor\"\r\n                            placeholder=\"{{formObj.acceptor}}\"\r\n                            (onChange)=\"onChange($event)\"\r\n                            [ngModelOptions]=\"{standalone: true}\"\r\n                            [style]=\"{'width':'100%'}\"\r\n                ></p-dropdown>\r\n                <!--{{formObj.acceptor}}-->\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group \">\r\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\r\n                <button class=\"pull-right  ui-button-secondary\" pButton type=\"button\"\r\n                        (click)=\"goBack()\" label=\"取消\"></button>\r\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submitAndDistribute ()\" label=\"保存\"></button>\r\n                <!--<button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submitTrick()\" label=\"提交\"></button>-->\r\n                <!--<button  class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"save()\" label=\"保存\"></button>-->\r\n\r\n            </div>\r\n        </div>\r\n    </form>\r\n    <p-dialog header=\"请选择\" [(visible)]=\"display\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\r\n        <p-tree [value]=\"filesTree4\"\r\n                selectionMode=\"single\"\r\n                [(selection)]=\"selected\"\r\n                (onNodeExpand)=\"nodeExpand($event)\"\r\n        ></p-tree>\r\n        <div style=\"margin-top:8px\">Selected Node: {{selected ? selected.label : 'none'}}</div>\r\n        <p-footer>\r\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\r\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"display=false\"\r\n                    class=\"ui-button-secondary\" label=\"取消\"></button>\r\n        </p-footer>\r\n    </p-dialog>\r\n    <app-personel-dialog *ngIf=\"displayPersonel\"\r\n                         (dataEmitter)=\"dataEmitter($event)\"\r\n                         (displayEmitter)=\"displayEmitter($event)\"></app-personel-dialog>\r\n    <p-growl [(value)]=\"message\"></p-growl>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept-edit/malfunction-accept-edit.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#myContent {\n  font-size: 14px; }\n  #myContent div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #myContent div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #myContent form {\n    background: white;\n    padding: 20px 26px 20px 0px;\n    border: 1px solid #e2e2e2; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept-edit/malfunction-accept-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionAcceptEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MalfunctionAcceptEditComponent = (function () {
    function MalfunctionAcceptEditComponent(maintenanceService, router, activatedRouter, storageService, publicService, fb, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.storageService = storageService;
        this.publicService = publicService;
        this.fb = fb;
        this.eventBusService = eventBusService;
        this.options = {
            '来源': [],
            '影响度': [],
            '紧急度': [],
            '故障级别': [],
            '所属系统': [],
            'departments': [],
        };
        this.defaultOption = {
            'fromx': null,
            'influence': null,
            'urgency': null,
            'level': null,
            'bt_system': null,
            'acceptor': null
        };
        this.display = false;
        this.filesTree4 = [];
        this.selected = [];
        this.message = []; // 交互提示弹出框
        this.formTitle = '创建故障单';
        this.uploadedFiles = [];
    }
    MalfunctionAcceptEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initForm();
        this.formObj = new __WEBPACK_IMPORTED_MODULE_9__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.zh = new __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__["a" /* PUblicMethod */]().initZh();
        this.maxDate = new Date();
        this.getOptions();
        this.formObj.status = '新建';
        this.formTitle = '新建故障单';
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.formObj.create_time = __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(new Date());
        this.formObj.occurrence_time = __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(new Date());
        (!this.formObj.sid) && (this.getDepPerson());
        this.ip = __WEBPACK_IMPORTED_MODULE_6__environments_environment__["a" /* environment */].url.management;
        this.displayPersonel = false;
        if (this.formObj.sid) {
            this.formTitle = '编辑故障单';
            this.getTirckMessage(this.formObj.sid);
            this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
                _this.eventBusService.maintenance.next(res);
            });
            this.formObj.acceptor = '222222';
        }
    };
    MalfunctionAcceptEditComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionAcceptEditComponent.prototype.getOptions = function () {
        var _this = this;
        this.maintenanceService.getParamOptions().subscribe(function (res) {
            _this.options = res;
            _this.getPrority(res['影响度'][0]['name'], res['紧急度'][0]['name']);
            _this.formObj.fromx = res['来源'][0]['name'];
            _this.formObj.level = res['故障级别'][0]['name'];
            _this.formObj.bt_system = res['所属系统'][0]['name'];
            _this.formObj.influence = res['影响度'][0]['name'];
            _this.formObj.urgency = res['紧急度'][0]['name'];
        });
    };
    MalfunctionAcceptEditComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            'submitter': '',
            'title': '',
            'content': '',
            'acceptor': ''
        });
    };
    MalfunctionAcceptEditComponent.prototype.showTreeDialog = function (opType) {
        var _this = this;
        switch (opType) {
            case 'submitter':
                this.displayPersonel = true;
                break;
            case 'department':
                this.display = true;
                this.publicService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
        }
    };
    MalfunctionAcceptEditComponent.prototype.clearTreeDialog = function (opType) {
        switch (opType) {
            case 'submitter':
                this.formObj.submitter = '';
                this.formObj.submitter_pid = '';
                this.formObj.submitter_org_oid = '';
                this.formObj.submitter_org = '';
                this.formObj.submitter_phone = '';
                break;
            case 'department':
                this.formObj.acceptor = '';
                this.formObj.acceptor_org = '';
                this.formObj.acceptor_org_oid = '';
                this.formObj.acceptor_pid = '';
                break;
        }
    };
    MalfunctionAcceptEditComponent.prototype.closeTreeDialog = function () {
        this.display = false;
        this.formObj.acceptor_org = this.selected['label'];
        this.formObj.acceptor_org_oid = this.selected['oid'];
        this.getDepPerson(this.selected['oid']);
    };
    MalfunctionAcceptEditComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    MalfunctionAcceptEditComponent.prototype.dataEmitter = function (event) {
        this.formObj.submitter = event.name;
        this.formObj.submitter_pid = event.pid;
        this.formObj.submitter_org_oid = event.oid;
        this.formObj.submitter_org = event.organization;
        this.formObj.submitter_phone = event.mobile;
    };
    MalfunctionAcceptEditComponent.prototype.displayEmitter = function (event) {
        this.displayPersonel = event;
    };
    MalfunctionAcceptEditComponent.prototype.onFocus = function () {
        if (this.formObj.influence['name']) {
            this.formObj.influence = this.formObj.influence['name'];
        }
        if (this.formObj.urgency['name']) {
            this.formObj.urgency = this.formObj.urgency['name'];
        }
        this.getPrority(this.formObj.influence, this.formObj.urgency);
    };
    MalfunctionAcceptEditComponent.prototype.getPrority = function (influency, urgency) {
        var _this = this;
        if (influency && urgency) {
            this.maintenanceService.getPrority(influency, urgency).subscribe(function (res) {
                if (res) {
                    _this.formObj.priority = res['priority'];
                    _this.getDeadline(influency, urgency, res['priority']);
                }
            });
        }
    };
    MalfunctionAcceptEditComponent.prototype.getDeadline = function (influency, urgency, prority) {
        var _this = this;
        if (influency && urgency && prority) {
            this.maintenanceService.getDeadline(influency, urgency, prority, this.formObj.create_time).subscribe(function (res) {
                if (res) {
                    _this.formObj.deadline = res['time_deadline'];
                }
            });
        }
    };
    MalfunctionAcceptEditComponent.prototype.onBeforeUpload = function (event) {
        var token = this.storageService.getToken('token');
        event.formData.append('access_token', token);
    };
    MalfunctionAcceptEditComponent.prototype.onUpload = function (event) {
        var xhrRespose = JSON.parse(event.xhr.response);
        if (xhrRespose.errcode && xhrRespose.errcode === '00000') {
            this.formObj.attachments = xhrRespose['datas'];
            this.message = [];
            this.message.push({ severity: 'success', summary: '消息提示', detail: '上传成功' });
        }
        else {
            this.message = [];
            this.message.push({ severity: 'error', summary: '消息提示', detail: "\u4E0A\u4F20\u5931\u8D25" + xhrRespose.errmsg });
        }
    };
    MalfunctionAcceptEditComponent.prototype.getDepPerson = function (oid) {
        var _this = this;
        this.publicService.getApprovers(oid).subscribe(function (res) {
            if (!res) {
                _this.options['departments'] = [];
            }
            else {
                var newArray = __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__["a" /* PUblicMethod */].formateDepDropDown(res);
                _this.options['departments'] = newArray;
                _this.formObj.acceptor_pid = newArray[0]['value']['pid'];
                _this.formObj.acceptor = newArray[0]['value']['name'];
            }
        });
    };
    MalfunctionAcceptEditComponent.prototype.onChange = function (event) {
        this.formObj.acceptor = event['value']['name'];
        this.formObj.acceptor_pid = event['value']['pid'];
    };
    MalfunctionAcceptEditComponent.prototype.save = function () {
        var _this = this;
        this.addSelectedName();
        this.filterOptionName();
        this.resetOccurtime();
        this.maintenanceService.saveTicket(this.formObj).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '保存成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u4FDD\u5B58\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionAcceptEditComponent.prototype.resetOccurtime = function () {
        this.formObj.occurrence_time = __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(this.formObj.occurrence_time);
    };
    MalfunctionAcceptEditComponent.prototype.addSelectedName = function () {
        (!this.formObj.fromx) && (this.formObj.fromx = this.defaultOption['fromx']);
        (!this.formObj.level) && (this.formObj.level = this.defaultOption['level']);
        (!this.formObj.bt_system) && (this.formObj.bt_system = this.defaultOption['bt_system']);
        (!this.formObj.influence) && (this.formObj.influence = this.defaultOption['influence']);
        (!this.formObj.urgency) && (this.formObj.urgency = this.defaultOption['urgency']);
        // (!this.formObj.acceptor) && (this.formObj.acceptor = this.defaultOption['acceptor']);
    };
    MalfunctionAcceptEditComponent.prototype.filterOptionName = function () {
        if (this.formObj.fromx['name']) {
            this.formObj.fromx = this.formObj.fromx['name'];
        }
        if (this.formObj.level['name']) {
            this.formObj.level = this.formObj.level['name'];
        }
        if (this.formObj.bt_system['name']) {
            this.formObj.bt_system = this.formObj.bt_system['name'];
        }
        if (this.formObj.influence['name']) {
            this.formObj.influence = this.formObj.influence['name'];
        }
        if (this.formObj.urgency['name']) {
            this.formObj.urgency = this.formObj.urgency['name'];
        }
        // if (this.formObj.acceptor['name']) {
        //     this.formObj.acceptor = this.formObj.acceptor['name'];
        // }
    };
    Object.defineProperty(MalfunctionAcceptEditComponent.prototype, "Submitter", {
        get: function () {
            return this.myForm.controls['submitter'].untouched && this.myForm.controls['submitter'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionAcceptEditComponent.prototype, "title", {
        get: function () {
            return this.myForm.controls['title'].untouched && this.myForm.controls['title'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionAcceptEditComponent.prototype, "content", {
        get: function () {
            // console.log('untouched', this.myForm.controls['content'].untouched);
            // console.log('required', this.myForm.controls['content'].hasError('required'));
            return this.myForm.controls['content'].untouched && this.myForm.controls['content'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionAcceptEditComponent.prototype, "acceptor", {
        get: function () {
            // console.log('untouched', this.myForm.controls['content'].untouched);
            // console.log('required', this.myForm.controls['content'].hasError('required'));
            return this.myForm.controls['acceptor'].untouched && this.myForm.controls['acceptor'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    MalfunctionAcceptEditComponent.prototype.setValidators = function () {
        this.myForm.controls['submitter'].setValidators([__WEBPACK_IMPORTED_MODULE_8__angular_forms__["Validators"].required]);
        this.myForm.controls['title'].setValidators([__WEBPACK_IMPORTED_MODULE_8__angular_forms__["Validators"].required]);
        this.myForm.controls['content'].setValidators([__WEBPACK_IMPORTED_MODULE_8__angular_forms__["Validators"].required]);
        this.myForm.controls['submitter'].updateValueAndValidity();
        this.myForm.controls['title'].updateValueAndValidity();
        this.myForm.controls['content'].updateValueAndValidity();
    };
    MalfunctionAcceptEditComponent.prototype.submitTrick = function () {
        this.addSelectedName();
        this.filterOptionName();
        if (!this.formObj.submitter
            || !this.formObj.title
            || !this.formObj.content) {
            this.showError();
            this.setValidators();
        }
        else {
            this.formObj.status = '待分配';
            this.submitPost(this.formObj);
        }
    };
    MalfunctionAcceptEditComponent.prototype.showError = function () {
        this.message = [];
        this.message.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    MalfunctionAcceptEditComponent.prototype.submitPost = function (paper) {
        var _this = this;
        this.resetOccurtime();
        this.maintenanceService.submitTicket(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionAcceptEditComponent.prototype.submitAndDistribute = function () {
        this.addSelectedName();
        this.filterOptionName();
        if (!this.formObj.submitter
            || !this.formObj.title
            || !this.formObj.content
            || !this.formObj.acceptor_org) {
            this.showError();
            this.setValidators();
            this.myForm.controls['acceptor'].setValidators([__WEBPACK_IMPORTED_MODULE_8__angular_forms__["Validators"].required]);
            this.myForm.controls['acceptor'].updateValueAndValidity();
        }
        else {
            this.formObj.status = '待接受';
            this.submitPost(this.formObj);
        }
    };
    MalfunctionAcceptEditComponent.prototype.getTirckMessage = function (sid) {
        var _this = this;
        this.maintenanceService.getTrick(sid).subscribe(function (res) {
            _this.formObj.sid = res['sid'];
            _this.formObj.fromx = res['fromx'];
            _this.formObj.status = res['status'];
            _this.formObj.submitter = res['submitter'];
            _this.formObj.submitter_pid = res['submitter_pid'];
            _this.formObj.submitter_org_oid = res['submitter_org_oid'];
            _this.formObj.submitter_org = res['submitter_org'];
            _this.formObj.submitter_phone = res['submitter_phone'];
            _this.formObj.occurrence_time = res['occurrence_time'];
            _this.formObj.deadline = res['deadline'];
            _this.formObj.influence = res['influence'];
            _this.formObj.urgency = res['urgency'];
            _this.formObj.priority = res['priority'];
            _this.formObj.level = res['level'];
            _this.formObj.bt_system = res['bt_system'];
            _this.formObj.addr = res['addr'];
            _this.formObj.title = res['title'];
            _this.formObj.content = res['content'];
            _this.formObj.service = res['service'];
            _this.formObj.devices = res['devices'];
            _this.formObj.acceptor_org = res['acceptor_org'];
            _this.formObj.acceptor_org_oid = res['acceptor_org_oid'];
            _this.formObj.acceptor = res['acceptor'];
            _this.formObj.acceptor_pid = res['acceptor_pid'];
            _this.formObj.create_time = res['create_time'];
            _this.defaultOption['fromx'] = res['fromx'];
            _this.defaultOption['influence'] = res['influence'];
            _this.defaultOption['urgency'] = res['urgency'];
            _this.defaultOption['level'] = res['level'];
            _this.defaultOption['bt_system'] = res['bt_system'];
            // this.defaultOption['acceptor'] = res['acceptor'];
            // this.hasAccepterOrg(res['acceptor_org_oid']);
            _this.clearDefaultOption();
        });
    };
    MalfunctionAcceptEditComponent.prototype.hasAccepterOrg = function (oid) {
        if (oid) {
            this.getDepPerson(oid);
        }
    };
    MalfunctionAcceptEditComponent.prototype.clearDefaultOption = function () {
        // this.formObj.acceptor && (this.formObj.acceptor = null);
        this.formObj.urgency && (this.formObj.urgency = null);
        this.formObj.fromx && (this.formObj.fromx = null);
        this.formObj.level && (this.formObj.level = null);
        this.formObj.bt_system && (this.formObj.bt_system = null);
        this.formObj.influence && (this.formObj.influence = null);
    };
    MalfunctionAcceptEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-accept-edit',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept-edit/malfunction-accept-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept-edit/malfunction-accept-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_4__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_8__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_7__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionAcceptEditComponent);
    return MalfunctionAcceptEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept-view/malfunction-accept-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionAccept\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>查看故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-simple-view></app-simple-view>\n        <app-simple-time></app-simple-time>\n    </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept-view/malfunction-accept-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionAccept {\n  font-size: 14px; }\n  #MalfunctionAccept div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionAccept div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionAccept form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-1 {\n    width: 10.5%; }\n  .col-sm-3 {\n    width: 22%; }\n  .col-sm-11 {\n    width: 88.666667%; }\n  .col-sm-10 {\n    width: 78.333333%; }\n  .col-sm-5 {\n    width: 38.666667%; }\n  .col-sm-6 {\n    width: 56%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept-view/malfunction-accept-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionAcceptViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MalfunctionAcceptViewComponent = (function () {
    function MalfunctionAcceptViewComponent(activatedRouter, router) {
        this.activatedRouter = activatedRouter;
        this.router = router;
    }
    MalfunctionAcceptViewComponent.prototype.ngOnInit = function () {
    };
    MalfunctionAcceptViewComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionAcceptViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-accept-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept-view/malfunction-accept-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept-view/malfunction-accept-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]])
    ], MalfunctionAcceptViewComponent);
    return MalfunctionAcceptViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept/malfunction-accept.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionAccept\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>接受故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-simple-view></app-simple-view>\n        <app-simple-time></app-simple-time>\n        <div class=\"form-group\">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button pButton type=\"button\"\n                        (click)=\"reject()\"\n                        label=\"拒绝\"\n                        [disabled]=\"saved\"\n                        class=\"pull-right ui-margin-right-10px\"></button>\n                <button pButton type=\"button\"  label=\"接受\" [disabled]=\"hadSaved\"\n                        (click)=\"accept()\"\n                        class=\"pull-right ui-margin-right-10px\"></button>\n            </div>\n        </div>\n    </form>\n    <p-growl [(value)]=\"message\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept/malfunction-accept.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionAccept {\n  font-size: 14px; }\n  #MalfunctionAccept div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionAccept div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionAccept form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-1 {\n    width: 10.5%; }\n  .col-sm-3 {\n    width: 22%; }\n  .col-sm-11 {\n    width: 88.666667%; }\n  .col-sm-10 {\n    width: 78.333333%; }\n  .col-sm-5 {\n    width: 38.666667%; }\n  .col-sm-6 {\n    width: 56%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-accept/malfunction-accept.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionAcceptComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MalfunctionAcceptComponent = (function () {
    function MalfunctionAcceptComponent(maintenanceService, router, activatedRouter, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.eventBusService = eventBusService;
        this.message = []; // 交互提示弹出框
    }
    MalfunctionAcceptComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formObj = new __WEBPACK_IMPORTED_MODULE_4__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionAcceptComponent.prototype.accept = function () {
        var _this = this;
        this.formObj.status = '待处理';
        this.maintenanceService.recieveOrRejectTrick(this.formObj).subscribe(function (res) {
            if (res === '00000') {
                _this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionProcess', { id: _this.formObj.sid, status: _this.formObj.status }]);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u6545\u969C\u5355\u63A5\u53D7\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionAcceptComponent.prototype.reject = function () {
        this.formObj.status = '待分配';
        console.dirxml(this.formObj);
        this.submitPost(this.formObj);
    };
    MalfunctionAcceptComponent.prototype.submitPost = function (paper) {
        var _this = this;
        this.maintenanceService.recieveOrRejectTrick(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionAcceptComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionAcceptComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-accept',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept/malfunction-accept.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-accept/malfunction-accept.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionAcceptComponent);
    return MalfunctionAcceptComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-add/malfunction-add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"myContent\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>{{formTitle}}</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">服务单号：</label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"自动生成\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"formObj.sid\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">来源：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"dropdownOption.fromx\"\n                            [(ngModel)]=\"formObj.fromx\"\n                            [placeholder]=\"formObj.fromx\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n            </div>\n            <label class=\"col-sm-2 control-label\">状态：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [(ngModel)]=\"formObj.status\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>报障人：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           placeholder=\"请选择报障人\"\n                           readonly\n                           formControlName=\"submitter\"\n                           [(ngModel)]=\"formObj.submitter\"\n                           class=\" form-control cursor_not_allowed\"\n                    />\n                    <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"Submitter\">\n                        <i class=\"fa fa-close\"></i>\n                        不能为空\n                    </div>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog('submitter')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"clearTreeDialog('submitter')\" label=\"清空\"></button>\n                </div>\n            </div>\n            <label class=\"col-sm-2 control-label\">所属组织：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [(ngModel)]=\"formObj.submitter_org\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">联系电话：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [(ngModel)]=\"formObj.submitter_phone\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       readonly />\n            </div>\n            <label class=\"col-sm-2 control-label\">受理时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [(ngModel)]=\"formObj.create_time\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>发生时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-calendar [(ngModel)]=\"formObj.occurrence_time\"\n                            [showIcon]=\"true\"\n                            [locale]=\"zh\"\n                            name=\"end_time\"\n                            dateFormat=\"yy-mm-dd\"\n                            [required]=\"true\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [showSeconds]=\"true\"\n                            [maxDate]=\"maxDate\"\n                            [showTime]=\"true\">\n                </p-calendar>\n            </div>\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>最终期限：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [(ngModel)]=\"formObj.deadline\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>影响度：\n            </label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"dropdownOption.influence\"\n                            [(ngModel)]=\"formObj.influence\"\n                            (onFocus)=\"onFocus()\"\n                            [placeholder]=\"formObj.influence\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n            </div>\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>紧急度：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <p-dropdown [options]=\"dropdownOption.urgency\"\n                            [(ngModel)]=\"formObj.urgency\"\n                            (onFocus)=\"onFocus()\"\n                            [placeholder]=\"formObj.urgency\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">优先级：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [(ngModel)]=\"formObj.priority\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       readonly />\n            </div>\n            <label class=\"col-sm-2 control-label\">故障级别：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"dropdownOption.level\"\n                            [(ngModel)]=\"formObj.level\"\n                            (onFocus)=\"onFocus()\"\n                            [placeholder]=\"formObj.level\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n            </div>\n\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>所属系统：\n            </label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"dropdownOption.bt_system\"\n                            [(ngModel)]=\"formObj.bt_system\"\n                            [placeholder]=\"formObj.bt_system\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n            </div>\n            <label class=\"col-sm-2 control-label\">发生地点：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           readonly\n                           [(ngModel)]=\"formObj.addr\"\n                           [ngModelOptions]=\"{standalone: true}\"\n                           class=\" form-control cursor_not_allowed\"\n                    />\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" label=\"清空\"></button>\n                </div>\n            </div>\n        </div>\n\n\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>故障标题：</label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n                <input type=\"text\" pInputText\n                       formControlName=\"title\"\n                       [(ngModel)]=\"formObj.title\"/>\n                <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"title\">\n                    <i class=\"fa fa-close\"></i>\n                    不能为空\n                </div>\n            </div>\n\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>故障描述：\n            </label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n                 <textarea [rows]=\"5\" pInputTextarea\n                           autoResize=\"autoResize\"\n                           formControlName=\"content\"\n                           [(ngModel)]=\"formObj.content\"></textarea>\n                <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"content\">\n                    <i class=\"fa fa-close\"></i>\n                    不能为空\n                </div>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">\n                上传附件：\n            </label>\n            <div class=\"col-sm-9 ui-fluid-no-padding \">\n                <p-fileUpload name=\"file\" url=\"{{ip}}/workflow/servicerequest/upload\"\n                              multiple=\"multiple\"\n                              accept=\"image/*,application/*,text/*\"\n                              chooseLabel=\"选择\"\n                              uploadLabel=\"上传\"\n                              cancelLabel=\"取消\"\n                              maxFileSize=\"3145728\"\n                              (onUpload)=\"onUpload($event)\"\n                              (onBeforeUpload)=\"onBeforeUpload($event)\"\n                              #form>\n                    <ng-template pTemplate=\"content\">\n                        <ul *ngIf=\"uploadedFiles?.length\">\n                            <li *ngFor=\"let file of uploadedFiles\">{{file.name}} - {{file.size}} bytes</li>\n                        </ul>\n                    </ng-template>\n                </p-fileUpload>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">\n                关联服务目录：\n            </label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <div class=\"col-sm-10 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           name=\"department\" placeholder=\"请选择关联服务目录\"\n                           readonly\n                           class=\"form-control cursor_not_allowed\"/>\n                </div>\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\"  label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\"  label=\"清空\"></button>\n                </div>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label  class=\"col-sm-2 control-label\">\n                关联设备编号：\n            </label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <div class=\"col-sm-10 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           name=\"department\" placeholder=\"请选择关联设备编号\"\n                           readonly\n                           class=\"form-control cursor_not_allowed\"/>\n                </div>\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\"  label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\"  label=\"清空\"></button>\n                </div>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>分配至部门：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           readonly\n                           [(ngModel)]=\"formObj.acceptor_org\"\n                           formControlName=\"acceptor\"\n                           class=\" form-control cursor_not_allowed\"\n                    />\n                    <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"acceptor\">\n                        <i class=\"fa fa-close\"></i>\n                        不能为空\n                    </div>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog('department')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"clearTreeDialog('department')\" label=\"清空\"></button>\n                </div>\n            </div>\n            <label class=\"col-sm-2 control-label\">分配至个人：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"dropdownOption.acceptor\"\n                            [(ngModel)]=\"formObj.acceptor\"\n                            placeholder=\"{{formObj.acceptor}}\"\n                            (onChange)=\"onChange($event)\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n                <!--{{formObj.acceptor}}-->\n                <!--{{formObj.acceptor_pid}}-->\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button class=\"pull-right  ui-button-secondary\" pButton type=\"button\"\n                        (click)=\"goBack()\" label=\"取消\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submitAndDistribute ()\" label=\"提交并分配\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submitTrick()\" label=\"提交\"></button>\n                <button  class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"save()\" label=\"保存\"></button>\n\n            </div>\n        </div>\n    </form>\n    <p-dialog header=\"请选择\" [(visible)]=\"display\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <p-tree [value]=\"filesTree4\"\n                selectionMode=\"single\"\n                [(selection)]=\"selected\"\n                (onNodeExpand)=\"nodeExpand($event)\"\n        ></p-tree>\n        <!--<div style=\"margin-top:8px\">Selected Node: {{selected ? selected.label : 'none'}}</div>-->\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"display=false\"\n                    class=\"ui-button-secondary\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n  <app-personel-dialog *ngIf=\"displayPersonel\"\n                       (dataEmitter)=\"dataEmitter($event)\"\n                       (displayEmitter)=\"displayEmitter($event)\">\n\n  </app-personel-dialog>\n  <p-growl [(value)]=\"message\"></p-growl>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-add/malfunction-add.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#myContent {\n  font-size: 14px; }\n  #myContent div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #myContent div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #myContent form {\n    background: white;\n    padding: 20px 26px 20px 0px;\n    border: 1px solid #e2e2e2; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-add/malfunction-add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionAddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__formobj_model__ = __webpack_require__("../../../../../src/app/maintenance/formobj.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MalfunctionAddComponent = (function () {
    function MalfunctionAddComponent(maintenanceService, router, activatedRouter, storageService, publicService, fb, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.storageService = storageService;
        this.publicService = publicService;
        this.fb = fb;
        this.eventBusService = eventBusService;
        this.display = false;
        this.filesTree4 = [];
        this.selected = [];
        this.message = []; // 交互提示弹出框
        this.formTitle = '创建故障单';
        this.uploadedFiles = [];
        this.dropdownOption = {
            fromx: [],
            level: [],
            bt_system: [],
            influence: [],
            urgency: [],
            acceptor: []
        };
    }
    MalfunctionAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initForm();
        this.formObj = new __WEBPACK_IMPORTED_MODULE_9__formobj_model__["a" /* FormobjModel */]();
        this.zh = new __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */]().initZh();
        this.maxDate = new Date();
        this.getDepPerson();
        this.getOptions();
        this.formObj.status = '新建';
        this.formTitle = '新建故障单';
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.formObj.create_time = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(new Date());
        this.formObj.occurrence_time = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(new Date());
        (!this.formObj.sid) && (this.getDepPerson());
        this.ip = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].url.management;
        this.displayPersonel = false;
        if (this.formObj.sid) {
            this.formTitle = '编辑故障单';
            this.getTirckMessage(this.formObj.sid);
            this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
                _this.eventBusService.maintenance.next(res);
            });
        }
    };
    MalfunctionAddComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionAddComponent.prototype.getOptions = function () {
        var _this = this;
        this.maintenanceService.getParamOptions().subscribe(function (res) {
            _this.getPrority(res['影响度'][0]['name'], res['紧急度'][0]['name']);
            _this.dropdownOption['fromx'] = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateDropDown(res['来源']);
            _this.dropdownOption['level'] = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateDropDown(res['故障级别']);
            _this.dropdownOption['bt_system'] = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateDropDown(res['所属系统']);
            _this.dropdownOption['influence'] = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateDropDown(res['影响度']);
            _this.dropdownOption['urgency'] = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateDropDown(res['紧急度']);
            _this.formObj.fromx = res['来源'][0]['name'];
            _this.formObj.level = res['故障级别'][0]['name'];
            _this.formObj.bt_system = res['所属系统'][0]['name'];
            _this.formObj.influence = res['影响度'][0]['name'];
            _this.formObj.urgency = res['紧急度'][0]['name'];
        });
    };
    MalfunctionAddComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            'submitter': '',
            'title': '',
            'content': '',
            'acceptor': ''
        });
    };
    MalfunctionAddComponent.prototype.showTreeDialog = function (opType) {
        var _this = this;
        switch (opType) {
            case 'submitter':
                this.displayPersonel = true;
                break;
            case 'department':
                this.display = true;
                this.publicService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
        }
    };
    MalfunctionAddComponent.prototype.clearTreeDialog = function (opType) {
        switch (opType) {
            case 'submitter':
                this.formObj.submitter = '';
                this.formObj.submitter_pid = '';
                this.formObj.submitter_org_oid = '';
                this.formObj.submitter_org = '';
                this.formObj.submitter_phone = '';
                break;
            case 'department':
                this.formObj.acceptor = '';
                this.formObj.acceptor_org = '';
                this.formObj.acceptor_org_oid = '';
                this.formObj.acceptor_pid = '';
                break;
        }
    };
    MalfunctionAddComponent.prototype.closeTreeDialog = function () {
        this.display = false;
        this.formObj.acceptor_org = this.selected['label'];
        this.formObj.acceptor_org_oid = this.selected['oid'];
        this.getDepPerson(this.selected['oid']);
    };
    MalfunctionAddComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    MalfunctionAddComponent.prototype.dataEmitter = function (event) {
        this.formObj.submitter = event.name;
        this.formObj.submitter_pid = event.pid;
        this.formObj.submitter_org_oid = event.oid;
        this.formObj.submitter_org = event.organization;
        this.formObj.submitter_phone = event.mobile;
    };
    MalfunctionAddComponent.prototype.displayEmitter = function (event) {
        this.displayPersonel = event;
    };
    MalfunctionAddComponent.prototype.onFocus = function () {
        if (this.formObj.influence['name']) {
            this.formObj.influence = this.formObj.influence['name'];
        }
        if (this.formObj.urgency['name']) {
            this.formObj.urgency = this.formObj.urgency['name'];
        }
        this.getPrority(this.formObj.influence, this.formObj.urgency);
    };
    MalfunctionAddComponent.prototype.getPrority = function (influency, urgency) {
        var _this = this;
        if (influency && urgency) {
            this.maintenanceService.getPrority(influency, urgency).subscribe(function (res) {
                if (res) {
                    _this.formObj.priority = res['priority'];
                    _this.getDeadline(influency, urgency, res['priority']);
                }
            });
        }
    };
    MalfunctionAddComponent.prototype.getDeadline = function (influency, urgency, prority) {
        var _this = this;
        if (influency && urgency && prority) {
            this.maintenanceService.getDeadline(influency, urgency, prority, this.formObj.create_time).subscribe(function (res) {
                if (res) {
                    _this.formObj.deadline = res['time_deadline'];
                }
            });
        }
    };
    MalfunctionAddComponent.prototype.onBeforeUpload = function (event) {
        var token = this.storageService.getToken('token');
        event.formData.append('access_token', token);
    };
    MalfunctionAddComponent.prototype.onUpload = function (event) {
        var xhrRespose = JSON.parse(event.xhr.response);
        if (xhrRespose.errcode && xhrRespose.errcode === '00000') {
            this.formObj.attachments = xhrRespose['datas'];
            this.message = [];
            this.message.push({ severity: 'success', summary: '消息提示', detail: '上传成功' });
        }
        else {
            this.message = [];
            this.message.push({ severity: 'error', summary: '消息提示', detail: "\u4E0A\u4F20\u5931\u8D25" + xhrRespose.errmsg });
        }
    };
    MalfunctionAddComponent.prototype.getDepPerson = function (oid) {
        var _this = this;
        this.publicService.getApprovers(oid).subscribe(function (res) {
            if (!res) {
                _this.dropdownOption.acceptor = [];
            }
            else {
                var newArray = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateDepDropDown(res);
                _this.dropdownOption.acceptor = newArray;
                _this.formObj.acceptor_pid = newArray[0]['value']['pid'];
                _this.formObj.acceptor = newArray[0]['value']['name'];
            }
        });
    };
    MalfunctionAddComponent.prototype.onChange = function (event) {
        this.formObj.acceptor = event['value']['name'];
        this.formObj.acceptor_pid = event['value']['pid'];
    };
    MalfunctionAddComponent.prototype.save = function () {
        var _this = this;
        this.resetOccurtime();
        this.maintenanceService.saveTicket(this.formObj).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '保存成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u4FDD\u5B58\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionAddComponent.prototype.resetOccurtime = function () {
        this.formObj.occurrence_time = __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(this.formObj.occurrence_time);
    };
    Object.defineProperty(MalfunctionAddComponent.prototype, "Submitter", {
        get: function () {
            return this.myForm.controls['submitter'].untouched && this.myForm.controls['submitter'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionAddComponent.prototype, "title", {
        get: function () {
            return this.myForm.controls['title'].untouched && this.myForm.controls['title'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionAddComponent.prototype, "content", {
        get: function () {
            return this.myForm.controls['content'].untouched && this.myForm.controls['content'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionAddComponent.prototype, "acceptor", {
        get: function () {
            return this.myForm.controls['acceptor'].untouched && this.myForm.controls['acceptor'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    MalfunctionAddComponent.prototype.setValidators = function () {
        this.myForm.controls['submitter'].setValidators([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]);
        this.myForm.controls['title'].setValidators([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]);
        this.myForm.controls['content'].setValidators([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]);
        this.myForm.controls['submitter'].updateValueAndValidity();
        this.myForm.controls['title'].updateValueAndValidity();
        this.myForm.controls['content'].updateValueAndValidity();
    };
    MalfunctionAddComponent.prototype.submitTrick = function () {
        if (!this.formObj.submitter
            || !this.formObj.title
            || !this.formObj.content) {
            this.showError();
            this.setValidators();
        }
        else {
            this.formObj.status = '待分配';
            this.submitPost(this.formObj);
        }
    };
    MalfunctionAddComponent.prototype.showError = function () {
        this.message = [];
        this.message.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    MalfunctionAddComponent.prototype.submitPost = function (paper) {
        var _this = this;
        this.resetOccurtime();
        this.maintenanceService.submitTicket(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionAddComponent.prototype.submitAndDistribute = function () {
        if (!this.formObj.submitter
            || !this.formObj.title
            || !this.formObj.content
            || !this.formObj.acceptor_org) {
            this.showError();
            this.setValidators();
            this.myForm.controls['acceptor'].setValidators([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]);
            this.myForm.controls['acceptor'].updateValueAndValidity();
        }
        else {
            this.formObj.status = '待接受';
            this.submitPost(this.formObj);
        }
    };
    MalfunctionAddComponent.prototype.getTirckMessage = function (sid) {
        var _this = this;
        this.maintenanceService.getTrick(sid).subscribe(function (res) {
            // this.formObj = new FormobjModel(res);
            _this.formObj.sid = res['sid'];
            _this.formObj.fromx = res['fromx'];
            _this.formObj.status = res['status'];
            _this.formObj.submitter = res['submitter'];
            _this.formObj.submitter_pid = res['submitter_pid'];
            _this.formObj.submitter_org_oid = res['submitter_org_oid'];
            _this.formObj.submitter_org = res['submitter_org'];
            _this.formObj.submitter_phone = res['submitter_phone'];
            _this.formObj.occurrence_time = res['occurrence_time'];
            _this.formObj.deadline = res['deadline'];
            _this.formObj.influence = res['influence'];
            _this.formObj.urgency = res['urgency'];
            _this.formObj.priority = res['priority'];
            _this.formObj.level = res['level'];
            _this.formObj.bt_system = res['bt_system'];
            _this.formObj.addr = res['addr'];
            _this.formObj.title = res['title'];
            _this.formObj.content = res['content'];
            _this.formObj.service = res['service'];
            _this.formObj.devices = res['devices'];
            _this.formObj.acceptor_org = res['acceptor_org'];
            _this.formObj.acceptor_org_oid = res['acceptor_org_oid'];
            _this.formObj.acceptor = res['acceptor'];
            _this.formObj.acceptor_pid = res['acceptor_pid'];
            _this.formObj.create_time = res['create_time'];
        });
    };
    MalfunctionAddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-add',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-add/malfunction-add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-add/malfunction-add.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_8__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_8__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_7__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionAddComponent);
    return MalfunctionAddComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-base/malfunction-base.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"feature-title\">故障管理</span>\n    </div>\n</div>\n<div class=\"content-section GridDemo\" id=\"MalfunctionBase\" >\n    <div class=\"col-sm-12 ui-fluid-no-padding\" >\n        <div class=\"col-sm-1\" *ngIf=\"flowChartDatas && flowChartDatas.length\">\n            <svg  xmlns=\"http://www.w3.org/2000/svg\" height=\"61em\" viewbox=\"0 0 300 1200\"  preserveAspectRatio=\"xMinYMin meet\" >\n                <g transform=\"translate(-40,0)\">\n                    <g transform=\"translate(0,0)\"  [routerLink]=\"['./malfunctionAdd']\" class=\"pointer_curser\">\n                        <rect x=\"50\" y=\"10\" width=\"100\" height=\"50\" rx=\"10\" ry=\"10\" stroke-width=\"0.5\" stroke=\"black\"  [attr.fill]=\"flowChartDatas[0].fill\"></rect>\n                        <text x=\"100\" y=\"40\" font-size=\"14\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[0].name}}</text>\n                        <!--<rect x=\"120\" y=\"10\" width=\"30\" height=\"15\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" fill=\"red\"></rect>-->\n                        <!--<text x=\"135\" y=\"23\" font-size=\"12\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[0].record}}</text>-->\n                    </g>\n                    <polygon points=\"0 0, 1 0, 1 40, 5 40, 0.5 50, -4 40, 0 40\" transform=\"translate(95,60)\" fill=\"#666\"></polygon>\n                    <g transform=\"translate(0,100)\" [routerLink]=\"['./malfunctionDistribute']\" class=\"pointer_curser\">\n                        <rect x=\"50\" y=\"10\" width=\"100\" height=\"50\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" [attr.fill]=\"flowChartDatas[1].fill\"></rect>\n                        <text x=\"100\" y=\"40\" font-size=\"14\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[1].name}}</text>\n                        <rect x=\"120\" y=\"10\" width=\"30\" height=\"15\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" fill=\"#E4E4E4\"></rect>\n                        <text x=\"135\" y=\"23\" font-size=\"12\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[1].record}}</text>\n                    </g>\n                    <polygon points=\"0 0, 1 0, 1 40, 5 40, 0.5 50, -4 40, 0 40\" transform=\"translate(95,160)\" fill=\"#666\"></polygon>\n                    <g transform=\"translate(0,200)\" [routerLink]=\"['./malfunctionReceive']\" class=\"pointer_curser\">\n                        <rect x=\"50\" y=\"10\" width=\"100\" height=\"50\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" [attr.fill]=\"flowChartDatas[2].fill\"></rect>\n                        <text x=\"100\" y=\"40\" font-size=\"14\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[2].name}}</text>\n                        <rect x=\"120\" y=\"10\" width=\"30\" height=\"15\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" fill=\"#E4E4E4\"></rect>\n                        <text x=\"135\" y=\"23\" font-size=\"12\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[2].record}}</text>\n                    </g>\n                    <polygon points=\"0 0, 1 0, 1 40, 5 40, 0.5 50, -4 40, 0 40\" transform=\"translate(95,260)\" fill=\"#666\"></polygon>\n                    <g transform=\"translate(0,300)\" [routerLink]=\"['./malfuntionDeal']\" class=\"pointer_curser\">\n                        <rect x=\"50\" y=\"10\" width=\"100\" height=\"50\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" [attr.fill]=\"flowChartDatas[3].fill\"></rect>\n                        <text x=\"100\" y=\"40\" font-size=\"14\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[3].name}}</text>\n                        <rect x=\"120\" y=\"10\" width=\"30\" height=\"15\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" fill=\"#E4E4E4\"></rect>\n                        <text x=\"135\" y=\"23\" font-size=\"12\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[3].record}}</text>\n                    </g>\n                    <polygon points=\"0 0, 1 0, 1 40, 5 40, 0.5 50, -4 40, 0 40\" transform=\"translate(95,360)\" fill=\"#666\"></polygon>\n                    <g transform=\"translate(0,400)\">\n                        <rect x=\"50\" y=\"10\" width=\"100\" height=\"50\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" stroke-dasharray=\"5,5\" [attr.fill]=\"flowChartDatas[4].fill\"></rect>\n                        <text x=\"100\" y=\"40\" font-size=\"14\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[4].name}}</text>\n                    </g>\n                    <polygon points=\"0 0, 1 0, 1 40, 5 40, 0.5 50, -4 40, 0 40\" transform=\"translate(95,460)\" fill=\"#666\"></polygon>\n                    <g transform=\"translate(0,500)\" [routerLink]=\"['./malfunctionSolve']\" class=\"pointer_curser\">\n                        <rect x=\"50\" y=\"10\" width=\"100\" height=\"50\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" [attr.fill]=\"flowChartDatas[5].fill\"></rect>\n                        <text x=\"100\" y=\"40\" font-size=\"14\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[5].name}}</text>\n                        <rect x=\"120\" y=\"10\" width=\"30\" height=\"15\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" fill=\"#E4E4E4\"></rect>\n                        <text x=\"135\" y=\"23\" font-size=\"12\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[5].record}}</text>\n                    </g>\n                    <polygon points=\"0 0, 1 0, 1 40, 5 40, 0.5 50, -4 40, 0 40\" transform=\"translate(95,560)\" fill=\"#666\"></polygon>\n                    <g transform=\"translate(0,600)\" [routerLink]=\"['./malfunctionOff']\" class=\"pointer_curser\">\n                        <rect x=\"50\" y=\"10\" width=\"100\" height=\"50\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" [attr.fill]=\"flowChartDatas[6].fill\"></rect>\n                        <text x=\"100\" y=\"40\" font-size=\"14\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[6].name}}</text>\n                        <rect x=\"120\" y=\"10\" width=\"30\" height=\"15\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" fill=\"#E4E4E4\"></rect>\n                        <text x=\"135\" y=\"23\" font-size=\"12\" fill=\"black\" text-anchor=\"middle\">{{flowChartDatas[6].record}}</text>\n                    </g>\n                    <polygon points=\"0 0, 1 0, 1 40, 5 40, 0.5 50, -4 40, 0 40\" transform=\"translate(95,660)\" fill=\"#666\"></polygon>\n                    <g transform=\"translate(0,700)\">\n                        <rect x=\"50\" y=\"10\" width=\"100\" height=\"50\" rx=\"10\" ry=\"10\" stroke-width=\"0.5\" stroke=\"black\" fill=\"none\"></rect>\n                        <text x=\"100\" y=\"40\" font-size=\"14\" fill=\"black\" text-anchor=\"middle\">关闭</text>\n                        <!--<rect x=\"120\" y=\"10\" width=\"30\" height=\"15\" rx=\"0\" ry=\"0\" stroke-width=\"0.5\" stroke=\"black\" fill=\"red\"></rect>-->\n                        <!--<text x=\"135\" y=\"23\" font-size=\"12\" fill=\"black\" text-anchor=\"middle\">关闭</text>-->\n                    </g>\n                </g>\n            </svg>\n        </div>\n        <div class=\"col-sm-11\" >\n            <router-outlet></router-outlet>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-base/malfunction-base.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".pointer_curser {\n  cursor: pointer; }\n\n.col-sm-11 {\n  width: 88.666667%;\n  margin-left: 1.8vw; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-base/malfunction-base.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionBaseComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionBaseComponent = (function () {
    function MalfunctionBaseComponent(eventBusService, maintenanceService) {
        this.eventBusService = eventBusService;
        this.maintenanceService = maintenanceService;
        this.flowChartDatas = []; // 流程图节点数据
    }
    MalfunctionBaseComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.maintenanceService.getFlowChart().subscribe(function (res) {
            _this.flowChartDatas = res;
        });
        this.eventBusService.maintenance.subscribe(function (res) {
            _this.flowChartDatas = res;
        });
    };
    MalfunctionBaseComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-base',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-base/malfunction-base.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-base/malfunction-base.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */]])
    ], MalfunctionBaseComponent);
    return MalfunctionBaseComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-change/malfunction-change.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionChange\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>分配故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n       <app-base-view></app-base-view>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>分配至部门：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           readonly\n                           [(ngModel)]=\"formObj.acceptor_org\"\n                           formControlName=\"acceptor\"\n                           class=\" form-control cursor_not_allowed\"\n                    />\n                    <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"acceptor\">\n                        <i class=\"fa fa-close\"></i>\n                        不能为空\n                    </div>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog('department')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"clearTreeDialog('department')\" label=\"清空\"></button>\n                </div>\n            </div>\n            <label class=\"col-sm-2 control-label\">分配至个人：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"options['departments']\"\n                            [(ngModel)]=\"formObj.acceptor\"\n                            placeholder=\"{{formObj.acceptor}}\"\n                            (onChange)=\"onChange($event)\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button class=\"pull-right  ui-button-secondary ui-margin-right-10px\" pButton type=\"button\" (click)=\"goBack()\" label=\"取消\" [disabled]=\"saved\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submitTrick()\" label=\"提交\" [disabled]=\"hadSaved\"></button>\n            </div>\n        </div>\n    </form>\n    <p-dialog header=\"请选择\" [(visible)]=\"display\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <p-tree [value]=\"filesTree4\"\n                selectionMode=\"single\"\n                [(selection)]=\"selected\"\n                (onNodeExpand)=\"nodeExpand($event)\"\n        ></p-tree>\n        <!--<div style=\"margin-top:8px\">Selected Node: {{selected ? selected.label : 'none'}}</div>-->\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"display=false\"\n                    class=\"ui-button-secondary\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"message\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-change/malfunction-change.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionChange {\n  font-size: 14px; }\n  #MalfunctionChange div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionChange div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionChange form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-change/malfunction-change.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionChangeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MalfunctionChangeComponent = (function () {
    function MalfunctionChangeComponent(maintenanceService, router, fb, publicService, activatedRouter, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.fb = fb;
        this.publicService = publicService;
        this.activatedRouter = activatedRouter;
        this.eventBusService = eventBusService;
        this.options = {
            '来源': [],
            '影响度': [],
            '紧急度': [],
            '故障级别': [],
            '所属系统': [],
            'departments': [],
        };
        this.defaultOption = {
            'acceptor': null,
            'fromx': null,
            'influence': null,
            'urgency': null,
            'level': null,
            'bt_system': null,
        };
        this.display = false;
        this.filesTree4 = [];
        this.selected = [];
        this.message = []; // 交互提示弹出框
    }
    MalfunctionChangeComponent.prototype.ngOnInit = function () {
        this.formObj = new __WEBPACK_IMPORTED_MODULE_5__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.getDepPerson();
        this.initForm();
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        (this.formObj.sid) && (this.getTrickMessage(this.formObj.sid));
        // this.maintenanceService.getFlowChart( this.initStatus).subscribe(res => {
        //     this.eventBusService.maintenance.next(res);
        // });
    };
    MalfunctionChangeComponent.prototype.getTrickMessage = function (sid) {
        var _this = this;
        this.maintenanceService.getTrick(sid).subscribe(function (res) {
            _this.formObj.acceptor = res['acceptor'];
            _this.formObj.acceptor_pid = res['acceptor_pid'];
            _this.formObj.acceptor_org = res['acceptor_org'];
            _this.formObj.acceptor_org_oid = res['acceptor_org_oid'];
            (_this.formObj.acceptor_org_oid) && (_this.getDepPerson(_this.formObj.acceptor_org_oid));
        });
    };
    MalfunctionChangeComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionChangeComponent.prototype.getDepPerson = function (oid) {
        var _this = this;
        this.publicService.getApprovers(oid).subscribe(function (res) {
            if (!res) {
                _this.options['departments'] = [];
            }
            else {
                var newArray = __WEBPACK_IMPORTED_MODULE_7__services_PUblicMethod__["a" /* PUblicMethod */].formateDepDropDown(res);
                _this.options['departments'] = newArray;
                _this.formObj.acceptor_pid = newArray[0]['value']['pid'];
                _this.formObj.acceptor = newArray[0]['value']['name'];
            }
        });
    };
    MalfunctionChangeComponent.prototype.showTreeDialog = function (opType) {
        var _this = this;
        switch (opType) {
            case 'department':
                this.display = true;
                this.publicService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
        }
    };
    MalfunctionChangeComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    MalfunctionChangeComponent.prototype.clearTreeDialog = function (opType) {
        switch (opType) {
            case 'submitter':
                this.formObj.submitter = '';
                this.formObj.submitter_pid = '';
                this.formObj.submitter_org_oid = '';
                this.formObj.submitter_org = '';
                this.formObj.submitter_phone = '';
                break;
            case 'department':
                this.formObj.acceptor = '';
                this.formObj.acceptor_org = '';
                this.formObj.acceptor_org_oid = '';
                this.formObj.acceptor_pid = '';
                break;
        }
    };
    MalfunctionChangeComponent.prototype.closeTreeDialog = function () {
        this.display = false;
        this.formObj.acceptor_org = this.selected['label'];
        this.formObj.acceptor_org_oid = this.selected['oid'];
        this.getDepPerson(this.selected['oid']);
    };
    MalfunctionChangeComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            'acceptor': ''
        });
    };
    Object.defineProperty(MalfunctionChangeComponent.prototype, "acceptor", {
        get: function () {
            // console.log('untouched', this.myForm.controls['content'].untouched);
            // console.log('required', this.myForm.controls['content'].hasError('required'));
            return this.myForm.controls['acceptor'].untouched && this.myForm.controls['acceptor'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    // addSelectedName() {
    //     (!this.formObj.acceptor) && (this.formObj.acceptor = this.defaultOption['acceptor']);
    // }
    // filterOptionName() {
    //     if (this.formObj.acceptor['name']) {
    //         this.formObj.acceptor = this.formObj.acceptor['name'];
    //     }
    // }
    // onSelectedDepartment(event) {
    //     let msg = this.formObj.acceptor;
    //     this.formObj.acceptor = msg['name'];
    //     this.formObj.acceptor_pid = msg['pid'];
    // }
    MalfunctionChangeComponent.prototype.onChange = function (event) {
        this.formObj.acceptor = event['value']['name'];
        this.formObj.acceptor_pid = event['value']['pid'];
    };
    MalfunctionChangeComponent.prototype.submitTrick = function () {
        // this.addSelectedName();
        // this.filterOptionName();
        if (!this.formObj.acceptor_org) {
            this.showError();
            this.myForm.controls['acceptor'].setValidators([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]);
            this.myForm.controls['acceptor'].updateValueAndValidity();
        }
        else {
            // console.log(this.formObj);
            this.formObj.status = '待接受';
            this.submitPost(this.formObj);
        }
    };
    MalfunctionChangeComponent.prototype.showError = function () {
        this.message = [];
        this.message.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    MalfunctionChangeComponent.prototype.submitPost = function (paper) {
        var _this = this;
        this.maintenanceService.changeTrick(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionChangeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-change',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-change/malfunction-change.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-change/malfunction-change.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionChangeComponent);
    return MalfunctionChangeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-close-view/malfunction-close-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionView\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>查看故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-colse-base-view></app-colse-base-view>\n    </form>\n</div>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-close-view/malfunction-close-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionView {\n  font-size: 14px; }\n  #MalfunctionView div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionView div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionView form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-close-view/malfunction-close-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionCloseViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MalfunctionCloseViewComponent = (function () {
    function MalfunctionCloseViewComponent(activatedRouter, router, maintenanceService, eventBusService) {
        this.activatedRouter = activatedRouter;
        this.router = router;
        this.maintenanceService = maintenanceService;
        this.eventBusService = eventBusService;
    }
    MalfunctionCloseViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionCloseViewComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionCloseViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-close-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-close-view/malfunction-close-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-close-view/malfunction-close-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionCloseViewComponent);
    return MalfunctionCloseViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-close/malfunction-close.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionClose\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>关闭故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-colse-base-view></app-colse-base-view>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                关闭代码：\n            </label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"options['close_code']\" [(ngModel)]=\"formObj.close_code\"\n                             optionLabel=\"name\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"></p-dropdown>\n            </div>\n            <label class=\"col-sm-2 control-label\">关闭时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <input type=\"text\" pInputText  name=\"department\"\n                       [(ngModel)]=\"formObj.close_time\" readonly class=\"cursor_not_allowed\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button  class=\"pull-right  ui-margin-right-10px\"  pButton type=\"button\" (click)=\"solved()\" label=\"关闭并生成知识库\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"close()\" label=\"关闭\" ></button>\n            </div>\n        </div>\n    </form>\n    <p-growl [(value)]=\"message\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-close/malfunction-close.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionClose {\n  font-size: 14px; }\n  #MalfunctionClose div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionClose div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionClose form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-close/malfunction-close.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionCloseComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MalfunctionCloseComponent = (function () {
    function MalfunctionCloseComponent(maintenanceService, router, activatedRouter, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.eventBusService = eventBusService;
        this.options = {
            'close_code': [],
        };
        this.defaultOption = {
            'close_code': null
        };
        this.message = []; // 交互提示弹出框
    }
    MalfunctionCloseComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formObj = new __WEBPACK_IMPORTED_MODULE_5__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.initCloseCode();
        this.formObj.close_time = __WEBPACK_IMPORTED_MODULE_3__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(new Date());
        this.formObj.close_code = this.options['close_code'][0]['name'];
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionCloseComponent.prototype.initCloseCode = function () {
        this.options['close_code'] = [
            { name: '成功解决', code: 'NY' },
            { name: '临时解决', code: 'RM' },
            { name: '不成功', code: 'LDN' },
            { name: '撤销', code: 'IST' }
        ];
    };
    MalfunctionCloseComponent.prototype.addSelectedName = function () {
        (!this.formObj.fromx) && (this.formObj.fromx = this.defaultOption['fromx']);
        (!this.formObj.level) && (this.formObj.level = this.defaultOption['level']);
        (!this.formObj.bt_system) && (this.formObj.bt_system = this.defaultOption['bt_system']);
        (!this.formObj.influence) && (this.formObj.influence = this.defaultOption['influence']);
        (!this.formObj.urgency) && (this.formObj.urgency = this.defaultOption['urgency']);
        (!this.formObj.acceptor) && (this.formObj.acceptor = this.defaultOption['acceptor']);
    };
    MalfunctionCloseComponent.prototype.filterOptionName = function () {
        if (this.formObj.close_code['name']) {
            this.formObj.close_code = this.formObj.close_code['name'];
        }
    };
    MalfunctionCloseComponent.prototype.close = function () {
        this.addSelectedName();
        this.filterOptionName();
        if (!this.formObj.close_code) {
            this.showError();
        }
        else {
            this.submitPost(this.formObj);
        }
    };
    MalfunctionCloseComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionCloseComponent.prototype.showError = function () {
        this.message = [];
        this.message.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    MalfunctionCloseComponent.prototype.submitPost = function (paper) {
        var _this = this;
        this.maintenanceService.offTrick(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionCloseComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-close',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-close/malfunction-close.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-close/malfunction-close.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionCloseComponent);
    return MalfunctionCloseComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-closed-view/malfunction-closed-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionView\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>查看故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-colse-base-view></app-colse-base-view>\n        <app-simple-close></app-simple-close>\n    </form>\n</div>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-closed-view/malfunction-closed-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionView {\n  font-size: 14px; }\n  #MalfunctionView div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionView div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionView form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-1 {\n    width: 10.5%; }\n  .col-sm-3 {\n    width: 22%; }\n  .col-sm-11 {\n    width: 88.666667%; }\n  .col-sm-10 {\n    width: 78.333333%; }\n  .col-sm-5 {\n    width: 38.666667%; }\n  .col-sm-6 {\n    width: 56%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-closed-view/malfunction-closed-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionClosedViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionClosedViewComponent = (function () {
    function MalfunctionClosedViewComponent(activatedRouter, router, eventBusService) {
        this.activatedRouter = activatedRouter;
        this.router = router;
        this.eventBusService = eventBusService;
    }
    MalfunctionClosedViewComponent.prototype.ngOnInit = function () {
    };
    MalfunctionClosedViewComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionClosedViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-closed-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-closed-view/malfunction-closed-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-closed-view/malfunction-closed-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionClosedViewComponent);
    return MalfunctionClosedViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-deal/malfunction-deal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section  introduction \">\n    <div>\n        <span class=\"title\">故障处理</span>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"malfunctionOverview\" >\n    <div class=\"ui-grid-row\">\n        <app-search-form\n                [searchType]=\"searchType\"\n                (searchInfoEmitter)=\"searchInfoEmitter($event)\"></app-search-form>\n    </div>\n    <div class=\"ui-grid-row\">\n        <app-malfunction-table\n                #child\n                [searchType]=\"searchType\"\n                [eventDatas]=\"eventDatas\"\n        ></app-malfunction-table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-deal/malfunction-deal.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".title {\n  font-size: 16px;\n  font-weight: bold; }\n\n.implementation {\n  padding: 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-deal/malfunction-deal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionDealComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionDealComponent = (function () {
    function MalfunctionDealComponent(eventBusService, maintenanceServicen) {
        this.eventBusService = eventBusService;
        this.maintenanceServicen = maintenanceServicen;
        this.searchType = '待处理'; // 搜索类型
    }
    MalfunctionDealComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.maintenanceServicen.getFlowChart().subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionDealComponent.prototype.searchInfoEmitter = function (event) {
        this.eventDatas = event;
    };
    MalfunctionDealComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-deal',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-deal/malfunction-deal.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-deal/malfunction-deal.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */]])
    ], MalfunctionDealComponent);
    return MalfunctionDealComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-distribute/malfunction-distribute.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"title\">待我处理</span>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"malfunctionOverview\" >\n    <div class=\"ui-grid-row\">\n        <app-search-form\n                [searchType]=\"searchType\"\n                (searchInfoEmitter)=\"searchInfoEmitter($event)\"></app-search-form>\n    </div>\n    <div class=\"ui-grid-row\">\n        <app-malfunction-table\n                #child\n                [searchType]=\"searchType\"\n                [eventDatas]=\"eventDatas\"\n        ></app-malfunction-table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-distribute/malfunction-distribute.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".title {\n  font-size: 16px;\n  font-weight: bold; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-distribute/malfunction-distribute.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionDistributeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionDistributeComponent = (function () {
    function MalfunctionDistributeComponent(eventBusService, maintenanceServicen) {
        this.eventBusService = eventBusService;
        this.maintenanceServicen = maintenanceServicen;
        this.searchType = '待分配'; // 搜索类型
    }
    MalfunctionDistributeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.maintenanceServicen.getFlowChart().subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionDistributeComponent.prototype.searchInfoEmitter = function (event) {
        this.eventDatas = event;
    };
    MalfunctionDistributeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-distribute',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-distribute/malfunction-distribute.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-distribute/malfunction-distribute.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */]])
    ], MalfunctionDistributeComponent);
    return MalfunctionDistributeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-edit/malfunction-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<p-messages [(value)]=\"msg\"></p-messages>\n<div class=\"content-section implementation GridDemo\" id=\"MalfunctionEdit\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>编辑故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    [routerLink]=\"['/index/maintenance/malfunctionBase']\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n        <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">服务单号：</label>\n                <div class=\"col-sm-3 ui-fluid-no-padding\">\n                    <input type=\"text\"\n                           pInputText\n                           placeholder=\"自动生成\"\n                           [(ngModel)]=\"malfNumber\"\n                           readonly\n                           class=\" form-control\"\n                           formControlName=\"malfNumber\"/>\n                </div>\n            <label class=\"col-sm-1 control-label\">来源：</label>\n            <div class=\"col-sm-3  ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"malSourceOptins\"\n                            [(ngModel)]=\"malSource\"\n                            optionLabel=\"name\"\n                            [style]=\"{'width':'100%'}\"\n                            [ngModelOptions]=\"{standalone: true}\"></p-dropdown>\n            </div>\n            <label class=\"col-sm-1 control-label\">状态：</label>\n            <div class=\"col-sm-3  ui-fluid-no-padding \">\n                <input type=\"text\" pInputText placeholder=\"新建\"\n                       class=\" form-control\"\n                       [(ngModel)]=\"malStatus\" readonly\n                       formControlName=\"malStatus\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label  class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                报告人：\n            </label>\n            <div class=\"col-sm-3 ui-fluid-no-padding\">\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText  name=\"\" placeholder=\"请选择报告人\"\n                           [(ngModel)]=\"reporter.name\"\n                           readonly class=\"cursor_not_allowed\"\n                           formControlName=\"reporterLabel\"/>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\" (click)=\"showTreeDialog('reporter')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\" (click)=\"clearTreeDialog('reporter')\" label=\"清空\"></button>\n                </div>\n            </div>\n                <label  class=\"col-sm-1 control-label\">组织：</label>\n                <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           class=\" form-control\"\n                           placeholder=\"选择报告人后自动显示\"\n                           [(ngModel)]=\"reporter.organization\"\n                           readonly   formControlName=\"reporterFather\"/>\n                </div>\n                <label  class=\"col-sm-1 control-label\">联系电话：</label>\n                <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           class=\" form-control\"\n                           placeholder=\"选择人员后自动显示\" [(ngModel)]=\"reporter.mobile\" readonly   formControlName=\"reporterRemark\"/>\n                </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-1 control-label\">受理时间：</label>\n            <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                <p-calendar [(ngModel)]=\"dealTime\"\n                            [showIcon]=\"true\"\n                            [locale]=\"zh\"\n                            name=\"end_time\"\n                            dateFormat=\"yy-mm-dd\"\n                            [required]=\"true\"\n                            [showTime]=\"true\"\n                            formControlName=\"dealTime\">\n                </p-calendar>\n            </div>\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                发生时间：\n            </label>\n            <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                <p-calendar [(ngModel)]=\"occurTime\"\n                            [showIcon]=\"true\"\n                            [locale]=\"zh\"\n                            name=\"end_time\"\n                            dateFormat=\"yy-mm-dd\"\n                            [required]=\"true\"\n                            [showTime]=\"true\"\n                            formControlName=\"occurTime\">\n                </p-calendar>\n            </div>\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                最终期限：\n            </label>\n            <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                <input type=\"text\" pInputText placeholder=\"影响度、紧急度、优先级后自动显示\"   class=\" form-control\"\n                       [(ngModel)]=\"malDeadline\" readonly formControlName=\"malDeadline\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                影响度：\n            </label>\n            <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                <p-dropdown [options]=\"malImpactOptions\"\n                            [(ngModel)]=\"malImpact\"\n                            optionLabel=\"name\"\n                            formControlName=\"malImpact\"\n                            [style]=\"{'width':'100%'}\"\n                            (onFocus)=\"onFocus()\"\n                ></p-dropdown>\n            </div>\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                紧急度：\n            </label>\n            <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                <p-dropdown [options]=\"malUrgencyOptions\"\n                            [(ngModel)]=\"malUrgency\"\n                            placeholder=\"{{ malUrgency?.name }}\"\n                            optionLabel=\"name\"\n                            formControlName=\"malUrgency\"\n                            [style]=\"{'width':'100%'}\"\n                            (onFocus)=\"onFocus()\"\n                ></p-dropdown>\n            </div>\n            <label class=\"col-sm-1 control-label\">优先级：</label>\n            <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                <input type=\"text\" pInputText placeholder=\"选择影响度和紧急度后自动显示\"\n                       [(ngModel)]=\"malfPrority\" readonly    class=\" form-control\"  formControlName=\"malfPrority\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-1 control-label\">故障级别：</label>\n            <div class=\"col-sm-3 ui-fluid ui-fluid-no-padding\">\n                <p-dropdown [options]=\"malLevelOptions\"\n                            [(ngModel)]=\"malLevel\"\n                            placeholder=\"{{ malLevel?.name }}\"\n                            optionLabel=\"name\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\">\n                </p-dropdown>\n            </div>\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                所属系统：\n            </label>\n            <div class=\"col-sm-3  ui-fluid-no-padding\">\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText  name=\"\" placeholder=\"请选择所属系统\"\n                           [(ngModel)]=\"belongSystem.label\" readonly class=\"cursor_not_allowed\"\n                           formControlName=\"malLevel\"/>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button pButton type=\"button\" (click)=\"showTreeDialog('belongSystem')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button pButton type=\"button\" (click)=\"clearTreeDialog('belongSystem')\" label=\"清空\"></button>\n                </div>\n            </div>\n                <label class=\"col-sm-1 control-label\">发生地点：</label>\n                <div class=\"col-sm-3  ui-fluid-no-padding\">\n                    <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                        <input type=\"text\" pInputText  name=\"department\" placeholder=\"请选择发生地点\"    formControlName=\"belongSystemLabel\"\n                               [(ngModel)]=\"locationOccur.label\" readonly class=\"cursor_not_allowed\"/>\n                    </div>\n                    <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                        <button pButton type=\"button\" (click)=\"showTreeDialog('locationOccur')\" label=\"选择\"></button>\n                    </div>\n                    <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                        <button pButton type=\"button\" (click)=\"clearTreeDialog('locationOccur')\" label=\"清空\"></button>\n                    </div>\n                </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                故障标题：\n            </label>\n            <div class=\"col-sm-11 ui-no-padding-left-15px ui-fluid\">\n                <input type=\"text\" pInputText  placeholder=\"请填写故障标题\"   formControlName=\"malTitle\"\n                       [(ngModel)]=\"malTitle\"/>\n            </div>\n        </div>\n        <div class=\"form-group ui-fluid\">\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                故障描述：\n            </label>\n            <div class=\"col-sm-11 ui-no-padding-left-15px \">\n                     <textarea [rows]=\"5\" pInputTextarea autoResize=\"autoResize\"    formControlName=\"malDesc\"\n                               [(ngModel)]=\"malDesc\"></textarea>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-1 control-label\">\n                上传附件：\n            </label>\n            <div class=\"col-sm-11 ui-fluid-no-padding \" *ngIf=\"malStatus == '新建' \">\n                <p-fileUpload name=\"file\" url=\"{{ip}}/workflow/trouble/upload\"\n                              multiple=\"multiple\"\n                              accept=\"image/*,application/*,text/*\"\n                              chooseLabel=\"选择\"\n                              uploadLabel=\"上传\"\n                              cancelLabel=\"取消\"\n                              maxFileSize=\"3145728\"\n                              (onUpload)=\"onUpload($event)\"\n                              (onBeforeUpload)=\"onBeforeUpload($event)\"\n                              #form>\n                    <ng-template pTemplate=\"content\">\n                        <ul *ngIf=\"uploadedFiles.length\">\n                            <li *ngFor=\"let file of uploadedFiles\">{{file.name}} - {{file.size}} bytes</li>\n                        </ul>\n                    </ng-template>\n                </p-fileUpload>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-1 control-label\">\n                关联服务目录：\n            </label>\n            <div class=\"col-sm-11 ui-fluid-no-padding\">\n                <div class=\"col-sm-10 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText  name=\"department\" placeholder=\"请选择关联服务目录\"  formControlName=\"relativeCategoreLabel\"\n                           [(ngModel)]=\"relativeCategore.label\" readonly class=\"cursor_not_allowed\"/>\n                </div>\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\" (click)=\"showTreeDialog('relativeCategore')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\" (click)=\"clearTreeDialog('relativeCategore')\" label=\"清空\"></button>\n                </div>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label  class=\"col-sm-1 control-label\">\n                关联设备编号：\n            </label>\n            <div class=\"col-sm-11 ui-fluid-no-padding\">\n                <div class=\"col-sm-10 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText  name=\"department\" placeholder=\"请选择关联设备编号\"     formControlName=\"relativeEquipLabel\"\n                           [(ngModel)]=\"relativeEquip.label\" readonly class=\"cursor_not_allowed\"/>\n                </div>\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\" (click)=\"showTreeDialog('relativeEquip')\" label=\"选择\"></button>\n\n                </div>\n                <div class=\"col-sm-1 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\" (click)=\"clearTreeDialog('relativeEquip')\" label=\"清空\"></button>\n                </div>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                分配至部门：\n            </label>\n            <div class=\"col-sm-5 ui-fluid-no-padding\">\n                <div class=\"col-sm-8 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText  name=\"department\"\n                           placeholder=\"请选择部门\"\n                           [(ngModel)]=\"selectedDepartment.label\" readonly\n                           class=\" form-control cursor_not_allowed\"\n                           formControlName=\"selectedDepartmentLabel\"/>\n                </div>\n                <div class=\"col-sm-2 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\" (click)=\"showTreeDialog('selectedDepartment')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-2 ui-fluid-no-padding ui-padding-10px\">\n                    <button ngClass=\"ui-g-12 ui-fluid-no-padding\" pButton type=\"button\" (click)=\"clearTreeDialog('selectedDepartment')\" label=\"清空\"></button>\n\n                </div>\n            </div>\n            <label class=\"col-sm-1 control-label\">分配至个人：</label>\n            <div class=\"col-sm-5 ui-fluid-no-padding\">\n                <p-dropdown [options]=\"malDepPersonOptions\"\n                            [(ngModel)]=\"malDepPerson\"\n                            optionLabel=\"name\"\n                            placeholder=\"请选择人员\"\n                            formControlName=\"malDepPerson\"\n                            [style]=\"{'width':'100%'}\"></p-dropdown>\n            </div>\n\n        </div>\n        <div class=\"form-group\">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button class=\"pull-right  ui-button-secondary\"  pButton type=\"button\" [routerLink]=\"['/index/maintenance/malfunctionBase']\" label=\"关闭\" [disabled]=\"saved\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submitAndDistribute()\" label=\"保存并分配\" [disabled]=\"hadSaved\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submitTrick()\" label=\"提交\" [disabled]=\"hadSaved\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"save()\" label=\"保存\" [disabled]=\"hadSaved\"></button>\n            </div>\n        </div>\n    </form>\n    <p-dialog header=\"{{ dialogHeader }}\" [(visible)]=\"departementDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <p-tree [value]=\"filesTree4\"\n                selectionMode=\"{{ selectedMode }}\"\n                [(selection)]=\"selected\"\n                (onNodeExpand)=\"nodeExpand($event)\"\n                (onNodeSelect)=\"nodeSelect($event)\"\n        ></p-tree>\n        <!--<div>Selected Nodes: <span *ngFor=\"let file of selected\">{{file.label}} </span></div>-->\n        <div style=\"margin-top:8px\">Selected Node: {{selected ? selected.label : 'none'}}</div>\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"departementDisplay=false\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n    <app-personel-dialog *ngIf=\"displayPersonel\"\n                         (dataEmitter)=\"dataEmitter($event)\"\n                         (displayEmitter)=\"displayEmitter($event)\"></app-personel-dialog>\n    <p-growl [(value)]=\"message\"></p-growl>\n    <p-growl [(value)]=\"message\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-edit/malfunction-edit.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n.form-horizontal .form-group {\n  margin-right: -15px;\n  margin-left: -31px; }\n\n#MalfunctionEdit {\n  font-size: 14px; }\n  #MalfunctionEdit div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionEdit div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionEdit form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-1 {\n    width: 10.5%; }\n  .col-sm-3 {\n    width: 22%; }\n  .col-sm-11 {\n    width: 88.666667%; }\n  .col-sm-10 {\n    width: 78.333333%; }\n  .col-sm-5 {\n    width: 38.666667%; }\n  .col-sm-6 {\n    width: 56%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-edit/malfunction-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment_prod__ = __webpack_require__("../../../../../src/environments/environment.prod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MalfunctionEditComponent = (function () {
    function MalfunctionEditComponent(maintenanceService, router, storageService, publicService, activatedRouter, fb, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.storageService = storageService;
        this.publicService = publicService;
        this.activatedRouter = activatedRouter;
        this.fb = fb;
        this.eventBusService = eventBusService;
        this.initStatus = ''; // 故障单状态
        this.malfNumber = ''; // 服务单号
        this.malSourceOptins = []; // 可选来源数据
        this.malImpactOptions = []; // 影响度可选数据
        this.malUrgencyOptions = []; // 紧急度可选数据
        this.malLevelOptions = []; // 故障级别选项
        this.malfPrority = ''; // 优先级
        this.malDepPersonOptions = []; // 分配个人可选数据
        this.malStatus = '新建'; // 状态
        this.reporter = []; // 报告人
        this.malDeadline = ''; // 最终期限
        this.malTitle = ''; // 故障单标题
        this.malDesc = ''; // 故障单描述
        this.uploadedFiles = [];
        this.uploadRes = []; // 上传成功返回的文件路径对象
        this.message = []; // 交互提示弹出框
        this.msg = []; // 验证弹出框
        this.belongSystem = []; // 所属系统
        this.locationOccur = []; // 发生地点
        this.relativeCategore = []; // 关联目录
        this.relativeEquip = []; // 关联设备编号
        this.selectedDepartment = []; // 分配至部门
    }
    MalfunctionEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initForm();
        var initSid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initStatus = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.initStatus).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
        this.zh = new __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */]().initZh();
        this.dealTime = new Date();
        this.getOptions();
        this.getDepPerson();
        this.departementDisplay = false;
        this.ip = __WEBPACK_IMPORTED_MODULE_5__environments_environment_prod__["a" /* environment */].url.management;
        this.initTrick(initSid);
    };
    MalfunctionEditComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            'sid': '',
            'malfNumber': '',
            'malStatus': '',
            'reporterLabel': ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required],
            'reporterFather': '',
            'reporterRemark': '',
            'dealTime': '',
            'occurTime': '',
            'malDeadline': '',
            'malImpact': '',
            'malUrgency': '',
            'malfPrority': '',
            'malLevel': '',
            'belongSystemLabel': '',
            'locationOccurLabel': '',
            'malTitle': ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required],
            'malDesc': ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required],
            'upload': '',
            'relativeCategoreLabel': '',
            'relativeEquipLabel': '',
            'selectedDepartmentLabel': '',
            'malDepPerson': ''
        });
    };
    MalfunctionEditComponent.prototype.initTrick = function (s) {
        var _this = this;
        this.maintenanceService.getTrick(s).subscribe(function (res) {
            console.log(res);
            var data = res['data'];
            if (res['errcode'] === '00000') {
                _this.malfNumber = data['sid'];
                _this.malSource = { name: data['fromx'] };
                _this.malStatus = data['status'];
                _this.reporter['name'] = data['submitter'];
                _this.reporter['organization'] = data['submitter_org'];
                _this.reporter['mobile'] = data['submitter_phone'];
                _this.reporter['pid'] = data['submitter_pid'];
                _this.reporter['oid'] = data['submitter_org_oid'];
                _this.malDeadline = data['deadline'];
                _this.malUrgency = { name: data['urgency'] };
                if (!data['influence']) {
                    _this.malImpact = { name: '请选择紧急度' };
                }
                else {
                    _this.malImpact = { name: data['influence'] };
                }
                if (!data['urgency']) {
                    _this.malUrgency = { name: '请选择紧急度' };
                }
                else {
                    _this.malUrgency = { name: data['urgency'] };
                }
                _this.malfPrority = data['priority'];
                if (!data['level']) {
                    _this.malLevel = { name: '请选择故障级别' };
                }
                else {
                    _this.malLevel = { name: data['level'] };
                }
                if (!data['acceptor']) {
                    _this.malDepPerson = { name: '请选择人员' };
                }
                else {
                    _this.malDepPerson = { name: data['acceptor'] };
                }
                _this.belongSystem['label'] = data['bt_system'];
                _this.locationOccur['label'] = data['addr'];
                _this.malTitle = data['title'];
                _this.malDesc = data['content'];
                _this.relativeCategore['label'] = data['service'];
                _this.relativeEquip['label'] = data['devices'];
                _this.selectedDepartment['label'] = data['acceptor_org'];
                (data['accept_time']) && (_this.dealTime = data['accept_time']);
                (data['occurrence_time']) && (_this.occurTime = new Date(data['occurrence_time']));
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u83B7\u53D6\u6545\u969C\u5355\u4FE1\u606F\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionEditComponent.prototype.getOptions = function () {
        var _this = this;
        this.maintenanceService.getParamOptions().subscribe(function (res) {
            if (!res) {
                _this.malSourceOptins = [];
                _this.malImpactOptions = [];
                _this.malUrgencyOptions = [];
                _this.malLevelOptions = [];
            }
            else {
                _this.malSourceOptins = res['来源'];
                _this.malImpactOptions = res['影响度'];
                _this.malUrgencyOptions = res['紧急度'];
                _this.malLevelOptions = res['故障级别'];
            }
        });
    };
    MalfunctionEditComponent.prototype.onBeforeUpload = function (event) {
        var token = this.storageService.getToken('token');
        event.formData.append('access_token', token);
    };
    MalfunctionEditComponent.prototype.onUpload = function (event) {
        var xhrRespose = JSON.parse(event.xhr.response);
        if (xhrRespose.errcode && xhrRespose.errcode === '00000') {
            this.uploadRes = xhrRespose['datas'];
            this.message = [];
            this.message.push({ severity: 'success', summary: '消息提示', detail: '上传成功' });
        }
        else {
            this.message = [];
            this.message.push({ severity: 'error', summary: '消息提示', detail: "\u4E0A\u4F20\u5931\u8D25" + xhrRespose.errmsg });
        }
    };
    // 部门组织树模态框
    MalfunctionEditComponent.prototype.showTreeDialog = function (which) {
        var _this = this;
        this.currentTreeName = which;
        this.selected = [];
        this.filesTree4 = [];
        this.departementDisplay = true;
        switch (which) {
            case 'reporter':
                this.departementDisplay = false;
                this.displayPersonel = true;
                break;
            case 'belongSystem':
                this.dialogHeader = '请选择所属系统';
                this.selectedMode = 'single';
                this.publicService.getMalfunctionBasalDatas('1.006', '1').subscribe(function (res) {
                    _this.filesTree4 = res;
                });
                break;
            case 'locationOccur':
                this.dialogHeader = '请选择发生地点';
                this.selectedMode = 'single';
                this.maintenanceService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
                break;
            case 'relativeCategore':
                this.dialogHeader = '请选择关联服务目录';
                this.selectedMode = 'single';
                this.maintenanceService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
                break;
            case 'relativeEquip':
                this.dialogHeader = '请选择关联设备编号';
                this.selectedMode = 'single';
                this.maintenanceService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
                break;
            case 'selectedDepartment':
                this.dialogHeader = '请选择部门';
                this.selectedMode = 'single';
                this.maintenanceService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
                break;
        }
    };
    // 组织树懒加载
    MalfunctionEditComponent.prototype.nodeExpand = function (event) {
        switch (this.currentTreeName) {
            case 'belongSystem':
                if (event.node) {
                    this.maintenanceService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                        event.node.children = res;
                    });
                }
                break;
            case 'locationOccur':
                if (event.node) {
                    this.maintenanceService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                        event.node.children = res;
                    });
                }
                break;
            case 'relativeCategore':
                if (event.node) {
                    this.maintenanceService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                        event.node.children = res;
                    });
                }
                break;
            case 'relativeEquip':
                if (event.node) {
                    this.maintenanceService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                        event.node.children = res;
                    });
                }
                break;
            case 'selectedDepartment':
                if (event.node) {
                    this.maintenanceService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                        event.node.children = res;
                    });
                }
                break;
        }
    };
    MalfunctionEditComponent.prototype.nodeSelect = function (event) {
        console.log(event);
    };
    // 清空组织树选中内容
    MalfunctionEditComponent.prototype.clearTreeDialog = function (which) {
        switch (which) {
            case 'reporter':
                this.reporter = [];
                break;
            case 'belongSystem':
                this.belongSystem = [];
                break;
            case 'locationOccur':
                this.locationOccur = [];
                break;
            case 'relativeCategore':
                this.relativeCategore = [];
                break;
            case 'relativeEquip':
                this.relativeEquip = [];
                break;
            case 'selectedDepartment':
                this.selectedDepartment = [];
                this.getDepPerson();
                break;
        }
    };
    // 组织树最后选择
    MalfunctionEditComponent.prototype.closeTreeDialog = function () {
        switch (this.currentTreeName) {
            case 'belongSystem':
                this.belongSystem = this.selected;
                break;
            case 'locationOccur':
                this.locationOccur = this.selected;
                break;
            case 'relativeCategore':
                this.relativeCategore = this.selected;
                break;
            case 'relativeEquip':
                this.relativeEquip = this.selected;
                break;
            case 'selectedDepartment':
                this.selectedDepartment = this.selected;
                this.getDepPerson(this.selected['oid']);
                break;
        }
        this.departementDisplay = false;
    };
    MalfunctionEditComponent.prototype.getDepPerson = function (oid) {
        var _this = this;
        this.publicService.getApprovers(oid).subscribe(function (res) {
            if (!res) {
                _this.malDepPersonOptions = [];
            }
            else {
                _this.malDepPersonOptions = res;
            }
        });
    };
    MalfunctionEditComponent.prototype.onFocus = function () {
        this.getPrority();
    };
    // 计算优先级
    MalfunctionEditComponent.prototype.getPrority = function () {
        var _this = this;
        if (this.malImpact && this.malUrgency) {
            this.maintenanceService.getPrority(this.malImpact['name'], this.malUrgency['name']).subscribe(function (res) {
                if (res) {
                    _this.malfPrority = res['priority'];
                    _this.getDeadline();
                }
            });
        }
    };
    // 计算最终期限
    MalfunctionEditComponent.prototype.getDeadline = function () {
        // if ( this.malImpact && this.malUrgency && this.malfPrority ) {
        //     this.maintenanceService.getDeadline( this.malImpact['name'], this.malUrgency['name'], this.malfPrority ).subscribe(res => {
        //         if (res) {
        //             this.malDeadline = res['deadline'];
        //         }
        //     });
        // }
    };
    // 日期格式化
    MalfunctionEditComponent.formatDate = function (date) {
        if (date) {
            return date.getFullYear() +
                '-' + pad(date.getMonth() + 1) +
                '-' + pad(date.getDate()) +
                ' ' + pad(date.getHours()) +
                ':' + pad(date.getMinutes()) +
                ':' + pad((date.getSeconds() / 1000).toFixed());
        }
        else {
            return date;
        }
        function pad(num) {
            if (num < 10) {
                return '0' + num;
            }
            return num;
        }
    };
    //  保存故障单
    MalfunctionEditComponent.prototype.save = function () {
        var _this = this;
        var pathArray = [];
        if (this.uploadRes.length > 0) {
            for (var _i = 0, _a = this.uploadRes; _i < _a.length; _i++) {
                var val = _a[_i];
                pathArray.push(val['path']);
            }
        }
        console.log(this.malLevel);
        var paper = {
            'sid': this.malfNumber,
            'name': '',
            'fromx': this.malSource['name'],
            'status': this.malStatus,
            'submitter': this.reporter['name'],
            'submitter_pid': this.reporter['pid'],
            'submitter_org': this.reporter['organization'],
            'submitter_org_oid': this.reporter['oid'],
            'submitter_phone': this.reporter['mobile'],
            // 'occurrence_time': MalfunctionAddComponent.formatDate( this.occurTime ),
            'deadline': this.malDeadline,
            'influence': this.malImpact['name'],
            'urgency': this.malUrgency['name'],
            'priority': this.malfPrority,
            'level': this.malLevel['name'],
            'bt_system': '',
            'addr': this.locationOccur['label'],
            'service': this.relativeCategore['label'],
            'devices': this.relativeEquip['label'],
            'title': this.malTitle,
            'content': this.malDesc,
            'attachment': pathArray,
            'acceptor': this.malDepPerson,
        };
        this.maintenanceService.saveTicket(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '保存成功' });
                window.setTimeout(function () {
                    _this.router.navigateByUrl('/index/maintenance/malfunctionBase');
                }, 1500);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u4FDD\u5B58\u5931\u8D25" + res });
            }
        });
    };
    //  提交故障单
    MalfunctionEditComponent.prototype.submitTrick = function () {
        var pathArray = [];
        if (this.uploadRes.length > 0) {
            for (var _i = 0, _a = this.uploadRes; _i < _a.length; _i++) {
                var val = _a[_i];
                pathArray.push(val['path']);
            }
        }
        console.log(this.reporter);
        var paper = {
            'sid': this.malfNumber,
            'name': '',
            'fromx': this.malSource['name'],
            'status': '待分配',
            'submitter': this.reporter['name'],
            'submitter_pid': this.reporter['pid'],
            'submitter_org': this.reporter['organization'],
            'submitter_org_oid': this.reporter['oid'],
            'submitter_phone': this.reporter['mobile'],
            'occurrence_time': __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(this.occurTime),
            'deadline': this.malDeadline,
            'influence': this.malImpact['name'],
            'urgency': this.malUrgency['name'],
            'priority': this.malfPrority,
            'level': this.malLevel['name'],
            'bt_system': this.belongSystem['label'],
            'addr': this.locationOccur['label'],
            'service': this.relativeCategore['label'],
            'devices': this.relativeCategore['label'],
            'title': this.malTitle,
            'content': this.malDesc,
            'attachment': pathArray,
            'acceptor': this.malDepPerson['name'],
            'acceptor_pid': this.malDepPerson['pid'],
            'acceptor_org': this.malDepPerson['label'],
            'acceptor_org_oid': this.malDepPerson['oid']
        };
        if (!this.reporter['name']
            || !this.occurTime
            || !this.malDeadline
            || !this.malImpact
            || !this.malUrgency
            || !this.belongSystem['label']
            || !this.malTitle
            || !this.malDesc) {
            this.showError();
        }
        else {
            this.submitPost(paper);
        }
    };
    MalfunctionEditComponent.prototype.showError = function () {
        this.msg = [];
        this.msg.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    MalfunctionEditComponent.prototype.submitPost = function (paper) {
        var _this = this;
        this.maintenanceService.submitTicket(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.msg = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.router.navigateByUrl('/index/maintenance/malfunctionBase');
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    //  提交并分配故障单
    MalfunctionEditComponent.prototype.submitAndDistribute = function () {
        var pathArray = [];
        if (this.uploadRes.length > 0) {
            for (var _i = 0, _a = this.uploadRes; _i < _a.length; _i++) {
                var val = _a[_i];
                pathArray.push(val['path']);
            }
        }
        var paper = {
            'sid': this.malfNumber,
            'name': '',
            'fromx': this.malSource['name'],
            'status': '待接受',
            'submitter': this.reporter['name'],
            'submitter_pid': this.reporter['pid'],
            'submitter_org': this.reporter['organization'],
            'submitter_org_oid': this.reporter['oid'],
            'submitter_phone': this.reporter['mobile'],
            'occurrence_time': __WEBPACK_IMPORTED_MODULE_6__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(this.occurTime),
            'deadline': this.malDeadline,
            'influence': this.malImpact['name'],
            'urgency': this.malUrgency['name'],
            'priority': this.malfPrority,
            'level': this.malLevel['name'],
            'bt_system': this.belongSystem['label'],
            'addr': this.locationOccur['label'],
            'service': this.relativeCategore['label'],
            'devices': this.relativeCategore['label'],
            'title': this.malTitle,
            'content': this.malDesc,
            'attachment': pathArray,
            'acceptor': this.malDepPerson['name'],
            'acceptor_pid': this.malDepPerson['pid'],
            'acceptor_org': this.malDepPerson[''],
            'acceptor_org_oid': this.malDepPerson['oid']
        };
        console.log(paper);
        if (!this.reporter['name']
            || !this.occurTime
            || !this.malDeadline
            || !this.malImpact
            || !this.malUrgency
            || !this.belongSystem['label']
            || !this.malTitle
            || !this.malDesc
            || !String(this.selectedDepartment)) {
            this.showError();
            console.log(!this.reporter['name']);
            console.log(!this.occurTime);
            console.log(!this.malDeadline);
            console.log(this.malImpact);
            console.log(!this.malUrgency);
            console.log(!this.belongSystem['label']);
            console.log(!this.malTitle);
            console.log(!this.malDesc);
            console.log(!String(this.selectedDepartment));
        }
        else {
            this.submitPost(paper);
        }
    };
    MalfunctionEditComponent.prototype.dataEmitter = function (event) {
        console.log(event);
        this.reporter = event;
    };
    MalfunctionEditComponent.prototype.displayEmitter = function (event) {
        this.displayPersonel = event;
    };
    MalfunctionEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-edit',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-edit/malfunction-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-edit/malfunction-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_4__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_8__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionEditComponent);
    return MalfunctionEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-off/malfunction-off.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"title\">已关闭</span>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"malfunctionOverview\" >\n    <div class=\"ui-grid-row\">\n        <app-search-form\n                [searchType]=\"searchType\"\n                (searchInfoEmitter)=\"searchInfoEmitter($event)\"></app-search-form>\n    </div>\n    <div class=\"ui-grid-row\">\n        <app-malfunction-table\n                #child\n                [searchType]=\"searchType\"\n                [eventDatas]=\"eventDatas\"\n        ></app-malfunction-table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-off/malfunction-off.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".title {\n  font-size: 16px;\n  font-weight: bold; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-off/malfunction-off.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionOffComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionOffComponent = (function () {
    function MalfunctionOffComponent(eventBusService, maintenanceServicen) {
        this.eventBusService = eventBusService;
        this.maintenanceServicen = maintenanceServicen;
        this.searchType = '已解决'; // 搜索类型
    }
    MalfunctionOffComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.maintenanceServicen.getFlowChart().subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionOffComponent.prototype.searchInfoEmitter = function (event) {
        this.eventDatas = event;
    };
    MalfunctionOffComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-off',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-off/malfunction-off.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-off/malfunction-off.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */]])
    ], MalfunctionOffComponent);
    return MalfunctionOffComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-overview/malfunction-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"feature-title\">故障总览</span>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"malfunctionOverview\" >\n    <div class=\"ui-grid-row\">\n        <app-search-form\n            [searchType]=\"searchType\"\n            (searchInfoEmitter)=\"searchInfoEmitter($event)\"></app-search-form>\n    </div>\n    <div class=\"ui-grid-row\">\n        <app-malfunction-table\n            #child\n            [searchType]=\"searchType\"\n            [eventDatas]=\"eventDatas\"\n        ></app-malfunction-table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-overview/malfunction-overview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-overview/malfunction-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionOverviewComponent = (function () {
    function MalfunctionOverviewComponent(eventBusService, maltenanceService) {
        this.eventBusService = eventBusService;
        this.maltenanceService = maltenanceService;
        this.searchType = 'trouble_get'; // 搜索类型
    }
    MalfunctionOverviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.maltenanceService.getFlowChart().subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionOverviewComponent.prototype.searchInfoEmitter = function (event) {
        this.eventDatas = event;
    };
    MalfunctionOverviewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-overview',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-overview/malfunction-overview.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-overview/malfunction-overview.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */]])
    ], MalfunctionOverviewComponent);
    return MalfunctionOverviewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-process/malfunction-process.component.html":
/***/ (function(module, exports) {

module.exports = "<p-messages [(value)]=\"msg\"></p-messages>\n<div class=\"content-section implementation GridDemo\" id=\"MalfunctionProcess\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>处理故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                   (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-simple-view></app-simple-view>\n        <app-simple-time></app-simple-time>\n        <div class=\"form-group \">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"turn()\" label=\"转派\" [disabled]=\"hadSaved\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"up()\" label=\"升级\" [disabled]=\"hadSaved\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\"  label=\"分配工作单\" [disabled]=\"hadSaved\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"deal()\" label=\"处理\" [disabled]=\"hadSaved\"></button>\n            </div>\n\n        </div>\n    </form>\n    <p-dialog header=\"{{ dialogHeader }}\" [(visible)]=\"departementDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <p-tree [value]=\"filesTree4\"\n                selectionMode=\"{{ selectedMode }}\"\n                [(selection)]=\"selected\"\n                (onNodeExpand)=\"nodeExpand($event)\"\n                (onNodeSelect)=\"nodeSelect($event)\"\n        ></p-tree>\n        <!--<div>Selected Nodes: <span *ngFor=\"let file of selected\">{{file.label}} </span></div>-->\n        <div style=\"margin-top:8px\">Selected Node: {{selected ? selected.label : 'none'}}</div>\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"departementDisplay=false\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"message\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-process/malfunction-process.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionProcess {\n  font-size: 14px; }\n  #MalfunctionProcess div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionProcess div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionProcess form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-process/malfunction-process.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionProcessComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MalfunctionProcessComponent = (function () {
    function MalfunctionProcessComponent(maintenanceService, router, activatedRouter, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.eventBusService = eventBusService;
        this.message = []; // 交互提示弹出框
    }
    MalfunctionProcessComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formObj = new __WEBPACK_IMPORTED_MODULE_4__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionProcessComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionProcessComponent.prototype.deal = function () {
        var _this = this;
        this.formObj.status = '处理中';
        this.maintenanceService.dealTrick(this.formObj).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '处理成功' });
                window.setTimeout(function () {
                    _this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionSolved', { id: _this.formObj.sid, status: _this.formObj.status }]);
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: '处理失败' + res });
            }
        });
    };
    MalfunctionProcessComponent.prototype.up = function () {
        this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionUp', { id: this.formObj.sid, status: this.formObj.status }]);
    };
    MalfunctionProcessComponent.prototype.turn = function () {
        this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionTurn', { id: this.formObj.sid, status: this.formObj.status }]);
    };
    MalfunctionProcessComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-process',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-process/malfunction-process.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-process/malfunction-process.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionProcessComponent);
    return MalfunctionProcessComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-receive/malfunction-receive.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"title\">接受故障单</span>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"malfunctionOverview\" >\n    <div class=\"ui-grid-row\">\n        <app-search-form\n                [searchType]=\"searchType\"\n                (searchInfoEmitter)=\"searchInfoEmitter($event)\"></app-search-form>\n    </div>\n    <div class=\"ui-grid-row\">\n        <app-malfunction-table\n                #child\n                [searchType]=\"searchType\"\n                [eventDatas]=\"eventDatas\"\n        ></app-malfunction-table>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-receive/malfunction-receive.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".title {\n  font-size: 16px;\n  font-weight: bold; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-receive/malfunction-receive.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionReceiveComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionReceiveComponent = (function () {
    function MalfunctionReceiveComponent(eventBusService, maintenanceServicen) {
        this.eventBusService = eventBusService;
        this.maintenanceServicen = maintenanceServicen;
        this.searchType = '待接受'; // 搜索类型
    }
    MalfunctionReceiveComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.maintenanceServicen.getFlowChart().subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionReceiveComponent.prototype.searchInfoEmitter = function (event) {
        this.eventDatas = event;
    };
    MalfunctionReceiveComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-receive',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-receive/malfunction-receive.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-receive/malfunction-receive.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */]])
    ], MalfunctionReceiveComponent);
    return MalfunctionReceiveComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solve/malfunction-solve.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"title\">故障解决</span>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"malfunctionOverview\" >\n    <div class=\"ui-grid-row\">\n        <app-search-form\n                [searchType]=\"searchType\"\n                (searchInfoEmitter)=\"searchInfoEmitter($event)\"></app-search-form>\n    </div>\n    <div class=\"ui-grid-row\">\n        <app-malfunction-table\n                #child\n                [searchType]=\"searchType\"\n                [eventDatas]=\"eventDatas\"\n        ></app-malfunction-table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solve/malfunction-solve.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".title {\n  font-size: 16px;\n  font-weight: bold; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solve/malfunction-solve.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionSolveComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionSolveComponent = (function () {
    function MalfunctionSolveComponent(eventBusService, maintenanceServicen) {
        this.eventBusService = eventBusService;
        this.maintenanceServicen = maintenanceServicen;
        this.searchType = '处理中'; // 搜索类型
    }
    MalfunctionSolveComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.maintenanceServicen.getFlowChart().subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionSolveComponent.prototype.searchInfoEmitter = function (event) {
        console.log(event);
        this.eventDatas = event;
    };
    MalfunctionSolveComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-solve',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solve/malfunction-solve.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solve/malfunction-solve.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */]])
    ], MalfunctionSolveComponent);
    return MalfunctionSolveComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solved-view/malfunction-solved-view.component.html":
/***/ (function(module, exports) {

module.exports = "<p-messages [(value)]=\"msg\"></p-messages>\n<div class=\"content-section implementation GridDemo\" id=\"MalfunctionSolved\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>查看解决故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-colse-base-view></app-colse-base-view>\n    </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solved-view/malfunction-solved-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionSolved {\n  font-size: 14px; }\n  #MalfunctionSolved div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionSolved div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionSolved form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-1 {\n    width: 10.5%; }\n  .col-sm-3 {\n    width: 22%; }\n  .col-sm-11 {\n    width: 88.666667%; }\n  .col-sm-10 {\n    width: 78.333333%; }\n  .col-sm-5 {\n    width: 38.666667%; }\n  .col-sm-6 {\n    width: 56%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solved-view/malfunction-solved-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionSolvedViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MalfunctionSolvedViewComponent = (function () {
    function MalfunctionSolvedViewComponent(router, activatedRouter, eventBusService) {
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.eventBusService = eventBusService;
    }
    MalfunctionSolvedViewComponent.prototype.ngOnInit = function () {
    };
    MalfunctionSolvedViewComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionSolvedViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-solved-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solved-view/malfunction-solved-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solved-view/malfunction-solved-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_2__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionSolvedViewComponent);
    return MalfunctionSolvedViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solved/malfunction-solved.component.html":
/***/ (function(module, exports) {

module.exports = "<p-messages [(value)]=\"msg\"></p-messages>\n<div class=\"content-section implementation GridDemo\" id=\"MalfunctionSolved\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>解决故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                   (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>解决人组织：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <!--<div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">-->\n                    <input type=\"text\" pInputText\n                           readonly\n                           [(ngModel)]=\"formObj.acceptor_org\"\n                           formControlName=\"solveOrgName\"\n                           class=\" form-control cursor_not_allowed\"\n                    />\n                    <!--<div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"solveOrgName\">-->\n                        <!--<i class=\"fa fa-close\"></i>-->\n                        <!--不能为空-->\n                    <!--</div>-->\n                <!--</div>-->\n                <!--<div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">-->\n                    <!--<button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog('department')\" label=\"选择\"></button>-->\n                <!--</div>-->\n                <!--<div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">-->\n                    <!--<button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"clearTreeDialog('department')\" label=\"清空\"></button>-->\n                <!--</div>-->\n            </div>\n            <label class=\"col-sm-2 control-label\">解决人：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <!--<p-dropdown [options]=\"options['departments']\"-->\n                            <!--[(ngModel)]=\"formObj.solve_per_name\"-->\n                            <!--optionLabel=\"name\"-->\n                            <!--(onChange)=\"onSelectedDepartment($event)\"-->\n                            <!--[ngModelOptions]=\"{standalone: true}\"-->\n                            <!--[style]=\"{'width':'100%'}\"-->\n                <!--&gt;</p-dropdown>-->\n                <input type=\"text\" pInputText\n                       readonly\n                       [(ngModel)]=\"formObj.acceptor\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       class=\" form-control cursor_not_allowed\"\n                />\n            </div>\n        </div>\n\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                开始处理时间:\n            </label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <input type=\"text\" pInputText  name=\"department\" placeholder=\"自动生成\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"formObj.process_time\" readonly class=\"form-control cursor_not_allowed\"/>\n            </div>\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                完成处理时间:\n            </label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-calendar [(ngModel)]=\"formObj.finish_time\"\n                            [showIcon]=\"true\"\n                            [locale]=\"zh\"\n                            name=\"end_time\"\n                            formControlName=\"finishTime\"\n                            dateFormat=\"yy-mm-dd\"\n                            [showTime]=\"true\">\n                </p-calendar>\n                <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"finishTime\">\n                    <i class=\"fa fa-close\"></i>\n                    不能为空\n                </div>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                故障原因：\n            </label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px \">\n                <input type=\"text\" pInputText\n                       class=\" form-control\"\n                       formControlName=\"reason\"\n                       [(ngModel)]=\"formObj.reason\"/>\n                <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"reason\">\n                    <i class=\"fa fa-close\"></i>\n                    不能为空\n                </div>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                解决方法：\n            </label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px \">\n                  <textarea [rows]=\"5\" pInputTextarea autoResize=\"autoResize\"\n                            class=\"form-control\"\n                            formControlName=\"means\"\n                            [(ngModel)]=\"formObj.means\"></textarea>\n                <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"means\">\n                    <i class=\"fa fa-close\"></i>\n                    不能为空\n                </div>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button class=\"pull-right  ui-button-secondary ui-margin-right-10px\"  pButton type=\"button\" (click)=\"goBack()\" label=\"取消\"></button>\n                <button class=\"pull-right ui-margin-right-10px\"  pButton type=\"button\" (click)=\"solved()\" label=\"解决\"></button>\n                <button class=\"pull-right ui-margin-right-10px\"  pButton type=\"button\" (click)=\"save()\" label=\"保存\"></button>\n            </div>\n        </div>\n    </form>\n\n    <p-dialog header=\"请选择\" [(visible)]=\"display\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <p-tree [value]=\"filesTree4\"\n                selectionMode=\"single\"\n                [(selection)]=\"selected\"\n                (onNodeExpand)=\"nodeExpand($event)\"\n        ></p-tree>\n        <div style=\"margin-top:8px\">Selected Node: {{selected ? selected.label : 'none'}}</div>\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"display=false\"\n                    class=\"ui-button-secondary\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"message\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solved/malfunction-solved.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionSolved {\n  font-size: 14px; }\n  #MalfunctionSolved div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionSolved div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionSolved form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-solved/malfunction-solved.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionSolvedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MalfunctionSolvedComponent = (function () {
    function MalfunctionSolvedComponent(maintenanceService, router, fb, publicService, activatedRouter, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.fb = fb;
        this.publicService = publicService;
        this.activatedRouter = activatedRouter;
        this.eventBusService = eventBusService;
        this.options = {
            '来源': [],
            '影响度': [],
            '紧急度': [],
            '故障级别': [],
            '所属系统': [],
            'departments': [],
        };
        this.defaultOption = {
            'solve_per_name': null
        };
        this.display = false;
        this.filesTree4 = [];
        this.selected = [];
        this.message = []; // 交互提示弹出框
    }
    MalfunctionSolvedComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.zh = new __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */]().initZh();
        this.formObj = new __WEBPACK_IMPORTED_MODULE_6__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.formObj.accept_time = __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(this.activatedRouter.snapshot.paramMap.get('time'));
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.getTrickMessage(this.formObj.sid);
        this.getDepPerson();
        this.initForm();
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionSolvedComponent.prototype.getTrickMessage = function (sid) {
        var _this = this;
        this.maintenanceService.getTrick(sid).subscribe(function (res) {
            _this.formObj.acceptor = res['acceptor'];
            _this.formObj.acceptor_org = res['acceptor_org'];
            _this.formObj.solve_per_name = res['acceptor'];
            _this.formObj.solve_per = res['acceptor_pid'];
            _this.formObj.solve_org_name = res['acceptor_org'];
            _this.formObj.solve_org = res['acceptor_org_oid'];
        });
    };
    MalfunctionSolvedComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionSolvedComponent.prototype.getDepPerson = function (oid) {
        var _this = this;
        this.publicService.getApprovers(oid).subscribe(function (res) {
            if (!res) {
                _this.options['departments'] = [];
            }
            else {
                _this.options['departments'] = res;
                _this.formObj.solve_per = res[0]['pid'];
                _this.formObj.solve_per_name = res[0]['name'];
            }
        });
    };
    MalfunctionSolvedComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            'means': '',
            'reason': '',
            'solveOrgName': '',
            'finishTime': ''
        });
    };
    MalfunctionSolvedComponent.prototype.showTreeDialog = function (opType) {
        var _this = this;
        switch (opType) {
            case 'department':
                this.display = true;
                this.publicService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
        }
    };
    MalfunctionSolvedComponent.prototype.clearTreeDialog = function (opType) {
        switch (opType) {
            case 'submitter':
                this.formObj.submitter = '';
                this.formObj.submitter_pid = '';
                this.formObj.submitter_org_oid = '';
                this.formObj.submitter_org = '';
                this.formObj.submitter_phone = '';
                break;
            case 'department':
                this.formObj.solve_per = '';
                this.formObj.solve_per_name = '';
                this.formObj.solve_org = '';
                this.formObj.solve_org_name = '';
                break;
        }
    };
    MalfunctionSolvedComponent.prototype.closeTreeDialog = function () {
        this.display = false;
        this.formObj.solve_org_name = this.selected['label'];
        this.formObj.solve_org = this.selected['oid'];
        this.getDepPerson(this.selected['oid']);
    };
    MalfunctionSolvedComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    Object.defineProperty(MalfunctionSolvedComponent.prototype, "means", {
        get: function () {
            return this.myForm.controls['means'].untouched && this.myForm.controls['means'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionSolvedComponent.prototype, "reason", {
        get: function () {
            return this.myForm.controls['reason'].untouched && this.myForm.controls['reason'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionSolvedComponent.prototype, "solveOrgName", {
        get: function () {
            return this.myForm.controls['solveOrgName'].untouched && this.myForm.controls['solveOrgName'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MalfunctionSolvedComponent.prototype, "finishTime", {
        get: function () {
            return this.myForm.controls['finishTime'].untouched && this.myForm.controls['finishTime'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    MalfunctionSolvedComponent.prototype.onSelectedDepartment = function (event) {
        var msg = this.formObj.acceptor;
        this.formObj.solve_per_name = msg['name'];
        this.formObj.solve_per = msg['pid'];
    };
    MalfunctionSolvedComponent.prototype.save = function () {
        this.addSelectedName();
        this.filterOptionName();
        this.formObj.status = '';
        this.submitPost(this.formObj);
    };
    MalfunctionSolvedComponent.prototype.addSelectedName = function () {
        (!this.formObj.solve_per_name) && (this.formObj.solve_per_name = this.defaultOption['solve_per_name']);
    };
    MalfunctionSolvedComponent.prototype.filterOptionName = function () {
        if (this.formObj.solve_per_name['name']) {
            this.formObj.solve_per_name = this.formObj.solve_per_name['name'];
        }
    };
    MalfunctionSolvedComponent.prototype.showError = function () {
        this.message = [];
        this.message.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    MalfunctionSolvedComponent.prototype.submitPost = function (paper) {
        var _this = this;
        this.maintenanceService.solveTrick(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionSolvedComponent.prototype.setValidators = function () {
        this.myForm.controls['solveOrgName'].setValidators([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required]);
        this.myForm.controls['reason'].setValidators([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required]);
        this.myForm.controls['means'].setValidators([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required]);
        this.myForm.controls['finishTime'].setValidators([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required]);
        this.myForm.controls['solveOrgName'].updateValueAndValidity();
        this.myForm.controls['reason'].updateValueAndValidity();
        this.myForm.controls['means'].updateValueAndValidity();
        this.myForm.controls['finishTime'].updateValueAndValidity();
    };
    MalfunctionSolvedComponent.prototype.solved = function () {
        if (!this.formObj.solve_org_name
            || !this.formObj.reason
            || !this.formObj.means) {
            this.setValidators();
            this.showError();
        }
        else {
            this.formObj.status = '已解决';
            this.submitPost(this.formObj);
        }
    };
    MalfunctionSolvedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-solved',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solved/malfunction-solved.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-solved/malfunction-solved.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_5__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionSolvedComponent);
    return MalfunctionSolvedComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-turn/malfunction-turn.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionTurn\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>转派故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-simple-view></app-simple-view>\n        <app-simple-time></app-simple-time>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label \">\n                <span ngClass=\"start_red\">*</span>\n                转派原因：\n            </label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"options['reassign_reason']\"\n                            [(ngModel)]=\"formObj.reassign_reason\"\n                            optionLabel=\"name\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"></p-dropdown>\n            </div>\n            <label class=\"col-sm-2 control-label \">转派时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <input type=\"text\" pInputText  name=\"department\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"formObj.reassign_time\" readonly\n                       class=\"form-control cursor_not_allowed\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>转派至部门：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           readonly\n                           [(ngModel)]=\"formObj.processor_org\"\n                           formControlName=\"processor_org\"\n                           class=\" form-control cursor_not_allowed\"\n                    />\n                    <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"acceptor\">\n                        <i class=\"fa fa-close\"></i>\n                        不能为空\n                    </div>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog('department')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"clearTreeDialog('department')\" label=\"清空\"></button>\n                </div>\n            </div>\n            <label class=\"col-sm-2 control-label\">转派至个人：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"options['processor']\"\n                            [(ngModel)]=\"formObj.processor\"\n                            optionLabel=\"name\"\n                            (onChange)=\"onSelectedDepartment($event)\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button class=\"pull-right  ui-button-secondary ui-margin-right-10px\"  pButton type=\"button\" (click)=\"goBack()\" label=\"取消\"></button>\n                <button class=\"pull-right ui-margin-right-10px\"  pButton type=\"button\" (click)=\"submitTrick()\" label=\"提交\"></button>\n            </div>\n        </div>\n    </form>\n    <p-dialog header=\"请选择\" [(visible)]=\"display\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <p-tree [value]=\"filesTree4\"\n                selectionMode=\"single\"\n                [(selection)]=\"selected\"\n                (onNodeExpand)=\"nodeExpand($event)\"\n        ></p-tree>\n        <!--<div style=\"margin-top:8px\">Selected Node: {{selected ? selected.label : 'none'}}</div>-->\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"display=false\"\n                    class=\"ui-button-secondary\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"message\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-turn/malfunction-turn.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionTurn {\n  font-size: 14px; }\n  #MalfunctionTurn div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionTurn div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionTurn form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-turn/malfunction-turn.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionTurnComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MalfunctionTurnComponent = (function () {
    function MalfunctionTurnComponent(maintenanceService, router, fb, storageService, publicService, activatedRouter, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.fb = fb;
        this.storageService = storageService;
        this.publicService = publicService;
        this.activatedRouter = activatedRouter;
        this.eventBusService = eventBusService;
        this.options = {
            'reassign_reason': [],
            'processor': []
        };
        this.defaultOption = {
            'upgrade_reason': null,
            'processor': null
        };
        this.display = false;
        this.filesTree4 = [];
        this.selected = [];
        this.message = []; // 交互提示弹出框
    }
    MalfunctionTurnComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formObj = new __WEBPACK_IMPORTED_MODULE_7__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.getDepPerson();
        this.initOption();
        this.formObj.reassign_reason = this.options['reassign_reason'][0]['name'];
        this.formObj.reassign_time = __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(new Date());
        this.initForm();
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionTurnComponent.prototype.initOption = function () {
        this.options['reassign_reason'] = [
            { name: '分派错误', code: 'fpcw' },
            { name: '工作交接', code: 'gzjj' }
        ];
    };
    MalfunctionTurnComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionTurnComponent.prototype.getDepPerson = function (oid) {
        var _this = this;
        this.publicService.getApprovers(oid).subscribe(function (res) {
            if (!res) {
                _this.options['processor'] = [];
            }
            else {
                _this.options['processor'] = res;
                _this.formObj.processor_pid = res[0]['pid'];
                _this.formObj.processor = res[0]['name'];
            }
        });
    };
    MalfunctionTurnComponent.prototype.showTreeDialog = function (opType) {
        var _this = this;
        switch (opType) {
            case 'department':
                this.display = true;
                this.publicService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
        }
    };
    MalfunctionTurnComponent.prototype.clearTreeDialog = function (opType) {
        switch (opType) {
            case 'department':
                this.formObj.processor = '';
                this.formObj.processor_pid = '';
                this.formObj.processor_org = '';
                this.formObj.processor_org_oid = '';
                break;
        }
    };
    MalfunctionTurnComponent.prototype.closeTreeDialog = function () {
        this.display = false;
        this.formObj.processor_org = this.selected['label'];
        this.formObj.processor_org_oid = this.selected['oid'];
        this.getDepPerson(this.selected['oid']);
    };
    MalfunctionTurnComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    MalfunctionTurnComponent.prototype.addSelectedName = function () {
        (!this.formObj.upgrade_reason) && (this.formObj.upgrade_reason = this.defaultOption['upgrade_reason']);
        (!this.formObj.processor) && (this.formObj.processor = this.defaultOption['processor']);
    };
    MalfunctionTurnComponent.prototype.filterOptionName = function () {
        if (this.formObj.processor['name']) {
            this.formObj.processor = this.formObj.processor['name'];
        }
        if (this.formObj.reassign_reason['name']) {
            this.formObj.reassign_reason = this.formObj.reassign_reason['name'];
        }
    };
    Object.defineProperty(MalfunctionTurnComponent.prototype, "acceptor", {
        get: function () {
            // console.log('untouched', this.myForm.controls['content'].untouched);
            // console.log('required', this.myForm.controls['content'].hasError('required'));
            return this.myForm.controls['processor_org'].untouched && this.myForm.controls['processor_org'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    MalfunctionTurnComponent.prototype.setValidators = function () {
        this.myForm.controls['processor_org'].setValidators([__WEBPACK_IMPORTED_MODULE_8__angular_forms__["Validators"].required]);
        this.myForm.controls['processor_org'].updateValueAndValidity();
    };
    MalfunctionTurnComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            'processor_org': '',
        });
    };
    MalfunctionTurnComponent.prototype.submitTrick = function () {
        this.addSelectedName();
        this.filterOptionName();
        console.log(this.formObj);
        if (!this.formObj.processor_org) {
            this.showError();
            this.setValidators();
        }
        else {
            this.turnPost(this.formObj);
        }
    };
    MalfunctionTurnComponent.prototype.showError = function () {
        this.message = [];
        this.message.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    MalfunctionTurnComponent.prototype.onSelectedDepartment = function (event) {
        var msg = this.formObj.processor;
        this.formObj.processor = msg['name'];
        this.formObj.processor_pid = msg['pid'];
    };
    MalfunctionTurnComponent.prototype.turnPost = function (paper) {
        var _this = this;
        this.maintenanceService.turnTrick(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionTurnComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-turn',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-turn/malfunction-turn.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-turn/malfunction-turn.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_8__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_4__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_6__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionTurnComponent);
    return MalfunctionTurnComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-up/malfunction-up.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionUp\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>升级故障单</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n        <app-base-view></app-base-view>\n        <app-simple-view></app-simple-view>\n        <app-simple-time></app-simple-time>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label \">\n                <span ngClass=\"start_red\">*</span>\n                升级原因：\n            </label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"options['upgrade_reason']\"\n                            [(ngModel)]=\"formObj.upgrade_reason\"\n                            (onChange)=\"switchReason($event)\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"></p-dropdown>\n            </div>\n            <label class=\"col-sm-2 control-label \">升级时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <input type=\"text\" pInputText  name=\"department\"\n                       [ngModelOptions]=\"{standalone: true}\"\n                        [(ngModel)]=\"formObj.upgrade_time\" readonly\n                       class=\"form-control cursor_not_allowed\"/>\n            </div>\n        </div>\n        <div class=\"form-group\" *ngIf=\"dispalyDep\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>分配至部门：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <div class=\"col-sm-6 ui-fluid ui-fluid-no-padding\">\n                    <input type=\"text\" pInputText\n                           readonly\n                           [(ngModel)]=\"formObj.processor_org\"\n                           formControlName=\"processor_org\"\n                           class=\" form-control cursor_not_allowed\"\n                    />\n                    <div class=\"ui-message ui-messages-error ui-corner-all\" *ngIf=\"acceptor\">\n                        <i class=\"fa fa-close\"></i>\n                        不能为空\n                    </div>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog('department')\" label=\"选择\"></button>\n                </div>\n                <div class=\"col-sm-3 ui-fluid-no-padding ui-padding-10px\">\n                    <button  ngClass=\"ui-g-12 ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"clearTreeDialog('department')\" label=\"清空\"></button>\n                </div>\n            </div>\n            <label class=\"col-sm-2 control-label\">分配至个人：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"options['departments']\"\n                            [(ngModel)]=\"formObj.processor\"\n                            (onChange)=\"onChange($event)\"\n                            placeholder=\"{{formObj.processor}}\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <div class=\"col-sm-12 ui-no-padding-left-15px ui-no-padding-right-15px\">\n                <button  class=\"pull-right  ui-button-secondary ui-margin-right-10px\" pButton type=\"button\" (click)=\"goBack()\" label=\"取消\" ></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submitTrick()\" label=\"提交\"></button>\n            </div>\n        </div>\n    </form>\n    <p-dialog header=\"请选择\" [(visible)]=\"display\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <p-tree [value]=\"filesTree4\"\n                selectionMode=\"single\"\n                [(selection)]=\"selected\"\n                (onNodeExpand)=\"nodeExpand($event)\"\n        ></p-tree>\n        <div style=\"margin-top:8px\">Selected Node: {{selected ? selected.label : 'none'}}</div>\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"display=false\"\n                    class=\"ui-button-secondary\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"message\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-up/malfunction-up.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionUp {\n  font-size: 14px; }\n  #MalfunctionUp div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionUp div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionUp form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-up/malfunction-up.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionUpComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MalfunctionUpComponent = (function () {
    function MalfunctionUpComponent(maintenanceService, router, fb, publicService, activatedRouter, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.fb = fb;
        this.publicService = publicService;
        this.activatedRouter = activatedRouter;
        this.eventBusService = eventBusService;
        this.options = {
            'upgrade_reason': []
        };
        this.defaultOption = {
            'upgrade_reason': null
        };
        this.display = false;
        this.filesTree4 = [];
        this.selected = [];
        this.message = []; // 交互提示弹出框
        this.dispalyDep = false;
    }
    MalfunctionUpComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formObj = new __WEBPACK_IMPORTED_MODULE_6__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.getDepPerson();
        this.initOption();
        this.formObj.upgrade_reason = this.options['upgrade_reason'][0]['value'];
        this.formObj.upgrade_time = __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(new Date());
        this.initForm();
        this.formObj.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionUpComponent.prototype.initOption = function () {
        this.options['upgrade_reason'] = [
            { label: '资源升级', value: '资源升级' },
            { label: '技术升级', value: '技术升级' }
        ];
        this.formObj.upgrade_reason = '资源升级';
    };
    MalfunctionUpComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionUpComponent.prototype.getDepPerson = function (oid) {
        var _this = this;
        this.publicService.getApprovers(oid).subscribe(function (res) {
            if (!res) {
                _this.options['departments'] = [];
            }
            else {
                var newArray = __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateDepDropDown(res);
                _this.options['departments'] = newArray;
                _this.formObj.processor_pid = newArray[0]['value']['pid'];
                _this.formObj.processor = newArray[0]['value']['name'];
            }
        });
    };
    MalfunctionUpComponent.prototype.showTreeDialog = function (opType) {
        var _this = this;
        switch (opType) {
            case 'department':
                this.display = true;
                this.publicService.getDepartmentDatas().subscribe(function (res) {
                    _this.filesTree4 = res;
                });
        }
    };
    MalfunctionUpComponent.prototype.clearTreeDialog = function (opType) {
        switch (opType) {
            case 'department':
                this.formObj.processor = '';
                this.formObj.processor_pid = '';
                this.formObj.processor_org = '';
                this.formObj.processor_org_oid = '';
                break;
        }
    };
    MalfunctionUpComponent.prototype.closeTreeDialog = function () {
        this.display = false;
        this.formObj.processor_org = this.selected['label'];
        this.formObj.processor_org_oid = this.selected['oid'];
        this.getDepPerson(this.selected['oid']);
    };
    MalfunctionUpComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    MalfunctionUpComponent.prototype.addSelectedName = function () {
        (!this.formObj.upgrade_reason) && (this.formObj.upgrade_reason = this.defaultOption['upgrade_reason']);
        // (!this.formObj.processor) && (this.formObj.processor = this.defaultOption['processor']);
    };
    MalfunctionUpComponent.prototype.filterOptionName = function () {
        if (this.formObj.processor['name']) {
            this.formObj.processor = this.formObj.processor['name'];
        }
        if (this.formObj.upgrade_reason['value']) {
            this.formObj.upgrade_reason = this.formObj.upgrade_reason['value'];
        }
    };
    Object.defineProperty(MalfunctionUpComponent.prototype, "acceptor", {
        get: function () {
            // console.log('untouched', this.myForm.controls['content'].untouched);
            // console.log('required', this.myForm.controls['content'].hasError('required'));
            return this.myForm.controls['processor_org'].untouched && this.myForm.controls['processor_org'].hasError('required');
        },
        enumerable: true,
        configurable: true
    });
    MalfunctionUpComponent.prototype.setValidators = function () {
        this.myForm.controls['processor_org'].setValidators([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required]);
        this.myForm.controls['processor_org'].updateValueAndValidity();
    };
    MalfunctionUpComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            'processor_org': '',
        });
    };
    MalfunctionUpComponent.prototype.onChange = function (event) {
        this.formObj.processor = event['value']['name'];
        this.formObj.processor_pid = event['value']['pid'];
    };
    MalfunctionUpComponent.prototype.submitTrick = function () {
        this.addSelectedName();
        this.filterOptionName();
        if (!this.formObj.processor_org && this.formObj.upgrade_reason === '技术升级') {
            this.showError();
            this.setValidators();
        }
        else {
            if (this.formObj.upgrade_reason === '资源升级') {
                this.formObj.processor = '';
                this.formObj.processor_pid = '';
                this.formObj.processor_org = '';
                this.formObj.processor_org_oid = '';
            }
            this.upPost(this.formObj);
        }
    };
    MalfunctionUpComponent.prototype.showError = function () {
        this.message = [];
        this.message.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    MalfunctionUpComponent.prototype.switchReason = function (event) {
        (event['value'] === '技术升级') && (this.dispalyDep = true);
        (event['value'] === '资源升级') && (this.dispalyDep = false);
    };
    MalfunctionUpComponent.prototype.upPost = function (paper) {
        var _this = this;
        this.maintenanceService.upTrick(paper).subscribe(function (res) {
            if (res === '00000') {
                _this.message = [];
                _this.message.push({ severity: 'success', summary: '消息提示', detail: '操作成功' });
                window.setTimeout(function () {
                    _this.goBack();
                }, 1100);
            }
            else {
                _this.message = [];
                _this.message.push({ severity: 'error', summary: '消息提示', detail: "\u64CD\u4F5C\u5931\u8D25" + res });
            }
        });
    };
    MalfunctionUpComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-up',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-up/malfunction-up.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-up/malfunction-up.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_5__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionUpComponent);
    return MalfunctionUpComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-view/malfunction-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section implementation GridDemo\" id=\"MalfunctionView\" >\r\n    <div class=\"title col-sm-12\">\r\n        <div class=\"col-sm-1\">\r\n            <span>查看故障单</span>\r\n        </div>\r\n        <div class=\"col-sm-11 pull-right\">\r\n            <button type=\"button\"\r\n                    class=\"pull-right\"\r\n                    pButton\r\n                    (click)=\"goBack()\"\r\n                    icon=\"fa-close\"\r\n                    style=\"width: 30px\"></button>\r\n        </div>\r\n    </div>\r\n    <form class=\"form-horizontal\">\r\n        <app-base-view></app-base-view>\r\n        <app-simple-view></app-simple-view>\r\n    </form>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-view/malfunction-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionView {\n  font-size: 14px; }\n  #MalfunctionView div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionView div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionView form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-1 {\n    width: 10.5%; }\n  .col-sm-3 {\n    width: 22%; }\n  .col-sm-11 {\n    width: 88.666667%; }\n  .col-sm-10 {\n    width: 78.333333%; }\n  .col-sm-5 {\n    width: 38.666667%; }\n  .col-sm-6 {\n    width: 56%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/malfunction-view/malfunction-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MalfunctionViewComponent = (function () {
    function MalfunctionViewComponent(activatedRouter, router, maintenanceService, eventBusService) {
        this.activatedRouter = activatedRouter;
        this.router = router;
        this.maintenanceService = maintenanceService;
        this.eventBusService = eventBusService;
    }
    MalfunctionViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.maintenanceService.getFlowChart(this.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    MalfunctionViewComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    MalfunctionViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-view/malfunction-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/malfunction-view/malfunction-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], MalfunctionViewComponent);
    return MalfunctionViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/base-view/base-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">故障单号：</label>\n    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.sid\"/>\n    </div>\n</div>\n<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">来源：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.fromx\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">状态：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.status\"/>\n    </div>\n</div>\n<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">报障人：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\"\n               class=\" form-control cursor_not_allowed\"\n               pInputText\n               placeholder=\"自动生成\"\n               [(ngModel)]=\"formObj.submitter\"\n               readonly />\n    </div>\n    <label class=\"col-sm-2 control-label\">组织：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\"\n               class=\" form-control cursor_not_allowed\"\n               pInputText\n               placeholder=\"自动生成\"\n               [(ngModel)]=\"formObj.submitter_org\"\n               readonly />\n    </div>\n</div>\n<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">联系电话：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.submitter_phone\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">受理时间：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.create_time\"/>\n    </div>\n</div>\n<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">发生时间：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.occurrence_time\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">最终期限：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.deadline\"/>\n    </div>\n</div>\n<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">影响度：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.influence\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">紧急度：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.urgency\"/>\n    </div>\n\n</div>\n<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">优先级：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.priority\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">故障级别：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.level\"/>\n    </div>\n</div>\n<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">所属系统：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.bt_system\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">发生地点：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.addr\"/>\n    </div>\n</div>\n<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">故障标题：</label>\n    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.title\"/>\n    </div>\n</div>\n<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">故障描述：</label>\n    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n         <textarea [rows]=\"5\" pInputTextarea\n                   autoResize=\"autoResize\"\n                   readonly\n                   class=\"form-control\"\n                   [(ngModel)]=\"formObj.content\"></textarea>\n    </div>\n</div>\n<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">\n        附件：\n    </label>\n    <div class=\"col-sm-9 ui-fluid-no-padding \">\n        <div *ngFor=\"let file of formObj.attachments\">\n            <app-upload-file-view [file]=\"file\"></app-upload-file-view>\n        </div>\n    </div>\n</div>\n<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">\n        关联服务目录：\n    </label>\n    <div class=\"col-sm-10 ui-no-padding-left-15px\">\n        <input type=\"text\" pInputText\n               name=\"department\" placeholder=\"请选择关联服务目录\"\n               readonly\n               [(ngModel)]=\"formObj.service\"\n               class=\"form-control cursor_not_allowed\"/>\n    </div>\n</div>\n<div class=\"form-group \">\n    <label  class=\"col-sm-2 control-label\">\n        关联设备编号：\n    </label>\n    <div class=\"col-sm-10 ui-no-padding-left-15px\">\n        <input type=\"text\" pInputText\n               name=\"department\" placeholder=\"请选择关联设备编号\"\n               readonly\n               [(ngModel)]=\"formObj.devices\"\n               class=\"form-control cursor_not_allowed\"/>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/base-view/base-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/base-view/base-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BaseViewComponent = (function () {
    function BaseViewComponent(maintenanceService, eventBusService, activatedRouter) {
        this.maintenanceService = maintenanceService;
        this.eventBusService = eventBusService;
        this.activatedRouter = activatedRouter;
    }
    BaseViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formObj = new __WEBPACK_IMPORTED_MODULE_2__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.formObj.status = this.activatedRouter.snapshot.paramMap.get('status');
        this.initTrick(this.sid);
        this.maintenanceService.getFlowChart(this.formObj.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
    };
    BaseViewComponent.prototype.initTrick = function (s) {
        var _this = this;
        this.maintenanceService.getTrick(s).subscribe(function (res) {
            if (res) {
                _this.formObj.sid = res['sid'];
                _this.formObj.fromx = res['fromx'];
                _this.formObj.status = res['status'];
                _this.formObj.submitter = res['submitter'];
                _this.formObj.submitter_org = res['submitter_org'];
                _this.formObj.submitter_phone = res['submitter_phone'];
                _this.formObj.create_time = res['create_time'];
                _this.formObj.occurrence_time = res['occurrence_time'];
                _this.formObj.deadline = res['deadline'];
                _this.formObj.influence = res['influence'];
                _this.formObj.urgency = res['urgency'];
                _this.formObj.priority = res['priority'];
                _this.formObj.level = res['level'];
                _this.formObj.bt_system = res['bt_system'];
                _this.formObj.addr = res['addr'];
                _this.formObj.title = res['title'];
                _this.formObj.content = res['content'];
                _this.formObj.attachments = res['attachments'];
                _this.formObj.service = res['service'];
                _this.formObj.devices = res['devices'];
            }
        });
    };
    BaseViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-base-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/base-view/base-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/base-view/base-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"]])
    ], BaseViewComponent);
    return BaseViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-deal/btn-deal.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-deal/btn-deal.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"  (click)=\"deal(data)\" label=\"处理\"\n        *ngIf=\"data.status == '待处理'\"\n       ></button>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-deal/btn-deal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnDealComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BtnDealComponent = (function () {
    function BtnDealComponent(maintenanceService, confirmationService, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.confirmationService = confirmationService;
        this.eventBusService = eventBusService;
        this.dataEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.fromSource = {};
    }
    BtnDealComponent.prototype.ngOnInit = function () {
        this.dialogDisplay = false;
    };
    BtnDealComponent.prototype.deal = function (data) {
        var _this = this;
        this.dialogDisplay = true;
        this.fromSource = {
            'sid': data['sid'],
            'solve_per': data['solve_per'],
            'solve_per_pid': data['pid'],
            'solve_org': data['solve_org'],
            'solve_org_oid': data['oid'],
            'reason': data['reason'],
            'means': data['means'],
            'finish_time': data['finish_time']
        };
        this.maintenanceService.getFlowChart(data.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
        this.confirmationService.confirm({
            message: '确认处理吗？',
            accept: function () {
                _this.sure();
            }
        });
    };
    BtnDealComponent.prototype.sure = function () {
        var _this = this;
        this.maintenanceService.dealTrick(this.fromSource).subscribe(function (res) {
            _this.dataEmitter.emit(res);
            _this.dialogDisplay = false;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnDealComponent.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], BtnDealComponent.prototype, "dataEmitter", void 0);
    BtnDealComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-deal',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-deal/btn-deal.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-deal/btn-deal.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], BtnDealComponent);
    return BtnDealComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-delete/btn-delete.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-delete/btn-delete.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"  (click)=\"delete(data)\" label=\"删除\"\n        *ngIf=\"data.status == '新建' || data.status == '待分配' || data.status == '待接受'\"></button>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-delete/btn-delete.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnDeleteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BtnDeleteComponent = (function () {
    function BtnDeleteComponent(maintenanceService, confirmationService, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.confirmationService = confirmationService;
        this.eventBusService = eventBusService;
        this.dataEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.sidArray = [];
    }
    BtnDeleteComponent.prototype.ngOnInit = function () {
        this.dialogDisplay = false;
    };
    BtnDeleteComponent.prototype.delete = function (data) {
        var _this = this;
        this.sidArray.length = 0;
        this.dialogDisplay = true;
        this.sidArray[this.sidArray.length] = data['sid'];
        this.maintenanceService.getFlowChart(data.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: function () {
                _this.sure();
            }
        });
    };
    BtnDeleteComponent.prototype.sure = function () {
        var _this = this;
        this.maintenanceService.deleteTrick(this.sidArray).subscribe(function (res) {
            _this.dataEmitter.emit(res);
            _this.dialogDisplay = false;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnDeleteComponent.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], BtnDeleteComponent.prototype, "dataEmitter", void 0);
    BtnDeleteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-delete',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-delete/btn-delete.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-delete/btn-delete.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], BtnDeleteComponent);
    return BtnDeleteComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-distribute/btn-distribute.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"ditribute()\"\n        label=\"分配\"\n        [routerLink]=\"['/index/maintenance/malfunctionBase/malfunctionChange', { id: data.sid, status: data.status} ]\"\n        *ngIf=\"data.status == '待分配'\"></button>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-distribute/btn-distribute.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-distribute/btn-distribute.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnDistributeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BtnDistributeComponent = (function () {
    function BtnDistributeComponent() {
    }
    BtnDistributeComponent.prototype.ngOnInit = function () {
    };
    BtnDistributeComponent.prototype.ditribute = function () {
        console.log(this.data);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnDistributeComponent.prototype, "data", void 0);
    BtnDistributeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-distribute',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-distribute/btn-distribute.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-distribute/btn-distribute.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BtnDistributeComponent);
    return BtnDistributeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-down/btn-down.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"solve(data)\"\n        label=\"转派\"\n        [routerLink]=\"['/index/maintenance/malfunctionBase/malfunctionTurn', { id: data.sid, status: data.status} ]\"\n        *ngIf=\"data.status == '待处理'\"\n></button>\n\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-down/btn-down.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-down/btn-down.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnDownComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BtnDownComponent = (function () {
    function BtnDownComponent() {
    }
    BtnDownComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnDownComponent.prototype, "data", void 0);
    BtnDownComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-down',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-down/btn-down.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-down/btn-down.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BtnDownComponent);
    return BtnDownComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-edit/btn-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"jumper()\" label=\"编辑\"\n        *ngIf=\"data.status == '新建' || data.status == '待分配' || data.status == '待接受'\"></button>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-edit/btn-edit.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-edit/btn-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BtnEditComponent = (function () {
    function BtnEditComponent(router) {
        this.router = router;
    }
    BtnEditComponent.prototype.ngOnInit = function () {
    };
    BtnEditComponent.prototype.jumper = function () {
        switch (this.data.status) {
            case '新建':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionAdd', { id: this.data.sid, status: this.data.status }]);
                break;
            case '待分配':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionAdd', { id: this.data.sid, status: this.data.status }]);
                break;
            case '待接受':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionAcceptEdit', { id: this.data.sid, status: this.data.status }]);
                break;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnEditComponent.prototype, "data", void 0);
    BtnEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-edit',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-edit/btn-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-edit/btn-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]])
    ], BtnEditComponent);
    return BtnEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-off/btn-off.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"  (click)=\"off(data)\" label=\"关闭\"\n        [routerLink]=\"['/index/maintenance/malfunctionBase/malfunctionClose', { id: data.sid, status: data.status}]\"\n        *ngIf=\" data.status == '已解决'\"></button>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-off/btn-off.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-off/btn-off.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnOffComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BtnOffComponent = (function () {
    function BtnOffComponent(maintenanceService) {
        this.maintenanceService = maintenanceService;
        this.dataEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.fromSource = {};
    }
    BtnOffComponent.prototype.ngOnInit = function () {
        this.dialogDisplay = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnOffComponent.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], BtnOffComponent.prototype, "dataEmitter", void 0);
    BtnOffComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-off',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-off/btn-off.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-off/btn-off.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */]])
    ], BtnOffComponent);
    return BtnOffComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-receive/btn-receive.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-receive/btn-receive.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"  (click)=\"receive(data)\" label=\"接受\"\n        *ngIf=\"data.status == '待接受'\"\n       ></button>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-receive/btn-receive.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnReceiveComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BtnReceiveComponent = (function () {
    function BtnReceiveComponent(maintenanceService, confirmationService, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.confirmationService = confirmationService;
        this.eventBusService = eventBusService;
        this.dataEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.fromSource = {};
    }
    BtnReceiveComponent.prototype.ngOnInit = function () {
        this.dialogDisplay = false;
    };
    BtnReceiveComponent.prototype.receive = function (data) {
        var _this = this;
        this.dialogDisplay = true;
        this.fromSource = {
            'sid': data['sid'],
            'status': '待处理'
        };
        this.maintenanceService.getFlowChart(data.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
        this.confirmationService.confirm({
            message: '确认接受吗？',
            accept: function () {
                _this.sure();
            }
        });
    };
    BtnReceiveComponent.prototype.sure = function () {
        var _this = this;
        this.maintenanceService.recieveOrRejectTrick(this.fromSource).subscribe(function (res) {
            _this.dataEmitter.emit(res);
            _this.dialogDisplay = false;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnReceiveComponent.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], BtnReceiveComponent.prototype, "dataEmitter", void 0);
    BtnReceiveComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-receive',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-receive/btn-receive.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-receive/btn-receive.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], BtnReceiveComponent);
    return BtnReceiveComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-reject/btn-reject.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-reject/btn-reject.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"  (click)=\"reject(data)\" label=\"拒绝\"\n        *ngIf=\"data.status == '待接受'\"\n       ></button>\n<!---->\n<!--<p-dialog header=\"确认框\" [(visible)]=\"dialogDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">-->\n    <!--确认拒绝吗？-->\n    <!--<p-footer>-->\n        <!--<button type=\"button\" pButton icon=\"fa-check\" (click)=\"sure()\" label=\"确定\"></button>-->\n        <!--<button type=\"button\" pButton icon=\"fa-close\" (click)=\"dialogDisplay=false\" label=\"取消\"></button>-->\n    <!--</p-footer>-->\n<!--</p-dialog>-->"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-reject/btn-reject.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnRejectComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BtnRejectComponent = (function () {
    function BtnRejectComponent(maintenanceService, confirmationService, eventBusService) {
        this.maintenanceService = maintenanceService;
        this.confirmationService = confirmationService;
        this.eventBusService = eventBusService;
        this.dataEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.fromSource = {};
    }
    BtnRejectComponent.prototype.ngOnInit = function () {
        this.dialogDisplay = false;
    };
    BtnRejectComponent.prototype.reject = function (data) {
        var _this = this;
        this.dialogDisplay = true;
        this.fromSource = {
            'sid': data['sid'],
            'status': '待分配'
        };
        this.maintenanceService.getFlowChart(data.status).subscribe(function (res) {
            _this.eventBusService.maintenance.next(res);
        });
        this.confirmationService.confirm({
            message: '确认拒绝吗？',
            accept: function () {
                _this.sure();
            }
        });
    };
    BtnRejectComponent.prototype.sure = function () {
        var _this = this;
        this.maintenanceService.recieveOrRejectTrick(this.fromSource).subscribe(function (res) {
            _this.dataEmitter.emit(res);
            _this.dialogDisplay = false;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnRejectComponent.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], BtnRejectComponent.prototype, "dataEmitter", void 0);
    BtnRejectComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-reject',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-reject/btn-reject.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-reject/btn-reject.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */]])
    ], BtnRejectComponent);
    return BtnRejectComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-solve/btn-solve.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"solve(data)\"\n        label=\"解决\"\n        [routerLink]=\"['/index/maintenance/malfunctionBase/malfunctionSolved', { id: data.sid, status: data.status} ]\"\n        *ngIf=\"data.status == '处理中'\"\n        ></button>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-solve/btn-solve.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-solve/btn-solve.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnSolveComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BtnSolveComponent = (function () {
    function BtnSolveComponent(maintenanceService) {
        this.maintenanceService = maintenanceService;
        this.dataEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.fromSource = {};
    }
    BtnSolveComponent.prototype.ngOnInit = function () {
        this.dialogDisplay = false;
    };
    BtnSolveComponent.prototype.solve = function (data) {
        this.dialogDisplay = true;
        this.fromSource = {
            'sid': data['sid'],
            'solve_per': data['solve_per'],
            'solve_org': data['solve_org'],
            'reason': data['reason'],
            'means': data['means'],
            'finish_time': data['finish_time']
        };
    };
    BtnSolveComponent.prototype.sure = function () {
        var _this = this;
        this.maintenanceService.solveTrick(this.fromSource).subscribe(function (res) {
            _this.dataEmitter.emit(res);
            _this.dialogDisplay = false;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnSolveComponent.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], BtnSolveComponent.prototype, "dataEmitter", void 0);
    BtnSolveComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-solve',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-solve/btn-solve.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-solve/btn-solve.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */]])
    ], BtnSolveComponent);
    return BtnSolveComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-up/btn-up.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"solve(data)\"\n        label=\"升级\"\n        [routerLink]=\"['/index/maintenance/malfunctionBase/malfunctionUp', { id: data.sid, status: data.status} ]\"\n        *ngIf=\"data.status == '待处理'\"\n></button>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-up/btn-up.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-up/btn-up.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnUpComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BtnUpComponent = (function () {
    function BtnUpComponent() {
    }
    BtnUpComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnUpComponent.prototype, "data", void 0);
    BtnUpComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-up',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-up/btn-up.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-up/btn-up.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BtnUpComponent);
    return BtnUpComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-view/btn-view.component.html":
/***/ (function(module, exports) {

module.exports = "<button\n        pButton type=\"button\"\n        (click)=\"view(data)\"\n        label=\"查看\"\n></button>\n<!---->"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-view/btn-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/btn-view/btn-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BtnViewComponent = (function () {
    function BtnViewComponent(router) {
        this.router = router;
    }
    BtnViewComponent.prototype.ngOnInit = function () {
    };
    BtnViewComponent.prototype.view = function (data) {
        switch (data.status) {
            case '新建':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionView', { id: data.sid, status: data.status }]);
                break;
            case '待分配':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionView', { id: data.sid, status: data.status }]);
                break;
            case '待接受':
            case '待处理':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionAcceptView', { id: data.sid, status: data.status }]);
                break;
            case '已解决':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionCloseView', { id: data.sid, status: data.status }]);
                break;
            case '已关闭':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionClosedView', { id: data.sid, status: data.status }]);
                break;
            case '处理中':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionSolvedView', { id: data.sid, status: data.status }]);
                break;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnViewComponent.prototype, "data", void 0);
    BtnViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-view/btn-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/btn-view/btn-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]])
    ], BtnViewComponent);
    return BtnViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/colse-base-view/colse-base-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">解决人组织：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.acceptor_org\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">解决人：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.acceptor\"/>\n    </div>\n</div>\n<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">开始处理时间：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.process_time\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">完成处理时间：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.finish_time\"/>\n    </div>\n</div>\n<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">故障原因：</label>\n    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.reason\"/>\n    </div>\n</div>\n<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">解决方法：</label>\n    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n         <textarea [rows]=\"5\" pInputTextarea\n                   autoResize=\"autoResize\"\n                   readonly\n                   class=\"form-control\"\n                   [(ngModel)]=\"formObj.means\"></textarea>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/colse-base-view/colse-base-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#MalfunctionClose {\n  font-size: 14px; }\n  #MalfunctionClose div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #MalfunctionClose div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #MalfunctionClose form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/colse-base-view/colse-base-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ColseBaseViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ColseBaseViewComponent = (function () {
    function ColseBaseViewComponent(maintenanceService, router, activatedRouter) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.activatedRouter = activatedRouter;
    }
    ColseBaseViewComponent.prototype.ngOnInit = function () {
        this.formObj = new __WEBPACK_IMPORTED_MODULE_3__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initTrick(this.sid);
    };
    ColseBaseViewComponent.prototype.initTrick = function (s) {
        var _this = this;
        this.maintenanceService.getTrick(s).subscribe(function (res) {
            if (res) {
                _this.formObj.solve_org_name = res['solve_org_name'];
                _this.formObj.solve_per_name = res['solve_per_name'];
                _this.formObj.process_time = res['process_time'];
                _this.formObj.finish_time = res['finish_time'];
                _this.formObj.reason = res['reason'];
                _this.formObj.means = res['means'];
                _this.formObj.acceptor_org = res['acceptor_org'];
                _this.formObj.acceptor = res['acceptor'];
            }
        });
    };
    ColseBaseViewComponent.prototype.goBack = function () {
        this.router.navigate(['../malfunctionOverview'], { relativeTo: this.activatedRouter });
    };
    ColseBaseViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-colse-base-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/colse-base-view/colse-base-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/colse-base-view/colse-base-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"]])
    ], ColseBaseViewComponent);
    return ColseBaseViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/malfunction-table/malfunction-table.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dataTable [value]=\"scheduleDatas\" [responsive]=\"true\" id=\"malfunctionTable\">\n\n    <p-column selectionMode=\"multiple\" ></p-column>\n    <p-column field=\"sid\" header=\"故障单号\" [sortable]=\"true\">\n        <ng-template let-data=\"rowData\" pTemplate=\"operator\">\n            <span (click)=\"onOperate(data)\" class=\"ui-cursor-point\">{{data.sid}}</span>\n        </ng-template>\n    </p-column>\n    <p-column field=\"creator\" header=\"受理人\" [sortable]=\"true\"></p-column>\n    <p-column field=\"create_time\" header=\"受理时间\" [sortable]=\"true\"></p-column>\n    <p-column field=\"submitter\" header=\"报告人\" [sortable]=\"true\"></p-column>\n    <p-column field=\"occurrence_time\" header=\"发生时间\" [sortable]=\"true\"></p-column>\n    <p-column field=\"bt_system\" header=\"所属系统\" [sortable]=\"true\"></p-column>\n    <p-column field=\"title\" header=\"故障标题\" [sortable]=\"true\"></p-column>\n    <p-column field=\"status\" header=\"状态\" [sortable]=\"true\"></p-column>\n    <p-column field=\"processor\" header=\"当前处理人\" [sortable]=\"true\"></p-column>\n    <p-column field=\"color\" header=\"操作\" [style]=\"{'width':'17.2vw'}\">\n        <ng-template let-data=\"rowData\" pTemplate=\"operator\">\n            <app-btn-up [data]=\"data\"></app-btn-up>\n            <app-btn-down [data]=\"data\"></app-btn-down>\n            <app-btn-distribute [data]=\"data\"></app-btn-distribute>\n            <app-btn-receive [data]=\"data\" (dataEmitter)=\"reciveDataEmitter($event)\"></app-btn-receive>\n            <app-btn-reject [data]=\"data\" (dataEmitter)=\"rejectDataEmitter($event)\"></app-btn-reject>\n            <app-btn-deal [data]=\"data\" (dataEmitter)=\"dealDataEmitter($event)\" ></app-btn-deal>\n            <app-btn-solve [data]=\"data\" (dataEmitter)=\"rejectDataEmitter($event)\"></app-btn-solve>\n            <app-btn-off [data]=\"data\" (dataEmitter)=\"rejectDataEmitter($event)\"></app-btn-off>\n            <app-btn-edit  [data]=\"data\"></app-btn-edit>\n            <app-btn-delete [data]=\"data\" (dataEmitter)=\"deletDataEmitter($event)\"></app-btn-delete>\n            <app-btn-view [data]=\"data\" ></app-btn-view>\n        </ng-template>\n    </p-column>\n    <ng-template pTemplate=\"emptymessage\">\n        当前没有数据\n    </ng-template>\n</p-dataTable>\n<p-paginator rows=\"10\" totalRecords=\"{{total}}\" [rowsPerPageOptions]=\"[10,20,30]\" pageLinkSize=\"{{page_total}}\"  (onPageChange)=\"paginate($event)\"></p-paginator>\n<p-growl [(value)]=\"msgs\"></p-growl>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/malfunction-table/malfunction-table.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#malfunctionTable /deep/ table thead tr th:nth-child(1) {\n  width: 2.6%; }\n\n#malfunctionTable /deep/ table thead tr th:nth-child(4),\n#malfunctionTable /deep/ table thead tr th:nth-child(6) {\n  width: 11%; }\n\n#malfunctionTable /deep/ table tbody tr td {\n  text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/malfunction-table/malfunction-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MalfunctionTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MalfunctionTableComponent = (function (_super) {
    __extends(MalfunctionTableComponent, _super);
    function MalfunctionTableComponent(maintenanceService, router, c, m) {
        var _this = _super.call(this, c, m) || this;
        _this.maintenanceService = maintenanceService;
        _this.router = router;
        _this.c = c;
        _this.m = m;
        _this.scheduleDatas = []; // 表格渲染数据
        _this.msgs = [];
        return _this;
    }
    MalfunctionTableComponent.prototype.ngOnInit = function () {
        this.initGetList();
    };
    MalfunctionTableComponent.prototype.ngOnChanges = function (changes) {
        var res = changes['eventDatas']['currentValue'];
        if (res) {
            if (res) {
                this.scheduleDatas = res.items;
                if (res['page']) {
                    this.total = res['page']['total'];
                    this.page_size = res['page']['page_size'];
                    this.page_total = res['page']['page_total'];
                }
            }
            else {
                this.scheduleDatas = [];
            }
        }
    };
    MalfunctionTableComponent.prototype.initGetList = function () {
        if (this.searchType === 'trouble_get') {
            this.getList(this.status = '');
        }
        else {
            this.getList(this.status = this.searchType);
        }
    };
    MalfunctionTableComponent.prototype.getList = function (status) {
        var _this = this;
        this.maintenanceService.getAllMainfunctionList('', '', '', '', '', status).subscribe(function (res) {
            if (res) {
                _this.scheduleDatas = res.items;
                _this.total = res['page']['total'];
                _this.page_size = res['page']['page_size'];
                _this.page_total = res['page']['page_total'];
            }
            else {
                _this.scheduleDatas = [];
            }
        });
    };
    MalfunctionTableComponent.prototype.paginate = function (event) {
        var _this = this;
        this.maintenanceService.getAllMainfunctionList(event.rows.toString(), (event.page + 1).toString(), '', '', '', this.status).subscribe(function (res) {
            if (res) {
                _this.scheduleDatas = res.items;
                _this.total = res['page']['total'];
                _this.page_size = res['page']['page_size'];
                _this.page_total = res['page']['page_total'];
            }
            else {
                _this.scheduleDatas = [];
            }
        });
    };
    MalfunctionTableComponent.prototype.deletDataEmitter = function (event) {
        if (event === '00000') {
            this.msgs = [];
            this.msgs.push({ severity: 'success', summary: '消息提示', detail: '删除成功' });
            this.initGetList();
        }
        else {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '消息提示', detail: '删除失败' + event });
        }
    };
    MalfunctionTableComponent.prototype.reciveDataEmitter = function (event) {
        if (event === '00000') {
            this.msgs = [];
            this.msgs.push({ severity: 'success', summary: '消息提示', detail: '接受成功' });
            this.initGetList();
        }
        else {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '消息提示', detail: '接收失败' + event });
        }
    };
    MalfunctionTableComponent.prototype.rejectDataEmitter = function (event) {
        if (event === '00000') {
            this.msgs = [];
            this.msgs.push({ severity: 'success', summary: '消息提示', detail: '拒绝成功' });
            this.initGetList();
        }
        else {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '消息提示', detail: '拒绝失败' + event });
        }
    };
    MalfunctionTableComponent.prototype.dealDataEmitter = function (event) {
        if (event === '00000') {
            this.alert('处理成功');
            this.initGetList();
        }
        else {
            this.alert("\u5904\u7406\u5931\u8D25 + " + event);
        }
    };
    MalfunctionTableComponent.prototype.onOperate = function (data) {
        switch (data.status) {
            case '新建':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionEdit', { id: data.sid, status: data.status }]);
                break;
            case '待分配':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionChange', { id: data.sid, status: data.status }]);
                break;
            case '待接受':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionAccept', { id: data.sid, status: data.status }]);
                break;
            case '待处理':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionProcess', { id: data.sid, status: data.status }]);
                break;
            case '处理中':
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionSolved', { id: data.sid, status: data.status }]);
                break;
            case '已解决':
                // this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionClose', { id: data.sid, status: data.status}]);
                this.router.navigate(['/index/maintenance/malfunctionBase/malfunctionCloseView', { id: data.sid, status: data.status }]);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MalfunctionTableComponent.prototype, "eventDatas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MalfunctionTableComponent.prototype, "searchType", void 0);
    MalfunctionTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-malfunction-table',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/malfunction-table/malfunction-table.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/malfunction-table/malfunction-table.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__["MessageService"]])
    ], MalfunctionTableComponent);
    return MalfunctionTableComponent;
}(__WEBPACK_IMPORTED_MODULE_4__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/personel-dialog/personel-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"请选择人员\" [(visible)]=\"display\" modal=\"modal\" width=\"1100\" [responsive]=\"true\" (onHide)=\"cancel()\">\n    <div class=\"ui-g\">\n        <div class=\"ui-g-4\">\n            <h4>组织</h4>\n            <p-tree [value]=\"orgs\" selectionMode=\"single\" [(selection)]=\"selected\" (onNodeExpand)=\"nodeExpand($event)\"(onNodeSelect) = \"NodeSelect($event)\" [contextMenu]=\"cm\"></p-tree>\n            <p-contextMenu #cm [model]=\"items\"></p-contextMenu>\n        </div>\n        <div class=\"ui-g-8\">\n            <h4>{{ titleName }}</h4>\n            <div class=\"ui-grid-row text_aligin_right\">\n                <p-dataTable [value]=\"tableDatas\"\n                             (onRowSelect)=\"handleRowSelect($event)\"\n                             [responsive]=\"true\"  id=\"manageTable\">\n                    <p-column selectionMode=\"single\" ></p-column>\n                    <p-column field=\"pid\" header=\"帐号\" [sortable]=\"true\"></p-column>\n                    <p-column field=\"name\" header=\"姓名\" [sortable]=\"true\"></p-column>\n                    <p-column field=\"mobile\" header=\"手机\" [sortable]=\"true\"></p-column>\n                    <p-column field=\"organization\" header=\"组织\" [sortable]=\"true\"></p-column>\n                    <p-column field=\"pid\" header=\"职位\" [sortable]=\"true\"></p-column>\n                    <ng-template pTemplate=\"emptymessage\">\n                        当前没有数据\n                    </ng-template>\n                </p-dataTable>\n            </div>\n        </div>\n    </div>\n    <p-footer>\n        <button type=\"button\" pButton icon=\"fa-check\" (click)=\"sure()\" label=\"确定\"></button>\n        <button type=\"button\" pButton icon=\"fa-close\" (click)=\"cancel()\" label=\"取消\"></button>\n    </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/personel-dialog/personel-dialog.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\n  margin-bottom: 1vw; }\n\n.padding-tblr {\n  padding: .25em .5em; }\n\n.start_red {\n  color: red; }\n\n.birthday {\n  display: inline-block; }\n\n@media screen and (max-width: 1366px) {\n  .ui-grid-col-1 {\n    width: 11.33333%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/personel-dialog/personel-dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonelDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PersonelDialogComponent = (function () {
    function PersonelDialogComponent(publicService) {
        this.publicService = publicService;
        this.dataEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.displayEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    PersonelDialogComponent.prototype.ngOnInit = function () {
        this.display = true;
        // 查询组织树
        this.queryOrgTree('');
        // 查询人员表格数据
        this.queryPersonList('');
    };
    // 组织树懒加载
    PersonelDialogComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getOrgTree(event.node.oid).subscribe(function (data) {
                console.log(data);
                event.node.children = data;
            });
        }
    };
    // 组织树选中
    PersonelDialogComponent.prototype.NodeSelect = function (event) {
        if (parseInt(event.node.dep) >= 1) {
            this.queryPersonList(event.node.oid);
        }
    };
    // 查询组织树数据
    PersonelDialogComponent.prototype.queryOrgTree = function (oid) {
        var _this = this;
        this.publicService.getOrgTree(oid).subscribe(function (data) {
            // console.log(data);
            _this.orgs = data;
        });
    };
    // 查询人员表格数据
    PersonelDialogComponent.prototype.queryPersonList = function (oid) {
        var _this = this;
        this.publicService.getPersonList(oid).subscribe(function (data) {
            // console.log(data);
            _this.tableDatas = data;
        });
    };
    PersonelDialogComponent.prototype.handleRowSelect = function (event) {
        // console.log(event);
        this.eventData = event.data;
    };
    PersonelDialogComponent.prototype.sure = function () {
        this.dataEmitter.emit(this.eventData);
        this.displayEmitter.emit(false);
    };
    PersonelDialogComponent.prototype.cancel = function () {
        this.displayEmitter.emit(false);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PersonelDialogComponent.prototype, "dataEmitter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PersonelDialogComponent.prototype, "displayEmitter", void 0);
    PersonelDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-personel-dialog',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/personel-dialog/personel-dialog.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/personel-dialog/personel-dialog.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_public_service__["a" /* PublicService */]])
    ], PersonelDialogComponent);
    return PersonelDialogComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/search-form/search-form.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "div[class=\"ui-g\"] {\r\n  color: #666666;\r\n  border: 1px solid #e2e2e2;\r\n  background: white;\r\n  height: 70px;\r\n  margin-bottom: 20px;\r\n  padding-left: 20px;\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-align: center;\r\n      -ms-flex-align: center;\r\n          align-items: center;\r\n  -webkit-box-pack: center;\r\n      -ms-flex-pack: center;\r\n          justify-content: center; }\r\n  div[class=\"ui-g\"] div:nth-child(1) {\r\n    padding-right: 40px;\r\n    -webkit-box-flex: 2;\r\n        -ms-flex: 2;\r\n            flex: 2; }\r\n    div[class=\"ui-g\"] div:nth-child(1) label {\r\n      padding-right: 10px; }\r\n    div[class=\"ui-g\"] div:nth-child(1) input {\r\n      width: 7.5vw; }\r\n  div[class=\"ui-g\"] div:nth-child(2) {\r\n    padding-right: 40px;\r\n    -webkit-box-flex: 2;\r\n        -ms-flex: 2;\r\n            flex: 2; }\r\n    div[class=\"ui-g\"] div:nth-child(2) label {\r\n      padding-right: 10px; }\r\n    div[class=\"ui-g\"] div:nth-child(2) input {\r\n      width: 69%; }\r\n  div[class=\"ui-g\"] div:nth-child(3) {\r\n    padding-right: 40px;\r\n    -webkit-box-flex: 2;\r\n        -ms-flex: 2;\r\n            flex: 2; }\r\n    div[class=\"ui-g\"] div:nth-child(3) label {\r\n      padding-right: 10px; }\r\n    div[class=\"ui-g\"] div:nth-child(3) p-calendar /deep/ input {\r\n      width: 7vw; }\r\n  div[class=\"ui-g\"] div:nth-child(4) {\r\n    padding-right: 40px;\r\n    -webkit-box-flex: 2;\r\n        -ms-flex: 2;\r\n            flex: 2; }\r\n    div[class=\"ui-g\"] div:nth-child(4) label {\r\n      padding-right: 10px; }\r\n    div[class=\"ui-g\"] div:nth-child(4) input {\r\n      width: 7.5vw; }\r\n  div[class=\"ui-g\"] div:nth-child(5) {\r\n    padding-right: 10px;\r\n    -webkit-box-flex: 2;\r\n        -ms-flex: 2;\r\n            flex: 2;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: horizontal;\r\n    -webkit-box-direction: reverse;\r\n        -ms-flex-direction: row-reverse;\r\n            flex-direction: row-reverse; }\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/search-form/search-form.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" id=\"overview\">\n    <div class=\"ui-g\">\n        <div >\n            <label for=\"\">故障单号：</label>\n            <input type=\"text\" pInputText  [(ngModel)]=\"malfNumber\"/>\n        </div>\n        <div >\n            <label for=\"\">报告人：</label>\n            <input type=\"text\" pInputText   [(ngModel)]=\"malfFrom\"/>\n        </div>\n        <div class=\"ui-grid-row\">\n            <label>发生时间：</label>\n            <p-calendar [(ngModel)]=\"malfOccur\" [showIcon]=\"true\" [locale]=\"zh\" name=\"end_time\"\n                       [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\"\n                       id=\"occurCalendar\">\n            </p-calendar>\n        </div>\n        <div >\n            <label for=\"\">状态：</label>\n            <p-dropdown [options]=\"malstatus\"\n                        [(ngModel)]=\"selectedStatus\"\n                        (onChange)=\"onChange($event)\"\n                        [style]=\"{'width':'60%'}\"></p-dropdown>\n        </div>\n        <div >\n            <button pButton type=\"button\"  label=\"高级查询\"></button>\n            <button pButton type=\"button\"  label=\"查询\" (click)=\"searchSchedule()\"></button>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/search-form/search-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchFormComponent = (function () {
    function SearchFormComponent(maintenanceService) {
        this.maintenanceService = maintenanceService;
        this.searchInfoEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    SearchFormComponent.prototype.ngOnInit = function () {
        this.zh = new __WEBPACK_IMPORTED_MODULE_2__services_PUblicMethod__["a" /* PUblicMethod */]().initZh();
        this.initFlowChart();
    };
    SearchFormComponent.prototype.initFlowChart = function () {
        var _this = this;
        this.maintenanceService.getAllMalfunctionStatus().subscribe(function (res) {
            var newArray = __WEBPACK_IMPORTED_MODULE_2__services_PUblicMethod__["a" /* PUblicMethod */].formateDropDown(res);
            _this.malstatus = newArray;
            _this.selectedStatus = newArray[0]['value'];
        });
    };
    SearchFormComponent.prototype.searchSchedule = function () {
        this.troubleGet();
    };
    SearchFormComponent.prototype.troubleGet = function () {
        var _this = this;
        this.malfOccur = __WEBPACK_IMPORTED_MODULE_2__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(this.malfOccur);
        this.maintenanceService.getAllMainfunctionList('', '', this.malfNumber, this.malfFrom, this.malfOccur, this.selectedStatus).subscribe(function (res) {
            if (res) {
                _this.searchInfoEmitter.emit(res);
            }
        });
    };
    SearchFormComponent.prototype.onChange = function (event) {
        this.selectedStatus = event['value'];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], SearchFormComponent.prototype, "searchType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], SearchFormComponent.prototype, "searchInfoEmitter", void 0);
    SearchFormComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-search-form',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/search-form/search-form.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/search-form/search-form.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */]])
    ], SearchFormComponent);
    return SearchFormComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-close/simple-close.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group \">\n    <label class=\"col-sm-2 control-label\">关闭代码：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.close_code\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">关闭时间：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.close_time\"/>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-close/simple-close.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-close/simple-close.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimpleCloseComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SimpleCloseComponent = (function () {
    function SimpleCloseComponent(maintenanceService, activatedRouter) {
        this.maintenanceService = maintenanceService;
        this.activatedRouter = activatedRouter;
    }
    SimpleCloseComponent.prototype.ngOnInit = function () {
        this.formObj = new __WEBPACK_IMPORTED_MODULE_1__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initTrick(this.sid);
    };
    SimpleCloseComponent.prototype.initTrick = function (s) {
        var _this = this;
        this.maintenanceService.getTrick(s).subscribe(function (res) {
            if (res) {
                _this.formObj.close_code = res['close_code'];
                _this.formObj.close_time = res['close_time'];
            }
        });
    };
    SimpleCloseComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-simple-close',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-close/simple-close.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-close/simple-close.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"]])
    ], SimpleCloseComponent);
    return SimpleCloseComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-time/simple-time.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">分配时间：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.assign_time\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">接受时间：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.accept_time\"/>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-time/simple-time.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-time/simple-time.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimpleTimeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SimpleTimeComponent = (function () {
    function SimpleTimeComponent(maintenanceService, activatedRouter) {
        this.maintenanceService = maintenanceService;
        this.activatedRouter = activatedRouter;
    }
    SimpleTimeComponent.prototype.ngOnInit = function () {
        this.formObj = new __WEBPACK_IMPORTED_MODULE_1__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initTrick(this.sid);
    };
    SimpleTimeComponent.prototype.initTrick = function (s) {
        var _this = this;
        this.maintenanceService.getTrick(s).subscribe(function (res) {
            if (res) {
                _this.formObj.accept_time = res['accept_time'];
                _this.formObj.assign_time = res['assign_time'];
            }
        });
    };
    SimpleTimeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-simple-time',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-time/simple-time.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-time/simple-time.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"]])
    ], SimpleTimeComponent);
    return SimpleTimeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-view/simple-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group\">\n    <label class=\"col-sm-2 control-label\">分配至部门：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.acceptor_org\"/>\n    </div>\n    <label class=\"col-sm-2 control-label\">分配至个人：</label>\n    <div class=\"col-sm-4 ui-fluid-no-padding\">\n        <input type=\"text\" pInputText\n               class=\"form-control cursor_not_allowed\"\n               placeholder=\"自动生成\"\n               readonly\n               [(ngModel)]=\"formObj.acceptor\"/>\n    </div>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-view/simple-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/simple-view/simple-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimpleViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__maintenance_service__ = __webpack_require__("../../../../../src/app/maintenance/maintenance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__matenanceObj_model__ = __webpack_require__("../../../../../src/app/maintenance/matenanceObj.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SimpleViewComponent = (function () {
    function SimpleViewComponent(maintenanceService, activatedRouter) {
        this.maintenanceService = maintenanceService;
        this.activatedRouter = activatedRouter;
    }
    SimpleViewComponent.prototype.ngOnInit = function () {
        this.formObj = new __WEBPACK_IMPORTED_MODULE_3__matenanceObj_model__["a" /* MatenanceObjModel */]();
        this.sid = this.activatedRouter.snapshot.paramMap.get('id');
        this.initTrick(this.sid);
    };
    SimpleViewComponent.prototype.initTrick = function (s) {
        var _this = this;
        this.maintenanceService.getTrick(s).subscribe(function (res) {
            if (res) {
                _this.formObj.acceptor = res['acceptor'];
                _this.formObj.acceptor_org = res['acceptor_org'];
            }
        });
    };
    SimpleViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-simple-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-view/simple-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/simple-view/simple-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__maintenance_service__["a" /* MaintenanceService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"]])
    ], SimpleViewComponent);
    return SimpleViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/upload-file-view/upload-file-view.component.html":
/***/ (function(module, exports) {

module.exports = "<a (click)=\"download()\">{{ file['file_name'] }}</a>\n"

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/upload-file-view/upload-file-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/maintenance/malfunction/public/upload-file-view/upload-file-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadFileViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UploadFileViewComponent = (function () {
    function UploadFileViewComponent() {
    }
    UploadFileViewComponent.prototype.ngOnInit = function () {
    };
    UploadFileViewComponent.prototype.download = function () {
        window.open(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].url.management + "/" + this.file.path, '_blank');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], UploadFileViewComponent.prototype, "file", void 0);
    UploadFileViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-upload-file-view',
            template: __webpack_require__("../../../../../src/app/maintenance/malfunction/public/upload-file-view/upload-file-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/maintenance/malfunction/public/upload-file-view/upload-file-view.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UploadFileViewComponent);
    return UploadFileViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/maintenance/matenanceObj.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MatenanceObjModel; });
var MatenanceObjModel = (function () {
    function MatenanceObjModel() {
        this.sid = '';
        this.name = '';
        this.status = '';
        this.create_time = '';
        this.creator = '';
        this.creator_pid = '';
        this.submitter = '';
        this.submitter_pid = '';
        this.submitter_org = '';
        this.submitter_org_oid = '';
        this.submitter_phone = '';
        this.submitter_email = '';
        this.submitter_wechat = '';
        this.fromx = '';
        this.occurrence_time = '';
        this.deadline = '';
        this.influence = '';
        this.urgency = '';
        this.priority = '';
        this.bt_system = '';
        this.level = '';
        this.addr = '';
        this.title = '';
        this.content = '';
        this.service = '';
        this.devices = '';
        this.work_sheets = '';
        this.attachments = [];
        this.assign_per = '';
        this.assign_per_pid = '';
        this.assign_time = '';
        this.reassign_per = '';
        this.reassign_per_pid = '';
        this.reassign_time = '';
        this.reassign_reason = '';
        this.acceptor = '';
        this.acceptor_pid = '';
        this.acceptor_org = '';
        this.acceptor_org_oid = '';
        this.accept_time = '';
        this.upgrade_per = '';
        this.upgrade_per_pid = '';
        this.upgrade_time = '';
        this.upgrade_reason = '';
        this.processor = '';
        this.processor_pid = '';
        this.processor_org = '';
        this.processor_org_oid = '';
        this.process_time = '';
        this.finish_time = '';
        this.process_times = '';
        this.working_hours = '';
        this.solve_per = '';
        this.solve_per_name = '';
        this.solve_org = '';
        this.solve_org_name = '';
        this.reason = '';
        this.means = '';
        this.lasting = '';
        this.close_time = '';
        this.close_code = '';
    }
    return MatenanceObjModel;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.prod.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    url: {
        management: 'http://127.0.0.1:80',
        managements: 'http://127.0.0.1:8080'
    },
    weinxinNumber: 789
};


/***/ })

});
//# sourceMappingURL=maintenance.module.chunk.js.map