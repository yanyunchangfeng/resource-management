webpackJsonp(["duty.module"],{

/***/ "../../../../../src/app/duty/create-handover/create-handover.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <!--<span class=\"gt\">&gt;</span>{{title}}-->\n        <span class=\"feature-title\">值班管理</span>\n    </div>\n</div>\n<div class=\"content-section\" id=\"myContent\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>{{title}}</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">交班单号：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"自动生成\"\n                       readonly\n                       [value]=\"formObj.sid\"\n                />\n            </div>\n            <label class=\"col-sm-2 control-label\">交班时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [value]=\"formObj.create_time\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">班次名称：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"自动生成\"\n                       readonly\n                       [value]=\"formObj.shift_name\"\n                />\n            </div>\n            <label class=\"col-sm-2 control-label\">班次时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [value]=\"formObj.shift_time\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>接班人：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding ui-fluid\">\n                <p-dropdown [options]=\"acceptors\"\n                            [(ngModel)]=\"formObj.acceptor\"\n                            placeholder=\"{{formObj.acceptor}}\"\n                            [ngModelOptions]=\"{standalone: true}\"\n                            [style]=\"{'width':'100%'}\"\n                ></p-dropdown>\n                <!--{{formObj.acceptor['label']}}-->\n                <!--{{formObj.acceptor['value']}}-->\n                <!--{{formObj.acceptor}}-->\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">\n                <span ngClass=\"start_red\">*</span>\n                交班内容：\n            </label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-no-padding-right-15px ui-fluid\">\n                 <textarea [rows]=\"5\" pInputTextarea\n                           autoResize=\"autoResize\"\n                           formControlName=\"content\"\n                           [(ngModel)]=\"formObj.content\"></textarea>\n                <app-field-error-display\n                        [displayError]=\"isFieldValid('content')\"\n                        errorMsg=\"不能为空\"></app-field-error-display>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">备注：</label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-no-padding-right-15px ui-fluid\">\n                 <textarea [rows]=\"5\" pInputTextarea\n                           autoResize=\"autoResize\"\n                           [ngModelOptions]=\"{standalone: true}\"\n                           [(ngModel)]=\"formObj.remarks\"></textarea>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <div class=\"col-sm-12 ui-no-padding-left-15px \">\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"submit()\" label=\"提交\" ></button>\n                <button  class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"save()\" label=\"保存\" ></button>\n            </div>\n        </div>\n    </form>\n</div>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<!--<div class=\"content-section\">-->\n    <!--<form [formGroup]=\"myForm\" class=\"form-horizontal\">-->\n     <!---->\n    <!--</form>-->\n<!--</div>-->\n\n"

/***/ }),

/***/ "../../../../../src/app/duty/create-handover/create-handover.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#myContent {\n  font-size: 14px; }\n  #myContent div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #myContent div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #myContent form {\n    background: white;\n    padding: 20px 26px 20px 0px;\n    border: 1px solid #e2e2e2; }\n\n.padding-left-2vw {\n  padding-left: 2vw; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 12.666667%; }\n  .col-sm-10 {\n    width: 79.333333%; } }\n\n@media (min-width: 1366px) {\n  .col-sm-1 {\n    width: 9.333333%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/create-handover/create-handover.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateHandoverComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handoverobj_model__ = __webpack_require__("../../../../../src/app/duty/handoverobj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CreateHandoverComponent = (function (_super) {
    __extends(CreateHandoverComponent, _super);
    function CreateHandoverComponent(fb, router, activedRouter, dutyService, c, m) {
        var _this = _super.call(this, c, m) || this;
        _this.fb = fb;
        _this.router = router;
        _this.activedRouter = activedRouter;
        _this.dutyService = dutyService;
        _this.c = c;
        _this.m = m;
        _this.title = '创建交接班';
        _this.editable = false;
        return _this;
        // this.acceptors = [
        //     {
        //         'label': '张珊',
        //         'value': '张珊'
        //     },
        //     {
        //         'label': '李思',
        //         'value': '李思'
        //     },
        //     {
        //         'label': '王舞',
        //         'value': '王舞'
        //     },
        //     {
        //         'label': '七喜',
        //         'value': '七喜'
        //     }
        // ];
    }
    CreateHandoverComponent.prototype.ngOnInit = function () {
        this.formObj = new __WEBPACK_IMPORTED_MODULE_3__handoverobj_model__["a" /* HandoverobjModel */]();
        this.initMyForm();
        this.getUserDutyMsg();
        this.getNextDutyPerson();
        this.getRouterParam();
    };
    CreateHandoverComponent.prototype.initMyForm = function () {
        this.myForm = this.fb.group({
            content: [null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]
        });
    };
    CreateHandoverComponent.prototype.getUserDutyMsg = function () {
        var _this = this;
        this.dutyService.getUserDutyMsg().subscribe(function (res) {
            if (res['errcode'] === '00000') {
                _this.formObj.shift_name = res['datas'][0]['sid'];
                _this.formObj.shift_id = res['datas'][0]['did'];
                _this.handleShiftTime(res);
            }
            else {
                _this.alert('获取当前用户班次信息失败', 'error');
            }
        });
    };
    CreateHandoverComponent.prototype.handleShiftTime = function (res) {
        var beginTime1 = res['datas'][0]['begin_time_1'];
        var endTime1 = res['datas'][0]['end_time_1'];
        var beginTime2 = res['datas'][0]['begin_time_2'];
        var endTime2 = res['datas'][0]['end_time_2'];
        if (beginTime1 && !beginTime2) {
            this.formObj.shift_time = __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(beginTime1) + " - " + __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(endTime1);
        }
        else if (beginTime1 && beginTime2) {
            this.formObj.shift_time = __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(beginTime1) + " - " + __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(endTime1) + " | " + __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(beginTime2) + " - " + __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(endTime2);
        }
    };
    CreateHandoverComponent.prototype.getNextDutyPerson = function () {
        var _this = this;
        this.dutyService.getNextDutyPerson().subscribe(function (res) {
            if (res['errcode'] === '00000') {
                _this.acceptors = __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].formateFlexDropDown(res['datas'], 'per_name', 'per');
                _this.activedRouter.queryParams.subscribe(function (param) {
                    if (param['datas']) {
                        _this.title = '编辑交接班';
                        _this.editable = true;
                        _this.formObj = new __WEBPACK_IMPORTED_MODULE_3__handoverobj_model__["a" /* HandoverobjModel */](JSON.parse(param['datas']));
                    }
                    else {
                        _this.formObj.acceptor = _this.acceptors[0]['value']['label'];
                        _this.formObj.acceptor_pid = _this.acceptors[0]['value']['value'];
                    }
                });
            }
            else {
                _this.alert('获取下一次值班人员失败', 'error');
            }
        });
    };
    CreateHandoverComponent.prototype.isFieldValid = function (field) {
        return __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].isFieldValid(this.myForm, field);
    };
    CreateHandoverComponent.prototype.resetSelectAccept = function () {
        if (typeof this.formObj.acceptor === 'object') {
            this.formObj.acceptor_pid = this.formObj.acceptor['value'];
            this.formObj.acceptor = this.formObj.acceptor['label'];
        }
    };
    CreateHandoverComponent.prototype.submit = function () {
        var _this = this;
        this.resetSelectAccept();
        if (this.myForm.valid) {
            this.dutyService.addHandover(this.formObj).subscribe(function (res) {
                if (res === '00000') {
                    _this.alert('新增成功');
                    _this.goBack();
                }
                else {
                    _this.alert("\u65B0\u589E\u5931\u8D25 + " + res, 'error');
                }
            });
        }
        else {
            __WEBPACK_IMPORTED_MODULE_8__services_PUblicMethod__["a" /* PUblicMethod */].validateAllFormFields(this.myForm);
        }
    };
    CreateHandoverComponent.prototype.save = function () {
        var _this = this;
        this.resetSelectAccept();
        this.dutyService.saveHandover(this.formObj).subscribe(function (res) {
            if (res === '00000') {
                _this.alert('保存成功');
                _this.goBack();
            }
            else {
                _this.alert("\u4FDD\u5B58\u5931\u8D25 + " + res, 'error');
            }
        });
    };
    CreateHandoverComponent.prototype.goBack = function () {
        this.router.navigate(['../handoveroverview'], { relativeTo: this.activedRouter });
    };
    //    EDIT
    CreateHandoverComponent.prototype.getRouterParam = function () {
        var _this = this;
        this.activedRouter.queryParams.subscribe(function (param) {
            if (param['datas']) {
                _this.title = '编辑交接班';
                _this.editable = true;
                _this.formObj = new __WEBPACK_IMPORTED_MODULE_3__handoverobj_model__["a" /* HandoverobjModel */](JSON.parse(param['datas']));
            }
        });
    };
    CreateHandoverComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-create-handover',
            template: __webpack_require__("../../../../../src/app/duty/create-handover/create-handover.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/create-handover/create-handover.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_4__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_6_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__["MessageService"]])
    ], CreateHandoverComponent);
    return CreateHandoverComponent;
}(__WEBPACK_IMPORTED_MODULE_5__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/duty/current-duty/current-duty.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \">\n    <div>\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>当日值班  </span>\n    </div>\n</div>\n<div class=\"content-section implementation\">\n    <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid bg\">\n        <div class=\"ui-grid-row row_bottome\">\n            <div class=\"ui-grid-col-3\"></div>\n            <div class=\"ui-grid-col-6 text_aligin_c\">\n                <h3 class=\"\">当日值班人员</h3>\n                <span>{{ currentTime }}</span>\n            </div>\n            <div class=\"ui-grid-col-3\"></div>\n        </div>\n        <div class=\"ui-col-row \">\n                <p-organizationChart id=\"organazatioTree\" [value]=\"data1\" selectionMode=\"single\" [(selection)]=\"selectedNode\" (onNodeSelect)=\"onNodeSelect($event)\">\n                    <ng-template let-node pTemplate=\"person\">\n                        <div class=\"my-center-text\">\n                            <div class=\"ui-corner-top header_wraper\" [style.background]=\"node.color\" >{{node.label}}</div>\n                            <div class=\"node-content\" [style.border-color]=\"node.color\">\n                                <img [src]=\"node.per_data.photo1 | safeHtml \" width=\"32\">\n                                <div>{{node.per_data.name}}</div>\n                            </div>\n                        </div>\n                    </ng-template>\n                </p-organizationChart>\n        </div>\n        <div class=\"ui-g\" *ngIf=\"display\">\n            <h3>当日无值班！</h3>\n        </div>\n    </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/duty/current-duty/current-duty.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".node-header, .node-content {\n  padding: .5em .7em; }\n\n.node-content {\n  border-width: 1px;\n  border-style: solid; }\n  .node-content img {\n    border-radius: 50%;\n    width: 4vw; }\n\n.header_wraper {\n  padding: .5em 1.5em;\n  color: white; }\n\n.title_bold {\n  font-size: 16px; }\n\n.border_bottom_gray {\n  border-bottom: 1px solid gray; }\n\n.text_aligin_c {\n  text-align: center; }\n\n#organazatioTree /deep/ div table {\n  text-align: center;\n  width: 100%; }\n  #organazatioTree /deep/ div table tr td {\n    padding: 0 1.75em; }\n    #organazatioTree /deep/ div table tr td > div {\n      padding: 0; }\n\n.row_bottome {\n  margin-bottom: 2vw;\n  border-bottom: 1px solid #ececec;\n  padding-bottom: .5vw; }\n\n.implementation > .bg {\n  background: white;\n  height: 44vw; }\n\n@media screen and (max-width: 1440px) {\n  .implementation > .bg {\n    background: white;\n    height: 56vw; } }\n\n@media screen and (max-width: 1366px) {\n  .implementation > .bg {\n    background: white;\n    height: 49vw; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/current-duty/current-duty.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CurrentDutyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CurrentDutyComponent = (function () {
    function CurrentDutyComponent(dutyService) {
        this.dutyService = dutyService;
        this.display = false;
    }
    CurrentDutyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dutyService.getCurrentDuty().subscribe(function (res) {
            if (!res) {
                _this.display = true;
            }
            else {
                _this.data1 = res;
                _this.display = false;
            }
        });
        this.initCurrentDuty();
        this.getcurrentTime();
    };
    CurrentDutyComponent.prototype.initCurrentDuty = function () {
        window.setInterval(function () {
            var _this = this;
            this.dutyService.getCurrentDuty().subscribe(function (res) {
                if (!res) {
                    _this.display = true;
                }
                else {
                    _this.data1 = res;
                    _this.display = false;
                }
            });
        }, 900000);
    };
    CurrentDutyComponent.prototype.getcurrentTime = function () {
        var _this = this;
        window.setInterval(function () {
            _this.currentTime = _this.getTime();
        }, 1000);
    };
    CurrentDutyComponent.prototype.getTime = function () {
        var today = new Date();
        var y, month, d, h, m, s;
        y = today.getFullYear();
        month = this.pad(today.getMonth() + 1);
        d = this.pad(today.getDate());
        h = this.pad(today.getHours());
        m = this.pad(today.getMinutes());
        s = this.pad(today.getSeconds());
        return y + "-" + month + "-" + d + "  " + h + ":" + m + ":" + s;
    };
    CurrentDutyComponent.prototype.pad = function (n) {
        if (n < 10) {
            return "0" + n;
        }
        return n;
    };
    CurrentDutyComponent.prototype.onNodeSelect = function (event) {
        console.log(222);
    };
    CurrentDutyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-current-duty',
            template: __webpack_require__("../../../../../src/app/duty/current-duty/current-duty.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/current-duty/current-duty.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__duty_service__["a" /* DutyService */]])
    ], CurrentDutyComponent);
    return CurrentDutyComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/duty-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DutyRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__current_duty_current_duty_component__ = __webpack_require__("../../../../../src/app/duty/current-duty/current-duty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__duty_shift_overview_duty_shift_overview_component__ = __webpack_require__("../../../../../src/app/duty/duty-shift-overview/duty-shift-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__overview_duty_overview_duty_component__ = __webpack_require__("../../../../../src/app/duty/overview-duty/overview-duty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__schedule_add_schedule_add_component__ = __webpack_require__("../../../../../src/app/duty/schedule-add/schedule-add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__schedule_approval_schedule_approval_component__ = __webpack_require__("../../../../../src/app/duty/schedule-approval/schedule-approval.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__schedule_edit_schedule_edit_component__ = __webpack_require__("../../../../../src/app/duty/schedule-edit/schedule-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__schedule_manage_schedule_manage_component__ = __webpack_require__("../../../../../src/app/duty/schedule-manage/schedule-manage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__schedule_view_schedule_view_component__ = __webpack_require__("../../../../../src/app/duty/schedule-view/schedule-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__setting_duty_setting_duty_component__ = __webpack_require__("../../../../../src/app/duty/setting-duty/setting-duty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__handover_overview_handover_overview_component__ = __webpack_require__("../../../../../src/app/duty/handover-overview/handover-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__min_handover_overview_min_handover_overview_component__ = __webpack_require__("../../../../../src/app/duty/min-handover-overview/min-handover-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__create_handover_create_handover_component__ = __webpack_require__("../../../../../src/app/duty/create-handover/create-handover.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__view_handover_view_handover_component__ = __webpack_require__("../../../../../src/app/duty/view-handover/view-handover.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var route = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__current_duty_current_duty_component__["a" /* CurrentDutyComponent */] },
    { path: 'current', component: __WEBPACK_IMPORTED_MODULE_2__current_duty_current_duty_component__["a" /* CurrentDutyComponent */] },
    { path: 'add', component: __WEBPACK_IMPORTED_MODULE_5__schedule_add_schedule_add_component__["a" /* ScheduleAddComponent */] },
    { path: 'setting', component: __WEBPACK_IMPORTED_MODULE_10__setting_duty_setting_duty_component__["a" /* SettingDutyComponent */] },
    { path: 'dutyshift', component: __WEBPACK_IMPORTED_MODULE_3__duty_shift_overview_duty_shift_overview_component__["a" /* DutyShiftOverviewComponent */] },
    { path: 'overview', component: __WEBPACK_IMPORTED_MODULE_4__overview_duty_overview_duty_component__["a" /* OverviewDutyComponent */] },
    { path: 'approval/:id', component: __WEBPACK_IMPORTED_MODULE_6__schedule_approval_schedule_approval_component__["a" /* ScheduleApprovalComponent */] },
    { path: 'edit/:id', component: __WEBPACK_IMPORTED_MODULE_7__schedule_edit_schedule_edit_component__["a" /* ScheduleEditComponent */] },
    { path: 'manage', component: __WEBPACK_IMPORTED_MODULE_8__schedule_manage_schedule_manage_component__["a" /* ScheduleManageComponent */] },
    { path: 'view/:id', component: __WEBPACK_IMPORTED_MODULE_9__schedule_view_schedule_view_component__["a" /* ScheduleViewComponent */] },
    { path: 'handoveroverview', component: __WEBPACK_IMPORTED_MODULE_11__handover_overview_handover_overview_component__["a" /* HandoverOverviewComponent */] },
    { path: 'minhandoveroverview', component: __WEBPACK_IMPORTED_MODULE_12__min_handover_overview_min_handover_overview_component__["a" /* MinHandoverOverviewComponent */] },
    { path: 'createhandover', component: __WEBPACK_IMPORTED_MODULE_13__create_handover_create_handover_component__["a" /* CreateHandoverComponent */] },
    { path: 'viewhandover', component: __WEBPACK_IMPORTED_MODULE_14__view_handover_view_handover_component__["a" /* ViewHandoverComponent */] }
];
var DutyRoutingModule = (function () {
    function DutyRoutingModule() {
    }
    DutyRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild(route)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]],
        })
    ], DutyRoutingModule);
    return DutyRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/duty/duty-shift-overview/duty-shift-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \">\n    <div>\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>调班日志  </span>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"dutyshift\">\n    <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\">\n        <div class=\"ui-grid-row\">\n            <div class=\"\">\n                <label for=\"scheduleName\">计划名称:</label>\n                <input id=\"scheduleName\" type=\"text\" pInputText placeholder=\"\" [(ngModel)]=\"scheduleName\"/>\n            </div>\n            <div class=\"\">\n                <label for=\"originOperater\">原值班人:</label>\n                <input id=\"originOperater\" type=\"text\" pInputText placeholder=\"\" [(ngModel)]=\"originOperater\"/>\n            </div>\n            <div class=\"\">\n                <label for=\"currentOperater\">操作人:</label>\n                <input id=\"currentOperater\" type=\"text\" pInputText  placeholder=\"\" [(ngModel)]=\"currentOperater\"/>\n            </div>\n            <div class=\"\">\n                <button pButton type=\"button\"  label=\"查询\" (click)=\"searchSchedule()\" ></button>\n                <button pButton type=\"button\"  label=\"清空\" (click)=\"clear()\" ></button>\n            </div>\n        </div>\n    </div>\n    <p-dataTable [value]=\"statusOptions\" [responsive]=\"true\"  >\n        <p-column [style]=\"{'width':'30px'}\"></p-column>\n        <p-column field=\"did\" header=\"计划编号\" [sortable]=\"true\"></p-column>\n        <p-column field=\"did_name\" header=\"计划名称\" [sortable]=\"true\"></p-column>\n        <p-column field=\"pre_per_name\" header=\"原值班人\" [sortable]=\"true\"></p-column>\n        <p-column field=\"cur_per_name\" header=\"现值班人\" [sortable]=\"true\"></p-column>\n        <p-column field=\"begin_time_1\" header=\"值班时间\" [sortable]=\"true\"></p-column>\n        <p-column field=\"operator_name\" header=\"操作人\" [sortable]=\"true\"></p-column>\n        <p-column field=\"operate_time\" header=\"操作时间\" [sortable]=\"true\"></p-column>\n        <ng-template pTemplate=\"emptymessage\">\n            当前没有数据\n        </ng-template>\n    </p-dataTable>\n    <p-paginator rows=\"10\" totalRecords=\"{{total}}\" [rowsPerPageOptions]=\"[10,20,30]\"   (onPageChange)=\"paginate($event)\"></p-paginator>\n    <p-growl [(value)]=\"msgs\"></p-growl>\n    <p-growl [(value)]=\"deletemsgs\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/duty/duty-shift-overview/duty-shift-overview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#dutyshift {\n  font-size: 14px; }\n  #dutyshift div[class=\"ui-grid-row\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center; }\n    #dutyshift div[class=\"ui-grid-row\"] div:nth-child(1) {\n      padding-right: 60px;\n      -webkit-box-flex: 2.6;\n          -ms-flex: 2.6;\n              flex: 2.6; }\n      #dutyshift div[class=\"ui-grid-row\"] div:nth-child(1) label {\n        padding-right: 10px; }\n      #dutyshift div[class=\"ui-grid-row\"] div:nth-child(1) input {\n        width: 15.6vw; }\n    #dutyshift div[class=\"ui-grid-row\"] div:nth-child(2) {\n      padding-right: 60px;\n      -webkit-box-flex: 2.6;\n          -ms-flex: 2.6;\n              flex: 2.6; }\n      #dutyshift div[class=\"ui-grid-row\"] div:nth-child(2) label {\n        padding-right: 10px; }\n      #dutyshift div[class=\"ui-grid-row\"] div:nth-child(2) input {\n        width: 15.6vw; }\n    #dutyshift div[class=\"ui-grid-row\"] div:nth-child(3) {\n      padding-right: 60px;\n      -webkit-box-flex: 2.5;\n          -ms-flex: 2.5;\n              flex: 2.5; }\n      #dutyshift div[class=\"ui-grid-row\"] div:nth-child(3) label {\n        padding-right: 10px; }\n      #dutyshift div[class=\"ui-grid-row\"] div:nth-child(3) input {\n        width: 15.6vw; }\n    #dutyshift div[class=\"ui-grid-row\"] div:nth-child(4) {\n      padding-right: 20px;\n      -webkit-box-flex: 2;\n          -ms-flex: 2;\n              flex: 2;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: row-reverse;\n              flex-direction: row-reverse; }\n      #dutyshift div[class=\"ui-grid-row\"] div:nth-child(4) button {\n        width: 100px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/duty-shift-overview/duty-shift-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DutyShiftOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DutyShiftOverviewComponent = (function () {
    function DutyShiftOverviewComponent(dutyService) {
        this.dutyService = dutyService;
        this.statusOptions = [];
    }
    DutyShiftOverviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dutyService.getALLshiftsLog().subscribe(function (res) {
            _this.statusOptions = res.items;
            _this.total = res.page.total;
            _this.page_size = res.page.page_size;
            _this.page_total = res.page.page_total;
        });
    };
    DutyShiftOverviewComponent.prototype.searchSchedule = function () {
        var _this = this;
        this.dutyService.getALLshiftsLog(this.scheduleName, this.originOperater, this.currentOperater).subscribe(function (res) {
            _this.statusOptions = res.items;
            _this.total = res.page.total;
            _this.page_size = res.page.page_size;
            _this.page_total = res.page.page_total;
        });
    };
    DutyShiftOverviewComponent.prototype.paginate = function (event) {
        var _this = this;
        // console.log(event);
        this.dutyService.getALLshiftsLog('', '', '', (event.page + 1).toString(), event.rows.toString()).subscribe(function (res) {
            _this.statusOptions = res.items;
            _this.total = res.page.total;
            _this.page_size = res.page.page_size;
            _this.page_total = res.page.page_total;
        });
    };
    DutyShiftOverviewComponent.prototype.clear = function () {
        this.scheduleName = '';
        this.originOperater = '';
        this.currentOperater = '';
    };
    DutyShiftOverviewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-duty-shift-overview',
            template: __webpack_require__("../../../../../src/app/duty/duty-shift-overview/duty-shift-overview.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/duty-shift-overview/duty-shift-overview.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__duty_service__["a" /* DutyService */]])
    ], DutyShiftOverviewComponent);
    return DutyShiftOverviewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/duty.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DutyModule", function() { return DutyModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__current_duty_current_duty_component__ = __webpack_require__("../../../../../src/app/duty/current-duty/current-duty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_share_module__ = __webpack_require__("../../../../../src/app/shared/share.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__duty_routing_module__ = __webpack_require__("../../../../../src/app/duty/duty-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__overview_duty_overview_duty_component__ = __webpack_require__("../../../../../src/app/duty/overview-duty/overview-duty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__schedule_manage_schedule_manage_component__ = __webpack_require__("../../../../../src/app/duty/schedule-manage/schedule-manage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__schedule_add_schedule_add_component__ = __webpack_require__("../../../../../src/app/duty/schedule-add/schedule-add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__schedule_edit_schedule_edit_component__ = __webpack_require__("../../../../../src/app/duty/schedule-edit/schedule-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__schedule_view_schedule_view_component__ = __webpack_require__("../../../../../src/app/duty/schedule-view/schedule-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__schedule_approval_schedule_approval_component__ = __webpack_require__("../../../../../src/app/duty/schedule-approval/schedule-approval.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__duty_shift_overview_duty_shift_overview_component__ = __webpack_require__("../../../../../src/app/duty/duty-shift-overview/duty-shift-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__setting_duty_setting_duty_component__ = __webpack_require__("../../../../../src/app/duty/setting-duty/setting-duty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__public_public_module__ = __webpack_require__("../../../../../src/app/public/public.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__create_handover_create_handover_component__ = __webpack_require__("../../../../../src/app/duty/create-handover/create-handover.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__handover_overview_handover_overview_component__ = __webpack_require__("../../../../../src/app/duty/handover-overview/handover-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__min_handover_overview_min_handover_overview_component__ = __webpack_require__("../../../../../src/app/duty/min-handover-overview/min-handover-overview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__view_handover_view_handover_component__ = __webpack_require__("../../../../../src/app/duty/view-handover/view-handover.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__public_search_form_search_form_component__ = __webpack_require__("../../../../../src/app/duty/public/search-form/search-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__public_handover_table_handover_table_component__ = __webpack_require__("../../../../../src/app/duty/public/handover-table/handover-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__public_btn_edit_btn_edit_component__ = __webpack_require__("../../../../../src/app/duty/public/btn-edit/btn-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__public_btn_delete_btn_delete_component__ = __webpack_require__("../../../../../src/app/duty/public/btn-delete/btn-delete.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__public_btn_accept_btn_accept_component__ = __webpack_require__("../../../../../src/app/duty/public/btn-accept/btn-accept.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__public_btn_reject_btn_reject_component__ = __webpack_require__("../../../../../src/app/duty/public/btn-reject/btn-reject.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__public_btn_rehandover_btn_rehandover_component__ = __webpack_require__("../../../../../src/app/duty/public/btn-rehandover/btn-rehandover.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var DutyModule = (function () {
    function DutyModule() {
    }
    DutyModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__current_duty_current_duty_component__["a" /* CurrentDutyComponent */],
                __WEBPACK_IMPORTED_MODULE_5__overview_duty_overview_duty_component__["a" /* OverviewDutyComponent */],
                __WEBPACK_IMPORTED_MODULE_6__schedule_manage_schedule_manage_component__["a" /* ScheduleManageComponent */],
                __WEBPACK_IMPORTED_MODULE_7__schedule_add_schedule_add_component__["a" /* ScheduleAddComponent */],
                __WEBPACK_IMPORTED_MODULE_8__schedule_edit_schedule_edit_component__["a" /* ScheduleEditComponent */],
                __WEBPACK_IMPORTED_MODULE_9__schedule_view_schedule_view_component__["a" /* ScheduleViewComponent */],
                __WEBPACK_IMPORTED_MODULE_10__schedule_approval_schedule_approval_component__["a" /* ScheduleApprovalComponent */],
                __WEBPACK_IMPORTED_MODULE_11__duty_shift_overview_duty_shift_overview_component__["a" /* DutyShiftOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_12__setting_duty_setting_duty_component__["a" /* SettingDutyComponent */],
                __WEBPACK_IMPORTED_MODULE_15__create_handover_create_handover_component__["a" /* CreateHandoverComponent */],
                __WEBPACK_IMPORTED_MODULE_16__handover_overview_handover_overview_component__["a" /* HandoverOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_17__min_handover_overview_min_handover_overview_component__["a" /* MinHandoverOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_18__view_handover_view_handover_component__["a" /* ViewHandoverComponent */],
                __WEBPACK_IMPORTED_MODULE_19__public_search_form_search_form_component__["a" /* SearchFormComponent */],
                __WEBPACK_IMPORTED_MODULE_20__public_handover_table_handover_table_component__["a" /* HandoverTableComponent */],
                __WEBPACK_IMPORTED_MODULE_21__public_btn_edit_btn_edit_component__["a" /* BtnEditComponent */],
                __WEBPACK_IMPORTED_MODULE_22__public_btn_delete_btn_delete_component__["a" /* BtnDeleteComponent */],
                __WEBPACK_IMPORTED_MODULE_23__public_btn_accept_btn_accept_component__["a" /* BtnAcceptComponent */],
                __WEBPACK_IMPORTED_MODULE_24__public_btn_reject_btn_reject_component__["a" /* BtnRejectComponent */],
                __WEBPACK_IMPORTED_MODULE_25__public_btn_rehandover_btn_rehandover_component__["a" /* BtnRehandoverComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__shared_share_module__["a" /* ShareModule */],
                __WEBPACK_IMPORTED_MODULE_3__duty_routing_module__["a" /* DutyRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_14__public_public_module__["a" /* PublicModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__duty_service__["a" /* DutyService */],
                __WEBPACK_IMPORTED_MODULE_13__services_public_service__["a" /* PublicService */]
            ]
        })
    ], DutyModule);
    return DutyModule;
}());



/***/ }),

/***/ "../../../../../src/app/duty/duty.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DutyService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DutyService = (function () {
    function DutyService(http, storageService) {
        this.http = http;
        this.storageService = storageService;
        this.ip = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management;
    }
    // 获取当日值班数据
    DutyService.prototype.getCurrentDuty = function () {
        var _this = this;
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'dutylist_per_get'
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            else {
                _this.changeObjectName(res['datas'], 'label', 'post');
            }
            return res['datas'];
        });
    };
    DutyService.prototype.changeObjectName = function (array, newName, oldName) {
        for (var i in array) {
            array[i][newName] = array[i][oldName];
            delete array[i][oldName];
            this.changeObjectName(array[i].children, newName, oldName);
        }
    };
    //  获取排班管理总数据
    DutyService.prototype.getAllSchduleDatas = function (name, status, page, pageSize) {
        (!name) && (name = '');
        (!status) && (status = '');
        (!page) && (page = '1');
        (!pageSize) && (pageSize = '10');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'duty_get_all',
            'data': {
                'condition': {
                    'name': name,
                    'status': status
                },
                'page': {
                    'page_size': pageSize,
                    'page_number': page
                }
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    };
    // 获取排班班次
    DutyService.prototype.getAllSchduleShifts = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': 'access_tokenxxxxxxxx',
            'type': 'shift_get_all',
            'data': {
                'status': 'true'
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //    获取新增计划编号
    //     getAddSchduleNumber() {
    //         let token = this.storageService.getToken('token');
    //         return this.http.post(
    //             `${this.ip}/onduty`,
    //             {
    //                 "access_token": token,
    //                 "type": "duty_add_id"
    //             }
    //         ).map((res: Response) => {
    //             let body = res.json();
    //             if (body.errcode !== '00000') {
    //                 return new Error(body.errmsg);
    //             }
    //             return body['data'];
    //         })
    //     }
    //    获取关联计划
    DutyService.prototype.getRelaticeScheduleData = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': 'access_tokenxxxxxxxx',
            'type': 'duty_relateddid_get',
            'data': {
                'begin_time': '2017-11-20',
            }
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return new Error(body.errmsg);
            // }
            // return body['datas'];
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //  获取组织树
    DutyService.prototype.getDepartmentDatas = function (oid, dep) {
        (!oid) && (oid = '');
        (!dep) && (dep = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/org", {
            'access_token': token,
            'type': 'get_suborg',
            'id': oid,
            'dep': dep
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            else {
                res['datas'].forEach(function (e) {
                    e.label = e.name;
                    delete e.name;
                    e.leaf = false;
                    e.data = e.name;
                });
            }
            return res['datas'];
        });
    };
    //  获取所有状态种类
    DutyService.prototype.getAllStatus = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': 'access_tokenxxxxxxxx',
            'type': 'duty_status_get'
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    };
    //   计划新增接口
    DutyService.prototype.addSchedule = function (name, ctime, departement, aproval, start, end, shifts, relative, status) {
        // let shiftArray = [];
        // let shiftStr = '';
        // let approvalArray = [];
        // let approvalStr = '';
        (!relative) && (relative = '');
        // shiftStr = this.returnDotString(shifts);
        // approvalStr = this.returnDotString(aproval);
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'duty_save',
            'data': {
                'did': '',
                'create_time': ctime,
                'name': name,
                'begin_time': start,
                'end_time': end,
                'related_duty': relative,
                'duty_type': '',
                'attendance_rule': 'true',
                'approver_structures': aproval,
                'status': status,
                'shift_structures': shifts,
                'orgs_structures': departement
            }
        }).map(function (res) {
            // let body = res.json();
            // return body;
            return res;
        });
    };
    // 计划修改接口
    DutyService.prototype.editSchedule = function (id, defaultCreator, name, ctime, departement, aproval, start, end, shifts, relative, status) {
        (!departement) && (departement = '');
        (!relative) && (relative = '');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'duty_mod',
            'data': {
                'did': id,
                'creator': defaultCreator,
                'creator_pid': '',
                'create_time': ctime,
                'name': name,
                'begin_time': start,
                'end_time': end,
                'related_duty': relative,
                'duty_type': '',
                'attendance_rule': '',
                'shift_change_way': '',
                'approver': '',
                'approver_structures': aproval,
                'approve_time': '',
                'approve_remarks': '',
                'status': status,
                'shifts': '',
                'shift_structures': shifts,
                'template_file_path': '',
                'schedules': null,
                'duty_org': '',
                'orgs_structures': departement,
            }
        }).map(function (res) {
            // let body = res.json();
            // if ( body.errcode !== '00000') {
            //     return body.errmsg;
            // }
            // return body.errcode;
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    // returnDotString(obj): string {
    //     let array = [];
    //     for( let value of obj) {
    //         if(value.sid){
    //             array.push(value.sid);
    //         }else if(value.pid) {
    //             array.push(value.pid);
    //         }
    //     }
    //     return array.join(',');
    // }
    //  通过ID获取计划接口
    DutyService.prototype.getScheduleById = function (id) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'duty_get',
            'id': id
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return  body['errmsg'];
            // }
            // return body['data'];
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //    下载模板
    DutyService.prototype.downloadModel = function (id) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'duty_download',
            'id': id
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return  false;
            // }
            // return body['data'];
            if (res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    };
    //  删除计划
    DutyService.prototype.deleteSchedule = function (id) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'duty_del',
            'ids': id
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return  body.errmsg;
            // }
            // return body.errcode;
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    审批接口
    DutyService.prototype.approveSchedule = function (id, remarks, status) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'duty_audit',
            'data': {
                'did': id,
                'approve_remarks': remarks,
                'status': status
            }
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return  false;
            // }
            // return true;
            if (res['errcode'] !== '00000') {
                return false;
            }
            return true;
        });
    };
    //    调班日志总览
    DutyService.prototype.getALLshiftsLog = function (did, pre, current, number, size) {
        (!did) && (did = '');
        (!pre) && (pre = '');
        (!current) && (current = '');
        (!number) && (number = '1');
        (!size) && (size = '10');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftchange_get_all',
            'data': {
                'condition': {
                    'did_name': did,
                    'pre_per_name': pre,
                    'operator_name': current
                },
                'page': {
                    'page_size': size,
                    'page_number': number
                }
            }
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return  false;
            // }
            // return body['data'];
            if (res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    };
    //    班次设置总览接口（下拉）
    DutyService.prototype.getAllshiftSetting = function (size, number) {
        (!number) && (number = '1');
        (!size) && (size = '10');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shift_get_all',
            'data': {
                'status': '启用'
            }
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return  false;
            // }
            // return body['datas'];
            if (res['errcode'] !== '00000') {
                return false;
            }
            return res['datas'];
        });
    };
    //    班次有分页的总览接口
    DutyService.prototype.getAllShiftQuery = function (number, size) {
        (!number) && (number = '1');
        (!size) && (size = '10');
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'type': 'shift_query',
            'data': {
                'condition': {
                    'status': ''
                },
                'page': {
                    'page_size': size,
                    'page_number': number
                }
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return false;
            }
            return res['data'];
        });
    };
    //    获取值班总览接口
    DutyService.prototype.getAllDuty = function (begin, end) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'dutylist_get',
            'data': {
                'begin_time': begin,
                'end_time': end
            }
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return  [];
            // }
            // return body['datas'];
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['datas'];
        });
    };
    //    获取可调班人员接口
    DutyService.prototype.getPremissionPerson = function (did, pre) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftchange_get_per',
            'data': {
                'did': did,
                'pre_per': pre
            }
        }).map(function (res) {
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //     return  false;
            // }
            // return body['datas'];
            if (res['errcode'] !== '00000') {
                return false;
            }
            return res['datas'];
        });
    };
    //    调班新增接口
    DutyService.prototype.addShift = function (did, sid, bt, pp, ppn, cp, cpn) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftchange_add',
            'data': {
                'did': did,
                'sid': sid,
                'begin_time_1': bt,
                'pre_per': pp,
                'pre_per_name': ppn,
                'cur_per': cp,
                'cur_per_name': cpn
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return false;
            }
            return true;
        });
    };
    //    新增班次
    DutyService.prototype.addDiyShift = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shift_add',
            'data': {
                'sid': obj['sid'],
                'name': obj['name'],
                'shift_type': obj['shift_type'],
                'begin_time_1': obj['begin_time_1'],
                'end_time_1': obj['end_time_1'],
                'begin_time_2': obj['begin_time_2'],
                'end_time_2': obj['end_time_2'],
                'overall_time': obj['overall_time'],
                'color': obj['color'],
                'status': obj['status']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //    编辑班次
    DutyService.prototype.editDiyShift = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shift_mod',
            'data': {
                'sid': obj['sid'],
                'name': obj['name'],
                'shift_type': obj['shift_type'],
                'begin_time_1': obj['begin_time_1'],
                'end_time_1': obj['end_time_1'],
                'begin_time_2': obj['begin_time_2'],
                'end_time_2': obj['end_time_2'],
                'overall_time': obj['overall_time'],
                'color': obj['color'],
                'status': obj['status']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //   冻结或解冻班次
    DutyService.prototype.freezeOrUnfreezeDiyShift = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shift_mod_status',
            'data': {
                'sid': obj['sid'],
                'status': obj['status']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //   删除班次
    DutyService.prototype.deletDiyShift = function (array) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shift_del',
            'ids': array
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //  获取登录用户当前值班信息
    DutyService.prototype.getUserDutyMsg = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'dutylist_get_byowner_now'
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res;
        });
    };
    //  获取下一次值班人员
    DutyService.prototype.getNextDutyPerson = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'dutylist_nextshift_per_get'
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res;
        });
    };
    //   新增交接班
    DutyService.prototype.addHandover = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftexchange_add',
            'data': {
                'sid': obj['sid'],
                'shift_id': obj['shift_id'],
                'shiftexchange_type': obj['shiftexchange_type'],
                'acceptor': obj['acceptor'],
                'acceptor_pid': obj['acceptor_pid'],
                'accept_time': obj['accept_time'],
                'content': obj['content'],
                'remarks': obj['remarks'],
                'shift_time': obj['shift_time'],
                'shift_name': obj['shift_name']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //   获取交接班总览接口
    DutyService.prototype.getHandoverOverView = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftexchange_get',
            'data': {
                'condition': {
                    'sid': obj['sid'],
                    'creator': obj['creator'],
                    'acceptor': obj['acceptor'],
                    'status': obj['status']
                },
                'page': {
                    'page_size': obj['page_size'],
                    'page_number': obj['page_number']
                }
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    };
    //   接受或拒绝交接班
    DutyService.prototype.acceptOrRejectHandover = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftexchange_accept',
            'data': {
                'sid': obj['sid'],
                'status': obj['status'],
                'accept_remarks': obj['accept_remarks']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //   保存交接班
    DutyService.prototype.saveHandover = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftexchange_save',
            'data': {
                'sid': obj['sid'],
                'shiftexchange_type': obj['shiftexchange_type'],
                'shift_id': obj['shift_id'],
                'acceptor': obj['acceptor'],
                'acceptor_pid': obj['acceptor_pid'],
                'content': obj['content'],
                'remarks': obj['remarks'],
                'shift_time': obj['shift_time'],
                'shift_name': obj['shift_name']
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //   删除交接班
    DutyService.prototype.deletHandover = function (array) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftexchange_del',
            'ids': array
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return res['errmsg'];
            }
            return res['errcode'];
        });
    };
    //   获得交接班所有状态
    DutyService.prototype.getHandoverStatus = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftexchange_statuslist_get'
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res;
        });
    };
    //   获取交接班带我处理
    DutyService.prototype.getMineHandover = function (obj) {
        var token = this.storageService.getToken('token');
        return this.http.post(this.ip + "/onduty", {
            'access_token': token,
            'type': 'shiftexchange_get_byowner',
            'data': {
                'condition': {
                    'status': obj['status']
                },
                'page': {
                    'page_size': obj['page_size'],
                    'page_number': obj['page_number']
                }
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                return [];
            }
            return res['data'];
        });
    };
    DutyService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */]])
    ], DutyService);
    return DutyService;
}());



/***/ }),

/***/ "../../../../../src/app/duty/handover-overview/handover-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\r\n    <div>\r\n        <span class=\"feature-title\">交接班总览</span>\r\n    </div>\r\n</div>\r\n<div class=\"content-section \" >\r\n    <div class=\"ui-g\">\r\n        <div>\r\n            <label for=\"\">交班单号：</label>\r\n            <input type=\"text\" pInputText  [(ngModel)]=\"searchObj.sid\"/>\r\n        </div>\r\n        <div>\r\n            <label for=\"\">交班人：</label>\r\n            <input type=\"text\" pInputText  [(ngModel)]=\"searchObj.creator\"/>\r\n        </div>\r\n        <div>\r\n            <label for=\"\">接班人：</label>\r\n            <input type=\"text\" pInputText  [(ngModel)]=\"searchObj.acceptor\"/>\r\n        </div>\r\n        <div >\r\n            <label for=\"\">状态：</label>\r\n            <p-dropdown [options]=\"allStatus\" [(ngModel)]=\"searchObj.status\"\r\n                        [style]=\"{'width':'80%'}\"></p-dropdown>\r\n        </div>\r\n        <div>\r\n            <button pButton type=\"button\"  label=\"查询\" (click)=\"search()\"></button>\r\n        </div>\r\n        <div >\r\n            <button pButton type=\"button\"  label=\"新建交班\" (click)=\"jumper()\"></button>\r\n        </div>\r\n    </div>\r\n    <div class=\"ui-grid-row\">\r\n        <app-handover-table></app-handover-table>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/duty/handover-overview/handover-overview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "div[class=\"ui-g\"] {\n  color: #666666;\n  border: 1px solid #e2e2e2;\n  background: white;\n  height: 70px;\n  margin-bottom: 20px;\n  padding-left: 20px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  div[class=\"ui-g\"] div:nth-child(1) {\n    padding-right: 20px;\n    -webkit-box-flex: 2.8;\n        -ms-flex: 2.8;\n            flex: 2.8; }\n    div[class=\"ui-g\"] div:nth-child(1) input {\n      width: 79%; }\n  div[class=\"ui-g\"] div:nth-child(2) {\n    padding-right: 20px;\n    -webkit-box-flex: 2.8;\n        -ms-flex: 2.8;\n            flex: 2.8; }\n    div[class=\"ui-g\"] div:nth-child(2) input {\n      width: 79%; }\n  div[class=\"ui-g\"] div:nth-child(3) {\n    padding-right: 40px;\n    -webkit-box-flex: 2.8;\n        -ms-flex: 2.8;\n            flex: 2.8; }\n    div[class=\"ui-g\"] div:nth-child(3) input {\n      width: 79%; }\n  div[class=\"ui-g\"] div:nth-child(4) {\n    padding-right: 40px;\n    -webkit-box-flex: 2.8;\n        -ms-flex: 2.8;\n            flex: 2.8; }\n  div[class=\"ui-g\"] div:nth-child(5) {\n    padding-right: 40px;\n    -webkit-box-flex: 1.8;\n        -ms-flex: 1.8;\n            flex: 1.8; }\n  div[class=\"ui-g\"] div:nth-child(6) {\n    padding-right: 40px;\n    -webkit-box-flex: 1.8;\n        -ms-flex: 1.8;\n            flex: 1.8;\n    text-align: right; }\n\n@media (min-width: 1366px) {\n  div[class=\"ui-g\"] div:nth-child(1) input {\n    width: 67%; }\n  div[class=\"ui-g\"] div:nth-child(2) input {\n    width: 73%; }\n  div[class=\"ui-g\"] div:nth-child(3) input {\n    width: 73%; } }\n\n@media (min-width: 1440px) {\n  div[class=\"ui-g\"] div:nth-child(1) input {\n    width: 69%; }\n  div[class=\"ui-g\"] div:nth-child(2) input {\n    width: 75%; }\n  div[class=\"ui-g\"] div:nth-child(3) input {\n    width: 75%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/handover-overview/handover-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandoverOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__searchObj_model__ = __webpack_require__("../../../../../src/app/duty/searchObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HandoverOverviewComponent = (function (_super) {
    __extends(HandoverOverviewComponent, _super);
    function HandoverOverviewComponent(router, activedRouter, eventBusService, dutyService, c, m) {
        var _this = _super.call(this, c, m) || this;
        _this.router = router;
        _this.activedRouter = activedRouter;
        _this.eventBusService = eventBusService;
        _this.dutyService = dutyService;
        _this.c = c;
        _this.m = m;
        return _this;
    }
    HandoverOverviewComponent.prototype.ngOnInit = function () {
        this.searchObj = new __WEBPACK_IMPORTED_MODULE_3__searchObj_model__["a" /* SearchObjModel */]();
        this.getStatus();
    };
    HandoverOverviewComponent.prototype.getStatus = function () {
        var _this = this;
        this.dutyService.getHandoverStatus().subscribe(function (res) {
            if (res['errcode'] === '00000') {
                _this.allStatus = __WEBPACK_IMPORTED_MODULE_5__services_PUblicMethod__["a" /* PUblicMethod */].formateDropDown(res['datas']);
                _this.searchObj.status = _this.allStatus[0]['value'];
            }
            else {
                _this.alert("\u83B7\u53D6\u6240\u6709\u72B6\u6001\u5931\u8D25 + " + res['errmsg'], 'error');
            }
        });
    };
    HandoverOverviewComponent.prototype.search = function () {
        this.eventBusService.handover.next(this.searchObj);
    };
    HandoverOverviewComponent.prototype.jumper = function () {
        this.router.navigate(['../createhandover'], { relativeTo: this.activedRouter });
    };
    HandoverOverviewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-handover-overview',
            template: __webpack_require__("../../../../../src/app/duty/handover-overview/handover-overview.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/handover-overview/handover-overview.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_2__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_7_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__["MessageService"]])
    ], HandoverOverviewComponent);
    return HandoverOverviewComponent;
}(__WEBPACK_IMPORTED_MODULE_6__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/duty/handoverobj.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandoverobjModel; });
var HandoverobjModel = (function () {
    function HandoverobjModel(obj) {
        this.sid = obj && obj['sid'] || '';
        this.creator = obj && obj['creator'] || '';
        this.acceptor = obj && obj['acceptor'] || '';
        this.status = obj && obj['status'] || '';
        this.creator_pid = obj && obj['creator_pid'] || '';
        this.creator_org = obj && obj['creator_org'] || '';
        this.creator_org_oid = obj && obj['creator_org_oid'] || '';
        this.create_time = obj && obj['create_time'] || '';
        this.shiftexchange_type = obj && obj['shiftexchange_type'] || '';
        this.acceptor_pid = obj && obj['acceptor_pid'] || '';
        this.accept_time = obj && obj['accept_time'] || '';
        this.content = obj && obj['content'] || '';
        this.remarks = obj && obj['remarks'] || '';
        this.shift_name = obj && obj['shift_name'] || '';
        this.shift_time = obj && obj['shift_time'] || '';
        this.shift_id = obj && obj['shift_id'] || '';
        this.accept_remarks = obj && obj['accept_remarks'] || '';
    }
    return HandoverobjModel;
}());



/***/ }),

/***/ "../../../../../src/app/duty/min-handover-overview/min-handover-overview.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"feature-title\">我的交接班</span>\n    </div>\n</div>\n<div class=\"content-section \" >\n    <div class=\"ui-grid-row\">\n        <app-handover-table [wichDatas]=\"mine\"></app-handover-table>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/duty/min-handover-overview/min-handover-overview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".content-section > div[class=\"ui-grid-row\"] {\n  margin-top: 2vw; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/min-handover-overview/min-handover-overview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MinHandoverOverviewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MinHandoverOverviewComponent = (function () {
    function MinHandoverOverviewComponent() {
        this.mine = '0';
    }
    MinHandoverOverviewComponent.prototype.ngOnInit = function () {
    };
    MinHandoverOverviewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-min-handover-overview',
            template: __webpack_require__("../../../../../src/app/duty/min-handover-overview/min-handover-overview.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/min-handover-overview/min-handover-overview.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MinHandoverOverviewComponent);
    return MinHandoverOverviewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/overview-duty/overview-duty.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \">\n    <div>\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>值班总览  </span>\n    </div>\n</div>\n<div class=\"content-section implementation GridDemo\" id=\"overview\">\n        <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\">\n            <div class=\"ui-grid-col-12\">\n                <span *ngFor=\"let shift of shifts\">\n                     <!--<span  class=\"span_bg\" [style.background]=\"shift.color\">{{shift.sid}}</span>-->\n                     <span  class=\"span_bg\" [style.background]=\"shift.color\">{{shift.sid}}: {{shift.begin_time_1}}-{{shift.end_time_1}}</span>\n                     <span  class=\"span_bg\" *ngIf=\"shift.shift_type == '两头班'\" [style.background]=\"shift.color\">{{shift.sid}}: {{shift.begin_time_2}} - {{shift.end_time_2}}</span>\n                </span>\n            </div>\n            <p-schedule [events]=\"events\"\n                        ngClass=\"test\"\n                        [header]=\"headerConfig\"\n                        [options]=\"optionsConfig\" (onEventClick)=\"onEventClick($event)\"\n                        (onViewRender)=\"onClickButton($event)\" [eventLimit]=\"4\"\n            ></p-schedule>\n        </div>\n    <p-dialog header=\"调班\" [(visible)]=\"display\" modal=\"modal\" width=\"500\" [responsive]=\"true\">\n        <div class=\"ui-grid-row\">\n            <div class=\"ui-grid-col-12\">\n                <label for=\"\">计划编号：</label>\n                <input type=\"text\" pInputText  [style.width.%]=\"90\" placeholder=\"请输入计划名\" [(ngModel)]=\"sDutys['snumber']\" readonly/>\n            </div>\n            <div class=\"ui-grid-col-12\">\n                <label for=\"\">调班日期：</label>\n                <input type=\"text\" pInputText  [style.width.%]=\"90\" placeholder=\"请输入计划名\"  [(ngModel)]=\"sDutys['sdate']\" readonly/>\n            </div>\n            <div class=\"ui-grid-col-12\">\n                <label for=\"\">调班班次：</label>\n                <input type=\"text\" pInputText  [style.width.%]=\"90\"\n                       placeholder=\"请输入计划名\"\n                       [(ngModel)]=\"sDutys['sshifts']\" readonly/>\n            </div>\n            <div class=\"ui-grid-col-12\">\n                <label for=\"\">当前值班人：</label>\n                <input type=\"text\" pInputText  [style.width.%]=\"90\"\n                       placeholder=\"请输入计划名\"\n                       [(ngModel)]=\"sDutys['scurrentPerson']\" readonly/>\n            </div>\n            <div class=\"ui-grid-col-12\">\n                    <label for=\"\">\n                        <span class=\"red_start\">*</span>\n                        调整后值班人：\n                    </label>\n                    <p-dropdown [options]=\"statusOptions\" [(ngModel)]=\"selectedStatus\" placeholder=\"请选择人员\"\n                                optionLabel=\"name\" [style]=\"{'width':'100%'}\"></p-dropdown>\n            </div>\n        </div>\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"changeShift()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"display=false\" label=\"取消\" class=\"ui-button-secondary\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"msgPop\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/duty/overview-duty/overview-duty.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".lh_1d7vw {\n  line-height: 1.6vw; }\n\n.te_r {\n  text-align: right; }\n\n.pr_1vw {\n  padding-right: 1vw; }\n\n.red_start {\n  color: red; }\n\n#overview div[class~=\"ui-grid\"] div:nth-child(1) {\n  background: white;\n  border: solid 1px #e2e2e2;\n  padding-top: 5px;\n  padding-left: 5px;\n  padding-right: 5px;\n  margin-bottom: 10px; }\n\n.span_bg {\n  display: inline-block;\n  width: auto;\n  text-align: left;\n  color: #2d2d2d;\n  height: 26px;\n  margin-bottom: 5px;\n  font-size: 12px;\n  padding-left: 10px;\n  padding-right: 10px;\n  word-wrap: break-word;\n  line-height: 26px; }\n\np-dialog /deep/ div[class=\"ui-grid-row\"] {\n  color: #666;\n  font-weight: normal; }\n  p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(1),\n  p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(2),\n  p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(3),\n  p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(4) {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    margin-bottom: 10px; }\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(1) label,\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(2) label,\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(3) label,\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(4) label {\n      -webkit-box-flex: 2.7;\n          -ms-flex: 2.7;\n              flex: 2.7;\n      text-align: right;\n      padding-right: 10px; }\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(1) input,\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(2) input,\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(3) input,\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(4) input {\n      -webkit-box-flex: 8;\n          -ms-flex: 8;\n              flex: 8; }\n  p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(5) {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    margin-bottom: 10px; }\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(5) > label {\n      -webkit-box-flex: 2.7;\n          -ms-flex: 2.7;\n              flex: 2.7;\n      text-align: right;\n      padding-right: 10px; }\n    p-dialog /deep/ div[class=\"ui-grid-row\"] div[class=\"ui-grid-col-12\"]:nth-child(5) p-dropdown {\n      -webkit-box-flex: 8;\n          -ms-flex: 8;\n              flex: 8; }\n\np-dialog /deep/ div[class=\"ui-dialog-content ui-widget-content\"] {\n  overflow: inherit; }\n\n@media screen and (max-width: 1440px) {\n  .span_bg {\n    width: auto;\n    height: 26px;\n    line-height: 26px;\n    padding-left: 8px; } }\n\n@media screen and (max-width: 1366px) {\n  .span_bg {\n    width: auto;\n    padding-left: 8px;\n    height: 26px;\n    line-height: 26px; } }\n\n.test {\n  background: white;\n  height: auto;\n  width: auto; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/overview-duty/overview-duty.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverviewDutyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OverviewDutyComponent = (function () {
    function OverviewDutyComponent(dutyService) {
        this.dutyService = dutyService;
        this.msgPop = []; // 验证消息弹框提示
        this.sDutys = {};
    }
    OverviewDutyComponent.prototype.ngOnInit = function () {
        this.windowSize = window.innerWidth;
        if (this.windowSize < 1024) {
            this.width = this.windowSize * 0.9;
        }
        else {
            this.width = this.windowSize * 0.8;
        }
        this.display = false;
        this.headerConfig = {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        };
        this.optionsConfig = {
            buttonText: {
                year: '年',
                today: '今天',
                month: '月',
                week: '周',
                day: '日',
            },
            editable: true,
            eventLimit: true,
            height: 800,
            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
            dayNames: ['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
            dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
            dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
            titleFormat: 'YYYY年M月'
        };
        this.getAllshiftSetting();
        this.timeObj = this.getCurrentNextTime();
        this.getAlldutys();
    };
    OverviewDutyComponent.prototype.getCurrentNextTime = function () {
        var monthArra = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        var currentTime = new Date();
        var curMontDays = new Date(currentTime.getFullYear(), monthArra[currentTime.getMonth()], 0).getDate();
        curMontDays = curMontDays.toString().length >= 2 ? curMontDays : '0' + curMontDays;
        var currentMonth = (monthArra[currentTime.getMonth()]).toString().length >= 2 ? monthArra[currentTime.getMonth()] : '0' + monthArra[currentTime.getMonth()];
        this.beginTime = currentTime.getFullYear() + '-' + currentMonth + '-' + '01';
        this.endTime = currentTime.getFullYear() + '-' + currentMonth + '-' + curMontDays;
        return { begin: this.beginTime, end: this.endTime };
    };
    OverviewDutyComponent.prototype.getAllshiftSetting = function () {
        var _this = this;
        this.dutyService.getAllshiftSetting().subscribe(function (res) {
            _this.shifts = res;
        });
    };
    OverviewDutyComponent.prototype.getAlldutys = function () {
        var _this = this;
        this.dutyService.getAllDuty(this.timeObj.begin, this.timeObj.end).subscribe(function (res) {
            _this.events = res;
        });
    };
    OverviewDutyComponent.prototype.onEventClick = function (event) {
        var _this = this;
        this.display = true;
        this.dutyService.getPremissionPerson(event['calEvent']['did'], event['calEvent']['per']).subscribe(function (res) {
            _this.statusOptions = res;
        });
        this.eventObj = event['calEvent'];
        this.sDutys = {
            snumber: event['calEvent']['did'],
            sdate: event['calEvent']['begin_time_1'],
            sshifts: event['calEvent']['sid'],
            scurrentPerson: event['calEvent']['per_name']
        };
    };
    OverviewDutyComponent.prototype.onClickButton = function (event) {
        var _this = this;
        this.beginTime = event.view.start.format('YYYY-MM-DD');
        this.endTime = event.view.end.format('YYYY-MM-DD');
        // console.log(event.view.start.format('YYYY-MM-DD'));
        // console.log(event.view.end.format('YYYY-MM-DD'));
        this.dutyService.getAllDuty(this.beginTime, this.endTime).subscribe(function (res) {
            _this.events = res;
        });
    };
    OverviewDutyComponent.prototype.changeShift = function () {
        var _this = this;
        console.dir(this.eventObj);
        this.dutyService.addShift(this.eventObj['did'], this.eventObj['sid'], this.eventObj['begin_time_1'], this.eventObj['per'], this.eventObj['per_name'], this.selectedStatus['pid'], this.selectedStatus['name']).subscribe(function (res) {
            if (res) {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '调班成功' });
                _this.getAlldutys();
            }
            else {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '调班失败' });
            }
        });
        this.display = false;
    };
    OverviewDutyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-overview-duty',
            template: __webpack_require__("../../../../../src/app/duty/overview-duty/overview-duty.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/overview-duty/overview-duty.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__duty_service__["a" /* DutyService */]])
    ], OverviewDutyComponent);
    return OverviewDutyComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/public/btn-accept/btn-accept.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"jumper()\" label=\"接受\"\n        *ngIf=\"data.status == '待接班'\"></button>\n"

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-accept/btn-accept.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-accept/btn-accept.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnAcceptComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handoverobj_model__ = __webpack_require__("../../../../../src/app/duty/handoverobj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var BtnAcceptComponent = (function (_super) {
    __extends(BtnAcceptComponent, _super);
    function BtnAcceptComponent(router, dutyService, eventBusService, c, m) {
        var _this = _super.call(this, c, m) || this;
        _this.router = router;
        _this.dutyService = dutyService;
        _this.eventBusService = eventBusService;
        _this.c = c;
        _this.m = m;
        return _this;
    }
    BtnAcceptComponent.prototype.ngOnInit = function () {
        this.handoverObj = new __WEBPACK_IMPORTED_MODULE_3__handoverobj_model__["a" /* HandoverobjModel */]();
    };
    BtnAcceptComponent.prototype.jumper = function () {
        var _this = this;
        this.handoverObj = new __WEBPACK_IMPORTED_MODULE_3__handoverobj_model__["a" /* HandoverobjModel */](this.data);
        this.handoverObj.status = '接受';
        this.confirmationService.confirm({
            message: '确认接受吗？',
            accept: function () {
                _this.requestAccept();
            },
            reject: function () {
            }
        });
    };
    BtnAcceptComponent.prototype.requestAccept = function () {
        var _this = this;
        this.dutyService.acceptOrRejectHandover(this.handoverObj).subscribe(function (res) {
            if (res === '00000') {
                _this.alert('接受成功');
                _this.eventBusService.handover.next(true);
            }
            else {
                _this.alert("\u63A5\u53D7\u5931\u8D25 + " + res, 'error');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnAcceptComponent.prototype, "data", void 0);
    BtnAcceptComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-accept',
            template: __webpack_require__("../../../../../src/app/duty/public/btn-accept/btn-accept.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/public/btn-accept/btn-accept.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_7__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__["MessageService"]])
    ], BtnAcceptComponent);
    return BtnAcceptComponent;
}(__WEBPACK_IMPORTED_MODULE_4__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/duty/public/btn-delete/btn-delete.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"jumper()\" label=\"删除\"\n        *ngIf=\"data.status == '新建'\"></button>\n"

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-delete/btn-delete.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-delete/btn-delete.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnDeleteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__handoverobj_model__ = __webpack_require__("../../../../../src/app/duty/handoverobj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var BtnDeleteComponent = (function (_super) {
    __extends(BtnDeleteComponent, _super);
    function BtnDeleteComponent(router, dutyService, eventBusService, c, m) {
        var _this = _super.call(this, c, m) || this;
        _this.router = router;
        _this.dutyService = dutyService;
        _this.eventBusService = eventBusService;
        _this.c = c;
        _this.m = m;
        return _this;
    }
    BtnDeleteComponent.prototype.ngOnInit = function () {
        this.handoverObj = new __WEBPACK_IMPORTED_MODULE_2__handoverobj_model__["a" /* HandoverobjModel */]();
    };
    BtnDeleteComponent.prototype.jumper = function () {
        var _this = this;
        this.handoverObj = new __WEBPACK_IMPORTED_MODULE_2__handoverobj_model__["a" /* HandoverobjModel */](this.data);
        this.confirmationService.confirm({
            message: '确认删除吗？',
            accept: function () {
                _this.requestDelete();
            },
            reject: function () {
            }
        });
    };
    BtnDeleteComponent.prototype.requestDelete = function () {
        var _this = this;
        this.dutyService.deletHandover([this.handoverObj['sid']]).subscribe(function (res) {
            if (res === '00000') {
                _this.alert('删除成功');
                _this.eventBusService.handover.next(true);
            }
            else {
                _this.alert("\u5220\u9664\u5931\u8D25 + " + res, 'error');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnDeleteComponent.prototype, "data", void 0);
    BtnDeleteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-delete',
            template: __webpack_require__("../../../../../src/app/duty/public/btn-delete/btn-delete.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/public/btn-delete/btn-delete.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_3__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_4__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__["MessageService"]])
    ], BtnDeleteComponent);
    return BtnDeleteComponent;
}(__WEBPACK_IMPORTED_MODULE_7__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/duty/public/btn-edit/btn-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"jumper()\" label=\"编辑\"\n        *ngIf=\"data.status == '新建'\"></button>"

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-edit/btn-edit.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-edit/btn-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BtnEditComponent = (function () {
    function BtnEditComponent(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
    }
    BtnEditComponent.prototype.ngOnInit = function () {
    };
    BtnEditComponent.prototype.jumper = function () {
        this.router.navigate(['../createhandover'], { queryParams: { datas: JSON.stringify(this.data) }, relativeTo: this.activatedRoute });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnEditComponent.prototype, "data", void 0);
    BtnEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-edit',
            template: __webpack_require__("../../../../../src/app/duty/public/btn-edit/btn-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/public/btn-edit/btn-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"]])
    ], BtnEditComponent);
    return BtnEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/public/btn-rehandover/btn-rehandover.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"jumper()\" label=\"再交接\"\n        *ngIf=\"data.status == '拒绝'\"></button>"

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-rehandover/btn-rehandover.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-rehandover/btn-rehandover.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnRehandoverComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BtnRehandoverComponent = (function () {
    function BtnRehandoverComponent(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
    }
    BtnRehandoverComponent.prototype.ngOnInit = function () {
    };
    BtnRehandoverComponent.prototype.jumper = function () {
        this.router.navigate(['../createhandover'], { queryParams: { datas: JSON.stringify(this.data) }, relativeTo: this.activatedRoute });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnRehandoverComponent.prototype, "data", void 0);
    BtnRehandoverComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-rehandover',
            template: __webpack_require__("../../../../../src/app/duty/public/btn-rehandover/btn-rehandover.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/public/btn-rehandover/btn-rehandover.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"]])
    ], BtnRehandoverComponent);
    return BtnRehandoverComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/public/btn-reject/btn-reject.component.html":
/***/ (function(module, exports) {

module.exports = "<button pButton type=\"button\"\n        (click)=\"jumper()\" label=\"拒绝\"\n        *ngIf=\"data.status == '待接班'\"></button>\n<p-dialog [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"350\" [minWidth]=\"200\">\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n        <div class=\"form-group\" >\n            <label class=\"col-sm-4 control-label\">\n                <span ngClass=\"start_red\">*</span>拒绝原因：</label>\n            <div class=\"col-sm-8 ui-fluid-no-padding\">\n                <div class=\"col-sm-12  ui-fluid\">\n                         <textarea [rows]=\"2\" pInputTextarea\n                                   autoResize=\"autoResize\"\n                                   formControlName=\"reason\"\n                                   [(ngModel)]=\"handoverObj.accept_remarks\"></textarea>\n                </div>\n            </div>\n            <app-field-error-display\n                    [displayError]=\"isFieldValid('reason')\"\n                    errorMsg=\"不能为空\"\n            ></app-field-error-display>\n        </div>\n    </form>\n    <p-footer>\n        <button  pButton type=\"button\" (click)=\"reject()\" label=\"确定\" ></button>\n        <button   pButton type=\"button\" (click)=\"onHide()\" label=\"取消\" ></button>\n    </p-footer>\n</p-dialog>"

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-reject/btn-reject.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/public/btn-reject/btn-reject.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BtnRejectComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__handoverobj_model__ = __webpack_require__("../../../../../src/app/duty/handoverobj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var BtnRejectComponent = (function (_super) {
    __extends(BtnRejectComponent, _super);
    function BtnRejectComponent(router, dutyService, eventBusService, c, m) {
        var _this = _super.call(this, c, m) || this;
        _this.router = router;
        _this.dutyService = dutyService;
        _this.eventBusService = eventBusService;
        _this.c = c;
        _this.m = m;
        _this.display = false;
        _this.fb = new __WEBPACK_IMPORTED_MODULE_8__angular_forms__["FormBuilder"]();
        return _this;
    }
    BtnRejectComponent.prototype.ngOnInit = function () {
        this.handoverObj = new __WEBPACK_IMPORTED_MODULE_2__handoverobj_model__["a" /* HandoverobjModel */]();
        this.initForm();
    };
    BtnRejectComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            reason: [null, __WEBPACK_IMPORTED_MODULE_8__angular_forms__["Validators"].required]
        });
    };
    BtnRejectComponent.prototype.isFieldValid = function (field) {
        return __WEBPACK_IMPORTED_MODULE_9__services_PUblicMethod__["a" /* PUblicMethod */].isFieldValid(this.myForm, 'reason');
    };
    BtnRejectComponent.prototype.jumper = function () {
        this.handoverObj = new __WEBPACK_IMPORTED_MODULE_2__handoverobj_model__["a" /* HandoverobjModel */](this.data);
        this.handoverObj.status = '拒绝';
        this.display = true;
    };
    BtnRejectComponent.prototype.onHide = function () {
        this.display = false;
        this.initForm();
    };
    BtnRejectComponent.prototype.reject = function () {
        if (this.myForm.valid) {
            this.requestReject();
        }
        else {
            __WEBPACK_IMPORTED_MODULE_9__services_PUblicMethod__["a" /* PUblicMethod */].validateAllFormFields(this.myForm);
        }
    };
    BtnRejectComponent.prototype.requestReject = function () {
        var _this = this;
        this.dutyService.acceptOrRejectHandover(this.handoverObj).subscribe(function (res) {
            if (res === '00000') {
                _this.alert('拒绝成功');
                _this.eventBusService.handover.next(true);
            }
            else {
                _this.alert("\u62D2\u7EDD\u5931\u8D25 + " + res, 'error');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BtnRejectComponent.prototype, "data", void 0);
    BtnRejectComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-btn-reject',
            template: __webpack_require__("../../../../../src/app/duty/public/btn-reject/btn-reject.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/public/btn-reject/btn-reject.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_4__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__["MessageService"]])
    ], BtnRejectComponent);
    return BtnRejectComponent;
}(__WEBPACK_IMPORTED_MODULE_7__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/duty/public/handover-table/handover-table.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dataTable [value]=\"scheduleDatas\" [responsive]=\"true\" id=\"malfunctionTable\">\n\n    <p-column selectionMode=\"multiple\" ></p-column>\n    <p-column field=\"sid\" header=\"交接单号\" [sortable]=\"true\">\n        <ng-template let-data=\"rowData\" pTemplate=\"operator\">\n            <span (click)=\"onOperate(data)\" class=\"ui-cursor-point\" >{{data.sid}}</span>\n        </ng-template>\n    </p-column>\n    <p-column field=\"creator\" header=\"交班人\" [sortable]=\"true\"></p-column>\n    <p-column field=\"create_time\" header=\"交班时间\" [sortable]=\"true\"></p-column>\n    <p-column field=\"acceptor\" header=\"接班人\" [sortable]=\"true\"></p-column>\n    <p-column field=\"accept_time\" header=\"接班时间\" [sortable]=\"true\"></p-column>\n    <p-column field=\"shiftexchange_type\" header=\"交班类型\" [sortable]=\"true\"></p-column>\n    <p-column field=\"status\" header=\"状态\" [sortable]=\"true\"></p-column>\n    <p-column field=\"color\" header=\"操作项\" [style]=\"{'width':'18vw'}\">\n        <ng-template let-data=\"rowData\" pTemplate=\"operator\">\n            <app-btn-accept [data]=\"data\"></app-btn-accept>\n            <app-btn-delete [data]=\"data\"></app-btn-delete>\n            <app-btn-edit [data]=\"data\"></app-btn-edit>\n            <app-btn-rehandover [data]=\"data\"></app-btn-rehandover>\n            <app-btn-reject [data]=\"data\"></app-btn-reject>\n        </ng-template>\n    </p-column>\n    <ng-template pTemplate=\"emptymessage\">\n        当前没有数据\n    </ng-template>\n</p-dataTable>\n<p-paginator rows=\"10\" totalRecords=\"{{total}}\" [rowsPerPageOptions]=\"[10,20,30]\" pageLinkSize=\"3\"  (onPageChange)=\"paginate($event)\"></p-paginator>\n"

/***/ }),

/***/ "../../../../../src/app/duty/public/handover-table/handover-table.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#malfunctionTable /deep/ table thead tr th:nth-child(1) {\n  width: 2.6%; }\n\n#malfunctionTable /deep/ table thead tr th:nth-child(4),\n#malfunctionTable /deep/ table thead tr th:nth-child(6),\n#malfunctionTable /deep/ table thead tr th:nth-child(7) {\n  width: 10%; }\n\n#malfunctionTable /deep/ table thead tr th:nth-child(5) {\n  width: 11%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/public/handover-table/handover-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandoverTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__searchObj_model__ = __webpack_require__("../../../../../src/app/duty/searchObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__ = __webpack_require__("../../../../../src/app/services/event-bus.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HandoverTableComponent = (function () {
    function HandoverTableComponent(dutyService, eventBusService, router, activatedRoute) {
        this.dutyService = dutyService;
        this.eventBusService = eventBusService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.scheduleDatas = []; // 表格渲染数据
    }
    HandoverTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.searchObj = new __WEBPACK_IMPORTED_MODULE_2__searchObj_model__["a" /* SearchObjModel */]();
        this.eventBusService.handover.subscribe(function (data) {
            (typeof data === 'boolean') && (_this.getOverviewDatas());
            if (typeof data === 'object') {
                _this.searchObj = data;
                _this.getOverviewDatas();
            }
        });
        (!this.wichDatas) && (this.getOverviewDatas());
        (this.wichDatas) && (this.getMineDatas());
        // this.getOverviewDatas();
        // this.getMineDatas();
    };
    HandoverTableComponent.prototype.getOverviewDatas = function () {
        var _this = this;
        this.dutyService.getHandoverOverView(this.searchObj).subscribe(function (res) {
            if (res) {
                _this.resetPage(res);
            }
            else {
                _this.scheduleDatas = [];
            }
        });
    };
    HandoverTableComponent.prototype.getMineDatas = function () {
        var _this = this;
        if (this.wichDatas) {
            this.searchObj.status = '待接班';
            this.dutyService.getMineHandover(this.searchObj).subscribe(function (res) {
                if (res) {
                    _this.resetPage(res);
                }
                else {
                    _this.scheduleDatas = [];
                }
            });
        }
    };
    HandoverTableComponent.prototype.resetPage = function (res) {
        if ('items' in res) {
            this.scheduleDatas = res.items;
            this.total = res['page']['total'];
            this.page_size = res['page']['page_size'];
            this.page_total = res['page']['page_total'];
        }
    };
    HandoverTableComponent.prototype.paginate = function (event) {
        this.searchObj.page_size = event.rows.toString();
        this.searchObj.page_number = (event.page + 1).toString();
        this.getOverviewDatas();
    };
    HandoverTableComponent.prototype.onOperate = function (data) {
        this.router.navigate(['../viewhandover'], { queryParams: { datas: JSON.stringify(data) }, relativeTo: this.activatedRoute });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], HandoverTableComponent.prototype, "wichDatas", void 0);
    HandoverTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-handover-table',
            template: __webpack_require__("../../../../../src/app/duty/public/handover-table/handover-table.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/public/handover-table/handover-table.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_3__services_event_bus_service__["a" /* EventBusService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["ActivatedRoute"]])
    ], HandoverTableComponent);
    return HandoverTableComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/public/search-form/search-form.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  search-form works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/duty/public/search-form/search-form.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/public/search-form/search-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SearchFormComponent = (function () {
    function SearchFormComponent() {
    }
    SearchFormComponent.prototype.ngOnInit = function () {
    };
    SearchFormComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-search-form',
            template: __webpack_require__("../../../../../src/app/duty/public/search-form/search-form.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/public/search-form/search-form.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SearchFormComponent);
    return SearchFormComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/schedule-add/schedule-add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \">\r\n    <div>\r\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>新增计划  </span>\r\n    </div>\r\n</div>\r\n<p-progressBar mode=\"indeterminate\" [hidden]=\"progressBar\"></p-progressBar>\r\n<div class=\"content-section implementation GridDemo\" id=\"dutyAdd\">\r\n    <div class=\"title col-sm-12\">\r\n        <div class=\"col-sm-1\">\r\n            <span>基本信息</span>\r\n        </div>\r\n        <div class=\"col-sm-11 pull-right\">\r\n            <button type=\"button\"\r\n                    class=\"pull-right\"\r\n                    pButton\r\n                    [routerLink]=\"['/index/duty/manage']\"\r\n                    icon=\"fa-close\"\r\n                    style=\"width: 30px\"></button>\r\n        </div>\r\n    </div>\r\n    <p-messages [(value)]=\"msgs\"></p-messages>\r\n    <form [formGroup]=\"newSchdule\" class=\"form-horizontal\">\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-1 control-label\" >计划编号：</label>\r\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\r\n                <input class=\" form-control cursor_not_allowed\"\r\n                       type=\"text\" formControlName=\"scheduleId\"\r\n                       pInputText id=\"did\"\r\n                       [(ngModel)]=\"defaultId\"\r\n                       placeholder=\"编号在保存后自动生成\"\r\n                       readonly/>\r\n            </div>\r\n            <label class=\"col-sm-1 control-label\">排班人员：</label>\r\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\r\n                <input  class=\"form-control cursor_not_allowed\"\r\n                        formControlName=\"scheduleCreator\"\r\n                        type=\"text\" pInputText id=\"creator\"\r\n                        readonly\r\n                        [(ngModel)]=\"defaultCreator\"\r\n                        placeholder=\"首次创建系统自动生成排班人员\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-1 control-label \">\r\n                <span ngClass=\"start_red\">*</span>\r\n                计划名称：\r\n            </label>\r\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\r\n                <input formControlName=\"schduleName\"\r\n                       class=\"form-control\"\r\n                       type=\"text\"\r\n                       pInputText id=\"name\"\r\n                       placeholder=\"请填写计划名称\"\r\n                       [class.my-dirty]=\"isName\"/>\r\n            </div>\r\n            <label class=\"col-sm-1 control-label\">\r\n                <span ngClass=\"start_red\">*</span>\r\n                排班时间：\r\n            </label>\r\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\r\n                <input class=\" form-control cursor_not_allowed\"\r\n                       type=\"text\"\r\n                       formControlName=\"scheduleCreateTime\"\r\n                       pInputText id=\"create_time\"\r\n                       readonly\r\n                       [ngModel]=\"defaultCreateTime\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-1 control-label\">\r\n                <span ngClass=\"start_red\">*</span>\r\n                值班部门：\r\n            </label>\r\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\r\n                <div class=\"col-sm-8 ui-fluid ui-fluid-no-padding\">\r\n                    <input type=\"text\"\r\n                           class=\"form-control\"\r\n                           pInputText\r\n                           name=\"department\"\r\n                           placeholder=\"请选择值班部门\"\r\n                           [(ngModel)]=\"selectedDepartment\"\r\n                           readonly class=\"cursor_not_allowed\"\r\n                           formControlName=\"schduleDepartment\" />\r\n                </div>\r\n                <div class=\"col-sm-2 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button pButton type=\"button\" ngClass=\"ui-g-12  ui-fluid-no-padding\" (click)=\"showTreeDialog()\" label=\"选择\"></button>\r\n                </div>\r\n                <div class=\"col-sm-2 ui-fluid-no-padding ui-padding-10px\">\r\n                    <button pButton type=\"button\" ngClass=\"ui-g-12  ui-fluid-no-padding\" (click)=\"clearTreeDialog()\" label=\"清空\"></button>\r\n                </div>\r\n            </div>\r\n            <label class=\"col-sm-1 control-label\">\r\n                <span ngClass=\"start_red\">*</span>\r\n                审批人：\r\n            </label>\r\n            <div class=\"col-sm-5 ui-fluid ui-no-padding-left-15px\">\r\n                <p-autoComplete\r\n                        formControlName=\"scheduleApprovor\"\r\n                        [(ngModel)]=\"approveSelectedMultipleOption\"                                                            [suggestions]=\"approveSearchOptions\"\r\n                        (completeMethod)=\"filterApproverMultiple($event)\"\r\n                        [minLength]=\"1\"\r\n                        [required]=\"true\"\r\n                        placeholder=\"请选择审批人\"\r\n                        field=\"name\"\r\n                        [multiple]=\"true\"\r\n                        name=\"test\"\r\n                        [dropdown]=\"true\"\r\n                        id=\"approval\">\r\n                </p-autoComplete>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-1 control-label\">\r\n                <span ngClass=\"start_red\">*</span>\r\n                开始时间：\r\n            </label>\r\n            <div class=\"col-sm-5 ui-fluid ui-no-padding-left-15px\">\r\n                <p-calendar\r\n                        formControlName=\"schduleStart\"\r\n                        [(ngModel)]=\"beginTime\"\r\n                        [showIcon]=\"true\" [locale]=\"zh\" name=\"beginTime\"\r\n                        [styleClass]=\"'schedule-add'\"\r\n                        dateFormat=\"yy-mm-dd\"\r\n                        [required]=\"true\"\r\n                        [minDate]=\"initStartTime\"\r\n                        (onSelect) = \"startTimeSelected(beginTime)\"\r\n                        id=\"startCalendar\">\r\n                </p-calendar>\r\n            </div>\r\n            <label class=\"col-sm-1 control-label\">\r\n                <span ngClass=\"start_red\">*</span>\r\n                结束时间：\r\n            </label>\r\n             <div class=\"col-sm-5 ui-no-padding-left-15px\">\r\n                <p-calendar\r\n                        ngClass=\"ui-fluid\"\r\n                        formControlName=\"schduleEnd\"\r\n                        [(ngModel)]=\"endTime\"\r\n                        [showIcon]=\"true\"\r\n                        [locale]=\"zh\"\r\n                        name=\"end_time\"\r\n                        [styleClass]=\"'schedule-add'\"\r\n                        dateFormat=\"yy-mm-dd\"\r\n                        [required]=\"true\"\r\n                        [minDate]=\"minEndTime\"\r\n                        id=\"endCalendar\">\r\n                </p-calendar>\r\n             </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-1 control-label\">\r\n                <span ngClass=\"start_red\">*</span>\r\n                排班班次：\r\n            </label>\r\n            <div class=\"col-sm-11 ui-no-padding-left-15px\">\r\n                <p-autoComplete\r\n                        ngClass=\"ui-fluid\"\r\n                        formControlName=\"schduleShift\"\r\n                        [(ngModel)]=\"shiftSelectedMultipleOption\"\r\n                        [suggestions]=\"shiftSearchOptions\"\r\n                        (completeMethod)=\"filterShiftMultipe($event)\"\r\n                        [minLength]=\"1\" [required]=\"true\"\r\n                        placeholder=\"请选择排班班次\"\r\n                        field=\"name\"\r\n                        [multiple]=\"true\"\r\n                        name=\"test\"\r\n                        [dropdown]=\"true\">\r\n                </p-autoComplete>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\" >\r\n            <label class=\"col-sm-1 control-label\">关联排班：</label>\r\n            <div class=\"col-sm-11 ui-no-padding-left-15px\">\r\n                <p-autoComplete\r\n                        ngClass=\"ui-fluid\"\r\n                        formControlName=\"schduleRelative\"\r\n                        [(ngModel)]=\"relativeSelectedMultipleOption\"\r\n                        [suggestions]=\"relativeSearchOptions\"\r\n                        (completeMethod)=\"filterRelativeMutipe($event)\"\r\n                        [minLength]=\"1\"\r\n                        placeholder=\"请选择关联排班\"\r\n                        field=\"name\"\r\n                        name=\"test\"\r\n                        [dropdown]=\"true\"\r\n                >\r\n                </p-autoComplete>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <div class=\"col-sm-12 ui-no-padding-left-15px\">\r\n                <button\r\n                        class=\"pull-right\"\r\n                        pButton\r\n                        type=\"button\"\r\n                        (click)=\"downloadModel()\"\r\n                        label=\"生成排班表\"\r\n                        [disabled]=\"hadSaved\"></button>\r\n                <!--<div class=\"\">-->\r\n                <p-fileUpload\r\n                        class=\"pull-right ui-margin-right-10px\"\r\n                        #fubauto\r\n                        mode=\"basic\"\r\n                        name=\"file\"\r\n                        url=\"{{ uploadIp }}\"\r\n                        *ngIf=\"downloadBtn\"\r\n                        accept=\"application/*\"\r\n                        maxFileSize=\"1000000\"  auto=\"true\" chooseLabel=\"导入排班表\"\r\n                        (onBeforeUpload)=\"onBeforeUpload($event)\"\r\n                        (onUpload)=\"onUpload($event)\">\r\n                </p-fileUpload>\r\n                <button pButton\r\n                        class=\"pull-right ui-margin-right-10px\"\r\n                        type=\"button\"\r\n                        label=\"导入排班表\"\r\n                        [disabled]=\"hadDownload\"\r\n                        *ngIf=\"hadDownload\"\r\n                        (onUpload)=\"onBasicUpload($event)\" ></button>\r\n                <!--</div>-->\r\n                <button pButton\r\n                        class=\"pull-right ui-margin-right-10px\"\r\n                        type=\"button\"\r\n                        (click)=\"submit()\"\r\n                        label=\"保存\"\r\n                        [disabled]=\"saved\"></button>\r\n            </div>\r\n        </div>\r\n</form>\r\n    <p-growl [(value)]=\"msgPop\"></p-growl>\r\n    <p-dialog header=\"部门选择\" [(visible)]=\"departementDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\r\n        <p-tree [value]=\"filesTree4\"\r\n                selectionMode=\"checkbox\"\r\n                [(selection)]=\"selected\"\r\n                (onNodeExpand)=\"nodeExpand($event)\"\r\n        ></p-tree>\r\n        <!--<div>Selected Nodes: <span *ngFor=\"let file of selected\">{{file.label}} </span></div>-->\r\n        <p-footer>\r\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\r\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"departementDisplay=false\" label=\"取消\"></button>\r\n        </p-footer>\r\n    </p-dialog>\r\n  <!--<form [formGroup]=\"form\">-->\r\n    <!--<div class=\"form-group\">-->\r\n      <!--<label for=\"user\">username</label>-->\r\n      <!--<input id=\"user\" type=\"text\" class=\"form-control\" formControlName = \"username\"-->\r\n             <!--[class.my-dirty] = \"isDirty\">-->\r\n    <!--</div>-->\r\n  <!--</form>-->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/duty/schedule-add/schedule-add.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#dutyAdd {\n  font-size: 14px; }\n  #dutyAdd div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #dutyAdd div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #dutyAdd form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 768px) {\n  .col-sm-11 {\n    width: 89.666667%; }\n  .col-sm-5 {\n    width: 39.666667%; }\n  .col-sm-1 {\n    width: 10.333333%; } }\n\n@media screen and (max-width: 1366px) {\n  button[ngClass=\"ui-g-12  ui-fluid-no-padding\"] {\n    border: solid 1px red; }\n    button[ngClass=\"ui-g-12  ui-fluid-no-padding\"] /deep/ span {\n      padding: .25em .1em; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/schedule-add/schedule-add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScheduleAddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ScheduleAddComponent = (function () {
    function ScheduleAddComponent(dutyService, storageService, publicService, router, fb) {
        this.dutyService = dutyService;
        this.storageService = storageService;
        this.publicService = publicService;
        this.router = router;
        this.fb = fb;
        this.selectedDepartment = ''; // 表单显示的已选中的部门组织
        this.msgs = []; // 验证消息提示
        this.msgPop = []; // 验证消息弹框提示
        this.filesTree4 = []; // 部门组织初始
        this.selectedStr = ''; // 部门选中的OID字符串
    }
    ScheduleAddComponent_1 = ScheduleAddComponent;
    ScheduleAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.uploadIp = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/onduty/upload";
        this.departementDisplay = false;
        this.initStartTime = new Date();
        this.beginTime = new Date();
        this.endTime = new Date();
        this.hadSaved = true;
        this.hadDownload = true;
        this.downloadBtn = false;
        this.progressBar = true;
        this.saved = false;
        this.zh = this.initZh();
        this.approveOid = '';
        this.dutyService.getDepartmentDatas().subscribe(function (res) {
            _this.filesTree4 = res;
        });
        this.defaultCreateTime = ScheduleAddComponent_1.formatDate(new Date());
    };
    ScheduleAddComponent.prototype.initZh = function () {
        return {
            firstDayOfWeek: 1,
            dayNames: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
            dayNamesShort: ['一', '二', '三', '四', '五', '六', '七'],
            dayNamesMin: ['一', '二', '三', '四', '五', '六', '七'],
            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
            monthNamesShort: ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二'],
        };
    };
    // 部门组织树模态框
    ScheduleAddComponent.prototype.showTreeDialog = function () {
        this.selected = [];
        this.departementDisplay = true;
    };
    // 日期格式化
    ScheduleAddComponent.formatDate = function (date) {
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        return y + '-' + m + '-' + d;
    };
    // 组织树懒加载
    ScheduleAddComponent.prototype.nodeExpand = function (event) {
        // console.log(event.node);
        if (event.node) {
            this.dutyService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    ScheduleAddComponent.prototype.clearTreeDialog = function () {
        var _this = this;
        this.selectedDepartment = '';
        var query = '';
        this.publicService.getApprovers(this.selectedStr).subscribe(function (res) {
            _this.approveSearchOptions = _this.approveSearchOptions = _this.filterCountry(query, res);
        });
    };
    ScheduleAddComponent.prototype.closeTreeDialog = function () {
        var _this = this;
        var selectedOid = [];
        var selectedName = [];
        var query = '';
        this.approveSelectedMultipleOption = [];
        // console.log(this.selected);
        for (var _i = 0, _a = this.selected; _i < _a.length; _i++) {
            var val = _a[_i];
            selectedOid.push(val['oid']);
            selectedName.push(val['label']);
        }
        this.selectedStr = selectedOid.join(',');
        this.selectedDepartment = selectedName.join(',');
        this.publicService.getApprovers(this.selectedStr).subscribe(function (res) {
            _this.approveSearchOptions = _this.approveSearchOptions = _this.filterCountry(query, res);
        });
        // console.log(this.selectedStr );
        this.departementDisplay = false;
    };
    ScheduleAddComponent.prototype.createForm = function () {
        this.newSchdule = this.fb.group({
            scheduleId: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            scheduleCreator: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            schduleName: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            scheduleCreateTime: [this.defaultCreateTime, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            scheduleApprovor: [this.approveSelectedMultipleOption],
            schduleDepartment: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            schduleStart: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            schduleEnd: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            schduleShift: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            schduleRelative: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    };
    Object.defineProperty(ScheduleAddComponent.prototype, "isName", {
        get: function () {
            return !this.newSchdule.controls['schduleName'].untouched && (!this.newSchdule.controls['schduleName'].valid);
        },
        enumerable: true,
        configurable: true
    });
    ScheduleAddComponent.prototype.filterApproverMultiple = function (event) {
        var _this = this;
        // console.log(this.approveOid);
        var query = event.query;
        this.publicService.getApprovers(this.selectedStr).subscribe(function (res) {
            _this.approveSearchOptions = _this.filterCountry(query, res);
        });
    };
    ScheduleAddComponent.prototype.filterShiftMultipe = function (event) {
        var _this = this;
        var query = event.query;
        this.dutyService.getAllSchduleShifts().subscribe(function (res) {
            _this.shiftSearchOptions = _this.filterCountry(query, res);
        });
    };
    ScheduleAddComponent.prototype.filterRelativeMutipe = function (event) {
        var _this = this;
        var query = event.query;
        this.dutyService.getRelaticeScheduleData().subscribe(function (res) {
            _this.relativeSearchOptions = _this.filterCountry(query, res);
        });
    };
    ScheduleAddComponent.prototype.showError = function () {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    ScheduleAddComponent.prototype.submit = function () {
        var _this = this;
        var selectedArray = [];
        if (this.selected) {
            for (var i = 0; i < this.selected.length; i++) {
                selectedArray[i] = {
                    oid: this.selected[i]['oid'],
                    name: this.selected[i]['label']
                };
            }
        }
        this.msgs = [];
        if ((!this.newSchdule.get('schduleName').value)
            || !this.defaultCreateTime
            || !this.approveSelectedMultipleOption
            || !this.shiftSelectedMultipleOption
            || !this.selectedDepartment) {
            this.showError();
        }
        else if (this.beginTime.toString() === this.endTime.toString()) {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '错误消息提示', detail: '起始时间和结束时间不能相同' });
        }
        else {
            this.dutyService.addSchedule(this.newSchdule.get('schduleName').value, this.defaultCreateTime, selectedArray, this.approveSelectedMultipleOption, ScheduleAddComponent_1.formatDate(this.beginTime), ScheduleAddComponent_1.formatDate(this.endTime), this.shiftSelectedMultipleOption, this.relativeSelectedMultipleOption, '草稿').subscribe(function (res) {
                if (res['errcode'] === '00000') {
                    _this.defaultId = res['data'].did;
                    _this.defaultCreator = res['data'].creator;
                    _this.msgPop = [];
                    _this.hadSaved = false;
                    _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '计划保存成功' });
                    _this.saved = true;
                }
                else {
                    _this.msgPop = [];
                    _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '计划保存失败' + '\n' + res });
                }
            });
        }
    };
    ScheduleAddComponent.prototype.filterCountry = function (query, countries) {
        var filtered = [];
        for (var i = 0; i < countries.length; i++) {
            var country = countries[i];
            if (country.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(country);
            }
        }
        return filtered;
    };
    ScheduleAddComponent.prototype.downloadModel = function () {
        var _this = this;
        this.progressBar = false;
        this.dutyService.downloadModel(this.defaultId).subscribe(function (res) {
            if (!res) {
                _this.progressBar = true;
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '模板生成失败' });
            }
            else {
                _this.progressBar = true;
                _this.msgPop = [];
                _this.hadDownload = false;
                _this.downloadBtn = true;
                window.open(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].url.management + "/" + res, '_blank');
                _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '模板生成成功！请在本地文件夹中查看' });
            }
        });
    };
    ScheduleAddComponent.prototype.onBeforeUpload = function (event) {
        var token = this.storageService.getToken('token');
        event.formData.append('access_token', token);
        event.formData.append('id', this.defaultId);
    };
    ScheduleAddComponent.prototype.onUpload = function (event) {
        var _this = this;
        var res = JSON.parse(event.xhr.response);
        if (res['errcode'] === '00000') {
            this.msgPop = [];
            this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '计划导入成功' });
            window.setTimeout(function () {
                _this.router.navigateByUrl('index/duty/manage');
            }, 1100);
        }
        else {
            this.msgPop = [];
            this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '计划导入失败' + res['errmsg'] });
        }
    };
    ScheduleAddComponent.prototype.onBasicUpload = function (event) {
        // console.log(event);
    };
    ScheduleAddComponent.prototype.startTimeSelected = function (t) {
        this.endTime = t;
        this.minEndTime = t;
        // console.log( this.endTime);
    };
    ScheduleAddComponent = ScheduleAddComponent_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-schedule-add',
            template: __webpack_require__("../../../../../src/app/duty/schedule-add/schedule-add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/schedule-add/schedule-add.component.scss")],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_4__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_5__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_6__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
    ], ScheduleAddComponent);
    return ScheduleAddComponent;
    var ScheduleAddComponent_1;
}());



/***/ }),

/***/ "../../../../../src/app/duty/schedule-approval/schedule-approval.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>审批计划  </span>\n    </div>\n</div>\n<p-progressBar mode=\"indeterminate\" [hidden]=\"progressBar\"></p-progressBar>\n<div class=\"content-section implementation GridDemo \" id=\"approvalSchedule\">\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>基本信息</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    [routerLink]=\"['/index/duty/manage']\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <p-messages [(value)]=\"msgs\"></p-messages>\n    <form [formGroup]=\"myForm\" class=\"form-horizontal\">\n        <div class=\"form-group \">\n            <label class=\"col-sm-1 control-label\" >计划编号：</label>\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                <input class=\"form-control cursor_not_allowed\" type=\"text\"\n                   pInputText\n                   placeholder=\"{{ defaultId }}\" readonly />\n            </div>\n            <label class=\"col-sm-1 control-label\">排班人员：</label>\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                <input  class=\"form-control cursor_not_allowed\"\n                        type=\"text\"\n                        pInputText\n                        placeholder=\"{{ defaultCreator }}\" readonly  />\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-1 control-label\">计划名称：</label>\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                <input  type=\"text\" pInputText\n                        placeholder=\"{{ defaultName }}\" readonly\n                        class=\" form-control cursor_not_allowed\"/>\n            </div>\n            <label class=\"col-sm-1 control-label\">排班时间：</label>\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                <input class=\"cursor_not_allowed\" type=\"text\" pInputText\n                       placeholder=\"{{ defaultCreateTime }}\" readonly\n                       class=\" form-control cursor_not_allowed\"/>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-1 control-label\">值班部门：</label>\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                <input class=\" form-control cursor_not_allowed\" type=\"text\"\n                       pInputText\n                       placeholder=\"{{ selectedDepartment }}\" readonly/>\n            </div>\n            <label class=\"col-sm-1 control-label\">审批人：</label>\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                <input  class=\" form-control cursor_not_allowed\" type=\"text\"\n                        pInputText\n                        placeholder=\"{{ approveSelectedMultipleOption }}\" readonly />\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-1 control-label\">开始时间：</label>\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                <input  class=\" form-control cursor_not_allowed\" type=\"text\" pInputText   placeholder=\"{{ beginTime }}\" readonly/>\n            </div>\n            <label class=\"col-sm-1 control-label\">结束时间：</label>\n            <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                <input class=\" form-control cursor_not_allowed\" type=\"text\"  pInputText placeholder=\"{{ endTime }}\" readonly/>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-1 control-label\" >值班班次：</label>\n            <div class=\"col-sm-11 ui-no-padding-left-15px\">\n                <input ngClass=\"ui-fluid\"\n                       class=\" form-control cursor_not_allowed\" type=\"text\"  pInputText\n                       placeholder=\"{{ shiftSelectedMultipleOption }}\" readonly/>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label  class=\"col-sm-1 control-label\">关联值班计划：</label>\n            <div class=\"col-sm-11 ui-no-padding-left-15px\">\n                <input  ngClass=\"ui-fluid\"\n                        class=\" form-control cursor_not_allowed\" type=\"text\" pInputText id=\"name\"\n                         placeholder=\"{{ relativeSelectedMultipleOption }}\" readonly/>\n            </div>\n        </div>\n        <div  class=\"form-group \">\n            <label class=\"col-sm-1 control-label\">\n                <span ngClass=\"start_red\">*</span>审批意见：</label>\n            <div class=\"col-sm-11 ui-no-padding-left-15px\">\n                <input  ngClass=\"ui-fluid\"\n                        class=\" form-control\"\n                        formControlName=\"apporvalSuggestion\"\n                        type=\"text\" pInputText name=\"suggestion\" placeholder=\"请填写意见\"\n                       [(ngModel)]=\"apporvalSuggestion\"/>\n                <app-field-error-display\n                    [displayError]=\"isFieldValid('apporvalSuggestion')\"\n                    errorMsg=\"不能为空\">\n                </app-field-error-display>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <div class=\"col-sm-12\">\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"oporate('拒绝')\" label=\"拒绝\" ></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"oporate('驳回')\" label=\"驳回\"></button>\n                <button class=\"pull-right ui-margin-right-10px\" pButton type=\"button\" (click)=\"oporate('通过')\" label=\"通过\" ></button>\n            </div>\n        </div>\n    </form>\n    <p-dialog header=\"确认消息\" [(visible)]=\"dialogdisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        确认{{whichOprate}}吗？\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"pass()\" label=\"{{whichOprate}}\" *ngIf=\"whichOprate=='通过'\"></button>\n            <button type=\"button\" pButton icon=\"fa-reply\" (click)=\"return()\" label=\"{{whichOprate}}\" *ngIf=\"whichOprate=='驳回'\"></button>\n            <button type=\"button\" pButton icon=\"fa-hand-paper-o\" (click)=\"return()\" label=\"{{whichOprate}}\" *ngIf=\"whichOprate=='拒绝'\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"dialogdisplay=false\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"msgPop\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/duty/schedule-approval/schedule-approval.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#approvalSchedule {\n  font-size: 14px; }\n  #approvalSchedule div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #approvalSchedule div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #approvalSchedule form {\n    background: white;\n    padding: 20px 26px 20px 0px; }\n\n@media (min-width: 1366px) {\n  .col-sm-11 {\n    width: 88.666667%; }\n  .col-sm-5 {\n    width: 38.666667%; }\n  .col-sm-1 {\n    width: 11.333333%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/schedule-approval/schedule-approval.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScheduleApprovalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ScheduleApprovalComponent = (function () {
    function ScheduleApprovalComponent(dutyService, activitedRoute, router) {
        this.dutyService = dutyService;
        this.activitedRoute = activitedRoute;
        this.router = router;
        this.formBuilder = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]();
        this.msgs = []; // 表单验证提示
        this.msgPop = []; // 提示弹出框
        this.selectedDepartment = ''; // 表单显示的已选中的部门组织
        this.dispalyError = false;
    }
    ScheduleApprovalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initForm();
        this.dialogdisplay = false;
        this.currentSheduleId = this.activitedRoute.snapshot.paramMap.get('id');
        this.dutyService.getScheduleById(this.currentSheduleId).subscribe(function (res) {
            _this.defaultId = res.did;
            _this.defaultCreator = res.creator;
            _this.defaultName = res.name;
            _this.newSchdule.get('schduleName').setValue(res.name);
            _this.defaultCreateTime = res.create_time;
            var departArray = [];
            res['orgs_structures'].forEach(function (data) {
                departArray.push(data['name']);
            });
            _this.selectedDepartment = String(departArray);
            _this.beginTime = res['begin_time'];
            _this.endTime = res.end_time;
            _this.approveSelectedMultipleOption = res['approver'];
            _this.shiftSelectedMultipleOption = res['shifts_name'];
            _this.relativeSelectedMultipleOption = res['related_duty'];
            _this.apporvalSuggestion = res.approve_remarks;
        });
        this.progressBar = true;
        this.beginTime = new Date();
        this.endTime = new Date();
        this.createForm();
    };
    ScheduleApprovalComponent.prototype.initForm = function () {
        this.myForm = this.formBuilder.group({
            apporvalSuggestion: [null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]
        });
    };
    ScheduleApprovalComponent.prototype.isFieldValid = function (field) {
        return !this.myForm.get(field).valid && this.myForm.get(field).touched;
    };
    ScheduleApprovalComponent.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormGroup"]) {
                _this.validateAllFormFields(control);
            }
        });
    };
    ScheduleApprovalComponent.prototype.createForm = function () {
        this.newSchdule = this.formBuilder.group({
            schduleName: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleApprovor: [this.approveSelectedMultipleOption, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleId: [this.defaultId, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleCreator: [this.defaultCreator, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleCreateTime: [this.defaultCreateTime, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]
        });
    };
    ScheduleApprovalComponent.prototype.oporate = function (oporate) {
        this.whichOprate = oporate;
        this.dialogdisplay = true;
    };
    ScheduleApprovalComponent.prototype.pass = function () {
        // this.validateAllFormFields(this.myForm);
        this.approvalSchedule('待执行');
    };
    ScheduleApprovalComponent.prototype.return = function () {
        if (this.apporvalSuggestion) {
            this.approvalSchedule(this.whichOprate);
            this.dialogdisplay = false;
        }
        else {
            this.validateAllFormFields(this.myForm);
            this.dialogdisplay = false;
        }
    };
    ScheduleApprovalComponent.prototype.showError = function () {
        this.msgPop = [];
        this.msgPop.push({ severity: 'error', summary: '错误消息提示', detail: this.whichOprate + '失败' });
    };
    ScheduleApprovalComponent.prototype.showSucc = function () {
        var _this = this;
        this.msgPop = [];
        this.msgPop.push({ severity: 'success', summary: '成功消息提示', detail: this.whichOprate + '成功' });
        window.setTimeout(function () {
            _this.router.navigateByUrl('index/duty/manage');
        }, 3000);
    };
    ScheduleApprovalComponent.prototype.approvalSchedule = function (status) {
        var _this = this;
        this.dutyService.approveSchedule(this.defaultId, this.apporvalSuggestion, status).subscribe(function (res) {
            if (res) {
                _this.showSucc();
            }
            else {
                _this.showError();
            }
        });
    };
    ScheduleApprovalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-schedule-approval',
            template: __webpack_require__("../../../../../src/app/duty/schedule-approval/schedule-approval.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/schedule-approval/schedule-approval.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"]])
    ], ScheduleApprovalComponent);
    return ScheduleApprovalComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/schedule-edit/schedule-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \">\n    <div>\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>编辑计划</span>\n    </div>\n</div>\n<p-progressBar mode=\"indeterminate\" [hidden]=\"progressBar\"></p-progressBar>\n<div class=\"content-section implementation GridDemo\" id=\"dutyedit\">\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>基本信息</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    [routerLink]=\"['/index/duty/manage']\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n        <p-messages [(value)]=\"msgs\"></p-messages>\n        <form [formGroup]=\"newSchdule\"  (ngSubmit)=\"submit\" class=\"form-horizontal\">\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">计划编号：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input class=\"form-control cursor_not_allowed\" type=\"text\" formControlName=\"scheduleId\"\n                           pInputText id=\"did\"[(ngModel)]=\"defaultId\" readonly />\n                </div>\n                <label class=\"col-sm-1 control-label\">排班人员：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input  class=\"form-control cursor_not_allowed\" formControlName=\"scheduleCreator\"\n                            type=\"text\" pInputText id=\"creator\"\n                            readonly [(ngModel)]=\"defaultCreator\" />\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">\n                    <span ngClass=\"start_red\">*</span>\n                    计划名称：\n                </label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input formControlName=\"schduleName\"\n                           class=\"form-control\"\n                           type=\"text\" pInputText id=\"name\"\n                           placeholder=\"请填写计划名称\"\n                           [class.ng-dirty]=\"!newSchdule.controls.schduleName.untouched || !newSchdule.controls.schduleName.valid\"\n                           [class.ng-invalid]=\"!newSchdule.controls.schduleName.untouched || !newSchdule.controls.schduleName.valid\"/>\n                </div>\n                <label class=\"col-sm-1 control-label\">\n                    <span ngClass=\"start_red\">*</span>\n                    排班时间：\n                </label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input class=\"form-control cursor_not_allowed\" type=\"text\" formControlName=\"scheduleCreateTime\"\n                           pInputText id=\"create_time\"  readonly [ngModel]=\"defaultCreateTime\"/>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">\n                    <span ngClass=\"start_red\">*</span>\n                    值班部门：\n                </label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <div class=\"col-sm-8 ui-fluid ui-fluid-no-padding\">\n                        <input type=\"text\"\n                               pInputText id=\"department\"\n                               name=\"department\"\n                               placeholder=\"请选择值班部门\"\n                               [(ngModel)]=\"selectedDepartment\"\n                               class=\"form-control\"\n                               formControlName=\"scheduleDepartment\"/>\n                    </div>\n                    <div class=\"col-sm-2 ui-fluid-no-padding ui-padding-10px\">\n                        <button ngClass=\"ui-g-12  ui-fluid-no-padding\"  pButton type=\"button\" (click)=\"showTreeDialog()\" label=\"选择\" ></button>\n                    </div>\n                    <div class=\"col-sm-2 ui-fluid-no-padding ui-padding-10px\">\n                        <button ngClass=\"ui-g-12  ui-fluid-no-padding\" pButton type=\"button\" (click)=\"clearTreeDialog()\" label=\"清空\"></button>\n                    </div>\n                </div>\n                <label class=\"col-sm-1 control-label\">\n                    <span ngClass=\"start_red\">*</span>\n                    审批人：\n                </label>\n                <div class=\"col-sm-5 ui-fluid ui-no-padding-left-15px\">\n                    <p-autoComplete  [suggestions]=\"approveSearchOptions\"\n                                     [formControl]=\"approvarValue\"\n                                     [(ngModel)]=\"approveSelectedMultipleOption\"\n                                     (completeMethod)=\"filterApproverMultiple($event)\"\n                                     [minLength]=\"1\" [required]=\"true\"\n                                     placeholder=\"请选择审批人\"\n                                     [multiple]=\"true\"\n                                     name=\"test\"\n                                     [dropdown]=\"true\"\n                                     formControlName=\"scheduleApprovor\"\n                                     id=\"approval\" >\n                    </p-autoComplete>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">\n                    <span ngClass=\"start_red\">*</span>\n                    开始时间：\n                </label>\n                <div class=\"col-sm-5 ui-fluid ui-no-padding-left-15px\">\n                    <p-calendar [(ngModel)]=\"beginTime\" [showIcon]=\"true\" [locale]=\"zh\" name=\"beginTime\"\n                                [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\" [required]=\"true\"\n                                (onSelect) = \"startTimeSelected(beginTime)\"\n                                [minDate]=\"beginTimeCalendar\"\n                                id=\"startCalendar\"\n                                formControlName=\"schduleStart\">\n                    </p-calendar>\n                </div>\n                <label class=\"col-sm-1 control-label\">\n                    <span ngClass=\"start_red\">*</span>\n                    结束时间：\n                </label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <p-calendar [(ngModel)]=\"endTime\" [showIcon]=\"true\" [locale]=\"zh\" name=\"end_time\"\n                                [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\" [required]=\"true\"\n                                [minDate]=\"endTime\"\n                                id=\"endCalendar\"\n                                ngClass=\"ui-fluid\"\n                                formControlName=\"schduleEnd\">\n                    </p-calendar>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">\n                    <span ngClass=\"start_red\">*</span>\n                    排班班次：\n                </label>\n                <div class=\"col-sm-11 ui-no-padding-left-15px\">\n                    <p-autoComplete  [suggestions]=\"shiftSearchOptions\"\n                                     [formControl]=\"shiftValue\"\n                                     [(ngModel)]=\"shiftSelectedMultipleOption\"\n                                     (completeMethod)=\"filterShiftMultipe($event)\"\n                                     [minLength]=\"1\" [required]=\"true\"\n                                     [multiple]=\"true\" name=\"test\"\n                                     [dropdown]=\"true\"\n                                     placeholder=\"请选择排班班次\"\n                                     ngClass=\"ui-fluid\"\n                                     formControlName=\"schduleShift\"\n                    >\n                    </p-autoComplete>\n                </div>\n\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">关联排班：</label>\n                <div class=\"col-sm-11 ui-no-padding-left-15px\">\n                    <p-autoComplete  [suggestions]=\"relativeSearchOptions\"\n                                     [formControl]=\"relativeValue\" [(ngModel)]=\"relativeSelectedMultipleOption\"\n                                     (completeMethod)=\"filterRelativeMutipe($event)\" [minLength]=\"1\"\n                                     placeholder=\"请选择关联排班\"\n                                     [multiple]=\"true\" name=\"test\"  [dropdown]=\"true\"\n                                     id=\"relative\"\n                                     ngClass=\"ui-fluid\"\n                                     formControlName=\"schduleRelative\">\n                    </p-autoComplete>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <div class=\"col-sm-12 ui-no-padding-left-15px\">\n                    <button  class=\"pull-right\" pButton type=\"button\" (click)=\"downloadModel()\" label=\"生成排班表\" [disabled]=\"hadSaved\"></button>\n                    <p-fileUpload  class=\"pull-right ui-margin-right-10px\"\n                        #fubauto\n                        mode=\"basic\"\n                        name=\"file\"\n                        url=\"{{ uploadIp }}\"\n                        *ngIf=\"downloadBtn\"\n                        accept=\"application/*\"\n                        maxFileSize=\"1000000\"  auto=\"true\" chooseLabel=\"导入排班表\"\n                        (onBeforeUpload)=\"onBeforeUpload($event)\"\n                        (onUpload)=\"onUpload($event)\">\n                    </p-fileUpload>\n                    <button pButton class=\"pull-right ui-margin-right-10px\"  type=\"button\" (click)=\"showInputBtn()\" label=\"导入排班表\" [disabled]=\"hadDownload\" *ngIf=\"hadDownload\" ></button>\n                    <button pButton  class=\"pull-right ui-margin-right-10px\"  type=\"button\" (click)=\"submit()\" label=\"保存\" [disabled]=\"saved\"></button>\n                </div>\n            </div>\n        </form>\n    <p-growl [(value)]=\"msgPop\"></p-growl>\n    <p-dialog header=\"部门选择\" [(visible)]=\"departementDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\n        <p-tree [value]=\"filesTree4\"\n                selectionMode=\"checkbox\"\n                [(selection)]=\"selected\"\n                (onNodeExpand)=\"nodeExpand($event)\"\n                (onUpload)=\"onUpload($event)\"\n        ></p-tree>\n        <div>Selected Nodes: <span *ngFor=\"let file of selected\">{{file.label}} </span></div>\n        <p-footer>\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"closeTreeDialog()\" label=\"确定\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"departementDisplay=false\" label=\"取消\"></button>\n        </p-footer>\n    </p-dialog>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/duty/schedule-edit/schedule-edit.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#dutyedit {\n  font-size: 14px; }\n  #dutyedit div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #dutyedit div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #dutyedit form {\n    background: white;\n    padding: 20px 26px; }\n\n@media (min-width: 768px) {\n  .col-sm-11 {\n    width: 89.666667%; }\n  .col-sm-5 {\n    width: 39.666667%; }\n  .col-sm-1 {\n    width: 10.333333%; } }\n\n@media screen and (max-width: 1366px) {\n  button[ngClass=\"ui-g-12  ui-fluid-no-padding\"] {\n    border: solid 1px red; }\n    button[ngClass=\"ui-g-12  ui-fluid-no-padding\"] /deep/ span {\n      padding: .25em .1em; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/schedule-edit/schedule-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScheduleEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ScheduleEditComponent = (function () {
    function ScheduleEditComponent(dutyService, activitedRoute, storageService, publicService, router) {
        this.dutyService = dutyService;
        this.activitedRoute = activitedRoute;
        this.storageService = storageService;
        this.publicService = publicService;
        this.router = router;
        this.shiftSelectedMultipleOption = []; // 排班班次已被选取的数据
        this.relativeSelectedMultipleOption = []; // 关联计划已被选择的数据
        this.approveSelectedMultipleOption = []; // 审批人已被选择的数据
        this.selectedDepartment = ''; // 表单显示的已选中的部门组织
        this.selectedStr = ''; // 部门选中的OID字符串
        this.msgs = []; // 表单验证提示
        this.msgPop = []; // 验证消息弹框提示
        this.formBuilder = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]();
        this.shiftValue = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]([]);
        this.approvarValue = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]([]);
        this.relativeValue = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]([]);
    }
    ScheduleEditComponent.prototype.ngOnInit = function () {
        this.currentSheduleId = this.activitedRoute.snapshot.paramMap.get('id');
        this.uploadIp = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].url.management + "/onduty/upload";
        this.createForm();
        this.initDatas();
        this.zh = this.initZh();
        this.initDepartmentDatas();
        this.departementDisplay = false;
        this.hadSaved = true;
        this.hadDownload = true;
        this.downloadBtn = false;
        this.progressBar = true;
        this.saved = false;
        this.selected = [];
    };
    ScheduleEditComponent.prototype.initDatas = function () {
        var _this = this;
        this.beginTimeCalendar = new Date();
        this.dutyService.getScheduleById(this.currentSheduleId).subscribe(function (res) {
            _this.defaultId = res.did;
            _this.defaultCreator = res.creator;
            _this.newSchdule.get('schduleName').setValue(res.name);
            _this.defaultCreateTime = res.create_time;
            _this.selectedDepartment = res.duty_org;
            _this.defaultStatus = res.status;
            _this.beginTime = res.begin_time;
            _this.initOrg = res.orgs_structures;
            _this.endTime = new Date(res.end_time);
            if (res['approver_structures']) {
                var initAppArray = [];
                _this.recivedAppDatas = res['approver_structures'];
                for (var _i = 0, _a = res['approver_structures']; _i < _a.length; _i++) {
                    var val = _a[_i];
                    initAppArray.push(val['name']);
                }
                _this.approvarValue.setValue(initAppArray);
            }
            if (res['shift_structures']) {
                var initShiftArray = [];
                _this.recicedShiftDatas = res['shift_structures'];
                for (var _b = 0, _c = res['shift_structures']; _b < _c.length; _b++) {
                    var val = _c[_b];
                    initShiftArray.push(val['name']);
                }
                _this.shiftValue.setValue(initShiftArray);
            }
            if (res['related_duty']) {
                var relatedArray = [];
                _this.recivedRelaDatas = res['related_duty'];
                relatedArray.push(res['related_duty']);
                _this.relativeValue.setValue(relatedArray);
            }
        });
    };
    ScheduleEditComponent.prototype.initZh = function () {
        return {
            firstDayOfWeek: 1,
            dayNames: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
            dayNamesShort: ['一', '二', '三', '四', '五', '六', '七'],
            dayNamesMin: ['一', '二', '三', '四', '五', '六', '七'],
            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
            monthNamesShort: ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二'],
        };
    };
    ScheduleEditComponent.prototype.initDepartmentDatas = function () {
        var _this = this;
        this.dutyService.getDepartmentDatas().subscribe(function (res) {
            _this.filesTree4 = res;
        });
    };
    // 部门组织树模态框
    ScheduleEditComponent.prototype.showTreeDialog = function () {
        this.departementDisplay = true;
    };
    // 组织树懒加载
    ScheduleEditComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.dutyService.getDepartmentDatas(event.node.oid, event.node.dep).subscribe(function (res) {
                event.node.children = res;
            });
        }
    };
    ScheduleEditComponent.prototype.clearTreeDialog = function () {
        var _this = this;
        this.selectedDepartment = '';
        var query = '';
        this.publicService.getApprovers(this.selectedStr).subscribe(function (res) {
            _this.approveSearchOptions = _this.approveSearchOptions = _this.filterCountry(query, res);
        });
    };
    ScheduleEditComponent.prototype.closeTreeDialog = function () {
        var _this = this;
        console.log(this.selected);
        this.initOrg = [];
        var selectedOid = [];
        var selectedName = [];
        var query = '';
        this.approveSelectedMultipleOption = [];
        // console.log(this.selected);
        for (var _i = 0, _a = this.selected; _i < _a.length; _i++) {
            var val = _a[_i];
            selectedOid.push(val['oid']);
            selectedName.push(val['label']);
        }
        this.selectedStr = selectedOid.join(',');
        this.selectedDepartment = selectedName.join(',');
        this.publicService.getApprovers(this.selectedStr).subscribe(function (res) {
            _this.approveSearchOptions = _this.approveSearchOptions = _this.filterCountry(query, res);
        });
        this.departementDisplay = false;
    };
    ScheduleEditComponent.prototype.createForm = function () {
        this.newSchdule = this.formBuilder.group({
            schduleName: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleApprovor: [this.approveSelectedMultipleOption, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleId: [this.defaultId, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleCreator: [this.defaultCreator, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleCreateTime: [this.defaultCreateTime, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleDepartment: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            schduleStart: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            schduleEnd: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            schduleShift: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            schduleRelative: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]
        });
    };
    ScheduleEditComponent.prototype.filterApproverMultiple = function (event) {
        var _this = this;
        var query = event.query;
        this.publicService.getApprovers(this.selectedStr).subscribe(function (res) {
            _this.recivedAppDatas = res;
            _this.approveSearchOptions = _this.filterCountry(query, res);
        });
    };
    ScheduleEditComponent.prototype.filterShiftMultipe = function (event) {
        var _this = this;
        var query = event.query;
        this.dutyService.getAllSchduleShifts().subscribe(function (res) {
            _this.recicedShiftDatas = res;
            _this.shiftSearchOptions = _this.filterCountry(query, res);
        });
    };
    ScheduleEditComponent.prototype.filterRelativeMutipe = function (event) {
        var _this = this;
        var query = event.query;
        this.dutyService.getRelaticeScheduleData().subscribe(function (res) {
            _this.recivedRelaDatas = res;
            _this.relativeSearchOptions = _this.filterCountry(query, res);
        });
    };
    ScheduleEditComponent.prototype.showError = function () {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '错误消息提示', detail: '请填写完整表单' });
    };
    ScheduleEditComponent.prototype.submit = function () {
        var _this = this;
        // console.log(this.approveSelectedMultipleOption);
        // console.log(this.recivedAppDatas);
        var approvePidArray = [];
        for (var i = 0; i < this.approveSelectedMultipleOption.length; i++) {
            for (var j = 0; j < this.recivedAppDatas.length; j++) {
                if (this.approveSelectedMultipleOption[i] === this.recivedAppDatas[j]['name']) {
                    approvePidArray.push(this.recivedAppDatas[j]);
                }
            }
        }
        // console.log(approvePidArray);
        // console.log(this.shiftSelectedMultipleOption);
        // console.log(this.recicedShiftDatas);
        var shiftsSidArray = [];
        for (var i = 0; i < this.shiftSelectedMultipleOption.length; i++) {
            for (var j = 0; j < this.recicedShiftDatas.length; j++) {
                if (this.shiftSelectedMultipleOption[i] === this.recicedShiftDatas[j]['name']) {
                    shiftsSidArray.push(this.recicedShiftDatas[j]);
                }
            }
        }
        // console.log(shiftsSidArray);
        // console.log(this.relativeSelectedMultipleOption);
        // console.log(this.recivedRelaDatas);
        var relativeDidStr = '';
        for (var i = 0; i < this.relativeSelectedMultipleOption.length; i++) {
            for (var j = 0; j < this.recivedRelaDatas.length; j++) {
                if (this.relativeSelectedMultipleOption[i] === this.recivedRelaDatas[j]['name']) {
                    relativeDidStr = this.recivedRelaDatas[j]['did'].toString();
                }
            }
        }
        // console.log(relativeDidStr);
        console.log(this.approveSelectedMultipleOption);
        var selectedArray = [];
        if (this.initOrg.length) {
            // console.log(this.initOrg);
            // console.log(23333);
            selectedArray = this.initOrg;
        }
        else {
            // console.log(this.selected);
            // console.log(55555);
            for (var i = 0; i < this.selected.length; i++) {
                selectedArray[i] = {
                    oid: this.selected[i]['oid'],
                    name: this.selected[i]['label']
                };
            }
        }
        this.msgs = [];
        if ((!this.defaultId)
            || (!this.defaultCreator)
            || (!this.newSchdule.get('schduleName').value)
            || !this.defaultCreateTime
            || !(String(this.approveSelectedMultipleOption))
            || !this.shiftSelectedMultipleOption
            || !this.selectedDepartment) {
            this.showError();
        }
        else if (this.beginTime.toString() === this.endTime.toString()) {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '错误消息提示', detail: '起始时间和结束时间不能相同' });
        }
        else {
            this.dutyService.editSchedule(this.defaultId, this.defaultCreator, this.newSchdule.get('schduleName').value, this.defaultCreateTime, selectedArray, approvePidArray, this.formatDate(this.beginTime), this.formatDate(this.endTime), shiftsSidArray, relativeDidStr, this.defaultStatus).subscribe(function (res) {
                if (res === '00000') {
                    _this.msgPop = [];
                    _this.hadSaved = false;
                    _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '计划保存成功' });
                    _this.saved = true;
                }
                else {
                    _this.msgPop = [];
                    _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '计划保存失败' + '\n' + res });
                }
            });
        }
    };
    ScheduleEditComponent.prototype.formatDate = function (date) {
        if (/[0-9]{4}-[0-9]{2}-[0-9]{2}/.exec(date)) {
            return date;
        }
        else {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            m = m < 10 ? '0' + m : m;
            var d = date.getDate();
            d = d < 10 ? ('0' + d) : d;
            return y + '-' + m + '-' + d;
        }
    };
    ScheduleEditComponent.prototype.filterCountry = function (query, countries) {
        var filtered = [];
        for (var i = 0; i < countries.length; i++) {
            var country = countries[i];
            if (country.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(country.name);
            }
        }
        return filtered;
    };
    ScheduleEditComponent.prototype.downloadModel = function () {
        var _this = this;
        this.progressBar = false;
        this.dutyService.downloadModel(this.defaultId).subscribe(function (res) {
            if (!res) {
                _this.progressBar = true;
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'error', summary: ' 提示消息', detail: '模板生成失败' });
            }
            else {
                _this.progressBar = true;
                _this.msgPop = [];
                _this.hadDownload = false;
                _this.downloadBtn = true;
                window.open(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].url.management + "/" + res, '_blank');
                _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '模板生成成功！请在本地文件夹中查看' });
            }
        });
    };
    ScheduleEditComponent.prototype.onBeforeUpload = function (event) {
        var token = this.storageService.getToken('token');
        event.formData.append('access_token', token);
        event.formData.append('id', this.defaultId);
    };
    ScheduleEditComponent.prototype.onUpload = function (event) {
        var _this = this;
        var res = JSON.parse(event.xhr.response);
        if (res['errcode'] === '00000') {
            this.msgPop = [];
            this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '计划导入成功' });
            window.setTimeout(function () {
                _this.router.navigateByUrl('index/duty/manage');
            }, 1100);
        }
        else {
            this.msgPop = [];
            this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '计划导入失败' + res['errmsg'] });
        }
    };
    ScheduleEditComponent.prototype.showInputBtn = function () {
        this.hadDownload = false;
    };
    ScheduleEditComponent.prototype.startTimeSelected = function (t) {
        this.endTime = t;
        // console.log( this.endTime);
    };
    ScheduleEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-schedule-edit',
            template: __webpack_require__("../../../../../src/app/duty/schedule-edit/schedule-edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/schedule-edit/schedule-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_4__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_6__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"]])
    ], ScheduleEditComponent);
    return ScheduleEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/schedule-manage/schedule-manage.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\r\n    <div>\r\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>排班总览  </span>\r\n    </div>\r\n</div>\r\n<div class=\"content-section implementation GridDemo\" id=\"manage\">\r\n    <!--<div class=\"ui-widget-header\">-->\r\n        <div class=\"ui-g\">\r\n            <div class=\"\">\r\n                <label for=\"scheduleName\">\r\n               计划名称:\r\n                </label>\r\n                <input id=\"scheduleName\" type=\"text\" pInputText  placeholder=\"请输入计划名\" [(ngModel)]=\"scheduleName\"/>\r\n            </div>\r\n            <div>\r\n                <label for=\"selectedStatus\">状态选择:</label>\r\n                <p-dropdown id=\"selectedStatus\" [options]=\"statusOptions\" [(ngModel)]=\"selectedStatus\" placeholder=\"请选择状态\" optionLabel=\"name\" [style]=\"{'width':'15vw'}\"></p-dropdown>\r\n            </div>\r\n            <div>\r\n                <button pButton type=\"button\"  label=\"查询\" (click)=\"searchSchedule()\"  ></button>\r\n                <button pButton type=\"button\"  label=\"新增计划\"  [routerLink]=\"['/index/duty/add']\"></button>\r\n            </div>\r\n        </div>\r\n    <!--</div>-->\r\n\r\n    <p-dataTable [value]=\"scheduleDatas\" [responsive]=\"true\" id=\"manageTable\">\r\n        <p-column selectionMode=\"multiple\" ></p-column>\r\n        <p-column field=\"did\" header=\"计划编号\" [sortable]=\"true\">\r\n            <ng-template let-data=\"rowData\" pTemplate=\"operator\">\r\n                <span [routerLink]=\"['/index/duty/view', data['did']]\" (click)=\"overviewSchdule(data)\" class=\"ui-cursor-point\" >{{data.did}}</span>\r\n            </ng-template>\r\n        </p-column>\r\n        <p-column field=\"name\" header=\"计划名称\" [sortable]=\"true\"></p-column>\r\n        <p-column field=\"create_time\" header=\"创建时间\" [sortable]=\"true\"></p-column>\r\n        <p-column field=\"creator\" header=\"排班人员\" [sortable]=\"true\"></p-column>\r\n        <p-column field=\"begin_time\" header=\"开始时间\" [sortable]=\"true\"></p-column>\r\n        <p-column field=\"end_time\" header=\"结束时间\" [sortable]=\"true\"></p-column>\r\n        <p-column field=\"status\" header=\"状态\" [sortable]=\"true\"></p-column>\r\n        <p-column field=\"approve_time\" header=\"审批时间\" [sortable]=\"true\"></p-column>\r\n        <p-column field=\"approver_structures\"   header=\"审批人\" [sortable]=\"true\">\r\n            <ng-template let-data=\"rowData\" pTemplate=\"test\">\r\n                <span>{{data.approver_structures[0].name}}</span>\r\n            </ng-template>\r\n        </p-column>\r\n        <p-column field=\"color\" header=\"操作\" [style]=\"{'width':'12vw'}\">\r\n            <ng-template let-schedule=\"rowData\" pTemplate=\"operator\">\r\n                <button pButton type=\"button\"  (click)=\"editSchdule(schedule)\"\r\n                        *ngIf=\"(schedule.status == '新建') || (schedule.status == '待审核') || (schedule.status == '驳回') || (schedule.status == '待执行') || (schedule.status == '已过期') || (schedule.status == '处理中')\"\r\n                        label=\"编辑\"  [routerLink]=\"['/index/duty/edit', schedule['did']]\" class=\" mb_d1vw\"></button>\r\n                <button pButton type=\"button\"  (click)=\"deleteSchdule(schedule)\"\r\n                        *ngIf=\"(schedule.status == '新建') || (schedule.status == '待审核') || (schedule.status == '驳回') || (schedule.status == '待执行') || (schedule.status == '已过期') || (schedule.status == '处理中')\"\r\n                        label=\"删除\" ></button>\r\n                <button pButton type=\"button\"  (click)=\"overviewSchdule(schedule)\" label=\"查看\" [routerLink]=\"['/index/duty/view', schedule['did']]\"  class=\" mb_d1vw\"></button>\r\n                <button pButton type=\"button\"  (click)=\"approvalSchdule(schedule)\"\r\n                        *ngIf=\"schedule.status == '待审核'\"\r\n                        label=\"审批\" [routerLink]=\"['/index/duty/approval', schedule['did']]\"  class=\" mb_d1vw\"></button>\r\n            </ng-template>\r\n        </p-column>\r\n        <ng-template pTemplate=\"emptymessage\">\r\n            当前没有数据\r\n        </ng-template>\r\n    </p-dataTable>\r\n    <p-paginator rows=\"10\" totalRecords=\"{{total}}\" [rowsPerPageOptions]=\"[10,20,30]\" pageLinkSize=\"{{page_total}}\"  (onPageChange)=\"paginate($event)\"></p-paginator>\r\n    <p-growl [(value)]=\"msgs\"></p-growl>\r\n    <p-growl [(value)]=\"deletemsgs\"></p-growl>\r\n    <p-dialog header=\"删除确认框\" [(visible)]=\"dialogDisplay\" modal=\"modal\" width=\"300\" [responsive]=\"true\">\r\n        确认删除吗？\r\n        <p-footer>\r\n          <button type=\"button\" pButton icon=\"fa-check\" (click)=\"sureDelete()\" label=\"确定\"></button>\r\n          <button type=\"button\" pButton icon=\"fa-close\" (click)=\"dialogDisplay=false\" label=\"取消\" class=\"ui-button-secondary\"></button>\r\n        </p-footer>\r\n    </p-dialog>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/duty/schedule-manage/schedule-manage.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#manage {\n  font-size: 14px !important; }\n  #manage .mb_d1vw {\n    margin-bottom: .1vw; }\n  #manage div[class=\"ui-g\"] {\n    color: #666666;\n    border: 1px solid #e2e2e2;\n    background: white;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center; }\n    #manage div[class=\"ui-g\"] div:nth-child(1) {\n      padding-right: 60px; }\n      #manage div[class=\"ui-g\"] div:nth-child(1) label {\n        padding-right: 20px; }\n      #manage div[class=\"ui-g\"] div:nth-child(1) input {\n        width: 15vw; }\n    #manage div[class=\"ui-g\"] div:nth-child(2) {\n      padding-right: 60px; }\n      #manage div[class=\"ui-g\"] div:nth-child(2) label {\n        padding-right: 20px; }\n    #manage div[class=\"ui-g\"] div:nth-child(3) {\n      padding-right: 20px;\n      -webkit-box-flex: 2;\n          -ms-flex: 2;\n              flex: 2;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: row-reverse;\n              flex-direction: row-reverse; }\n      #manage div[class=\"ui-g\"] div:nth-child(3) button:nth-child(2) {\n        margin-right: 10px; }\n\n#manageTable /deep/ table thead tr th:nth-child(1) {\n  width: 2.6%; }\n\n#manageTable /deep/ table thead tr th:nth-child(2) {\n  width: 11%; }\n\n#manageTable /deep/ table thead tr th:nth-child(11) {\n  width: 16% !important; }\n\n#manageTable /deep/ table tbody tr td {\n  word-break: break-all;\n  word-wrap: break-word;\n  padding: 0.25em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/schedule-manage/schedule-manage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScheduleManageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ScheduleManageComponent = (function () {
    function ScheduleManageComponent(dutyService) {
        this.dutyService = dutyService;
        this.scheduleDatas = [];
        this.msgs = [];
        this.deletemsgs = [];
        this.did = [];
    }
    ScheduleManageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dutyService.getAllStatus().subscribe(function (res) {
            _this.statusOptions = res;
        });
        this.dutyService.getAllSchduleDatas().subscribe(function (res) {
            _this.scheduleDatas = res.items;
            _this.total = res.page.total;
            _this.page_size = res.page.page_size;
            _this.page_total = res.page.page_total;
        });
        this.dialogDisplay = false;
    };
    ScheduleManageComponent.prototype.editSchdule = function (schedule) {
        if (true) {
            this.showWarn('编辑');
        }
    };
    ScheduleManageComponent.prototype.deleteSchdule = function (schedule) {
        this.did = [];
        this.dialogDisplay = true;
        this.did.push(schedule.did);
    };
    ScheduleManageComponent.prototype.sureDelete = function (schedule) {
        var _this = this;
        // if(true) {
        //     this.showWarn('删除');
        // }
        this.dutyService.deleteSchedule(this.did).subscribe(function (res) {
            if (res === '00000') {
                _this.showSuccess();
                _this.dutyService.getAllSchduleDatas().subscribe(function (res) {
                    _this.scheduleDatas = res.items;
                    _this.total = res.page.total;
                    _this.page_size = res.page.page_size;
                    _this.page_total = res.page.page_total;
                });
            }
            else {
                _this.showError(res);
            }
        });
        this.dialogDisplay = false;
    };
    ScheduleManageComponent.prototype.overviewSchdule = function (schedule) {
        this.showWarn('查看');
    };
    ScheduleManageComponent.prototype.approvalSchdule = function (schedule) {
        this.showWarn('审批');
    };
    ScheduleManageComponent.prototype.showWarn = function (operator) {
        this.msgs = [];
        this.msgs.push({ severity: 'warn', summary: '注意', detail: '您没有' + operator + '权限！' });
    };
    ScheduleManageComponent.prototype.showSuccess = function () {
        this.deletemsgs = [];
        this.deletemsgs.push({ severity: 'success', summary: '消息提示', detail: '删除成功' });
    };
    ScheduleManageComponent.prototype.showError = function (res) {
        this.deletemsgs = [];
        this.deletemsgs.push({ severity: 'error', summary: '消息提示', detail: '删除失败' + res });
    };
    ScheduleManageComponent.prototype.searchSchedule = function () {
        var _this = this;
        (!this.selectedStatus) && (this.selectedStatus = {});
        this.dutyService.getAllSchduleDatas(this.scheduleName, this.selectedStatus['name']).subscribe(function (res) {
            _this.scheduleDatas = res.items;
            _this.total = res.page.total;
            _this.page_size = res.page.page_size;
            _this.page_total = res.page.page_total;
        });
    };
    ScheduleManageComponent.prototype.paginate = function (event) {
        var _this = this;
        this.dutyService.getAllSchduleDatas('', '', (event.page + 1).toString(), event.rows.toString()).subscribe(function (res) {
            _this.scheduleDatas = res.items;
            _this.total = res.page.total;
            _this.page_size = res.page.page_size;
            _this.page_total = res.page.page_total;
        });
    };
    ScheduleManageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-schedule-manage',
            template: __webpack_require__("../../../../../src/app/duty/schedule-manage/schedule-manage.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/schedule-manage/schedule-manage.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__duty_service__["a" /* DutyService */]])
    ], ScheduleManageComponent);
    return ScheduleManageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/schedule-view/schedule-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>查看计划  </span>\n    </div>\n</div>\n<p-progressBar mode=\"indeterminate\" [hidden]=\"progressBar\"></p-progressBar>\n<div class=\"content-section implementation GridDemo \" id=\"viewSchedule\">\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>基本信息</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    [routerLink]=\"['/index/duty/manage']\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <p-growl [(value)]=\"msgPop\"></p-growl>\n        <form [formGroup]=\"newSchdule\" class=\"form-horizontal\">\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\" >计划编号：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input class=\"form-control cursor_not_allowed\" type=\"text\" formControlName=\"scheduleId\"  pInputText placeholder=\"{{ defaultId }}\" readonly />\n                </div>\n                <label class=\"col-sm-1 control-label\">排班人员：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input  class=\" form-control cursor_not_allowed\" formControlName=\"scheduleCreator\" type=\"text\"\n                            pInputText placeholder=\"{{ defaultCreator }}\" readonly  />\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">计划名称：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input class=\" form-control cursor_not_allowed\"\n                           formControlName=\"schduleName\" type=\"text\" pInputText\n                            placeholder=\"{{ defaultName }}\" readonly\n                           [class.my-dirty]=\"isName\"/>\n                </div>\n                <label class=\"col-sm-1 control-label\">排班时间：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input class=\" form-control cursor_not_allowed\"\n                           type=\"text\" formControlName=\"scheduleCreateTime\" pInputText\n                           placeholder=\"{{ defaultCreateTime }}\" readonly/>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">值班部门：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input class=\" form-control cursor_not_allowed\"type=\"text\"\n                           pInputText\n                           placeholder=\"{{ selectedDepartment }}\" readonly/>\n\n                </div>\n                <label class=\"col-sm-1 control-label\">审批人：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input  class=\" form-control cursor_not_allowed\"\n                            formControlName=\"scheduleCreator\" type=\"text\" pInputText\n                            placeholder=\"{{ approveSelectedMultipleOption }}\" readonly/>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\">开始时间：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input  class=\" form-control cursor_not_allowed\" type=\"text\"\n                            pInputText formControlName=\"start\"\n                           placeholder=\"{{ beginTime }}\" readonly/>\n                </div>\n                <label class=\"col-sm-1 control-label\">结束时间：</label>\n                <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                    <input  class=\" form-control cursor_not_allowed\"\n                            type=\"text\" formControlName=\"scheduleCreateTime\"\n                            pInputText id=\"create_time\"\n                            placeholder=\"{{ endTime }}\" readonly/>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label class=\"col-sm-1 control-label\" >值班班次：</label>\n                <div class=\"col-sm-11 ui-no-padding-left-15px\">\n                    <input ngClass=\"ui-fluid\"\n                           class=\" form-control cursor_not_allowed\" type=\"text\"\n                           formControlName=\"shift\"  pInputText\n                           placeholder=\"{{ shiftSelectedMultipleOption }}\" readonly/>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label  class=\"col-sm-1 control-label\">关联值班计划：</label>\n                <div class=\"col-sm-11 ui-no-padding-left-15px\">\n                    <input ngClass=\"ui-fluid\"\n                           class=\" form-control cursor_not_allowed\"\n                           type=\"text\" pInputText\n                           placeholder=\"{{ relativeSelectedMultipleOption }}\" readonly/>\n                </div>\n            </div>\n          <!--{{defaultstatus}}-->\n            <div class=\"form-group\" *ngIf=\"defaultstatus !== '新建' && defaultstatus !== '待审核' \">\n              <!---->\n                <label class=\"col-sm-1 control-label\">审批意见：</label>\n                <div class=\"col-sm-11 ui-no-padding-left-15px\">\n                    <input   ngClass=\"ui-fluid\"\n                             class=\" form-control cursor_not_allowed\"\n                             type=\"text\" pInputText id=\"name\"\n                             placeholder=\"{{ apporvalSuggestion }}\" readonly/>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <div class=\"col-sm-12 ui-no-padding-left-15px\">\n                    <button class=\"pull-right\" pButton type=\"button\" (click)=\"download()\" label=\"下载排班计划\"></button>\n                </div>\n            </div>\n        </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/duty/schedule-view/schedule-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#viewSchedule {\n  font-size: 14px; }\n  #viewSchedule div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #viewSchedule div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #viewSchedule form {\n    background: white;\n    padding: 20px 26px; }\n\n@media (min-width: 1440px) {\n  .col-sm-11 {\n    width: 88.666667%; }\n  .col-sm-5 {\n    width: 38.666667%; }\n  .col-sm-1 {\n    width: 11.333333%; } }\n\n@media (min-width: 1366px) {\n  .col-sm-11 {\n    width: 87.666667%; }\n  .col-sm-5 {\n    width: 37.666667%; }\n  .col-sm-1 {\n    width: 12.333333%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/schedule-view/schedule-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScheduleViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ScheduleViewComponent = (function () {
    function ScheduleViewComponent(dutyService, activitedRoute) {
        this.dutyService = dutyService;
        this.activitedRoute = activitedRoute;
        this.formBuilder = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]();
        this.msgs = []; // 表单验证提示
        this.selectedDepartment = ''; // 表单显示的已选中的部门组织
        this.msgPop = []; // 验证消息弹框提示
        this.template_file_path = '';
    }
    ScheduleViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.currentSheduleId = this.activitedRoute.snapshot.paramMap.get('id');
        this.dutyService.getScheduleById(this.currentSheduleId).subscribe(function (res) {
            console.log(res);
            _this.defaultId = res.did;
            _this.defaultCreator = res.creator;
            _this.defaultName = res.name;
            _this.defaultCreateTime = res.create_time;
            _this.selectedDepartment = res.duty_org;
            _this.beginTime = res['begin_time'];
            _this.endTime = res.end_time;
            _this.approveSelectedMultipleOption = res['approver'];
            _this.shiftSelectedMultipleOption = res['shifts_name'];
            _this.relativeSelectedMultipleOption = res['related_duty'];
            _this.defaultstatus = res['status'];
            _this.apporvalSuggestion = res.approve_remarks;
            _this.template_file_path = res['template_file_path'];
        });
        this.progressBar = true;
    };
    Object.defineProperty(ScheduleViewComponent.prototype, "isName", {
        get: function () {
            return !this.newSchdule.controls['schduleName'].untouched && (!this.newSchdule.controls['schduleName'].valid);
        },
        enumerable: true,
        configurable: true
    });
    ScheduleViewComponent.prototype.createForm = function () {
        this.newSchdule = this.formBuilder.group({
            schduleName: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleApprovor: [this.approveSelectedMultipleOption, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleId: [this.defaultId, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleCreator: [this.defaultCreator, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            scheduleCreateTime: [this.defaultCreateTime, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            shift: '',
            start: this.beginTime
        });
    };
    ScheduleViewComponent.prototype.download = function () {
        this.progressBar = false;
        if (!this.template_file_path) {
            console.log(this.template_file_path);
            this.progressBar = true;
            this.msgPop = [];
            this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '下载排班计划失败：排班计划未提交' });
        }
        else {
            this.progressBar = true;
            this.msgPop = [];
            window.open(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].url.management + "/" + this.template_file_path, '_blank');
            this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '模板生成成功！请在本地文件夹中查看' });
        }
    };
    ScheduleViewComponent.prototype.hiddenProgBar = function () {
        this.progressBar = !this.progressBar;
    };
    ScheduleViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-schedule-view',
            template: __webpack_require__("../../../../../src/app/duty/schedule-view/schedule-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/schedule-view/schedule-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__duty_service__["a" /* DutyService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"]])
    ], ScheduleViewComponent);
    return ScheduleViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/duty/searchObj.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchObjModel; });
var SearchObjModel = (function () {
    function SearchObjModel(obj) {
        this.sid = obj && obj['sid'] || '';
        this.creator = obj && obj['creator'] || '';
        this.acceptor = obj && obj['acceptor'] || '';
        this.status = obj && obj['status'] || '';
        this.page_size = obj && obj['page_size'] || '10';
        this.page_number = obj && obj['page_number'] || '1';
    }
    return SearchObjModel;
}());



/***/ }),

/***/ "../../../../../src/app/duty/setting-duty/setting-duty.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <span class=\"feature-title\">值班管理<span class=\"gt\">&gt;</span>班次设置  </span>\n    </div>\n</div>\n\n<div class=\"content-section implementation GridDemo\" id=\"dutySetting\">\n    <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\">\n        <div class=\"ui-grid-row\">\n            <div>\n                <button pButton type=\"button\"  label=\"新增班次\" (click)=\"displayDialog('add')\"></button>\n            </div>\n        </div>\n    </div>\n\n    <p-dataTable [value]=\"scheduleDatas\" [responsive]=\"true\"  [(selection)]=\"selectShifts\">\n        <p-column [style]=\"{'width':'30px'}\" selectionMode=\"multiple\" ></p-column>\n        <p-column field=\"color\" header=\"值班颜色\" [sortable]=\"true\">\n            <ng-template let-bg=\"rowData\" pTemplate=\"bg\">\n                <div (click)=\"test(bg)\" [style.width.%]=\"100\"\n                      [style.height.%]=\"100\" [style.background]=\"bg.color\" >{{bg.color}}</div>\n            </ng-template>\n        </p-column>\n        <p-column field=\"name\" header=\"班次名称\" [sortable]=\"true\"></p-column>\n        <p-column field=\"sid\" header=\"班次代码\" [sortable]=\"true\"></p-column>\n        <p-column field=\"shift_type\" header=\"班次类型\" [sortable]=\"true\"></p-column>\n        <p-column field=\"overall_time\" header=\"总时间(h)\" [sortable]=\"true\"></p-column>\n        <p-column field=\"begin_time_1\" header=\"开始时间\" [sortable]=\"true\"></p-column>\n        <p-column field=\"end_time_1\" header=\"结束时间\" [sortable]=\"true\"></p-column>\n        <p-column field=\"status\" header=\"状态\" [sortable]=\"true\"></p-column>\n        <p-column field=\"\" header=\"操作\" [style]=\"{'width':'12vw'}\">\n            <ng-template let-schedule=\"rowData\" pTemplate=\"operator\">\n                <button pButton type=\"button\"  (click)=\"editShifts(schedule)\" label=\"编辑\"  ></button>\n                <button pButton type=\"button\" *ngIf=\"schedule.status === '启用'\" (click)=\"freezeOrUnfreeze(schedule)\" label=\"冻结\" ></button>\n                <button pButton type=\"button\" *ngIf=\"schedule.status === '冻结'\" (click)=\"freezeOrUnfreeze(schedule)\" label=\"启用\" ></button>\n                <button pButton type=\"button\"  (click)=\"view(schedule)\" label=\"查看\"  ></button>\n                <button pButton type=\"button\"  (click)=\"deleteShifts(schedule)\" label=\"删除\" ></button>\n            </ng-template>\n        </p-column>\n        <ng-template pTemplate=\"emptymessage\">\n            当前没有数据\n        </ng-template>\n    </p-dataTable>\n    <p-paginator rows=\"10\" totalRecords=\"{{total}}\" [rowsPerPageOptions]=\"[10,20,30]\"   (onPageChange)=\"paginate($event)\"></p-paginator>\n    <!--<p-growl [(value)]=\"msgs\"></p-growl>-->\n    <!--<p-growl [(value)]=\"deletemsgs\"></p-growl>-->\n\n    <p-dialog header=\"{{dialogHeader}}\" [(visible)]=\"display\" modal=\"modal\" width=\"600\" (onHide)=\"onHide()\"[responsive]=\"true\">\n        <p-messages [(value)]=\"applymsgs\"></p-messages>\n        <div class=\"ui-grid-row\">\n            <form [formGroup]=\"shiftForm\" class=\"form-horizontal\">\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">\n                        <span ngClass=\"start_red\">*</span>班次名称：</label>\n                    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n                        <input type=\"text\" pInputText\n                               formControlName=\"name\"\n                               class=\"form-control cursor_not_allowed\"\n                               [(ngModel)]=\"shiftObj.name\"/>\n                        <app-field-error-display\n                           [displayError]=\"isFieldValid('name')\"\n                            errorMsg=\"不能为空\"\n                        ></app-field-error-display>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">\n                        <span ngClass=\"start_red\">*</span>班次代码：</label>\n                    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n                        <input type=\"text\" pInputText\n                               formControlName=\"sid\"\n                               class=\"form-control cursor_not_allowed\"\n                               [(ngModel)]=\"shiftObj.sid\"/>\n                        <app-field-error-display\n                                [displayError]=\"isFieldValid('sid')\"\n                                errorMsg=\"不能为空\"\n                        ></app-field-error-display>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">\n                        <span ngClass=\"start_red\">*</span>班次类型：</label>\n                    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid input-padding\">\n                        <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                            <p-radioButton name=\"group\"\n                                           value=\"单班次\"\n                                           label=\"单班次\"\n                                           [(ngModel)]=\"shiftObj.shift_type\"\n                                           inputId=\"preopt3\"\n                                           (onClick)=\"dbshift('sg')\"\n                                           [ngModelOptions]=\"{standalone: true}\">\n                            </p-radioButton>\n                        </div>\n                        <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                            <p-radioButton name=\"group\"\n                                           value=\"两头班\"\n                                           label=\"两头班\"\n                                           [(ngModel)]=\"shiftObj.shift_type\"\n                                           inputId=\"preopt4\"\n                                           (onClick)=\"dbshift('db')\"\n                                           [ngModelOptions]=\"{standalone: true}\">\n                            </p-radioButton>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">\n                        <span ngClass=\"start_red\">*</span>值班时间1：</label>\n                    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                        <p-calendar [(ngModel)]=\"beginTime\"\n                                    [timeOnly]=\"true\"\n                                    [showIcon]=\"true\"\n                                    [showSeconds]=\"true\"\n                                    formControlName=\"beginTime\"\n                                    [styleClass]=\"'schedule-add'\"\n                                    (onSelect)=\"computeTime()\">\n                        </p-calendar>\n                        <app-field-error-display\n                            [displayError]=\"isFieldValid('beginTime')\"\n                            errorMsg=\"不能为空\"\n                        ></app-field-error-display>\n                    </div>\n                    <label class=\"col-sm-1 control-label\"> 至</label>\n                    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                        <p-calendar [(ngModel)]=\"endTime\"\n                                    [timeOnly]=\"true\"\n                                    [showIcon]=\"true\"\n                                    [showSeconds]=\"true\"\n                                    formControlName=\"endTime\"\n                                    [styleClass]=\"'schedule-add'\"\n                                    (onSelect)=\"computeTime()\">\n                        </p-calendar>\n                        <app-field-error-display\n                                [displayError]=\"isFieldValid('endTime')\"\n                                errorMsg=\"不能为空\"\n                        ></app-field-error-display>\n                    </div>\n                </div>\n                <div class=\"form-group\" *ngIf=\"displayTime\">\n                    <label class=\"col-sm-2 control-label\">\n                        <span ngClass=\"start_red\">*</span>值班时间2：</label>\n                    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                        <p-calendar [(ngModel)]=\"dbeginTime\"\n                                    [timeOnly]=\"true\"\n                                    [showIcon]=\"true\"\n                                    [showSeconds]=\"true\"\n                                    [styleClass]=\"'schedule-add'\"\n                                    [required]=\"true\"\n                                    formControlName=\"dbeginTime\"\n                                    (onSelect)=\"computeTime()\">\n                        </p-calendar>\n                        <app-field-error-display\n                                [displayError]=\"isFieldValid('dbeginTime')\"\n                                errorMsg=\"不能为空\"\n                        ></app-field-error-display>\n                    </div>\n                    <label class=\"col-sm-1 control-label\"> 至</label>\n                    <div class=\"col-sm-4 ui-fluid-no-padding ui-fluid\">\n                        <p-calendar [(ngModel)]=\"dendTime\"\n                                    [timeOnly]=\"true\"\n                                    [showIcon]=\"true\"\n                                    [showSeconds]=\"true\"\n                                    [styleClass]=\"'schedule-add'\"\n                                    [required]=\"true\"\n                                    formControlName=\"dendTime\"\n                                    (onSelect)=\"computeTime()\">\n                        </p-calendar>\n                        <app-field-error-display\n                                [displayError]=\"isFieldValid('dendTime')\"\n                                errorMsg=\"不能为空\"\n                        ></app-field-error-display>\n                    </div>\n                </div>\n                <div class=\"form-group \">\n                    <label class=\"col-sm-2 control-label\">\n                        <span ngClass=\"start_red\">*</span>总时间(h)：\n                    </label>\n                    <div class=\"col-sm-10 ui-no-padding-left-15px\">\n                        <input type=\"text\" pInputText\n                               name=\"department\" placeholder=\"请选择关联服务目录\"\n                               readonly\n                               [(ngModel)]=\"shiftObj.overall_time\"\n                               [ngModelOptions]=\"{standalone: true}\"\n                               class=\"form-control cursor_not_allowed\"/>\n                    </div>\n                </div>\n                <div class=\"form-group \">\n                    <label class=\"col-sm-2 control-label\">\n                        <span ngClass=\"start_red\">*</span>班次颜色：\n                    </label>\n                    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid\">\n                        <p-colorPicker [(ngModel)]=\"shiftObj.color\"\n                                       id=\"colorPicker\"\n                                       [ngModelOptions]=\"{standalone: true}\">\n                        </p-colorPicker>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">\n                        <span ngClass=\"start_red\">*</span>状态：</label>\n                    <div class=\"col-sm-10 ui-no-padding-left-15px ui-fluid input-padding\">\n                        <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                            <p-radioButton name=\"group2\"\n                                           value=\"启用\"\n                                           label=\"启用\"\n                                           [(ngModel)]=\"shiftObj.status\"\n                                           inputId=\"preopt1\"\n                                           [ngModelOptions]=\"{standalone: true}\">\n                            </p-radioButton>\n                        </div>\n                        <div class=\"col-sm-5 ui-no-padding-left-15px\">\n                            <p-radioButton name=\"group2\"\n                                           value=\"冻结\"\n                                           label=\"冻结\"\n                                           [(ngModel)]=\"shiftObj.status\"\n                                           inputId=\"preopt2\"\n                                           [ngModelOptions]=\"{standalone: true}\">\n                            </p-radioButton>\n                        </div>\n                    </div>\n                </div>\n            </form>\n        </div>\n        <p-footer *ngIf=\"disableForm\">\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"addshift()\" label=\"保存\"></button>\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"cancel()\" label=\"取消\" class=\"ui-button-secondary\"></button>\n        </p-footer>\n    </p-dialog>\n    <p-growl [(value)]=\"msgPop\"></p-growl>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/duty/setting-duty/setting-duty.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n#colorPicker /deep/ div input {\n  width: 14vw; }\n\n.red_start {\n  color: red; }\n\n#dutySetting {\n  font-size: 14px; }\n  #dutySetting p-dataTable /deep/ div table tbody tr td {\n    word-break: break-all;\n    word-wrap: break-word;\n    padding: 0.25em; }\n    #dutySetting p-dataTable /deep/ div table tbody tr td button {\n      margin-bottom: .1vw; }\n  #dutySetting p-dataTable /deep/ table thead tr th:nth-child(10) {\n    width: 16% !important; }\n  #dutySetting div[class=\"ui-grid-row\"]:nth-child(1) {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  #dutySetting p-calendar /deep/ span > input {\n    border-radius: 0;\n    padding: .3em;\n    height: auto;\n    box-shadow: none !important; }\n\n.calendar-control {\n  border-radius: 0;\n  padding: .3em;\n  height: auto;\n  box-shadow: none !important; }\n\n.input-padding {\n  padding: 7px; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 21.666667%; }\n  .col-sm-10 {\n    width: 78.333333%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/setting-duty/setting-duty.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingDutyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__duty_service__ = __webpack_require__("../../../../../src/app/duty/duty.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__ = __webpack_require__("../../../../../src/app/services/PUblicMethod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shiftObj_model__ = __webpack_require__("../../../../../src/app/duty/shiftObj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SettingDutyComponent = (function (_super) {
    __extends(SettingDutyComponent, _super);
    function SettingDutyComponent(dutyService, c, m) {
        var _this = _super.call(this, c, m) || this;
        _this.dutyService = dutyService;
        _this.c = c;
        _this.m = m;
        _this.beginTime = new Date();
        _this.endTime = new Date();
        _this.dbeginTime = new Date();
        _this.dendTime = new Date();
        _this.fb = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]();
        _this.currentPage = '1';
        _this.currentRow = '10';
        _this.displayTime = false;
        _this.disableForm = true;
        _this.markedAdUntouded = function () {
            _this.shiftForm.get('name').markAsUntouched({ onlySelf: true });
            _this.shiftForm.get('beginTime').markAsUntouched({ onlySelf: true });
            _this.shiftForm.get('endTime').markAsUntouched({ onlySelf: true });
            _this.shiftForm.get('dbeginTime').markAsUntouched({ onlySelf: true });
            _this.shiftForm.get('dendTime').markAsUntouched({ onlySelf: true });
        };
        return _this;
    }
    SettingDutyComponent.prototype.ngOnInit = function () {
        this.shiftObj = new __WEBPACK_IMPORTED_MODULE_5__shiftObj_model__["a" /* ShiftObjModel */]();
        this.creatForm();
        this.shiftObj.status = '启用';
        this.shiftObj.shift_type = '单班次';
        this.display = false;
        this.shiftObj.overall_time = '0';
        this.shiftObj.color = '#f51313';
        this.zh = new __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */]().initZh();
        this.beginTime = new Date();
        this.endTime = new Date();
        this.dbeginTime = new Date();
        this.dendTime = new Date();
        this.getSettings();
    };
    SettingDutyComponent.prototype.getSettings = function () {
        var _this = this;
        this.dutyService.getAllShiftQuery(this.currentPage, this.currentRow).subscribe(function (res) {
            _this.scheduleDatas = res.items;
            console.log(res);
            _this.total = res.page.total;
            _this.page_size = res.page.page_size;
            _this.page_total = res.page.page_total;
        });
    };
    SettingDutyComponent.prototype.creatForm = function () {
        this.shiftForm = this.fb.group({
            name: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            sid: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            beginTime: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            endTime: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            dbeginTime: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            dendTime: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    };
    SettingDutyComponent.prototype.isFieldValid = function (field) {
        return !this.shiftForm.get(field).valid && this.shiftForm.get(field).touched;
    };
    SettingDutyComponent.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormGroup"]) {
                _this.validateAllFormFields(control);
            }
        });
    };
    SettingDutyComponent.prototype.dbshift = function (type) {
        (type === 'sg') && (this.displayTime = false);
        (type === 'db') && (this.displayTime = true);
    };
    SettingDutyComponent.prototype.displayDialog = function (type) {
        this.shiftForm.get('name').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('sid').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('beginTime').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('endTime').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('dbeginTime').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('dendTime').enable({ onlySelf: true, emitEvent: true });
        if (type === 'add') {
            this.dialogHeader = '新增班次';
            this.shiftObj.name = '';
            this.shiftObj.sid = '';
            this.shiftObj.shift_type = '单班次';
            this.beginTime = new Date();
            this.endTime = new Date();
            this.dbeginTime = new Date();
            this.dendTime = new Date();
            this.shiftObj.overall_time = '0';
            this.shiftObj.color = '#f51313';
            this.shiftObj.status = '启用';
            this.creatForm();
        }
        if (type === 'edit') {
            this.dialogHeader = '编辑班次';
        }
        if (type === 'view') {
            this.dialogHeader = '查看班次';
        }
        ;
        this.display = true;
    };
    SettingDutyComponent.prototype.requestAddShift = function (obj) {
        var _this = this;
        this.dutyService.addDiyShift(obj).subscribe(function (res) {
            if (res === '00000') {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '班次新增成功' });
                _this.display = false;
                _this.getSettings();
            }
            else {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '班次新增失败' + '\n' + res });
            }
        });
    };
    SettingDutyComponent.prototype.cancel = function () {
        this.msgPop = [];
        this.display = false;
    };
    SettingDutyComponent.prototype.requestEditShift = function (obj) {
        var _this = this;
        this.dutyService.editDiyShift(obj).subscribe(function (res) {
            if (res === '00000') {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '班次编辑成功' });
                _this.display = false;
                _this.getSettings();
            }
            else {
                _this.msgPop = [];
                _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '班次编辑失败' + '\n' + res });
            }
        });
    };
    SettingDutyComponent.prototype.formateTime = function () {
        this.shiftObj.end_time_1 = __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(this.endTime);
        this.shiftObj.begin_time_1 = __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(this.beginTime);
        this.shiftObj.end_time_2 = __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(this.dendTime);
        this.shiftObj.begin_time_2 = __WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateMiniteTime(this.dbeginTime);
    };
    SettingDutyComponent.prototype.addshift = function () {
        this.formateTime();
        if (!this.displayTime && this.shiftObj.sid && this.shiftObj.name) {
            this.shiftObj.begin_time_2 = '';
            this.shiftObj.end_time_2 = '';
            (this.dialogHeader === '新增班次') && (this.requestAddShift(this.shiftObj));
            (this.dialogHeader === '编辑班次') && (this.requestEditShift(this.shiftObj));
        }
        else if (this.displayTime && this.shiftObj.sid && this.shiftObj.name) {
            (this.dialogHeader === '新增班次') && (this.requestAddShift(this.shiftObj));
            (this.dialogHeader === '编辑班次') && (this.requestEditShift(this.shiftObj));
        }
        else {
            this.validateAllFormFields(this.shiftForm);
        }
    };
    SettingDutyComponent.prototype.editShifts = function (shift) {
        this.displayDialog('edit');
        this.shiftForm.get('name').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('sid').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('beginTime').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('endTime').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('dbeginTime').enable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('dendTime').enable({ onlySelf: true, emitEvent: true });
        this.shiftObj = new __WEBPACK_IMPORTED_MODULE_5__shiftObj_model__["a" /* ShiftObjModel */](shift);
        this.beginTime = new Date(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateTimeToEntryTime(shift.begin_time_1)));
        this.endTime = new Date(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateTimeToEntryTime(shift.end_time_1)));
        this.dispalyDoubleTime(shift);
    };
    SettingDutyComponent.prototype.dispalyDoubleTime = function (shift) {
        if (this.shiftObj.shift_type === '两头班') {
            this.displayTime = true;
            this.dbeginTime = new Date(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateTimeToEntryTime(shift.begin_time_2)));
            this.dendTime = new Date(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateTimeToEntryTime(shift.end_time_2)));
        }
    };
    SettingDutyComponent.prototype.freezeOrUnfreeze = function (shift) {
        var _this = this;
        var msg = '';
        (shift.status === '启用') && (msg = '确认冻结吗？');
        (shift.status === '冻结') && (msg = '确认启用吗？');
        this.c.confirm({
            message: msg,
            accept: function () {
                if (shift.status === '启用') {
                    shift.status = '冻结';
                }
                else if (shift.status === '冻结') {
                    shift.status = '启用';
                }
                _this.dutyService.freezeOrUnfreezeDiyShift(shift).subscribe(function (res) {
                    if (res === '00000') {
                        _this.alert('操作成功');
                        _this.getSettings();
                    }
                    else {
                        _this.alert("\u64CD\u4F5C\u5931\u8D25 + " + res, 'error');
                        _this.getSettings();
                    }
                    // (res === '00000') && (this.alert('操作成功'));
                    // (!(res === '00000')) && (this.alert(`操作失败 + ${res}`, 'error'));
                });
            },
            reject: function () {
            }
        });
    };
    SettingDutyComponent.prototype.view = function (shift) {
        this.markedAdUntouded();
        this.displayDialog('view');
        this.shiftObj = new __WEBPACK_IMPORTED_MODULE_5__shiftObj_model__["a" /* ShiftObjModel */](shift);
        this.beginTime = new Date(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateTimeToEntryTime(shift.begin_time_1)));
        this.endTime = new Date(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateEnrtyTime(__WEBPACK_IMPORTED_MODULE_4__services_PUblicMethod__["a" /* PUblicMethod */].formateTimeToEntryTime(shift.end_time_1)));
        this.dispalyDoubleTime(shift);
        this.shiftForm.get('name').disable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('sid').disable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('beginTime').disable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('endTime').disable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('dbeginTime').disable({ onlySelf: true, emitEvent: true });
        this.shiftForm.get('dendTime').disable({ onlySelf: true, emitEvent: true });
        // this.shiftForm.get('shift_type').disable({onlySelf: true, emitEvent: true});
        // this.shiftForm.get('status').disable({onlySelf: true, emitEvent: true});
        this.disableForm = false;
    };
    SettingDutyComponent.prototype.deleteShifts = function (shift) {
        var _this = this;
        this.c.confirm({
            message: '确认删除吗?',
            accept: function () {
                _this.dutyService.deletDiyShift([shift.sid]).subscribe(function (res) {
                    if (res === '00000') {
                        _this.msgPop = [];
                        _this.msgPop.push({ severity: 'success', summary: '消息提示', detail: '班次删除成功' });
                        _this.getSettings();
                    }
                    else {
                        _this.msgPop = [];
                        _this.msgPop.push({ severity: 'error', summary: '提示消息', detail: '班次删除失败' + '\n' + res });
                    }
                });
            }
        });
    };
    SettingDutyComponent.prototype.computeTime = function () {
        var datas = this.endTime.getTime() - this.beginTime.getTime();
        // 计算出小时数
        var leave1 = datas % (24 * 3600 * 1000); // 计算天数后剩余的毫秒数
        var hours = Math.floor(leave1 / (3600 * 1000));
        // 计算相差分钟数
        var leave2 = leave1 % (3600 * 1000); // 计算小时数后剩余的毫秒数
        var minutes = Math.floor(leave2 / (60 * 1000));
        // 计算相差秒数
        var leave3 = leave2 % (60 * 1000); // 计算分钟数后剩余的毫秒数
        var seconds = Math.round(leave3 / 1000);
        // console.log(" 相差 "+hours+"小时 "+minutes+" 分钟"+seconds+" 秒");
        this.shiftObj.overall_time = hours + '小时 ' + minutes + ' 分钟' + seconds + ' 秒';
    };
    SettingDutyComponent.prototype.paginate = function (event) {
        var _this = this;
        this.currentPage = (event.page + 1).toString();
        this.currentRow = event.rows.toString();
        this.dutyService.getAllShiftQuery((event.page + 1).toString(), event.rows.toString()).subscribe(function (res) {
            _this.scheduleDatas = res.items;
            _this.total = res.page.total;
            _this.page_size = res.page.page_size;
            _this.page_total = res.page.page_total;
        });
    };
    SettingDutyComponent.prototype.onHide = function () {
        this.display = false;
        this.displayTime = false;
        this.disableForm = true;
    };
    SettingDutyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-setting-duty',
            template: __webpack_require__("../../../../../src/app/duty/setting-duty/setting-duty.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/setting-duty/setting-duty.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__duty_service__["a" /* DutyService */],
            __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_7_primeng_components_common_messageservice__["MessageService"]])
    ], SettingDutyComponent);
    return SettingDutyComponent;
}(__WEBPACK_IMPORTED_MODULE_6__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/duty/shiftObj.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShiftObjModel; });
var ShiftObjModel = (function () {
    function ShiftObjModel(obj) {
        this.sid = obj && obj['sid'] || '';
        this.name = obj && obj['name'] || '';
        this.shift_type = obj && obj['shift_type'] || '';
        this.begin_time_1 = obj && obj['begin_time_1'] || '';
        this.end_time_1 = obj && obj['end_time_1'] || '';
        this.begin_time_2 = obj && obj['begin_time_2'] || '';
        this.end_time_2 = obj && obj['end_time_2'] || '';
        this.overall_time = obj && obj['overall_time'] || '';
        this.color = obj && obj['color'] || '';
        this.status = obj && obj['status'] || '';
    }
    return ShiftObjModel;
}());



/***/ }),

/***/ "../../../../../src/app/duty/view-handover/view-handover.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction\">\n    <div>\n        <!--<span class=\"gt\">&gt;</span>{{title}}-->\n        <span class=\"feature-title\">值班管理</span>\n    </div>\n</div>\n<div class=\"content-section\" id=\"myContent\" >\n    <div class=\"title col-sm-12\">\n        <div class=\"col-sm-1\">\n            <span>{{title}}</span>\n        </div>\n        <div class=\"col-sm-11 pull-right\">\n            <button type=\"button\"\n                    class=\"pull-right\"\n                    pButton\n                    (click)=\"goBack()\"\n                    icon=\"fa-close\"\n                    style=\"width: 30px\"></button>\n        </div>\n    </div>\n    <form  class=\"form-horizontal\">\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">交班单号：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       readonly\n                       [value]=\"formObj.sid\"\n                />\n            </div>\n            <label class=\"col-sm-2 control-label\">交班时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [value]=\"formObj.create_time\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">班次名称：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"自动生成\"\n                       readonly\n                       [value]=\"formObj.shift_name\"\n                />\n            </div>\n            <label class=\"col-sm-2 control-label\">班次时间：</label>\n            <div class=\"col-sm-4 ui-fluid-no-padding\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [value]=\"formObj.shift_time\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">接班人：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding ui-fluid\">\n                <input type=\"text\"\n                       class=\" form-control cursor_not_allowed\"\n                       pInputText\n                       placeholder=\"自动生成\"\n                       [value]=\"formObj.acceptor\"\n                       readonly />\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">交班内容：</label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-no-padding-right-15px ui-fluid\">\n                 <textarea [rows]=\"5\" pInputTextarea\n                           autoResize=\"autoResize\"\n                           readonly\n                           class=\" form-control cursor_not_allowed\"\n                           [value]=\"formObj.content\"></textarea>\n            </div>\n        </div>\n        <div class=\"form-group \">\n            <label class=\"col-sm-2 control-label\">备注：</label>\n            <div class=\"col-sm-10 ui-no-padding-left-15px ui-no-padding-right-15px ui-fluid\">\n                 <textarea [rows]=\"5\" pInputTextarea\n                           autoResize=\"autoResize\"\n                           readonly\n                           class=\" form-control cursor_not_allowed\"\n                           [value]=\"formObj.remarks\"></textarea>\n            </div>\n        </div>\n    </form>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/duty/view-handover/view-handover.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#myContent {\n  font-size: 14px; }\n  #myContent div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #myContent div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #myContent form {\n    background: white;\n    padding: 20px 26px 20px 0px;\n    border: 1px solid #e2e2e2; }\n\n.padding-left-2vw {\n  padding-left: 2vw; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 12.666667%; }\n  .col-sm-10 {\n    width: 79.333333%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/duty/view-handover/view-handover.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewHandoverComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__handoverobj_model__ = __webpack_require__("../../../../../src/app/duty/handoverobj.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ViewHandoverComponent = (function () {
    function ViewHandoverComponent(router, activedRouter) {
        this.router = router;
        this.activedRouter = activedRouter;
        this.title = '查看';
    }
    ViewHandoverComponent.prototype.ngOnInit = function () {
        this.formObj = new __WEBPACK_IMPORTED_MODULE_1__handoverobj_model__["a" /* HandoverobjModel */]();
        this.getRouterParam();
    };
    ViewHandoverComponent.prototype.getRouterParam = function () {
        var _this = this;
        this.activedRouter.queryParams.subscribe(function (param) {
            if (param['datas']) {
                _this.formObj = new __WEBPACK_IMPORTED_MODULE_1__handoverobj_model__["a" /* HandoverobjModel */](JSON.parse(param['datas']));
                console.dir(JSON.parse(param['datas']));
            }
        });
    };
    ViewHandoverComponent.prototype.goBack = function () {
        this.router.navigate(['../handoveroverview'], { relativeTo: this.activedRouter });
    };
    ViewHandoverComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-view-handover',
            template: __webpack_require__("../../../../../src/app/duty/view-handover/view-handover.component.html"),
            styles: [__webpack_require__("../../../../../src/app/duty/view-handover/view-handover.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"]])
    ], ViewHandoverComponent);
    return ViewHandoverComponent;
}());



/***/ })

});
//# sourceMappingURL=duty.module.chunk.js.map