webpackJsonp(["report.module"],{

/***/ "../../../../../src/app/report/history-report/history-report.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"content-section introduction  \">\n  <div>\n    <span class=\"feature-title\">报障管理<span class=\"gt\">&gt;</span>历史报障  </span>\n  </div>\n</div>\n\n<div class=\"content-section implementation GridDemo history\" >\n  <div class=\"ui-g \">\n    <div class=\"ui-g-12 ui-md-12 ui-lg-12 \">\n  <div class=\"mysearch\">\n      <div class=\"ui-g\">\n        <div class=\"ui-g-4 ui-sm-12\">\n          <label class=\"ui-g-3 ui-sm-5 text-right\">故障所属:</label>\n          <div class=\"ui-g-3  ui-sm-7 ui-fluid\">\n            <input pInputText type=\"text\" name=\"sid\" placeholder=\"故障所属\" [(ngModel)]=\"queryModel.fault_type\" />\n          </div>\n          <label class=\"ui-g-3 ui-sm-5 ui-no-padding-left-15px text-right\">报障人:</label>\n          <div class=\"ui-g-3 ui-sm-7 ui-fluid  ui-no-padding-right-15px \">\n            <input  pInputText type=\"text\" name=\"status\" placeholder=\"报障人\"  [(ngModel)]=\"queryModel.people\" />\n          </div>\n        </div>\n        <div class=\"ui-g-4 ui-sm-12\">\n          <label class=\"ui-g-3 ui-sm-5 text-right\">报障时间:</label>\n          <div class=\"ui-g-4  ui-sm-7 ui-fluid\">\n            <p-calendar  [showIcon]=\"true\" [locale]=\"zh\" name=\"time_start\"\n                         [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\"  dataType=\"string\"\n                         [minDate]=\"minDate\" [(ngModel)]=\"queryModel.time_start\"  >\n            </p-calendar>\n          </div>\n          <label class=\"ui-g-1 ui-sm-5 text-center\" >至:</label>\n          <div class=\"ui-g-4  ui-sm-7 ui-fluid \">\n            <p-calendar  [showIcon]=\"true\" [locale]=\"zh\" name=\"end_time\"\n                         [styleClass]=\"'schedule-add'\" dateFormat=\"yy-mm-dd\"  dataType=\"string\"\n                         [minDate]=\"minDate\" [(ngModel)]=\"queryModel.time_end\" >\n            </p-calendar>\n          </div>\n        </div>\n        <div class=\"ui-g-4 ui-sm-12\">\n          <label  class=\"ui-g-3 ui-sm-5 text-right\">状态:</label>\n          <div class=\"ui-g-4  ui-sm-7 ui-fluid \">\n            <p-dropdown [options]=\"MaterialStatusData\" [(ngModel)]=\"queryModel.status\" [autoWidth]=\"false\" name=\"cycle\" (onChange)=\"suggestInsepectiones()\" ></p-dropdown>\n          </div>\n          <div class=\"ui-g-5 ui-sm-12 option\">\n            <button pButton type=\"button\" label=\"查询\" ngClass=\"ui-sm-12\" (click)=\"suggestInsepectiones()\" ></button>\n            <button pButton  label=\"清空\" ngClass=\"ui-sm-12\"  (click)=\"clearSearch()\"></button>\n          </div>\n        </div>\n      </div>\n  </div>\n    </div>\n  </div>\n\n  <div class=\"ui-g \">\n    <div class=\"ui-g-12 ui-md-12 ui-lg-12 \">\n    <p-dataTable class=\"report\" [value]=\"historyReportModel\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n                   [totalRecords]=\"totalRecords\" (onLazyLoad)=\"loadCarsLazy($event)\" [stacked]=\"stacked\" [responsive]=\"true\" [editable]=\"true\">\n      <p-column  field=\"cid\" header=\"单号\" [sortable]=\"true\" >\n        <ng-template let-data=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n          <span (click)=\"onOperate(data)\" class=\"curser\">{{data.cid}}</span>\n        </ng-template>\n      </p-column>\n      <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [sortable]=\"true\"></p-column>\n        <p-column field=\"color\" header=\"操作\" >\n          <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n            <button pButton type=\"button\" label=\"编辑\" (click)=\"updateOption(dataSource[i])\" *ngIf=\"dataSource[i]['status']==='新建'\"></button>\n            <button pButton type=\"button\" label=\"删除\" (click)=\"deleteReport(dataSource[i],i)\" *ngIf=\"dataSource[i]['status']==='新建'\"></button>\n            <button pButton type=\"button\" label=\"处理\" (click)=\"showReportTreatmentMask(dataSource[i],i)\"  *ngIf=\"dataSource[i]['status']==='未处理'\"></button>\n            <button pButton type=\"button\" label=\"查看\" (click)=\"showReport(dataSource[i])\"></button>\n          </ng-template>\n        </p-column>\n        <ng-template pTemplate=\"emptymessage\">\n          当前没有数据\n        </ng-template>\n      </p-dataTable>\n    </div>\n  </div>\n</div>\n\n<!--查看详情model-->\n<app-view-detail\n  [reportDetail]=\"currentHistoryReport\"\n  (closeViewDetail)=\"closeViewDetail($event)\"\n  *ngIf=\"showViewDetailMask\" ></app-view-detail>\n<!--报障处理-->\n<app-report-treatment\n  [reportTreatment]=\"currentHistoryReport\"\n  (closeReportTreatmentMask)=\"closeReportTreamentMask($event)\"\n  *ngIf=\"showReportTreatment\" (updateDev)=\"updatehistoryReportModel($event)\"></app-report-treatment>\n\n"

/***/ }),

/***/ "../../../../../src/app/report/history-report/history-report.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n.mysearch {\n  margin-bottom: 20px;\n  background: #fff; }\n\n@media screen and (min-width: 1024px) {\n  .ui-g label {\n    margin-top: 2px; } }\n\n@media screen and (max-width: 768px) {\n  .ui-g label {\n    text-align: left; } }\n\n.option {\n  text-align: right; }\n\n.text-right {\n  text-align: right; }\n\n@media screen and (min-width: 1366px) {\n  .history /deep/ table thead tr th:last-child {\n    width: 15%; }\n  .history /deep/ table thead tr th:nth-child(2) {\n    width: 10%; }\n  .history /deep/ table thead tr th:nth-child(3) {\n    width: 10%; }\n  .history /deep/ table thead tr th:nth-child(5) {\n    width: 8%; }\n  .history /deep/ table thead tr th:nth-child(6) {\n    width: 7%; }\n  .history /deep/ table thead tr th:nth-child(7) {\n    width: 6%; } }\n\n.report {\n  height: 50px;\n  overflow: auto; }\n\n.history label {\n  display: inline-block;\n  max-width: 100%;\n  margin-bottom: 5px;\n  color: black;\n  font-weight: 100; }\n\n.curser {\n  color: #39b9c6;\n  cursor: pointer; }\n\n/*修改表格排序时候的字体颜色*/\n/deep/ .ui-datatable th.ui-state-active {\n  color: #555555; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/history-report/history-report.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryReportComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HistoryReportComponent = (function (_super) {
    __extends(HistoryReportComponent, _super);
    function HistoryReportComponent(reportService, storageService, router, route, confirmationService, messageService) {
        var _this = _super.call(this, confirmationService, messageService) || this;
        _this.reportService = reportService;
        _this.storageService = storageService;
        _this.router = router;
        _this.route = route;
        _this.confirmationService = confirmationService;
        _this.messageService = messageService;
        _this.stacked = false;
        _this.historyReportModel = [];
        _this.showViewDetailMask = false; //报障详情
        _this.showReportTreatment = false; //报障处理
        _this.chooseCids = [];
        _this.selectMaterial = [];
        _this.display = false;
        return _this;
    }
    HistoryReportComponent.prototype.ngOnInit = function () {
        this.brands = [
            { label: 'All Brands', value: null },
            { label: 'Audi', value: 'Audi' },
            { label: 'BMW', value: 'BMW' },
            { label: 'Fiat', value: 'Fiat' },
            { label: 'Honda', value: 'Honda' },
            { label: 'Jaguar', value: 'Jaguar' },
            { label: 'Mercedes', value: 'Mercedes' },
            { label: 'Renault', value: 'Renault' },
            { label: 'VW', value: 'VW' },
            { label: 'Volvo', value: 'Volvo' }
        ];
        this.zh = {
            firstDayOfWeek: 1,
            dayNames: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
            dayNamesShort: ['一', '二', '三', '四', '五', '六', '七'],
            dayNamesMin: ['一', '二', '三', '四', '五', '六', '七'],
            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
            monthNamesShort: ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二'],
        };
        this.windowSize = window.innerWidth;
        if (this.windowSize < 1024) {
            this.stacked = true;
        }
        this.cols = [
            { field: 'fault_type', header: '故障所属' },
            { field: 'people', header: '报障人' },
            { field: 'time', header: '报障时间' },
            { field: 'location', header: '故障位置' },
            { field: 'status', header: '状态' },
            { field: 'handle_people', header: '处理人' },
            { field: 'handle_datetime', header: '完成时间' },
        ];
        this.queryModel = {
            'id': '',
            'people': '',
            'fault_type': '',
            'status': '',
            'time_start': '',
            'time_end': '',
            'handle_people': '',
        };
        this.queryReport();
        this.queryReportStatusDate();
    };
    //查询历史报障
    HistoryReportComponent.prototype.queryReport = function () {
        var _this = this;
        this.reportService.getHistoryReport(this.queryModel).subscribe(function (data) {
            _this.dataSource = data ? data : [];
            console.log(_this.dataSource);
            for (var i = 0; i < _this.dataSource.length; i++) {
                var devices = _this.dataSource[i]['devices'] ? _this.dataSource[i]['devices'] : [];
                _this.dataSource[i]['showdevices'] = [];
                for (var j = 0; j < devices.length; j++) {
                    var temp = devices[j]['ano'];
                    _this.dataSource[i]['showdevices'].push(temp);
                }
                _this.dataSource[i]['showdevices'].join(';');
            }
            console.log(_this.dataSource);
            _this.totalRecords = _this.dataSource.length;
            _this.historyReportModel = _this.dataSource.slice(0, 10);
        });
    };
    HistoryReportComponent.prototype.clearSearch = function () {
        this.queryModel.fault_type = '';
        this.queryModel.people = '';
        this.queryModel.time_start = '';
        this.queryModel.time_end = '';
    };
    HistoryReportComponent.prototype.suggestInsepectiones = function () {
        this.queryReport();
    };
    //查看详情
    HistoryReportComponent.prototype.showReport = function (report) {
        this.currentHistoryReport = report;
        console.log(this.currentHistoryReport);
        this.showViewDetailMask = !this.showViewDetailMask;
    };
    HistoryReportComponent.prototype.closeViewDetail = function (bool) {
        this.showViewDetailMask = bool;
    };
    //故障处理model
    HistoryReportComponent.prototype.showReportTreatmentMask = function (report) {
        this.storageService.setReportId('reportid', report.id);
        this.showReportTreatment = !this.showReportTreatment;
        this.currentHistoryReport = report;
    };
    HistoryReportComponent.prototype.closeReportTreamentMask = function (bool) {
        this.showReportTreatment = bool;
    };
    HistoryReportComponent.prototype.loadCarsLazy = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.dataSource) {
                _this.historyReportModel = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 250);
    };
    //更改处理后的历史报障
    HistoryReportComponent.prototype.updatehistoryReportModel = function () {
        this.queryReport();
    };
    HistoryReportComponent.prototype.onOperate = function (report) {
        this.currentHistoryReport = report;
        this.showViewDetailMask = !this.showViewDetailMask;
    };
    HistoryReportComponent.prototype.queryReportStatusDate = function () {
        var _this = this;
        this.reportService.getStatusReport().subscribe(function (data) {
            if (!data) {
                _this.MaterialStatusData = [];
            }
            else {
                _this.MaterialStatusData = data;
                _this.MaterialStatusData = _this.reportService.formatDropdownData(_this.MaterialStatusData);
            }
        });
    };
    HistoryReportComponent.prototype.updateOption = function (current) {
        var cid = current['cid'];
        this.router.navigate(['../review'], { queryParams: { cid: cid, state: 'update', title: '编辑' }, relativeTo: this.route });
    };
    HistoryReportComponent.prototype.deleteReport = function (current) {
        var _this = this;
        var cid = current['cid'];
        //请求成功后删除本地数据
        this.confirm('确认删除吗?', function () { _this.deleteoption(cid); });
    };
    HistoryReportComponent.prototype.deleteoption = function (cid) {
        var _this = this;
        this.reportService.deleteReport(cid).subscribe(function () {
            _this.queryReport();
            _this.alert('删除成功');
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，请联系管理员或稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    HistoryReportComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-history-report',
            template: __webpack_require__("../../../../../src/app/report/history-report/history-report.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/history-report/history-report.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__report_service__["a" /* ReportService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__["MessageService"]])
    ], HistoryReportComponent);
    return HistoryReportComponent;
}(__WEBPACK_IMPORTED_MODULE_6__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/report/person-dialog/person-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"请选择人员\" [(visible)]=\"display\" modal=\"modal\" [width]=\"width\" [responsive]=\"true\" (onHide)=\"cancel()\">\n  <div class=\"ui-g\">\n    <div class=\"ui-g-4\">\n      <h4>组织</h4>\n      <p-tree [value]=\"orgs\" selectionMode=\"single\" [(selection)]=\"selected\" (onNodeExpand)=\"nodeExpand($event)\"(onNodeSelect) = \"NodeSelect($event)\" [contextMenu]=\"cm\"></p-tree>\n      <p-contextMenu #cm [model]=\"items\"></p-contextMenu>\n    </div>\n    <div class=\"ui-g-8\">\n      <h4>{{ titleName }}</h4>\n      <div class=\"ui-grid-row text_aligin_right\">\n        <p-dataTable [value]=\"tableDatas\"\n                     (onRowSelect)=\"handleRowSelect($event)\"\n                     [responsive]=\"true\"  id=\"manageTable\">\n          <p-column selectionMode=\"single\" [style]=\"{'width':'38px'}\" ></p-column>\n          <p-column field=\"pid\" header=\"帐号\" [sortable]=\"true\"></p-column>\n          <p-column field=\"name\" header=\"姓名\" [sortable]=\"true\"></p-column>\n          <p-column field=\"mobile\" header=\"手机\" [sortable]=\"true\"></p-column>\n          <p-column field=\"organization\" header=\"组织\" [sortable]=\"true\"></p-column>\n          <p-column field=\"pid\" header=\"职位\" [sortable]=\"true\"></p-column>\n          <ng-template pTemplate=\"emptymessage\">\n            当前没有数据\n          </ng-template>\n        </p-dataTable>\n      </div>\n    </div>\n  </div>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" (click)=\"sure()\"  label=\"确定\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"cancel()\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/report/person-dialog/person-dialog.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\n  margin-bottom: 1vw; }\n\n.padding-tblr {\n  padding: .25em .5em; }\n\n.start_red {\n  color: red; }\n\n.birthday {\n  display: inline-block; }\n\n@media screen and (max-width: 1366px) {\n  .ui-grid-col-1 {\n    width: 11.33333%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/person-dialog/person-dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PersonDialogComponent = (function () {
    function PersonDialogComponent(publicService) {
        this.publicService = publicService;
        this.dataEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.displayEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    PersonDialogComponent.prototype.ngOnInit = function () {
        this.display = true;
        // 查询组织树
        this.queryOrgTree('');
        // 查询人员表格数据
        this.queryPersonList('');
        if (window.innerWidth < 1440) {
            this.width = window.innerWidth * 0.6;
        }
        else {
            this.width = window.innerWidth * 0.6;
        }
    };
    // 组织树懒加载
    PersonDialogComponent.prototype.nodeExpand = function (event) {
        if (event.node) {
            this.publicService.getOrgTree(event.node.oid).subscribe(function (data) {
                console.log(data);
                event.node.children = data;
            });
        }
    };
    // 组织树选中
    PersonDialogComponent.prototype.NodeSelect = function (event) {
        if (parseInt(event.node.dep) >= 1) {
            this.queryPersonList(event.node.oid);
        }
    };
    // 查询组织树数据
    PersonDialogComponent.prototype.queryOrgTree = function (oid) {
        var _this = this;
        this.publicService.getOrgTree(oid).subscribe(function (data) {
            // console.log(data);
            _this.orgs = data;
        });
    };
    // 查询人员表格数据
    PersonDialogComponent.prototype.queryPersonList = function (oid) {
        var _this = this;
        this.publicService.getPersonList(oid).subscribe(function (data) {
            // console.log(data);
            _this.tableDatas = data;
        });
    };
    PersonDialogComponent.prototype.handleRowSelect = function (event) {
        // console.log(event);
        this.eventData = event.data;
        console.log(this.eventData);
    };
    PersonDialogComponent.prototype.sure = function () {
        this.dataEmitter.emit(this.eventData);
        this.displayEmitter.emit(false);
    };
    PersonDialogComponent.prototype.cancel = function () {
        this.displayEmitter.emit(false);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PersonDialogComponent.prototype, "dataEmitter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PersonDialogComponent.prototype, "displayEmitter", void 0);
    PersonDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-person-dialog',
            template: __webpack_require__("../../../../../src/app/report/person-dialog/person-dialog.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/person-dialog/person-dialog.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_public_service__["a" /* PublicService */]])
    ], PersonDialogComponent);
    return PersonDialogComponent;
}());



/***/ }),

/***/ "../../../../../src/app/report/report-add-equipment/report-add-equipment.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog [(visible)]=\"display\" modal=\"modal\"  [responsive]=\"true\" [width]=\"width\" (onHide)=\"closeEquementMask(false)\">\n  <p-header>\n    添加设备\n  </p-header>\n  <form>\n      <div class=\"ui-g\">\n        <div class=\"ui-g-8 ui-sm-12\">\n          <label class=\"ui-g-2 ui-sm-5  text-right\">设备编号:</label>\n          <div class=\"ui-g-4 ui-sm-7 ui-fluid  ui-no-padding-right-15px \">\n            <input  pInputText type=\"text\" name=\"ano\" placeholder=\"设备编号\"  [(ngModel)]=\"queryModel.ano\" />\n          </div>\n          <label class=\"ui-g-2 ui-sm-5 text-right\">设备名称:</label>\n          <div class=\"ui-g-4  ui-sm-7 ui-fluid\">\n            <input pInputText type=\"text\" name=\"name\" placeholder=\"设备名称\" [(ngModel)]=\"queryModel.name\" />\n          </div>\n        </div>\n        <div class=\"ui-g-3 ui-sm-12\">\n          <div class=\"ui-g-12 ui-sm-12 option\">\n            <button pButton type=\"button\" label=\"查询\" ngClass=\"ui-sm-12\" (click)=\"queryByKeyWords()\" ></button>\n            <button pButton label=\"清空\" ngClass=\"ui-sm-12\" (click)=\"clearSearch()\"></button>\n          </div>\n        </div>\n      </div>\n\n  </form>\n  <div class=\"ui-g\">\n    <div class=\"ui-g-12\">\n      <div class=\"file-box reportaddequement \">\n        <p-dataTable emptyMessage=\"没有数据\" [value]=\"assets\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n                     [totalRecords]=\"totalRecords\" [(selection)]=\"selectMaterial\" (onLazyLoad)=\"loadCarsLazy($event)\" [stacked]=\"stacked\" [responsive]=\"true\" [scrollable]=\"true\" scrollHeight=\"500px\" >\n          <p-column [style]=\"{'width':'38px'}\" selectionMode=\"multiple\"></p-column>\n          <p-column  *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\"></p-column>\n        </p-dataTable>\n      </div>\n    </div>\n  </div>\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" label=\"确定\"  (click)=\"formSubmit(false)\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\"  label=\"取消\" (click)=\"closeEquementMask(false)\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/report/report-add-equipment/report-add-equipment.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (min-width: 1024px) and (max-width: 1920px) {\n  .reportaddequement /deep/ table thead tr th:last-child {\n    width: 10%; }\n  .reportaddequement /deep/ table thead tr th:nth-child(2) {\n    width: 20%; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/report-add-equipment/report-add-equipment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportAddEquipmentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportAddEquipmentComponent = (function () {
    function ReportAddEquipmentComponent(reportService, comfirmationService) {
        this.reportService = reportService;
        this.comfirmationService = comfirmationService;
        this.display = false;
        this.closereportEquement = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"];
        this.addDev = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.selectMaterial = [];
    }
    ReportAddEquipmentComponent.prototype.ngOnInit = function () {
        this.queryModel = {
            "ano": "",
            "father": "",
            "card_position": "",
            "asset_no": "",
            "name": "",
            "brand": "",
            "modle": "",
            "function": "",
            "system": "",
            "status": "",
            "on_line_date_start": "",
            "on_line_date_end": "",
            "room": "",
            "cabinet": "",
            "location": "",
            "belong_dapart": "",
            "belong_dapart_manager": " ",
            "manage_dapart": "",
            "manager": "",
            "ip": "",
            "maintain_vender": ""
        };
        this.display = true;
        if (window.innerWidth < 1440) {
            this.width = window.innerWidth * 0.6;
        }
        else {
            this.width = window.innerWidth * 0.6;
        }
        this.cols = [
            { field: 'ano', header: '设备编号' },
            { field: 'name', header: '设备名称' },
            { field: 'asset_no', header: '设备型号' },
            { field: 'asset_no', header: '设备位置' },
            { field: 'asset_no', header: '责任人' },
            { field: 'status', header: '设备状态' },
        ];
        this.queryAssets();
    };
    ReportAddEquipmentComponent.prototype.closeEquementMask = function (bool) {
        this.closereportEquement.emit(bool);
    };
    ReportAddEquipmentComponent.prototype.queryAssets = function () {
        var _this = this;
        this.reportService.getAssets().subscribe(function (asset) {
            _this.dataSource = asset;
            if (_this.dataSource) {
                _this.totalRecords = _this.dataSource.length;
                _this.assets = _this.dataSource.slice(0, 10);
            }
            else {
                _this.totalRecords = 0;
                _this.assets = [];
            }
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，请联系管理员或稍后重试';
            }
            else {
                message = err;
            }
            _this.comfirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ReportAddEquipmentComponent.prototype.queryByKeyWords = function () {
        var _this = this;
        for (var key in this.queryModel) {
            if (!this.queryModel[key]) {
                this.queryModel[key] = '';
            }
            this.queryModel[key] = this.queryModel[key].trim();
        }
        this.reportService.queryByKeyWords(this.queryModel).subscribe(function (data) {
            _this.dataSource = data;
            if (_this.dataSource) {
                _this.totalRecords = _this.dataSource.length;
                _this.assets = _this.dataSource.slice(0, 10);
            }
            else {
                _this.totalRecords = 0;
                _this.assets = [];
            }
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，请联系管理员或稍后重试';
            }
            else {
                message = err;
            }
            _this.comfirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ReportAddEquipmentComponent.prototype.loadCarsLazy = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.dataSource) {
                _this.assets = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 250);
    };
    ReportAddEquipmentComponent.prototype.clearSearch = function () {
        this.queryModel.ano = '';
        this.queryModel.name = '';
    };
    ReportAddEquipmentComponent.prototype.formSubmit = function (bool) {
        this.addDev.emit(this.selectMaterial);
        // this.closeEquementMask(false);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ReportAddEquipmentComponent.prototype, "closereportEquement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ReportAddEquipmentComponent.prototype, "addDev", void 0);
    ReportAddEquipmentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-report-add-equipment',
            template: __webpack_require__("../../../../../src/app/report/report-add-equipment/report-add-equipment.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/report-add-equipment/report-add-equipment.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__report_service__["a" /* ReportService */],
            __WEBPACK_IMPORTED_MODULE_1_primeng_primeng__["ConfirmationService"]])
    ], ReportAddEquipmentComponent);
    return ReportAddEquipmentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/report/report-handle-view/report-handle-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n.datastatistice-container{\r\n  position: relative;\r\n  width: 100%;\r\n  /*height: 100%;*/\r\n  height: 485px;\r\n  background: #ffffff;\r\n}\r\n.reports-container{\r\n  width: 100%;\r\n  /*height: 125%;*/\r\n  height: 100%;\r\n  position: relative;\r\n  top:-20px;\r\n  z-index: 9;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/report-handle-view/report-handle-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"datastatistice-container\">\n  <!--<div [ngSwitch]=\"titleCase\">-->\n  <!--<div class=\"bj-container\" *ngSwitchCase=1>-->\n  <!--<p-dropdown class=\"text-center\" [options]=\"chooseType\" [style]=\"{'width':'120px'}\" [(ngModel)]=\"selectedType\" (onChange)=\"onTypeChange()\">-->\n  <!--</p-dropdown>-->\n  <!--<p-dropdown class=\"text-center\" [style]=\"{'marginLeft':'5px','width':'60px'}\" [options]=\"chooseWidth\"-->\n  <!--[(ngModel)]=\"selectedWidth\" (onChange)=\"onWidthChange()\">-->\n  <!--</p-dropdown>-->\n  <!--<button pButton type=\"button\" class=\"ui-button-danger btn-confirm\"  label=\"删除\" (click)=\"delMe()\"></button>-->\n  <!--&lt;!&ndash;<button pButton type=\"button\" class=\"ui-button-secondary-info btn-confirm\"  label=\"确认\" style=\"margin-right: 10px\" (click)=\"confirmReports()\"></button>&ndash;&gt;-->\n\n  <!--</div>-->\n  <!--<div class=\"bj-container\" style=\"background-color: transparent\" *ngIf=\"editReportsIsShow\">-->\n  <!--<button pButton type=\"button\" class=\"ui-button-warning btn-confirm\"  label=\"编辑\"  (click)=\"editReports()\" ></button>-->\n  <!--</div>-->\n  <!--<div *ngSwitchDefault></div>-->\n  <!--</div>-->\n  <div class=\"reports-container\" #asset>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/report/report-handle-view/report-handle-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportHandleViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_echarts__ = __webpack_require__("../../../../echarts/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_echarts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_echarts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__util_color_util__ = __webpack_require__("../../../../../src/app/util/color.util.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_IntervalObservable__ = __webpack_require__("../../../../rxjs/_esm5/observable/IntervalObservable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ReportHandleViewComponent = (function () {
    function ReportHandleViewComponent(reportService) {
        var _this = this;
        this.reportService = reportService;
        this.reportName = [];
        this.reportCount = [];
        this.opt = {
            color: __WEBPACK_IMPORTED_MODULE_2__util_color_util__["a" /* default */].baseColor,
            title: {
                text: '今日报障总览',
                top: 20,
                x: 'center',
                textStyle: {
                    color: '#333333',
                    fontStyle: 'normal',
                    fontWeight: "bold",
                    // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
                    fontSize: 14 //主题文字z字体大小，默认为18px
                }
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    // type: 'cross'
                    type: 'shadow'
                }
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
                    axisTick: {
                        alignWithLabel: true
                    }
                }
            ],
            yAxis: [{
                    // name: '/万元',
                    // min: 0,
                    // max: 10000,
                    // splitNumber: 10,
                    // axisLabel: {
                    //   formatter: function (value) {
                    //     return (value / 10000);
                    //   }
                    // },
                    type: 'value'
                }],
            series: [{
                    name: '数量',
                    type: 'bar',
                    barCategoryGap: '50%',
                    itemStyle: {
                        normal: {
                            color: function (params) {
                                var color = __WEBPACK_IMPORTED_MODULE_2__util_color_util__["a" /* default */].genColor(_this.opt.series[0].data);
                                return color[params.dataIndex];
                            }
                        }
                    },
                    data: [100, 200, 300, 400, 500, 600, 100, 200, 300, 400, 500, 600]
                }]
        };
    }
    ReportHandleViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.echart = __WEBPACK_IMPORTED_MODULE_1_echarts__["init"](this.asset.nativeElement);
        this.echart.setOption(this.opt);
        this.reportService.getReportView().subscribe(function (data) {
            _this.report = data.handle_count;
            if (!_this.report) {
                _this.report = [];
            }
            for (var i = 0; i < _this.report.length; i++) {
                _this.reportName.push(_this.report[i].name);
                _this.reportCount.push(_this.report[i].count);
            }
            _this.opt.series[0]['data'] = _this.reportCount;
            _this.opt.xAxis[0]['data'] = _this.reportName;
            _this.echart.setOption(_this.opt);
            _this.intervalObser = __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_IntervalObservable__["a" /* IntervalObservable */].create(100).subscribe(function (index) {
                _this.initWidth();
            });
        });
    };
    ReportHandleViewComponent.prototype.initWidth = function () {
        this.echart.resize();
        this.intervalObser.unsubscribe();
    };
    ReportHandleViewComponent.prototype.ngOnDestroy = function () {
        this.echart.dispose(this.asset.nativeElement);
        this.echart = null;
    };
    ReportHandleViewComponent.prototype.onWindowResize = function (event) {
        this.echart.resize();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('asset'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], ReportHandleViewComponent.prototype, "asset", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ReportHandleViewComponent.prototype, "onWindowResize", null);
    ReportHandleViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-report-handle-view',
            template: __webpack_require__("../../../../../src/app/report/report-handle-view/report-handle-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/report-handle-view/report-handle-view.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__report_service__["a" /* ReportService */]])
    ], ReportHandleViewComponent);
    return ReportHandleViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/report/report-location/report-location.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"请选故障位置\" [(visible)]=\"display\" modal=\"modal\" [width]=\"width\" [responsive]=\"true\" (onHide)=\"closeLocationMask(false)\">\n  <div class=\"ui-g content\">\n    <p-tree [value]=\"treeDatas\"\n            selectionMode=\"checkbox\"\n            [(selection)]=\"selected\"\n            (onNodeSelect)=\"onNodeSelect($event)\"\n    ></p-tree>\n  </div>\n\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" (click)=\"formSubmit()\" label=\"确定\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeLocationMask(false)\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n"

/***/ }),

/***/ "../../../../../src/app/report/report-location/report-location.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin-bottom-1vw {\n  margin-bottom: 1vw; }\n\n.padding-tblr {\n  padding: .25em .5em; }\n\n.start_red {\n  color: red; }\n\n.birthday {\n  display: inline-block; }\n\n@media screen and (max-width: 1366px) {\n  .ui-grid-col-1 {\n    width: 11.33333%; } }\n\n.content {\n  height: 200px;\n  overflow: auto; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/report-location/report-location.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportLocationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__capacity_cabinet_model__ = __webpack_require__("../../../../../src/app/capacity/cabinet.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReportLocationComponent = (function () {
    function ReportLocationComponent(publicService, reportService) {
        this.publicService = publicService;
        this.reportService = reportService;
        this.treeDatas = [];
        // 表格数据
        this.closeLocation = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.addTree = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ReportLocationComponent.prototype.ngOnInit = function () {
        if (window.innerWidth < 1440) {
            this.width = window.innerWidth * 0.3;
        }
        else {
            this.width = window.innerWidth * 0.3;
        }
        this.cabinetObj = new __WEBPACK_IMPORTED_MODULE_2__capacity_cabinet_model__["a" /* CabinetModel */]();
        this.display = true;
        // 查询组织树
        this.queryModel = {
            'father_did': '',
            'sp_type_min': ''
        };
        if (this.treeState === 'update') {
            this.pnAnddataSource = this.reportService.deepClone(this.pnAnddataSource);
            this.submitData = {
                'cid': this.pnAnddataSource['cid'],
                "devices": this.pnAnddataSource['data']
            };
        }
        this.initTreeDatas();
    };
    ReportLocationComponent.prototype.handleRowSelect = function (event) {
        this.eventData = event.data;
    };
    ReportLocationComponent.prototype.closeLocationMask = function (bool) {
        this.closeLocation.emit(bool);
    };
    ReportLocationComponent.prototype.initTreeDatas = function () {
        var _this = this;
        this.publicService.getCapBasalDatas('', this.cabinetObj.sp_type).subscribe(function (res) {
            _this.treeDatas = res;
        });
    };
    ReportLocationComponent.prototype.formSubmit = function () {
        var arr = [];
        for (var _i = 0, _a = this.selected; _i < _a.length; _i++) {
            var key = _a[_i];
            var obj = {};
            obj['label'] = key['label'];
            obj['did'] = key['did'];
            arr.push(obj);
        }
        this.addTree.emit(arr);
    };
    ReportLocationComponent.prototype.onNodeSelect = function (event) {
        console.log(event);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('expandingTree'),
        __metadata("design:type", Array)
    ], ReportLocationComponent.prototype, "selected", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ReportLocationComponent.prototype, "closeLocation", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ReportLocationComponent.prototype, "addTree", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ReportLocationComponent.prototype, "treeState", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ReportLocationComponent.prototype, "pnAnddataSource", void 0);
    ReportLocationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-report-location',
            template: __webpack_require__("../../../../../src/app/report/report-location/report-location.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/report-location/report-location.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_public_service__["a" /* PublicService */],
            __WEBPACK_IMPORTED_MODULE_3__report_service__["a" /* ReportService */]])
    ], ReportLocationComponent);
    return ReportLocationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/report/report-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__report_start_report_start_component__ = __webpack_require__("../../../../../src/app/report/report-start/report-start.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__report_view_report_view_component__ = __webpack_require__("../../../../../src/app/report/report-view/report-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__history_report_history_report_component__ = __webpack_require__("../../../../../src/app/report/history-report/history-report.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__view_detail_view_detail_component__ = __webpack_require__("../../../../../src/app/report/view-detail/view-detail.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var route = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_3__report_view_report_view_component__["a" /* ReportViewComponent */] },
    { path: 'review', component: __WEBPACK_IMPORTED_MODULE_2__report_start_report_start_component__["a" /* ReportStartComponent */] },
    { path: 'history', component: __WEBPACK_IMPORTED_MODULE_4__history_report_history_report_component__["a" /* HistoryReportComponent */] },
    { path: 'ViewDetail', component: __WEBPACK_IMPORTED_MODULE_5__view_detail_view_detail_component__["a" /* ViewDetailComponent */] },
];
var ReportRoutingModule = (function () {
    function ReportRoutingModule() {
    }
    ReportRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild(route),
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]]
        })
    ], ReportRoutingModule);
    return ReportRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/report/report-start/report-start.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-section introduction \">\n  <div>\n    <span class=\"feature-title\">报障管理<span class=\"gt\">&gt;</span>{{title}}  </span>\n  </div>\n</div>\n<div class=\"content-section implementation GridDemo report\">\n  <p-panel>\n    <p-header>\n      <div class=\"ui-helper-clearfix\">\n        <button  pButton type=\"button\"  label=\"返回\" (click)=\"goBack()\" [ngStyle]=\"{'width':'100px'}\" ></button>\n        <button  pButton type=\"submit\" [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"保存\" (click)=\"formSave()\"  ></button>\n        <button  pButton type=\"submit\" [ngStyle]=\"{'float':'right','width':'100px'}\" label=\"提交\" [disabled]=\"!reportForm.valid\" (click)=\"formSubmit()\" ></button>\n      </div>\n    </p-header>\n    <form class=\"form-horizontal\" [formGroup]=\"reportForm\" *ngIf=\"viewState ==='apply'\"  >\n    <div class=\"form-group ui-fluid\" >\n      <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\"><span></span>单号：</label>\n      <div class=\"col-sm-4\">\n        <input formControlName=\"cid\" pInputText type=\"text\"  name=\"cid\"   placeholder=\"单号自动生成\"  class=\"no-border\" readonly/>\n      </div>\n      <label  class=\"col-sm-3 control-label ui-no-padding-right-15px\"><span>*</span>故障所属：</label>\n      <div class=\"col-sm-4\">\n        <p-autoComplete formControlName=\"fault_type\" name=\"fault_type\" [(ngModel)]=\"submitReportData.fault_type\" [suggestions]=\"brandoptions\" (completeMethod)=\"searchSuggest($event,'fault_type')\" [size]=\"30\"\n                        [minLength]=\"1\" placeholder=\"\" [dropdown]=\"true\"  >\n          <ng-template let-brand pTemplate=\"item\">\n            <div class=\"ui-helper-clearfix\" style=\"border-bottom:1px solid #D5D5D5\">\n              <div style=\"font-size:18px;float:right;margin:10px 10px 0 0\">{{brand}}</div>\n            </div>\n          </ng-template>\n        </p-autoComplete>\n        <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['fault_type'].valid&&(!reportForm.controls['fault_type'].untouched)\" >\n          <i class=\"fa fa-close\"></i>\n          故障所属必填\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group ui-fluid\" >\n\n      <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\"><span>*</span>报障时间：</label>\n      <div class=\"col-sm-4 \">\n        <input formControlName=\"time\" pInputText type=\"text\"  name=\"people\" [(ngModel)]=\"submitReportData.time\"    class=\"no-border\" readonly/>\n        <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['time'].valid&&(!reportForm.controls['time'].untouched)\" >\n          <i class=\"fa fa-close\"></i>\n          报障时间必填\n        </div>\n      </div>\n      <label  class=\"col-sm-3 control-label ui-no-padding-right-15px\"><span>*</span>报障人：</label>\n      <div class=\"col-sm-2\">\n        <input formControlName=\"people\" pInputText type=\"text\"  name=\"people\" [(ngModel)]=\"submitReportData.people\"  placeholder=\"报障人\"  class=\"no-border\" readonly/>\n        <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['people'].valid&&(!reportForm.controls['people'].untouched)\" >\n          <i class=\"fa fa-close\"></i>\n          报障人必填\n        </div>\n      </div>\n      <div class=\"col-sm-1\">\n        <button pButton  type=\"button\" (click)=\"showPersonMask()\" label=\"选择\"></button>\n      </div>\n      <div class=\"col-sm-1\">\n        <button pButton  type=\"button\" (click)=\"clearpeopleDialog()\" label=\"清空\"></button>\n      </div>\n    </div>\n\n    <div class=\"form-group ui-fluid\" >\n      <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\">故障位置：</label>\n      <div class=\"col-sm-9\">\n        <input pInputText type=\"text\" pInputText  formControlName=\"location\"  placeholder=\"故障位置\" name=\"location\" [(ngModel)]=\"submitReportData.location\" readonly/>\n      </div>\n      <div class=\"col-sm-1\">\n        <button pButton  type=\"button\" (click)=\"showreportAddLocationMask()\" label=\"选择\"></button>\n      </div>\n      <div class=\"col-sm-1\">\n        <button pButton  type=\"button\" (click)=\"clearLocationDialog()\" label=\"清空\"></button>\n      </div>\n    </div>\n    <div class=\"form-group ui-fluid\" >\n      <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\"><span>*</span>故障标题：</label>\n      <div class=\"col-sm-11\">\n        <input formControlName=\"title\" pInputText type=\"text\"  name=\"title\"   placeholder=\"故障标题\"  class=\"no-border\"  [(ngModel)]=\"submitReportData.title\" />\n        <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['title'].valid&&(!reportForm.controls['title'].untouched)\" >\n          <i class=\"fa fa-close\"></i>\n          保障标题必填\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group ui-fluid\" >\n      <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\"><span>*</span>故障描述：</label>\n      <div class=\"col-sm-11\">\n        <textarea pInputTextarea type=\"text\"  formControlName=\"content\" name=\"content\" [(ngModel)]=\"submitReportData.content\"></textarea>\n        <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['content'].valid&&(!reportForm.controls['content'].untouched)\" >\n          <i class=\"fa fa-close\"></i>\n          故障描述必填\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group ui-fluid\" >\n      <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\">关联设备：</label>\n      <div class=\"col-sm-9\">\n        <input formControlName=\"asset\" pInputText type=\"text\"  name=\"people\" [(ngModel)]=\"submitReportData.devices\"  placeholder=\"关联设备\"  class=\"no-border\" readonly/>\n      </div>\n      <div class=\"col-sm-1\">\n        <button pButton  type=\"button\" (click)=\"showreportAddEquementMask()\" label=\"选择\"></button>\n      </div>\n      <div class=\"col-sm-1\">\n        <button pButton  type=\"button\" (click)=\"clearAssetDialog()\" label=\"清空\"></button>\n      </div>\n    </div>\n    <div class=\"form-group ui-fluid\" >\n      <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\">上传图片：</label>\n\n      <div class=\"col-sm-11\">\n        <p-fileUpload  name=\"file\" url=\"{{uploadPhotoUrl}}\" (onUpload)=\"onBasicUploadAuto($event)\" chooseLabel=\"选择\" uploadLabel=\"上传\" cancelLabel=\"取消\" multiple=\"multiple\"></p-fileUpload>\n        <ng-template pTemplate=\"ui-button-text\">\n          <div>选择文件</div>\n        </ng-template>\n      </div>\n    </div>\n    </form>\n    <form class=\"form-horizontal\" [formGroup]=\"reportForm\" *ngIf=\"viewState ==='update'\"  >\n\n      <div class=\"form-group ui-fluid\" >\n        <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\"><span></span>单号：</label>\n        <div class=\"col-sm-4\">\n          <input formControlName=\"cid\" pInputText type=\"text\"    placeholder=\"单号自动生成\"  class=\"no-border\" readonly value=\"{{reportData?.cid}}\" />\n        </div>\n        <label  class=\"col-sm-3 control-label ui-no-padding-right-15px\"><span>*</span>故障所属：</label>\n        <div class=\"col-sm-4\">\n          <p-autoComplete formControlName=\"fault_type\" name=\"fault_type\" [(ngModel)]=\"reportData.fault_type\" [suggestions]=\"brandoptions\" (completeMethod)=\"searchSuggest($event,'fault_type')\" [size]=\"30\"\n                          [minLength]=\"1\" placeholder=\"\" [dropdown]=\"true\"  >\n            <ng-template let-brand pTemplate=\"item\">\n              <div class=\"ui-helper-clearfix\" style=\"border-bottom:1px solid #D5D5D5\">\n                <div style=\"font-size:18px;float:right;margin:10px 10px 0 0\">{{brand}}</div>\n              </div>\n            </ng-template>\n          </p-autoComplete>\n          <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['fault_type'].valid&&(!reportForm.controls['fault_type'].untouched)\" >\n            <i class=\"fa fa-close\"></i>\n            故障所属必填\n          </div>\n        </div>\n      </div>\n      <div class=\"form-group ui-fluid\" >\n\n        <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\"><span>*</span>报障时间：</label>\n        <div class=\"col-sm-4 \">\n          <input formControlName=\"time\" pInputText type=\"text\"  name=\"people\" [(ngModel)]=\"reportData.time\"    class=\"no-border\" readonly/>\n          <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['time'].valid&&(!reportForm.controls['time'].untouched)\" >\n            <i class=\"fa fa-close\"></i>\n            报障时间必填\n          </div>\n        </div>\n        <label  class=\"col-sm-3 control-label ui-no-padding-right-15px\"><span>*</span>报障人：</label>\n        <div class=\"col-sm-2\">\n          <input formControlName=\"people\" pInputText type=\"text\"  name=\"people\" [(ngModel)]=\"reportData.people\"  placeholder=\"报障人\"  class=\"no-border\" readonly/>\n          <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['people'].valid&&(!reportForm.controls['people'].untouched)\" >\n            <i class=\"fa fa-close\"></i>\n            报障人必填\n          </div>\n\n        </div>\n        <div class=\"col-sm-1\">\n          <button pButton  type=\"button\" (click)=\"showPersonMask()\" label=\"选择\"></button>\n        </div>\n        <div class=\"col-sm-1\">\n          <button pButton name=\"people\"  type=\"button\" (click)=\"clearpeopleDialog()\" label=\"清空\"></button>\n        </div>\n      </div>\n\n      <div class=\"form-group ui-fluid\" >\n        <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\">故障位置：</label>\n        <div class=\"col-sm-9\">\n          <input pInputText type=\"text\" pInputText  formControlName=\"location\"  placeholder=\"故障位置\" name=\"location\" [(ngModel)]=\"reportData.location\" readonly/>\n        </div>\n        <div class=\"col-sm-1\">\n          <button pButton  type=\"button\" (click)=\"showreportAddLocationMask()\" label=\"选择\"></button>\n        </div>\n        <div class=\"col-sm-1\">\n          <button pButton  name=\"location\" type=\"button\" (click)=\"clearLocationDialog()\" label=\"清空\"></button>\n        </div>\n      </div>\n      <div class=\"form-group ui-fluid\" >\n        <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\"><span>*</span>故障标题：</label>\n        <div class=\"col-sm-11\">\n          <input formControlName=\"title\" pInputText type=\"text\"  name=\"title\"   placeholder=\"故障标题\"  class=\"no-border\"  [(ngModel)]=\"reportData.title\" />\n          <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['title'].valid&&(!reportForm.controls['title'].untouched)\" >\n            <i class=\"fa fa-close\"></i>\n            保障标题必填\n          </div>\n        </div>\n      </div>\n      <div class=\"form-group ui-fluid\" >\n        <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\"><span>*</span>故障描述：</label>\n        <div class=\"col-sm-11\">\n          <textarea pInputTextarea type=\"text\"  formControlName=\"content\" name=\"content\" [(ngModel)]=\"reportData.content\"></textarea>\n          <div class=\"ui-message ui-messages-error ui-corner-all\"  *ngIf=\"!reportForm.controls['content'].valid&&(!reportForm.controls['content'].untouched)\" >\n            <i class=\"fa fa-close\"></i>\n            故障描述必填\n          </div>\n        </div>\n      </div>\n      <div class=\"form-group ui-fluid\" >\n        <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\">关联设备：</label>\n        <div class=\"col-sm-9\">\n          <input formControlName=\"asset\" pInputText type=\"text\"  name=\"people\" [(ngModel)]=\"reportData.devices\"  placeholder=\"关联设备\"  class=\"no-border\" readonly/>\n        </div>\n        <div class=\"col-sm-1\">\n          <button pButton  type=\"button\" (click)=\"showreportAddEquementMask()\" label=\"选择\"></button>\n        </div>\n        <div class=\"col-sm-1\">\n          <button pButton  type=\"button\" (click)=\"clearAssetDialog()\" label=\"清空\"></button>\n        </div>\n      </div>\n      <div class=\"form-group ui-fluid\" >\n        <label  class=\"col-sm-1 control-label ui-no-padding-right-15px\">上传图片：</label>\n\n        <div class=\"col-sm-11\">\n          <p-fileUpload  name=\"file\" url=\"{{uploadPhotoUrl}}\" files=\"{{files}}\" (onUpload)=\"onBasicUploadAuto($event)\" chooseLabel=\"选择\" uploadLabel=\"上传\" cancelLabel=\"取消\" multiple=\"multiple\" ></p-fileUpload>\n          <ng-template pTemplate=\"ui-button-text\">\n            <div>选择文件</div>\n          </ng-template>\n        </div>\n      </div>\n      </form>\n    </p-panel>\n</div>\n<app-person-dialog\n  *ngIf=\"displayPersonel\"\n(dataEmitter)=\"dataEmitter($event)\"\n(displayEmitter)=\"displayEmitter($event)\">\n\n</app-person-dialog>\n<app-report-add-equipment\n  *ngIf=\"showreportAddEquement\"\n  (closereportEquement)=\"closeAddEquementMask($event)\"\n  (addDev)=\"addDev($event)\"\n></app-report-add-equipment>\n<app-report-location\n  *ngIf=\"displayLocation\"\n  (closeLocation)=\"closeAddLocationMask($event)\"\n  (addTree)=\"addConstructionOrg($event)\"\n></app-report-location>\n"

/***/ }),

/***/ "../../../../../src/app/report/report-start/report-start.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "th {\n  text-align: center;\n  font-size: 36px; }\n\ntd {\n  text-align: center; }\n\nbutton-text-icon-left .fa, .ui-button-text-icon-right .fa {\n  position: absolute;\n  top: 50%;\n  margin-top: -.5em;\n  left: -2px; }\n\n.type {\n  color: red;\n  font-size: 20px; }\n\n.types {\n  color: white;\n  font-size: 20px; }\n\n.start_red {\n  color: red; }\n\n.report /deep/ .ui-fluid .ui-button-text-icon-left .ui-button-text {\n  padding-left: 2.1em; }\n\n.report /deep/ .ui-button-icon-only .ui-button-text {\n  padding: 0.2em; }\n\n.report input {\n  border-bottom: 1px solid #D6D6D6; }\n\n/deep/.ui-button-text-icon-left .fa {\n  z-index: 0; }\n\n/deep/.ui-fileupload-choose input[type=file] {\n  z-index: 1; }\n\n.feature-title {\n  font-size: 18px;\n  color: #333333; }\n\n/deep/.ui-widget-header {\n  background-color: white;\n  background: linear-gradient(to bottom, #ffffff 0%, #ffffff 100%); }\n\n/deep/.ui-button-info {\n  background-color: #39b9c6; }\n\n.report /deep/ .form-horizontal .control-label {\n  padding-top: 0px;\n  margin-bottom: 0;\n  text-align: right; }\n\n.report .ui-panel.ui-widget {\n  height: 700px;\n  /* overflow: auto; */ }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/report-start/report-start.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportStartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PHOTOURL = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].url.management + '/fault/fault_imgs_upload';
var ReportStartComponent = (function (_super) {
    __extends(ReportStartComponent, _super);
    function ReportStartComponent(reportService, router, route, confirmationService, messageService) {
        var _this = _super.call(this, confirmationService, messageService) || this;
        _this.reportService = reportService;
        _this.router = router;
        _this.route = route;
        _this.confirmationService = confirmationService;
        _this.messageService = messageService;
        _this.msgs = [];
        _this.fb = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]();
        _this.displayPersonel = false;
        _this.uploadPhotoUrl = PHOTOURL;
        _this.files = [];
        _this.showreportAddEquement = false;
        _this.displayLocation = false;
        _this.dataSource = [];
        _this.reportData = {};
        _this.editDataSource = [];
        _this.editInspections = [];
        _this.qureyModel = {
            field: '',
            value: ''
        };
        return _this;
    }
    ReportStartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.files = [1, 2];
        this.title = '保障发起';
        this.initStartTime = new Date();
        this.viewState = 'apply';
        //初始化验证
        this.reportStatus = ['未处理', '已处理'];
        this.submitReportData = {
            'people': this.brand,
            'fault_type': '',
            'status': '',
            'time': this.reportService.getFormatTime(),
            'location': '',
            'content': '',
            'img_url': '',
            'title': '',
            'submitdevices': ''
        };
        this.reportForm = this.fb.group({
            'cid': [''],
            'fault_type': ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            'time': ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            'people': ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            'location': [''],
            'title': ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            'content': ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            'asset': [''],
        });
        this.route.queryParams.subscribe(function (params) {
            if (params['cid'] && params['state'] && params['title']) {
                _this.title = params['title'];
                _this.viewState = params['state'];
                var queryModel = {
                    'cid': params['cid'],
                    'people': "",
                    'fault_type': "",
                    'status': "",
                    'time_start': "",
                    'time_end': "",
                };
                _this.reportService.getHistoryReport(queryModel).subscribe(function (data) {
                    _this.reportData = data[0];
                    console.log(_this.reportData);
                    _this.reportData['devices'] ? _this.reportData['devices'] : _this.reportData['devices'] = [];
                    var devices = [];
                    for (var i = 0; i < _this.reportData['devices'].length; i++) {
                        var temp = _this.reportData['devices'][i];
                        devices.push(temp['ano']);
                    }
                    _this.editDataSource = _this.reportData['devices'] ? _this.reportData['devices'] : [];
                    if (!_this.editDataSource) {
                        _this.editDataSource = [];
                        _this.editInspections = [];
                        _this.totalRecords = 0;
                    }
                    _this.submitReportData.submitdevices = _this.reportData['devices'];
                    _this.reportData['devices'] = devices.join(';');
                });
            }
        });
    };
    ReportStartComponent.prototype.onSubmitReport = function () {
        var _this = this;
        this.reportService.onSubmitReport(this.submitReportData).subscribe(function () {
            _this.router.navigate(['../history'], { relativeTo: _this.route });
        });
    };
    //修改提交
    ReportStartComponent.prototype.updateReportSubmit = function (submitData, reportData) {
        var _this = this;
        submitData['subdevices'] = reportData;
        submitData['status'] = this.reportStatus[0];
        this.reportService.updateReport(submitData).subscribe(function () {
            _this.router.navigate(['../history'], { queryParams: { title: '历史保障' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    //图片上传
    ReportStartComponent.prototype.onBasicUploadAuto = function (event) {
        var res = JSON.parse(event.xhr.responseText);
        if (res['errcode'] !== '00000') {
            this.toast(res['errmsg']);
            // this.msgs.push({severity: 'error', summary: '提示消息', detail: res['errmsg']});
            // this.message.push({severity: 'info', summary: 'File Uploaded', detail: res['errmsg']});
            // this.msgs = [];
            return;
        }
        var photoUrl = res['datas'];
        var imgurl = [];
        for (var i = 0; i < photoUrl.length; i++) {
            imgurl.push(photoUrl[i].path);
        }
        this.submitReportData.img_url = imgurl.join(';');
    };
    ReportStartComponent.prototype.searchSuggest = function (searchText, name) {
        var _this = this;
        this.qureyModel = {
            field: name,
            value: searchText.query
        };
        for (var key in this.qureyModel) {
            this.qureyModel[key] = this.qureyModel[key].trim();
        }
        this.reportService.searchChange(this.qureyModel).subscribe(function (datas) {
            switch (name) {
                case 'fault_type':
                    _this.brandoptions = datas;
                    console.log(_this.brandoptions);
                    break;
            }
        });
    };
    ReportStartComponent.prototype.displayEmitter = function (event) {
        this.displayPersonel = event;
    };
    ReportStartComponent.prototype.dataEmitter = function (event) {
        if (this.viewState === 'apply') {
            this.submitReportData.people = event.name;
        }
        else {
            this.reportData['people'] = event.name;
        }
    };
    ReportStartComponent.prototype.showPersonMask = function () {
        this.displayPersonel = !this.displayPersonel;
    };
    ReportStartComponent.prototype.clearpeopleDialog = function () {
        this.submitReportData.people = '';
        this.reportData['people'] = '';
    };
    ReportStartComponent.prototype.clearLocationDialog = function () {
        this.submitReportData.location = '';
        this.reportData['location'] = '';
    };
    ReportStartComponent.prototype.clearAssetDialog = function () {
        this.submitReportData.devices = '';
        this.reportData['devices'] = '';
    };
    ReportStartComponent.prototype.showreportAddEquementMask = function () {
        this.showreportAddEquement = !this.showreportAddEquement;
    };
    ReportStartComponent.prototype.closeAddEquementMask = function (bool) {
        this.showreportAddEquement = bool;
    };
    ReportStartComponent.prototype.addDev = function (metail) {
        this.showreportAddEquement = false;
        var devices = [];
        for (var i = 0; i < metail.length; i++) {
            var temp = metail[i];
            devices.push(temp['ano']);
        }
        this.submitReportData.submitdevices = metail;
        if (this.viewState === 'apply') {
            this.submitReportData.devices = devices.join(';');
        }
        else {
            this.reportData['devices'] = metail;
            this.reportData['devices'] = devices.join(';');
        }
    };
    ReportStartComponent.prototype.showreportAddLocationMask = function () {
        this.displayLocation = !this.displayLocation;
    };
    ReportStartComponent.prototype.closeAddLocationMask = function (bool) {
        this.displayLocation = bool;
    };
    ReportStartComponent.prototype.addConstructionOrg = function (org) {
        this.submitReportData.location = [];
        for (var i = 0; i < org.length; i++) {
            this.submitReportData.location.push(org[i]['label']);
        }
        if (this.viewState === 'apply') {
            this.submitReportData.location = this.submitReportData.location.join('>');
        }
        else {
            this.reportData['location'] = this.submitReportData['location'].join('>');
        }
        this.displayLocation = false;
    };
    //保存
    ReportStartComponent.prototype.addReportSave = function (submitData) {
        var _this = this;
        var devices = [];
        submitData.devices = devices;
        this.reportService.queryReportSave(this.submitReportData).subscribe(function () {
            _this.router.navigate(['../history'], { queryParams: { title: '历史保障' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    //修改保存
    ReportStartComponent.prototype.updateReportSave = function (submitData) {
        var _this = this;
        submitData['subdevices'] = this.submitReportData.submitdevices;
        this.reportService.updateReport(submitData).subscribe(function () {
            _this.router.navigate(['../history'], { queryParams: { title: '历史保障' }, relativeTo: _this.route });
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ;
    ReportStartComponent.prototype.formSave = function () {
        if (this.viewState === 'apply') {
            this.addReportSave(this.submitReportData);
        }
        else if (this.viewState === 'update') {
            this.updateReportSave(this.reportData);
        }
    };
    ReportStartComponent.prototype.formSubmit = function () {
        if (this.viewState === 'apply') {
            this.onSubmitReport();
        }
        else if (this.viewState === 'update') {
            this.updateReportSubmit(this.reportData, this.editDataSource);
        }
    };
    ReportStartComponent.prototype.goBack = function () {
        this.router.navigate(['../history'], { relativeTo: this.route });
    };
    ReportStartComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-report-start',
            template: __webpack_require__("../../../../../src/app/report/report-start/report-start.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/report-start/report-start.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__report_service__["a" /* ReportService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_6_primeng_components_common_messageservice__["MessageService"]])
    ], ReportStartComponent);
    return ReportStartComponent;
}(__WEBPACK_IMPORTED_MODULE_7__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/report/report-treatment/report-treatment.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog header=\"处理报障单\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\" width \"\n          (onHide)=\"closeReportDetailMask(false)\"  class=\"treat\">\n  <!--<p-panel header=\"报障单\">-->\n    <form  [formGroup]=\"reportTreatmentForm\" class=\"treatment\">\n      <div class=\"ui-grid ui-grid-responsive ui-grid-pad ui-fluid\" style=\"margin: 10px 0px\">\n        <div class=\"ui-grid-row\">\n          <div class=\"ui-grid-col-1 hidden text-right\">\n            故障状态:\n          </div>\n          <div class=\"ui-grid-col-11\">\n            <select name=\"status\" [(ngModel)]=\"treamentReportData.status\" hidden formControlName=\"status\">\n              <option *ngFor=\"let status of reportTreamentStatus\" value=\"{{status}}\">{{status}}</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"ui-grid-row\">\n          <div class=\"ui-grid-col-2 text-right\">\n            <span style=\"color: red\">*</span>故障处理内容:\n          </div>\n          <div class=\"ui-grid-col-11\">\n            <textarea pInputTextarea type=\"text\" name=\"content\" [(ngModel)]=\"treamentReportData.handle_content\"  formControlName=\"handle_content\"></textarea>\n          </div>\n        </div>\n      </div>\n    </form>\n  <!--</p-panel>-->\n  <p-footer>\n    <button type=\"button\" pButton icon=\"fa-check\" (click)=\"updateReportTreamentContent()\" label=\"确定\" [disabled]=\"!reportTreatmentForm.valid\"></button>\n    <button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeReportDetailMask(false)\" label=\"取消\"></button>\n  </p-footer>\n</p-dialog>\n\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/report/report-treatment/report-treatment.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".upload_container {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(194, 194, 179, 0.6); }\n  .upload_container .window {\n    position: absolute;\n    background: #FFFFFF;\n    width: 60.47vw;\n    height: 30.45vw;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%); }\n    .upload_container .window .import {\n      height: 2vw;\n      background: #c6cdd7; }\n      .upload_container .window .import p {\n        color: #323842;\n        font-size: 1.8rem;\n        font-weight: bold;\n        line-height: 2vw;\n        padding-left: 1vw; }\n      .upload_container .window .import span {\n        font-size: 3rem;\n        font-weight: lighter;\n        line-height: 2vw;\n        position: absolute;\n        top: 0;\n        right: 0;\n        width: 2vw;\n        height: 2vw;\n        cursor: pointer;\n        text-align: center;\n        color: #fff;\n        background: #3f89ec; }\n    .upload_container .window .file-box {\n      position: relative;\n      width: 340px;\n      top: 10px;\n      margin: 0 auto; }\n      .upload_container .window .file-box .form {\n        height: 44.47vw;\n        border: 1px solid #c6cdd7;\n        margin: 1vw;\n        padding-top: 6.77vw;\n        padding-left: 4.68vw; }\n      .upload_container .window .file-box .file-boxs {\n        position: relative;\n        width: 840px; }\n      .upload_container .window .file-box .txt {\n        height: 32px;\n        border: 1px solid #cdcdcd;\n        width: 300px; }\n      .upload_container .window .file-box .btn {\n        background-color: #FFF;\n        border: 1px solid #CDCDCD;\n        height: 33px;\n        width: 70px; }\n      .upload_container .window .file-box .file {\n        position: absolute;\n        top: 0;\n        right: 80px;\n        height: 24px;\n        opacity: 0;\n        width: 260px; }\n\n@media only screen and (max-width: 760px) {\n  .upload_container {\n    position: fixed;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background: rgba(194, 194, 179, 0.6); }\n    .upload_container .window {\n      overflow: auto;\n      position: absolute;\n      background: #ffffff;\n      width: 90vw;\n      height: 80.45vw;\n      top: 50%;\n      left: 50%;\n      -webkit-transform: translate(-50%, -50%);\n              transform: translate(-50%, -50%); }\n      .upload_container .window .import {\n        height: 4vw;\n        background: #c6cdd7; }\n        .upload_container .window .import p {\n          color: #323842;\n          font-size: 1rem;\n          font-weight: bold;\n          line-height: 4vw;\n          padding-left: 2vw; }\n        .upload_container .window .import span {\n          font-size: 3rem;\n          font-weight: lighter;\n          line-height: 3vw;\n          position: absolute;\n          top: 0;\n          right: 0;\n          width: 4vw;\n          height: 4vw;\n          cursor: pointer;\n          text-align: center;\n          color: #fff;\n          background: #3f89ec; }\n      .upload_container .window .file-box {\n        position: relative;\n        width: 340px;\n        top: 10px;\n        margin: 0 auto; }\n        .upload_container .window .file-box .form {\n          height: 44.47vw;\n          border: 1px solid #c6cdd7;\n          margin: 1vw;\n          padding-top: 6.77vw;\n          padding-left: 4.68vw; }\n        .upload_container .window .file-box .file-boxs {\n          position: relative;\n          width: 840px; }\n        .upload_container .window .file-box .txt {\n          height: 32px;\n          border: 1px solid #cdcdcd;\n          width: 300px; }\n        .upload_container .window .file-box .btn {\n          background-color: #FFF;\n          border: 1px solid #CDCDCD;\n          height: 33px;\n          width: 70px; }\n        .upload_container .window .file-box .file {\n          position: absolute;\n          top: 0;\n          right: 80px;\n          height: 24px;\n          opacity: 0;\n          width: 260px; } }\n\n/deep/ .treat .ui-panel.ui-widget {\n  height: 180px !important;\n  overflow: auto; }\n\n.treatment textarea {\n  min-height: 300px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/report-treatment/report-treatment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportTreatmentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReportTreatmentComponent = (function () {
    function ReportTreatmentComponent(reportService, storageService) {
        this.reportService = reportService;
        this.storageService = storageService;
        this.closeReportTreatmentMask = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.updateDev = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //修改处理内容
        this.fb = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]();
    }
    ReportTreatmentComponent.prototype.ngOnInit = function () {
        this.windowSize = window.innerWidth;
        if (this.windowSize < 1440) {
            this.width = this.windowSize * 0.4;
        }
        else {
            this.width = this.windowSize * 0.6;
        }
        this.display = true;
        this.reportTreamentStatus = ['未处理', '已处理'];
        this.treamentReportData = {
            'id': this.reportTreatment.cid,
            'status': this.reportTreamentStatus[1],
            'handle_content': '',
            'handle_people': this.storageService.getUserName('userName'),
            'handle_datetime': ''
        };
        this.reportTreatmentForm = this.fb.group({
            'status': '',
            'handle_content': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            'handle_people': [''],
            'handle_datetime': [''],
        });
    };
    ReportTreatmentComponent.prototype.updateReportTreamentContent = function () {
        var _this = this;
        this.treamentReportData.handle_datetime = this.reportService.getFormatTime();
        this.reportService.handleReport(this.treamentReportData).subscribe(function (data) {
            _this.updateDev.emit(_this.treamentReportData['handle_content']);
            _this.closeReportTreatmentMask.emit(false);
        }, function (err) {
        });
    };
    ReportTreatmentComponent.prototype.closeReportDetailMask = function (bool) {
        this.closeReportTreatmentMask.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ReportTreatmentComponent.prototype, "closeReportTreatmentMask", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ReportTreatmentComponent.prototype, "updateDev", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ReportTreatmentComponent.prototype, "reportTreatment", void 0);
    ReportTreatmentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-report-treatment',
            template: __webpack_require__("../../../../../src/app/report/report-treatment/report-treatment.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/report-treatment/report-treatment.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__report_service__["a" /* ReportService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */]])
    ], ReportTreatmentComponent);
    return ReportTreatmentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/report/report-view-echart/report-view-echart.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".datastatistice-container{\r\n   position: relative;\r\n   width: 100%;\r\n   /*height: 100%;*/\r\n   height:485px;\r\n  background: #ffffff;\r\n }\r\n.reports-container{\r\n  width: 100%;\r\n  /*height: 125%;*/\r\n  height: 100%;\r\n  position: relative;\r\n  top:-20px;\r\n  z-index: 9;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/report-view-echart/report-view-echart.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"datastatistice-container\">\n  <div class=\"reports-container\" #room>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/report/report-view-echart/report-view-echart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportViewEchartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_echarts__ = __webpack_require__("../../../../echarts/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_echarts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_echarts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__util_color_util__ = __webpack_require__("../../../../../src/app/util/color.util.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_IntervalObservable__ = __webpack_require__("../../../../rxjs/_esm5/observable/IntervalObservable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ReportViewEchartComponent = (function () {
    function ReportViewEchartComponent(reportService) {
        this.reportService = reportService;
        this.reportViewName = [];
        this.reportViewCount = [];
        this.reportViewColors = [];
        this.option = {
            title: {
                text: '今日报障总览',
                top: 20,
                x: 'center',
                textStyle: {
                    color: '#333333',
                    fontStyle: 'normal',
                    fontWeight: "bold",
                    // fontFamily:"san-serif",//主题文字字体，默认微软雅黑
                    fontSize: 14 //主题文字z字体大小，默认为18px
                }
            },
            color: __WEBPACK_IMPORTED_MODULE_2__util_color_util__["a" /* default */].baseColor,
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                type: 'scroll',
                orient: 'vertical',
                // right: 0,
                left: 0,
                top: 20,
                bottom: 20,
                data: []
            },
            series: [
                {
                    // name: '姓名',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '50%'],
                    data: [{
                            value: 3350,
                            name: '深圳'
                        },
                        {
                            value: 310,
                            name: '北京'
                        },
                        {
                            value: 234,
                            name: '广州'
                        },
                        {
                            value: 135,
                            name: '上海'
                        },
                        {
                            value: 1548,
                            name: '长沙'
                        }],
                    // data: [123,435,567,667,334],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
    }
    ReportViewEchartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.baseColors = [
            '#25859e',
            '#6acece',
            '#e78816',
            '#eabc7f',
            '#12619d',
            '#ad2532',
            '#15938d',
            '#cb4a1c',
            '#b3aa9b',
            '#042d4c',
        ];
        this.echart = __WEBPACK_IMPORTED_MODULE_1_echarts__["init"](this.room.nativeElement);
        this.echart.setOption(this.option);
        this.reportService.getReportView().subscribe(function (data) {
            _this.reportView = data.type_count;
            if (!_this.reportView) {
                _this.reportView = [];
            }
            var num = 0;
            for (var i = 0; i < _this.reportView.length; i++) {
                _this.reportViewName.push(_this.reportView[i].name);
                _this.reportViewCount.push(_this.reportView[i].count);
                if (num < _this.baseColors.length) {
                    _this.reportViewColors[i] = _this.baseColors[num++];
                }
                else {
                    num = 0;
                    _this.reportViewColors[i] = _this.baseColors[num++];
                }
            }
            _this.option.series[0]['data'] = _this.reportService.formatPieData(_this.reportView);
            _this.option.legend['data'] = _this.reportViewName;
            _this.option.color = _this.reportViewColors;
            _this.echart.setOption(_this.option);
            _this.intervalObser = __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_IntervalObservable__["a" /* IntervalObservable */].create(100).subscribe(function (index) {
                _this.initWidth();
            });
        });
    };
    ReportViewEchartComponent.prototype.initWidth = function () {
        this.echart.resize();
        this.intervalObser.unsubscribe();
    };
    ReportViewEchartComponent.prototype.ngOnDestroy = function () {
        this.echart.dispose(this.room.nativeElement);
        this.echart = null;
    };
    ReportViewEchartComponent.prototype.onWindowResize = function (event) {
        this.echart.resize();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('room'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], ReportViewEchartComponent.prototype, "room", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ReportViewEchartComponent.prototype, "onWindowResize", null);
    ReportViewEchartComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-report-view-echart',
            template: __webpack_require__("../../../../../src/app/report/report-view-echart/report-view-echart.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/report-view-echart/report-view-echart.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__report_service__["a" /* ReportService */]])
    ], ReportViewEchartComponent);
    return ReportViewEchartComponent;
}());



/***/ }),

/***/ "../../../../../src/app/report/report-view/report-view.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n<div class=\"content-section introduction \">\n  <div>\n    <span class=\"feature-title\">报障管理<span class=\"gt\">&gt;</span>报障总览  </span>\n  </div>\n</div>\n<div class=\"content-section implementation GridDemo reportview  \">\n\n      <div class=\"ui-g\">\n    <div class=\"ui-g-12 ui-g-nopad \">\n      <div class=\"ui-g \">\n        <div class=\"ui-g-12 ui-md-12 ui-lg-6\">\n          <div class=\"box box-solid\">\n            <div class=\"box-body\">\n              <div class=\"media\">\n                <app-report-handle-view></app-report-handle-view>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"ui-g-12 ui-md-12 ui-lg-6\">\n            <div class=\"box box-solid\">\n              <div class=\"box-body\">\n                <div class=\"media\">\n                  <app-report-view-echart></app-report-view-echart>\n                </div>\n              </div>\n            </div>\n        </div>\n\n      </div>\n    </div>\n  </div>\n\n\n  <div class=\"ui-g \">\n    <div class=\"ui-g-12 ui-md-12 ui-lg-12 \">\n        <p-dataTable emptyMessage=\"没有数据\" [value]=\"historyReportOrder\" [lazy]=\"true\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\"\n               [totalRecords]=\"totalRecords\" (onLazyLoad)=\"loadCarsLazy($event)\">\n    <!--<p-header>今日报障单</p-header>-->\n    <p-column  field=\"cid\" header=\"单号\" [sortable]=\"true\" >\n      <ng-template let-data=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n        <span (click)=\"onOperate(data)\" class=\"curser\">{{data.cid}}</span>\n      </ng-template>\n    </p-column>\n    <p-column *ngFor=\"let col of cols\" field=\"{{col.field}}\" header=\"{{col.header}}\" [sortable]=\"true\"></p-column>\n    <p-column header=\"操作\" >\n      <ng-template let-car=\"rowData\" pTemplate=\"body\" let-i=\"rowIndex\">\n        <button pButton type=\"button\" label=\"编辑\" (click)=\"updateOption(dataSource[i])\" *ngIf=\"dataSource[i]['status']==='新建'\"></button>\n        <button pButton type=\"button\" label=\"删除\" (click)=\"deleteReport(dataSource[i],i)\" *ngIf=\"dataSource[i]['status']==='新建'\"></button>\n        <button pButton type=\"button\" label=\"处理\" (click)=\"showReportTreatmentMask(dataSource[i],i)\"  *ngIf=\"dataSource[i]['status']==='未处理'\"></button>\n        <button pButton type=\"button\" label=\"查看\" (click)=\"showReport(dataSource[i])\"></button>\n      </ng-template>\n    </p-column>\n  </p-dataTable>\n    </div>\n  </div>\n</div>\n<!--查看详情model-->\n<!-- <app-view-detail\n  [reportDetail]=\"currentHistoryReport\"\n  (closeViewDetail)=\"closeViewDetail($event)\"\n  *ngIf=\"showViewDetailMask\"></app-view-detail>\n报障总览处理-->\n<!-- <app-report-treatment\n  [reportTreatment]=\"currentHistoryReport\"\n  (closeReportTreatmentMask)=\"closeReportTreamentMask($event)\"\n  *ngIf=\"showReportTreatment\" (updateDev)=\"updatehistoryReportModel($event)\"></app-report-treatment> -->\n"

/***/ }),

/***/ "../../../../../src/app/report/report-view/report-view.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".nf-chart {\n  width: 100%;\n  height: 400px;\n  float: left; }\n\n@media screen and (min-width: 1366px) {\n  .reportview /deep/ table thead tr th:last-child {\n    width: 15%; } }\n\n.curser {\n  color: #39b9c6;\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/report-view/report-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__ = __webpack_require__("../../../../primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_page__ = __webpack_require__("../../../../../src/app/base.page.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ReportViewComponent = (function (_super) {
    __extends(ReportViewComponent, _super);
    function ReportViewComponent(reportService, storageService, route, router, confirmationService, messageService) {
        var _this = _super.call(this, confirmationService, messageService) || this;
        _this.reportService = reportService;
        _this.storageService = storageService;
        _this.route = route;
        _this.router = router;
        _this.confirmationService = confirmationService;
        _this.messageService = messageService;
        _this.showViewDetailMask = false; //查看详情
        _this.bar = {}; //柱状图
        _this.pie = {}; //饼状图
        _this.report = []; //报障处理总览
        _this.reportView = []; //报障总览
        _this.reportName = [];
        _this.reportCount = [];
        _this.reportViewName = []; //报障总览名称
        _this.reportViewCount = []; //报障总览内容
        _this.historyReportOrder = []; //历史报障单
        _this.display = false; //模态框默认隐藏
        _this.showReportTreatment = false; //报障总览处理
        return _this;
    }
    ReportViewComponent.prototype.ngOnInit = function () {
        this.cols = [
            // {field: 'cid', header: '单号'},
            { field: 'fault_type', header: '故障所属' },
            { field: 'people', header: '报障人' },
            { field: 'time', header: '报障时间' },
            { field: 'location', header: '故障位置' },
            { field: 'status', header: '状态' },
            { field: 'handle_people', header: '处理人' },
            { field: 'handle_datetime', header: '完成时间' },
        ];
        this.queryReportView();
    };
    //查看详情
    ReportViewComponent.prototype.showReport = function (report) {
        this.currentHistoryReport = report;
        this.showViewDetailMask = !this.showViewDetailMask;
    };
    ReportViewComponent.prototype.closeViewDetail = function (bool) {
        this.showViewDetailMask = bool;
    };
    ReportViewComponent.prototype.onOperate = function (report) {
        this.currentHistoryReport = report;
        this.showViewDetailMask = !this.showViewDetailMask;
    };
    //故障处理model
    ReportViewComponent.prototype.showReportTreatmentMask = function (report) {
        this.storageService.setReportId('reportid', report.id);
        this.currentHistoryReport = report;
        this.showReportTreatment = !this.showReportTreatment;
    };
    ReportViewComponent.prototype.closeReportTreamentMask = function (bool) {
        this.showReportTreatment = bool;
    };
    ReportViewComponent.prototype.loadCarsLazy = function (event) {
        var _this = this;
        setTimeout(function () {
            if (_this.dataSource) {
                _this.historyReportOrder = _this.dataSource.slice(event.first, (event.first + event.rows));
            }
        }, 250);
    };
    ReportViewComponent.prototype.updateOption = function (current) {
        var cid = current['cid'];
        this.router.navigate(['../review'], { queryParams: { cid: cid, state: 'update', title: '编辑' }, relativeTo: this.route });
    };
    ReportViewComponent.prototype.updatehistoryReportModel = function (handle_content) {
        var reportId = parseInt(this.storageService.getReportId('reportid'));
        this.queryReportView();
        for (var i = 0; i < this.dataSource.length; i++) {
            if (this.dataSource[i]['id'] === reportId) {
                this.dataSource[i]['showHandleButton'] = false;
                this.dataSource[i]['status'] = '已处理';
                this.dataSource[i]['handle_content'] = handle_content;
            }
        }
    };
    ReportViewComponent.prototype.deleteReport = function (current) {
        var _this = this;
        var cid = current['cid'];
        //请求成功后删除本地数据
        this.confirm('确认删除吗?', function () { _this.deleteoption(cid); });
    };
    ReportViewComponent.prototype.deleteoption = function (cid) {
        var _this = this;
        this.reportService.deleteReport(cid).subscribe(function () {
            _this.queryReportView();
            _this.alert('删除成功');
        }, function (err) {
            var message;
            if (JSON.parse(JSON.stringify(err)).status === 0 || JSON.parse(JSON.stringify(err)).status === 504) {
                message = '似乎网络出现了问题，请联系管理员或稍后重试';
            }
            else {
                message = err;
            }
            _this.confirmationService.confirm({
                message: message,
                rejectVisible: false,
            });
        });
    };
    ReportViewComponent.prototype.queryReportView = function () {
        var _this = this;
        this.reportService.getReportView().subscribe(function (data) {
            //1.将获取的数据保存到变量report中;
            //2.遍历report数组
            _this.report = data.handle_count;
            if (!_this.report) {
                _this.report = [];
            }
            for (var i = 0; i < _this.report.length; i++) {
                _this.reportName.push(_this.report[i].name);
                _this.reportCount.push(_this.report[i].count);
            }
            //  报障总览
            _this.reportView = data.type_count;
            if (!_this.reportView) {
                _this.reportView = [];
            }
            for (var i = 0; i < _this.reportView.length; i++) {
                _this.reportViewName.push(_this.reportView[i].name);
                _this.reportViewCount.push(_this.reportView[i].count);
            }
            _this.bar = {
                title: {
                    subtext: '',
                    x: 'center'
                },
                color: ['#3398DB'],
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                    },
                    formatter: ""
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
                        // data: this.reportName,
                        axisTick: {
                            alignWithLabel: true
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '数量',
                        type: 'bar',
                        barWidth: '60%',
                        data: [10, 52, 200, 334, 390, 330, 220, 1000, 500, 444, 999, 11]
                        // data: this.reportCount
                    }
                ]
            };
            _this.pie = {
                theme: '',
                event: [
                    {
                        type: "click",
                        cb: function (res) {
                            console.log(res);
                        }
                    }
                ],
                title: {
                    text: '',
                    subtext: '',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: ['深圳', '北京', '广州', '上海', '长沙']
                    // data: this.reportViewName
                },
                series: [{
                        name: '访问来源',
                        type: 'pie',
                        startAngle: -180,
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: _this.reportService.formatPieData(_this.reportView),
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }]
            };
            //查询今日报障
            _this.dataSource = data.list;
            if (!_this.dataSource) {
                _this.dataSource = [];
            }
            for (var i = 0; i < _this.dataSource.length; i++) {
                var devices = _this.dataSource[i]['devices'] ? _this.dataSource[i]['devices'] : [];
                // let devices = this.dataSource[i]['devices'];
                _this.dataSource[i]['showdevices'] = [];
                for (var j = 0; j < devices.length; j++) {
                    var temp = devices[j]['ano'];
                    _this.dataSource[i]['showdevices'].push(temp);
                }
                _this.dataSource[i]['showdevices'].join(";");
            }
            _this.totalRecords = _this.dataSource.length;
            _this.historyReportOrder = _this.dataSource.slice(0, 10);
        }, function (err) {
        });
    };
    ReportViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-report-view',
            template: __webpack_require__("../../../../../src/app/report/report-view/report-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/report-view/report-view.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__report_service__["a" /* ReportService */],
            __WEBPACK_IMPORTED_MODULE_2__services_storage_service__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"],
            __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["ConfirmationService"],
            __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__["MessageService"]])
    ], ReportViewComponent);
    return ReportViewComponent;
}(__WEBPACK_IMPORTED_MODULE_6__base_page__["a" /* BasePage */]));



/***/ }),

/***/ "../../../../../src/app/report/report.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportModule", function() { return ReportModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__report_start_report_start_component__ = __webpack_require__("../../../../../src/app/report/report-start/report-start.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__report_view_report_view_component__ = __webpack_require__("../../../../../src/app/report/report-view/report-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_share_module__ = __webpack_require__("../../../../../src/app/shared/share.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__report_routing_module__ = __webpack_require__("../../../../../src/app/report/report-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__history_report_history_report_component__ = __webpack_require__("../../../../../src/app/report/history-report/history-report.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__report_service__ = __webpack_require__("../../../../../src/app/report/report.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__view_detail_view_detail_component__ = __webpack_require__("../../../../../src/app/report/view-detail/view-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__report_treatment_report_treatment_component__ = __webpack_require__("../../../../../src/app/report/report-treatment/report-treatment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__report_handle_view_report_handle_view_component__ = __webpack_require__("../../../../../src/app/report/report-handle-view/report-handle-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__report_view_echart_report_view_echart_component__ = __webpack_require__("../../../../../src/app/report/report-view-echart/report-view-echart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__person_dialog_person_dialog_component__ = __webpack_require__("../../../../../src/app/report/person-dialog/person-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_public_service__ = __webpack_require__("../../../../../src/app/services/public.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__report_add_equipment_report_add_equipment_component__ = __webpack_require__("../../../../../src/app/report/report-add-equipment/report-add-equipment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__report_location_report_location_component__ = __webpack_require__("../../../../../src/app/report/report-location/report-location.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var ReportModule = (function () {
    function ReportModule() {
    }
    ReportModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__report_start_report_start_component__["a" /* ReportStartComponent */],
                __WEBPACK_IMPORTED_MODULE_2__report_view_report_view_component__["a" /* ReportViewComponent */],
                __WEBPACK_IMPORTED_MODULE_5__history_report_history_report_component__["a" /* HistoryReportComponent */],
                __WEBPACK_IMPORTED_MODULE_7__view_detail_view_detail_component__["a" /* ViewDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_8__report_treatment_report_treatment_component__["a" /* ReportTreatmentComponent */],
                __WEBPACK_IMPORTED_MODULE_9__report_handle_view_report_handle_view_component__["a" /* ReportHandleViewComponent */],
                __WEBPACK_IMPORTED_MODULE_10__report_view_echart_report_view_echart_component__["a" /* ReportViewEchartComponent */],
                __WEBPACK_IMPORTED_MODULE_12__person_dialog_person_dialog_component__["a" /* PersonDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_14__report_add_equipment_report_add_equipment_component__["a" /* ReportAddEquipmentComponent */],
                __WEBPACK_IMPORTED_MODULE_15__report_location_report_location_component__["a" /* ReportLocationComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__shared_share_module__["a" /* ShareModule */],
                __WEBPACK_IMPORTED_MODULE_4__report_routing_module__["a" /* ReportRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_11_primeng_primeng__["SplitButtonModule"],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_6__report_service__["a" /* ReportService */], __WEBPACK_IMPORTED_MODULE_13__services_public_service__["a" /* PublicService */]]
        })
    ], ReportModule);
    return ReportModule;
}());



/***/ }),

/***/ "../../../../../src/app/report/report.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_storage_service__ = __webpack_require__("../../../../../src/app/services/storage.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReportService = (function () {
    function ReportService(http, storageService) {
        this.http = http;
        this.storageService = storageService;
    }
    //添加
    ReportService.prototype.onSubmitReport = function (report) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            'access_token': token,
            'type': 'fault_add',
            'data': {
                'cid': report.cid,
                'people': report.people,
                'fault_type': report.fault_type,
                'status': report.status,
                'time': report.time,
                'location': report.location,
                'content': report.content,
                'title': report.title,
                'handle_people': report.handle_people,
                'handle_datetime': report.handle_datetime,
                'img_url': report.img_url,
                'devices': report.submitdevices,
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //修改
    ReportService.prototype.updateReport = function (report) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            access_token: token,
            type: 'fault_mod',
            'data': {
                'people': report.people,
                'cid': report.cid,
                'fault_type': report.fault_type,
                'status': report.status,
                'time': report.time,
                'location': report.location,
                'content': report.content,
                'title': report.title,
                'handle_people': report.handle_people,
                'handle_datetime': report.handle_datetime,
                'img_url': report.img_url,
                'devices': report.subdevices,
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //查询历史报障
    ReportService.prototype.getHistoryReport = function (report) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            'access_token': token,
            'type': 'fault_get',
            'data': {
                'cid': report.cid,
                'people': report.people,
                'fault_type': report.fault_type,
                'status': report.status,
                'time_start': report.time_start,
                'time_end': report.time_end,
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //删除保障单接口
    ReportService.prototype.deleteReport = function (cids) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            "access_token": token,
            "type": "fault_del",
            "cids": [cids]
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    //  报障总览查询
    ReportService.prototype.getReportView = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            'access_token': token,
            'type': 'fault_statistics',
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //搜索建议
    ReportService.prototype.searchChange = function (queryModel) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            'access_token': '',
            'type': 'asset_diffield_get',
            'data': {
                'field': queryModel.field,
                'value': queryModel.value
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    ReportService.prototype.handleReport = function (report) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            'access_token': token,
            'type': 'fault_handle',
            'data': {
                'cid': report.id,
                'handle_content': report.handle_content
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //查询资产
    ReportService.prototype.getAssets = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/asset", {
            'access_token': token,
            'type': 'asset_get'
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
            // let body = res.json();
            // if (body.errcode !== '00000') {
            //   throw new Error(body.errmsg);
            // }
            // return body.datas
        });
    };
    //查找功能
    ReportService.prototype.queryByKeyWords = function (asset) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/asset", {
            'access_token': token,
            'type': 'asset_get',
            'datas': {
                'ano': asset.ano,
                'father': asset.father,
                'card_position': asset.card_position,
                'asset_no': asset.asset_no,
                'name': asset.name,
                'brand': asset.brand,
                'modle': asset.modle,
                'function': asset.function,
                'system': asset.system,
                'status': asset.status,
                'on_line_date_start': asset.on_line_date_start,
                'on_line_date_end': asset.on_line_date_end,
                'room': asset.room,
                'cabinet': asset.cabinet,
                'location': asset.location,
                'belong_dapart': asset.belong_dapart,
                'belong_dapart_manager': asset.belong_dapart_manager,
                'manage_dapart': asset.manage_dapart,
                'manager': asset.manager,
                'ip': asset.ip,
                'maintain_vender': asset.maintain_vender
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
            // let body = res.json();
            // if(body.errcode!=='00000'){
            //   throw new Error(body.errmsg)
            // }
            // return body.datas
        });
    };
    //所有状态获取接口
    ReportService.prototype.getStatusReport = function () {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            'access_token': token,
            'type': 'fault_get_allstatus',
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                throw new Error(res['errmsg']);
            }
            return res['datas'];
        });
    };
    //保障发起保存接口
    ReportService.prototype.queryReportSave = function (report) {
        var token = this.storageService.getToken('token');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].url.management + "/fault", {
            "access_token": token,
            "type": "fault_save",
            "data": {
                'people': report.people,
                'fault_type': report.fault_type,
                'status': report.status,
                'time': report.time,
                'location': report.location,
                'content': report.content,
                'title': report.title,
                'handle_people': report.handle_people,
                'handle_datetime': report.handle_datetime,
                'img_url': report.img_url,
                "devices": report.submitdevices
            }
        }).map(function (res) {
            if (res['errcode'] !== '00000') {
                var reg = /No datas/;
                if (res['errmsg'].search(reg) !== -1) {
                    return [];
                }
                throw new Error(res['errmsg']);
            }
            return res['data'];
        });
    };
    ReportService.prototype.formatPieData = function (arr) {
        var newarr = [];
        for (var i = 0; i < arr.length; i++) {
            var obj = {};
            var def = arr[i];
            obj['name'] = def['name'];
            obj['value'] = def['count'];
            newarr.push(obj);
        }
        return newarr;
    };
    ReportService.prototype.formatDropdownData = function (arr) {
        var newarr = [];
        for (var i = 0; i < arr.length; i++) {
            var obj = {};
            var temp = arr[i];
            obj['label'] = temp['name'];
            obj['value'] = temp['code'];
            newarr.push(obj);
        }
        return newarr;
    };
    ReportService.prototype.getFormatTime = function (date) {
        if (!date) {
            date = new Date();
        }
        else {
            date = new Date(date);
        }
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        var hour = date.getHours();
        var minutes = date.getMinutes();
        var second = date.getSeconds();
        day = day <= 9 ? '0' + day : day;
        month = month <= 9 ? '0' + month : month;
        hour = hour <= 9 ? '0' + hour : hour;
        minutes = minutes <= 9 ? '0' + minutes : minutes;
        second = second <= 9 ? '0' + second : second;
        return year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + second;
    };
    ReportService.prototype.deepClone = function (obj) {
        var o, i, j, k;
        if (typeof (obj) != "object" || obj === null)
            return obj;
        if (obj instanceof (Array)) {
            o = [];
            i = 0;
            j = obj.length;
            for (; i < j; i++) {
                if (typeof (obj[i]) == "object" && obj[i] != null) {
                    o[i] = this.deepClone(obj[i]);
                }
                else {
                    o[i] = obj[i];
                }
            }
        }
        else {
            o = {};
            for (i in obj) {
                if (typeof (obj[i]) == "object" && obj[i] != null) {
                    o[i] = this.deepClone(obj[i]);
                }
                else {
                    o[i] = obj[i];
                }
            }
        }
        return o;
    };
    ReportService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__services_storage_service__["a" /* StorageService */]])
    ], ReportService);
    return ReportService;
}());



/***/ }),

/***/ "../../../../../src/app/report/view-detail/view-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<p-dialog  header=\"报障单详情\" [(visible)]=\"display\" modal=\"modal\" [responsive]=\"true\" [width]=\"width\"   (onHide)=\"closeViewDetailMask(false)\" class=\"dialog-height\" >\n    <form  class=\"form-horizontal content\">\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">故障单号：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.cid\"/>\n            </div>\n        </div>\n\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">故障所属：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.fault_type\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">报障人：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.people\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">报障时间：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.time\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">故障位置：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.location\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">故障标题：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.title\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">故障描述：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.content\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">处理人：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.handle_people\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">完成时间：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.handle_datetime\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">故障状态：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <input type=\"text\" pInputText\n                       class=\"form-control cursor_not_allowed\"\n                       placeholder=\"\"\n                       readonly\n                       [ngModelOptions]=\"{standalone: true}\"\n                       [(ngModel)]=\"reportDetail.status\"/>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">故障处理内容：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n               <textarea [rows]=\"5\" pInputTextarea\n                         autoResize=\"autoResize\"\n                         readonly\n                         class=\"form-control\"\n                         [ngModelOptions]=\"{standalone: true}\"\n                         [(ngModel)]=\"reportDetail.handle_content\"></textarea>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">关联设备：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n               <textarea [rows]=\"5\" pInputTextarea\n                         autoResize=\"autoResize\"\n                         readonly\n                         class=\"form-control\"\n                         [ngModelOptions]=\"{standalone: true}\"\n                         [(ngModel)]=\"reportDetail.showdevices\"></textarea>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\">现场照片：</label>\n            <div class=\"col-sm-10 ui-fluid-no-padding\">\n                <ng-container *ngFor=\"let img of imgUrl\">\n                    <img src=\"{{img}}\" alt=\"无图片信息\" class=\"img-my\">\n                </ng-container>\n            </div>\n        </div>\n    </form>\n    <p-footer>\n      <!--<button type=\"button\" pButton icon=\"fa-close\" (click)=\"closeViewDetailMask(false)\" label=\"取消\"></button>-->\n    </p-footer>\n  </p-dialog>\n\n"

/***/ }),

/***/ "../../../../../src/app/report/view-detail/view-detail.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input .my-dirty {\n  border-bottom: solid 1px orangered; }\n\ninput:not(.my-dirty) {\n  border-bottom: solid 1px #bfbaba !important; }\n\n.cursor_not_allowed {\n  cursor: not-allowed; }\n\n#myContent {\n  font-size: 14px; }\n  #myContent div[class~=\"title\"] {\n    color: #666666;\n    background: white;\n    border: 1px solid #e2e2e2;\n    height: 70px;\n    margin-bottom: 20px;\n    padding-left: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #myContent div[class~=\"title\"] button {\n      border: 1px solid red;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse; }\n  #myContent form {\n    background: white;\n    padding: 20px 26px 20px 0px;\n    border: 1px solid #e2e2e2; }\n\n.padding-left-2vw {\n  padding-left: 2vw; }\n\n@media (min-width: 768px) {\n  .col-sm-2 {\n    width: 13.666667%; }\n  .col-sm-10 {\n    width: 81.333333%; }\n  .col-sm-9 {\n    width: 80.2%; } }\n\n.dialog-height {\n  max-height: 39vw; }\n\n.img-my {\n  max-width: 975px; }\n\n.content {\n  max-height: 400px;\n  overflow: auto;\n  overflow-x: hidden; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/report/view-detail/view-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ViewDetailComponent = (function () {
    function ViewDetailComponent() {
        this.closeViewDetail = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.imgUrl = [];
    }
    ViewDetailComponent.prototype.ngOnInit = function () {
        this.windowSize = window.innerWidth;
        if (this.windowSize < 1024) {
            this.width = this.windowSize * 0.4;
        }
        else {
            this.width = this.windowSize * 0.6;
        }
        this.display = true;
        if (!this.reportDetail.img_url) {
            this.imgUrl = [];
        }
        else {
            this.imgUrl = this.reportDetail.img_url.split(';');
            for (var i = 0; i < this.imgUrl.length; i++) {
                this.imgUrl[i] = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].url.management + '/' + this.imgUrl[i];
            }
            // this.imgUrl = environment.url.management+'/'+this.reportDetail.img_url;
        }
    };
    ViewDetailComponent.prototype.closeViewDetailMask = function (bool) {
        this.closeViewDetail.emit(bool);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ViewDetailComponent.prototype, "closeViewDetail", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ViewDetailComponent.prototype, "reportDetail", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ViewDetailComponent.prototype, "state", void 0);
    ViewDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-view-detail',
            template: __webpack_require__("../../../../../src/app/report/view-detail/view-detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/report/view-detail/view-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ViewDetailComponent);
    return ViewDetailComponent;
}());



/***/ })

});
//# sourceMappingURL=report.module.chunk.js.map